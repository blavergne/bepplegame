import lime.Assets;
#if !macro


class ApplicationMain {
	
	
	public static var config:lime.app.Config;
	public static var preloader:openfl.display.Preloader;
	
	private static var app:lime.app.Application;
	
	
	public static function create ():Void {
		
		app = new openfl.display.Application ();
		app.create (config);
		
		var display = new flixel.system.FlxPreloader ();
		
		preloader = new openfl.display.Preloader (display);
		preloader.onComplete = init;
		preloader.create (config);
		
		#if js
		var urls = [];
		var types = [];
		
		
		urls.push ("assets/data/data-goes-here.txt");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/images/animatedAppleA.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/animatedAppleB.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/answerButton.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/appleJuiceItemImage.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/appleRotAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/appleShedAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/appleTruck.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/appleTruckButtonOnAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/backGroundMassA.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/backGroundMassAmaster.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/backGroundMassB.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/backGroundMassBmaster.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/backGroundMassC.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/backGroundMassCmaster.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/backGroundMassD.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/backGroundMassDmaster.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/backGroundMassE.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/backGroundMassEmaster.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/backGroundMassF.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/backGroundMassFmaster.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/backGroundMassFMouse.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/backGroundMassG.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/backGroundMassGmaster.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/backGroundMassH.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/backGroundMassHmaster.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/basketAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/blackBoard.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/blackBoardCorrectBorder.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/blackBoardWrongBorder.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/boilerImage.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/bridgeImage.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/buildBar.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/buildBarShaft.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/buildBarTab.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/buttonClear.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/buttonEight.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/buttonFive.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/buttonFour.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/buttonNine.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/buttonOne.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/buttonSeven.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/buttonSix.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/buttonThree.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/buttonTwo.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/buttonZero.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/buttonZeroShadow.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/buyLandButton.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/calcImage.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/chalkEquals.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/chalkNumbers_0.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/chalkNumbers_1.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/chalkNumbers_2.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/chalkNumbers_3.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/chalkNumbers_4.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/chalkNumbers_5.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/chalkNumbers_6.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/chalkNumbers_7.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/chalkNumbers_8.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/chalkNumbers_9.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/cloud_blue.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/coyPondImage.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/dropletsAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/dropletsAnimationS.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/dropletsAnimationT.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/dropletsAnimationU.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/fillingBottleAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/fishIcon.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/fishItemAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/gameTile.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/gameTileBoarder.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/gameTileOwned.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/goldCoin.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/greenHouseAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/greenHouseIconImage.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/GreenHouseItemAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/greenHouseShrub.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/greenHouseTree.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/greenHouseTreeAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/holeAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/images-go-here.txt");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/images/islandMouseAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/juiceMachineAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/juiceMachineIcon.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/juiceMachineSwitchAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/koiFishAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/koiFishItemDrag.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/koiPad.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/koiPondAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/koiPondImage.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/koiRock.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/koiSand.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/marketSideBar.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/marketSideBarShaft.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/marketSideBarTab.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/market_seed.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/meteorField.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/meteorFieldMaster.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/minusOneImage.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/minusOperation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/mouseAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/multiOperation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/optionsScreen.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/packOfJuiceIcon.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/packOfPiesIcon.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/pickedAppleLeaves.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/pieIcon.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/pieMachineAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/pieMachineItem.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/plusOperation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/rainCloudAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/rainCloudAnimationA.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/repairAnimationS.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/rockyForeground.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/rollingMoon.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/seedItemAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/sidewaysIsland.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/spaceBackground.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/sprinklerAlertAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/sprinklerAlertAnimationS.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/sprinklerAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/sprinklerSwtichAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/sunAnimationA.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/sunAnimationB.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/sunAnimationC.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/sunTokenAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/systemStar.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/systemStarAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/tileAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/tileAnimationTEST.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/tileSprite.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/titleBackground.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/titleEasy.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/titleImage.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/titleImageProcessed.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/titleScreenGrass.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/treeAnim0.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/treeAnim1.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/treeAnim2.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/treeAnim3.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/treeAnim4.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/treeAnim5.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/treeAnim6.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/treeAnim7.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/treeAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/treeAnimationA.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/treeAnimationB.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/treeDeathAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/treeDeathAnimation97.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/treeDeathAnimation98.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/treeDeathAnimation99.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/treeSpriteA.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/treeSpriteA99.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/tree_seed.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/truckBellAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/truckBellImage.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/tutCheckBoxAnimate.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/tutHand.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/tutHandAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/tutMessageEight.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/tutMessageFive.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/tutMessageFour.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/tutMessageNine.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/tutMessageOne.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/tutMessageSeven.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/tutMessageSix.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/tutMessageTen.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/tutMessageThree.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/tutMessageTwo.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/upsideDownIsland.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/waterfallAnimation.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/waterfallImage.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/waterFallTile.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/weedA.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/images/weedB.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/music/BeppleSongONe.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/music/BeppleSongOneOGG.ogg");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/music/music-goes-here.txt");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/sounds/alertButtonSound.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/buySignSound.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/calcButtonSound.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/cashRegisterSound.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/coinsSound.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/correctAnswerSoundA.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/fallenAppleSound.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/fillingJuiceBottleSound.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/greenHouseWindChimeSound.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/growingTree.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/growingTreeA.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/growingTreeB.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/growingTreeC.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/growingTreeD.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/incorrectAnswerSound.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/juiceMachineSound.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/koiClicked0.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/koiClicked1.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/koiClicked2.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/koiClicked4.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/koiPondPlacedSound.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/koiPondSound.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/marketSlideClosedSound.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/marketSlideOpenSound.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/mouseHoleSound.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/mouseWalkingSound.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/pickedApplesGround.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/pickedLeavesSound.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/pieBakedSound.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/placedMachineSound.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/plantingSeedSound.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/pouringJuiceSound.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/raining.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/sounds-go-here.txt");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/sounds/sprinklerButtonSound.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/sprinklerSound.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/sunshine.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/truckButtonSound.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/truckSound.wav");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/sounds/beep.mp3");
		types.push (AssetType.MUSIC);
		
		
		urls.push ("assets/sounds/flixel.mp3");
		types.push (AssetType.MUSIC);
		
		
		
		preloader.load (urls, types);
		#end
		
		var result = app.exec ();
		
		#if sys
		Sys.exit (result);
		#end
		
	}
	
	
	public static function init ():Void {
		
		var loaded = 0;
		var total = 0;
		var library_onLoad = function (__) {
			
			loaded++;
			
			if (loaded == total) {
				
				start ();
				
			}
			
		}
		
		preloader = null;
		
		
		
		if (loaded == total) {
			
			start ();
			
		}
		
	}
	
	
	public static function main () {
		
		config = {
			
			antialiasing: Std.int (0),
			background: Std.int (0),
			borderless: false,
			depthBuffer: false,
			fps: Std.int (60),
			fullscreen: false,
			height: Std.int (480),
			orientation: "landscape",
			resizable: true,
			stencilBuffer: false,
			title: "Bepple",
			vsync: true,
			width: Std.int (640),
			
		}
		
		#if js
		#if (munit || utest)
		flash.Lib.embed (null, 640, 480, "000000");
		#end
		#else
		create ();
		#end
		
	}
	
	
	public static function start ():Void {
		
		openfl.Lib.current.stage.align = openfl.display.StageAlign.TOP_LEFT;
		openfl.Lib.current.stage.scaleMode = openfl.display.StageScaleMode.NO_SCALE;
		
		var hasMain = false;
		
		for (methodName in Type.getClassFields (Main)) {
			
			if (methodName == "main") {
				
				hasMain = true;
				break;
				
			}
			
		}
		
		if (hasMain) {
			
			Reflect.callMethod (Main, Reflect.field (Main, "main"), []);
			
		} else {
			
			var instance:DocumentClass = Type.createInstance (DocumentClass, []);
			
			/*if (Std.is (instance, openfl.display.DisplayObject)) {
				
				openfl.Lib.current.addChild (cast instance);
				
			}*/
			
		}
		
		openfl.Lib.current.stage.dispatchEvent (new openfl.events.Event (openfl.events.Event.RESIZE, false, false));
		
	}
	
	
	#if neko
	@:noCompletion public static function __init__ () {
		
		var loader = new neko.vm.Loader (untyped $loader);
		loader.addPath (haxe.io.Path.directory (Sys.executablePath ()));
		loader.addPath ("./");
		loader.addPath ("@executable_path/");
		
	}
	#end
	
	
}


@:build(DocumentClass.build())
@:keep class DocumentClass extends Main {}


#else


import haxe.macro.Context;
import haxe.macro.Expr;


class DocumentClass {
	
	
	macro public static function build ():Array<Field> {
		
		var classType = Context.getLocalClass ().get ();
		var searchTypes = classType;
		
		while (searchTypes.superClass != null) {
			
			if (searchTypes.pack.length == 2 && searchTypes.pack[1] == "display" && searchTypes.name == "DisplayObject") {
				
				var fields = Context.getBuildFields ();
				
				var method = macro {
					
					openfl.Lib.current.addChild (this);
					super ();
					dispatchEvent (new openfl.events.Event (openfl.events.Event.ADDED_TO_STAGE, false, false));
					
				}
				
				fields.push ({ name: "new", access: [ APublic ], kind: FFun({ args: [], expr: method, params: [], ret: macro :Void }), pos: Context.currentPos () });
				
				return fields;
				
			}
			
			searchTypes = searchTypes.superClass.t.get ();
			
		}
		
		return null;
		
	}
	
	
}


#end
