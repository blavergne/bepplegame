package;


import haxe.Timer;
import haxe.Unserializer;
import lime.app.Preloader;
import lime.audio.openal.AL;
import lime.audio.AudioBuffer;
import lime.graphics.Font;
import lime.graphics.Image;
import lime.utils.ByteArray;
import lime.utils.UInt8Array;
import lime.Assets;

#if (sys || nodejs)
import sys.FileSystem;
#end

#if flash
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Loader;
import flash.events.Event;
import flash.media.Sound;
import flash.net.URLLoader;
import flash.net.URLRequest;
#end


class DefaultAssetLibrary extends AssetLibrary {
	
	
	public var className (default, null) = new Map <String, Dynamic> ();
	public var path (default, null) = new Map <String, String> ();
	public var type (default, null) = new Map <String, AssetType> ();
	
	private var lastModified:Float;
	private var timer:Timer;
	
	
	public function new () {
		
		super ();
		
		#if flash
		
		className.set ("assets/data/data-goes-here.txt", __ASSET__assets_data_data_goes_here_txt);
		type.set ("assets/data/data-goes-here.txt", AssetType.TEXT);
		className.set ("assets/images/animatedAppleA.png", __ASSET__assets_images_animatedapplea_png);
		type.set ("assets/images/animatedAppleA.png", AssetType.IMAGE);
		className.set ("assets/images/animatedAppleB.png", __ASSET__assets_images_animatedappleb_png);
		type.set ("assets/images/animatedAppleB.png", AssetType.IMAGE);
		className.set ("assets/images/answerButton.png", __ASSET__assets_images_answerbutton_png);
		type.set ("assets/images/answerButton.png", AssetType.IMAGE);
		className.set ("assets/images/appleJuiceItemImage.png", __ASSET__assets_images_applejuiceitemimage_png);
		type.set ("assets/images/appleJuiceItemImage.png", AssetType.IMAGE);
		className.set ("assets/images/appleRotAnimation.png", __ASSET__assets_images_applerotanimation_png);
		type.set ("assets/images/appleRotAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/appleShedAnimation.png", __ASSET__assets_images_appleshedanimation_png);
		type.set ("assets/images/appleShedAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/appleTruck.png", __ASSET__assets_images_appletruck_png);
		type.set ("assets/images/appleTruck.png", AssetType.IMAGE);
		className.set ("assets/images/appleTruckButtonOnAnimation.png", __ASSET__assets_images_appletruckbuttononanimation_png);
		type.set ("assets/images/appleTruckButtonOnAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/backGroundMassA.png", __ASSET__assets_images_backgroundmassa_png);
		type.set ("assets/images/backGroundMassA.png", AssetType.IMAGE);
		className.set ("assets/images/backGroundMassAmaster.png", __ASSET__assets_images_backgroundmassamaster_png);
		type.set ("assets/images/backGroundMassAmaster.png", AssetType.IMAGE);
		className.set ("assets/images/backGroundMassB.png", __ASSET__assets_images_backgroundmassb_png);
		type.set ("assets/images/backGroundMassB.png", AssetType.IMAGE);
		className.set ("assets/images/backGroundMassBmaster.png", __ASSET__assets_images_backgroundmassbmaster_png);
		type.set ("assets/images/backGroundMassBmaster.png", AssetType.IMAGE);
		className.set ("assets/images/backGroundMassC.png", __ASSET__assets_images_backgroundmassc_png);
		type.set ("assets/images/backGroundMassC.png", AssetType.IMAGE);
		className.set ("assets/images/backGroundMassCmaster.png", __ASSET__assets_images_backgroundmasscmaster_png);
		type.set ("assets/images/backGroundMassCmaster.png", AssetType.IMAGE);
		className.set ("assets/images/backGroundMassD.png", __ASSET__assets_images_backgroundmassd_png);
		type.set ("assets/images/backGroundMassD.png", AssetType.IMAGE);
		className.set ("assets/images/backGroundMassDmaster.png", __ASSET__assets_images_backgroundmassdmaster_png);
		type.set ("assets/images/backGroundMassDmaster.png", AssetType.IMAGE);
		className.set ("assets/images/backGroundMassE.png", __ASSET__assets_images_backgroundmasse_png);
		type.set ("assets/images/backGroundMassE.png", AssetType.IMAGE);
		className.set ("assets/images/backGroundMassEmaster.png", __ASSET__assets_images_backgroundmassemaster_png);
		type.set ("assets/images/backGroundMassEmaster.png", AssetType.IMAGE);
		className.set ("assets/images/backGroundMassF.png", __ASSET__assets_images_backgroundmassf_png);
		type.set ("assets/images/backGroundMassF.png", AssetType.IMAGE);
		className.set ("assets/images/backGroundMassFmaster.png", __ASSET__assets_images_backgroundmassfmaster_png);
		type.set ("assets/images/backGroundMassFmaster.png", AssetType.IMAGE);
		className.set ("assets/images/backGroundMassFMouse.png", __ASSET__assets_images_backgroundmassfmouse_png);
		type.set ("assets/images/backGroundMassFMouse.png", AssetType.IMAGE);
		className.set ("assets/images/backGroundMassG.png", __ASSET__assets_images_backgroundmassg_png);
		type.set ("assets/images/backGroundMassG.png", AssetType.IMAGE);
		className.set ("assets/images/backGroundMassGmaster.png", __ASSET__assets_images_backgroundmassgmaster_png);
		type.set ("assets/images/backGroundMassGmaster.png", AssetType.IMAGE);
		className.set ("assets/images/backGroundMassH.png", __ASSET__assets_images_backgroundmassh_png);
		type.set ("assets/images/backGroundMassH.png", AssetType.IMAGE);
		className.set ("assets/images/backGroundMassHmaster.png", __ASSET__assets_images_backgroundmasshmaster_png);
		type.set ("assets/images/backGroundMassHmaster.png", AssetType.IMAGE);
		className.set ("assets/images/basketAnimation.png", __ASSET__assets_images_basketanimation_png);
		type.set ("assets/images/basketAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/blackBoard.png", __ASSET__assets_images_blackboard_png);
		type.set ("assets/images/blackBoard.png", AssetType.IMAGE);
		className.set ("assets/images/blackBoardCorrectBorder.png", __ASSET__assets_images_blackboardcorrectborder_png);
		type.set ("assets/images/blackBoardCorrectBorder.png", AssetType.IMAGE);
		className.set ("assets/images/blackBoardWrongBorder.png", __ASSET__assets_images_blackboardwrongborder_png);
		type.set ("assets/images/blackBoardWrongBorder.png", AssetType.IMAGE);
		className.set ("assets/images/boilerImage.png", __ASSET__assets_images_boilerimage_png);
		type.set ("assets/images/boilerImage.png", AssetType.IMAGE);
		className.set ("assets/images/bridgeImage.png", __ASSET__assets_images_bridgeimage_png);
		type.set ("assets/images/bridgeImage.png", AssetType.IMAGE);
		className.set ("assets/images/buildBar.png", __ASSET__assets_images_buildbar_png);
		type.set ("assets/images/buildBar.png", AssetType.IMAGE);
		className.set ("assets/images/buildBarShaft.png", __ASSET__assets_images_buildbarshaft_png);
		type.set ("assets/images/buildBarShaft.png", AssetType.IMAGE);
		className.set ("assets/images/buildBarTab.png", __ASSET__assets_images_buildbartab_png);
		type.set ("assets/images/buildBarTab.png", AssetType.IMAGE);
		className.set ("assets/images/buttonClear.png", __ASSET__assets_images_buttonclear_png);
		type.set ("assets/images/buttonClear.png", AssetType.IMAGE);
		className.set ("assets/images/buttonEight.png", __ASSET__assets_images_buttoneight_png);
		type.set ("assets/images/buttonEight.png", AssetType.IMAGE);
		className.set ("assets/images/buttonFive.png", __ASSET__assets_images_buttonfive_png);
		type.set ("assets/images/buttonFive.png", AssetType.IMAGE);
		className.set ("assets/images/buttonFour.png", __ASSET__assets_images_buttonfour_png);
		type.set ("assets/images/buttonFour.png", AssetType.IMAGE);
		className.set ("assets/images/buttonNine.png", __ASSET__assets_images_buttonnine_png);
		type.set ("assets/images/buttonNine.png", AssetType.IMAGE);
		className.set ("assets/images/buttonOne.png", __ASSET__assets_images_buttonone_png);
		type.set ("assets/images/buttonOne.png", AssetType.IMAGE);
		className.set ("assets/images/buttonSeven.png", __ASSET__assets_images_buttonseven_png);
		type.set ("assets/images/buttonSeven.png", AssetType.IMAGE);
		className.set ("assets/images/buttonSix.png", __ASSET__assets_images_buttonsix_png);
		type.set ("assets/images/buttonSix.png", AssetType.IMAGE);
		className.set ("assets/images/buttonThree.png", __ASSET__assets_images_buttonthree_png);
		type.set ("assets/images/buttonThree.png", AssetType.IMAGE);
		className.set ("assets/images/buttonTwo.png", __ASSET__assets_images_buttontwo_png);
		type.set ("assets/images/buttonTwo.png", AssetType.IMAGE);
		className.set ("assets/images/buttonZero.png", __ASSET__assets_images_buttonzero_png);
		type.set ("assets/images/buttonZero.png", AssetType.IMAGE);
		className.set ("assets/images/buttonZeroShadow.png", __ASSET__assets_images_buttonzeroshadow_png);
		type.set ("assets/images/buttonZeroShadow.png", AssetType.IMAGE);
		className.set ("assets/images/buyLandButton.png", __ASSET__assets_images_buylandbutton_png);
		type.set ("assets/images/buyLandButton.png", AssetType.IMAGE);
		className.set ("assets/images/calcImage.png", __ASSET__assets_images_calcimage_png);
		type.set ("assets/images/calcImage.png", AssetType.IMAGE);
		className.set ("assets/images/chalkEquals.png", __ASSET__assets_images_chalkequals_png);
		type.set ("assets/images/chalkEquals.png", AssetType.IMAGE);
		className.set ("assets/images/chalkNumbers_0.png", __ASSET__assets_images_chalknumbers_0_png);
		type.set ("assets/images/chalkNumbers_0.png", AssetType.IMAGE);
		className.set ("assets/images/chalkNumbers_1.png", __ASSET__assets_images_chalknumbers_1_png);
		type.set ("assets/images/chalkNumbers_1.png", AssetType.IMAGE);
		className.set ("assets/images/chalkNumbers_2.png", __ASSET__assets_images_chalknumbers_2_png);
		type.set ("assets/images/chalkNumbers_2.png", AssetType.IMAGE);
		className.set ("assets/images/chalkNumbers_3.png", __ASSET__assets_images_chalknumbers_3_png);
		type.set ("assets/images/chalkNumbers_3.png", AssetType.IMAGE);
		className.set ("assets/images/chalkNumbers_4.png", __ASSET__assets_images_chalknumbers_4_png);
		type.set ("assets/images/chalkNumbers_4.png", AssetType.IMAGE);
		className.set ("assets/images/chalkNumbers_5.png", __ASSET__assets_images_chalknumbers_5_png);
		type.set ("assets/images/chalkNumbers_5.png", AssetType.IMAGE);
		className.set ("assets/images/chalkNumbers_6.png", __ASSET__assets_images_chalknumbers_6_png);
		type.set ("assets/images/chalkNumbers_6.png", AssetType.IMAGE);
		className.set ("assets/images/chalkNumbers_7.png", __ASSET__assets_images_chalknumbers_7_png);
		type.set ("assets/images/chalkNumbers_7.png", AssetType.IMAGE);
		className.set ("assets/images/chalkNumbers_8.png", __ASSET__assets_images_chalknumbers_8_png);
		type.set ("assets/images/chalkNumbers_8.png", AssetType.IMAGE);
		className.set ("assets/images/chalkNumbers_9.png", __ASSET__assets_images_chalknumbers_9_png);
		type.set ("assets/images/chalkNumbers_9.png", AssetType.IMAGE);
		className.set ("assets/images/cloud_blue.png", __ASSET__assets_images_cloud_blue_png);
		type.set ("assets/images/cloud_blue.png", AssetType.IMAGE);
		className.set ("assets/images/coyPondImage.png", __ASSET__assets_images_coypondimage_png);
		type.set ("assets/images/coyPondImage.png", AssetType.IMAGE);
		className.set ("assets/images/dropletsAnimation.png", __ASSET__assets_images_dropletsanimation_png);
		type.set ("assets/images/dropletsAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/dropletsAnimationS.png", __ASSET__assets_images_dropletsanimations_png);
		type.set ("assets/images/dropletsAnimationS.png", AssetType.IMAGE);
		className.set ("assets/images/dropletsAnimationT.png", __ASSET__assets_images_dropletsanimationt_png);
		type.set ("assets/images/dropletsAnimationT.png", AssetType.IMAGE);
		className.set ("assets/images/dropletsAnimationU.png", __ASSET__assets_images_dropletsanimationu_png);
		type.set ("assets/images/dropletsAnimationU.png", AssetType.IMAGE);
		className.set ("assets/images/fillingBottleAnimation.png", __ASSET__assets_images_fillingbottleanimation_png);
		type.set ("assets/images/fillingBottleAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/fishIcon.png", __ASSET__assets_images_fishicon_png);
		type.set ("assets/images/fishIcon.png", AssetType.IMAGE);
		className.set ("assets/images/fishItemAnimation.png", __ASSET__assets_images_fishitemanimation_png);
		type.set ("assets/images/fishItemAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/gameTile.png", __ASSET__assets_images_gametile_png);
		type.set ("assets/images/gameTile.png", AssetType.IMAGE);
		className.set ("assets/images/gameTileBoarder.png", __ASSET__assets_images_gametileboarder_png);
		type.set ("assets/images/gameTileBoarder.png", AssetType.IMAGE);
		className.set ("assets/images/gameTileOwned.png", __ASSET__assets_images_gametileowned_png);
		type.set ("assets/images/gameTileOwned.png", AssetType.IMAGE);
		className.set ("assets/images/goldCoin.png", __ASSET__assets_images_goldcoin_png);
		type.set ("assets/images/goldCoin.png", AssetType.IMAGE);
		className.set ("assets/images/greenHouseAnimation.png", __ASSET__assets_images_greenhouseanimation_png);
		type.set ("assets/images/greenHouseAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/greenHouseIconImage.png", __ASSET__assets_images_greenhouseiconimage_png);
		type.set ("assets/images/greenHouseIconImage.png", AssetType.IMAGE);
		className.set ("assets/images/GreenHouseItemAnimation.png", __ASSET__assets_images_greenhouseitemanimation_png);
		type.set ("assets/images/GreenHouseItemAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/greenHouseShrub.png", __ASSET__assets_images_greenhouseshrub_png);
		type.set ("assets/images/greenHouseShrub.png", AssetType.IMAGE);
		className.set ("assets/images/greenHouseTree.png", __ASSET__assets_images_greenhousetree_png);
		type.set ("assets/images/greenHouseTree.png", AssetType.IMAGE);
		className.set ("assets/images/greenHouseTreeAnimation.png", __ASSET__assets_images_greenhousetreeanimation_png);
		type.set ("assets/images/greenHouseTreeAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/holeAnimation.png", __ASSET__assets_images_holeanimation_png);
		type.set ("assets/images/holeAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/images-go-here.txt", __ASSET__assets_images_images_go_here_txt);
		type.set ("assets/images/images-go-here.txt", AssetType.TEXT);
		className.set ("assets/images/islandMouseAnimation.png", __ASSET__assets_images_islandmouseanimation_png);
		type.set ("assets/images/islandMouseAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/juiceMachineAnimation.png", __ASSET__assets_images_juicemachineanimation_png);
		type.set ("assets/images/juiceMachineAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/juiceMachineIcon.png", __ASSET__assets_images_juicemachineicon_png);
		type.set ("assets/images/juiceMachineIcon.png", AssetType.IMAGE);
		className.set ("assets/images/juiceMachineSwitchAnimation.png", __ASSET__assets_images_juicemachineswitchanimation_png);
		type.set ("assets/images/juiceMachineSwitchAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/koiFishAnimation.png", __ASSET__assets_images_koifishanimation_png);
		type.set ("assets/images/koiFishAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/koiFishItemDrag.png", __ASSET__assets_images_koifishitemdrag_png);
		type.set ("assets/images/koiFishItemDrag.png", AssetType.IMAGE);
		className.set ("assets/images/koiPad.png", __ASSET__assets_images_koipad_png);
		type.set ("assets/images/koiPad.png", AssetType.IMAGE);
		className.set ("assets/images/koiPondAnimation.png", __ASSET__assets_images_koipondanimation_png);
		type.set ("assets/images/koiPondAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/koiPondImage.png", __ASSET__assets_images_koipondimage_png);
		type.set ("assets/images/koiPondImage.png", AssetType.IMAGE);
		className.set ("assets/images/koiRock.png", __ASSET__assets_images_koirock_png);
		type.set ("assets/images/koiRock.png", AssetType.IMAGE);
		className.set ("assets/images/koiSand.png", __ASSET__assets_images_koisand_png);
		type.set ("assets/images/koiSand.png", AssetType.IMAGE);
		className.set ("assets/images/marketSideBar.png", __ASSET__assets_images_marketsidebar_png);
		type.set ("assets/images/marketSideBar.png", AssetType.IMAGE);
		className.set ("assets/images/marketSideBarShaft.png", __ASSET__assets_images_marketsidebarshaft_png);
		type.set ("assets/images/marketSideBarShaft.png", AssetType.IMAGE);
		className.set ("assets/images/marketSideBarTab.png", __ASSET__assets_images_marketsidebartab_png);
		type.set ("assets/images/marketSideBarTab.png", AssetType.IMAGE);
		className.set ("assets/images/market_seed.png", __ASSET__assets_images_market_seed_png);
		type.set ("assets/images/market_seed.png", AssetType.IMAGE);
		className.set ("assets/images/meteorField.png", __ASSET__assets_images_meteorfield_png);
		type.set ("assets/images/meteorField.png", AssetType.IMAGE);
		className.set ("assets/images/meteorFieldMaster.png", __ASSET__assets_images_meteorfieldmaster_png);
		type.set ("assets/images/meteorFieldMaster.png", AssetType.IMAGE);
		className.set ("assets/images/minusOneImage.png", __ASSET__assets_images_minusoneimage_png);
		type.set ("assets/images/minusOneImage.png", AssetType.IMAGE);
		className.set ("assets/images/minusOperation.png", __ASSET__assets_images_minusoperation_png);
		type.set ("assets/images/minusOperation.png", AssetType.IMAGE);
		className.set ("assets/images/mouseAnimation.png", __ASSET__assets_images_mouseanimation_png);
		type.set ("assets/images/mouseAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/multiOperation.png", __ASSET__assets_images_multioperation_png);
		type.set ("assets/images/multiOperation.png", AssetType.IMAGE);
		className.set ("assets/images/optionsScreen.png", __ASSET__assets_images_optionsscreen_png);
		type.set ("assets/images/optionsScreen.png", AssetType.IMAGE);
		className.set ("assets/images/packOfJuiceIcon.png", __ASSET__assets_images_packofjuiceicon_png);
		type.set ("assets/images/packOfJuiceIcon.png", AssetType.IMAGE);
		className.set ("assets/images/packOfPiesIcon.png", __ASSET__assets_images_packofpiesicon_png);
		type.set ("assets/images/packOfPiesIcon.png", AssetType.IMAGE);
		className.set ("assets/images/pickedAppleLeaves.png", __ASSET__assets_images_pickedappleleaves_png);
		type.set ("assets/images/pickedAppleLeaves.png", AssetType.IMAGE);
		className.set ("assets/images/pieIcon.png", __ASSET__assets_images_pieicon_png);
		type.set ("assets/images/pieIcon.png", AssetType.IMAGE);
		className.set ("assets/images/pieMachineAnimation.png", __ASSET__assets_images_piemachineanimation_png);
		type.set ("assets/images/pieMachineAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/pieMachineItem.png", __ASSET__assets_images_piemachineitem_png);
		type.set ("assets/images/pieMachineItem.png", AssetType.IMAGE);
		className.set ("assets/images/plusOperation.png", __ASSET__assets_images_plusoperation_png);
		type.set ("assets/images/plusOperation.png", AssetType.IMAGE);
		className.set ("assets/images/rainCloudAnimation.png", __ASSET__assets_images_raincloudanimation_png);
		type.set ("assets/images/rainCloudAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/rainCloudAnimationA.png", __ASSET__assets_images_raincloudanimationa_png);
		type.set ("assets/images/rainCloudAnimationA.png", AssetType.IMAGE);
		className.set ("assets/images/repairAnimationS.png", __ASSET__assets_images_repairanimations_png);
		type.set ("assets/images/repairAnimationS.png", AssetType.IMAGE);
		className.set ("assets/images/rockyForeground.png", __ASSET__assets_images_rockyforeground_png);
		type.set ("assets/images/rockyForeground.png", AssetType.IMAGE);
		className.set ("assets/images/rollingMoon.png", __ASSET__assets_images_rollingmoon_png);
		type.set ("assets/images/rollingMoon.png", AssetType.IMAGE);
		className.set ("assets/images/seedItemAnimation.png", __ASSET__assets_images_seeditemanimation_png);
		type.set ("assets/images/seedItemAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/sidewaysIsland.png", __ASSET__assets_images_sidewaysisland_png);
		type.set ("assets/images/sidewaysIsland.png", AssetType.IMAGE);
		className.set ("assets/images/spaceBackground.png", __ASSET__assets_images_spacebackground_png);
		type.set ("assets/images/spaceBackground.png", AssetType.IMAGE);
		className.set ("assets/images/sprinklerAlertAnimation.png", __ASSET__assets_images_sprinkleralertanimation_png);
		type.set ("assets/images/sprinklerAlertAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/sprinklerAlertAnimationS.png", __ASSET__assets_images_sprinkleralertanimations_png);
		type.set ("assets/images/sprinklerAlertAnimationS.png", AssetType.IMAGE);
		className.set ("assets/images/sprinklerAnimation.png", __ASSET__assets_images_sprinkleranimation_png);
		type.set ("assets/images/sprinklerAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/sprinklerSwtichAnimation.png", __ASSET__assets_images_sprinklerswtichanimation_png);
		type.set ("assets/images/sprinklerSwtichAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/sunAnimationA.png", __ASSET__assets_images_sunanimationa_png);
		type.set ("assets/images/sunAnimationA.png", AssetType.IMAGE);
		className.set ("assets/images/sunAnimationB.png", __ASSET__assets_images_sunanimationb_png);
		type.set ("assets/images/sunAnimationB.png", AssetType.IMAGE);
		className.set ("assets/images/sunAnimationC.png", __ASSET__assets_images_sunanimationc_png);
		type.set ("assets/images/sunAnimationC.png", AssetType.IMAGE);
		className.set ("assets/images/sunTokenAnimation.png", __ASSET__assets_images_suntokenanimation_png);
		type.set ("assets/images/sunTokenAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/systemStar.png", __ASSET__assets_images_systemstar_png);
		type.set ("assets/images/systemStar.png", AssetType.IMAGE);
		className.set ("assets/images/systemStarAnimation.png", __ASSET__assets_images_systemstaranimation_png);
		type.set ("assets/images/systemStarAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/tileAnimation.png", __ASSET__assets_images_tileanimation_png);
		type.set ("assets/images/tileAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/tileAnimationTEST.png", __ASSET__assets_images_tileanimationtest_png);
		type.set ("assets/images/tileAnimationTEST.png", AssetType.IMAGE);
		className.set ("assets/images/tileSprite.png", __ASSET__assets_images_tilesprite_png);
		type.set ("assets/images/tileSprite.png", AssetType.IMAGE);
		className.set ("assets/images/titleBackground.png", __ASSET__assets_images_titlebackground_png);
		type.set ("assets/images/titleBackground.png", AssetType.IMAGE);
		className.set ("assets/images/titleEasy.png", __ASSET__assets_images_titleeasy_png);
		type.set ("assets/images/titleEasy.png", AssetType.IMAGE);
		className.set ("assets/images/titleImage.png", __ASSET__assets_images_titleimage_png);
		type.set ("assets/images/titleImage.png", AssetType.IMAGE);
		className.set ("assets/images/titleImageProcessed.png", __ASSET__assets_images_titleimageprocessed_png);
		type.set ("assets/images/titleImageProcessed.png", AssetType.IMAGE);
		className.set ("assets/images/titleScreenGrass.png", __ASSET__assets_images_titlescreengrass_png);
		type.set ("assets/images/titleScreenGrass.png", AssetType.IMAGE);
		className.set ("assets/images/treeAnim0.png", __ASSET__assets_images_treeanim0_png);
		type.set ("assets/images/treeAnim0.png", AssetType.IMAGE);
		className.set ("assets/images/treeAnim1.png", __ASSET__assets_images_treeanim1_png);
		type.set ("assets/images/treeAnim1.png", AssetType.IMAGE);
		className.set ("assets/images/treeAnim2.png", __ASSET__assets_images_treeanim2_png);
		type.set ("assets/images/treeAnim2.png", AssetType.IMAGE);
		className.set ("assets/images/treeAnim3.png", __ASSET__assets_images_treeanim3_png);
		type.set ("assets/images/treeAnim3.png", AssetType.IMAGE);
		className.set ("assets/images/treeAnim4.png", __ASSET__assets_images_treeanim4_png);
		type.set ("assets/images/treeAnim4.png", AssetType.IMAGE);
		className.set ("assets/images/treeAnim5.png", __ASSET__assets_images_treeanim5_png);
		type.set ("assets/images/treeAnim5.png", AssetType.IMAGE);
		className.set ("assets/images/treeAnim6.png", __ASSET__assets_images_treeanim6_png);
		type.set ("assets/images/treeAnim6.png", AssetType.IMAGE);
		className.set ("assets/images/treeAnim7.png", __ASSET__assets_images_treeanim7_png);
		type.set ("assets/images/treeAnim7.png", AssetType.IMAGE);
		className.set ("assets/images/treeAnimation.png", __ASSET__assets_images_treeanimation_png);
		type.set ("assets/images/treeAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/treeAnimationA.png", __ASSET__assets_images_treeanimationa_png);
		type.set ("assets/images/treeAnimationA.png", AssetType.IMAGE);
		className.set ("assets/images/treeAnimationB.png", __ASSET__assets_images_treeanimationb_png);
		type.set ("assets/images/treeAnimationB.png", AssetType.IMAGE);
		className.set ("assets/images/treeDeathAnimation.png", __ASSET__assets_images_treedeathanimation_png);
		type.set ("assets/images/treeDeathAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/treeDeathAnimation97.png", __ASSET__assets_images_treedeathanimation97_png);
		type.set ("assets/images/treeDeathAnimation97.png", AssetType.IMAGE);
		className.set ("assets/images/treeDeathAnimation98.png", __ASSET__assets_images_treedeathanimation98_png);
		type.set ("assets/images/treeDeathAnimation98.png", AssetType.IMAGE);
		className.set ("assets/images/treeDeathAnimation99.png", __ASSET__assets_images_treedeathanimation99_png);
		type.set ("assets/images/treeDeathAnimation99.png", AssetType.IMAGE);
		className.set ("assets/images/treeSpriteA.png", __ASSET__assets_images_treespritea_png);
		type.set ("assets/images/treeSpriteA.png", AssetType.IMAGE);
		className.set ("assets/images/treeSpriteA99.png", __ASSET__assets_images_treespritea99_png);
		type.set ("assets/images/treeSpriteA99.png", AssetType.IMAGE);
		className.set ("assets/images/tree_seed.png", __ASSET__assets_images_tree_seed_png);
		type.set ("assets/images/tree_seed.png", AssetType.IMAGE);
		className.set ("assets/images/truckBellAnimation.png", __ASSET__assets_images_truckbellanimation_png);
		type.set ("assets/images/truckBellAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/truckBellImage.png", __ASSET__assets_images_truckbellimage_png);
		type.set ("assets/images/truckBellImage.png", AssetType.IMAGE);
		className.set ("assets/images/tutCheckBoxAnimate.png", __ASSET__assets_images_tutcheckboxanimate_png);
		type.set ("assets/images/tutCheckBoxAnimate.png", AssetType.IMAGE);
		className.set ("assets/images/tutHand.png", __ASSET__assets_images_tuthand_png);
		type.set ("assets/images/tutHand.png", AssetType.IMAGE);
		className.set ("assets/images/tutHandAnimation.png", __ASSET__assets_images_tuthandanimation_png);
		type.set ("assets/images/tutHandAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/tutMessageEight.png", __ASSET__assets_images_tutmessageeight_png);
		type.set ("assets/images/tutMessageEight.png", AssetType.IMAGE);
		className.set ("assets/images/tutMessageFive.png", __ASSET__assets_images_tutmessagefive_png);
		type.set ("assets/images/tutMessageFive.png", AssetType.IMAGE);
		className.set ("assets/images/tutMessageFour.png", __ASSET__assets_images_tutmessagefour_png);
		type.set ("assets/images/tutMessageFour.png", AssetType.IMAGE);
		className.set ("assets/images/tutMessageNine.png", __ASSET__assets_images_tutmessagenine_png);
		type.set ("assets/images/tutMessageNine.png", AssetType.IMAGE);
		className.set ("assets/images/tutMessageOne.png", __ASSET__assets_images_tutmessageone_png);
		type.set ("assets/images/tutMessageOne.png", AssetType.IMAGE);
		className.set ("assets/images/tutMessageSeven.png", __ASSET__assets_images_tutmessageseven_png);
		type.set ("assets/images/tutMessageSeven.png", AssetType.IMAGE);
		className.set ("assets/images/tutMessageSix.png", __ASSET__assets_images_tutmessagesix_png);
		type.set ("assets/images/tutMessageSix.png", AssetType.IMAGE);
		className.set ("assets/images/tutMessageTen.png", __ASSET__assets_images_tutmessageten_png);
		type.set ("assets/images/tutMessageTen.png", AssetType.IMAGE);
		className.set ("assets/images/tutMessageThree.png", __ASSET__assets_images_tutmessagethree_png);
		type.set ("assets/images/tutMessageThree.png", AssetType.IMAGE);
		className.set ("assets/images/tutMessageTwo.png", __ASSET__assets_images_tutmessagetwo_png);
		type.set ("assets/images/tutMessageTwo.png", AssetType.IMAGE);
		className.set ("assets/images/upsideDownIsland.png", __ASSET__assets_images_upsidedownisland_png);
		type.set ("assets/images/upsideDownIsland.png", AssetType.IMAGE);
		className.set ("assets/images/waterfallAnimation.png", __ASSET__assets_images_waterfallanimation_png);
		type.set ("assets/images/waterfallAnimation.png", AssetType.IMAGE);
		className.set ("assets/images/waterfallImage.png", __ASSET__assets_images_waterfallimage_png);
		type.set ("assets/images/waterfallImage.png", AssetType.IMAGE);
		className.set ("assets/images/waterFallTile.png", __ASSET__assets_images_waterfalltile_png);
		type.set ("assets/images/waterFallTile.png", AssetType.IMAGE);
		className.set ("assets/images/weedA.png", __ASSET__assets_images_weeda_png);
		type.set ("assets/images/weedA.png", AssetType.IMAGE);
		className.set ("assets/images/weedB.png", __ASSET__assets_images_weedb_png);
		type.set ("assets/images/weedB.png", AssetType.IMAGE);
		className.set ("assets/music/BeppleSongONe.wav", __ASSET__assets_music_bepplesongone_wav);
		type.set ("assets/music/BeppleSongONe.wav", AssetType.SOUND);
		className.set ("assets/music/BeppleSongOneOGG.ogg", __ASSET__assets_music_bepplesongoneogg_ogg);
		type.set ("assets/music/BeppleSongOneOGG.ogg", AssetType.SOUND);
		className.set ("assets/music/music-goes-here.txt", __ASSET__assets_music_music_goes_here_txt);
		type.set ("assets/music/music-goes-here.txt", AssetType.TEXT);
		className.set ("assets/sounds/alertButtonSound.wav", __ASSET__assets_sounds_alertbuttonsound_wav);
		type.set ("assets/sounds/alertButtonSound.wav", AssetType.SOUND);
		className.set ("assets/sounds/buySignSound.wav", __ASSET__assets_sounds_buysignsound_wav);
		type.set ("assets/sounds/buySignSound.wav", AssetType.SOUND);
		className.set ("assets/sounds/calcButtonSound.wav", __ASSET__assets_sounds_calcbuttonsound_wav);
		type.set ("assets/sounds/calcButtonSound.wav", AssetType.SOUND);
		className.set ("assets/sounds/cashRegisterSound.wav", __ASSET__assets_sounds_cashregistersound_wav);
		type.set ("assets/sounds/cashRegisterSound.wav", AssetType.SOUND);
		className.set ("assets/sounds/coinsSound.wav", __ASSET__assets_sounds_coinssound_wav);
		type.set ("assets/sounds/coinsSound.wav", AssetType.SOUND);
		className.set ("assets/sounds/correctAnswerSoundA.wav", __ASSET__assets_sounds_correctanswersounda_wav);
		type.set ("assets/sounds/correctAnswerSoundA.wav", AssetType.SOUND);
		className.set ("assets/sounds/fallenAppleSound.wav", __ASSET__assets_sounds_fallenapplesound_wav);
		type.set ("assets/sounds/fallenAppleSound.wav", AssetType.SOUND);
		className.set ("assets/sounds/fillingJuiceBottleSound.wav", __ASSET__assets_sounds_fillingjuicebottlesound_wav);
		type.set ("assets/sounds/fillingJuiceBottleSound.wav", AssetType.SOUND);
		className.set ("assets/sounds/greenHouseWindChimeSound.wav", __ASSET__assets_sounds_greenhousewindchimesound_wav);
		type.set ("assets/sounds/greenHouseWindChimeSound.wav", AssetType.SOUND);
		className.set ("assets/sounds/growingTree.wav", __ASSET__assets_sounds_growingtree_wav);
		type.set ("assets/sounds/growingTree.wav", AssetType.SOUND);
		className.set ("assets/sounds/growingTreeA.wav", __ASSET__assets_sounds_growingtreea_wav);
		type.set ("assets/sounds/growingTreeA.wav", AssetType.SOUND);
		className.set ("assets/sounds/growingTreeB.wav", __ASSET__assets_sounds_growingtreeb_wav);
		type.set ("assets/sounds/growingTreeB.wav", AssetType.SOUND);
		className.set ("assets/sounds/growingTreeC.wav", __ASSET__assets_sounds_growingtreec_wav);
		type.set ("assets/sounds/growingTreeC.wav", AssetType.SOUND);
		className.set ("assets/sounds/growingTreeD.wav", __ASSET__assets_sounds_growingtreed_wav);
		type.set ("assets/sounds/growingTreeD.wav", AssetType.SOUND);
		className.set ("assets/sounds/incorrectAnswerSound.wav", __ASSET__assets_sounds_incorrectanswersound_wav);
		type.set ("assets/sounds/incorrectAnswerSound.wav", AssetType.SOUND);
		className.set ("assets/sounds/juiceMachineSound.wav", __ASSET__assets_sounds_juicemachinesound_wav);
		type.set ("assets/sounds/juiceMachineSound.wav", AssetType.SOUND);
		className.set ("assets/sounds/koiClicked0.wav", __ASSET__assets_sounds_koiclicked0_wav);
		type.set ("assets/sounds/koiClicked0.wav", AssetType.SOUND);
		className.set ("assets/sounds/koiClicked1.wav", __ASSET__assets_sounds_koiclicked1_wav);
		type.set ("assets/sounds/koiClicked1.wav", AssetType.SOUND);
		className.set ("assets/sounds/koiClicked2.wav", __ASSET__assets_sounds_koiclicked2_wav);
		type.set ("assets/sounds/koiClicked2.wav", AssetType.SOUND);
		className.set ("assets/sounds/koiClicked4.wav", __ASSET__assets_sounds_koiclicked4_wav);
		type.set ("assets/sounds/koiClicked4.wav", AssetType.SOUND);
		className.set ("assets/sounds/koiPondPlacedSound.wav", __ASSET__assets_sounds_koipondplacedsound_wav);
		type.set ("assets/sounds/koiPondPlacedSound.wav", AssetType.SOUND);
		className.set ("assets/sounds/koiPondSound.wav", __ASSET__assets_sounds_koipondsound_wav);
		type.set ("assets/sounds/koiPondSound.wav", AssetType.SOUND);
		className.set ("assets/sounds/marketSlideClosedSound.wav", __ASSET__assets_sounds_marketslideclosedsound_wav);
		type.set ("assets/sounds/marketSlideClosedSound.wav", AssetType.SOUND);
		className.set ("assets/sounds/marketSlideOpenSound.wav", __ASSET__assets_sounds_marketslideopensound_wav);
		type.set ("assets/sounds/marketSlideOpenSound.wav", AssetType.SOUND);
		className.set ("assets/sounds/mouseHoleSound.wav", __ASSET__assets_sounds_mouseholesound_wav);
		type.set ("assets/sounds/mouseHoleSound.wav", AssetType.SOUND);
		className.set ("assets/sounds/mouseWalkingSound.wav", __ASSET__assets_sounds_mousewalkingsound_wav);
		type.set ("assets/sounds/mouseWalkingSound.wav", AssetType.SOUND);
		className.set ("assets/sounds/pickedApplesGround.wav", __ASSET__assets_sounds_pickedapplesground_wav);
		type.set ("assets/sounds/pickedApplesGround.wav", AssetType.SOUND);
		className.set ("assets/sounds/pickedLeavesSound.wav", __ASSET__assets_sounds_pickedleavessound_wav);
		type.set ("assets/sounds/pickedLeavesSound.wav", AssetType.SOUND);
		className.set ("assets/sounds/pieBakedSound.wav", __ASSET__assets_sounds_piebakedsound_wav);
		type.set ("assets/sounds/pieBakedSound.wav", AssetType.SOUND);
		className.set ("assets/sounds/placedMachineSound.wav", __ASSET__assets_sounds_placedmachinesound_wav);
		type.set ("assets/sounds/placedMachineSound.wav", AssetType.SOUND);
		className.set ("assets/sounds/plantingSeedSound.wav", __ASSET__assets_sounds_plantingseedsound_wav);
		type.set ("assets/sounds/plantingSeedSound.wav", AssetType.SOUND);
		className.set ("assets/sounds/pouringJuiceSound.wav", __ASSET__assets_sounds_pouringjuicesound_wav);
		type.set ("assets/sounds/pouringJuiceSound.wav", AssetType.SOUND);
		className.set ("assets/sounds/raining.wav", __ASSET__assets_sounds_raining_wav);
		type.set ("assets/sounds/raining.wav", AssetType.SOUND);
		className.set ("assets/sounds/sounds-go-here.txt", __ASSET__assets_sounds_sounds_go_here_txt);
		type.set ("assets/sounds/sounds-go-here.txt", AssetType.TEXT);
		className.set ("assets/sounds/sprinklerButtonSound.wav", __ASSET__assets_sounds_sprinklerbuttonsound_wav);
		type.set ("assets/sounds/sprinklerButtonSound.wav", AssetType.SOUND);
		className.set ("assets/sounds/sprinklerSound.wav", __ASSET__assets_sounds_sprinklersound_wav);
		type.set ("assets/sounds/sprinklerSound.wav", AssetType.SOUND);
		className.set ("assets/sounds/sunshine.wav", __ASSET__assets_sounds_sunshine_wav);
		type.set ("assets/sounds/sunshine.wav", AssetType.SOUND);
		className.set ("assets/sounds/truckButtonSound.wav", __ASSET__assets_sounds_truckbuttonsound_wav);
		type.set ("assets/sounds/truckButtonSound.wav", AssetType.SOUND);
		className.set ("assets/sounds/truckSound.wav", __ASSET__assets_sounds_trucksound_wav);
		type.set ("assets/sounds/truckSound.wav", AssetType.SOUND);
		className.set ("assets/sounds/beep.mp3", __ASSET__assets_sounds_beep_mp3);
		type.set ("assets/sounds/beep.mp3", AssetType.MUSIC);
		className.set ("assets/sounds/flixel.mp3", __ASSET__assets_sounds_flixel_mp3);
		type.set ("assets/sounds/flixel.mp3", AssetType.MUSIC);
		
		
		#elseif html5
		
		var id;
		id = "assets/data/data-goes-here.txt";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/images/animatedAppleA.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/animatedAppleB.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/answerButton.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/appleJuiceItemImage.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/appleRotAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/appleShedAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/appleTruck.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/appleTruckButtonOnAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassA.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassAmaster.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassB.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassBmaster.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassC.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassCmaster.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassD.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassDmaster.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassE.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassEmaster.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassF.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassFmaster.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassFMouse.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassG.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassGmaster.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassH.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassHmaster.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/basketAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/blackBoard.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/blackBoardCorrectBorder.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/blackBoardWrongBorder.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/boilerImage.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/bridgeImage.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buildBar.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buildBarShaft.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buildBarTab.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buttonClear.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buttonEight.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buttonFive.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buttonFour.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buttonNine.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buttonOne.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buttonSeven.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buttonSix.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buttonThree.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buttonTwo.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buttonZero.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buttonZeroShadow.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buyLandButton.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/calcImage.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/chalkEquals.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/chalkNumbers_0.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/chalkNumbers_1.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/chalkNumbers_2.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/chalkNumbers_3.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/chalkNumbers_4.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/chalkNumbers_5.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/chalkNumbers_6.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/chalkNumbers_7.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/chalkNumbers_8.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/chalkNumbers_9.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/cloud_blue.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/coyPondImage.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/dropletsAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/dropletsAnimationS.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/dropletsAnimationT.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/dropletsAnimationU.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/fillingBottleAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/fishIcon.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/fishItemAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/gameTile.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/gameTileBoarder.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/gameTileOwned.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/goldCoin.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/greenHouseAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/greenHouseIconImage.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/GreenHouseItemAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/greenHouseShrub.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/greenHouseTree.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/greenHouseTreeAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/holeAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/images-go-here.txt";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/images/islandMouseAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/juiceMachineAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/juiceMachineIcon.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/juiceMachineSwitchAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/koiFishAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/koiFishItemDrag.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/koiPad.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/koiPondAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/koiPondImage.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/koiRock.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/koiSand.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/marketSideBar.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/marketSideBarShaft.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/marketSideBarTab.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/market_seed.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/meteorField.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/meteorFieldMaster.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/minusOneImage.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/minusOperation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/mouseAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/multiOperation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/optionsScreen.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/packOfJuiceIcon.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/packOfPiesIcon.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/pickedAppleLeaves.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/pieIcon.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/pieMachineAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/pieMachineItem.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/plusOperation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/rainCloudAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/rainCloudAnimationA.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/repairAnimationS.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/rockyForeground.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/rollingMoon.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/seedItemAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/sidewaysIsland.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/spaceBackground.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/sprinklerAlertAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/sprinklerAlertAnimationS.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/sprinklerAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/sprinklerSwtichAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/sunAnimationA.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/sunAnimationB.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/sunAnimationC.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/sunTokenAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/systemStar.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/systemStarAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tileAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tileAnimationTEST.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tileSprite.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/titleBackground.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/titleEasy.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/titleImage.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/titleImageProcessed.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/titleScreenGrass.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeAnim0.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeAnim1.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeAnim2.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeAnim3.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeAnim4.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeAnim5.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeAnim6.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeAnim7.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeAnimationA.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeAnimationB.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeDeathAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeDeathAnimation97.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeDeathAnimation98.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeDeathAnimation99.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeSpriteA.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeSpriteA99.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tree_seed.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/truckBellAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/truckBellImage.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tutCheckBoxAnimate.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tutHand.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tutHandAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tutMessageEight.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tutMessageFive.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tutMessageFour.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tutMessageNine.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tutMessageOne.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tutMessageSeven.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tutMessageSix.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tutMessageTen.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tutMessageThree.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tutMessageTwo.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/upsideDownIsland.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/waterfallAnimation.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/waterfallImage.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/waterFallTile.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/weedA.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/images/weedB.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/music/BeppleSongONe.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/music/BeppleSongOneOGG.ogg";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/music/music-goes-here.txt";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/sounds/alertButtonSound.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/buySignSound.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/calcButtonSound.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/cashRegisterSound.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/coinsSound.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/correctAnswerSoundA.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/fallenAppleSound.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/fillingJuiceBottleSound.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/greenHouseWindChimeSound.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/growingTree.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/growingTreeA.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/growingTreeB.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/growingTreeC.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/growingTreeD.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/incorrectAnswerSound.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/juiceMachineSound.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/koiClicked0.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/koiClicked1.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/koiClicked2.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/koiClicked4.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/koiPondPlacedSound.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/koiPondSound.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/marketSlideClosedSound.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/marketSlideOpenSound.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/mouseHoleSound.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/mouseWalkingSound.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/pickedApplesGround.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/pickedLeavesSound.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/pieBakedSound.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/placedMachineSound.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/plantingSeedSound.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/pouringJuiceSound.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/raining.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/sounds-go-here.txt";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/sounds/sprinklerButtonSound.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/sprinklerSound.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/sunshine.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/truckButtonSound.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/truckSound.wav";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/beep.mp3";
		path.set (id, id);
		
		type.set (id, AssetType.MUSIC);
		id = "assets/sounds/flixel.mp3";
		path.set (id, id);
		
		type.set (id, AssetType.MUSIC);
		
		
		#else
		
		#if openfl
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		#end
		
		#if (windows || mac || linux)
		
		var useManifest = false;
		
		className.set ("assets/data/data-goes-here.txt", __ASSET__assets_data_data_goes_here_txt);
		type.set ("assets/data/data-goes-here.txt", AssetType.TEXT);
		
		className.set ("assets/images/animatedAppleA.png", __ASSET__assets_images_animatedapplea_png);
		type.set ("assets/images/animatedAppleA.png", AssetType.IMAGE);
		
		className.set ("assets/images/animatedAppleB.png", __ASSET__assets_images_animatedappleb_png);
		type.set ("assets/images/animatedAppleB.png", AssetType.IMAGE);
		
		className.set ("assets/images/answerButton.png", __ASSET__assets_images_answerbutton_png);
		type.set ("assets/images/answerButton.png", AssetType.IMAGE);
		
		className.set ("assets/images/appleJuiceItemImage.png", __ASSET__assets_images_applejuiceitemimage_png);
		type.set ("assets/images/appleJuiceItemImage.png", AssetType.IMAGE);
		
		className.set ("assets/images/appleRotAnimation.png", __ASSET__assets_images_applerotanimation_png);
		type.set ("assets/images/appleRotAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/appleShedAnimation.png", __ASSET__assets_images_appleshedanimation_png);
		type.set ("assets/images/appleShedAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/appleTruck.png", __ASSET__assets_images_appletruck_png);
		type.set ("assets/images/appleTruck.png", AssetType.IMAGE);
		
		className.set ("assets/images/appleTruckButtonOnAnimation.png", __ASSET__assets_images_appletruckbuttononanimation_png);
		type.set ("assets/images/appleTruckButtonOnAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/backGroundMassA.png", __ASSET__assets_images_backgroundmassa_png);
		type.set ("assets/images/backGroundMassA.png", AssetType.IMAGE);
		
		className.set ("assets/images/backGroundMassAmaster.png", __ASSET__assets_images_backgroundmassamaster_png);
		type.set ("assets/images/backGroundMassAmaster.png", AssetType.IMAGE);
		
		className.set ("assets/images/backGroundMassB.png", __ASSET__assets_images_backgroundmassb_png);
		type.set ("assets/images/backGroundMassB.png", AssetType.IMAGE);
		
		className.set ("assets/images/backGroundMassBmaster.png", __ASSET__assets_images_backgroundmassbmaster_png);
		type.set ("assets/images/backGroundMassBmaster.png", AssetType.IMAGE);
		
		className.set ("assets/images/backGroundMassC.png", __ASSET__assets_images_backgroundmassc_png);
		type.set ("assets/images/backGroundMassC.png", AssetType.IMAGE);
		
		className.set ("assets/images/backGroundMassCmaster.png", __ASSET__assets_images_backgroundmasscmaster_png);
		type.set ("assets/images/backGroundMassCmaster.png", AssetType.IMAGE);
		
		className.set ("assets/images/backGroundMassD.png", __ASSET__assets_images_backgroundmassd_png);
		type.set ("assets/images/backGroundMassD.png", AssetType.IMAGE);
		
		className.set ("assets/images/backGroundMassDmaster.png", __ASSET__assets_images_backgroundmassdmaster_png);
		type.set ("assets/images/backGroundMassDmaster.png", AssetType.IMAGE);
		
		className.set ("assets/images/backGroundMassE.png", __ASSET__assets_images_backgroundmasse_png);
		type.set ("assets/images/backGroundMassE.png", AssetType.IMAGE);
		
		className.set ("assets/images/backGroundMassEmaster.png", __ASSET__assets_images_backgroundmassemaster_png);
		type.set ("assets/images/backGroundMassEmaster.png", AssetType.IMAGE);
		
		className.set ("assets/images/backGroundMassF.png", __ASSET__assets_images_backgroundmassf_png);
		type.set ("assets/images/backGroundMassF.png", AssetType.IMAGE);
		
		className.set ("assets/images/backGroundMassFmaster.png", __ASSET__assets_images_backgroundmassfmaster_png);
		type.set ("assets/images/backGroundMassFmaster.png", AssetType.IMAGE);
		
		className.set ("assets/images/backGroundMassFMouse.png", __ASSET__assets_images_backgroundmassfmouse_png);
		type.set ("assets/images/backGroundMassFMouse.png", AssetType.IMAGE);
		
		className.set ("assets/images/backGroundMassG.png", __ASSET__assets_images_backgroundmassg_png);
		type.set ("assets/images/backGroundMassG.png", AssetType.IMAGE);
		
		className.set ("assets/images/backGroundMassGmaster.png", __ASSET__assets_images_backgroundmassgmaster_png);
		type.set ("assets/images/backGroundMassGmaster.png", AssetType.IMAGE);
		
		className.set ("assets/images/backGroundMassH.png", __ASSET__assets_images_backgroundmassh_png);
		type.set ("assets/images/backGroundMassH.png", AssetType.IMAGE);
		
		className.set ("assets/images/backGroundMassHmaster.png", __ASSET__assets_images_backgroundmasshmaster_png);
		type.set ("assets/images/backGroundMassHmaster.png", AssetType.IMAGE);
		
		className.set ("assets/images/basketAnimation.png", __ASSET__assets_images_basketanimation_png);
		type.set ("assets/images/basketAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/blackBoard.png", __ASSET__assets_images_blackboard_png);
		type.set ("assets/images/blackBoard.png", AssetType.IMAGE);
		
		className.set ("assets/images/blackBoardCorrectBorder.png", __ASSET__assets_images_blackboardcorrectborder_png);
		type.set ("assets/images/blackBoardCorrectBorder.png", AssetType.IMAGE);
		
		className.set ("assets/images/blackBoardWrongBorder.png", __ASSET__assets_images_blackboardwrongborder_png);
		type.set ("assets/images/blackBoardWrongBorder.png", AssetType.IMAGE);
		
		className.set ("assets/images/boilerImage.png", __ASSET__assets_images_boilerimage_png);
		type.set ("assets/images/boilerImage.png", AssetType.IMAGE);
		
		className.set ("assets/images/bridgeImage.png", __ASSET__assets_images_bridgeimage_png);
		type.set ("assets/images/bridgeImage.png", AssetType.IMAGE);
		
		className.set ("assets/images/buildBar.png", __ASSET__assets_images_buildbar_png);
		type.set ("assets/images/buildBar.png", AssetType.IMAGE);
		
		className.set ("assets/images/buildBarShaft.png", __ASSET__assets_images_buildbarshaft_png);
		type.set ("assets/images/buildBarShaft.png", AssetType.IMAGE);
		
		className.set ("assets/images/buildBarTab.png", __ASSET__assets_images_buildbartab_png);
		type.set ("assets/images/buildBarTab.png", AssetType.IMAGE);
		
		className.set ("assets/images/buttonClear.png", __ASSET__assets_images_buttonclear_png);
		type.set ("assets/images/buttonClear.png", AssetType.IMAGE);
		
		className.set ("assets/images/buttonEight.png", __ASSET__assets_images_buttoneight_png);
		type.set ("assets/images/buttonEight.png", AssetType.IMAGE);
		
		className.set ("assets/images/buttonFive.png", __ASSET__assets_images_buttonfive_png);
		type.set ("assets/images/buttonFive.png", AssetType.IMAGE);
		
		className.set ("assets/images/buttonFour.png", __ASSET__assets_images_buttonfour_png);
		type.set ("assets/images/buttonFour.png", AssetType.IMAGE);
		
		className.set ("assets/images/buttonNine.png", __ASSET__assets_images_buttonnine_png);
		type.set ("assets/images/buttonNine.png", AssetType.IMAGE);
		
		className.set ("assets/images/buttonOne.png", __ASSET__assets_images_buttonone_png);
		type.set ("assets/images/buttonOne.png", AssetType.IMAGE);
		
		className.set ("assets/images/buttonSeven.png", __ASSET__assets_images_buttonseven_png);
		type.set ("assets/images/buttonSeven.png", AssetType.IMAGE);
		
		className.set ("assets/images/buttonSix.png", __ASSET__assets_images_buttonsix_png);
		type.set ("assets/images/buttonSix.png", AssetType.IMAGE);
		
		className.set ("assets/images/buttonThree.png", __ASSET__assets_images_buttonthree_png);
		type.set ("assets/images/buttonThree.png", AssetType.IMAGE);
		
		className.set ("assets/images/buttonTwo.png", __ASSET__assets_images_buttontwo_png);
		type.set ("assets/images/buttonTwo.png", AssetType.IMAGE);
		
		className.set ("assets/images/buttonZero.png", __ASSET__assets_images_buttonzero_png);
		type.set ("assets/images/buttonZero.png", AssetType.IMAGE);
		
		className.set ("assets/images/buttonZeroShadow.png", __ASSET__assets_images_buttonzeroshadow_png);
		type.set ("assets/images/buttonZeroShadow.png", AssetType.IMAGE);
		
		className.set ("assets/images/buyLandButton.png", __ASSET__assets_images_buylandbutton_png);
		type.set ("assets/images/buyLandButton.png", AssetType.IMAGE);
		
		className.set ("assets/images/calcImage.png", __ASSET__assets_images_calcimage_png);
		type.set ("assets/images/calcImage.png", AssetType.IMAGE);
		
		className.set ("assets/images/chalkEquals.png", __ASSET__assets_images_chalkequals_png);
		type.set ("assets/images/chalkEquals.png", AssetType.IMAGE);
		
		className.set ("assets/images/chalkNumbers_0.png", __ASSET__assets_images_chalknumbers_0_png);
		type.set ("assets/images/chalkNumbers_0.png", AssetType.IMAGE);
		
		className.set ("assets/images/chalkNumbers_1.png", __ASSET__assets_images_chalknumbers_1_png);
		type.set ("assets/images/chalkNumbers_1.png", AssetType.IMAGE);
		
		className.set ("assets/images/chalkNumbers_2.png", __ASSET__assets_images_chalknumbers_2_png);
		type.set ("assets/images/chalkNumbers_2.png", AssetType.IMAGE);
		
		className.set ("assets/images/chalkNumbers_3.png", __ASSET__assets_images_chalknumbers_3_png);
		type.set ("assets/images/chalkNumbers_3.png", AssetType.IMAGE);
		
		className.set ("assets/images/chalkNumbers_4.png", __ASSET__assets_images_chalknumbers_4_png);
		type.set ("assets/images/chalkNumbers_4.png", AssetType.IMAGE);
		
		className.set ("assets/images/chalkNumbers_5.png", __ASSET__assets_images_chalknumbers_5_png);
		type.set ("assets/images/chalkNumbers_5.png", AssetType.IMAGE);
		
		className.set ("assets/images/chalkNumbers_6.png", __ASSET__assets_images_chalknumbers_6_png);
		type.set ("assets/images/chalkNumbers_6.png", AssetType.IMAGE);
		
		className.set ("assets/images/chalkNumbers_7.png", __ASSET__assets_images_chalknumbers_7_png);
		type.set ("assets/images/chalkNumbers_7.png", AssetType.IMAGE);
		
		className.set ("assets/images/chalkNumbers_8.png", __ASSET__assets_images_chalknumbers_8_png);
		type.set ("assets/images/chalkNumbers_8.png", AssetType.IMAGE);
		
		className.set ("assets/images/chalkNumbers_9.png", __ASSET__assets_images_chalknumbers_9_png);
		type.set ("assets/images/chalkNumbers_9.png", AssetType.IMAGE);
		
		className.set ("assets/images/cloud_blue.png", __ASSET__assets_images_cloud_blue_png);
		type.set ("assets/images/cloud_blue.png", AssetType.IMAGE);
		
		className.set ("assets/images/coyPondImage.png", __ASSET__assets_images_coypondimage_png);
		type.set ("assets/images/coyPondImage.png", AssetType.IMAGE);
		
		className.set ("assets/images/dropletsAnimation.png", __ASSET__assets_images_dropletsanimation_png);
		type.set ("assets/images/dropletsAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/dropletsAnimationS.png", __ASSET__assets_images_dropletsanimations_png);
		type.set ("assets/images/dropletsAnimationS.png", AssetType.IMAGE);
		
		className.set ("assets/images/dropletsAnimationT.png", __ASSET__assets_images_dropletsanimationt_png);
		type.set ("assets/images/dropletsAnimationT.png", AssetType.IMAGE);
		
		className.set ("assets/images/dropletsAnimationU.png", __ASSET__assets_images_dropletsanimationu_png);
		type.set ("assets/images/dropletsAnimationU.png", AssetType.IMAGE);
		
		className.set ("assets/images/fillingBottleAnimation.png", __ASSET__assets_images_fillingbottleanimation_png);
		type.set ("assets/images/fillingBottleAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/fishIcon.png", __ASSET__assets_images_fishicon_png);
		type.set ("assets/images/fishIcon.png", AssetType.IMAGE);
		
		className.set ("assets/images/fishItemAnimation.png", __ASSET__assets_images_fishitemanimation_png);
		type.set ("assets/images/fishItemAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/gameTile.png", __ASSET__assets_images_gametile_png);
		type.set ("assets/images/gameTile.png", AssetType.IMAGE);
		
		className.set ("assets/images/gameTileBoarder.png", __ASSET__assets_images_gametileboarder_png);
		type.set ("assets/images/gameTileBoarder.png", AssetType.IMAGE);
		
		className.set ("assets/images/gameTileOwned.png", __ASSET__assets_images_gametileowned_png);
		type.set ("assets/images/gameTileOwned.png", AssetType.IMAGE);
		
		className.set ("assets/images/goldCoin.png", __ASSET__assets_images_goldcoin_png);
		type.set ("assets/images/goldCoin.png", AssetType.IMAGE);
		
		className.set ("assets/images/greenHouseAnimation.png", __ASSET__assets_images_greenhouseanimation_png);
		type.set ("assets/images/greenHouseAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/greenHouseIconImage.png", __ASSET__assets_images_greenhouseiconimage_png);
		type.set ("assets/images/greenHouseIconImage.png", AssetType.IMAGE);
		
		className.set ("assets/images/GreenHouseItemAnimation.png", __ASSET__assets_images_greenhouseitemanimation_png);
		type.set ("assets/images/GreenHouseItemAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/greenHouseShrub.png", __ASSET__assets_images_greenhouseshrub_png);
		type.set ("assets/images/greenHouseShrub.png", AssetType.IMAGE);
		
		className.set ("assets/images/greenHouseTree.png", __ASSET__assets_images_greenhousetree_png);
		type.set ("assets/images/greenHouseTree.png", AssetType.IMAGE);
		
		className.set ("assets/images/greenHouseTreeAnimation.png", __ASSET__assets_images_greenhousetreeanimation_png);
		type.set ("assets/images/greenHouseTreeAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/holeAnimation.png", __ASSET__assets_images_holeanimation_png);
		type.set ("assets/images/holeAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/images-go-here.txt", __ASSET__assets_images_images_go_here_txt);
		type.set ("assets/images/images-go-here.txt", AssetType.TEXT);
		
		className.set ("assets/images/islandMouseAnimation.png", __ASSET__assets_images_islandmouseanimation_png);
		type.set ("assets/images/islandMouseAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/juiceMachineAnimation.png", __ASSET__assets_images_juicemachineanimation_png);
		type.set ("assets/images/juiceMachineAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/juiceMachineIcon.png", __ASSET__assets_images_juicemachineicon_png);
		type.set ("assets/images/juiceMachineIcon.png", AssetType.IMAGE);
		
		className.set ("assets/images/juiceMachineSwitchAnimation.png", __ASSET__assets_images_juicemachineswitchanimation_png);
		type.set ("assets/images/juiceMachineSwitchAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/koiFishAnimation.png", __ASSET__assets_images_koifishanimation_png);
		type.set ("assets/images/koiFishAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/koiFishItemDrag.png", __ASSET__assets_images_koifishitemdrag_png);
		type.set ("assets/images/koiFishItemDrag.png", AssetType.IMAGE);
		
		className.set ("assets/images/koiPad.png", __ASSET__assets_images_koipad_png);
		type.set ("assets/images/koiPad.png", AssetType.IMAGE);
		
		className.set ("assets/images/koiPondAnimation.png", __ASSET__assets_images_koipondanimation_png);
		type.set ("assets/images/koiPondAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/koiPondImage.png", __ASSET__assets_images_koipondimage_png);
		type.set ("assets/images/koiPondImage.png", AssetType.IMAGE);
		
		className.set ("assets/images/koiRock.png", __ASSET__assets_images_koirock_png);
		type.set ("assets/images/koiRock.png", AssetType.IMAGE);
		
		className.set ("assets/images/koiSand.png", __ASSET__assets_images_koisand_png);
		type.set ("assets/images/koiSand.png", AssetType.IMAGE);
		
		className.set ("assets/images/marketSideBar.png", __ASSET__assets_images_marketsidebar_png);
		type.set ("assets/images/marketSideBar.png", AssetType.IMAGE);
		
		className.set ("assets/images/marketSideBarShaft.png", __ASSET__assets_images_marketsidebarshaft_png);
		type.set ("assets/images/marketSideBarShaft.png", AssetType.IMAGE);
		
		className.set ("assets/images/marketSideBarTab.png", __ASSET__assets_images_marketsidebartab_png);
		type.set ("assets/images/marketSideBarTab.png", AssetType.IMAGE);
		
		className.set ("assets/images/market_seed.png", __ASSET__assets_images_market_seed_png);
		type.set ("assets/images/market_seed.png", AssetType.IMAGE);
		
		className.set ("assets/images/meteorField.png", __ASSET__assets_images_meteorfield_png);
		type.set ("assets/images/meteorField.png", AssetType.IMAGE);
		
		className.set ("assets/images/meteorFieldMaster.png", __ASSET__assets_images_meteorfieldmaster_png);
		type.set ("assets/images/meteorFieldMaster.png", AssetType.IMAGE);
		
		className.set ("assets/images/minusOneImage.png", __ASSET__assets_images_minusoneimage_png);
		type.set ("assets/images/minusOneImage.png", AssetType.IMAGE);
		
		className.set ("assets/images/minusOperation.png", __ASSET__assets_images_minusoperation_png);
		type.set ("assets/images/minusOperation.png", AssetType.IMAGE);
		
		className.set ("assets/images/mouseAnimation.png", __ASSET__assets_images_mouseanimation_png);
		type.set ("assets/images/mouseAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/multiOperation.png", __ASSET__assets_images_multioperation_png);
		type.set ("assets/images/multiOperation.png", AssetType.IMAGE);
		
		className.set ("assets/images/optionsScreen.png", __ASSET__assets_images_optionsscreen_png);
		type.set ("assets/images/optionsScreen.png", AssetType.IMAGE);
		
		className.set ("assets/images/packOfJuiceIcon.png", __ASSET__assets_images_packofjuiceicon_png);
		type.set ("assets/images/packOfJuiceIcon.png", AssetType.IMAGE);
		
		className.set ("assets/images/packOfPiesIcon.png", __ASSET__assets_images_packofpiesicon_png);
		type.set ("assets/images/packOfPiesIcon.png", AssetType.IMAGE);
		
		className.set ("assets/images/pickedAppleLeaves.png", __ASSET__assets_images_pickedappleleaves_png);
		type.set ("assets/images/pickedAppleLeaves.png", AssetType.IMAGE);
		
		className.set ("assets/images/pieIcon.png", __ASSET__assets_images_pieicon_png);
		type.set ("assets/images/pieIcon.png", AssetType.IMAGE);
		
		className.set ("assets/images/pieMachineAnimation.png", __ASSET__assets_images_piemachineanimation_png);
		type.set ("assets/images/pieMachineAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/pieMachineItem.png", __ASSET__assets_images_piemachineitem_png);
		type.set ("assets/images/pieMachineItem.png", AssetType.IMAGE);
		
		className.set ("assets/images/plusOperation.png", __ASSET__assets_images_plusoperation_png);
		type.set ("assets/images/plusOperation.png", AssetType.IMAGE);
		
		className.set ("assets/images/rainCloudAnimation.png", __ASSET__assets_images_raincloudanimation_png);
		type.set ("assets/images/rainCloudAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/rainCloudAnimationA.png", __ASSET__assets_images_raincloudanimationa_png);
		type.set ("assets/images/rainCloudAnimationA.png", AssetType.IMAGE);
		
		className.set ("assets/images/repairAnimationS.png", __ASSET__assets_images_repairanimations_png);
		type.set ("assets/images/repairAnimationS.png", AssetType.IMAGE);
		
		className.set ("assets/images/rockyForeground.png", __ASSET__assets_images_rockyforeground_png);
		type.set ("assets/images/rockyForeground.png", AssetType.IMAGE);
		
		className.set ("assets/images/rollingMoon.png", __ASSET__assets_images_rollingmoon_png);
		type.set ("assets/images/rollingMoon.png", AssetType.IMAGE);
		
		className.set ("assets/images/seedItemAnimation.png", __ASSET__assets_images_seeditemanimation_png);
		type.set ("assets/images/seedItemAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/sidewaysIsland.png", __ASSET__assets_images_sidewaysisland_png);
		type.set ("assets/images/sidewaysIsland.png", AssetType.IMAGE);
		
		className.set ("assets/images/spaceBackground.png", __ASSET__assets_images_spacebackground_png);
		type.set ("assets/images/spaceBackground.png", AssetType.IMAGE);
		
		className.set ("assets/images/sprinklerAlertAnimation.png", __ASSET__assets_images_sprinkleralertanimation_png);
		type.set ("assets/images/sprinklerAlertAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/sprinklerAlertAnimationS.png", __ASSET__assets_images_sprinkleralertanimations_png);
		type.set ("assets/images/sprinklerAlertAnimationS.png", AssetType.IMAGE);
		
		className.set ("assets/images/sprinklerAnimation.png", __ASSET__assets_images_sprinkleranimation_png);
		type.set ("assets/images/sprinklerAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/sprinklerSwtichAnimation.png", __ASSET__assets_images_sprinklerswtichanimation_png);
		type.set ("assets/images/sprinklerSwtichAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/sunAnimationA.png", __ASSET__assets_images_sunanimationa_png);
		type.set ("assets/images/sunAnimationA.png", AssetType.IMAGE);
		
		className.set ("assets/images/sunAnimationB.png", __ASSET__assets_images_sunanimationb_png);
		type.set ("assets/images/sunAnimationB.png", AssetType.IMAGE);
		
		className.set ("assets/images/sunAnimationC.png", __ASSET__assets_images_sunanimationc_png);
		type.set ("assets/images/sunAnimationC.png", AssetType.IMAGE);
		
		className.set ("assets/images/sunTokenAnimation.png", __ASSET__assets_images_suntokenanimation_png);
		type.set ("assets/images/sunTokenAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/systemStar.png", __ASSET__assets_images_systemstar_png);
		type.set ("assets/images/systemStar.png", AssetType.IMAGE);
		
		className.set ("assets/images/systemStarAnimation.png", __ASSET__assets_images_systemstaranimation_png);
		type.set ("assets/images/systemStarAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/tileAnimation.png", __ASSET__assets_images_tileanimation_png);
		type.set ("assets/images/tileAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/tileAnimationTEST.png", __ASSET__assets_images_tileanimationtest_png);
		type.set ("assets/images/tileAnimationTEST.png", AssetType.IMAGE);
		
		className.set ("assets/images/tileSprite.png", __ASSET__assets_images_tilesprite_png);
		type.set ("assets/images/tileSprite.png", AssetType.IMAGE);
		
		className.set ("assets/images/titleBackground.png", __ASSET__assets_images_titlebackground_png);
		type.set ("assets/images/titleBackground.png", AssetType.IMAGE);
		
		className.set ("assets/images/titleEasy.png", __ASSET__assets_images_titleeasy_png);
		type.set ("assets/images/titleEasy.png", AssetType.IMAGE);
		
		className.set ("assets/images/titleImage.png", __ASSET__assets_images_titleimage_png);
		type.set ("assets/images/titleImage.png", AssetType.IMAGE);
		
		className.set ("assets/images/titleImageProcessed.png", __ASSET__assets_images_titleimageprocessed_png);
		type.set ("assets/images/titleImageProcessed.png", AssetType.IMAGE);
		
		className.set ("assets/images/titleScreenGrass.png", __ASSET__assets_images_titlescreengrass_png);
		type.set ("assets/images/titleScreenGrass.png", AssetType.IMAGE);
		
		className.set ("assets/images/treeAnim0.png", __ASSET__assets_images_treeanim0_png);
		type.set ("assets/images/treeAnim0.png", AssetType.IMAGE);
		
		className.set ("assets/images/treeAnim1.png", __ASSET__assets_images_treeanim1_png);
		type.set ("assets/images/treeAnim1.png", AssetType.IMAGE);
		
		className.set ("assets/images/treeAnim2.png", __ASSET__assets_images_treeanim2_png);
		type.set ("assets/images/treeAnim2.png", AssetType.IMAGE);
		
		className.set ("assets/images/treeAnim3.png", __ASSET__assets_images_treeanim3_png);
		type.set ("assets/images/treeAnim3.png", AssetType.IMAGE);
		
		className.set ("assets/images/treeAnim4.png", __ASSET__assets_images_treeanim4_png);
		type.set ("assets/images/treeAnim4.png", AssetType.IMAGE);
		
		className.set ("assets/images/treeAnim5.png", __ASSET__assets_images_treeanim5_png);
		type.set ("assets/images/treeAnim5.png", AssetType.IMAGE);
		
		className.set ("assets/images/treeAnim6.png", __ASSET__assets_images_treeanim6_png);
		type.set ("assets/images/treeAnim6.png", AssetType.IMAGE);
		
		className.set ("assets/images/treeAnim7.png", __ASSET__assets_images_treeanim7_png);
		type.set ("assets/images/treeAnim7.png", AssetType.IMAGE);
		
		className.set ("assets/images/treeAnimation.png", __ASSET__assets_images_treeanimation_png);
		type.set ("assets/images/treeAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/treeAnimationA.png", __ASSET__assets_images_treeanimationa_png);
		type.set ("assets/images/treeAnimationA.png", AssetType.IMAGE);
		
		className.set ("assets/images/treeAnimationB.png", __ASSET__assets_images_treeanimationb_png);
		type.set ("assets/images/treeAnimationB.png", AssetType.IMAGE);
		
		className.set ("assets/images/treeDeathAnimation.png", __ASSET__assets_images_treedeathanimation_png);
		type.set ("assets/images/treeDeathAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/treeDeathAnimation97.png", __ASSET__assets_images_treedeathanimation97_png);
		type.set ("assets/images/treeDeathAnimation97.png", AssetType.IMAGE);
		
		className.set ("assets/images/treeDeathAnimation98.png", __ASSET__assets_images_treedeathanimation98_png);
		type.set ("assets/images/treeDeathAnimation98.png", AssetType.IMAGE);
		
		className.set ("assets/images/treeDeathAnimation99.png", __ASSET__assets_images_treedeathanimation99_png);
		type.set ("assets/images/treeDeathAnimation99.png", AssetType.IMAGE);
		
		className.set ("assets/images/treeSpriteA.png", __ASSET__assets_images_treespritea_png);
		type.set ("assets/images/treeSpriteA.png", AssetType.IMAGE);
		
		className.set ("assets/images/treeSpriteA99.png", __ASSET__assets_images_treespritea99_png);
		type.set ("assets/images/treeSpriteA99.png", AssetType.IMAGE);
		
		className.set ("assets/images/tree_seed.png", __ASSET__assets_images_tree_seed_png);
		type.set ("assets/images/tree_seed.png", AssetType.IMAGE);
		
		className.set ("assets/images/truckBellAnimation.png", __ASSET__assets_images_truckbellanimation_png);
		type.set ("assets/images/truckBellAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/truckBellImage.png", __ASSET__assets_images_truckbellimage_png);
		type.set ("assets/images/truckBellImage.png", AssetType.IMAGE);
		
		className.set ("assets/images/tutCheckBoxAnimate.png", __ASSET__assets_images_tutcheckboxanimate_png);
		type.set ("assets/images/tutCheckBoxAnimate.png", AssetType.IMAGE);
		
		className.set ("assets/images/tutHand.png", __ASSET__assets_images_tuthand_png);
		type.set ("assets/images/tutHand.png", AssetType.IMAGE);
		
		className.set ("assets/images/tutHandAnimation.png", __ASSET__assets_images_tuthandanimation_png);
		type.set ("assets/images/tutHandAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/tutMessageEight.png", __ASSET__assets_images_tutmessageeight_png);
		type.set ("assets/images/tutMessageEight.png", AssetType.IMAGE);
		
		className.set ("assets/images/tutMessageFive.png", __ASSET__assets_images_tutmessagefive_png);
		type.set ("assets/images/tutMessageFive.png", AssetType.IMAGE);
		
		className.set ("assets/images/tutMessageFour.png", __ASSET__assets_images_tutmessagefour_png);
		type.set ("assets/images/tutMessageFour.png", AssetType.IMAGE);
		
		className.set ("assets/images/tutMessageNine.png", __ASSET__assets_images_tutmessagenine_png);
		type.set ("assets/images/tutMessageNine.png", AssetType.IMAGE);
		
		className.set ("assets/images/tutMessageOne.png", __ASSET__assets_images_tutmessageone_png);
		type.set ("assets/images/tutMessageOne.png", AssetType.IMAGE);
		
		className.set ("assets/images/tutMessageSeven.png", __ASSET__assets_images_tutmessageseven_png);
		type.set ("assets/images/tutMessageSeven.png", AssetType.IMAGE);
		
		className.set ("assets/images/tutMessageSix.png", __ASSET__assets_images_tutmessagesix_png);
		type.set ("assets/images/tutMessageSix.png", AssetType.IMAGE);
		
		className.set ("assets/images/tutMessageTen.png", __ASSET__assets_images_tutmessageten_png);
		type.set ("assets/images/tutMessageTen.png", AssetType.IMAGE);
		
		className.set ("assets/images/tutMessageThree.png", __ASSET__assets_images_tutmessagethree_png);
		type.set ("assets/images/tutMessageThree.png", AssetType.IMAGE);
		
		className.set ("assets/images/tutMessageTwo.png", __ASSET__assets_images_tutmessagetwo_png);
		type.set ("assets/images/tutMessageTwo.png", AssetType.IMAGE);
		
		className.set ("assets/images/upsideDownIsland.png", __ASSET__assets_images_upsidedownisland_png);
		type.set ("assets/images/upsideDownIsland.png", AssetType.IMAGE);
		
		className.set ("assets/images/waterfallAnimation.png", __ASSET__assets_images_waterfallanimation_png);
		type.set ("assets/images/waterfallAnimation.png", AssetType.IMAGE);
		
		className.set ("assets/images/waterfallImage.png", __ASSET__assets_images_waterfallimage_png);
		type.set ("assets/images/waterfallImage.png", AssetType.IMAGE);
		
		className.set ("assets/images/waterFallTile.png", __ASSET__assets_images_waterfalltile_png);
		type.set ("assets/images/waterFallTile.png", AssetType.IMAGE);
		
		className.set ("assets/images/weedA.png", __ASSET__assets_images_weeda_png);
		type.set ("assets/images/weedA.png", AssetType.IMAGE);
		
		className.set ("assets/images/weedB.png", __ASSET__assets_images_weedb_png);
		type.set ("assets/images/weedB.png", AssetType.IMAGE);
		
		className.set ("assets/music/BeppleSongONe.wav", __ASSET__assets_music_bepplesongone_wav);
		type.set ("assets/music/BeppleSongONe.wav", AssetType.SOUND);
		
		className.set ("assets/music/BeppleSongOneOGG.ogg", __ASSET__assets_music_bepplesongoneogg_ogg);
		type.set ("assets/music/BeppleSongOneOGG.ogg", AssetType.SOUND);
		
		className.set ("assets/music/music-goes-here.txt", __ASSET__assets_music_music_goes_here_txt);
		type.set ("assets/music/music-goes-here.txt", AssetType.TEXT);
		
		className.set ("assets/sounds/alertButtonSound.wav", __ASSET__assets_sounds_alertbuttonsound_wav);
		type.set ("assets/sounds/alertButtonSound.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/buySignSound.wav", __ASSET__assets_sounds_buysignsound_wav);
		type.set ("assets/sounds/buySignSound.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/calcButtonSound.wav", __ASSET__assets_sounds_calcbuttonsound_wav);
		type.set ("assets/sounds/calcButtonSound.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/cashRegisterSound.wav", __ASSET__assets_sounds_cashregistersound_wav);
		type.set ("assets/sounds/cashRegisterSound.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/coinsSound.wav", __ASSET__assets_sounds_coinssound_wav);
		type.set ("assets/sounds/coinsSound.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/correctAnswerSoundA.wav", __ASSET__assets_sounds_correctanswersounda_wav);
		type.set ("assets/sounds/correctAnswerSoundA.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/fallenAppleSound.wav", __ASSET__assets_sounds_fallenapplesound_wav);
		type.set ("assets/sounds/fallenAppleSound.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/fillingJuiceBottleSound.wav", __ASSET__assets_sounds_fillingjuicebottlesound_wav);
		type.set ("assets/sounds/fillingJuiceBottleSound.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/greenHouseWindChimeSound.wav", __ASSET__assets_sounds_greenhousewindchimesound_wav);
		type.set ("assets/sounds/greenHouseWindChimeSound.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/growingTree.wav", __ASSET__assets_sounds_growingtree_wav);
		type.set ("assets/sounds/growingTree.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/growingTreeA.wav", __ASSET__assets_sounds_growingtreea_wav);
		type.set ("assets/sounds/growingTreeA.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/growingTreeB.wav", __ASSET__assets_sounds_growingtreeb_wav);
		type.set ("assets/sounds/growingTreeB.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/growingTreeC.wav", __ASSET__assets_sounds_growingtreec_wav);
		type.set ("assets/sounds/growingTreeC.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/growingTreeD.wav", __ASSET__assets_sounds_growingtreed_wav);
		type.set ("assets/sounds/growingTreeD.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/incorrectAnswerSound.wav", __ASSET__assets_sounds_incorrectanswersound_wav);
		type.set ("assets/sounds/incorrectAnswerSound.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/juiceMachineSound.wav", __ASSET__assets_sounds_juicemachinesound_wav);
		type.set ("assets/sounds/juiceMachineSound.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/koiClicked0.wav", __ASSET__assets_sounds_koiclicked0_wav);
		type.set ("assets/sounds/koiClicked0.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/koiClicked1.wav", __ASSET__assets_sounds_koiclicked1_wav);
		type.set ("assets/sounds/koiClicked1.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/koiClicked2.wav", __ASSET__assets_sounds_koiclicked2_wav);
		type.set ("assets/sounds/koiClicked2.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/koiClicked4.wav", __ASSET__assets_sounds_koiclicked4_wav);
		type.set ("assets/sounds/koiClicked4.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/koiPondPlacedSound.wav", __ASSET__assets_sounds_koipondplacedsound_wav);
		type.set ("assets/sounds/koiPondPlacedSound.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/koiPondSound.wav", __ASSET__assets_sounds_koipondsound_wav);
		type.set ("assets/sounds/koiPondSound.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/marketSlideClosedSound.wav", __ASSET__assets_sounds_marketslideclosedsound_wav);
		type.set ("assets/sounds/marketSlideClosedSound.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/marketSlideOpenSound.wav", __ASSET__assets_sounds_marketslideopensound_wav);
		type.set ("assets/sounds/marketSlideOpenSound.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/mouseHoleSound.wav", __ASSET__assets_sounds_mouseholesound_wav);
		type.set ("assets/sounds/mouseHoleSound.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/mouseWalkingSound.wav", __ASSET__assets_sounds_mousewalkingsound_wav);
		type.set ("assets/sounds/mouseWalkingSound.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/pickedApplesGround.wav", __ASSET__assets_sounds_pickedapplesground_wav);
		type.set ("assets/sounds/pickedApplesGround.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/pickedLeavesSound.wav", __ASSET__assets_sounds_pickedleavessound_wav);
		type.set ("assets/sounds/pickedLeavesSound.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/pieBakedSound.wav", __ASSET__assets_sounds_piebakedsound_wav);
		type.set ("assets/sounds/pieBakedSound.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/placedMachineSound.wav", __ASSET__assets_sounds_placedmachinesound_wav);
		type.set ("assets/sounds/placedMachineSound.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/plantingSeedSound.wav", __ASSET__assets_sounds_plantingseedsound_wav);
		type.set ("assets/sounds/plantingSeedSound.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/pouringJuiceSound.wav", __ASSET__assets_sounds_pouringjuicesound_wav);
		type.set ("assets/sounds/pouringJuiceSound.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/raining.wav", __ASSET__assets_sounds_raining_wav);
		type.set ("assets/sounds/raining.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/sounds-go-here.txt", __ASSET__assets_sounds_sounds_go_here_txt);
		type.set ("assets/sounds/sounds-go-here.txt", AssetType.TEXT);
		
		className.set ("assets/sounds/sprinklerButtonSound.wav", __ASSET__assets_sounds_sprinklerbuttonsound_wav);
		type.set ("assets/sounds/sprinklerButtonSound.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/sprinklerSound.wav", __ASSET__assets_sounds_sprinklersound_wav);
		type.set ("assets/sounds/sprinklerSound.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/sunshine.wav", __ASSET__assets_sounds_sunshine_wav);
		type.set ("assets/sounds/sunshine.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/truckButtonSound.wav", __ASSET__assets_sounds_truckbuttonsound_wav);
		type.set ("assets/sounds/truckButtonSound.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/truckSound.wav", __ASSET__assets_sounds_trucksound_wav);
		type.set ("assets/sounds/truckSound.wav", AssetType.SOUND);
		
		className.set ("assets/sounds/beep.mp3", __ASSET__assets_sounds_beep_mp3);
		type.set ("assets/sounds/beep.mp3", AssetType.MUSIC);
		
		className.set ("assets/sounds/flixel.mp3", __ASSET__assets_sounds_flixel_mp3);
		type.set ("assets/sounds/flixel.mp3", AssetType.MUSIC);
		
		
		if (useManifest) {
			
			loadManifest ();
			
			if (Sys.args ().indexOf ("-livereload") > -1) {
				
				var path = FileSystem.fullPath ("manifest");
				lastModified = FileSystem.stat (path).mtime.getTime ();
				
				timer = new Timer (2000);
				timer.run = function () {
					
					var modified = FileSystem.stat (path).mtime.getTime ();
					
					if (modified > lastModified) {
						
						lastModified = modified;
						loadManifest ();
						
						if (eventCallback != null) {
							
							eventCallback (this, "change");
							
						}
						
					}
					
				}
				
			}
			
		}
		
		#else
		
		loadManifest ();
		
		#end
		#end
		
	}
	
	
	public override function exists (id:String, type:String):Bool {
		
		var requestedType = type != null ? cast (type, AssetType) : null;
		var assetType = this.type.get (id);
		
		if (assetType != null) {
			
			if (assetType == requestedType || ((requestedType == SOUND || requestedType == MUSIC) && (assetType == MUSIC || assetType == SOUND))) {
				
				return true;
				
			}
			
			#if flash
			
			if ((assetType == BINARY || assetType == TEXT) && requestedType == BINARY) {
				
				return true;
				
			} else if (path.exists (id)) {
				
				return true;
				
			}
			
			#else
			
			if (requestedType == BINARY || requestedType == null || (assetType == BINARY && requestedType == TEXT)) {
				
				return true;
				
			}
			
			#end
			
		}
		
		return false;
		
	}
	
	
	public override function getAudioBuffer (id:String):AudioBuffer {
		
		#if flash
		
		var buffer = new AudioBuffer ();
		buffer.src = cast (Type.createInstance (className.get (id), []), Sound);
		return buffer;
		
		#elseif html5
		
		return null;
		//return new Sound (new URLRequest (path.get (id)));
		
		#else
		
		return AudioBuffer.fromFile (path.get (id));
		//if (className.exists(id)) return cast (Type.createInstance (className.get (id), []), Sound);
		//else return new Sound (new URLRequest (path.get (id)), null, type.get (id) == MUSIC);
		
		#end
		
	}
	
	
	public override function getBytes (id:String):ByteArray {
		
		#if flash
		
		return cast (Type.createInstance (className.get (id), []), ByteArray);
		
		#elseif html5
		
		var bytes:ByteArray = null;
		var data = Preloader.loaders.get (path.get (id)).data;
		
		if (Std.is (data, String)) {
			
			bytes = new ByteArray ();
			bytes.writeUTFBytes (data);
			
		} else if (Std.is (data, ByteArray)) {
			
			bytes = cast data;
			
		} else {
			
			bytes = null;
			
		}
		
		if (bytes != null) {
			
			bytes.position = 0;
			return bytes;
			
		} else {
			
			return null;
		}
		
		#else
		
		if (className.exists(id)) return cast (Type.createInstance (className.get (id), []), ByteArray);
		else return ByteArray.readFile (path.get (id));
		
		#end
		
	}
	
	
	public override function getFont (id:String):Dynamic /*Font*/ {
		
		// TODO: Complete Lime Font API
		
		#if openfl
		#if (flash || js)
		
		return cast (Type.createInstance (className.get (id), []), openfl.text.Font);
		
		#else
		
		if (className.exists (id)) {
			
			var fontClass = className.get (id);
			openfl.text.Font.registerFont (fontClass);
			return cast (Type.createInstance (fontClass, []), openfl.text.Font);
			
		} else {
			
			return new openfl.text.Font (path.get (id));
			
		}
		
		#end
		#end
		
		return null;
		
	}
	
	
	public override function getImage (id:String):Image {
		
		#if flash
		
		return Image.fromBitmapData (cast (Type.createInstance (className.get (id), []), BitmapData));
		
		#elseif html5
		
		return Image.fromImageElement (Preloader.images.get (path.get (id)));
		
		#else
		
		return Image.fromFile (path.get (id));
		
		#end
		
	}
	
	
	/*public override function getMusic (id:String):Dynamic {
		
		#if flash
		
		return cast (Type.createInstance (className.get (id), []), Sound);
		
		#elseif openfl_html5
		
		//var sound = new Sound ();
		//sound.__buffer = true;
		//sound.load (new URLRequest (path.get (id)));
		//return sound;
		return null;
		
		#elseif html5
		
		return null;
		//return new Sound (new URLRequest (path.get (id)));
		
		#else
		
		return null;
		//if (className.exists(id)) return cast (Type.createInstance (className.get (id), []), Sound);
		//else return new Sound (new URLRequest (path.get (id)), null, true);
		
		#end
		
	}*/
	
	
	public override function getPath (id:String):String {
		
		//#if ios
		
		//return SystemPath.applicationDirectory + "/assets/" + path.get (id);
		
		//#else
		
		return path.get (id);
		
		//#end
		
	}
	
	
	public override function getText (id:String):String {
		
		#if html5
		
		var bytes:ByteArray = null;
		var data = Preloader.loaders.get (path.get (id)).data;
		
		if (Std.is (data, String)) {
			
			return cast data;
			
		} else if (Std.is (data, ByteArray)) {
			
			bytes = cast data;
			
		} else {
			
			bytes = null;
			
		}
		
		if (bytes != null) {
			
			bytes.position = 0;
			return bytes.readUTFBytes (bytes.length);
			
		} else {
			
			return null;
		}
		
		#else
		
		var bytes = getBytes (id);
		
		if (bytes == null) {
			
			return null;
			
		} else {
			
			return bytes.readUTFBytes (bytes.length);
			
		}
		
		#end
		
	}
	
	
	public override function isLocal (id:String, type:String):Bool {
		
		var requestedType = type != null ? cast (type, AssetType) : null;
		
		#if flash
		
		if (requestedType != AssetType.MUSIC && requestedType != AssetType.SOUND) {
			
			return className.exists (id);
			
		}
		
		#end
		
		return true;
		
	}
	
	
	public override function list (type:String):Array<String> {
		
		var requestedType = type != null ? cast (type, AssetType) : null;
		var items = [];
		
		for (id in this.type.keys ()) {
			
			if (requestedType == null || exists (id, type)) {
				
				items.push (id);
				
			}
			
		}
		
		return items;
		
	}
	
	
	public override function loadAudioBuffer (id:String, handler:AudioBuffer -> Void):Void {
		
		#if (flash || js)
		
		//if (path.exists (id)) {
			
		//	var loader = new Loader ();
		//	loader.contentLoaderInfo.addEventListener (Event.COMPLETE, function (event) {
				
		//		handler (cast (event.currentTarget.content, Bitmap).bitmapData);
				
		//	});
		//	loader.load (new URLRequest (path.get (id)));
			
		//} else {
			
			handler (getAudioBuffer (id));
			
		//}
		
		#else
		
		handler (getAudioBuffer (id));
		
		#end
		
	}
	
	
	public override function loadBytes (id:String, handler:ByteArray -> Void):Void {
		
		#if flash
		
		if (path.exists (id)) {
			
			var loader = new URLLoader ();
			loader.addEventListener (Event.COMPLETE, function (event:Event) {
				
				var bytes = new ByteArray ();
				bytes.writeUTFBytes (event.currentTarget.data);
				bytes.position = 0;
				
				handler (bytes);
				
			});
			loader.load (new URLRequest (path.get (id)));
			
		} else {
			
			handler (getBytes (id));
			
		}
		
		#else
		
		handler (getBytes (id));
		
		#end
		
	}
	
	
	public override function loadImage (id:String, handler:Image -> Void):Void {
		
		#if flash
		
		if (path.exists (id)) {
			
			var loader = new Loader ();
			loader.contentLoaderInfo.addEventListener (Event.COMPLETE, function (event:Event) {
				
				var bitmapData = cast (event.currentTarget.content, Bitmap).bitmapData;
				handler (Image.fromBitmapData (bitmapData));
				
			});
			loader.load (new URLRequest (path.get (id)));
			
		} else {
			
			handler (getImage (id));
			
		}
		
		#else
		
		handler (getImage (id));
		
		#end
		
	}
	
	
	#if (!flash && !html5)
	private function loadManifest ():Void {
		
		try {
			
			#if blackberry
			var bytes = ByteArray.readFile ("app/native/manifest");
			#elseif tizen
			var bytes = ByteArray.readFile ("../res/manifest");
			#elseif emscripten
			var bytes = ByteArray.readFile ("assets/manifest");
			#elseif (mac && java)
			var bytes = ByteArray.readFile ("../Resources/manifest");
			#else
			var bytes = ByteArray.readFile ("manifest");
			#end
			
			if (bytes != null) {
				
				bytes.position = 0;
				
				if (bytes.length > 0) {
					
					var data = bytes.readUTFBytes (bytes.length);
					
					if (data != null && data.length > 0) {
						
						var manifest:Array<Dynamic> = Unserializer.run (data);
						
						for (asset in manifest) {
							
							if (!className.exists (asset.id)) {
								
								path.set (asset.id, asset.path);
								type.set (asset.id, cast (asset.type, AssetType));
								
							}
							
						}
						
					}
					
				}
				
			} else {
				
				trace ("Warning: Could not load asset manifest (bytes was null)");
				
			}
		
		} catch (e:Dynamic) {
			
			trace ('Warning: Could not load asset manifest (${e})');
			
		}
		
	}
	#end
	
	
	/*public override function loadMusic (id:String, handler:Dynamic -> Void):Void {
		
		#if (flash || js)
		
		//if (path.exists (id)) {
			
		//	var loader = new Loader ();
		//	loader.contentLoaderInfo.addEventListener (Event.COMPLETE, function (event) {
				
		//		handler (cast (event.currentTarget.content, Bitmap).bitmapData);
				
		//	});
		//	loader.load (new URLRequest (path.get (id)));
			
		//} else {
			
			handler (getMusic (id));
			
		//}
		
		#else
		
		handler (getMusic (id));
		
		#end
		
	}*/
	
	
	public override function loadText (id:String, handler:String -> Void):Void {
		
		//#if html5
		
		/*if (path.exists (id)) {
			
			var loader = new URLLoader ();
			loader.addEventListener (Event.COMPLETE, function (event:Event) {
				
				handler (event.currentTarget.data);
				
			});
			loader.load (new URLRequest (path.get (id)));
			
		} else {
			
			handler (getText (id));
			
		}*/
		
		//#else
		
		var callback = function (bytes:ByteArray):Void {
			
			if (bytes == null) {
				
				handler (null);
				
			} else {
				
				handler (bytes.readUTFBytes (bytes.length));
				
			}
			
		}
		
		loadBytes (id, callback);
		
		//#end
		
	}
	
	
}


#if !display
#if flash

@:keep @:bind #if display private #end class __ASSET__assets_data_data_goes_here_txt extends flash.utils.ByteArray { }
@:keep @:bind #if display private #end class __ASSET__assets_images_animatedapplea_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_animatedappleb_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_answerbutton_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_applejuiceitemimage_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_applerotanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_appleshedanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_appletruck_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_appletruckbuttononanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_backgroundmassa_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_backgroundmassamaster_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_backgroundmassb_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_backgroundmassbmaster_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_backgroundmassc_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_backgroundmasscmaster_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_backgroundmassd_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_backgroundmassdmaster_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_backgroundmasse_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_backgroundmassemaster_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_backgroundmassf_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_backgroundmassfmaster_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_backgroundmassfmouse_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_backgroundmassg_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_backgroundmassgmaster_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_backgroundmassh_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_backgroundmasshmaster_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_basketanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_blackboard_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_blackboardcorrectborder_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_blackboardwrongborder_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_boilerimage_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_bridgeimage_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_buildbar_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_buildbarshaft_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_buildbartab_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_buttonclear_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_buttoneight_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_buttonfive_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_buttonfour_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_buttonnine_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_buttonone_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_buttonseven_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_buttonsix_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_buttonthree_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_buttontwo_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_buttonzero_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_buttonzeroshadow_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_buylandbutton_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_calcimage_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_chalkequals_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_chalknumbers_0_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_chalknumbers_1_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_chalknumbers_2_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_chalknumbers_3_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_chalknumbers_4_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_chalknumbers_5_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_chalknumbers_6_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_chalknumbers_7_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_chalknumbers_8_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_chalknumbers_9_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_cloud_blue_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_coypondimage_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_dropletsanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_dropletsanimations_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_dropletsanimationt_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_dropletsanimationu_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_fillingbottleanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_fishicon_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_fishitemanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_gametile_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_gametileboarder_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_gametileowned_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_goldcoin_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_greenhouseanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_greenhouseiconimage_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_greenhouseitemanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_greenhouseshrub_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_greenhousetree_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_greenhousetreeanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_holeanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_images_go_here_txt extends flash.utils.ByteArray { }
@:keep @:bind #if display private #end class __ASSET__assets_images_islandmouseanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_juicemachineanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_juicemachineicon_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_juicemachineswitchanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_koifishanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_koifishitemdrag_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_koipad_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_koipondanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_koipondimage_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_koirock_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_koisand_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_marketsidebar_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_marketsidebarshaft_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_marketsidebartab_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_market_seed_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_meteorfield_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_meteorfieldmaster_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_minusoneimage_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_minusoperation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_mouseanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_multioperation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_optionsscreen_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_packofjuiceicon_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_packofpiesicon_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_pickedappleleaves_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_pieicon_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_piemachineanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_piemachineitem_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_plusoperation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_raincloudanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_raincloudanimationa_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_repairanimations_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_rockyforeground_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_rollingmoon_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_seeditemanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_sidewaysisland_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_spacebackground_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_sprinkleralertanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_sprinkleralertanimations_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_sprinkleranimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_sprinklerswtichanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_sunanimationa_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_sunanimationb_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_sunanimationc_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_suntokenanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_systemstar_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_systemstaranimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_tileanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_tileanimationtest_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_tilesprite_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_titlebackground_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_titleeasy_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_titleimage_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_titleimageprocessed_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_titlescreengrass_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_treeanim0_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_treeanim1_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_treeanim2_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_treeanim3_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_treeanim4_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_treeanim5_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_treeanim6_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_treeanim7_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_treeanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_treeanimationa_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_treeanimationb_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_treedeathanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_treedeathanimation97_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_treedeathanimation98_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_treedeathanimation99_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_treespritea_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_treespritea99_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_tree_seed_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_truckbellanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_truckbellimage_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_tutcheckboxanimate_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_tuthand_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_tuthandanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_tutmessageeight_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_tutmessagefive_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_tutmessagefour_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_tutmessagenine_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_tutmessageone_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_tutmessageseven_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_tutmessagesix_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_tutmessageten_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_tutmessagethree_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_tutmessagetwo_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_upsidedownisland_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_waterfallanimation_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_waterfallimage_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_waterfalltile_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_weeda_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_images_weedb_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_music_bepplesongone_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_music_bepplesongoneogg_ogg extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_music_music_goes_here_txt extends flash.utils.ByteArray { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_alertbuttonsound_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_buysignsound_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_calcbuttonsound_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_cashregistersound_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_coinssound_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_correctanswersounda_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_fallenapplesound_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_fillingjuicebottlesound_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_greenhousewindchimesound_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_growingtree_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_growingtreea_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_growingtreeb_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_growingtreec_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_growingtreed_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_incorrectanswersound_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_juicemachinesound_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_koiclicked0_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_koiclicked1_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_koiclicked2_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_koiclicked4_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_koipondplacedsound_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_koipondsound_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_marketslideclosedsound_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_marketslideopensound_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_mouseholesound_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_mousewalkingsound_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_pickedapplesground_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_pickedleavessound_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_piebakedsound_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_placedmachinesound_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_plantingseedsound_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_pouringjuicesound_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_raining_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_sounds_go_here_txt extends flash.utils.ByteArray { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_sprinklerbuttonsound_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_sprinklersound_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_sunshine_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_truckbuttonsound_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_trucksound_wav extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_beep_mp3 extends flash.media.Sound { }
@:keep @:bind #if display private #end class __ASSET__assets_sounds_flixel_mp3 extends flash.media.Sound { }


#elseif html5

#if openfl




























































































































































































































#end

#else

#if openfl

#end

#if (windows || mac || linux)


@:file("assets/data/data-goes-here.txt") class __ASSET__assets_data_data_goes_here_txt extends lime.utils.ByteArray {}
@:bitmap("assets/images/animatedAppleA.png") class __ASSET__assets_images_animatedapplea_png extends lime.graphics.Image {}
@:bitmap("assets/images/animatedAppleB.png") class __ASSET__assets_images_animatedappleb_png extends lime.graphics.Image {}
@:bitmap("assets/images/answerButton.png") class __ASSET__assets_images_answerbutton_png extends lime.graphics.Image {}
@:bitmap("assets/images/appleJuiceItemImage.png") class __ASSET__assets_images_applejuiceitemimage_png extends lime.graphics.Image {}
@:bitmap("assets/images/appleRotAnimation.png") class __ASSET__assets_images_applerotanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/appleShedAnimation.png") class __ASSET__assets_images_appleshedanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/appleTruck.png") class __ASSET__assets_images_appletruck_png extends lime.graphics.Image {}
@:bitmap("assets/images/appleTruckButtonOnAnimation.png") class __ASSET__assets_images_appletruckbuttononanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/backGroundMassA.png") class __ASSET__assets_images_backgroundmassa_png extends lime.graphics.Image {}
@:bitmap("assets/images/backGroundMassAmaster.png") class __ASSET__assets_images_backgroundmassamaster_png extends lime.graphics.Image {}
@:bitmap("assets/images/backGroundMassB.png") class __ASSET__assets_images_backgroundmassb_png extends lime.graphics.Image {}
@:bitmap("assets/images/backGroundMassBmaster.png") class __ASSET__assets_images_backgroundmassbmaster_png extends lime.graphics.Image {}
@:bitmap("assets/images/backGroundMassC.png") class __ASSET__assets_images_backgroundmassc_png extends lime.graphics.Image {}
@:bitmap("assets/images/backGroundMassCmaster.png") class __ASSET__assets_images_backgroundmasscmaster_png extends lime.graphics.Image {}
@:bitmap("assets/images/backGroundMassD.png") class __ASSET__assets_images_backgroundmassd_png extends lime.graphics.Image {}
@:bitmap("assets/images/backGroundMassDmaster.png") class __ASSET__assets_images_backgroundmassdmaster_png extends lime.graphics.Image {}
@:bitmap("assets/images/backGroundMassE.png") class __ASSET__assets_images_backgroundmasse_png extends lime.graphics.Image {}
@:bitmap("assets/images/backGroundMassEmaster.png") class __ASSET__assets_images_backgroundmassemaster_png extends lime.graphics.Image {}
@:bitmap("assets/images/backGroundMassF.png") class __ASSET__assets_images_backgroundmassf_png extends lime.graphics.Image {}
@:bitmap("assets/images/backGroundMassFmaster.png") class __ASSET__assets_images_backgroundmassfmaster_png extends lime.graphics.Image {}
@:bitmap("assets/images/backGroundMassFMouse.png") class __ASSET__assets_images_backgroundmassfmouse_png extends lime.graphics.Image {}
@:bitmap("assets/images/backGroundMassG.png") class __ASSET__assets_images_backgroundmassg_png extends lime.graphics.Image {}
@:bitmap("assets/images/backGroundMassGmaster.png") class __ASSET__assets_images_backgroundmassgmaster_png extends lime.graphics.Image {}
@:bitmap("assets/images/backGroundMassH.png") class __ASSET__assets_images_backgroundmassh_png extends lime.graphics.Image {}
@:bitmap("assets/images/backGroundMassHmaster.png") class __ASSET__assets_images_backgroundmasshmaster_png extends lime.graphics.Image {}
@:bitmap("assets/images/basketAnimation.png") class __ASSET__assets_images_basketanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/blackBoard.png") class __ASSET__assets_images_blackboard_png extends lime.graphics.Image {}
@:bitmap("assets/images/blackBoardCorrectBorder.png") class __ASSET__assets_images_blackboardcorrectborder_png extends lime.graphics.Image {}
@:bitmap("assets/images/blackBoardWrongBorder.png") class __ASSET__assets_images_blackboardwrongborder_png extends lime.graphics.Image {}
@:bitmap("assets/images/boilerImage.png") class __ASSET__assets_images_boilerimage_png extends lime.graphics.Image {}
@:bitmap("assets/images/bridgeImage.png") class __ASSET__assets_images_bridgeimage_png extends lime.graphics.Image {}
@:bitmap("assets/images/buildBar.png") class __ASSET__assets_images_buildbar_png extends lime.graphics.Image {}
@:bitmap("assets/images/buildBarShaft.png") class __ASSET__assets_images_buildbarshaft_png extends lime.graphics.Image {}
@:bitmap("assets/images/buildBarTab.png") class __ASSET__assets_images_buildbartab_png extends lime.graphics.Image {}
@:bitmap("assets/images/buttonClear.png") class __ASSET__assets_images_buttonclear_png extends lime.graphics.Image {}
@:bitmap("assets/images/buttonEight.png") class __ASSET__assets_images_buttoneight_png extends lime.graphics.Image {}
@:bitmap("assets/images/buttonFive.png") class __ASSET__assets_images_buttonfive_png extends lime.graphics.Image {}
@:bitmap("assets/images/buttonFour.png") class __ASSET__assets_images_buttonfour_png extends lime.graphics.Image {}
@:bitmap("assets/images/buttonNine.png") class __ASSET__assets_images_buttonnine_png extends lime.graphics.Image {}
@:bitmap("assets/images/buttonOne.png") class __ASSET__assets_images_buttonone_png extends lime.graphics.Image {}
@:bitmap("assets/images/buttonSeven.png") class __ASSET__assets_images_buttonseven_png extends lime.graphics.Image {}
@:bitmap("assets/images/buttonSix.png") class __ASSET__assets_images_buttonsix_png extends lime.graphics.Image {}
@:bitmap("assets/images/buttonThree.png") class __ASSET__assets_images_buttonthree_png extends lime.graphics.Image {}
@:bitmap("assets/images/buttonTwo.png") class __ASSET__assets_images_buttontwo_png extends lime.graphics.Image {}
@:bitmap("assets/images/buttonZero.png") class __ASSET__assets_images_buttonzero_png extends lime.graphics.Image {}
@:bitmap("assets/images/buttonZeroShadow.png") class __ASSET__assets_images_buttonzeroshadow_png extends lime.graphics.Image {}
@:bitmap("assets/images/buyLandButton.png") class __ASSET__assets_images_buylandbutton_png extends lime.graphics.Image {}
@:bitmap("assets/images/calcImage.png") class __ASSET__assets_images_calcimage_png extends lime.graphics.Image {}
@:bitmap("assets/images/chalkEquals.png") class __ASSET__assets_images_chalkequals_png extends lime.graphics.Image {}
@:bitmap("assets/images/chalkNumbers_0.png") class __ASSET__assets_images_chalknumbers_0_png extends lime.graphics.Image {}
@:bitmap("assets/images/chalkNumbers_1.png") class __ASSET__assets_images_chalknumbers_1_png extends lime.graphics.Image {}
@:bitmap("assets/images/chalkNumbers_2.png") class __ASSET__assets_images_chalknumbers_2_png extends lime.graphics.Image {}
@:bitmap("assets/images/chalkNumbers_3.png") class __ASSET__assets_images_chalknumbers_3_png extends lime.graphics.Image {}
@:bitmap("assets/images/chalkNumbers_4.png") class __ASSET__assets_images_chalknumbers_4_png extends lime.graphics.Image {}
@:bitmap("assets/images/chalkNumbers_5.png") class __ASSET__assets_images_chalknumbers_5_png extends lime.graphics.Image {}
@:bitmap("assets/images/chalkNumbers_6.png") class __ASSET__assets_images_chalknumbers_6_png extends lime.graphics.Image {}
@:bitmap("assets/images/chalkNumbers_7.png") class __ASSET__assets_images_chalknumbers_7_png extends lime.graphics.Image {}
@:bitmap("assets/images/chalkNumbers_8.png") class __ASSET__assets_images_chalknumbers_8_png extends lime.graphics.Image {}
@:bitmap("assets/images/chalkNumbers_9.png") class __ASSET__assets_images_chalknumbers_9_png extends lime.graphics.Image {}
@:bitmap("assets/images/cloud_blue.png") class __ASSET__assets_images_cloud_blue_png extends lime.graphics.Image {}
@:bitmap("assets/images/coyPondImage.png") class __ASSET__assets_images_coypondimage_png extends lime.graphics.Image {}
@:bitmap("assets/images/dropletsAnimation.png") class __ASSET__assets_images_dropletsanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/dropletsAnimationS.png") class __ASSET__assets_images_dropletsanimations_png extends lime.graphics.Image {}
@:bitmap("assets/images/dropletsAnimationT.png") class __ASSET__assets_images_dropletsanimationt_png extends lime.graphics.Image {}
@:bitmap("assets/images/dropletsAnimationU.png") class __ASSET__assets_images_dropletsanimationu_png extends lime.graphics.Image {}
@:bitmap("assets/images/fillingBottleAnimation.png") class __ASSET__assets_images_fillingbottleanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/fishIcon.png") class __ASSET__assets_images_fishicon_png extends lime.graphics.Image {}
@:bitmap("assets/images/fishItemAnimation.png") class __ASSET__assets_images_fishitemanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/gameTile.png") class __ASSET__assets_images_gametile_png extends lime.graphics.Image {}
@:bitmap("assets/images/gameTileBoarder.png") class __ASSET__assets_images_gametileboarder_png extends lime.graphics.Image {}
@:bitmap("assets/images/gameTileOwned.png") class __ASSET__assets_images_gametileowned_png extends lime.graphics.Image {}
@:bitmap("assets/images/goldCoin.png") class __ASSET__assets_images_goldcoin_png extends lime.graphics.Image {}
@:bitmap("assets/images/greenHouseAnimation.png") class __ASSET__assets_images_greenhouseanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/greenHouseIconImage.png") class __ASSET__assets_images_greenhouseiconimage_png extends lime.graphics.Image {}
@:bitmap("assets/images/GreenHouseItemAnimation.png") class __ASSET__assets_images_greenhouseitemanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/greenHouseShrub.png") class __ASSET__assets_images_greenhouseshrub_png extends lime.graphics.Image {}
@:bitmap("assets/images/greenHouseTree.png") class __ASSET__assets_images_greenhousetree_png extends lime.graphics.Image {}
@:bitmap("assets/images/greenHouseTreeAnimation.png") class __ASSET__assets_images_greenhousetreeanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/holeAnimation.png") class __ASSET__assets_images_holeanimation_png extends lime.graphics.Image {}
@:file("assets/images/images-go-here.txt") class __ASSET__assets_images_images_go_here_txt extends lime.utils.ByteArray {}
@:bitmap("assets/images/islandMouseAnimation.png") class __ASSET__assets_images_islandmouseanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/juiceMachineAnimation.png") class __ASSET__assets_images_juicemachineanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/juiceMachineIcon.png") class __ASSET__assets_images_juicemachineicon_png extends lime.graphics.Image {}
@:bitmap("assets/images/juiceMachineSwitchAnimation.png") class __ASSET__assets_images_juicemachineswitchanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/koiFishAnimation.png") class __ASSET__assets_images_koifishanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/koiFishItemDrag.png") class __ASSET__assets_images_koifishitemdrag_png extends lime.graphics.Image {}
@:bitmap("assets/images/koiPad.png") class __ASSET__assets_images_koipad_png extends lime.graphics.Image {}
@:bitmap("assets/images/koiPondAnimation.png") class __ASSET__assets_images_koipondanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/koiPondImage.png") class __ASSET__assets_images_koipondimage_png extends lime.graphics.Image {}
@:bitmap("assets/images/koiRock.png") class __ASSET__assets_images_koirock_png extends lime.graphics.Image {}
@:bitmap("assets/images/koiSand.png") class __ASSET__assets_images_koisand_png extends lime.graphics.Image {}
@:bitmap("assets/images/marketSideBar.png") class __ASSET__assets_images_marketsidebar_png extends lime.graphics.Image {}
@:bitmap("assets/images/marketSideBarShaft.png") class __ASSET__assets_images_marketsidebarshaft_png extends lime.graphics.Image {}
@:bitmap("assets/images/marketSideBarTab.png") class __ASSET__assets_images_marketsidebartab_png extends lime.graphics.Image {}
@:bitmap("assets/images/market_seed.png") class __ASSET__assets_images_market_seed_png extends lime.graphics.Image {}
@:bitmap("assets/images/meteorField.png") class __ASSET__assets_images_meteorfield_png extends lime.graphics.Image {}
@:bitmap("assets/images/meteorFieldMaster.png") class __ASSET__assets_images_meteorfieldmaster_png extends lime.graphics.Image {}
@:bitmap("assets/images/minusOneImage.png") class __ASSET__assets_images_minusoneimage_png extends lime.graphics.Image {}
@:bitmap("assets/images/minusOperation.png") class __ASSET__assets_images_minusoperation_png extends lime.graphics.Image {}
@:bitmap("assets/images/mouseAnimation.png") class __ASSET__assets_images_mouseanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/multiOperation.png") class __ASSET__assets_images_multioperation_png extends lime.graphics.Image {}
@:bitmap("assets/images/optionsScreen.png") class __ASSET__assets_images_optionsscreen_png extends lime.graphics.Image {}
@:bitmap("assets/images/packOfJuiceIcon.png") class __ASSET__assets_images_packofjuiceicon_png extends lime.graphics.Image {}
@:bitmap("assets/images/packOfPiesIcon.png") class __ASSET__assets_images_packofpiesicon_png extends lime.graphics.Image {}
@:bitmap("assets/images/pickedAppleLeaves.png") class __ASSET__assets_images_pickedappleleaves_png extends lime.graphics.Image {}
@:bitmap("assets/images/pieIcon.png") class __ASSET__assets_images_pieicon_png extends lime.graphics.Image {}
@:bitmap("assets/images/pieMachineAnimation.png") class __ASSET__assets_images_piemachineanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/pieMachineItem.png") class __ASSET__assets_images_piemachineitem_png extends lime.graphics.Image {}
@:bitmap("assets/images/plusOperation.png") class __ASSET__assets_images_plusoperation_png extends lime.graphics.Image {}
@:bitmap("assets/images/rainCloudAnimation.png") class __ASSET__assets_images_raincloudanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/rainCloudAnimationA.png") class __ASSET__assets_images_raincloudanimationa_png extends lime.graphics.Image {}
@:bitmap("assets/images/repairAnimationS.png") class __ASSET__assets_images_repairanimations_png extends lime.graphics.Image {}
@:bitmap("assets/images/rockyForeground.png") class __ASSET__assets_images_rockyforeground_png extends lime.graphics.Image {}
@:bitmap("assets/images/rollingMoon.png") class __ASSET__assets_images_rollingmoon_png extends lime.graphics.Image {}
@:bitmap("assets/images/seedItemAnimation.png") class __ASSET__assets_images_seeditemanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/sidewaysIsland.png") class __ASSET__assets_images_sidewaysisland_png extends lime.graphics.Image {}
@:bitmap("assets/images/spaceBackground.png") class __ASSET__assets_images_spacebackground_png extends lime.graphics.Image {}
@:bitmap("assets/images/sprinklerAlertAnimation.png") class __ASSET__assets_images_sprinkleralertanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/sprinklerAlertAnimationS.png") class __ASSET__assets_images_sprinkleralertanimations_png extends lime.graphics.Image {}
@:bitmap("assets/images/sprinklerAnimation.png") class __ASSET__assets_images_sprinkleranimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/sprinklerSwtichAnimation.png") class __ASSET__assets_images_sprinklerswtichanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/sunAnimationA.png") class __ASSET__assets_images_sunanimationa_png extends lime.graphics.Image {}
@:bitmap("assets/images/sunAnimationB.png") class __ASSET__assets_images_sunanimationb_png extends lime.graphics.Image {}
@:bitmap("assets/images/sunAnimationC.png") class __ASSET__assets_images_sunanimationc_png extends lime.graphics.Image {}
@:bitmap("assets/images/sunTokenAnimation.png") class __ASSET__assets_images_suntokenanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/systemStar.png") class __ASSET__assets_images_systemstar_png extends lime.graphics.Image {}
@:bitmap("assets/images/systemStarAnimation.png") class __ASSET__assets_images_systemstaranimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/tileAnimation.png") class __ASSET__assets_images_tileanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/tileAnimationTEST.png") class __ASSET__assets_images_tileanimationtest_png extends lime.graphics.Image {}
@:bitmap("assets/images/tileSprite.png") class __ASSET__assets_images_tilesprite_png extends lime.graphics.Image {}
@:bitmap("assets/images/titleBackground.png") class __ASSET__assets_images_titlebackground_png extends lime.graphics.Image {}
@:bitmap("assets/images/titleEasy.png") class __ASSET__assets_images_titleeasy_png extends lime.graphics.Image {}
@:bitmap("assets/images/titleImage.png") class __ASSET__assets_images_titleimage_png extends lime.graphics.Image {}
@:bitmap("assets/images/titleImageProcessed.png") class __ASSET__assets_images_titleimageprocessed_png extends lime.graphics.Image {}
@:bitmap("assets/images/titleScreenGrass.png") class __ASSET__assets_images_titlescreengrass_png extends lime.graphics.Image {}
@:bitmap("assets/images/treeAnim0.png") class __ASSET__assets_images_treeanim0_png extends lime.graphics.Image {}
@:bitmap("assets/images/treeAnim1.png") class __ASSET__assets_images_treeanim1_png extends lime.graphics.Image {}
@:bitmap("assets/images/treeAnim2.png") class __ASSET__assets_images_treeanim2_png extends lime.graphics.Image {}
@:bitmap("assets/images/treeAnim3.png") class __ASSET__assets_images_treeanim3_png extends lime.graphics.Image {}
@:bitmap("assets/images/treeAnim4.png") class __ASSET__assets_images_treeanim4_png extends lime.graphics.Image {}
@:bitmap("assets/images/treeAnim5.png") class __ASSET__assets_images_treeanim5_png extends lime.graphics.Image {}
@:bitmap("assets/images/treeAnim6.png") class __ASSET__assets_images_treeanim6_png extends lime.graphics.Image {}
@:bitmap("assets/images/treeAnim7.png") class __ASSET__assets_images_treeanim7_png extends lime.graphics.Image {}
@:bitmap("assets/images/treeAnimation.png") class __ASSET__assets_images_treeanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/treeAnimationA.png") class __ASSET__assets_images_treeanimationa_png extends lime.graphics.Image {}
@:bitmap("assets/images/treeAnimationB.png") class __ASSET__assets_images_treeanimationb_png extends lime.graphics.Image {}
@:bitmap("assets/images/treeDeathAnimation.png") class __ASSET__assets_images_treedeathanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/treeDeathAnimation97.png") class __ASSET__assets_images_treedeathanimation97_png extends lime.graphics.Image {}
@:bitmap("assets/images/treeDeathAnimation98.png") class __ASSET__assets_images_treedeathanimation98_png extends lime.graphics.Image {}
@:bitmap("assets/images/treeDeathAnimation99.png") class __ASSET__assets_images_treedeathanimation99_png extends lime.graphics.Image {}
@:bitmap("assets/images/treeSpriteA.png") class __ASSET__assets_images_treespritea_png extends lime.graphics.Image {}
@:bitmap("assets/images/treeSpriteA99.png") class __ASSET__assets_images_treespritea99_png extends lime.graphics.Image {}
@:bitmap("assets/images/tree_seed.png") class __ASSET__assets_images_tree_seed_png extends lime.graphics.Image {}
@:bitmap("assets/images/truckBellAnimation.png") class __ASSET__assets_images_truckbellanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/truckBellImage.png") class __ASSET__assets_images_truckbellimage_png extends lime.graphics.Image {}
@:bitmap("assets/images/tutCheckBoxAnimate.png") class __ASSET__assets_images_tutcheckboxanimate_png extends lime.graphics.Image {}
@:bitmap("assets/images/tutHand.png") class __ASSET__assets_images_tuthand_png extends lime.graphics.Image {}
@:bitmap("assets/images/tutHandAnimation.png") class __ASSET__assets_images_tuthandanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/tutMessageEight.png") class __ASSET__assets_images_tutmessageeight_png extends lime.graphics.Image {}
@:bitmap("assets/images/tutMessageFive.png") class __ASSET__assets_images_tutmessagefive_png extends lime.graphics.Image {}
@:bitmap("assets/images/tutMessageFour.png") class __ASSET__assets_images_tutmessagefour_png extends lime.graphics.Image {}
@:bitmap("assets/images/tutMessageNine.png") class __ASSET__assets_images_tutmessagenine_png extends lime.graphics.Image {}
@:bitmap("assets/images/tutMessageOne.png") class __ASSET__assets_images_tutmessageone_png extends lime.graphics.Image {}
@:bitmap("assets/images/tutMessageSeven.png") class __ASSET__assets_images_tutmessageseven_png extends lime.graphics.Image {}
@:bitmap("assets/images/tutMessageSix.png") class __ASSET__assets_images_tutmessagesix_png extends lime.graphics.Image {}
@:bitmap("assets/images/tutMessageTen.png") class __ASSET__assets_images_tutmessageten_png extends lime.graphics.Image {}
@:bitmap("assets/images/tutMessageThree.png") class __ASSET__assets_images_tutmessagethree_png extends lime.graphics.Image {}
@:bitmap("assets/images/tutMessageTwo.png") class __ASSET__assets_images_tutmessagetwo_png extends lime.graphics.Image {}
@:bitmap("assets/images/upsideDownIsland.png") class __ASSET__assets_images_upsidedownisland_png extends lime.graphics.Image {}
@:bitmap("assets/images/waterfallAnimation.png") class __ASSET__assets_images_waterfallanimation_png extends lime.graphics.Image {}
@:bitmap("assets/images/waterfallImage.png") class __ASSET__assets_images_waterfallimage_png extends lime.graphics.Image {}
@:bitmap("assets/images/waterFallTile.png") class __ASSET__assets_images_waterfalltile_png extends lime.graphics.Image {}
@:bitmap("assets/images/weedA.png") class __ASSET__assets_images_weeda_png extends lime.graphics.Image {}
@:bitmap("assets/images/weedB.png") class __ASSET__assets_images_weedb_png extends lime.graphics.Image {}
@:sound("assets/music/BeppleSongONe.wav") class __ASSET__assets_music_bepplesongone_wav extends lime.audio.AudioSource {}
@:sound("assets/music/BeppleSongOneOGG.ogg") class __ASSET__assets_music_bepplesongoneogg_ogg extends lime.audio.AudioSource {}
@:file("assets/music/music-goes-here.txt") class __ASSET__assets_music_music_goes_here_txt extends lime.utils.ByteArray {}
@:sound("assets/sounds/alertButtonSound.wav") class __ASSET__assets_sounds_alertbuttonsound_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/buySignSound.wav") class __ASSET__assets_sounds_buysignsound_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/calcButtonSound.wav") class __ASSET__assets_sounds_calcbuttonsound_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/cashRegisterSound.wav") class __ASSET__assets_sounds_cashregistersound_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/coinsSound.wav") class __ASSET__assets_sounds_coinssound_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/correctAnswerSoundA.wav") class __ASSET__assets_sounds_correctanswersounda_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/fallenAppleSound.wav") class __ASSET__assets_sounds_fallenapplesound_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/fillingJuiceBottleSound.wav") class __ASSET__assets_sounds_fillingjuicebottlesound_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/greenHouseWindChimeSound.wav") class __ASSET__assets_sounds_greenhousewindchimesound_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/growingTree.wav") class __ASSET__assets_sounds_growingtree_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/growingTreeA.wav") class __ASSET__assets_sounds_growingtreea_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/growingTreeB.wav") class __ASSET__assets_sounds_growingtreeb_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/growingTreeC.wav") class __ASSET__assets_sounds_growingtreec_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/growingTreeD.wav") class __ASSET__assets_sounds_growingtreed_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/incorrectAnswerSound.wav") class __ASSET__assets_sounds_incorrectanswersound_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/juiceMachineSound.wav") class __ASSET__assets_sounds_juicemachinesound_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/koiClicked0.wav") class __ASSET__assets_sounds_koiclicked0_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/koiClicked1.wav") class __ASSET__assets_sounds_koiclicked1_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/koiClicked2.wav") class __ASSET__assets_sounds_koiclicked2_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/koiClicked4.wav") class __ASSET__assets_sounds_koiclicked4_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/koiPondPlacedSound.wav") class __ASSET__assets_sounds_koipondplacedsound_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/koiPondSound.wav") class __ASSET__assets_sounds_koipondsound_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/marketSlideClosedSound.wav") class __ASSET__assets_sounds_marketslideclosedsound_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/marketSlideOpenSound.wav") class __ASSET__assets_sounds_marketslideopensound_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/mouseHoleSound.wav") class __ASSET__assets_sounds_mouseholesound_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/mouseWalkingSound.wav") class __ASSET__assets_sounds_mousewalkingsound_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/pickedApplesGround.wav") class __ASSET__assets_sounds_pickedapplesground_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/pickedLeavesSound.wav") class __ASSET__assets_sounds_pickedleavessound_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/pieBakedSound.wav") class __ASSET__assets_sounds_piebakedsound_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/placedMachineSound.wav") class __ASSET__assets_sounds_placedmachinesound_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/plantingSeedSound.wav") class __ASSET__assets_sounds_plantingseedsound_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/pouringJuiceSound.wav") class __ASSET__assets_sounds_pouringjuicesound_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/raining.wav") class __ASSET__assets_sounds_raining_wav extends lime.audio.AudioSource {}
@:file("assets/sounds/sounds-go-here.txt") class __ASSET__assets_sounds_sounds_go_here_txt extends lime.utils.ByteArray {}
@:sound("assets/sounds/sprinklerButtonSound.wav") class __ASSET__assets_sounds_sprinklerbuttonsound_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/sprinklerSound.wav") class __ASSET__assets_sounds_sprinklersound_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/sunshine.wav") class __ASSET__assets_sounds_sunshine_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/truckButtonSound.wav") class __ASSET__assets_sounds_truckbuttonsound_wav extends lime.audio.AudioSource {}
@:sound("assets/sounds/truckSound.wav") class __ASSET__assets_sounds_trucksound_wav extends lime.audio.AudioSource {}
@:sound("C:/HaxeToolkit/haxe/lib/flixel/3,3,6/assets/sounds/beep.mp3") class __ASSET__assets_sounds_beep_mp3 extends lime.audio.AudioSource {}
@:sound("C:/HaxeToolkit/haxe/lib/flixel/3,3,6/assets/sounds/flixel.mp3") class __ASSET__assets_sounds_flixel_mp3 extends lime.audio.AudioSource {}



#end

#end
#end

