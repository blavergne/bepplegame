package;


import haxe.Timer;
import haxe.Unserializer;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.MovieClip;
import openfl.events.Event;
import openfl.text.Font;
import openfl.media.Sound;
import openfl.net.URLRequest;
import openfl.utils.ByteArray;
import openfl.Assets;

#if (flash || js)
import openfl.display.Loader;
import openfl.events.Event;
import openfl.net.URLLoader;
#end

#if sys
import sys.FileSystem;
#end

#if ios
import openfl._v2.utils.SystemPath;
#end


@:access(openfl.media.Sound)
class DefaultAssetLibrary extends AssetLibrary {
	
	
	public var className (default, null) = new Map <String, Dynamic> ();
	public var path (default, null) = new Map <String, String> ();
	public var type (default, null) = new Map <String, AssetType> ();
	
	private var lastModified:Float;
	private var timer:Timer;
	
	
	public function new () {
		
		super ();
		
		#if flash
		
		path.set ("assets/data/data-goes-here.txt", "assets/data/data-goes-here.txt");
		type.set ("assets/data/data-goes-here.txt", AssetType.TEXT);
		path.set ("assets/images/animatedAppleA.png", "assets/images/animatedAppleA.png");
		type.set ("assets/images/animatedAppleA.png", AssetType.IMAGE);
		path.set ("assets/images/animatedAppleB.png", "assets/images/animatedAppleB.png");
		type.set ("assets/images/animatedAppleB.png", AssetType.IMAGE);
		path.set ("assets/images/answerButton.png", "assets/images/answerButton.png");
		type.set ("assets/images/answerButton.png", AssetType.IMAGE);
		path.set ("assets/images/appleJuiceItemImage.png", "assets/images/appleJuiceItemImage.png");
		type.set ("assets/images/appleJuiceItemImage.png", AssetType.IMAGE);
		path.set ("assets/images/appleRotAnimation.png", "assets/images/appleRotAnimation.png");
		type.set ("assets/images/appleRotAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/appleShedAnimation.png", "assets/images/appleShedAnimation.png");
		type.set ("assets/images/appleShedAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/appleTruck.png", "assets/images/appleTruck.png");
		type.set ("assets/images/appleTruck.png", AssetType.IMAGE);
		path.set ("assets/images/appleTruckButtonOnAnimation.png", "assets/images/appleTruckButtonOnAnimation.png");
		type.set ("assets/images/appleTruckButtonOnAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/backGroundMassA.png", "assets/images/backGroundMassA.png");
		type.set ("assets/images/backGroundMassA.png", AssetType.IMAGE);
		path.set ("assets/images/backGroundMassAmaster.png", "assets/images/backGroundMassAmaster.png");
		type.set ("assets/images/backGroundMassAmaster.png", AssetType.IMAGE);
		path.set ("assets/images/backGroundMassB.png", "assets/images/backGroundMassB.png");
		type.set ("assets/images/backGroundMassB.png", AssetType.IMAGE);
		path.set ("assets/images/backGroundMassBmaster.png", "assets/images/backGroundMassBmaster.png");
		type.set ("assets/images/backGroundMassBmaster.png", AssetType.IMAGE);
		path.set ("assets/images/backGroundMassC.png", "assets/images/backGroundMassC.png");
		type.set ("assets/images/backGroundMassC.png", AssetType.IMAGE);
		path.set ("assets/images/backGroundMassCmaster.png", "assets/images/backGroundMassCmaster.png");
		type.set ("assets/images/backGroundMassCmaster.png", AssetType.IMAGE);
		path.set ("assets/images/backGroundMassD.png", "assets/images/backGroundMassD.png");
		type.set ("assets/images/backGroundMassD.png", AssetType.IMAGE);
		path.set ("assets/images/backGroundMassDmaster.png", "assets/images/backGroundMassDmaster.png");
		type.set ("assets/images/backGroundMassDmaster.png", AssetType.IMAGE);
		path.set ("assets/images/backGroundMassE.png", "assets/images/backGroundMassE.png");
		type.set ("assets/images/backGroundMassE.png", AssetType.IMAGE);
		path.set ("assets/images/backGroundMassEmaster.png", "assets/images/backGroundMassEmaster.png");
		type.set ("assets/images/backGroundMassEmaster.png", AssetType.IMAGE);
		path.set ("assets/images/backGroundMassF.png", "assets/images/backGroundMassF.png");
		type.set ("assets/images/backGroundMassF.png", AssetType.IMAGE);
		path.set ("assets/images/backGroundMassFmaster.png", "assets/images/backGroundMassFmaster.png");
		type.set ("assets/images/backGroundMassFmaster.png", AssetType.IMAGE);
		path.set ("assets/images/backGroundMassFMouse.png", "assets/images/backGroundMassFMouse.png");
		type.set ("assets/images/backGroundMassFMouse.png", AssetType.IMAGE);
		path.set ("assets/images/backGroundMassG.png", "assets/images/backGroundMassG.png");
		type.set ("assets/images/backGroundMassG.png", AssetType.IMAGE);
		path.set ("assets/images/backGroundMassGmaster.png", "assets/images/backGroundMassGmaster.png");
		type.set ("assets/images/backGroundMassGmaster.png", AssetType.IMAGE);
		path.set ("assets/images/backGroundMassH.png", "assets/images/backGroundMassH.png");
		type.set ("assets/images/backGroundMassH.png", AssetType.IMAGE);
		path.set ("assets/images/backGroundMassHmaster.png", "assets/images/backGroundMassHmaster.png");
		type.set ("assets/images/backGroundMassHmaster.png", AssetType.IMAGE);
		path.set ("assets/images/basketAnimation.png", "assets/images/basketAnimation.png");
		type.set ("assets/images/basketAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/blackBoard.png", "assets/images/blackBoard.png");
		type.set ("assets/images/blackBoard.png", AssetType.IMAGE);
		path.set ("assets/images/blackBoardCorrectBorder.png", "assets/images/blackBoardCorrectBorder.png");
		type.set ("assets/images/blackBoardCorrectBorder.png", AssetType.IMAGE);
		path.set ("assets/images/blackBoardWrongBorder.png", "assets/images/blackBoardWrongBorder.png");
		type.set ("assets/images/blackBoardWrongBorder.png", AssetType.IMAGE);
		path.set ("assets/images/boilerImage.png", "assets/images/boilerImage.png");
		type.set ("assets/images/boilerImage.png", AssetType.IMAGE);
		path.set ("assets/images/bridgeImage.png", "assets/images/bridgeImage.png");
		type.set ("assets/images/bridgeImage.png", AssetType.IMAGE);
		path.set ("assets/images/buildBar.png", "assets/images/buildBar.png");
		type.set ("assets/images/buildBar.png", AssetType.IMAGE);
		path.set ("assets/images/buildBarShaft.png", "assets/images/buildBarShaft.png");
		type.set ("assets/images/buildBarShaft.png", AssetType.IMAGE);
		path.set ("assets/images/buildBarTab.png", "assets/images/buildBarTab.png");
		type.set ("assets/images/buildBarTab.png", AssetType.IMAGE);
		path.set ("assets/images/buttonClear.png", "assets/images/buttonClear.png");
		type.set ("assets/images/buttonClear.png", AssetType.IMAGE);
		path.set ("assets/images/buttonEight.png", "assets/images/buttonEight.png");
		type.set ("assets/images/buttonEight.png", AssetType.IMAGE);
		path.set ("assets/images/buttonFive.png", "assets/images/buttonFive.png");
		type.set ("assets/images/buttonFive.png", AssetType.IMAGE);
		path.set ("assets/images/buttonFour.png", "assets/images/buttonFour.png");
		type.set ("assets/images/buttonFour.png", AssetType.IMAGE);
		path.set ("assets/images/buttonNine.png", "assets/images/buttonNine.png");
		type.set ("assets/images/buttonNine.png", AssetType.IMAGE);
		path.set ("assets/images/buttonOne.png", "assets/images/buttonOne.png");
		type.set ("assets/images/buttonOne.png", AssetType.IMAGE);
		path.set ("assets/images/buttonSeven.png", "assets/images/buttonSeven.png");
		type.set ("assets/images/buttonSeven.png", AssetType.IMAGE);
		path.set ("assets/images/buttonSix.png", "assets/images/buttonSix.png");
		type.set ("assets/images/buttonSix.png", AssetType.IMAGE);
		path.set ("assets/images/buttonThree.png", "assets/images/buttonThree.png");
		type.set ("assets/images/buttonThree.png", AssetType.IMAGE);
		path.set ("assets/images/buttonTwo.png", "assets/images/buttonTwo.png");
		type.set ("assets/images/buttonTwo.png", AssetType.IMAGE);
		path.set ("assets/images/buttonZero.png", "assets/images/buttonZero.png");
		type.set ("assets/images/buttonZero.png", AssetType.IMAGE);
		path.set ("assets/images/buttonZeroShadow.png", "assets/images/buttonZeroShadow.png");
		type.set ("assets/images/buttonZeroShadow.png", AssetType.IMAGE);
		path.set ("assets/images/buyLandButton.png", "assets/images/buyLandButton.png");
		type.set ("assets/images/buyLandButton.png", AssetType.IMAGE);
		path.set ("assets/images/calcImage.png", "assets/images/calcImage.png");
		type.set ("assets/images/calcImage.png", AssetType.IMAGE);
		path.set ("assets/images/chalkEquals.png", "assets/images/chalkEquals.png");
		type.set ("assets/images/chalkEquals.png", AssetType.IMAGE);
		path.set ("assets/images/chalkNumbers_0.png", "assets/images/chalkNumbers_0.png");
		type.set ("assets/images/chalkNumbers_0.png", AssetType.IMAGE);
		path.set ("assets/images/chalkNumbers_1.png", "assets/images/chalkNumbers_1.png");
		type.set ("assets/images/chalkNumbers_1.png", AssetType.IMAGE);
		path.set ("assets/images/chalkNumbers_2.png", "assets/images/chalkNumbers_2.png");
		type.set ("assets/images/chalkNumbers_2.png", AssetType.IMAGE);
		path.set ("assets/images/chalkNumbers_3.png", "assets/images/chalkNumbers_3.png");
		type.set ("assets/images/chalkNumbers_3.png", AssetType.IMAGE);
		path.set ("assets/images/chalkNumbers_4.png", "assets/images/chalkNumbers_4.png");
		type.set ("assets/images/chalkNumbers_4.png", AssetType.IMAGE);
		path.set ("assets/images/chalkNumbers_5.png", "assets/images/chalkNumbers_5.png");
		type.set ("assets/images/chalkNumbers_5.png", AssetType.IMAGE);
		path.set ("assets/images/chalkNumbers_6.png", "assets/images/chalkNumbers_6.png");
		type.set ("assets/images/chalkNumbers_6.png", AssetType.IMAGE);
		path.set ("assets/images/chalkNumbers_7.png", "assets/images/chalkNumbers_7.png");
		type.set ("assets/images/chalkNumbers_7.png", AssetType.IMAGE);
		path.set ("assets/images/chalkNumbers_8.png", "assets/images/chalkNumbers_8.png");
		type.set ("assets/images/chalkNumbers_8.png", AssetType.IMAGE);
		path.set ("assets/images/chalkNumbers_9.png", "assets/images/chalkNumbers_9.png");
		type.set ("assets/images/chalkNumbers_9.png", AssetType.IMAGE);
		path.set ("assets/images/cloud_blue.png", "assets/images/cloud_blue.png");
		type.set ("assets/images/cloud_blue.png", AssetType.IMAGE);
		path.set ("assets/images/coyPondImage.png", "assets/images/coyPondImage.png");
		type.set ("assets/images/coyPondImage.png", AssetType.IMAGE);
		path.set ("assets/images/dropletsAnimation.png", "assets/images/dropletsAnimation.png");
		type.set ("assets/images/dropletsAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/dropletsAnimationS.png", "assets/images/dropletsAnimationS.png");
		type.set ("assets/images/dropletsAnimationS.png", AssetType.IMAGE);
		path.set ("assets/images/dropletsAnimationT.png", "assets/images/dropletsAnimationT.png");
		type.set ("assets/images/dropletsAnimationT.png", AssetType.IMAGE);
		path.set ("assets/images/dropletsAnimationU.png", "assets/images/dropletsAnimationU.png");
		type.set ("assets/images/dropletsAnimationU.png", AssetType.IMAGE);
		path.set ("assets/images/fillingBottleAnimation.png", "assets/images/fillingBottleAnimation.png");
		type.set ("assets/images/fillingBottleAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/fishIcon.png", "assets/images/fishIcon.png");
		type.set ("assets/images/fishIcon.png", AssetType.IMAGE);
		path.set ("assets/images/fishItemAnimation.png", "assets/images/fishItemAnimation.png");
		type.set ("assets/images/fishItemAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/gameTile.png", "assets/images/gameTile.png");
		type.set ("assets/images/gameTile.png", AssetType.IMAGE);
		path.set ("assets/images/gameTileBoarder.png", "assets/images/gameTileBoarder.png");
		type.set ("assets/images/gameTileBoarder.png", AssetType.IMAGE);
		path.set ("assets/images/gameTileOwned.png", "assets/images/gameTileOwned.png");
		type.set ("assets/images/gameTileOwned.png", AssetType.IMAGE);
		path.set ("assets/images/goldCoin.png", "assets/images/goldCoin.png");
		type.set ("assets/images/goldCoin.png", AssetType.IMAGE);
		path.set ("assets/images/greenHouseAnimation.png", "assets/images/greenHouseAnimation.png");
		type.set ("assets/images/greenHouseAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/greenHouseIconImage.png", "assets/images/greenHouseIconImage.png");
		type.set ("assets/images/greenHouseIconImage.png", AssetType.IMAGE);
		path.set ("assets/images/GreenHouseItemAnimation.png", "assets/images/GreenHouseItemAnimation.png");
		type.set ("assets/images/GreenHouseItemAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/greenHouseShrub.png", "assets/images/greenHouseShrub.png");
		type.set ("assets/images/greenHouseShrub.png", AssetType.IMAGE);
		path.set ("assets/images/greenHouseTree.png", "assets/images/greenHouseTree.png");
		type.set ("assets/images/greenHouseTree.png", AssetType.IMAGE);
		path.set ("assets/images/greenHouseTreeAnimation.png", "assets/images/greenHouseTreeAnimation.png");
		type.set ("assets/images/greenHouseTreeAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/holeAnimation.png", "assets/images/holeAnimation.png");
		type.set ("assets/images/holeAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/images-go-here.txt", "assets/images/images-go-here.txt");
		type.set ("assets/images/images-go-here.txt", AssetType.TEXT);
		path.set ("assets/images/islandMouseAnimation.png", "assets/images/islandMouseAnimation.png");
		type.set ("assets/images/islandMouseAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/juiceMachineAnimation.png", "assets/images/juiceMachineAnimation.png");
		type.set ("assets/images/juiceMachineAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/juiceMachineIcon.png", "assets/images/juiceMachineIcon.png");
		type.set ("assets/images/juiceMachineIcon.png", AssetType.IMAGE);
		path.set ("assets/images/juiceMachineSwitchAnimation.png", "assets/images/juiceMachineSwitchAnimation.png");
		type.set ("assets/images/juiceMachineSwitchAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/koiFishAnimation.png", "assets/images/koiFishAnimation.png");
		type.set ("assets/images/koiFishAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/koiFishItemDrag.png", "assets/images/koiFishItemDrag.png");
		type.set ("assets/images/koiFishItemDrag.png", AssetType.IMAGE);
		path.set ("assets/images/koiPad.png", "assets/images/koiPad.png");
		type.set ("assets/images/koiPad.png", AssetType.IMAGE);
		path.set ("assets/images/koiPondAnimation.png", "assets/images/koiPondAnimation.png");
		type.set ("assets/images/koiPondAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/koiPondImage.png", "assets/images/koiPondImage.png");
		type.set ("assets/images/koiPondImage.png", AssetType.IMAGE);
		path.set ("assets/images/koiRock.png", "assets/images/koiRock.png");
		type.set ("assets/images/koiRock.png", AssetType.IMAGE);
		path.set ("assets/images/koiSand.png", "assets/images/koiSand.png");
		type.set ("assets/images/koiSand.png", AssetType.IMAGE);
		path.set ("assets/images/marketSideBar.png", "assets/images/marketSideBar.png");
		type.set ("assets/images/marketSideBar.png", AssetType.IMAGE);
		path.set ("assets/images/marketSideBarShaft.png", "assets/images/marketSideBarShaft.png");
		type.set ("assets/images/marketSideBarShaft.png", AssetType.IMAGE);
		path.set ("assets/images/marketSideBarTab.png", "assets/images/marketSideBarTab.png");
		type.set ("assets/images/marketSideBarTab.png", AssetType.IMAGE);
		path.set ("assets/images/market_seed.png", "assets/images/market_seed.png");
		type.set ("assets/images/market_seed.png", AssetType.IMAGE);
		path.set ("assets/images/meteorField.png", "assets/images/meteorField.png");
		type.set ("assets/images/meteorField.png", AssetType.IMAGE);
		path.set ("assets/images/meteorFieldMaster.png", "assets/images/meteorFieldMaster.png");
		type.set ("assets/images/meteorFieldMaster.png", AssetType.IMAGE);
		path.set ("assets/images/minusOneImage.png", "assets/images/minusOneImage.png");
		type.set ("assets/images/minusOneImage.png", AssetType.IMAGE);
		path.set ("assets/images/minusOperation.png", "assets/images/minusOperation.png");
		type.set ("assets/images/minusOperation.png", AssetType.IMAGE);
		path.set ("assets/images/mouseAnimation.png", "assets/images/mouseAnimation.png");
		type.set ("assets/images/mouseAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/multiOperation.png", "assets/images/multiOperation.png");
		type.set ("assets/images/multiOperation.png", AssetType.IMAGE);
		path.set ("assets/images/optionsScreen.png", "assets/images/optionsScreen.png");
		type.set ("assets/images/optionsScreen.png", AssetType.IMAGE);
		path.set ("assets/images/packOfJuiceIcon.png", "assets/images/packOfJuiceIcon.png");
		type.set ("assets/images/packOfJuiceIcon.png", AssetType.IMAGE);
		path.set ("assets/images/packOfPiesIcon.png", "assets/images/packOfPiesIcon.png");
		type.set ("assets/images/packOfPiesIcon.png", AssetType.IMAGE);
		path.set ("assets/images/pickedAppleLeaves.png", "assets/images/pickedAppleLeaves.png");
		type.set ("assets/images/pickedAppleLeaves.png", AssetType.IMAGE);
		path.set ("assets/images/pieIcon.png", "assets/images/pieIcon.png");
		type.set ("assets/images/pieIcon.png", AssetType.IMAGE);
		path.set ("assets/images/pieMachineAnimation.png", "assets/images/pieMachineAnimation.png");
		type.set ("assets/images/pieMachineAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/pieMachineItem.png", "assets/images/pieMachineItem.png");
		type.set ("assets/images/pieMachineItem.png", AssetType.IMAGE);
		path.set ("assets/images/plusOperation.png", "assets/images/plusOperation.png");
		type.set ("assets/images/plusOperation.png", AssetType.IMAGE);
		path.set ("assets/images/rainCloudAnimation.png", "assets/images/rainCloudAnimation.png");
		type.set ("assets/images/rainCloudAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/rainCloudAnimationA.png", "assets/images/rainCloudAnimationA.png");
		type.set ("assets/images/rainCloudAnimationA.png", AssetType.IMAGE);
		path.set ("assets/images/repairAnimationS.png", "assets/images/repairAnimationS.png");
		type.set ("assets/images/repairAnimationS.png", AssetType.IMAGE);
		path.set ("assets/images/rockyForeground.png", "assets/images/rockyForeground.png");
		type.set ("assets/images/rockyForeground.png", AssetType.IMAGE);
		path.set ("assets/images/rollingMoon.png", "assets/images/rollingMoon.png");
		type.set ("assets/images/rollingMoon.png", AssetType.IMAGE);
		path.set ("assets/images/seedItemAnimation.png", "assets/images/seedItemAnimation.png");
		type.set ("assets/images/seedItemAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/sidewaysIsland.png", "assets/images/sidewaysIsland.png");
		type.set ("assets/images/sidewaysIsland.png", AssetType.IMAGE);
		path.set ("assets/images/spaceBackground.png", "assets/images/spaceBackground.png");
		type.set ("assets/images/spaceBackground.png", AssetType.IMAGE);
		path.set ("assets/images/sprinklerAlertAnimation.png", "assets/images/sprinklerAlertAnimation.png");
		type.set ("assets/images/sprinklerAlertAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/sprinklerAlertAnimationS.png", "assets/images/sprinklerAlertAnimationS.png");
		type.set ("assets/images/sprinklerAlertAnimationS.png", AssetType.IMAGE);
		path.set ("assets/images/sprinklerAnimation.png", "assets/images/sprinklerAnimation.png");
		type.set ("assets/images/sprinklerAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/sprinklerSwtichAnimation.png", "assets/images/sprinklerSwtichAnimation.png");
		type.set ("assets/images/sprinklerSwtichAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/sunAnimationA.png", "assets/images/sunAnimationA.png");
		type.set ("assets/images/sunAnimationA.png", AssetType.IMAGE);
		path.set ("assets/images/sunAnimationB.png", "assets/images/sunAnimationB.png");
		type.set ("assets/images/sunAnimationB.png", AssetType.IMAGE);
		path.set ("assets/images/sunAnimationC.png", "assets/images/sunAnimationC.png");
		type.set ("assets/images/sunAnimationC.png", AssetType.IMAGE);
		path.set ("assets/images/sunTokenAnimation.png", "assets/images/sunTokenAnimation.png");
		type.set ("assets/images/sunTokenAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/systemStar.png", "assets/images/systemStar.png");
		type.set ("assets/images/systemStar.png", AssetType.IMAGE);
		path.set ("assets/images/systemStarAnimation.png", "assets/images/systemStarAnimation.png");
		type.set ("assets/images/systemStarAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/tileAnimation.png", "assets/images/tileAnimation.png");
		type.set ("assets/images/tileAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/tileAnimationTEST.png", "assets/images/tileAnimationTEST.png");
		type.set ("assets/images/tileAnimationTEST.png", AssetType.IMAGE);
		path.set ("assets/images/tileSprite.png", "assets/images/tileSprite.png");
		type.set ("assets/images/tileSprite.png", AssetType.IMAGE);
		path.set ("assets/images/titleBackground.png", "assets/images/titleBackground.png");
		type.set ("assets/images/titleBackground.png", AssetType.IMAGE);
		path.set ("assets/images/titleEasy.png", "assets/images/titleEasy.png");
		type.set ("assets/images/titleEasy.png", AssetType.IMAGE);
		path.set ("assets/images/titleImage.png", "assets/images/titleImage.png");
		type.set ("assets/images/titleImage.png", AssetType.IMAGE);
		path.set ("assets/images/titleImageProcessed.png", "assets/images/titleImageProcessed.png");
		type.set ("assets/images/titleImageProcessed.png", AssetType.IMAGE);
		path.set ("assets/images/titleScreenGrass.png", "assets/images/titleScreenGrass.png");
		type.set ("assets/images/titleScreenGrass.png", AssetType.IMAGE);
		path.set ("assets/images/treeAnim0.png", "assets/images/treeAnim0.png");
		type.set ("assets/images/treeAnim0.png", AssetType.IMAGE);
		path.set ("assets/images/treeAnim1.png", "assets/images/treeAnim1.png");
		type.set ("assets/images/treeAnim1.png", AssetType.IMAGE);
		path.set ("assets/images/treeAnim2.png", "assets/images/treeAnim2.png");
		type.set ("assets/images/treeAnim2.png", AssetType.IMAGE);
		path.set ("assets/images/treeAnim3.png", "assets/images/treeAnim3.png");
		type.set ("assets/images/treeAnim3.png", AssetType.IMAGE);
		path.set ("assets/images/treeAnim4.png", "assets/images/treeAnim4.png");
		type.set ("assets/images/treeAnim4.png", AssetType.IMAGE);
		path.set ("assets/images/treeAnim5.png", "assets/images/treeAnim5.png");
		type.set ("assets/images/treeAnim5.png", AssetType.IMAGE);
		path.set ("assets/images/treeAnim6.png", "assets/images/treeAnim6.png");
		type.set ("assets/images/treeAnim6.png", AssetType.IMAGE);
		path.set ("assets/images/treeAnim7.png", "assets/images/treeAnim7.png");
		type.set ("assets/images/treeAnim7.png", AssetType.IMAGE);
		path.set ("assets/images/treeAnimation.png", "assets/images/treeAnimation.png");
		type.set ("assets/images/treeAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/treeAnimationA.png", "assets/images/treeAnimationA.png");
		type.set ("assets/images/treeAnimationA.png", AssetType.IMAGE);
		path.set ("assets/images/treeAnimationB.png", "assets/images/treeAnimationB.png");
		type.set ("assets/images/treeAnimationB.png", AssetType.IMAGE);
		path.set ("assets/images/treeDeathAnimation.png", "assets/images/treeDeathAnimation.png");
		type.set ("assets/images/treeDeathAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/treeDeathAnimation97.png", "assets/images/treeDeathAnimation97.png");
		type.set ("assets/images/treeDeathAnimation97.png", AssetType.IMAGE);
		path.set ("assets/images/treeDeathAnimation98.png", "assets/images/treeDeathAnimation98.png");
		type.set ("assets/images/treeDeathAnimation98.png", AssetType.IMAGE);
		path.set ("assets/images/treeDeathAnimation99.png", "assets/images/treeDeathAnimation99.png");
		type.set ("assets/images/treeDeathAnimation99.png", AssetType.IMAGE);
		path.set ("assets/images/treeSpriteA.png", "assets/images/treeSpriteA.png");
		type.set ("assets/images/treeSpriteA.png", AssetType.IMAGE);
		path.set ("assets/images/treeSpriteA99.png", "assets/images/treeSpriteA99.png");
		type.set ("assets/images/treeSpriteA99.png", AssetType.IMAGE);
		path.set ("assets/images/tree_seed.png", "assets/images/tree_seed.png");
		type.set ("assets/images/tree_seed.png", AssetType.IMAGE);
		path.set ("assets/images/truckBellAnimation.png", "assets/images/truckBellAnimation.png");
		type.set ("assets/images/truckBellAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/truckBellImage.png", "assets/images/truckBellImage.png");
		type.set ("assets/images/truckBellImage.png", AssetType.IMAGE);
		path.set ("assets/images/tutCheckBoxAnimate.png", "assets/images/tutCheckBoxAnimate.png");
		type.set ("assets/images/tutCheckBoxAnimate.png", AssetType.IMAGE);
		path.set ("assets/images/tutHand.png", "assets/images/tutHand.png");
		type.set ("assets/images/tutHand.png", AssetType.IMAGE);
		path.set ("assets/images/tutHandAnimation.png", "assets/images/tutHandAnimation.png");
		type.set ("assets/images/tutHandAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/tutMessageEight.png", "assets/images/tutMessageEight.png");
		type.set ("assets/images/tutMessageEight.png", AssetType.IMAGE);
		path.set ("assets/images/tutMessageFive.png", "assets/images/tutMessageFive.png");
		type.set ("assets/images/tutMessageFive.png", AssetType.IMAGE);
		path.set ("assets/images/tutMessageFour.png", "assets/images/tutMessageFour.png");
		type.set ("assets/images/tutMessageFour.png", AssetType.IMAGE);
		path.set ("assets/images/tutMessageNine.png", "assets/images/tutMessageNine.png");
		type.set ("assets/images/tutMessageNine.png", AssetType.IMAGE);
		path.set ("assets/images/tutMessageOne.png", "assets/images/tutMessageOne.png");
		type.set ("assets/images/tutMessageOne.png", AssetType.IMAGE);
		path.set ("assets/images/tutMessageSeven.png", "assets/images/tutMessageSeven.png");
		type.set ("assets/images/tutMessageSeven.png", AssetType.IMAGE);
		path.set ("assets/images/tutMessageSix.png", "assets/images/tutMessageSix.png");
		type.set ("assets/images/tutMessageSix.png", AssetType.IMAGE);
		path.set ("assets/images/tutMessageTen.png", "assets/images/tutMessageTen.png");
		type.set ("assets/images/tutMessageTen.png", AssetType.IMAGE);
		path.set ("assets/images/tutMessageThree.png", "assets/images/tutMessageThree.png");
		type.set ("assets/images/tutMessageThree.png", AssetType.IMAGE);
		path.set ("assets/images/tutMessageTwo.png", "assets/images/tutMessageTwo.png");
		type.set ("assets/images/tutMessageTwo.png", AssetType.IMAGE);
		path.set ("assets/images/upsideDownIsland.png", "assets/images/upsideDownIsland.png");
		type.set ("assets/images/upsideDownIsland.png", AssetType.IMAGE);
		path.set ("assets/images/waterfallAnimation.png", "assets/images/waterfallAnimation.png");
		type.set ("assets/images/waterfallAnimation.png", AssetType.IMAGE);
		path.set ("assets/images/waterfallImage.png", "assets/images/waterfallImage.png");
		type.set ("assets/images/waterfallImage.png", AssetType.IMAGE);
		path.set ("assets/images/waterFallTile.png", "assets/images/waterFallTile.png");
		type.set ("assets/images/waterFallTile.png", AssetType.IMAGE);
		path.set ("assets/images/weedA.png", "assets/images/weedA.png");
		type.set ("assets/images/weedA.png", AssetType.IMAGE);
		path.set ("assets/images/weedB.png", "assets/images/weedB.png");
		type.set ("assets/images/weedB.png", AssetType.IMAGE);
		path.set ("assets/music/BeppleSongONe.wav", "assets/music/BeppleSongONe.wav");
		type.set ("assets/music/BeppleSongONe.wav", AssetType.SOUND);
		path.set ("assets/music/BeppleSongOneOGG.ogg", "assets/music/BeppleSongOneOGG.ogg");
		type.set ("assets/music/BeppleSongOneOGG.ogg", AssetType.SOUND);
		path.set ("assets/music/music-goes-here.txt", "assets/music/music-goes-here.txt");
		type.set ("assets/music/music-goes-here.txt", AssetType.TEXT);
		path.set ("assets/sounds/alertButtonSound.wav", "assets/sounds/alertButtonSound.wav");
		type.set ("assets/sounds/alertButtonSound.wav", AssetType.SOUND);
		path.set ("assets/sounds/buySignSound.wav", "assets/sounds/buySignSound.wav");
		type.set ("assets/sounds/buySignSound.wav", AssetType.SOUND);
		path.set ("assets/sounds/calcButtonSound.wav", "assets/sounds/calcButtonSound.wav");
		type.set ("assets/sounds/calcButtonSound.wav", AssetType.SOUND);
		path.set ("assets/sounds/cashRegisterSound.wav", "assets/sounds/cashRegisterSound.wav");
		type.set ("assets/sounds/cashRegisterSound.wav", AssetType.SOUND);
		path.set ("assets/sounds/coinsSound.wav", "assets/sounds/coinsSound.wav");
		type.set ("assets/sounds/coinsSound.wav", AssetType.SOUND);
		path.set ("assets/sounds/correctAnswerSoundA.wav", "assets/sounds/correctAnswerSoundA.wav");
		type.set ("assets/sounds/correctAnswerSoundA.wav", AssetType.SOUND);
		path.set ("assets/sounds/fallenAppleSound.wav", "assets/sounds/fallenAppleSound.wav");
		type.set ("assets/sounds/fallenAppleSound.wav", AssetType.SOUND);
		path.set ("assets/sounds/fillingJuiceBottleSound.wav", "assets/sounds/fillingJuiceBottleSound.wav");
		type.set ("assets/sounds/fillingJuiceBottleSound.wav", AssetType.SOUND);
		path.set ("assets/sounds/greenHouseWindChimeSound.wav", "assets/sounds/greenHouseWindChimeSound.wav");
		type.set ("assets/sounds/greenHouseWindChimeSound.wav", AssetType.SOUND);
		path.set ("assets/sounds/growingTree.wav", "assets/sounds/growingTree.wav");
		type.set ("assets/sounds/growingTree.wav", AssetType.SOUND);
		path.set ("assets/sounds/growingTreeA.wav", "assets/sounds/growingTreeA.wav");
		type.set ("assets/sounds/growingTreeA.wav", AssetType.SOUND);
		path.set ("assets/sounds/growingTreeB.wav", "assets/sounds/growingTreeB.wav");
		type.set ("assets/sounds/growingTreeB.wav", AssetType.SOUND);
		path.set ("assets/sounds/growingTreeC.wav", "assets/sounds/growingTreeC.wav");
		type.set ("assets/sounds/growingTreeC.wav", AssetType.SOUND);
		path.set ("assets/sounds/growingTreeD.wav", "assets/sounds/growingTreeD.wav");
		type.set ("assets/sounds/growingTreeD.wav", AssetType.SOUND);
		path.set ("assets/sounds/incorrectAnswerSound.wav", "assets/sounds/incorrectAnswerSound.wav");
		type.set ("assets/sounds/incorrectAnswerSound.wav", AssetType.SOUND);
		path.set ("assets/sounds/juiceMachineSound.wav", "assets/sounds/juiceMachineSound.wav");
		type.set ("assets/sounds/juiceMachineSound.wav", AssetType.SOUND);
		path.set ("assets/sounds/koiClicked0.wav", "assets/sounds/koiClicked0.wav");
		type.set ("assets/sounds/koiClicked0.wav", AssetType.SOUND);
		path.set ("assets/sounds/koiClicked1.wav", "assets/sounds/koiClicked1.wav");
		type.set ("assets/sounds/koiClicked1.wav", AssetType.SOUND);
		path.set ("assets/sounds/koiClicked2.wav", "assets/sounds/koiClicked2.wav");
		type.set ("assets/sounds/koiClicked2.wav", AssetType.SOUND);
		path.set ("assets/sounds/koiClicked4.wav", "assets/sounds/koiClicked4.wav");
		type.set ("assets/sounds/koiClicked4.wav", AssetType.SOUND);
		path.set ("assets/sounds/koiPondPlacedSound.wav", "assets/sounds/koiPondPlacedSound.wav");
		type.set ("assets/sounds/koiPondPlacedSound.wav", AssetType.SOUND);
		path.set ("assets/sounds/koiPondSound.wav", "assets/sounds/koiPondSound.wav");
		type.set ("assets/sounds/koiPondSound.wav", AssetType.SOUND);
		path.set ("assets/sounds/marketSlideClosedSound.wav", "assets/sounds/marketSlideClosedSound.wav");
		type.set ("assets/sounds/marketSlideClosedSound.wav", AssetType.SOUND);
		path.set ("assets/sounds/marketSlideOpenSound.wav", "assets/sounds/marketSlideOpenSound.wav");
		type.set ("assets/sounds/marketSlideOpenSound.wav", AssetType.SOUND);
		path.set ("assets/sounds/mouseHoleSound.wav", "assets/sounds/mouseHoleSound.wav");
		type.set ("assets/sounds/mouseHoleSound.wav", AssetType.SOUND);
		path.set ("assets/sounds/mouseWalkingSound.wav", "assets/sounds/mouseWalkingSound.wav");
		type.set ("assets/sounds/mouseWalkingSound.wav", AssetType.SOUND);
		path.set ("assets/sounds/pickedApplesGround.wav", "assets/sounds/pickedApplesGround.wav");
		type.set ("assets/sounds/pickedApplesGround.wav", AssetType.SOUND);
		path.set ("assets/sounds/pickedLeavesSound.wav", "assets/sounds/pickedLeavesSound.wav");
		type.set ("assets/sounds/pickedLeavesSound.wav", AssetType.SOUND);
		path.set ("assets/sounds/pieBakedSound.wav", "assets/sounds/pieBakedSound.wav");
		type.set ("assets/sounds/pieBakedSound.wav", AssetType.SOUND);
		path.set ("assets/sounds/placedMachineSound.wav", "assets/sounds/placedMachineSound.wav");
		type.set ("assets/sounds/placedMachineSound.wav", AssetType.SOUND);
		path.set ("assets/sounds/plantingSeedSound.wav", "assets/sounds/plantingSeedSound.wav");
		type.set ("assets/sounds/plantingSeedSound.wav", AssetType.SOUND);
		path.set ("assets/sounds/pouringJuiceSound.wav", "assets/sounds/pouringJuiceSound.wav");
		type.set ("assets/sounds/pouringJuiceSound.wav", AssetType.SOUND);
		path.set ("assets/sounds/raining.wav", "assets/sounds/raining.wav");
		type.set ("assets/sounds/raining.wav", AssetType.SOUND);
		path.set ("assets/sounds/sounds-go-here.txt", "assets/sounds/sounds-go-here.txt");
		type.set ("assets/sounds/sounds-go-here.txt", AssetType.TEXT);
		path.set ("assets/sounds/sprinklerButtonSound.wav", "assets/sounds/sprinklerButtonSound.wav");
		type.set ("assets/sounds/sprinklerButtonSound.wav", AssetType.SOUND);
		path.set ("assets/sounds/sprinklerSound.wav", "assets/sounds/sprinklerSound.wav");
		type.set ("assets/sounds/sprinklerSound.wav", AssetType.SOUND);
		path.set ("assets/sounds/sunshine.wav", "assets/sounds/sunshine.wav");
		type.set ("assets/sounds/sunshine.wav", AssetType.SOUND);
		path.set ("assets/sounds/truckButtonSound.wav", "assets/sounds/truckButtonSound.wav");
		type.set ("assets/sounds/truckButtonSound.wav", AssetType.SOUND);
		path.set ("assets/sounds/truckSound.wav", "assets/sounds/truckSound.wav");
		type.set ("assets/sounds/truckSound.wav", AssetType.SOUND);
		path.set ("assets/sounds/beep.ogg", "assets/sounds/beep.ogg");
		type.set ("assets/sounds/beep.ogg", AssetType.SOUND);
		path.set ("assets/sounds/flixel.ogg", "assets/sounds/flixel.ogg");
		type.set ("assets/sounds/flixel.ogg", AssetType.SOUND);
		
		
		#elseif html5
		
		var id;
		id = "assets/data/data-goes-here.txt";
		path.set (id, id);
		type.set (id, AssetType.TEXT);
		id = "assets/images/animatedAppleA.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/animatedAppleB.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/answerButton.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/appleJuiceItemImage.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/appleRotAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/appleShedAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/appleTruck.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/appleTruckButtonOnAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassA.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassAmaster.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassB.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassBmaster.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassC.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassCmaster.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassD.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassDmaster.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassE.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassEmaster.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassF.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassFmaster.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassFMouse.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassG.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassGmaster.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassH.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/backGroundMassHmaster.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/basketAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/blackBoard.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/blackBoardCorrectBorder.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/blackBoardWrongBorder.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/boilerImage.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/bridgeImage.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buildBar.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buildBarShaft.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buildBarTab.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buttonClear.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buttonEight.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buttonFive.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buttonFour.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buttonNine.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buttonOne.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buttonSeven.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buttonSix.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buttonThree.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buttonTwo.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buttonZero.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buttonZeroShadow.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/buyLandButton.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/calcImage.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/chalkEquals.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/chalkNumbers_0.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/chalkNumbers_1.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/chalkNumbers_2.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/chalkNumbers_3.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/chalkNumbers_4.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/chalkNumbers_5.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/chalkNumbers_6.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/chalkNumbers_7.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/chalkNumbers_8.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/chalkNumbers_9.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/cloud_blue.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/coyPondImage.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/dropletsAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/dropletsAnimationS.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/dropletsAnimationT.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/dropletsAnimationU.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/fillingBottleAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/fishIcon.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/fishItemAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/gameTile.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/gameTileBoarder.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/gameTileOwned.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/goldCoin.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/greenHouseAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/greenHouseIconImage.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/GreenHouseItemAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/greenHouseShrub.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/greenHouseTree.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/greenHouseTreeAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/holeAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/images-go-here.txt";
		path.set (id, id);
		type.set (id, AssetType.TEXT);
		id = "assets/images/islandMouseAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/juiceMachineAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/juiceMachineIcon.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/juiceMachineSwitchAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/koiFishAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/koiFishItemDrag.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/koiPad.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/koiPondAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/koiPondImage.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/koiRock.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/koiSand.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/marketSideBar.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/marketSideBarShaft.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/marketSideBarTab.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/market_seed.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/meteorField.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/meteorFieldMaster.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/minusOneImage.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/minusOperation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/mouseAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/multiOperation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/optionsScreen.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/packOfJuiceIcon.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/packOfPiesIcon.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/pickedAppleLeaves.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/pieIcon.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/pieMachineAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/pieMachineItem.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/plusOperation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/rainCloudAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/rainCloudAnimationA.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/repairAnimationS.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/rockyForeground.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/rollingMoon.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/seedItemAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/sidewaysIsland.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/spaceBackground.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/sprinklerAlertAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/sprinklerAlertAnimationS.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/sprinklerAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/sprinklerSwtichAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/sunAnimationA.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/sunAnimationB.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/sunAnimationC.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/sunTokenAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/systemStar.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/systemStarAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tileAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tileAnimationTEST.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tileSprite.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/titleBackground.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/titleEasy.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/titleImage.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/titleImageProcessed.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/titleScreenGrass.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeAnim0.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeAnim1.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeAnim2.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeAnim3.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeAnim4.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeAnim5.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeAnim6.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeAnim7.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeAnimationA.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeAnimationB.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeDeathAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeDeathAnimation97.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeDeathAnimation98.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeDeathAnimation99.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeSpriteA.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/treeSpriteA99.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tree_seed.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/truckBellAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/truckBellImage.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tutCheckBoxAnimate.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tutHand.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tutHandAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tutMessageEight.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tutMessageFive.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tutMessageFour.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tutMessageNine.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tutMessageOne.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tutMessageSeven.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tutMessageSix.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tutMessageTen.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tutMessageThree.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/tutMessageTwo.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/upsideDownIsland.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/waterfallAnimation.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/waterfallImage.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/waterFallTile.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/weedA.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/weedB.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/music/BeppleSongONe.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/music/BeppleSongOneOGG.ogg";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/music/music-goes-here.txt";
		path.set (id, id);
		type.set (id, AssetType.TEXT);
		id = "assets/sounds/alertButtonSound.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/buySignSound.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/calcButtonSound.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/cashRegisterSound.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/coinsSound.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/correctAnswerSoundA.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/fallenAppleSound.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/fillingJuiceBottleSound.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/greenHouseWindChimeSound.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/growingTree.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/growingTreeA.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/growingTreeB.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/growingTreeC.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/growingTreeD.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/incorrectAnswerSound.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/juiceMachineSound.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/koiClicked0.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/koiClicked1.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/koiClicked2.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/koiClicked4.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/koiPondPlacedSound.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/koiPondSound.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/marketSlideClosedSound.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/marketSlideOpenSound.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/mouseHoleSound.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/mouseWalkingSound.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/pickedApplesGround.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/pickedLeavesSound.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/pieBakedSound.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/placedMachineSound.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/plantingSeedSound.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/pouringJuiceSound.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/raining.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/sounds-go-here.txt";
		path.set (id, id);
		type.set (id, AssetType.TEXT);
		id = "assets/sounds/sprinklerButtonSound.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/sprinklerSound.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/sunshine.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/truckButtonSound.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/truckSound.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/beep.ogg";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/flixel.ogg";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		
		
		#else
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		#if (windows || mac || linux)
		
		var useManifest = false;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		useManifest = true;
		
		
		if (useManifest) {
			
			loadManifest ();
			
			if (Sys.args ().indexOf ("-livereload") > -1) {
				
				var path = FileSystem.fullPath ("manifest");
				lastModified = FileSystem.stat (path).mtime.getTime ();
				
				timer = new Timer (2000);
				timer.run = function () {
					
					var modified = FileSystem.stat (path).mtime.getTime ();
					
					if (modified > lastModified) {
						
						lastModified = modified;
						loadManifest ();
						
						if (eventCallback != null) {
							
							eventCallback (this, "change");
							
						}
						
					}
					
				}
				
			}
			
		}
		
		#else
		
		loadManifest ();
		
		#end
		#end
		
	}
	
	
	public override function exists (id:String, type:AssetType):Bool {
		
		var assetType = this.type.get (id);
		
		#if pixi
		
		if (assetType == IMAGE) {
			
			return true;
			
		} else {
			
			return false;
			
		}
		
		#end
		
		if (assetType != null) {
			
			if (assetType == type || ((type == SOUND || type == MUSIC) && (assetType == MUSIC || assetType == SOUND))) {
				
				return true;
				
			}
			
			#if flash
			
			if ((assetType == BINARY || assetType == TEXT) && type == BINARY) {
				
				return true;
				
			} else if (path.exists (id)) {
				
				return true;
				
			}
			
			#else
			
			if (type == BINARY || type == null || (assetType == BINARY && type == TEXT)) {
				
				return true;
				
			}
			
			#end
			
		}
		
		return false;
		
	}
	
	
	public override function getBitmapData (id:String):BitmapData {
		
		#if pixi
		
		return BitmapData.fromImage (path.get (id));
		
		#elseif (flash)
		
		return cast (Type.createInstance (className.get (id), []), BitmapData);
		
		#elseif openfl_html5
		
		return BitmapData.fromImage (ApplicationMain.images.get (path.get (id)));
		
		#elseif js
		
		return cast (ApplicationMain.loaders.get (path.get (id)).contentLoaderInfo.content, Bitmap).bitmapData;
		
		#else
		
		if (className.exists(id)) return cast (Type.createInstance (className.get (id), []), BitmapData);
		else return BitmapData.load (path.get (id));
		
		#end
		
	}
	
	
	public override function getBytes (id:String):ByteArray {
		
		#if (flash)
		
		return cast (Type.createInstance (className.get (id), []), ByteArray);

		#elseif (js || openfl_html5 || pixi)
		
		var bytes:ByteArray = null;
		var data = ApplicationMain.urlLoaders.get (path.get (id)).data;
		
		if (Std.is (data, String)) {
			
			bytes = new ByteArray ();
			bytes.writeUTFBytes (data);
			
		} else if (Std.is (data, ByteArray)) {
			
			bytes = cast data;
			
		} else {
			
			bytes = null;
			
		}

		if (bytes != null) {
			
			bytes.position = 0;
			return bytes;
			
		} else {
			
			return null;
		}
		
		#else
		
		if (className.exists(id)) return cast (Type.createInstance (className.get (id), []), ByteArray);
		else return ByteArray.readFile (path.get (id));
		
		#end
		
	}
	
	
	public override function getFont (id:String):Font {
		
		#if pixi
		
		return null;
		
		#elseif (flash || js)
		
		return cast (Type.createInstance (className.get (id), []), Font);
		
		#else
		
		if (className.exists(id)) {
			var fontClass = className.get(id);
			Font.registerFont(fontClass);
			return cast (Type.createInstance (fontClass, []), Font);
		} else return new Font (path.get (id));
		
		#end
		
	}
	
	
	public override function getMusic (id:String):Sound {
		
		#if pixi
		
		return null;
		
		#elseif (flash)
		
		return cast (Type.createInstance (className.get (id), []), Sound);
		
		#elseif openfl_html5
		
		var sound = new Sound ();
		sound.__buffer = true;
		sound.load (new URLRequest (path.get (id)));
		return sound; 
		
		#elseif js
		
		return new Sound (new URLRequest (path.get (id)));
		
		#else
		
		if (className.exists(id)) return cast (Type.createInstance (className.get (id), []), Sound);
		else return new Sound (new URLRequest (path.get (id)), null, true);
		
		#end
		
	}
	
	
	public override function getPath (id:String):String {
		
		#if ios
		
		return SystemPath.applicationDirectory + "/assets/" + path.get (id);
		
		#else
		
		return path.get (id);
		
		#end
		
	}
	
	
	public override function getSound (id:String):Sound {
		
		#if pixi
		
		return null;
		
		#elseif (flash)
		
		return cast (Type.createInstance (className.get (id), []), Sound);
		
		#elseif js
		
		return new Sound (new URLRequest (path.get (id)));
		
		#else
		
		if (className.exists(id)) return cast (Type.createInstance (className.get (id), []), Sound);
		else return new Sound (new URLRequest (path.get (id)), null, type.get (id) == MUSIC);
		
		#end
		
	}
	
	
	public override function getText (id:String):String {
		
		#if js
		
		var bytes:ByteArray = null;
		var data = ApplicationMain.urlLoaders.get (path.get (id)).data;
		
		if (Std.is (data, String)) {
			
			return cast data;
			
		} else if (Std.is (data, ByteArray)) {
			
			bytes = cast data;
			
		} else {
			
			bytes = null;
			
		}
		
		if (bytes != null) {
			
			bytes.position = 0;
			return bytes.readUTFBytes (bytes.length);
			
		} else {
			
			return null;
		}
		
		#else
		
		var bytes = getBytes (id);
		
		if (bytes == null) {
			
			return null;
			
		} else {
			
			return bytes.readUTFBytes (bytes.length);
			
		}
		
		#end
		
	}
	
	
	public override function isLocal (id:String, type:AssetType):Bool {
		
		#if flash
		
		if (type != AssetType.MUSIC && type != AssetType.SOUND) {
			
			return className.exists (id);
			
		}
		
		#end
		
		return true;
		
	}
	
	
	public override function list (type:AssetType):Array<String> {
		
		var items = [];
		
		for (id in this.type.keys ()) {
			
			if (type == null || exists (id, type)) {
				
				items.push (id);
				
			}
			
		}
		
		return items;
		
	}
	
	
	public override function loadBitmapData (id:String, handler:BitmapData -> Void):Void {
		
		#if pixi
		
		handler (getBitmapData (id));
		
		#elseif (flash || js)
		
		if (path.exists (id)) {
			
			var loader = new Loader ();
			loader.contentLoaderInfo.addEventListener (Event.COMPLETE, function (event:Event) {
				
				handler (cast (event.currentTarget.content, Bitmap).bitmapData);
				
			});
			loader.load (new URLRequest (path.get (id)));
			
		} else {
			
			handler (getBitmapData (id));
			
		}
		
		#else
		
		handler (getBitmapData (id));
		
		#end
		
	}
	
	
	public override function loadBytes (id:String, handler:ByteArray -> Void):Void {
		
		#if pixi
		
		handler (getBytes (id));
		
		#elseif (flash || js)
		
		if (path.exists (id)) {
			
			var loader = new URLLoader ();
			loader.addEventListener (Event.COMPLETE, function (event:Event) {
				
				var bytes = new ByteArray ();
				bytes.writeUTFBytes (event.currentTarget.data);
				bytes.position = 0;
				
				handler (bytes);
				
			});
			loader.load (new URLRequest (path.get (id)));
			
		} else {
			
			handler (getBytes (id));
			
		}
		
		#else
		
		handler (getBytes (id));
		
		#end
		
	}
	
	
	public override function loadFont (id:String, handler:Font -> Void):Void {
		
		#if (flash || js)
		
		/*if (path.exists (id)) {
			
			var loader = new Loader ();
			loader.contentLoaderInfo.addEventListener (Event.COMPLETE, function (event) {
				
				handler (cast (event.currentTarget.content, Bitmap).bitmapData);
				
			});
			loader.load (new URLRequest (path.get (id)));
			
		} else {*/
			
			handler (getFont (id));
			
		//}
		
		#else
		
		handler (getFont (id));
		
		#end
		
	}
	
	
	#if (!flash && !html5)
	private function loadManifest ():Void {
		
		try {
			
			#if blackberry
			var bytes = ByteArray.readFile ("app/native/manifest");
			#elseif tizen
			var bytes = ByteArray.readFile ("../res/manifest");
			#elseif emscripten
			var bytes = ByteArray.readFile ("assets/manifest");
			#else
			var bytes = ByteArray.readFile ("manifest");
			#end
			
			if (bytes != null) {
				
				bytes.position = 0;
				
				if (bytes.length > 0) {
					
					var data = bytes.readUTFBytes (bytes.length);
					
					if (data != null && data.length > 0) {
						
						var manifest:Array<Dynamic> = Unserializer.run (data);
						
						for (asset in manifest) {
							
							if (!className.exists (asset.id)) {
								
								path.set (asset.id, asset.path);
								type.set (asset.id, Type.createEnum (AssetType, asset.type));
								
							}
							
						}
						
					}
					
				}
				
			} else {
				
				trace ("Warning: Could not load asset manifest (bytes was null)");
				
			}
		
		} catch (e:Dynamic) {
			
			trace ('Warning: Could not load asset manifest (${e})');
			
		}
		
	}
	#end
	
	
	public override function loadMusic (id:String, handler:Sound -> Void):Void {
		
		#if (flash || js)
		
		/*if (path.exists (id)) {
			
			var loader = new Loader ();
			loader.contentLoaderInfo.addEventListener (Event.COMPLETE, function (event) {
				
				handler (cast (event.currentTarget.content, Bitmap).bitmapData);
				
			});
			loader.load (new URLRequest (path.get (id)));
			
		} else {*/
			
			handler (getMusic (id));
			
		//}
		
		#else
		
		handler (getMusic (id));
		
		#end
		
	}
	
	
	public override function loadSound (id:String, handler:Sound -> Void):Void {
		
		#if (flash || js)
		
		/*if (path.exists (id)) {
			
			var loader = new Loader ();
			loader.contentLoaderInfo.addEventListener (Event.COMPLETE, function (event) {
				
				handler (cast (event.currentTarget.content, Bitmap).bitmapData);
				
			});
			loader.load (new URLRequest (path.get (id)));
			
		} else {*/
			
			handler (getSound (id));
			
		//}
		
		#else
		
		handler (getSound (id));
		
		#end
		
	}
	
	
	public override function loadText (id:String, handler:String -> Void):Void {
		
		#if js
		
		if (path.exists (id)) {
			
			var loader = new URLLoader ();
			loader.addEventListener (Event.COMPLETE, function (event:Event) {
				
				handler (event.currentTarget.data);
				
			});
			loader.load (new URLRequest (path.get (id)));
			
		} else {
			
			handler (getText (id));
			
		}
		
		#else
		
		var callback = function (bytes:ByteArray):Void {
			
			if (bytes == null) {
				
				handler (null);
				
			} else {
				
				handler (bytes.readUTFBytes (bytes.length));
				
			}
			
		}
		
		loadBytes (id, callback);
		
		#end
		
	}
	
	
}


#if pixi
#elseif flash






























































































































































































































#elseif html5






























































































































































































































#else

#if (windows || mac || linux)







#else




#end

#end
