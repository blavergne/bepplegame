#include <hxcpp.h>

#ifndef INCLUDED_MarketItem
#include <MarketItem.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_text_FlxText
#include <flixel/text/FlxText.h>
#endif

Void MarketItem_obj::__construct()
{
HX_STACK_FRAME("MarketItem","new",0xfd9ccd81,"MarketItem.new","MarketItem.hx",21,0x77611d0f)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(21)
	super::__construct(null(),null(),null());
}
;
	return null();
}

//MarketItem_obj::~MarketItem_obj() { }

Dynamic MarketItem_obj::__CreateEmpty() { return  new MarketItem_obj; }
hx::ObjectPtr< MarketItem_obj > MarketItem_obj::__new()
{  hx::ObjectPtr< MarketItem_obj > result = new MarketItem_obj();
	result->__construct();
	return result;}

Dynamic MarketItem_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< MarketItem_obj > result = new MarketItem_obj();
	result->__construct();
	return result;}

::flixel::FlxSprite MarketItem_obj::getItemSprite( ){
	HX_STACK_FRAME("MarketItem","getItemSprite",0xe209120f,"MarketItem.getItemSprite","MarketItem.hx",26,0x77611d0f)
	HX_STACK_THIS(this)
	HX_STACK_LINE(26)
	return this->itemSprite;
}


HX_DEFINE_DYNAMIC_FUNC0(MarketItem_obj,getItemSprite,return )

int MarketItem_obj::getItemCost( ){
	HX_STACK_FRAME("MarketItem","getItemCost",0x0fe17dd7,"MarketItem.getItemCost","MarketItem.hx",30,0x77611d0f)
	HX_STACK_THIS(this)
	HX_STACK_LINE(30)
	return this->itemCost;
}


HX_DEFINE_DYNAMIC_FUNC0(MarketItem_obj,getItemCost,return )

::String MarketItem_obj::getItemName( ){
	HX_STACK_FRAME("MarketItem","getItemName",0x171c31d5,"MarketItem.getItemName","MarketItem.hx",34,0x77611d0f)
	HX_STACK_THIS(this)
	HX_STACK_LINE(34)
	return this->itemName;
}


HX_DEFINE_DYNAMIC_FUNC0(MarketItem_obj,getItemName,return )

int MarketItem_obj::getItemCount( ){
	HX_STACK_FRAME("MarketItem","getItemCount",0xd56e1e05,"MarketItem.getItemCount","MarketItem.hx",38,0x77611d0f)
	HX_STACK_THIS(this)
	HX_STACK_LINE(38)
	return this->itemCount;
}


HX_DEFINE_DYNAMIC_FUNC0(MarketItem_obj,getItemCount,return )

bool MarketItem_obj::getItemClicked( ){
	HX_STACK_FRAME("MarketItem","getItemClicked",0x8088895d,"MarketItem.getItemClicked","MarketItem.hx",42,0x77611d0f)
	HX_STACK_THIS(this)
	HX_STACK_LINE(42)
	return this->itemClicked;
}


HX_DEFINE_DYNAMIC_FUNC0(MarketItem_obj,getItemClicked,return )

Void MarketItem_obj::decItemCost( ){
{
		HX_STACK_FRAME("MarketItem","decItemCost",0x8b3e4303,"MarketItem.decItemCost","MarketItem.hx",46,0x77611d0f)
		HX_STACK_THIS(this)
		HX_STACK_LINE(46)
		(this->itemCost)--;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(MarketItem_obj,decItemCost,(void))

Void MarketItem_obj::incItemCost( ){
{
		HX_STACK_FRAME("MarketItem","incItemCost",0xd234921f,"MarketItem.incItemCost","MarketItem.hx",50,0x77611d0f)
		HX_STACK_THIS(this)
		HX_STACK_LINE(50)
		(this->itemCost)++;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(MarketItem_obj,incItemCost,(void))

Void MarketItem_obj::decItemCount( ){
{
		HX_STACK_FRAME("MarketItem","decItemCount",0x4b3ddf59,"MarketItem.decItemCount","MarketItem.hx",54,0x77611d0f)
		HX_STACK_THIS(this)
		HX_STACK_LINE(54)
		(this->itemCount)--;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(MarketItem_obj,decItemCount,(void))

Void MarketItem_obj::incItemCount( ){
{
		HX_STACK_FRAME("MarketItem","incItemCount",0x1bccc8bd,"MarketItem.incItemCount","MarketItem.hx",58,0x77611d0f)
		HX_STACK_THIS(this)
		HX_STACK_LINE(58)
		(this->itemCount)++;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(MarketItem_obj,incItemCount,(void))

Void MarketItem_obj::setItemAvailable( bool available){
{
		HX_STACK_FRAME("MarketItem","setItemAvailable",0xb153f373,"MarketItem.setItemAvailable","MarketItem.hx",62,0x77611d0f)
		HX_STACK_THIS(this)
		HX_STACK_ARG(available,"available")
		HX_STACK_LINE(62)
		this->itemAvailable = available;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(MarketItem_obj,setItemAvailable,(void))

bool MarketItem_obj::getItemAvailable( ){
	HX_STACK_FRAME("MarketItem","getItemAvailable",0x5b1205ff,"MarketItem.getItemAvailable","MarketItem.hx",66,0x77611d0f)
	HX_STACK_THIS(this)
	HX_STACK_LINE(66)
	return this->itemAvailable;
}


HX_DEFINE_DYNAMIC_FUNC0(MarketItem_obj,getItemAvailable,return )

Void MarketItem_obj::setAvailableAnimation( ){
{
		HX_STACK_FRAME("MarketItem","setAvailableAnimation",0x01689d9e,"MarketItem.setAvailableAnimation","MarketItem.hx",70,0x77611d0f)
		HX_STACK_THIS(this)
		HX_STACK_LINE(70)
		if ((this->itemAvailable)){
			HX_STACK_LINE(71)
			this->itemSprite->animation->play(HX_CSTRING("available"),null(),null());
		}
		else{
			HX_STACK_LINE(74)
			this->itemSprite->animation->play(HX_CSTRING("notAvailable"),null(),null());
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(MarketItem_obj,setAvailableAnimation,(void))


MarketItem_obj::MarketItem_obj()
{
}

void MarketItem_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(MarketItem);
	HX_MARK_MEMBER_NAME(itemSprite,"itemSprite");
	HX_MARK_MEMBER_NAME(itemName,"itemName");
	HX_MARK_MEMBER_NAME(itemCost,"itemCost");
	HX_MARK_MEMBER_NAME(itemCount,"itemCount");
	HX_MARK_MEMBER_NAME(itemAvailable,"itemAvailable");
	HX_MARK_MEMBER_NAME(itemClicked,"itemClicked");
	HX_MARK_MEMBER_NAME(itemCountFieldText,"itemCountFieldText");
	::flixel::FlxSprite_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void MarketItem_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(itemSprite,"itemSprite");
	HX_VISIT_MEMBER_NAME(itemName,"itemName");
	HX_VISIT_MEMBER_NAME(itemCost,"itemCost");
	HX_VISIT_MEMBER_NAME(itemCount,"itemCount");
	HX_VISIT_MEMBER_NAME(itemAvailable,"itemAvailable");
	HX_VISIT_MEMBER_NAME(itemClicked,"itemClicked");
	HX_VISIT_MEMBER_NAME(itemCountFieldText,"itemCountFieldText");
	::flixel::FlxSprite_obj::__Visit(HX_VISIT_ARG);
}

Dynamic MarketItem_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 8:
		if (HX_FIELD_EQ(inName,"itemName") ) { return itemName; }
		if (HX_FIELD_EQ(inName,"itemCost") ) { return itemCost; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"itemCount") ) { return itemCount; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"itemSprite") ) { return itemSprite; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"itemClicked") ) { return itemClicked; }
		if (HX_FIELD_EQ(inName,"getItemCost") ) { return getItemCost_dyn(); }
		if (HX_FIELD_EQ(inName,"getItemName") ) { return getItemName_dyn(); }
		if (HX_FIELD_EQ(inName,"decItemCost") ) { return decItemCost_dyn(); }
		if (HX_FIELD_EQ(inName,"incItemCost") ) { return incItemCost_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"getItemCount") ) { return getItemCount_dyn(); }
		if (HX_FIELD_EQ(inName,"decItemCount") ) { return decItemCount_dyn(); }
		if (HX_FIELD_EQ(inName,"incItemCount") ) { return incItemCount_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"itemAvailable") ) { return itemAvailable; }
		if (HX_FIELD_EQ(inName,"getItemSprite") ) { return getItemSprite_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"getItemClicked") ) { return getItemClicked_dyn(); }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"setItemAvailable") ) { return setItemAvailable_dyn(); }
		if (HX_FIELD_EQ(inName,"getItemAvailable") ) { return getItemAvailable_dyn(); }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"itemCountFieldText") ) { return itemCountFieldText; }
		break;
	case 21:
		if (HX_FIELD_EQ(inName,"setAvailableAnimation") ) { return setAvailableAnimation_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic MarketItem_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 8:
		if (HX_FIELD_EQ(inName,"itemName") ) { itemName=inValue.Cast< ::String >(); return inValue; }
		if (HX_FIELD_EQ(inName,"itemCost") ) { itemCost=inValue.Cast< int >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"itemCount") ) { itemCount=inValue.Cast< int >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"itemSprite") ) { itemSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"itemClicked") ) { itemClicked=inValue.Cast< bool >(); return inValue; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"itemAvailable") ) { itemAvailable=inValue.Cast< bool >(); return inValue; }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"itemCountFieldText") ) { itemCountFieldText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void MarketItem_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("itemSprite"));
	outFields->push(HX_CSTRING("itemName"));
	outFields->push(HX_CSTRING("itemCost"));
	outFields->push(HX_CSTRING("itemCount"));
	outFields->push(HX_CSTRING("itemAvailable"));
	outFields->push(HX_CSTRING("itemClicked"));
	outFields->push(HX_CSTRING("itemCountFieldText"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(MarketItem_obj,itemSprite),HX_CSTRING("itemSprite")},
	{hx::fsString,(int)offsetof(MarketItem_obj,itemName),HX_CSTRING("itemName")},
	{hx::fsInt,(int)offsetof(MarketItem_obj,itemCost),HX_CSTRING("itemCost")},
	{hx::fsInt,(int)offsetof(MarketItem_obj,itemCount),HX_CSTRING("itemCount")},
	{hx::fsBool,(int)offsetof(MarketItem_obj,itemAvailable),HX_CSTRING("itemAvailable")},
	{hx::fsBool,(int)offsetof(MarketItem_obj,itemClicked),HX_CSTRING("itemClicked")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(MarketItem_obj,itemCountFieldText),HX_CSTRING("itemCountFieldText")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("itemSprite"),
	HX_CSTRING("itemName"),
	HX_CSTRING("itemCost"),
	HX_CSTRING("itemCount"),
	HX_CSTRING("itemAvailable"),
	HX_CSTRING("itemClicked"),
	HX_CSTRING("itemCountFieldText"),
	HX_CSTRING("getItemSprite"),
	HX_CSTRING("getItemCost"),
	HX_CSTRING("getItemName"),
	HX_CSTRING("getItemCount"),
	HX_CSTRING("getItemClicked"),
	HX_CSTRING("decItemCost"),
	HX_CSTRING("incItemCost"),
	HX_CSTRING("decItemCount"),
	HX_CSTRING("incItemCount"),
	HX_CSTRING("setItemAvailable"),
	HX_CSTRING("getItemAvailable"),
	HX_CSTRING("setAvailableAnimation"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(MarketItem_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(MarketItem_obj::__mClass,"__mClass");
};

#endif

Class MarketItem_obj::__mClass;

void MarketItem_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("MarketItem"), hx::TCanCast< MarketItem_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void MarketItem_obj::__boot()
{
}

