#include <hxcpp.h>

#ifndef INCLUDED_SystemStar
#include <SystemStar.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif

Void SystemStar_obj::__construct()
{
HX_STACK_FRAME("SystemStar","new",0xa03bc3f3,"SystemStar.new","SystemStar.hx",15,0x4c7c3add)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(16)
	super::__construct(null());
	HX_STACK_LINE(17)
	this->init();
}
;
	return null();
}

//SystemStar_obj::~SystemStar_obj() { }

Dynamic SystemStar_obj::__CreateEmpty() { return  new SystemStar_obj; }
hx::ObjectPtr< SystemStar_obj > SystemStar_obj::__new()
{  hx::ObjectPtr< SystemStar_obj > result = new SystemStar_obj();
	result->__construct();
	return result;}

Dynamic SystemStar_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< SystemStar_obj > result = new SystemStar_obj();
	result->__construct();
	return result;}

Void SystemStar_obj::init( ){
{
		HX_STACK_FRAME("SystemStar","init",0x90c8681d,"SystemStar.init","SystemStar.hx",21,0x4c7c3add)
		HX_STACK_THIS(this)
		HX_STACK_LINE(22)
		::flixel::group::FlxGroup _g = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(22)
		this->systemStarGroup = _g;
		HX_STACK_LINE(23)
		::flixel::FlxSprite _g1 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(23)
		this->systemStarSprite = _g1;
		HX_STACK_LINE(24)
		this->systemStarSprite->loadGraphic(HX_CSTRING("assets/images/systemStarAnimation.png"),true,(int)34,(int)34,null(),null());
		HX_STACK_LINE(25)
		this->systemStarSprite->animation->add(HX_CSTRING("starAnimation"),Array_obj< int >::__new().Add((int)0).Add((int)1).Add((int)2).Add((int)3).Add((int)4).Add((int)5).Add((int)6).Add((int)7).Add((int)8).Add((int)9).Add((int)10).Add((int)11).Add((int)12),(int)1,true);
		HX_STACK_LINE(26)
		this->systemStarSprite->animation->play(HX_CSTRING("starAnimation"),null(),null());
		HX_STACK_LINE(27)
		this->systemStarSprite->set_x((int)500);
		HX_STACK_LINE(28)
		this->systemStarSprite->set_y((int)40);
		HX_STACK_LINE(29)
		this->systemStarGroup->add(this->systemStarSprite);
		HX_STACK_LINE(30)
		this->add(this->systemStarGroup);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(SystemStar_obj,init,(void))


SystemStar_obj::SystemStar_obj()
{
}

void SystemStar_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(SystemStar);
	HX_MARK_MEMBER_NAME(systemStarGroup,"systemStarGroup");
	HX_MARK_MEMBER_NAME(systemStarSprite,"systemStarSprite");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void SystemStar_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(systemStarGroup,"systemStarGroup");
	HX_VISIT_MEMBER_NAME(systemStarSprite,"systemStarSprite");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic SystemStar_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"init") ) { return init_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"systemStarGroup") ) { return systemStarGroup; }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"systemStarSprite") ) { return systemStarSprite; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic SystemStar_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 15:
		if (HX_FIELD_EQ(inName,"systemStarGroup") ) { systemStarGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"systemStarSprite") ) { systemStarSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void SystemStar_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("systemStarGroup"));
	outFields->push(HX_CSTRING("systemStarSprite"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(SystemStar_obj,systemStarGroup),HX_CSTRING("systemStarGroup")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(SystemStar_obj,systemStarSprite),HX_CSTRING("systemStarSprite")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("systemStarGroup"),
	HX_CSTRING("systemStarSprite"),
	HX_CSTRING("init"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(SystemStar_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(SystemStar_obj::__mClass,"__mClass");
};

#endif

Class SystemStar_obj::__mClass;

void SystemStar_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("SystemStar"), hx::TCanCast< SystemStar_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void SystemStar_obj::__boot()
{
}

