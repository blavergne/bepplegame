#include <hxcpp.h>

#ifndef INCLUDED_AppleBasket
#include <AppleBasket.h>
#endif
#ifndef INCLUDED_AppleShed
#include <AppleShed.h>
#endif
#ifndef INCLUDED_AppleTruck
#include <AppleTruck.h>
#endif
#ifndef INCLUDED_AsteroidBelt
#include <AsteroidBelt.h>
#endif
#ifndef INCLUDED_Cloud
#include <Cloud.h>
#endif
#ifndef INCLUDED_Island
#include <Island.h>
#endif
#ifndef INCLUDED_JuiceIcon
#include <JuiceIcon.h>
#endif
#ifndef INCLUDED_JuiceMachine
#include <JuiceMachine.h>
#endif
#ifndef INCLUDED_JuicePackIcon
#include <JuicePackIcon.h>
#endif
#ifndef INCLUDED_Market
#include <Market.h>
#endif
#ifndef INCLUDED_MenuState
#include <MenuState.h>
#endif
#ifndef INCLUDED_NumberPad
#include <NumberPad.h>
#endif
#ifndef INCLUDED_OptionsScreen
#include <OptionsScreen.h>
#endif
#ifndef INCLUDED_PieIcon
#include <PieIcon.h>
#endif
#ifndef INCLUDED_PieMachine
#include <PieMachine.h>
#endif
#ifndef INCLUDED_PiePackIcon
#include <PiePackIcon.h>
#endif
#ifndef INCLUDED_PlayState
#include <PlayState.h>
#endif
#ifndef INCLUDED_ProblemBoard
#include <ProblemBoard.h>
#endif
#ifndef INCLUDED_Seed
#include <Seed.h>
#endif
#ifndef INCLUDED_Std
#include <Std.h>
#endif
#ifndef INCLUDED_SunToken
#include <SunToken.h>
#endif
#ifndef INCLUDED_SystemStar
#include <SystemStar.h>
#endif
#ifndef INCLUDED_Tile
#include <Tile.h>
#endif
#ifndef INCLUDED_TileBoard
#include <TileBoard.h>
#endif
#ifndef INCLUDED_TileBoarder
#include <TileBoarder.h>
#endif
#ifndef INCLUDED_Tree
#include <Tree.h>
#endif
#ifndef INCLUDED_TruckButton
#include <TruckButton.h>
#endif
#ifndef INCLUDED_Tutorial
#include <Tutorial.h>
#endif
#ifndef INCLUDED_Weed
#include <Weed.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxCamera
#include <flixel/FlxCamera.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_FlxState
#include <flixel/FlxState.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_input_mouse_FlxMouse
#include <flixel/input/mouse/FlxMouse.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxInput
#include <flixel/interfaces/IFlxInput.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxPooled
#include <flixel/interfaces/IFlxPooled.h>
#endif
#ifndef INCLUDED_flixel_plugin_FlxPlugin
#include <flixel/plugin/FlxPlugin.h>
#endif
#ifndef INCLUDED_flixel_plugin_MouseEventManager
#include <flixel/plugin/MouseEventManager.h>
#endif
#ifndef INCLUDED_flixel_system_FlxSound
#include <flixel/system/FlxSound.h>
#endif
#ifndef INCLUDED_flixel_system_frontEnds_SoundFrontEnd
#include <flixel/system/frontEnds/SoundFrontEnd.h>
#endif
#ifndef INCLUDED_flixel_text_FlxText
#include <flixel/text/FlxText.h>
#endif
#ifndef INCLUDED_flixel_ui_FlxButton
#include <flixel/ui/FlxButton.h>
#endif
#ifndef INCLUDED_flixel_ui_FlxTypedButton
#include <flixel/ui/FlxTypedButton.h>
#endif
#ifndef INCLUDED_flixel_util_FlxCollision
#include <flixel/util/FlxCollision.h>
#endif
#ifndef INCLUDED_flixel_util_FlxDestroyUtil
#include <flixel/util/FlxDestroyUtil.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPoint
#include <flixel/util/FlxPoint.h>
#endif
#ifndef INCLUDED_flixel_util_FlxRandom
#include <flixel/util/FlxRandom.h>
#endif
#ifndef INCLUDED_flixel_util_FlxTimer
#include <flixel/util/FlxTimer.h>
#endif
#ifndef INCLUDED_haxe_Log
#include <haxe/Log.h>
#endif
#ifndef INCLUDED_openfl__v2_events_EventDispatcher
#include <openfl/_v2/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__v2_events_IEventDispatcher
#include <openfl/_v2/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__v2_media_SoundChannel
#include <openfl/_v2/media/SoundChannel.h>
#endif

Void PlayState_obj::__construct(Dynamic MaxSize)
{
HX_STACK_FRAME("PlayState","new",0xf8bf96cf,"PlayState.new","PlayState.hx",29,0xb30d7781)
HX_STACK_THIS(this)
HX_STACK_ARG(MaxSize,"MaxSize")
{
	HX_STACK_LINE(114)
	this->tutUnpauseAlertsDone = false;
	HX_STACK_LINE(113)
	this->closeSecondTreeGrowthCheck = false;
	HX_STACK_LINE(112)
	this->tutorialWrongAnswersSkip = true;
	HX_STACK_LINE(111)
	this->callBackStopTwo = false;
	HX_STACK_LINE(110)
	this->callBackStopOne = true;
	HX_STACK_LINE(109)
	this->truckTutSection = true;
	HX_STACK_LINE(107)
	this->marketOpenCheck = true;
	HX_STACK_LINE(106)
	this->closeAppleCountCheck = false;
	HX_STACK_LINE(105)
	this->closeTreeGrowthCheck = false;
	HX_STACK_LINE(104)
	this->ownedTile = false;
	HX_STACK_LINE(103)
	this->dropletClicked = false;
	HX_STACK_LINE(102)
	this->sunClicked = false;
	HX_STACK_LINE(101)
	this->overlaysInUse = false;
	HX_STACK_LINE(71)
	this->appleDisplayLock = true;
	HX_STACK_LINE(70)
	this->boardIsDown = false;
	HX_STACK_LINE(69)
	this->answerString = HX_CSTRING("");
	HX_STACK_LINE(68)
	this->appleCountString = HX_CSTRING("");
	HX_STACK_LINE(67)
	this->islandCreationTimer = (int)1;
	HX_STACK_LINE(66)
	this->appleAndJuiceForPieTimer = (int)1;
	HX_STACK_LINE(65)
	this->appleForJuiceTimer = (int)1;
	HX_STACK_LINE(64)
	this->leaveTruckTimer = (int)2;
	HX_STACK_LINE(63)
	this->loadAppleTimer = (int)1;
	HX_STACK_LINE(61)
	this->currentTruckLvl = (int)1;
	HX_STACK_LINE(60)
	this->piePerTenCost = (int)25;
	HX_STACK_LINE(59)
	this->juicePerTenCost = (int)15;
	HX_STACK_LINE(58)
	this->applePerTenCost = (int)5;
	HX_STACK_LINE(57)
	this->buySignCost = (int)2;
	HX_STACK_LINE(56)
	this->appleCount = (int)0;
	HX_STACK_LINE(55)
	this->menuVisible = (int)0;
	HX_STACK_LINE(44)
	this->lastTileClicked = null();
	HX_STACK_LINE(29)
	super::__construct(MaxSize);
}
;
	return null();
}

//PlayState_obj::~PlayState_obj() { }

Dynamic PlayState_obj::__CreateEmpty() { return  new PlayState_obj; }
hx::ObjectPtr< PlayState_obj > PlayState_obj::__new(Dynamic MaxSize)
{  hx::ObjectPtr< PlayState_obj > result = new PlayState_obj();
	result->__construct(MaxSize);
	return result;}

Dynamic PlayState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< PlayState_obj > result = new PlayState_obj();
	result->__construct(inArgs[0]);
	return result;}

Void PlayState_obj::create( ){
{
		HX_STACK_FRAME("PlayState","create",0x82220fed,"PlayState.create","PlayState.hx",125,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(126)
		this->initPlaystate();
		HX_STACK_LINE(128)
		this->super::create();
	}
return null();
}


Void PlayState_obj::initPlaystate( ){
{
		HX_STACK_FRAME("PlayState","initPlaystate",0x4051bd9c,"PlayState.initPlaystate","PlayState.hx",132,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(135)
		::flixel::ui::FlxButton _g = ::flixel::ui::FlxButton_obj::__new((int)550,(int)450,HX_CSTRING("Menu"),this->openMenu_dyn());		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(135)
		this->menuButton = _g;
		HX_STACK_LINE(136)
		this->tutorialOn = ::MenuState_obj::tutorialOn;
		HX_STACK_LINE(137)
		this->set_bgColor((int)-10027009);
		HX_STACK_LINE(138)
		::Tutorial _g1 = ::Tutorial_obj::__new((int)0);		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(138)
		this->tutorial = _g1;
		HX_STACK_LINE(139)
		::TileBoard _g2 = ::TileBoard_obj::__new(null(),null());		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(139)
		this->tileBoard = _g2;
		HX_STACK_LINE(140)
		::TileBoarder _g3 = ::TileBoarder_obj::__new(null(),null());		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(140)
		this->currentTileBorder = _g3;
		HX_STACK_LINE(141)
		::Market _g4 = ::Market_obj::__new();		HX_STACK_VAR(_g4,"_g4");
		HX_STACK_LINE(141)
		this->market = _g4;
		HX_STACK_LINE(142)
		::AppleShed _g5 = ::AppleShed_obj::__new();		HX_STACK_VAR(_g5,"_g5");
		HX_STACK_LINE(142)
		this->appleShed = _g5;
		HX_STACK_LINE(143)
		::TruckButton _g6 = ::TruckButton_obj::__new();		HX_STACK_VAR(_g6,"_g6");
		HX_STACK_LINE(143)
		this->truckButton = _g6;
		HX_STACK_LINE(144)
		::AppleTruck _g7 = ::AppleTruck_obj::__new();		HX_STACK_VAR(_g7,"_g7");
		HX_STACK_LINE(144)
		this->appleTruck = _g7;
		HX_STACK_LINE(145)
		::AppleBasket _g8 = ::AppleBasket_obj::__new();		HX_STACK_VAR(_g8,"_g8");
		HX_STACK_LINE(145)
		this->basket = _g8;
		HX_STACK_LINE(146)
		::Seed _g9 = ::Seed_obj::__new(null(),null());		HX_STACK_VAR(_g9,"_g9");
		HX_STACK_LINE(146)
		this->seed = _g9;
		HX_STACK_LINE(147)
		::SystemStar _g10 = ::SystemStar_obj::__new();		HX_STACK_VAR(_g10,"_g10");
		HX_STACK_LINE(147)
		this->systemStar = _g10;
		HX_STACK_LINE(148)
		::JuiceIcon _g11 = ::JuiceIcon_obj::__new((int)60,(int)70);		HX_STACK_VAR(_g11,"_g11");
		HX_STACK_LINE(148)
		this->juiceMachineIcon = _g11;
		HX_STACK_LINE(149)
		::JuicePackIcon _g12 = ::JuicePackIcon_obj::__new();		HX_STACK_VAR(_g12,"_g12");
		HX_STACK_LINE(149)
		this->juicePackIcon = _g12;
		HX_STACK_LINE(150)
		::PiePackIcon _g13 = ::PiePackIcon_obj::__new();		HX_STACK_VAR(_g13,"_g13");
		HX_STACK_LINE(150)
		this->piePackIcon = _g13;
		HX_STACK_LINE(151)
		::PieIcon _g14 = ::PieIcon_obj::__new(null(),null());		HX_STACK_VAR(_g14,"_g14");
		HX_STACK_LINE(151)
		this->pieMachineIcon = _g14;
		HX_STACK_LINE(152)
		::flixel::plugin::MouseEventManager_obj::remove(this->seed);
		HX_STACK_LINE(153)
		::flixel::plugin::MouseEventManager_obj::remove(this->juiceMachineIcon);
		HX_STACK_LINE(154)
		::flixel::plugin::MouseEventManager_obj::remove(this->pieMachineIcon);
		HX_STACK_LINE(155)
		::flixel::FlxSprite _g15 = this->truckButton->getTruckButtonSprite();		HX_STACK_VAR(_g15,"_g15");
		HX_STACK_LINE(155)
		::flixel::plugin::MouseEventManager_obj::add(_g15,this->callTruck_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(156)
		::Seed _g16 = ::Seed_obj::__new(null(),null());		HX_STACK_VAR(_g16,"_g16");
		HX_STACK_LINE(156)
		this->seedDrag = _g16;
		HX_STACK_LINE(157)
		::JuiceIcon _g17 = ::JuiceIcon_obj::__new((int)60,(int)70);		HX_STACK_VAR(_g17,"_g17");
		HX_STACK_LINE(157)
		this->juiceMachineIconDrag = _g17;
		HX_STACK_LINE(158)
		::PieIcon _g18 = ::PieIcon_obj::__new(null(),null());		HX_STACK_VAR(_g18,"_g18");
		HX_STACK_LINE(158)
		this->pieMachineIconDrag = _g18;
		HX_STACK_LINE(159)
		::ProblemBoard _g19 = ::ProblemBoard_obj::__new();		HX_STACK_VAR(_g19,"_g19");
		HX_STACK_LINE(159)
		this->bb = _g19;
		HX_STACK_LINE(160)
		::flixel::FlxSprite _g20 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g20,"_g20");
		HX_STACK_LINE(160)
		this->buySign = _g20;
		HX_STACK_LINE(161)
		::flixel::group::FlxGroup _g21 = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g21,"_g21");
		HX_STACK_LINE(161)
		this->aboveGrassLayer = _g21;
		HX_STACK_LINE(162)
		::flixel::group::FlxGroup _g22 = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g22,"_g22");
		HX_STACK_LINE(162)
		this->belowBottomSprites = _g22;
		HX_STACK_LINE(163)
		::flixel::group::FlxGroup _g23 = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g23,"_g23");
		HX_STACK_LINE(163)
		this->bottomeSprites = _g23;
		HX_STACK_LINE(164)
		::flixel::group::FlxGroup _g24 = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g24,"_g24");
		HX_STACK_LINE(164)
		this->plantLayer = _g24;
		HX_STACK_LINE(165)
		::flixel::group::FlxGroup _g25 = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g25,"_g25");
		HX_STACK_LINE(165)
		this->machineLayer = _g25;
		HX_STACK_LINE(166)
		::flixel::FlxSprite _g26 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g26,"_g26");
		HX_STACK_LINE(166)
		this->backgroundSprite = _g26;
		HX_STACK_LINE(167)
		::AsteroidBelt _g27 = ::AsteroidBelt_obj::__new((int)-60);		HX_STACK_VAR(_g27,"_g27");
		HX_STACK_LINE(167)
		this->asteroidBelt = _g27;
		HX_STACK_LINE(168)
		::flixel::FlxSprite _g28 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g28,"_g28");
		HX_STACK_LINE(168)
		this->foreGroundRocks = _g28;
		HX_STACK_LINE(169)
		::flixel::FlxSprite _g29 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g29,"_g29");
		HX_STACK_LINE(169)
		this->bridge = _g29;
		HX_STACK_LINE(170)
		this->backgroundSprite->loadGraphic(HX_CSTRING("assets/images/spaceBackground.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(171)
		this->buySign->loadGraphic(HX_CSTRING("assets/images/buyLandButton.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(172)
		this->foreGroundRocks->loadGraphic(HX_CSTRING("assets/images/rockyForeground.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(173)
		this->bridge->loadGraphic(HX_CSTRING("assets/images/bridgeImage.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(174)
		::flixel::text::FlxText _g30 = ::flixel::text::FlxText_obj::__new((int)123,(int)43,(int)32,this->appleCountString,(int)12,false);		HX_STACK_VAR(_g30,"_g30");
		HX_STACK_LINE(174)
		this->appleCountFieldText = _g30;
		HX_STACK_LINE(175)
		this->belowBottomSprites->add(this->backgroundSprite);
		HX_STACK_LINE(176)
		this->belowBottomSprites->add(this->asteroidBelt);
		HX_STACK_LINE(177)
		this->belowBottomSprites->add(this->systemStar);
		HX_STACK_LINE(178)
		this->appleCountFieldText->set_color((int)268435455);
		HX_STACK_LINE(179)
		this->seedDrag->set_alpha(.5);
		HX_STACK_LINE(180)
		this->juiceMachineIconDrag->set_alpha(.5);
		HX_STACK_LINE(181)
		this->pieMachineIconDrag->set_alpha(.5);
		HX_STACK_LINE(182)
		this->foreGroundRocks->set_x((int)30);
		HX_STACK_LINE(183)
		this->foreGroundRocks->set_y((int)265);
		HX_STACK_LINE(184)
		this->bridge->set_y((int)260);
		HX_STACK_LINE(185)
		this->bridge->set_x((int)-55);
		HX_STACK_LINE(187)
		this->add(this->belowBottomSprites);
		HX_STACK_LINE(188)
		this->add(this->bottomeSprites);
		HX_STACK_LINE(189)
		this->add(this->floatingIsland);
		HX_STACK_LINE(190)
		this->add(this->basket);
		HX_STACK_LINE(191)
		this->add(this->juicePackIcon);
		HX_STACK_LINE(192)
		this->add(this->piePackIcon);
		HX_STACK_LINE(193)
		this->drawBoard();
		HX_STACK_LINE(194)
		this->add(this->truckButton);
		HX_STACK_LINE(195)
		this->add(this->market);
		HX_STACK_LINE(196)
		this->add(this->appleShed);
		HX_STACK_LINE(197)
		this->plantWeeds();
		HX_STACK_LINE(198)
		this->add(this->seed);
		HX_STACK_LINE(199)
		this->add(this->machineLayer);
		HX_STACK_LINE(200)
		this->add(this->plantLayer);
		HX_STACK_LINE(201)
		this->add(this->bridge);
		HX_STACK_LINE(202)
		this->add(this->appleTruck);
		HX_STACK_LINE(203)
		this->add(this->seedDrag);
		HX_STACK_LINE(204)
		this->add(this->juiceMachineIcon);
		HX_STACK_LINE(205)
		this->add(this->juiceMachineIconDrag);
		HX_STACK_LINE(206)
		this->add(this->pieMachineIcon);
		HX_STACK_LINE(207)
		this->add(this->pieMachineIconDrag);
		HX_STACK_LINE(208)
		if ((this->tutorialOn)){
			HX_STACK_LINE(209)
			this->add(this->tutorial);
		}
		HX_STACK_LINE(211)
		this->add(this->bb);
		HX_STACK_LINE(212)
		::NumberPad _g31 = ::NumberPad_obj::__new();		HX_STACK_VAR(_g31,"_g31");
		HX_STACK_LINE(212)
		this->numPad = _g31;
		HX_STACK_LINE(213)
		::String _g32 = this->numPad->getPlayerAnswerString();		HX_STACK_VAR(_g32,"_g32");
		HX_STACK_LINE(213)
		::flixel::text::FlxText _g33 = ::flixel::text::FlxText_obj::__new((int)235,(int)132,(int)105,_g32,(int)24,false);		HX_STACK_VAR(_g33,"_g33");
		HX_STACK_LINE(213)
		this->answerFieldText = _g33;
		HX_STACK_LINE(214)
		this->answerFieldText->set_color((int)0);
		HX_STACK_LINE(215)
		this->add(this->aboveGrassLayer);
		HX_STACK_LINE(216)
		this->add(this->numPad);
		HX_STACK_LINE(217)
		this->add(this->menuButton);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,initPlaystate,(void))

Void PlayState_obj::openMenu( ){
{
		HX_STACK_FRAME("PlayState","openMenu",0x9858a9ba,"PlayState.openMenu","PlayState.hx",221,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(221)
		if (((this->menuVisible == (int)0))){
			HX_STACK_LINE(222)
			::OptionsScreen _g = ::OptionsScreen_obj::__new();		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(222)
			this->menuScreen = _g;
			HX_STACK_LINE(223)
			this->menuScreen->menuVisible((int)1);
			HX_STACK_LINE(224)
			this->add(this->menuScreen);
			HX_STACK_LINE(225)
			this->releaseAllClickEvents((int)1);
			HX_STACK_LINE(226)
			this->menuVisible = (int)1;
		}
		else{
			HX_STACK_LINE(229)
			this->menuScreen->destroy();
			HX_STACK_LINE(230)
			this->releaseAllClickEvents((int)0);
			HX_STACK_LINE(231)
			this->menuVisible = (int)0;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,openMenu,(void))

Void PlayState_obj::releaseAllClickEvents( int val){
{
		HX_STACK_FRAME("PlayState","releaseAllClickEvents",0xc65e1c36,"PlayState.releaseAllClickEvents","PlayState.hx",237,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(237)
		if (((val == (int)1))){
			HX_STACK_LINE(238)
			this->numPad->unregisterNumPadEvents();
			HX_STACK_LINE(239)
			this->seedDrag->unregisterSeedEvent();
			HX_STACK_LINE(240)
			this->pieMachineIconDrag->unregisterPieEvent();
			HX_STACK_LINE(241)
			this->juiceMachineIconDrag->unregisterJuiceEvent();
			HX_STACK_LINE(242)
			::flixel::FlxSprite _g = this->truckButton->getTruckButtonSprite();		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(242)
			::flixel::plugin::MouseEventManager_obj::remove(_g);
			HX_STACK_LINE(243)
			this->market->unregisterEvents();
			HX_STACK_LINE(244)
			{
				HX_STACK_LINE(244)
				int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(244)
				int _g2 = this->treeArray->length;		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(244)
				while((true)){
					HX_STACK_LINE(244)
					if ((!(((_g1 < _g2))))){
						HX_STACK_LINE(244)
						break;
					}
					HX_STACK_LINE(244)
					int k = (_g1)++;		HX_STACK_VAR(k,"k");
					HX_STACK_LINE(245)
					if (((this->treeArray->__get(k).StaticCast< ::Tree >() != null()))){
						HX_STACK_LINE(246)
						this->treeArray->__get(k).StaticCast< ::Tree >()->unregisterEvents();
						HX_STACK_LINE(247)
						this->treeArray->__get(k).StaticCast< ::Tree >()->unregisterTreeClickedMouseEvent();
						HX_STACK_LINE(248)
						this->treeArray->__get(k).StaticCast< ::Tree >()->unregisterAppleEvent();
					}
				}
			}
			HX_STACK_LINE(251)
			{
				HX_STACK_LINE(251)
				int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(251)
				int _g2 = this->board->length;		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(251)
				while((true)){
					HX_STACK_LINE(251)
					if ((!(((_g1 < _g2))))){
						HX_STACK_LINE(251)
						break;
					}
					HX_STACK_LINE(251)
					int i = (_g1)++;		HX_STACK_VAR(i,"i");
					HX_STACK_LINE(252)
					{
						HX_STACK_LINE(252)
						int _g3 = (int)0;		HX_STACK_VAR(_g3,"_g3");
						HX_STACK_LINE(252)
						int _g21 = this->board->length;		HX_STACK_VAR(_g21,"_g21");
						HX_STACK_LINE(252)
						while((true)){
							HX_STACK_LINE(252)
							if ((!(((_g3 < _g21))))){
								HX_STACK_LINE(252)
								break;
							}
							HX_STACK_LINE(252)
							int j = (_g3)++;		HX_STACK_VAR(j,"j");
							HX_STACK_LINE(253)
							::flixel::plugin::MouseEventManager_obj::remove(this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >());
						}
					}
				}
			}
		}
		else{
			HX_STACK_LINE(258)
			this->seedDrag->registerSeedEvent();
			HX_STACK_LINE(259)
			this->pieMachineIconDrag->registerPieEvent();
			HX_STACK_LINE(260)
			this->juiceMachineIconDrag->registerJuiceEvent();
			HX_STACK_LINE(261)
			::flixel::FlxSprite _g1 = this->truckButton->getTruckButtonSprite();		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(261)
			::flixel::plugin::MouseEventManager_obj::add(_g1,this->callTruck_dyn(),null(),null(),null(),null(),null(),null());
			HX_STACK_LINE(262)
			this->market->registerEvents();
			HX_STACK_LINE(263)
			{
				HX_STACK_LINE(263)
				int _g11 = (int)0;		HX_STACK_VAR(_g11,"_g11");
				HX_STACK_LINE(263)
				int _g = this->board->length;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(263)
				while((true)){
					HX_STACK_LINE(263)
					if ((!(((_g11 < _g))))){
						HX_STACK_LINE(263)
						break;
					}
					HX_STACK_LINE(263)
					int i = (_g11)++;		HX_STACK_VAR(i,"i");
					HX_STACK_LINE(264)
					{
						HX_STACK_LINE(264)
						int _g3 = (int)0;		HX_STACK_VAR(_g3,"_g3");
						HX_STACK_LINE(264)
						int _g2 = this->board->length;		HX_STACK_VAR(_g2,"_g2");
						HX_STACK_LINE(264)
						while((true)){
							HX_STACK_LINE(264)
							if ((!(((_g3 < _g2))))){
								HX_STACK_LINE(264)
								break;
							}
							HX_STACK_LINE(264)
							int j = (_g3)++;		HX_STACK_VAR(j,"j");
							HX_STACK_LINE(265)
							if ((!(this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >()->getHasObject()))){
								HX_STACK_LINE(266)
								::flixel::plugin::MouseEventManager_obj::add(this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >(),this->buyButtonPopUp_dyn(),this->placeObject_dyn(),null(),null(),null(),null(),null());
							}
						}
					}
				}
			}
			HX_STACK_LINE(270)
			{
				HX_STACK_LINE(270)
				int _g11 = (int)0;		HX_STACK_VAR(_g11,"_g11");
				HX_STACK_LINE(270)
				int _g = this->treeArray->length;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(270)
				while((true)){
					HX_STACK_LINE(270)
					if ((!(((_g11 < _g))))){
						HX_STACK_LINE(270)
						break;
					}
					HX_STACK_LINE(270)
					int k = (_g11)++;		HX_STACK_VAR(k,"k");
					HX_STACK_LINE(271)
					if (((this->treeArray->__get(k).StaticCast< ::Tree >() != null()))){
						HX_STACK_LINE(272)
						::flixel::FlxSprite _g2 = this->treeArray->__get(k).StaticCast< ::Tree >()->getTreeSprite();		HX_STACK_VAR(_g2,"_g2");
						HX_STACK_LINE(272)
						::flixel::plugin::MouseEventManager_obj::add(_g2,this->treeClicked_dyn(),null(),null(),null(),null(),null(),null());
						HX_STACK_LINE(273)
						this->treeArray->__get(k).StaticCast< ::Tree >()->registerEvents();
						HX_STACK_LINE(274)
						this->treeArray->__get(k).StaticCast< ::Tree >()->registerTreeClickedMouseEvent();
						HX_STACK_LINE(275)
						this->treeArray->__get(k).StaticCast< ::Tree >()->registerAppleEvent();
					}
				}
			}
			HX_STACK_LINE(278)
			this->numPad->registerEvent();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,releaseAllClickEvents,(void))

Void PlayState_obj::initTreeArray( ){
{
		HX_STACK_FRAME("PlayState","initTreeArray",0xbf2a195a,"PlayState.initTreeArray","PlayState.hx",284,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(284)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(284)
		while((true)){
			HX_STACK_LINE(284)
			if ((!(((_g < (int)9))))){
				HX_STACK_LINE(284)
				break;
			}
			HX_STACK_LINE(284)
			int i = (_g)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(285)
			this->treeArray[i] = null();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,initTreeArray,(void))

Void PlayState_obj::plantWeeds( ){
{
		HX_STACK_FRAME("PlayState","plantWeeds",0xf12d52ec,"PlayState.plantWeeds","PlayState.hx",290,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(291)
		::Weed _g = ::Weed_obj::__new((int)1);		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(291)
		this->weed = _g;
		HX_STACK_LINE(292)
		this->weed->getChosenWeed()->set_x((int)150);
		HX_STACK_LINE(293)
		this->weed->getChosenWeed()->set_y((int)260);
		HX_STACK_LINE(294)
		this->plantLayer->add(this->weed);
		HX_STACK_LINE(295)
		::Weed _g1 = ::Weed_obj::__new((int)1);		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(295)
		this->weed = _g1;
		HX_STACK_LINE(296)
		this->weed->getChosenWeed()->set_x((int)450);
		HX_STACK_LINE(297)
		this->weed->getChosenWeed()->set_y((int)260);
		HX_STACK_LINE(298)
		this->plantLayer->add(this->weed);
		HX_STACK_LINE(299)
		::Weed _g2 = ::Weed_obj::__new((int)1);		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(299)
		this->weed = _g2;
		HX_STACK_LINE(300)
		this->weed->getChosenWeed()->set_x((int)330);
		HX_STACK_LINE(301)
		this->weed->getChosenWeed()->set_y((int)375);
		HX_STACK_LINE(302)
		this->plantLayer->add(this->weed);
		HX_STACK_LINE(303)
		::Weed _g3 = ::Weed_obj::__new((int)1);		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(303)
		this->weed = _g3;
		HX_STACK_LINE(304)
		this->weed->getChosenWeed()->set_x((int)305);
		HX_STACK_LINE(305)
		this->weed->getChosenWeed()->set_y((int)190);
		HX_STACK_LINE(306)
		this->plantLayer->add(this->weed);
		HX_STACK_LINE(307)
		::Weed _g4 = ::Weed_obj::__new((int)1);		HX_STACK_VAR(_g4,"_g4");
		HX_STACK_LINE(307)
		this->weed = _g4;
		HX_STACK_LINE(308)
		this->weed->getChosenWeed()->set_x((int)70);
		HX_STACK_LINE(309)
		this->weed->getChosenWeed()->set_y((int)260);
		HX_STACK_LINE(310)
		this->plantLayer->add(this->weed);
		HX_STACK_LINE(311)
		::Weed _g5 = ::Weed_obj::__new((int)1);		HX_STACK_VAR(_g5,"_g5");
		HX_STACK_LINE(311)
		this->weed = _g5;
		HX_STACK_LINE(312)
		this->weed->getChosenWeed()->set_x((int)150);
		HX_STACK_LINE(313)
		this->weed->getChosenWeed()->set_y((int)190);
		HX_STACK_LINE(314)
		this->plantLayer->add(this->weed);
		HX_STACK_LINE(315)
		::Weed _g6 = ::Weed_obj::__new((int)0);		HX_STACK_VAR(_g6,"_g6");
		HX_STACK_LINE(315)
		this->weed = _g6;
		HX_STACK_LINE(316)
		this->weed->getChosenWeed()->set_x((int)365);
		HX_STACK_LINE(317)
		this->weed->getChosenWeed()->set_y((int)225);
		HX_STACK_LINE(318)
		this->plantLayer->add(this->weed);
		HX_STACK_LINE(319)
		::Weed _g7 = ::Weed_obj::__new((int)0);		HX_STACK_VAR(_g7,"_g7");
		HX_STACK_LINE(319)
		this->weed = _g7;
		HX_STACK_LINE(320)
		this->weed->getChosenWeed()->set_x((int)170);
		HX_STACK_LINE(321)
		this->weed->getChosenWeed()->set_y((int)240);
		HX_STACK_LINE(322)
		this->plantLayer->add(this->weed);
		HX_STACK_LINE(323)
		::Weed _g8 = ::Weed_obj::__new((int)0);		HX_STACK_VAR(_g8,"_g8");
		HX_STACK_LINE(323)
		this->weed = _g8;
		HX_STACK_LINE(324)
		this->weed->getChosenWeed()->set_x((int)190);
		HX_STACK_LINE(325)
		this->weed->getChosenWeed()->set_y((int)320);
		HX_STACK_LINE(326)
		this->plantLayer->add(this->weed);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,plantWeeds,(void))

Void PlayState_obj::createFloatingIsland( ){
{
		HX_STACK_FRAME("PlayState","createFloatingIsland",0x88336508,"PlayState.createFloatingIsland","PlayState.hx",330,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(331)
		if (((this->islandCreationTimer <= (int)0))){
			HX_STACK_LINE(332)
			::Island _g = ::Island_obj::__new();		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(332)
			this->floatingIsland = _g;
			HX_STACK_LINE(333)
			this->bottomeSprites->add(this->floatingIsland);
			HX_STACK_LINE(334)
			int _g1 = ::flixel::util::FlxRandom_obj::intRanged((int)2,(int)10,null());		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(334)
			this->islandCreationTimer = _g1;
		}
		HX_STACK_LINE(336)
		hx::SubEq(this->islandCreationTimer,::flixel::FlxG_obj::elapsed);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,createFloatingIsland,(void))

Void PlayState_obj::initFlyingJuiceBottleArray( ){
{
		HX_STACK_FRAME("PlayState","initFlyingJuiceBottleArray",0xb8803703,"PlayState.initFlyingJuiceBottleArray","PlayState.hx",341,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(341)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(341)
		while((true)){
			HX_STACK_LINE(341)
			if ((!(((_g < (int)9))))){
				HX_STACK_LINE(341)
				break;
			}
			HX_STACK_LINE(341)
			int i = (_g)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(342)
			this->flyingJuiceBottleArray[i] = null();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,initFlyingJuiceBottleArray,(void))

Void PlayState_obj::initJuiceMachineArray( ){
{
		HX_STACK_FRAME("PlayState","initJuiceMachineArray",0x68c8ed11,"PlayState.initJuiceMachineArray","PlayState.hx",348,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(348)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(348)
		while((true)){
			HX_STACK_LINE(348)
			if ((!(((_g < (int)9))))){
				HX_STACK_LINE(348)
				break;
			}
			HX_STACK_LINE(348)
			int i = (_g)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(349)
			this->juiceMachineArray[i] = null();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,initJuiceMachineArray,(void))

Void PlayState_obj::initPieMachineArray( ){
{
		HX_STACK_FRAME("PlayState","initPieMachineArray",0x10c7f23d,"PlayState.initPieMachineArray","PlayState.hx",355,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(355)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(355)
		while((true)){
			HX_STACK_LINE(355)
			if ((!(((_g < (int)9))))){
				HX_STACK_LINE(355)
				break;
			}
			HX_STACK_LINE(355)
			int i = (_g)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(356)
			this->pieMachineArray[i] = null();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,initPieMachineArray,(void))

Void PlayState_obj::addFlyingBottleArray( ::JuiceIcon flyingBottle){
{
		HX_STACK_FRAME("PlayState","addFlyingBottleArray",0x5f067ab4,"PlayState.addFlyingBottleArray","PlayState.hx",362,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(flyingBottle,"flyingBottle")
		HX_STACK_LINE(362)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(362)
		int _g = this->flyingJuiceBottleArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(362)
		while((true)){
			HX_STACK_LINE(362)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(362)
				break;
			}
			HX_STACK_LINE(362)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(363)
			if (((this->flyingJuiceBottleArray->__get(i).StaticCast< ::JuiceIcon >() == null()))){
				HX_STACK_LINE(364)
				this->flyingJuiceBottleArray[i] = flyingBottle;
				HX_STACK_LINE(365)
				return null();
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,addFlyingBottleArray,(void))

Void PlayState_obj::addTreeArray( ::Tree treeObj){
{
		HX_STACK_FRAME("PlayState","addTreeArray",0xab2424ab,"PlayState.addTreeArray","PlayState.hx",372,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(treeObj,"treeObj")
		HX_STACK_LINE(372)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(372)
		int _g = this->treeArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(372)
		while((true)){
			HX_STACK_LINE(372)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(372)
				break;
			}
			HX_STACK_LINE(372)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(373)
			if (((this->treeArray->__get(i).StaticCast< ::Tree >() == null()))){
				HX_STACK_LINE(374)
				this->treeArray[i] = treeObj;
				HX_STACK_LINE(375)
				return null();
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,addTreeArray,(void))

Void PlayState_obj::addJuiceMachineArray( ::JuiceMachine machineObj){
{
		HX_STACK_FRAME("PlayState","addJuiceMachineArray",0xfca83162,"PlayState.addJuiceMachineArray","PlayState.hx",382,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(machineObj,"machineObj")
		HX_STACK_LINE(382)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(382)
		int _g = this->juiceMachineArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(382)
		while((true)){
			HX_STACK_LINE(382)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(382)
				break;
			}
			HX_STACK_LINE(382)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(383)
			if (((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >() == null()))){
				HX_STACK_LINE(384)
				this->juiceMachineArray[i] = machineObj;
				HX_STACK_LINE(385)
				return null();
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,addJuiceMachineArray,(void))

Void PlayState_obj::addPieMachineArray( ::PieMachine machineObj){
{
		HX_STACK_FRAME("PlayState","addPieMachineArray",0xc8a6d04e,"PlayState.addPieMachineArray","PlayState.hx",392,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(machineObj,"machineObj")
		HX_STACK_LINE(392)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(392)
		int _g = this->pieMachineArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(392)
		while((true)){
			HX_STACK_LINE(392)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(392)
				break;
			}
			HX_STACK_LINE(392)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(393)
			if (((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >() == null()))){
				HX_STACK_LINE(394)
				this->pieMachineArray[i] = machineObj;
				HX_STACK_LINE(395)
				return null();
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,addPieMachineArray,(void))

Void PlayState_obj::tileBoarderFlip( ){
{
		HX_STACK_FRAME("PlayState","tileBoarderFlip",0xb78e6941,"PlayState.tileBoarderFlip","PlayState.hx",403,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(403)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(403)
		while((true)){
			HX_STACK_LINE(403)
			if ((!(((_g < (int)3))))){
				HX_STACK_LINE(403)
				break;
			}
			HX_STACK_LINE(403)
			int i = (_g)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(404)
			{
				HX_STACK_LINE(404)
				int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(404)
				while((true)){
					HX_STACK_LINE(404)
					if ((!(((_g1 < (int)3))))){
						HX_STACK_LINE(404)
						break;
					}
					HX_STACK_LINE(404)
					int j = (_g1)++;		HX_STACK_VAR(j,"j");
					struct _Function_4_1{
						inline static bool Block( int &i,hx::ObjectPtr< ::PlayState_obj > __this,int &j){
							HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",407,0xb30d7781)
							{
								HX_STACK_LINE(407)
								return (  ((::flixel::util::FlxCollision_obj::pixelPerfectPointCheck(::flixel::FlxG_obj::mouse->screenX,::flixel::FlxG_obj::mouse->screenY,__this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >(),null()))) ? bool(__this->juiceMachineIconDrag->getDragging()) : bool(false) );
							}
							return null();
						}
					};
					struct _Function_4_2{
						inline static bool Block( int &i,hx::ObjectPtr< ::PlayState_obj > __this,int &j){
							HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",408,0xb30d7781)
							{
								HX_STACK_LINE(408)
								return (  ((::flixel::util::FlxCollision_obj::pixelPerfectPointCheck(::flixel::FlxG_obj::mouse->screenX,::flixel::FlxG_obj::mouse->screenY,__this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >(),null()))) ? bool(__this->pieMachineIconDrag->getDragging()) : bool(false) );
							}
							return null();
						}
					};
					HX_STACK_LINE(406)
					if (((  ((!(((  ((!(((  ((::flixel::util::FlxCollision_obj::pixelPerfectPointCheck(::flixel::FlxG_obj::mouse->screenX,::flixel::FlxG_obj::mouse->screenY,this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >(),null()))) ? bool(this->seedDrag->getDragging()) : bool(false) ))))) ? bool(_Function_4_1::Block(i,this,j)) : bool(true) ))))) ? bool(_Function_4_2::Block(i,this,j)) : bool(true) ))){
						HX_STACK_LINE(410)
						this->boarder->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::TileBoarder >()->boarderSwitch(HX_CSTRING("on"));
					}
					else{
						HX_STACK_LINE(415)
						this->boarder->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::TileBoarder >()->boarderSwitch(HX_CSTRING("off"));
					}
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,tileBoarderFlip,(void))

Void PlayState_obj::buyButtonPopUp( ::Tile sprite){
{
		HX_STACK_FRAME("PlayState","buyButtonPopUp",0xdd7edf45,"PlayState.buyButtonPopUp","PlayState.hx",425,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(425)
		if (((  ((!(this->numPad->getPadScrollingIn()))) ? bool(!(sprite->getTileOwned())) : bool(false) ))){
			HX_STACK_LINE(426)
			this->buySign->set_x((sprite->x + (int)70));
			HX_STACK_LINE(427)
			this->buySign->set_y((sprite->y + (int)38));
			HX_STACK_LINE(428)
			this->aboveGrassLayer->remove(this->buySignCostFieldText,null());
			HX_STACK_LINE(429)
			::flixel::plugin::MouseEventManager_obj::add(this->buySign,this->buyLand_dyn(),null(),null(),null(),null(),null(),null());
			HX_STACK_LINE(430)
			if ((!(sprite->getTileClicked()))){
				HX_STACK_LINE(431)
				this->buySign->revive();
				HX_STACK_LINE(432)
				this->aboveGrassLayer->add(this->buySign);
				HX_STACK_LINE(433)
				::String _g = ::Std_obj::string(this->buySignCost);		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(433)
				::flixel::text::FlxText _g1 = ::flixel::text::FlxText_obj::__new((this->buySign->x + (int)33),(this->buySign->y + (int)1),(int)16,_g,(int)12,null());		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(433)
				this->buySignCostFieldText = _g1;
				HX_STACK_LINE(434)
				this->buySignCostFieldText->set_color((int)15066459);
				HX_STACK_LINE(435)
				this->aboveGrassLayer->add(this->buySignCostFieldText);
				HX_STACK_LINE(436)
				sprite->setTileClicked(true);
				HX_STACK_LINE(437)
				if (((bool((this->lastTileClicked != sprite)) && bool((this->lastTileClicked != null()))))){
					HX_STACK_LINE(438)
					this->lastTileClicked->setTileClicked(false);
				}
			}
			else{
				HX_STACK_LINE(442)
				this->buySign->kill();
				HX_STACK_LINE(443)
				this->aboveGrassLayer->remove(this->buySignCostFieldText,null());
				HX_STACK_LINE(444)
				sprite->setTileClicked(false);
			}
			HX_STACK_LINE(446)
			this->lastTileClicked = sprite;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,buyButtonPopUp,(void))

Void PlayState_obj::displayAppleCount( ){
{
		HX_STACK_FRAME("PlayState","displayAppleCount",0x145168a6,"PlayState.displayAppleCount","PlayState.hx",451,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(453)
		{
			HX_STACK_LINE(453)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(453)
			int _g = this->treeArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(453)
			while((true)){
				HX_STACK_LINE(453)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(453)
					break;
				}
				HX_STACK_LINE(453)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(454)
				if (((this->treeArray->__get(i).StaticCast< ::Tree >() != null()))){
					HX_STACK_LINE(455)
					int _g2 = this->market->getCoinCount();		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(455)
					this->treeArray->__get(i).StaticCast< ::Tree >()->currentGoldCount(_g2);
					struct _Function_4_1{
						inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
							HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",456,0xb30d7781)
							{
								HX_STACK_LINE(456)
								int _g11 = __this->basket->getBasketFullVal();		HX_STACK_VAR(_g11,"_g11");
								HX_STACK_LINE(456)
								return (__this->appleCount < _g11);
							}
							return null();
						}
					};
					HX_STACK_LINE(456)
					if (((  ((this->treeArray->__get(i).StaticCast< ::Tree >()->getAppleStored())) ? bool(_Function_4_1::Block(this)) : bool(false) ))){
						HX_STACK_LINE(457)
						(this->appleCount)++;
						HX_STACK_LINE(459)
						int _g21 = this->basket->getBasketFullVal();		HX_STACK_VAR(_g21,"_g21");
						HX_STACK_LINE(459)
						this->appleShed->animateShed(this->appleCount,_g21);
						HX_STACK_LINE(460)
						this->treeArray->__get(i).StaticCast< ::Tree >()->setAppleStored(false);
					}
					HX_STACK_LINE(462)
					this->treeArray->__get(i).StaticCast< ::Tree >()->setAppleClicked(false);
				}
			}
		}
		struct _Function_1_1{
			inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",465,0xb30d7781)
				{
					HX_STACK_LINE(465)
					int _g3 = __this->market->getJuicePackCount();		HX_STACK_VAR(_g3,"_g3");
					HX_STACK_LINE(465)
					return (_g3 >= (int)10);
				}
				return null();
			}
		};
		struct _Function_1_2{
			inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",465,0xb30d7781)
				{
					HX_STACK_LINE(465)
					int _g4 = __this->market->getPiePackCount();		HX_STACK_VAR(_g4,"_g4");
					HX_STACK_LINE(465)
					return (_g4 >= (int)10);
				}
				return null();
			}
		};
		HX_STACK_LINE(465)
		if (((  (((  (((  ((!(((  ((!(((this->appleCount >= (int)10))))) ? bool(_Function_1_1::Block(this)) : bool(true) ))))) ? bool(_Function_1_2::Block(this)) : bool(true) ))) ? bool(!(this->truckButton->getButtonClickable())) : bool(false) ))) ? bool(this->appleTruck->getTruckIsReset()) : bool(false) ))){
			HX_STACK_LINE(466)
			this->truckButton->setButtonLevel((int)1);
			HX_STACK_LINE(467)
			this->truckButton->buttonSetting();
			HX_STACK_LINE(468)
			this->truckButton->setButtonClickable(true);
		}
		struct _Function_1_3{
			inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",470,0xb30d7781)
				{
					HX_STACK_LINE(470)
					int _g5 = __this->market->getJuicePackCount();		HX_STACK_VAR(_g5,"_g5");
					HX_STACK_LINE(470)
					return (_g5 < (int)10);
				}
				return null();
			}
		};
		struct _Function_1_4{
			inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",470,0xb30d7781)
				{
					HX_STACK_LINE(470)
					int _g6 = __this->market->getPiePackCount();		HX_STACK_VAR(_g6,"_g6");
					HX_STACK_LINE(470)
					return (_g6 < (int)10);
				}
				return null();
			}
		};
		HX_STACK_LINE(470)
		if (((  (((  (((this->appleCount < (int)10))) ? bool(_Function_1_3::Block(this)) : bool(false) ))) ? bool(_Function_1_4::Block(this)) : bool(false) ))){
			HX_STACK_LINE(471)
			this->truckButton->setButtonLevel((int)2);
			HX_STACK_LINE(472)
			this->truckButton->buttonSetting();
			HX_STACK_LINE(473)
			this->truckButton->setButtonClickable(false);
		}
		HX_STACK_LINE(475)
		::String _g7 = ::Std_obj::string(this->appleCount);		HX_STACK_VAR(_g7,"_g7");
		HX_STACK_LINE(475)
		this->appleCountFieldText->set_text(_g7);
		HX_STACK_LINE(476)
		this->add(this->appleCountFieldText);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,displayAppleCount,(void))

Void PlayState_obj::displayPadNumbers( ){
{
		HX_STACK_FRAME("PlayState","displayPadNumbers",0xfa8e4008,"PlayState.displayPadNumbers","PlayState.hx",480,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(482)
		::String _g = this->numPad->getPlayerAnswerString();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(482)
		this->answerFieldText->set_text(_g);
		HX_STACK_LINE(483)
		Float _g1 = this->numPad->getPadY();		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(483)
		Float _g2 = (_g1 + (int)12);		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(483)
		this->answerFieldText->set_y(_g2);
		HX_STACK_LINE(484)
		this->add(this->answerFieldText);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,displayPadNumbers,(void))

Void PlayState_obj::placeObject( ::Tile sprite){
{
		HX_STACK_FRAME("PlayState","placeObject",0x47ce8455,"PlayState.placeObject","PlayState.hx",488,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(489)
		if (((  (((  (((  ((::flixel::util::FlxCollision_obj::pixelPerfectCheck(this->seedDrag,sprite,null(),null()))) ? bool(::flixel::util::FlxCollision_obj::pixelPerfectPointCheck(::flixel::FlxG_obj::mouse->screenX,::flixel::FlxG_obj::mouse->screenY,sprite,null())) : bool(false) ))) ? bool(!(sprite->getHasObject())) : bool(false) ))) ? bool(sprite->getTileOwned()) : bool(false) ))){
			HX_STACK_LINE(490)
			::flixel::system::FlxSound _g = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/plantingSeedSound.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(490)
			this->plantingSeedSound = _g;
			HX_STACK_LINE(491)
			this->plantingSeedSound->play(null());
			HX_STACK_LINE(492)
			this->market->decSeedCount();
			HX_STACK_LINE(493)
			::flixel::FlxSprite _g1 = this->basket->getBasketSprite();		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(493)
			::Tree _g2 = ::Tree_obj::__new(_g1);		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(493)
			this->tree = _g2;
			HX_STACK_LINE(494)
			this->tree->registerEvents();
			HX_STACK_LINE(495)
			this->tree->setTreeGrowthPhase((int)0);
			HX_STACK_LINE(496)
			this->tree->setMyTile(sprite);
			HX_STACK_LINE(497)
			::flixel::FlxSprite _g3 = this->tree->getTreeSprite();		HX_STACK_VAR(_g3,"_g3");
			HX_STACK_LINE(497)
			::flixel::plugin::MouseEventManager_obj::add(_g3,this->treeClicked_dyn(),null(),null(),null(),null(),null(),null());
			HX_STACK_LINE(498)
			bool _g4 = sprite->getTileOwned();		HX_STACK_VAR(_g4,"_g4");
			HX_STACK_LINE(498)
			this->tree->setTreeGroundColor(_g4);
			HX_STACK_LINE(499)
			this->tree->setTreeX((sprite->x + (int)67));
			HX_STACK_LINE(500)
			this->tree->setTreeY((sprite->y - (int)25));
			HX_STACK_LINE(501)
			this->tree->positionAlerts();
			HX_STACK_LINE(502)
			sprite->setHasObject(true);
			HX_STACK_LINE(503)
			this->addTreeArray(this->tree);
			HX_STACK_LINE(504)
			this->secondTreeTutMessage();
			HX_STACK_LINE(505)
			this->tree->growTree();
			HX_STACK_LINE(506)
			this->aboveGrassLayer->add(this->tree);
			HX_STACK_LINE(507)
			if ((this->tutorialOn)){
				HX_STACK_LINE(508)
				this->treeArray->__get((int)0).StaticCast< ::Tree >()->setTreeThirstTut((int)9999);
				HX_STACK_LINE(509)
				this->treeArray->__get((int)0).StaticCast< ::Tree >()->setTreeSunlightTut((int)9999);
				HX_STACK_LINE(510)
				this->tutorial->destroy();
				HX_STACK_LINE(511)
				::Tutorial _g5 = ::Tutorial_obj::__new((int)1);		HX_STACK_VAR(_g5,"_g5");
				HX_STACK_LINE(511)
				this->tutorial = _g5;
				HX_STACK_LINE(512)
				this->add(this->tutorial);
			}
		}
		HX_STACK_LINE(515)
		if (((  (((  (((  ((::flixel::util::FlxCollision_obj::pixelPerfectCheck(this->juiceMachineIconDrag,sprite,null(),null()))) ? bool(::flixel::util::FlxCollision_obj::pixelPerfectPointCheck(::flixel::FlxG_obj::mouse->screenX,::flixel::FlxG_obj::mouse->screenY,sprite,null())) : bool(false) ))) ? bool(!(sprite->getHasObject())) : bool(false) ))) ? bool(sprite->getTileOwned()) : bool(false) ))){
			HX_STACK_LINE(516)
			this->market->decjuiceCount((int)1);
			HX_STACK_LINE(517)
			::JuiceMachine _g6 = ::JuiceMachine_obj::__new(sprite);		HX_STACK_VAR(_g6,"_g6");
			HX_STACK_LINE(517)
			this->juiceMachine = _g6;
			HX_STACK_LINE(518)
			this->juiceMachine->setRiseRun();
			HX_STACK_LINE(519)
			sprite->setHasObject(true);
			HX_STACK_LINE(520)
			this->addJuiceMachineArray(this->juiceMachine);
			HX_STACK_LINE(521)
			this->machineLayer->add(this->juiceMachine);
		}
		HX_STACK_LINE(523)
		if (((  (((  (((  ((::flixel::util::FlxCollision_obj::pixelPerfectCheck(this->pieMachineIconDrag,sprite,null(),null()))) ? bool(::flixel::util::FlxCollision_obj::pixelPerfectPointCheck(::flixel::FlxG_obj::mouse->screenX,::flixel::FlxG_obj::mouse->screenY,sprite,null())) : bool(false) ))) ? bool(!(sprite->getHasObject())) : bool(false) ))) ? bool(sprite->getTileOwned()) : bool(false) ))){
			HX_STACK_LINE(524)
			this->market->decPieCount();
			HX_STACK_LINE(525)
			::PieMachine _g7 = ::PieMachine_obj::__new(sprite);		HX_STACK_VAR(_g7,"_g7");
			HX_STACK_LINE(525)
			this->pieMachine = _g7;
			HX_STACK_LINE(526)
			this->pieMachine->setRiseRun();
			HX_STACK_LINE(527)
			sprite->setHasObject(true);
			HX_STACK_LINE(528)
			this->addPieMachineArray(this->pieMachine);
			HX_STACK_LINE(529)
			this->machineLayer->add(this->pieMachine);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,placeObject,(void))

Void PlayState_obj::treeClicked( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("PlayState","treeClicked",0xa0e35a58,"PlayState.treeClicked","PlayState.hx",533,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,treeClicked,(void))

Void PlayState_obj::secondTreeTutMessage( ){
{
		HX_STACK_FRAME("PlayState","secondTreeTutMessage",0x59e2b477,"PlayState.secondTreeTutMessage","PlayState.hx",539,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(539)
		if (((this->treeArray->__get((int)1).StaticCast< ::Tree >() != null()))){
			HX_STACK_LINE(540)
			::Tutorial _g = ::Tutorial_obj::__new((int)5);		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(540)
			this->tutorial = _g;
			HX_STACK_LINE(541)
			if ((this->tutorialOn)){
				HX_STACK_LINE(542)
				this->add(this->tutorial);
			}
			HX_STACK_LINE(544)
			this->tutorialOn = false;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,secondTreeTutMessage,(void))

Void PlayState_obj::buyLand( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("PlayState","buyLand",0xdb3c3a40,"PlayState.buyLand","PlayState.hx",550,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(550)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(550)
		while((true)){
			HX_STACK_LINE(550)
			if ((!(((_g < (int)3))))){
				HX_STACK_LINE(550)
				break;
			}
			HX_STACK_LINE(550)
			int i = (_g)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(551)
			{
				HX_STACK_LINE(551)
				int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(551)
				while((true)){
					HX_STACK_LINE(551)
					if ((!(((_g1 < (int)3))))){
						HX_STACK_LINE(551)
						break;
					}
					HX_STACK_LINE(551)
					int j = (_g1)++;		HX_STACK_VAR(j,"j");
					struct _Function_4_1{
						inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
							HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",552,0xb30d7781)
							{
								HX_STACK_LINE(552)
								int _g2 = __this->market->getCoinCount();		HX_STACK_VAR(_g2,"_g2");
								HX_STACK_LINE(552)
								return (_g2 >= __this->buySignCost);
							}
							return null();
						}
					};
					HX_STACK_LINE(552)
					if (((  ((::flixel::util::FlxCollision_obj::pixelPerfectPointCheck(::flixel::FlxG_obj::mouse->screenX,::flixel::FlxG_obj::mouse->screenY,this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >(),null()))) ? bool(_Function_4_1::Block(this)) : bool(false) ))){
						HX_STACK_LINE(553)
						this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >()->setAsOwned();
						HX_STACK_LINE(554)
						this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >()->setHasObject(false);
						HX_STACK_LINE(555)
						this->buySign->kill();
						HX_STACK_LINE(556)
						this->market->addSubtractCoins(HX_CSTRING("-"),this->buySignCost);
						HX_STACK_LINE(557)
						this->aboveGrassLayer->remove(this->buySignCostFieldText,null());
						HX_STACK_LINE(558)
						(this->buySignCost)++;
					}
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,buyLand,(void))

Void PlayState_obj::callTruck( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("PlayState","callTruck",0xdec0d9d0,"PlayState.callTruck","PlayState.hx",565,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		struct _Function_1_1{
			inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",565,0xb30d7781)
				{
					HX_STACK_LINE(565)
					int _g = __this->market->getJuicePackCount();		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(565)
					return (_g >= (int)10);
				}
				return null();
			}
		};
		struct _Function_1_2{
			inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",565,0xb30d7781)
				{
					HX_STACK_LINE(565)
					int _g1 = __this->market->getPiePackCount();		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(565)
					return (_g1 >= (int)10);
				}
				return null();
			}
		};
		HX_STACK_LINE(565)
		if (((  (((  ((!(((  ((!(((this->appleCount >= (int)10))))) ? bool(_Function_1_1::Block(this)) : bool(true) ))))) ? bool(_Function_1_2::Block(this)) : bool(true) ))) ? bool(this->appleTruck->getTruckIsReset()) : bool(false) ))){
			HX_STACK_LINE(566)
			Float _g2 = this->tutorial->getCurrentTutSectionNumber();		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(566)
			if (((  (((_g2 == (int)1))) ? bool(this->tutorialOn) : bool(false) ))){
				HX_STACK_LINE(567)
				this->tutorial->destroy();
			}
			HX_STACK_LINE(569)
			if ((this->tutorialOn)){
				HX_STACK_LINE(570)
				this->tutorial->destroy();
			}
			HX_STACK_LINE(572)
			this->appleTruck->setPickUpReady(true);
			HX_STACK_LINE(573)
			this->truckButton->setButtonLevel((int)2);
			HX_STACK_LINE(574)
			this->truckButton->buttonSetting();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,callTruck,(void))

Void PlayState_obj::truckLoadingGoods( ){
{
		HX_STACK_FRAME("PlayState","truckLoadingGoods",0xdb59a568,"PlayState.truckLoadingGoods","PlayState.hx",579,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(579)
		if ((this->appleTruck->getLoadingApples())){
			HX_STACK_LINE(580)
			if ((this->truckTutSection)){
				HX_STACK_LINE(582)
				if ((this->tutorialOn)){
					HX_STACK_LINE(583)
					this->tutorial->destroy();
					HX_STACK_LINE(584)
					::Tutorial _g = ::Tutorial_obj::__new((int)4);		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(584)
					this->tutorial = _g;
					HX_STACK_LINE(585)
					this->add(this->tutorial);
				}
				HX_STACK_LINE(587)
				this->truckTutSection = false;
			}
			HX_STACK_LINE(589)
			hx::SubEq(this->loadAppleTimer,::flixel::FlxG_obj::elapsed);
			HX_STACK_LINE(590)
			if (((this->loadAppleTimer < (int)0))){
				HX_STACK_LINE(591)
				int _g1 = this->appleTruck->getAppleTruckCapacity();		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(591)
				if (((  (((_g1 > (int)0))) ? bool((this->appleCount >= (int)10)) : bool(false) ))){
					HX_STACK_LINE(592)
					hx::SubEq(this->appleCount,(int)10);
					HX_STACK_LINE(593)
					this->appleTruck->decTruckCapacity();
					HX_STACK_LINE(595)
					int _g2 = this->basket->getBasketFullVal();		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(595)
					this->appleShed->animateShed(this->appleCount,_g2);
					HX_STACK_LINE(596)
					this->market->addSubtractCoins(HX_CSTRING("+"),this->applePerTenCost);
					HX_STACK_LINE(597)
					::flixel::system::FlxSound _g3 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/coinsSound.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g3,"_g3");
					HX_STACK_LINE(597)
					this->coinsSound = _g3;
					HX_STACK_LINE(598)
					this->coinsSound->play(null());
					HX_STACK_LINE(599)
					this->loadAppleTimer = (int)1;
				}
				HX_STACK_LINE(601)
				int _g4 = this->appleTruck->getAppleTruckCapacity();		HX_STACK_VAR(_g4,"_g4");
				struct _Function_3_1{
					inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",601,0xb30d7781)
						{
							HX_STACK_LINE(601)
							int _g5 = __this->market->getJuicePackCount();		HX_STACK_VAR(_g5,"_g5");
							HX_STACK_LINE(601)
							return (_g5 >= (int)10);
						}
						return null();
					}
				};
				HX_STACK_LINE(601)
				if (((  (((_g4 > (int)0))) ? bool(_Function_3_1::Block(this)) : bool(false) ))){
					HX_STACK_LINE(602)
					this->market->loadJuicePacksDec();
					HX_STACK_LINE(603)
					this->appleTruck->decTruckCapacity();
					HX_STACK_LINE(604)
					this->market->addSubtractCoins(HX_CSTRING("+"),this->juicePerTenCost);
					HX_STACK_LINE(605)
					::flixel::system::FlxSound _g6 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/coinsSound.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g6,"_g6");
					HX_STACK_LINE(605)
					this->coinsSound = _g6;
					HX_STACK_LINE(606)
					this->coinsSound->play(null());
					HX_STACK_LINE(607)
					this->loadAppleTimer = (int)1;
				}
				HX_STACK_LINE(609)
				int _g7 = this->appleTruck->getAppleTruckCapacity();		HX_STACK_VAR(_g7,"_g7");
				struct _Function_3_2{
					inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",609,0xb30d7781)
						{
							HX_STACK_LINE(609)
							int _g8 = __this->market->getPiePackCount();		HX_STACK_VAR(_g8,"_g8");
							HX_STACK_LINE(609)
							return (_g8 >= (int)10);
						}
						return null();
					}
				};
				HX_STACK_LINE(609)
				if (((  (((_g7 > (int)0))) ? bool(_Function_3_2::Block(this)) : bool(false) ))){
					HX_STACK_LINE(610)
					this->market->loadPiePacksDec();
					HX_STACK_LINE(611)
					this->appleTruck->decTruckCapacity();
					HX_STACK_LINE(612)
					this->market->addSubtractCoins(HX_CSTRING("+"),this->piePerTenCost);
					HX_STACK_LINE(613)
					::flixel::system::FlxSound _g9 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/coinsSound.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g9,"_g9");
					HX_STACK_LINE(613)
					this->coinsSound = _g9;
					HX_STACK_LINE(614)
					this->coinsSound->play(null());
					HX_STACK_LINE(615)
					this->loadAppleTimer = (int)1;
				}
			}
			HX_STACK_LINE(618)
			int _g10 = this->appleTruck->getAppleTruckCapacity();		HX_STACK_VAR(_g10,"_g10");
			struct _Function_2_1{
				inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
					HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",618,0xb30d7781)
					{
						struct _Function_3_1{
							inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
								HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",618,0xb30d7781)
								{
									HX_STACK_LINE(618)
									int _g11 = __this->market->getJuicePackCount();		HX_STACK_VAR(_g11,"_g11");
									HX_STACK_LINE(618)
									return (_g11 < (int)10);
								}
								return null();
							}
						};
						struct _Function_3_2{
							inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
								HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",618,0xb30d7781)
								{
									HX_STACK_LINE(618)
									int _g12 = __this->market->getPiePackCount();		HX_STACK_VAR(_g12,"_g12");
									HX_STACK_LINE(618)
									return (_g12 < (int)10);
								}
								return null();
							}
						};
						HX_STACK_LINE(618)
						return (  (((  (((__this->appleCount < (int)10))) ? bool(_Function_3_1::Block(__this)) : bool(false) ))) ? bool(_Function_3_2::Block(__this)) : bool(false) );
					}
					return null();
				}
			};
			HX_STACK_LINE(618)
			if (((  ((!(((_g10 == (int)0))))) ? bool(_Function_2_1::Block(this)) : bool(true) ))){
				HX_STACK_LINE(619)
				hx::SubEq(this->leaveTruckTimer,::flixel::FlxG_obj::elapsed);
				HX_STACK_LINE(620)
				if (((this->leaveTruckTimer < (int)0))){
					HX_STACK_LINE(621)
					this->loadAppleTimer = (int)1;
					HX_STACK_LINE(622)
					this->appleTruck->setTruckSize(this->currentTruckLvl);
					HX_STACK_LINE(623)
					this->appleTruck->setLoadingApples(false);
					HX_STACK_LINE(624)
					this->appleTruck->setPickUpReady(false);
					HX_STACK_LINE(625)
					this->truckButton->setButtonClickable(false);
					HX_STACK_LINE(626)
					this->leaveTruckTimer = (int)2;
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,truckLoadingGoods,(void))

Void PlayState_obj::stopSeedDragIfBoard( ){
{
		HX_STACK_FRAME("PlayState","stopSeedDragIfBoard",0xac547091,"PlayState.stopSeedDragIfBoard","PlayState.hx",634,0xb30d7781)
		HX_STACK_THIS(this)
		struct _Function_1_1{
			inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",634,0xb30d7781)
				{
					HX_STACK_LINE(634)
					int _g = __this->market->getSeedCount();		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(634)
					return (_g < (int)1);
				}
				return null();
			}
		};
		HX_STACK_LINE(634)
		if (((  ((!(this->overlaysInUse))) ? bool(_Function_1_1::Block(this)) : bool(true) ))){
			HX_STACK_LINE(636)
			this->seedDrag->setDragging(false);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,stopSeedDragIfBoard,(void))

Void PlayState_obj::stopJuiceDragIfBoard( ){
{
		HX_STACK_FRAME("PlayState","stopJuiceDragIfBoard",0xd98ab548,"PlayState.stopJuiceDragIfBoard","PlayState.hx",644,0xb30d7781)
		HX_STACK_THIS(this)
		struct _Function_1_1{
			inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",644,0xb30d7781)
				{
					HX_STACK_LINE(644)
					int _g = __this->market->getJuiceCount();		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(644)
					return (_g < (int)1);
				}
				return null();
			}
		};
		HX_STACK_LINE(644)
		if (((  ((!(this->overlaysInUse))) ? bool(_Function_1_1::Block(this)) : bool(true) ))){
			HX_STACK_LINE(646)
			this->juiceMachineIconDrag->setDragging(false);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,stopJuiceDragIfBoard,(void))

Void PlayState_obj::stopPieDragIfBoard( ){
{
		HX_STACK_FRAME("PlayState","stopPieDragIfBoard",0x18c2f91c,"PlayState.stopPieDragIfBoard","PlayState.hx",655,0xb30d7781)
		HX_STACK_THIS(this)
		struct _Function_1_1{
			inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",655,0xb30d7781)
				{
					HX_STACK_LINE(655)
					int _g = __this->market->getPieCount();		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(655)
					return (_g < (int)1);
				}
				return null();
			}
		};
		HX_STACK_LINE(655)
		if (((  ((!(this->overlaysInUse))) ? bool(_Function_1_1::Block(this)) : bool(true) ))){
			HX_STACK_LINE(657)
			this->pieMachineIconDrag->setDragging(false);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,stopPieDragIfBoard,(void))

Void PlayState_obj::drawBoard( ){
{
		HX_STACK_FRAME("PlayState","drawBoard",0xbb4b7fb1,"PlayState.drawBoard","PlayState.hx",663,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(664)
		this->add(this->foreGroundRocks);
		HX_STACK_LINE(665)
		Array< ::Dynamic > _g = this->tileBoard->getBoardArray();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(665)
		this->board = _g;
		HX_STACK_LINE(666)
		Array< ::Dynamic > _g1 = this->tileBoard->getBoardBoarderArray();		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(666)
		this->boarder = _g1;
		HX_STACK_LINE(667)
		Array< ::Dynamic > _g2 = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(667)
		this->treeArray = _g2;
		HX_STACK_LINE(668)
		Array< ::Dynamic > _g3 = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(668)
		this->juiceMachineArray = _g3;
		HX_STACK_LINE(669)
		Array< ::Dynamic > _g4 = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(_g4,"_g4");
		HX_STACK_LINE(669)
		this->pieMachineArray = _g4;
		HX_STACK_LINE(670)
		Array< ::Dynamic > _g5 = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(_g5,"_g5");
		HX_STACK_LINE(670)
		this->flyingJuiceBottleArray = _g5;
		HX_STACK_LINE(671)
		this->initTreeArray();
		HX_STACK_LINE(672)
		this->initJuiceMachineArray();
		HX_STACK_LINE(673)
		this->initPieMachineArray();
		HX_STACK_LINE(674)
		this->initFlyingJuiceBottleArray();
		HX_STACK_LINE(675)
		int shiftX = (int)0;		HX_STACK_VAR(shiftX,"shiftX");
		HX_STACK_LINE(676)
		int shiftY = (int)0;		HX_STACK_VAR(shiftY,"shiftY");
		HX_STACK_LINE(677)
		int newPosX = (int)0;		HX_STACK_VAR(newPosX,"newPosX");
		HX_STACK_LINE(678)
		int newPosY = (int)0;		HX_STACK_VAR(newPosY,"newPosY");
		HX_STACK_LINE(680)
		{
			HX_STACK_LINE(680)
			int _g11 = (int)0;		HX_STACK_VAR(_g11,"_g11");
			HX_STACK_LINE(680)
			int _g6 = this->board->length;		HX_STACK_VAR(_g6,"_g6");
			HX_STACK_LINE(680)
			while((true)){
				HX_STACK_LINE(680)
				if ((!(((_g11 < _g6))))){
					HX_STACK_LINE(680)
					break;
				}
				HX_STACK_LINE(680)
				int i = (_g11)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(682)
				shiftX = (int)0;
				HX_STACK_LINE(683)
				shiftY = (int)0;
				HX_STACK_LINE(685)
				{
					HX_STACK_LINE(685)
					int _g31 = (int)0;		HX_STACK_VAR(_g31,"_g31");
					HX_STACK_LINE(685)
					int _g21 = this->board->length;		HX_STACK_VAR(_g21,"_g21");
					HX_STACK_LINE(685)
					while((true)){
						HX_STACK_LINE(685)
						if ((!(((_g31 < _g21))))){
							HX_STACK_LINE(685)
							break;
						}
						HX_STACK_LINE(685)
						int j = (_g31)++;		HX_STACK_VAR(j,"j");
						HX_STACK_LINE(687)
						{
							HX_STACK_LINE(687)
							::Tile _g41 = this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >();		HX_STACK_VAR(_g41,"_g41");
							HX_STACK_LINE(687)
							_g41->set_x((_g41->x + ((newPosX + shiftX))));
						}
						HX_STACK_LINE(688)
						{
							HX_STACK_LINE(688)
							::Tile _g41 = this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >();		HX_STACK_VAR(_g41,"_g41");
							HX_STACK_LINE(688)
							_g41->set_y((_g41->y + ((newPosY + shiftY))));
						}
						HX_STACK_LINE(689)
						{
							HX_STACK_LINE(689)
							::TileBoarder _g41 = this->boarder->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::TileBoarder >();		HX_STACK_VAR(_g41,"_g41");
							HX_STACK_LINE(689)
							_g41->set_x((_g41->x + ((newPosX + shiftX))));
						}
						HX_STACK_LINE(690)
						{
							HX_STACK_LINE(690)
							::TileBoarder _g41 = this->boarder->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::TileBoarder >();		HX_STACK_VAR(_g41,"_g41");
							HX_STACK_LINE(690)
							_g41->set_y((_g41->y + ((newPosY + shiftY))));
						}
						HX_STACK_LINE(691)
						::flixel::plugin::MouseEventManager_obj::add(this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >(),this->buyButtonPopUp_dyn(),this->placeObject_dyn(),null(),null(),null(),null(),null());
						HX_STACK_LINE(692)
						this->add(this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >());
						HX_STACK_LINE(693)
						this->add(this->boarder->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::TileBoarder >());
						HX_STACK_LINE(694)
						hx::AddEq(shiftX,(int)96);
						HX_STACK_LINE(695)
						hx::AddEq(shiftY,(int)48);
					}
				}
				HX_STACK_LINE(699)
				hx::SubEq(newPosX,(int)96);
				HX_STACK_LINE(700)
				hx::AddEq(newPosY,(int)48);
			}
		}
		HX_STACK_LINE(703)
		this->board->__get((int)2).StaticCast< Array< ::Dynamic > >()->__get((int)0).StaticCast< ::Tile >()->setHasObject(true);
		HX_STACK_LINE(704)
		this->board->__get((int)2).StaticCast< Array< ::Dynamic > >()->__get((int)0).StaticCast< ::Tile >()->setAsOwned();
		HX_STACK_LINE(705)
		this->board->__get((int)1).StaticCast< Array< ::Dynamic > >()->__get((int)0).StaticCast< ::Tile >()->setAsOwned();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,drawBoard,(void))

Void PlayState_obj::letItRain( ::Tree treeObj){
{
		HX_STACK_FRAME("PlayState","letItRain",0xd3d1fcc9,"PlayState.letItRain","PlayState.hx",709,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(treeObj,"treeObj")
		HX_STACK_LINE(710)
		::Cloud _g = ::Cloud_obj::__new();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(710)
		this->cloud = _g;
		HX_STACK_LINE(711)
		Float _g1 = treeObj->getTreeX();		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(711)
		this->cloud->setCloudX(_g1);
		HX_STACK_LINE(712)
		Float _g2 = treeObj->getTreeY();		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(712)
		Float _g3 = (_g2 - (int)50);		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(712)
		this->cloud->setCloudY(_g3);
		HX_STACK_LINE(713)
		this->aboveGrassLayer->add(this->cloud);
		HX_STACK_LINE(714)
		this->cloud = null();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,letItRain,(void))

Void PlayState_obj::letItShine( ::Tree treeObj){
{
		HX_STACK_FRAME("PlayState","letItShine",0x1bf24656,"PlayState.letItShine","PlayState.hx",717,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(treeObj,"treeObj")
		HX_STACK_LINE(719)
		::SunToken _g = ::SunToken_obj::__new();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(719)
		this->sun = _g;
		HX_STACK_LINE(720)
		Float _g1 = treeObj->getTreeX();		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(720)
		Float _g2 = (_g1 - (int)2);		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(720)
		this->sun->setSunX(_g2);
		HX_STACK_LINE(721)
		Float _g3 = treeObj->getTreeY();		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(721)
		Float _g4 = (_g3 - (int)50);		HX_STACK_VAR(_g4,"_g4");
		HX_STACK_LINE(721)
		this->sun->setSunY(_g4);
		HX_STACK_LINE(722)
		this->aboveGrassLayer->add(this->sun);
		HX_STACK_LINE(723)
		this->sun = null();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,letItShine,(void))

Void PlayState_obj::unregisterAllAlertEvents( ){
{
		HX_STACK_FRAME("PlayState","unregisterAllAlertEvents",0xaae15401,"PlayState.unregisterAllAlertEvents","PlayState.hx",728,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(728)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(728)
		int _g = this->treeArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(728)
		while((true)){
			HX_STACK_LINE(728)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(728)
				break;
			}
			HX_STACK_LINE(728)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(729)
			if (((this->treeArray->__get(i).StaticCast< ::Tree >() != null()))){
				HX_STACK_LINE(730)
				this->treeArray->__get(i).StaticCast< ::Tree >()->unregisterEvents();
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,unregisterAllAlertEvents,(void))

Void PlayState_obj::registerAllAlertEvents( ){
{
		HX_STACK_FRAME("PlayState","registerAllAlertEvents",0xa77b44a8,"PlayState.registerAllAlertEvents","PlayState.hx",737,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(737)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(737)
		int _g = this->treeArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(737)
		while((true)){
			HX_STACK_LINE(737)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(737)
				break;
			}
			HX_STACK_LINE(737)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(738)
			if (((this->treeArray->__get(i).StaticCast< ::Tree >() != null()))){
				HX_STACK_LINE(739)
				this->treeArray->__get(i).StaticCast< ::Tree >()->registerEvents();
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,registerAllAlertEvents,(void))

Void PlayState_obj::checkForAlertClick( ){
{
		HX_STACK_FRAME("PlayState","checkForAlertClick",0xd1ef603e,"PlayState.checkForAlertClick","PlayState.hx",745,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(747)
		{
			HX_STACK_LINE(747)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(747)
			int _g = this->treeArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(747)
			while((true)){
				HX_STACK_LINE(747)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(747)
					break;
				}
				HX_STACK_LINE(747)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(748)
				if (((this->treeArray->__get(i).StaticCast< ::Tree >() != null()))){
					HX_STACK_LINE(749)
					if (((  ((!(this->treeArray->__get(i).StaticCast< ::Tree >()->getDropletClicked()))) ? bool(this->treeArray->__get(i).StaticCast< ::Tree >()->getSunClicked()) : bool(true) ))){
						HX_STACK_LINE(750)
						::flixel::system::FlxSound _g2 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/alertButtonSound.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g2,"_g2");
						HX_STACK_LINE(750)
						this->alertButtonSound = _g2;
						HX_STACK_LINE(751)
						this->alertButtonSound->play(null());
						HX_STACK_LINE(752)
						Float _g11 = this->tutorial->getCurrentTutSectionNumber();		HX_STACK_VAR(_g11,"_g11");
						HX_STACK_LINE(752)
						if (((  (((_g11 == (int)3))) ? bool(!(this->tutUnpauseAlertsDone)) : bool(false) ))){
							HX_STACK_LINE(753)
							this->tutorial->destroy();
							HX_STACK_LINE(754)
							this->treeArray->__get((int)0).StaticCast< ::Tree >()->unpauseAlertTimers();
							HX_STACK_LINE(755)
							this->tutUnpauseAlertsDone = true;
						}
						HX_STACK_LINE(757)
						Float _g21 = this->tutorial->getCurrentTutSectionNumber();		HX_STACK_VAR(_g21,"_g21");
						struct _Function_5_1{
							inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
								HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",757,0xb30d7781)
								{
									HX_STACK_LINE(757)
									Float _g3 = __this->tutorial->getCurrentTutSectionNumber();		HX_STACK_VAR(_g3,"_g3");
									HX_STACK_LINE(757)
									return (_g3 == (int)4);
								}
								return null();
							}
						};
						HX_STACK_LINE(757)
						if (((  (((  ((!(((_g21 == (int)1))))) ? bool(_Function_5_1::Block(this)) : bool(true) ))) ? bool(this->tutorialOn) : bool(false) ))){
							HX_STACK_LINE(758)
							this->tutorial->destroy();
						}
						HX_STACK_LINE(760)
						if ((this->treeArray->__get(i).StaticCast< ::Tree >()->getSunClicked())){
							HX_STACK_LINE(761)
							this->sunClicked = true;
							HX_STACK_LINE(762)
							this->dropletClicked = false;
						}
						HX_STACK_LINE(764)
						if ((this->treeArray->__get(i).StaticCast< ::Tree >()->getDropletClicked())){
							HX_STACK_LINE(765)
							this->dropletClicked = true;
							HX_STACK_LINE(766)
							this->sunClicked = false;
						}
						HX_STACK_LINE(771)
						this->bb->destroy();
						HX_STACK_LINE(772)
						this->bb = null();
						HX_STACK_LINE(773)
						::ProblemBoard _g4 = ::ProblemBoard_obj::__new();		HX_STACK_VAR(_g4,"_g4");
						HX_STACK_LINE(773)
						this->bb = _g4;
						HX_STACK_LINE(774)
						this->currentTargetTree = this->treeArray->__get(i).StaticCast< ::Tree >();
						HX_STACK_LINE(775)
						this->unregisterAllAlertEvents();
						HX_STACK_LINE(776)
						this->add(this->numPad);
						HX_STACK_LINE(777)
						this->add(this->bb);
						HX_STACK_LINE(778)
						this->add(this->answerFieldText);
						HX_STACK_LINE(779)
						this->numPad->setPadScrollingIn(true);
						HX_STACK_LINE(780)
						this->numPad->registerEvent();
						HX_STACK_LINE(781)
						this->bb->pickOperation(HX_CSTRING("tree"));
						HX_STACK_LINE(782)
						this->bb->setBoardInUse(true);
						HX_STACK_LINE(783)
						this->overlaysInUse = true;
						HX_STACK_LINE(784)
						this->treeArray->__get(i).StaticCast< ::Tree >()->setDropletClicked(false);
						HX_STACK_LINE(785)
						this->treeArray->__get(i).StaticCast< ::Tree >()->setSunClicked(false);
					}
				}
			}
		}
		HX_STACK_LINE(790)
		{
			HX_STACK_LINE(790)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(790)
			int _g = this->juiceMachineArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(790)
			while((true)){
				HX_STACK_LINE(790)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(790)
					break;
				}
				HX_STACK_LINE(790)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(791)
				if (((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >() != null()))){
					HX_STACK_LINE(792)
					if ((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->getRepairClicked())){
						HX_STACK_LINE(793)
						this->members->__Field(HX_CSTRING("remove"),true)(this->numPad);
						HX_STACK_LINE(794)
						this->members->__Field(HX_CSTRING("remove"),true)(this->bb);
						HX_STACK_LINE(795)
						this->members->__Field(HX_CSTRING("remove"),true)(this->answerFieldText);
						HX_STACK_LINE(796)
						this->currentTargetJuiceMachine = this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >();
						HX_STACK_LINE(797)
						this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->unregisterMachineEvents();
						HX_STACK_LINE(798)
						this->add(this->numPad);
						HX_STACK_LINE(799)
						this->add(this->bb);
						HX_STACK_LINE(800)
						this->add(this->answerFieldText);
						HX_STACK_LINE(801)
						this->numPad->setPadScrollingIn(true);
						HX_STACK_LINE(802)
						this->numPad->registerEvent();
						HX_STACK_LINE(803)
						this->bb->pickOperation(HX_CSTRING("machine"));
						HX_STACK_LINE(804)
						this->bb->setBoardInUse(true);
						HX_STACK_LINE(805)
						this->overlaysInUse = true;
						HX_STACK_LINE(806)
						this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->setRepairClicked(false);
					}
				}
			}
		}
		HX_STACK_LINE(810)
		{
			HX_STACK_LINE(810)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(810)
			int _g = this->pieMachineArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(810)
			while((true)){
				HX_STACK_LINE(810)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(810)
					break;
				}
				HX_STACK_LINE(810)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(811)
				if (((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >() != null()))){
					HX_STACK_LINE(812)
					if ((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->getRepairClicked())){
						HX_STACK_LINE(813)
						this->members->__Field(HX_CSTRING("remove"),true)(this->numPad);
						HX_STACK_LINE(814)
						this->members->__Field(HX_CSTRING("remove"),true)(this->bb);
						HX_STACK_LINE(815)
						this->members->__Field(HX_CSTRING("remove"),true)(this->answerFieldText);
						HX_STACK_LINE(816)
						this->currentTargetPieMachine = this->pieMachineArray->__get(i).StaticCast< ::PieMachine >();
						HX_STACK_LINE(817)
						this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->unregisterMachineEvents();
						HX_STACK_LINE(818)
						this->add(this->numPad);
						HX_STACK_LINE(819)
						this->add(this->bb);
						HX_STACK_LINE(820)
						this->add(this->answerFieldText);
						HX_STACK_LINE(821)
						this->numPad->setPadScrollingIn(true);
						HX_STACK_LINE(822)
						this->numPad->registerEvent();
						HX_STACK_LINE(823)
						this->bb->pickOperation(HX_CSTRING("machine"));
						HX_STACK_LINE(824)
						this->bb->setBoardInUse(true);
						HX_STACK_LINE(825)
						this->overlaysInUse = true;
						HX_STACK_LINE(826)
						this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->setRepairClicked(false);
					}
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,checkForAlertClick,(void))

Void PlayState_obj::submitAnswer( ){
{
		HX_STACK_FRAME("PlayState","submitAnswer",0xe4355f67,"PlayState.submitAnswer","PlayState.hx",834,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(834)
		if ((this->numPad->getAnswerButtonPressed())){
			HX_STACK_LINE(835)
			this->numPad->unregisterNumPadEvents();
			HX_STACK_LINE(836)
			::String _g = this->numPad->getPlayerAnswerString();		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(836)
			::String _g1 = this->bb->getProblemAnswerString();		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(836)
			if (((_g == _g1))){
				HX_STACK_LINE(837)
				::flixel::system::FlxSound _g2 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/correctAnswerSoundA.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(837)
				this->correctAnswerSound = _g2;
				HX_STACK_LINE(838)
				this->correctAnswerSound->play(null());
				HX_STACK_LINE(839)
				this->bb->setBoarderColor(HX_CSTRING("correctAnswer"));
				HX_STACK_LINE(840)
				if ((this->dropletClicked)){
					struct _Function_4_1{
						inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
							HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",841,0xb30d7781)
							{
								HX_STACK_LINE(841)
								int _g3 = __this->treeArray->__get((int)0).StaticCast< ::Tree >()->getTreeGrowthPhase();		HX_STACK_VAR(_g3,"_g3");
								HX_STACK_LINE(841)
								return (_g3 < (int)6);
							}
							return null();
						}
					};
					HX_STACK_LINE(841)
					if (((  ((this->tutorialOn)) ? bool(_Function_4_1::Block(this)) : bool(false) ))){
						HX_STACK_LINE(842)
						this->treeArray->__get((int)0).StaticCast< ::Tree >()->setThirstValue((int)0);
						HX_STACK_LINE(843)
						this->treeArray->__get((int)0).StaticCast< ::Tree >()->incGrowTree();
						HX_STACK_LINE(844)
						this->treeArray->__get((int)0).StaticCast< ::Tree >()->setSunlightValue((int)1);
					}
					HX_STACK_LINE(846)
					this->currentTargetTree->setTreeWatered();
					HX_STACK_LINE(847)
					this->currentTargetTree->incGrowTree();
					HX_STACK_LINE(848)
					this->letItRain(this->currentTargetTree);
					HX_STACK_LINE(849)
					this->currentTargetTree->resetDropTimer();
					HX_STACK_LINE(850)
					this->dropletClicked = false;
				}
				else{
					HX_STACK_LINE(852)
					if ((this->sunClicked)){
						struct _Function_5_1{
							inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
								HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",853,0xb30d7781)
								{
									HX_STACK_LINE(853)
									int _g4 = __this->treeArray->__get((int)0).StaticCast< ::Tree >()->getTreeGrowthPhase();		HX_STACK_VAR(_g4,"_g4");
									HX_STACK_LINE(853)
									return (_g4 < (int)6);
								}
								return null();
							}
						};
						HX_STACK_LINE(853)
						if (((  ((this->tutorialOn)) ? bool(_Function_5_1::Block(this)) : bool(false) ))){
							HX_STACK_LINE(854)
							this->treeArray->__get((int)0).StaticCast< ::Tree >()->setSunlightValue((int)0);
							HX_STACK_LINE(855)
							this->treeArray->__get((int)0).StaticCast< ::Tree >()->incGrowTree();
							HX_STACK_LINE(856)
							this->treeArray->__get((int)0).StaticCast< ::Tree >()->setThirstValue((int)1);
						}
						HX_STACK_LINE(858)
						this->currentTargetTree->setTreeSunLight();
						HX_STACK_LINE(859)
						this->currentTargetTree->incGrowTree();
						HX_STACK_LINE(860)
						this->letItShine(this->currentTargetTree);
						HX_STACK_LINE(861)
						this->currentTargetTree->resetSunTimer();
						HX_STACK_LINE(862)
						this->sunClicked = false;
					}
					else{
						HX_STACK_LINE(864)
						if (((this->currentTargetJuiceMachine != null()))){
							HX_STACK_LINE(865)
							this->currentTargetJuiceMachine->setMachineOn(false);
							HX_STACK_LINE(866)
							this->currentTargetJuiceMachine->resetRepairAlertTimer();
							HX_STACK_LINE(867)
							this->currentTargetJuiceMachine->setAnimateRepairSprite(false);
							HX_STACK_LINE(868)
							this->currentTargetJuiceMachine = null();
						}
						else{
							HX_STACK_LINE(871)
							this->currentTargetPieMachine->setMachineOn(false);
							HX_STACK_LINE(872)
							this->currentTargetPieMachine->resetRepairAlertTimer();
							HX_STACK_LINE(873)
							this->currentTargetPieMachine->setAnimateRepairSprite(false);
						}
					}
				}
			}
			else{
				HX_STACK_LINE(877)
				if ((this->tutorialOn)){
					HX_STACK_LINE(878)
					int _g5 = this->treeArray->__get((int)0).StaticCast< ::Tree >()->getTreeGrowthPhase();		HX_STACK_VAR(_g5,"_g5");
					HX_STACK_LINE(878)
					if (((_g5 > (int)5))){
						HX_STACK_LINE(879)
						this->bb->setBoarderColor(HX_CSTRING("incorrectAnswer"));
						HX_STACK_LINE(880)
						if ((this->dropletClicked)){
							HX_STACK_LINE(881)
							this->currentTargetTree->setTreeThirsty(null());
						}
						else{
							HX_STACK_LINE(883)
							if ((this->sunClicked)){
								HX_STACK_LINE(884)
								this->currentTargetTree->setTreeNoLight(null());
							}
						}
					}
				}
				else{
					HX_STACK_LINE(889)
					this->bb->setBoarderColor(HX_CSTRING("incorrectAnswer"));
					HX_STACK_LINE(890)
					::flixel::system::FlxSound _g6 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/incorrectAnswerSound.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g6,"_g6");
					HX_STACK_LINE(890)
					this->incorrectAnswerSound = _g6;
					HX_STACK_LINE(891)
					this->incorrectAnswerSound->play(null());
					HX_STACK_LINE(892)
					if ((this->dropletClicked)){
						HX_STACK_LINE(893)
						this->currentTargetTree->setTreeThirsty(null());
					}
					else{
						HX_STACK_LINE(895)
						if ((this->sunClicked)){
							HX_STACK_LINE(896)
							this->currentTargetTree->setTreeNoLight(null());
						}
					}
				}
			}
			HX_STACK_LINE(901)
			this->bb->setBoardInUse(false);
			HX_STACK_LINE(902)
			this->numPad->setPadScrollingIn(false);
			HX_STACK_LINE(903)
			this->overlaysInUse = false;
			HX_STACK_LINE(904)
			this->numPad->resetAnswerDigits();
			HX_STACK_LINE(905)
			this->bb->resetProblemString();
			HX_STACK_LINE(906)
			this->registerAllAlertEvents();
			HX_STACK_LINE(907)
			this->numPad->setAnswerButtonPressed(false);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,submitAnswer,(void))

Void PlayState_obj::destroyObjects( ){
{
		HX_STACK_FRAME("PlayState","destroyObjects",0x58f9d8eb,"PlayState.destroyObjects","PlayState.hx",914,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(914)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(914)
		int _g = this->treeArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(914)
		while((true)){
			HX_STACK_LINE(914)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(914)
				break;
			}
			HX_STACK_LINE(914)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(915)
			if (((this->treeArray->__get(i).StaticCast< ::Tree >() != null()))){
				HX_STACK_LINE(916)
				if ((this->treeArray->__get(i).StaticCast< ::Tree >()->getAmIDeadYet())){
					HX_STACK_LINE(917)
					::flixel::FlxSprite _g2 = this->treeArray->__get(i).StaticCast< ::Tree >()->getTreeSprite();		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(917)
					::flixel::plugin::MouseEventManager_obj::remove(_g2);
					HX_STACK_LINE(918)
					this->treeArray->__get(i).StaticCast< ::Tree >()->getMyTile()->setHasObject(false);
					HX_STACK_LINE(919)
					if (((this->currentTargetTree == this->treeArray->__get(i).StaticCast< ::Tree >()))){
						HX_STACK_LINE(920)
						this->currentTargetTree = null();
						HX_STACK_LINE(921)
						::haxe::Log_obj::trace(HX_CSTRING("currentTargetTree deleted "),hx::SourceInfo(HX_CSTRING("PlayState.hx"),921,HX_CSTRING("PlayState"),HX_CSTRING("destroyObjects")));
					}
					HX_STACK_LINE(923)
					this->treeArray->__get(i).StaticCast< ::Tree >()->destroy();
					HX_STACK_LINE(924)
					this->treeArray[i] = null();
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,destroyObjects,(void))

Void PlayState_obj::waterBill( ){
{
		HX_STACK_FRAME("PlayState","waterBill",0x000b536d,"PlayState.waterBill","PlayState.hx",930,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(931)
		int clientCount = (int)0;		HX_STACK_VAR(clientCount,"clientCount");
		HX_STACK_LINE(932)
		{
			HX_STACK_LINE(932)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(932)
			int _g = this->treeArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(932)
			while((true)){
				HX_STACK_LINE(932)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(932)
					break;
				}
				HX_STACK_LINE(932)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(933)
				if (((this->treeArray->__get(i).StaticCast< ::Tree >() != null()))){
					HX_STACK_LINE(934)
					if ((this->treeArray->__get(i).StaticCast< ::Tree >()->getSprinklerSystemOn())){
						HX_STACK_LINE(935)
						(clientCount)++;
					}
				}
			}
		}
		HX_STACK_LINE(939)
		this->market->setWaterClientCount(clientCount);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,waterBill,(void))

Void PlayState_obj::electricBill( ){
{
		HX_STACK_FRAME("PlayState","electricBill",0x71cce235,"PlayState.electricBill","PlayState.hx",942,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(943)
		int clientCount = (int)0;		HX_STACK_VAR(clientCount,"clientCount");
		HX_STACK_LINE(944)
		{
			HX_STACK_LINE(944)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(944)
			int _g = this->juiceMachineArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(944)
			while((true)){
				HX_STACK_LINE(944)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(944)
					break;
				}
				HX_STACK_LINE(944)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(945)
				if (((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >() != null()))){
					HX_STACK_LINE(946)
					if ((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->getMachineOn())){
						HX_STACK_LINE(947)
						(clientCount)++;
					}
				}
			}
		}
		HX_STACK_LINE(951)
		{
			HX_STACK_LINE(951)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(951)
			int _g = this->pieMachineArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(951)
			while((true)){
				HX_STACK_LINE(951)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(951)
					break;
				}
				HX_STACK_LINE(951)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(952)
				if (((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >() != null()))){
					HX_STACK_LINE(953)
					if ((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->getMachineOn())){
						HX_STACK_LINE(954)
						(clientCount)++;
					}
				}
			}
		}
		HX_STACK_LINE(958)
		this->market->setElectricClientCount(clientCount);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,electricBill,(void))

Void PlayState_obj::appleBill( int machinesInUse){
{
		HX_STACK_FRAME("PlayState","appleBill",0xe3351e30,"PlayState.appleBill","PlayState.hx",961,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(machinesInUse,"machinesInUse")
		HX_STACK_LINE(962)
		hx::SubEq(this->appleForJuiceTimer,::flixel::FlxG_obj::elapsed);
		HX_STACK_LINE(963)
		if (((this->appleCount > (int)1))){
			HX_STACK_LINE(964)
			if (((this->appleForJuiceTimer < (int)0))){
				HX_STACK_LINE(965)
				hx::SubEq(this->appleCount,(machinesInUse * (int)2));
				HX_STACK_LINE(966)
				this->market->stockJuicePacks(machinesInUse);
				HX_STACK_LINE(968)
				this->appleForJuiceTimer = (int)1;
			}
		}
		else{
			HX_STACK_LINE(972)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(972)
			int _g = this->juiceMachineArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(972)
			while((true)){
				HX_STACK_LINE(972)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(972)
					break;
				}
				HX_STACK_LINE(972)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(973)
				if (((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >() != null()))){
					HX_STACK_LINE(974)
					this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->setFillingBottle(false);
					HX_STACK_LINE(975)
					this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->setMachineOn(true);
					HX_STACK_LINE(976)
					this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->turnMachineOn(null());
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,appleBill,(void))

Void PlayState_obj::appleAndJuiceBill( int machinesInUse){
{
		HX_STACK_FRAME("PlayState","appleAndJuiceBill",0xcf1514d9,"PlayState.appleAndJuiceBill","PlayState.hx",982,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(machinesInUse,"machinesInUse")
		HX_STACK_LINE(983)
		hx::SubEq(this->appleAndJuiceForPieTimer,::flixel::FlxG_obj::elapsed);
		struct _Function_1_1{
			inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",984,0xb30d7781)
				{
					HX_STACK_LINE(984)
					int _g = __this->market->getJuicePackCount();		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(984)
					return (_g > (int)0);
				}
				return null();
			}
		};
		HX_STACK_LINE(984)
		if (((  (((this->appleCount > (int)1))) ? bool(_Function_1_1::Block(this)) : bool(false) ))){
			HX_STACK_LINE(985)
			if (((this->appleAndJuiceForPieTimer < (int)0))){
				HX_STACK_LINE(986)
				hx::SubEq(this->appleCount,(machinesInUse * (int)2));
				HX_STACK_LINE(987)
				this->market->decjuicePackCount(machinesInUse);
				HX_STACK_LINE(988)
				this->market->stockPiePacks(machinesInUse);
				HX_STACK_LINE(990)
				this->appleAndJuiceForPieTimer = (int)1;
			}
		}
		else{
			HX_STACK_LINE(994)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(994)
			int _g = this->juiceMachineArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(994)
			while((true)){
				HX_STACK_LINE(994)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(994)
					break;
				}
				HX_STACK_LINE(994)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(995)
				if (((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >() != null()))){
					HX_STACK_LINE(996)
					this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->setMachineOn(true);
					HX_STACK_LINE(997)
					this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->turnMachineOn(null());
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,appleAndJuiceBill,(void))

Void PlayState_obj::turnWaterOff( ){
{
		HX_STACK_FRAME("PlayState","turnWaterOff",0x16ba7be6,"PlayState.turnWaterOff","PlayState.hx",1003,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1004)
		int _g = this->market->getCoinCount();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(1004)
		if (((_g <= (int)0))){
			HX_STACK_LINE(1005)
			this->market->setCoinCount((int)0);
			HX_STACK_LINE(1006)
			{
				HX_STACK_LINE(1006)
				int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(1006)
				int _g2 = this->treeArray->length;		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(1006)
				while((true)){
					HX_STACK_LINE(1006)
					if ((!(((_g1 < _g2))))){
						HX_STACK_LINE(1006)
						break;
					}
					HX_STACK_LINE(1006)
					int i = (_g1)++;		HX_STACK_VAR(i,"i");
					HX_STACK_LINE(1007)
					if (((this->treeArray->__get(i).StaticCast< ::Tree >() != null()))){
						HX_STACK_LINE(1008)
						this->treeArray->__get(i).StaticCast< ::Tree >()->setSprinklerSystemOn(false);
					}
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,turnWaterOff,(void))

Void PlayState_obj::applesAndJuiceForPie( ){
{
		HX_STACK_FRAME("PlayState","applesAndJuiceForPie",0x68212b16,"PlayState.applesAndJuiceForPie","PlayState.hx",1015,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1015)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(1015)
		int _g = this->pieMachineArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(1015)
		while((true)){
			HX_STACK_LINE(1015)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(1015)
				break;
			}
			HX_STACK_LINE(1015)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(1016)
			if (((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >() != null()))){
				struct _Function_3_1{
					inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",1017,0xb30d7781)
						{
							HX_STACK_LINE(1017)
							int _g2 = __this->market->getJuicePackCount();		HX_STACK_VAR(_g2,"_g2");
							HX_STACK_LINE(1017)
							return (_g2 > (int)0);
						}
						return null();
					}
				};
				HX_STACK_LINE(1017)
				if (((  (((  (((this->appleCount > (int)1))) ? bool(_Function_3_1::Block(this)) : bool(false) ))) ? bool(this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->getMachineOn()) : bool(false) ))){
					HX_STACK_LINE(1018)
					hx::SubEq(this->appleAndJuiceForPieTimer,::flixel::FlxG_obj::elapsed);
					HX_STACK_LINE(1019)
					if (((this->appleAndJuiceForPieTimer < (int)0))){
						HX_STACK_LINE(1020)
						hx::SubEq(this->appleCount,(int)2);
						HX_STACK_LINE(1021)
						this->market->decjuicePackCount((int)1);
						HX_STACK_LINE(1022)
						this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->setPastryFlying(true);
						HX_STACK_LINE(1023)
						::flixel::FlxSprite _g11 = this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->getPieMachineFlyingPastry();		HX_STACK_VAR(_g11,"_g11");
						HX_STACK_LINE(1023)
						this->add(_g11);
						HX_STACK_LINE(1024)
						this->appleAndJuiceForPieTimer = (int)1;
					}
				}
				else{
					HX_STACK_LINE(1028)
					if (((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >() != null()))){
						HX_STACK_LINE(1029)
						this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->setMachineOn(true);
						HX_STACK_LINE(1030)
						this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->turnMachineOn(null());
					}
				}
				HX_STACK_LINE(1033)
				if ((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->getInPiePack())){
					HX_STACK_LINE(1034)
					this->market->stockPiePacks((int)1);
					HX_STACK_LINE(1035)
					this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->setInPiePack(false);
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,applesAndJuiceForPie,(void))

Void PlayState_obj::applesForJuice( ){
{
		HX_STACK_FRAME("PlayState","applesForJuice",0x9d206881,"PlayState.applesForJuice","PlayState.hx",1042,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1042)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(1042)
		int _g = this->juiceMachineArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(1042)
		while((true)){
			HX_STACK_LINE(1042)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(1042)
				break;
			}
			HX_STACK_LINE(1042)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(1043)
			if (((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >() != null()))){
				HX_STACK_LINE(1044)
				if (((  (((this->appleCount > (int)1))) ? bool(this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->getMachineOn()) : bool(false) ))){
					HX_STACK_LINE(1045)
					hx::SubEq(this->appleForJuiceTimer,::flixel::FlxG_obj::elapsed);
					HX_STACK_LINE(1046)
					if (((this->appleForJuiceTimer < (int)0))){
						HX_STACK_LINE(1047)
						hx::SubEq(this->appleCount,(int)2);
						HX_STACK_LINE(1048)
						this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->setBottleFlying(true);
						HX_STACK_LINE(1049)
						::flixel::FlxSprite _g2 = this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->getJuiceMachineFlyingBotte();		HX_STACK_VAR(_g2,"_g2");
						HX_STACK_LINE(1049)
						this->add(_g2);
						HX_STACK_LINE(1050)
						this->appleForJuiceTimer = (int)1;
					}
				}
				else{
					HX_STACK_LINE(1054)
					if (((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >() != null()))){
						HX_STACK_LINE(1055)
						this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->setFillingBottle(false);
						HX_STACK_LINE(1056)
						this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->setMachineOn(true);
						HX_STACK_LINE(1057)
						this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->turnMachineOn(null());
					}
				}
				HX_STACK_LINE(1060)
				if ((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->getInJuicePack())){
					HX_STACK_LINE(1061)
					this->market->stockJuicePacks((int)1);
					HX_STACK_LINE(1062)
					this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->setInJuicePack(false);
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,applesForJuice,(void))

Void PlayState_obj::waterSprinklerBill( ){
{
		HX_STACK_FRAME("PlayState","waterSprinklerBill",0xa60b65a9,"PlayState.waterSprinklerBill","PlayState.hx",1069,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1069)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(1069)
		int _g = this->treeArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(1069)
		while((true)){
			HX_STACK_LINE(1069)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(1069)
				break;
			}
			HX_STACK_LINE(1069)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(1070)
			if (((this->treeArray->__get(i).StaticCast< ::Tree >() != null()))){
				HX_STACK_LINE(1071)
				if ((this->treeArray->__get(i).StaticCast< ::Tree >()->getPayWaterBill())){
					HX_STACK_LINE(1072)
					this->market->coinPaymentUtil((int)1);
					HX_STACK_LINE(1073)
					this->treeArray->__get(i).StaticCast< ::Tree >()->setPayWaterBill(false);
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,waterSprinklerBill,(void))

Void PlayState_obj::factoryElectricBill( ){
{
		HX_STACK_FRAME("PlayState","factoryElectricBill",0x5caeb2dd,"PlayState.factoryElectricBill","PlayState.hx",1079,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1080)
		{
			HX_STACK_LINE(1080)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(1080)
			int _g = this->juiceMachineArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(1080)
			while((true)){
				HX_STACK_LINE(1080)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(1080)
					break;
				}
				HX_STACK_LINE(1080)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(1081)
				if (((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >() != null()))){
					HX_STACK_LINE(1082)
					if ((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->getPayElectricBill())){
						HX_STACK_LINE(1083)
						this->market->coinPaymentUtil((int)1);
						HX_STACK_LINE(1084)
						this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->setPayElectricBill(false);
					}
				}
			}
		}
		HX_STACK_LINE(1088)
		{
			HX_STACK_LINE(1088)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(1088)
			int _g = this->pieMachineArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(1088)
			while((true)){
				HX_STACK_LINE(1088)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(1088)
					break;
				}
				HX_STACK_LINE(1088)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(1089)
				if (((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >() != null()))){
					HX_STACK_LINE(1090)
					if ((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->getPayElectricBill())){
						HX_STACK_LINE(1091)
						this->market->coinPaymentUtil((int)1);
						HX_STACK_LINE(1092)
						this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->setPayElectricBill(false);
					}
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,factoryElectricBill,(void))

Void PlayState_obj::removeCallBackOnPad( ){
{
		HX_STACK_FRAME("PlayState","removeCallBackOnPad",0x8efa3eda,"PlayState.removeCallBackOnPad","PlayState.hx",1098,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1099)
		if (((  ((this->numPad->getPadScrollingIn())) ? bool(this->callBackStopOne) : bool(false) ))){
			HX_STACK_LINE(1100)
			{
				HX_STACK_LINE(1100)
				int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(1100)
				int _g = this->treeArray->length;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(1100)
				while((true)){
					HX_STACK_LINE(1100)
					if ((!(((_g1 < _g))))){
						HX_STACK_LINE(1100)
						break;
					}
					HX_STACK_LINE(1100)
					int i = (_g1)++;		HX_STACK_VAR(i,"i");
					HX_STACK_LINE(1101)
					if (((this->treeArray->__get(i).StaticCast< ::Tree >() != null()))){
						HX_STACK_LINE(1102)
						this->treeArray->__get(i).StaticCast< ::Tree >()->unregisterTreeClickedMouseEvent();
					}
				}
			}
			HX_STACK_LINE(1105)
			{
				HX_STACK_LINE(1105)
				int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(1105)
				int _g = this->juiceMachineArray->length;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(1105)
				while((true)){
					HX_STACK_LINE(1105)
					if ((!(((_g1 < _g))))){
						HX_STACK_LINE(1105)
						break;
					}
					HX_STACK_LINE(1105)
					int i = (_g1)++;		HX_STACK_VAR(i,"i");
					HX_STACK_LINE(1106)
					if (((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >() != null()))){
						HX_STACK_LINE(1107)
						this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->unregisterMachineEvents();
					}
				}
			}
			HX_STACK_LINE(1110)
			{
				HX_STACK_LINE(1110)
				int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(1110)
				int _g = this->pieMachineArray->length;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(1110)
				while((true)){
					HX_STACK_LINE(1110)
					if ((!(((_g1 < _g))))){
						HX_STACK_LINE(1110)
						break;
					}
					HX_STACK_LINE(1110)
					int i = (_g1)++;		HX_STACK_VAR(i,"i");
					HX_STACK_LINE(1111)
					if (((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >() != null()))){
						HX_STACK_LINE(1112)
						this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->unregisterMachineEvents();
					}
				}
			}
			HX_STACK_LINE(1115)
			this->callBackStopOne = false;
			HX_STACK_LINE(1116)
			this->callBackStopTwo = true;
		}
		HX_STACK_LINE(1118)
		if (((  ((!(this->numPad->getPadScrollingIn()))) ? bool(this->callBackStopTwo) : bool(false) ))){
			HX_STACK_LINE(1119)
			{
				HX_STACK_LINE(1119)
				int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(1119)
				int _g = this->treeArray->length;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(1119)
				while((true)){
					HX_STACK_LINE(1119)
					if ((!(((_g1 < _g))))){
						HX_STACK_LINE(1119)
						break;
					}
					HX_STACK_LINE(1119)
					int i = (_g1)++;		HX_STACK_VAR(i,"i");
					HX_STACK_LINE(1120)
					if (((this->treeArray->__get(i).StaticCast< ::Tree >() != null()))){
						HX_STACK_LINE(1121)
						this->treeArray->__get(i).StaticCast< ::Tree >()->registerTreeClickedMouseEvent();
					}
				}
			}
			HX_STACK_LINE(1124)
			{
				HX_STACK_LINE(1124)
				int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(1124)
				int _g = this->juiceMachineArray->length;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(1124)
				while((true)){
					HX_STACK_LINE(1124)
					if ((!(((_g1 < _g))))){
						HX_STACK_LINE(1124)
						break;
					}
					HX_STACK_LINE(1124)
					int i = (_g1)++;		HX_STACK_VAR(i,"i");
					HX_STACK_LINE(1125)
					if (((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >() != null()))){
						HX_STACK_LINE(1126)
						this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->registerMachineEvents();
					}
				}
			}
			HX_STACK_LINE(1129)
			{
				HX_STACK_LINE(1129)
				int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(1129)
				int _g = this->pieMachineArray->length;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(1129)
				while((true)){
					HX_STACK_LINE(1129)
					if ((!(((_g1 < _g))))){
						HX_STACK_LINE(1129)
						break;
					}
					HX_STACK_LINE(1129)
					int i = (_g1)++;		HX_STACK_VAR(i,"i");
					HX_STACK_LINE(1130)
					if (((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >() != null()))){
						HX_STACK_LINE(1131)
						this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->registerMachineEvents();
					}
				}
			}
			HX_STACK_LINE(1134)
			this->callBackStopOne = true;
			HX_STACK_LINE(1135)
			this->callBackStopTwo = false;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,removeCallBackOnPad,(void))

Void PlayState_obj::createAsteroidBelt( ){
{
		HX_STACK_FRAME("PlayState","createAsteroidBelt",0x33efcd73,"PlayState.createAsteroidBelt","PlayState.hx",1140,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1140)
		if (((this->asteroidBelt->getAsteroidBeltSprite()->x == (int)0))){
			HX_STACK_LINE(1141)
			::AsteroidBelt _g = ::AsteroidBelt_obj::__new((int)-700);		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(1141)
			this->asteroidBelt = _g;
			HX_STACK_LINE(1142)
			this->belowBottomSprites->add(this->asteroidBelt);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,createAsteroidBelt,(void))

Void PlayState_obj::currentTreeGrowthValue( ){
{
		HX_STACK_FRAME("PlayState","currentTreeGrowthValue",0x789dafa4,"PlayState.currentTreeGrowthValue","PlayState.hx",1147,0xb30d7781)
		HX_STACK_THIS(this)
		struct _Function_1_1{
			inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",1148,0xb30d7781)
				{
					HX_STACK_LINE(1148)
					Float _g = __this->tutorial->getCurrentTutSectionNumber();		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(1148)
					return (_g < (int)2);
				}
				return null();
			}
		};
		HX_STACK_LINE(1148)
		if (((  (((  (((this->treeArray->__get((int)0).StaticCast< ::Tree >() != null()))) ? bool(_Function_1_1::Block(this)) : bool(false) ))) ? bool(this->tutorialOn) : bool(false) ))){
			HX_STACK_LINE(1149)
			int _g1 = this->treeArray->__get((int)0).StaticCast< ::Tree >()->getTreeGrowthPhase();		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(1149)
			this->tutorial->getTutTreeGrowthVal(_g1);
			HX_STACK_LINE(1150)
			int _g2 = this->treeArray->__get((int)0).StaticCast< ::Tree >()->getTreeGrowthPhase();		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(1150)
			if (((  (((_g2 == (int)6))) ? bool(!(this->closeTreeGrowthCheck)) : bool(false) ))){
				HX_STACK_LINE(1151)
				this->treeArray->__get((int)0).StaticCast< ::Tree >()->setTreeSunlightTut((int)10);
				HX_STACK_LINE(1152)
				this->treeArray->__get((int)0).StaticCast< ::Tree >()->setTreeThirstTut((int)15);
				HX_STACK_LINE(1153)
				this->tutorial->destroy();
				HX_STACK_LINE(1154)
				::Tutorial _g3 = ::Tutorial_obj::__new((int)2);		HX_STACK_VAR(_g3,"_g3");
				HX_STACK_LINE(1154)
				this->tutorial = _g3;
				HX_STACK_LINE(1155)
				this->add(this->tutorial);
				HX_STACK_LINE(1156)
				this->closeTreeGrowthCheck = true;
			}
		}
		HX_STACK_LINE(1159)
		if (((bool((bool((this->appleCount == (int)10)) && bool(!(this->closeAppleCountCheck)))) && bool(this->tutorialOn)))){
			HX_STACK_LINE(1160)
			this->tutorial->destroy();
			HX_STACK_LINE(1161)
			::Tutorial _g4 = ::Tutorial_obj::__new((int)3);		HX_STACK_VAR(_g4,"_g4");
			HX_STACK_LINE(1161)
			this->tutorial = _g4;
			HX_STACK_LINE(1162)
			this->add(this->tutorial);
			HX_STACK_LINE(1163)
			this->treeArray->__get((int)0).StaticCast< ::Tree >()->setSunlightValue((int)1);
			HX_STACK_LINE(1164)
			this->treeArray->__get((int)0).StaticCast< ::Tree >()->pauseAlertTimers();
			HX_STACK_LINE(1165)
			this->closeAppleCountCheck = true;
		}
		HX_STACK_LINE(1167)
		if (((this->treeArray->__get((int)0).StaticCast< ::Tree >() != null()))){
			HX_STACK_LINE(1168)
			int _g5 = this->treeArray->__get((int)0).StaticCast< ::Tree >()->getTreeGrowthPhase();		HX_STACK_VAR(_g5,"_g5");
			HX_STACK_LINE(1168)
			if (((  (((  (((_g5 == (int)1))) ? bool(this->tutorialOn) : bool(false) ))) ? bool(!(this->closeSecondTreeGrowthCheck)) : bool(false) ))){
				HX_STACK_LINE(1169)
				this->tutorial->destroy();
				HX_STACK_LINE(1170)
				::Tutorial _g6 = ::Tutorial_obj::__new(1.5);		HX_STACK_VAR(_g6,"_g6");
				HX_STACK_LINE(1170)
				this->tutorial = _g6;
				HX_STACK_LINE(1171)
				this->add(this->tutorial);
				HX_STACK_LINE(1172)
				this->closeSecondTreeGrowthCheck = true;
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,currentTreeGrowthValue,(void))

Void PlayState_obj::marketTutOpened( ){
{
		HX_STACK_FRAME("PlayState","marketTutOpened",0xcca3666f,"PlayState.marketTutOpened","PlayState.hx",1180,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1180)
		if (((bool(this->marketOpenCheck) && bool(this->tutorialOn)))){
			HX_STACK_LINE(1181)
			Float _g = this->tutorial->getCurrentTutSectionNumber();		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(1181)
			if (((_g == (int)4))){
				HX_STACK_LINE(1182)
				this->treeArray->__get((int)0).StaticCast< ::Tree >()->setThirstValue((int)0);
				HX_STACK_LINE(1183)
				this->treeArray->__get((int)0).StaticCast< ::Tree >()->setSunlightValue((int)1);
				HX_STACK_LINE(1184)
				if ((this->market->getMarketOpen())){
					HX_STACK_LINE(1185)
					this->tutorial->destroy();
					HX_STACK_LINE(1186)
					this->marketOpenCheck = false;
					HX_STACK_LINE(1187)
					this->tutorialOn = false;
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,marketTutOpened,(void))

Void PlayState_obj::restartGameOver( ){
{
		HX_STACK_FRAME("PlayState","restartGameOver",0x6d7c2e44,"PlayState.restartGameOver","PlayState.hx",1194,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1194)
		if (((this->menuScreen != null()))){
			HX_STACK_LINE(1195)
			bool _g = this->menuScreen->getGameRestart();		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(1195)
			if (((_g == true))){
				HX_STACK_LINE(1196)
				this->tutorial->destroy();
				HX_STACK_LINE(1197)
				this->tutorialOn = false;
				HX_STACK_LINE(1198)
				this->add(this->tutorial);
				HX_STACK_LINE(1199)
				this->appleCount = (int)0;
				HX_STACK_LINE(1200)
				this->market->setSeedCount((int)1);
				HX_STACK_LINE(1201)
				this->market->setPieCount((int)0);
				HX_STACK_LINE(1202)
				this->market->setJuiceCount((int)0);
				HX_STACK_LINE(1203)
				this->market->setCoinCount((int)0);
				HX_STACK_LINE(1204)
				this->market->setseedItemCount((int)2);
				HX_STACK_LINE(1205)
				this->market->setPieItemCount((int)1);
				HX_STACK_LINE(1206)
				this->market->setJuiceItemCount((int)1);
				HX_STACK_LINE(1207)
				this->market->setPiePackCount((int)0);
				HX_STACK_LINE(1208)
				this->market->setJuicePackCount((int)0);
				HX_STACK_LINE(1209)
				{
					HX_STACK_LINE(1209)
					int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(1209)
					int _g2 = this->treeArray->length;		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(1209)
					while((true)){
						HX_STACK_LINE(1209)
						if ((!(((_g1 < _g2))))){
							HX_STACK_LINE(1209)
							break;
						}
						HX_STACK_LINE(1209)
						int i = (_g1)++;		HX_STACK_VAR(i,"i");
						HX_STACK_LINE(1210)
						if (((this->treeArray->__get(i).StaticCast< ::Tree >() != null()))){
							HX_STACK_LINE(1211)
							this->treeArray->__get(i).StaticCast< ::Tree >()->destroy();
							HX_STACK_LINE(1212)
							this->treeArray[i] = null();
						}
					}
				}
				HX_STACK_LINE(1215)
				{
					HX_STACK_LINE(1215)
					int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(1215)
					int _g2 = this->board->length;		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(1215)
					while((true)){
						HX_STACK_LINE(1215)
						if ((!(((_g1 < _g2))))){
							HX_STACK_LINE(1215)
							break;
						}
						HX_STACK_LINE(1215)
						int i = (_g1)++;		HX_STACK_VAR(i,"i");
						HX_STACK_LINE(1216)
						{
							HX_STACK_LINE(1216)
							int _g3 = (int)0;		HX_STACK_VAR(_g3,"_g3");
							HX_STACK_LINE(1216)
							int _g21 = this->board->length;		HX_STACK_VAR(_g21,"_g21");
							HX_STACK_LINE(1216)
							while((true)){
								HX_STACK_LINE(1216)
								if ((!(((_g3 < _g21))))){
									HX_STACK_LINE(1216)
									break;
								}
								HX_STACK_LINE(1216)
								int j = (_g3)++;		HX_STACK_VAR(j,"j");
								HX_STACK_LINE(1217)
								this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >()->setAsUnowned();
								HX_STACK_LINE(1218)
								this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >()->setHasObject(false);
							}
						}
					}
				}
				HX_STACK_LINE(1221)
				{
					HX_STACK_LINE(1221)
					int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(1221)
					int _g2 = this->juiceMachineArray->length;		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(1221)
					while((true)){
						HX_STACK_LINE(1221)
						if ((!(((_g1 < _g2))))){
							HX_STACK_LINE(1221)
							break;
						}
						HX_STACK_LINE(1221)
						int i = (_g1)++;		HX_STACK_VAR(i,"i");
						HX_STACK_LINE(1222)
						if (((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >() != null()))){
							HX_STACK_LINE(1223)
							this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->destroy();
							HX_STACK_LINE(1224)
							this->juiceMachineArray[i] = null();
						}
					}
				}
				HX_STACK_LINE(1227)
				{
					HX_STACK_LINE(1227)
					int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(1227)
					int _g2 = this->pieMachineArray->length;		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(1227)
					while((true)){
						HX_STACK_LINE(1227)
						if ((!(((_g1 < _g2))))){
							HX_STACK_LINE(1227)
							break;
						}
						HX_STACK_LINE(1227)
						int i = (_g1)++;		HX_STACK_VAR(i,"i");
						HX_STACK_LINE(1228)
						if (((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >() != null()))){
							HX_STACK_LINE(1229)
							this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->destroy();
							HX_STACK_LINE(1230)
							this->pieMachineArray[i] = null();
						}
					}
				}
				HX_STACK_LINE(1233)
				{
					HX_STACK_LINE(1233)
					int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(1233)
					int _g2 = this->board->length;		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(1233)
					while((true)){
						HX_STACK_LINE(1233)
						if ((!(((_g1 < _g2))))){
							HX_STACK_LINE(1233)
							break;
						}
						HX_STACK_LINE(1233)
						int i = (_g1)++;		HX_STACK_VAR(i,"i");
						HX_STACK_LINE(1234)
						{
							HX_STACK_LINE(1234)
							int _g3 = (int)0;		HX_STACK_VAR(_g3,"_g3");
							HX_STACK_LINE(1234)
							int _g21 = this->board->length;		HX_STACK_VAR(_g21,"_g21");
							HX_STACK_LINE(1234)
							while((true)){
								HX_STACK_LINE(1234)
								if ((!(((_g3 < _g21))))){
									HX_STACK_LINE(1234)
									break;
								}
								HX_STACK_LINE(1234)
								int j = (_g3)++;		HX_STACK_VAR(j,"j");
								HX_STACK_LINE(1235)
								this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >()->setTileClicked(false);
							}
						}
					}
				}
				HX_STACK_LINE(1238)
				this->board->__get((int)1).StaticCast< Array< ::Dynamic > >()->__get((int)0).StaticCast< ::Tile >()->setAsOwned();
				HX_STACK_LINE(1239)
				this->board->__get((int)2).StaticCast< Array< ::Dynamic > >()->__get((int)0).StaticCast< ::Tile >()->setAsOwned();
				HX_STACK_LINE(1240)
				this->board->__get((int)2).StaticCast< Array< ::Dynamic > >()->__get((int)0).StaticCast< ::Tile >()->setHasObject(true);
				HX_STACK_LINE(1241)
				this->buySignCost = (int)2;
				HX_STACK_LINE(1242)
				this->menuScreen->setGameRestart(false);
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,restartGameOver,(void))

Void PlayState_obj::destroySound( ){
{
		HX_STACK_FRAME("PlayState","destroySound",0x8e01f926,"PlayState.destroySound","PlayState.hx",1247,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1248)
		if (((bool((this->coinsSound != null())) && bool(!(((this->coinsSound->_channel != null()))))))){
			HX_STACK_LINE(1249)
			::flixel::system::FlxSound _g = ::flixel::util::FlxDestroyUtil_obj::destroy(this->coinsSound);		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(1249)
			this->coinsSound = _g;
			HX_STACK_LINE(1250)
			this->coinsSound = null();
			HX_STACK_LINE(1251)
			::haxe::Log_obj::trace(HX_CSTRING("destroying counsound"),hx::SourceInfo(HX_CSTRING("PlayState.hx"),1251,HX_CSTRING("PlayState"),HX_CSTRING("destroySound")));
		}
		HX_STACK_LINE(1253)
		if (((bool((this->alertButtonSound != null())) && bool(!(((this->alertButtonSound->_channel != null()))))))){
			HX_STACK_LINE(1254)
			::flixel::system::FlxSound _g1 = ::flixel::util::FlxDestroyUtil_obj::destroy(this->alertButtonSound);		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(1254)
			this->alertButtonSound = _g1;
			HX_STACK_LINE(1255)
			this->alertButtonSound = null();
			HX_STACK_LINE(1256)
			::haxe::Log_obj::trace(HX_CSTRING("destroying alertButtonSound"),hx::SourceInfo(HX_CSTRING("PlayState.hx"),1256,HX_CSTRING("PlayState"),HX_CSTRING("destroySound")));
		}
		HX_STACK_LINE(1258)
		if (((bool((this->correctAnswerSound != null())) && bool(!(((this->correctAnswerSound->_channel != null()))))))){
			HX_STACK_LINE(1259)
			::flixel::system::FlxSound _g2 = ::flixel::util::FlxDestroyUtil_obj::destroy(this->correctAnswerSound);		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(1259)
			this->correctAnswerSound = _g2;
			HX_STACK_LINE(1260)
			this->correctAnswerSound = null();
			HX_STACK_LINE(1261)
			::haxe::Log_obj::trace(HX_CSTRING("destroying correctAnswerSound"),hx::SourceInfo(HX_CSTRING("PlayState.hx"),1261,HX_CSTRING("PlayState"),HX_CSTRING("destroySound")));
		}
		HX_STACK_LINE(1263)
		if (((bool((this->incorrectAnswerSound != null())) && bool(!(((this->incorrectAnswerSound->_channel != null()))))))){
			HX_STACK_LINE(1264)
			::flixel::system::FlxSound _g3 = ::flixel::util::FlxDestroyUtil_obj::destroy(this->incorrectAnswerSound);		HX_STACK_VAR(_g3,"_g3");
			HX_STACK_LINE(1264)
			this->incorrectAnswerSound = _g3;
			HX_STACK_LINE(1265)
			this->incorrectAnswerSound = null();
			HX_STACK_LINE(1266)
			::haxe::Log_obj::trace(HX_CSTRING("destroying incorrectAnswerSound"),hx::SourceInfo(HX_CSTRING("PlayState.hx"),1266,HX_CSTRING("PlayState"),HX_CSTRING("destroySound")));
		}
		HX_STACK_LINE(1268)
		if (((bool((this->plantingSeedSound != null())) && bool(!(((this->plantingSeedSound->_channel != null()))))))){
			HX_STACK_LINE(1269)
			::flixel::system::FlxSound _g4 = ::flixel::util::FlxDestroyUtil_obj::destroy(this->plantingSeedSound);		HX_STACK_VAR(_g4,"_g4");
			HX_STACK_LINE(1269)
			this->plantingSeedSound = _g4;
			HX_STACK_LINE(1270)
			this->plantingSeedSound = null();
			HX_STACK_LINE(1271)
			::haxe::Log_obj::trace(HX_CSTRING("destroying plantingSeedSound"),hx::SourceInfo(HX_CSTRING("PlayState.hx"),1271,HX_CSTRING("PlayState"),HX_CSTRING("destroySound")));
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,destroySound,(void))

Void PlayState_obj::update( ){
{
		HX_STACK_FRAME("PlayState","update",0x8d182efa,"PlayState.update","PlayState.hx",1279,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1280)
		this->removeCallBackOnPad();
		HX_STACK_LINE(1281)
		this->factoryElectricBill();
		HX_STACK_LINE(1282)
		this->waterSprinklerBill();
		HX_STACK_LINE(1283)
		this->applesForJuice();
		HX_STACK_LINE(1284)
		this->applesAndJuiceForPie();
		HX_STACK_LINE(1285)
		this->turnWaterOff();
		HX_STACK_LINE(1286)
		this->destroyObjects();
		HX_STACK_LINE(1287)
		this->submitAnswer();
		HX_STACK_LINE(1288)
		this->checkForAlertClick();
		HX_STACK_LINE(1289)
		this->stopSeedDragIfBoard();
		HX_STACK_LINE(1290)
		this->stopJuiceDragIfBoard();
		HX_STACK_LINE(1291)
		this->stopPieDragIfBoard();
		HX_STACK_LINE(1292)
		this->tileBoarderFlip();
		HX_STACK_LINE(1293)
		this->displayAppleCount();
		HX_STACK_LINE(1294)
		this->displayPadNumbers();
		HX_STACK_LINE(1295)
		this->truckLoadingGoods();
		HX_STACK_LINE(1296)
		this->createFloatingIsland();
		HX_STACK_LINE(1297)
		this->createAsteroidBelt();
		HX_STACK_LINE(1298)
		this->currentTreeGrowthValue();
		HX_STACK_LINE(1299)
		this->marketTutOpened();
		HX_STACK_LINE(1300)
		this->restartGameOver();
		HX_STACK_LINE(1301)
		this->destroySound();
		HX_STACK_LINE(1302)
		this->super::update();
	}
return null();
}


Void PlayState_obj::destroy( ){
{
		HX_STACK_FRAME("PlayState","destroy",0x6ec756e9,"PlayState.destroy","PlayState.hx",1311,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1311)
		this->super::destroy();
	}
return null();
}



PlayState_obj::PlayState_obj()
{
}

void PlayState_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(PlayState);
	HX_MARK_MEMBER_NAME(menuButton,"menuButton");
	HX_MARK_MEMBER_NAME(menuScreen,"menuScreen");
	HX_MARK_MEMBER_NAME(tutorial,"tutorial");
	HX_MARK_MEMBER_NAME(weed,"weed");
	HX_MARK_MEMBER_NAME(systemStar,"systemStar");
	HX_MARK_MEMBER_NAME(backgroundSprite,"backgroundSprite");
	HX_MARK_MEMBER_NAME(aboveGrassLayer,"aboveGrassLayer");
	HX_MARK_MEMBER_NAME(plantLayer,"plantLayer");
	HX_MARK_MEMBER_NAME(machineLayer,"machineLayer");
	HX_MARK_MEMBER_NAME(belowBottomSprites,"belowBottomSprites");
	HX_MARK_MEMBER_NAME(bottomeSprites,"bottomeSprites");
	HX_MARK_MEMBER_NAME(floatingIsland,"floatingIsland");
	HX_MARK_MEMBER_NAME(tileBoard,"tileBoard");
	HX_MARK_MEMBER_NAME(lastTileClicked,"lastTileClicked");
	HX_MARK_MEMBER_NAME(tileBoardBorder,"tileBoardBorder");
	HX_MARK_MEMBER_NAME(board,"board");
	HX_MARK_MEMBER_NAME(boarder,"boarder");
	HX_MARK_MEMBER_NAME(treeArray,"treeArray");
	HX_MARK_MEMBER_NAME(juiceMachineArray,"juiceMachineArray");
	HX_MARK_MEMBER_NAME(pieMachineArray,"pieMachineArray");
	HX_MARK_MEMBER_NAME(flyingJuiceBottleArray,"flyingJuiceBottleArray");
	HX_MARK_MEMBER_NAME(numPad,"numPad");
	HX_MARK_MEMBER_NAME(bb,"bb");
	HX_MARK_MEMBER_NAME(menuVisible,"menuVisible");
	HX_MARK_MEMBER_NAME(appleCount,"appleCount");
	HX_MARK_MEMBER_NAME(buySignCost,"buySignCost");
	HX_MARK_MEMBER_NAME(applePerTenCost,"applePerTenCost");
	HX_MARK_MEMBER_NAME(juicePerTenCost,"juicePerTenCost");
	HX_MARK_MEMBER_NAME(piePerTenCost,"piePerTenCost");
	HX_MARK_MEMBER_NAME(currentTruckLvl,"currentTruckLvl");
	HX_MARK_MEMBER_NAME(randomIslandCreationTimer,"randomIslandCreationTimer");
	HX_MARK_MEMBER_NAME(loadAppleTimer,"loadAppleTimer");
	HX_MARK_MEMBER_NAME(leaveTruckTimer,"leaveTruckTimer");
	HX_MARK_MEMBER_NAME(appleForJuiceTimer,"appleForJuiceTimer");
	HX_MARK_MEMBER_NAME(appleAndJuiceForPieTimer,"appleAndJuiceForPieTimer");
	HX_MARK_MEMBER_NAME(islandCreationTimer,"islandCreationTimer");
	HX_MARK_MEMBER_NAME(appleCountString,"appleCountString");
	HX_MARK_MEMBER_NAME(answerString,"answerString");
	HX_MARK_MEMBER_NAME(boardIsDown,"boardIsDown");
	HX_MARK_MEMBER_NAME(appleDisplayLock,"appleDisplayLock");
	HX_MARK_MEMBER_NAME(asteroidBelt,"asteroidBelt");
	HX_MARK_MEMBER_NAME(bridge,"bridge");
	HX_MARK_MEMBER_NAME(buySign,"buySign");
	HX_MARK_MEMBER_NAME(foreGroundRocks,"foreGroundRocks");
	HX_MARK_MEMBER_NAME(answerFieldText,"answerFieldText");
	HX_MARK_MEMBER_NAME(appleCountFieldText,"appleCountFieldText");
	HX_MARK_MEMBER_NAME(buySignCostFieldText,"buySignCostFieldText");
	HX_MARK_MEMBER_NAME(market,"market");
	HX_MARK_MEMBER_NAME(truckButton,"truckButton");
	HX_MARK_MEMBER_NAME(juiceMachine,"juiceMachine");
	HX_MARK_MEMBER_NAME(pieMachine,"pieMachine");
	HX_MARK_MEMBER_NAME(appleShed,"appleShed");
	HX_MARK_MEMBER_NAME(appleTruck,"appleTruck");
	HX_MARK_MEMBER_NAME(tree,"tree");
	HX_MARK_MEMBER_NAME(basket,"basket");
	HX_MARK_MEMBER_NAME(currentTargetTree,"currentTargetTree");
	HX_MARK_MEMBER_NAME(currentTargetJuiceMachine,"currentTargetJuiceMachine");
	HX_MARK_MEMBER_NAME(currentTargetPieMachine,"currentTargetPieMachine");
	HX_MARK_MEMBER_NAME(juiceMachineIcon,"juiceMachineIcon");
	HX_MARK_MEMBER_NAME(pieMachineIcon,"pieMachineIcon");
	HX_MARK_MEMBER_NAME(juiceMachineIconDrag,"juiceMachineIconDrag");
	HX_MARK_MEMBER_NAME(pieMachineIconDrag,"pieMachineIconDrag");
	HX_MARK_MEMBER_NAME(juicePackIcon,"juicePackIcon");
	HX_MARK_MEMBER_NAME(piePackIcon,"piePackIcon");
	HX_MARK_MEMBER_NAME(seed,"seed");
	HX_MARK_MEMBER_NAME(seedDrag,"seedDrag");
	HX_MARK_MEMBER_NAME(cloud,"cloud");
	HX_MARK_MEMBER_NAME(sun,"sun");
	HX_MARK_MEMBER_NAME(currentTileBorder,"currentTileBorder");
	HX_MARK_MEMBER_NAME(overlaysInUse,"overlaysInUse");
	HX_MARK_MEMBER_NAME(sunClicked,"sunClicked");
	HX_MARK_MEMBER_NAME(dropletClicked,"dropletClicked");
	HX_MARK_MEMBER_NAME(ownedTile,"ownedTile");
	HX_MARK_MEMBER_NAME(closeTreeGrowthCheck,"closeTreeGrowthCheck");
	HX_MARK_MEMBER_NAME(closeAppleCountCheck,"closeAppleCountCheck");
	HX_MARK_MEMBER_NAME(marketOpenCheck,"marketOpenCheck");
	HX_MARK_MEMBER_NAME(tutorialOn,"tutorialOn");
	HX_MARK_MEMBER_NAME(truckTutSection,"truckTutSection");
	HX_MARK_MEMBER_NAME(callBackStopOne,"callBackStopOne");
	HX_MARK_MEMBER_NAME(callBackStopTwo,"callBackStopTwo");
	HX_MARK_MEMBER_NAME(tutorialWrongAnswersSkip,"tutorialWrongAnswersSkip");
	HX_MARK_MEMBER_NAME(closeSecondTreeGrowthCheck,"closeSecondTreeGrowthCheck");
	HX_MARK_MEMBER_NAME(tutUnpauseAlertsDone,"tutUnpauseAlertsDone");
	HX_MARK_MEMBER_NAME(coinsSound,"coinsSound");
	HX_MARK_MEMBER_NAME(alertButtonSound,"alertButtonSound");
	HX_MARK_MEMBER_NAME(correctAnswerSound,"correctAnswerSound");
	HX_MARK_MEMBER_NAME(incorrectAnswerSound,"incorrectAnswerSound");
	HX_MARK_MEMBER_NAME(plantingSeedSound,"plantingSeedSound");
	::flixel::FlxState_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void PlayState_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(menuButton,"menuButton");
	HX_VISIT_MEMBER_NAME(menuScreen,"menuScreen");
	HX_VISIT_MEMBER_NAME(tutorial,"tutorial");
	HX_VISIT_MEMBER_NAME(weed,"weed");
	HX_VISIT_MEMBER_NAME(systemStar,"systemStar");
	HX_VISIT_MEMBER_NAME(backgroundSprite,"backgroundSprite");
	HX_VISIT_MEMBER_NAME(aboveGrassLayer,"aboveGrassLayer");
	HX_VISIT_MEMBER_NAME(plantLayer,"plantLayer");
	HX_VISIT_MEMBER_NAME(machineLayer,"machineLayer");
	HX_VISIT_MEMBER_NAME(belowBottomSprites,"belowBottomSprites");
	HX_VISIT_MEMBER_NAME(bottomeSprites,"bottomeSprites");
	HX_VISIT_MEMBER_NAME(floatingIsland,"floatingIsland");
	HX_VISIT_MEMBER_NAME(tileBoard,"tileBoard");
	HX_VISIT_MEMBER_NAME(lastTileClicked,"lastTileClicked");
	HX_VISIT_MEMBER_NAME(tileBoardBorder,"tileBoardBorder");
	HX_VISIT_MEMBER_NAME(board,"board");
	HX_VISIT_MEMBER_NAME(boarder,"boarder");
	HX_VISIT_MEMBER_NAME(treeArray,"treeArray");
	HX_VISIT_MEMBER_NAME(juiceMachineArray,"juiceMachineArray");
	HX_VISIT_MEMBER_NAME(pieMachineArray,"pieMachineArray");
	HX_VISIT_MEMBER_NAME(flyingJuiceBottleArray,"flyingJuiceBottleArray");
	HX_VISIT_MEMBER_NAME(numPad,"numPad");
	HX_VISIT_MEMBER_NAME(bb,"bb");
	HX_VISIT_MEMBER_NAME(menuVisible,"menuVisible");
	HX_VISIT_MEMBER_NAME(appleCount,"appleCount");
	HX_VISIT_MEMBER_NAME(buySignCost,"buySignCost");
	HX_VISIT_MEMBER_NAME(applePerTenCost,"applePerTenCost");
	HX_VISIT_MEMBER_NAME(juicePerTenCost,"juicePerTenCost");
	HX_VISIT_MEMBER_NAME(piePerTenCost,"piePerTenCost");
	HX_VISIT_MEMBER_NAME(currentTruckLvl,"currentTruckLvl");
	HX_VISIT_MEMBER_NAME(randomIslandCreationTimer,"randomIslandCreationTimer");
	HX_VISIT_MEMBER_NAME(loadAppleTimer,"loadAppleTimer");
	HX_VISIT_MEMBER_NAME(leaveTruckTimer,"leaveTruckTimer");
	HX_VISIT_MEMBER_NAME(appleForJuiceTimer,"appleForJuiceTimer");
	HX_VISIT_MEMBER_NAME(appleAndJuiceForPieTimer,"appleAndJuiceForPieTimer");
	HX_VISIT_MEMBER_NAME(islandCreationTimer,"islandCreationTimer");
	HX_VISIT_MEMBER_NAME(appleCountString,"appleCountString");
	HX_VISIT_MEMBER_NAME(answerString,"answerString");
	HX_VISIT_MEMBER_NAME(boardIsDown,"boardIsDown");
	HX_VISIT_MEMBER_NAME(appleDisplayLock,"appleDisplayLock");
	HX_VISIT_MEMBER_NAME(asteroidBelt,"asteroidBelt");
	HX_VISIT_MEMBER_NAME(bridge,"bridge");
	HX_VISIT_MEMBER_NAME(buySign,"buySign");
	HX_VISIT_MEMBER_NAME(foreGroundRocks,"foreGroundRocks");
	HX_VISIT_MEMBER_NAME(answerFieldText,"answerFieldText");
	HX_VISIT_MEMBER_NAME(appleCountFieldText,"appleCountFieldText");
	HX_VISIT_MEMBER_NAME(buySignCostFieldText,"buySignCostFieldText");
	HX_VISIT_MEMBER_NAME(market,"market");
	HX_VISIT_MEMBER_NAME(truckButton,"truckButton");
	HX_VISIT_MEMBER_NAME(juiceMachine,"juiceMachine");
	HX_VISIT_MEMBER_NAME(pieMachine,"pieMachine");
	HX_VISIT_MEMBER_NAME(appleShed,"appleShed");
	HX_VISIT_MEMBER_NAME(appleTruck,"appleTruck");
	HX_VISIT_MEMBER_NAME(tree,"tree");
	HX_VISIT_MEMBER_NAME(basket,"basket");
	HX_VISIT_MEMBER_NAME(currentTargetTree,"currentTargetTree");
	HX_VISIT_MEMBER_NAME(currentTargetJuiceMachine,"currentTargetJuiceMachine");
	HX_VISIT_MEMBER_NAME(currentTargetPieMachine,"currentTargetPieMachine");
	HX_VISIT_MEMBER_NAME(juiceMachineIcon,"juiceMachineIcon");
	HX_VISIT_MEMBER_NAME(pieMachineIcon,"pieMachineIcon");
	HX_VISIT_MEMBER_NAME(juiceMachineIconDrag,"juiceMachineIconDrag");
	HX_VISIT_MEMBER_NAME(pieMachineIconDrag,"pieMachineIconDrag");
	HX_VISIT_MEMBER_NAME(juicePackIcon,"juicePackIcon");
	HX_VISIT_MEMBER_NAME(piePackIcon,"piePackIcon");
	HX_VISIT_MEMBER_NAME(seed,"seed");
	HX_VISIT_MEMBER_NAME(seedDrag,"seedDrag");
	HX_VISIT_MEMBER_NAME(cloud,"cloud");
	HX_VISIT_MEMBER_NAME(sun,"sun");
	HX_VISIT_MEMBER_NAME(currentTileBorder,"currentTileBorder");
	HX_VISIT_MEMBER_NAME(overlaysInUse,"overlaysInUse");
	HX_VISIT_MEMBER_NAME(sunClicked,"sunClicked");
	HX_VISIT_MEMBER_NAME(dropletClicked,"dropletClicked");
	HX_VISIT_MEMBER_NAME(ownedTile,"ownedTile");
	HX_VISIT_MEMBER_NAME(closeTreeGrowthCheck,"closeTreeGrowthCheck");
	HX_VISIT_MEMBER_NAME(closeAppleCountCheck,"closeAppleCountCheck");
	HX_VISIT_MEMBER_NAME(marketOpenCheck,"marketOpenCheck");
	HX_VISIT_MEMBER_NAME(tutorialOn,"tutorialOn");
	HX_VISIT_MEMBER_NAME(truckTutSection,"truckTutSection");
	HX_VISIT_MEMBER_NAME(callBackStopOne,"callBackStopOne");
	HX_VISIT_MEMBER_NAME(callBackStopTwo,"callBackStopTwo");
	HX_VISIT_MEMBER_NAME(tutorialWrongAnswersSkip,"tutorialWrongAnswersSkip");
	HX_VISIT_MEMBER_NAME(closeSecondTreeGrowthCheck,"closeSecondTreeGrowthCheck");
	HX_VISIT_MEMBER_NAME(tutUnpauseAlertsDone,"tutUnpauseAlertsDone");
	HX_VISIT_MEMBER_NAME(coinsSound,"coinsSound");
	HX_VISIT_MEMBER_NAME(alertButtonSound,"alertButtonSound");
	HX_VISIT_MEMBER_NAME(correctAnswerSound,"correctAnswerSound");
	HX_VISIT_MEMBER_NAME(incorrectAnswerSound,"incorrectAnswerSound");
	HX_VISIT_MEMBER_NAME(plantingSeedSound,"plantingSeedSound");
	::flixel::FlxState_obj::__Visit(HX_VISIT_ARG);
}

Dynamic PlayState_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 2:
		if (HX_FIELD_EQ(inName,"bb") ) { return bb; }
		break;
	case 3:
		if (HX_FIELD_EQ(inName,"sun") ) { return sun; }
		break;
	case 4:
		if (HX_FIELD_EQ(inName,"weed") ) { return weed; }
		if (HX_FIELD_EQ(inName,"tree") ) { return tree; }
		if (HX_FIELD_EQ(inName,"seed") ) { return seed; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"board") ) { return board; }
		if (HX_FIELD_EQ(inName,"cloud") ) { return cloud; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"numPad") ) { return numPad; }
		if (HX_FIELD_EQ(inName,"bridge") ) { return bridge; }
		if (HX_FIELD_EQ(inName,"market") ) { return market; }
		if (HX_FIELD_EQ(inName,"basket") ) { return basket; }
		if (HX_FIELD_EQ(inName,"create") ) { return create_dyn(); }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"boarder") ) { return boarder; }
		if (HX_FIELD_EQ(inName,"buySign") ) { return buySign; }
		if (HX_FIELD_EQ(inName,"buyLand") ) { return buyLand_dyn(); }
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"tutorial") ) { return tutorial; }
		if (HX_FIELD_EQ(inName,"seedDrag") ) { return seedDrag; }
		if (HX_FIELD_EQ(inName,"openMenu") ) { return openMenu_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"tileBoard") ) { return tileBoard; }
		if (HX_FIELD_EQ(inName,"treeArray") ) { return treeArray; }
		if (HX_FIELD_EQ(inName,"appleShed") ) { return appleShed; }
		if (HX_FIELD_EQ(inName,"ownedTile") ) { return ownedTile; }
		if (HX_FIELD_EQ(inName,"callTruck") ) { return callTruck_dyn(); }
		if (HX_FIELD_EQ(inName,"drawBoard") ) { return drawBoard_dyn(); }
		if (HX_FIELD_EQ(inName,"letItRain") ) { return letItRain_dyn(); }
		if (HX_FIELD_EQ(inName,"waterBill") ) { return waterBill_dyn(); }
		if (HX_FIELD_EQ(inName,"appleBill") ) { return appleBill_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"menuButton") ) { return menuButton; }
		if (HX_FIELD_EQ(inName,"menuScreen") ) { return menuScreen; }
		if (HX_FIELD_EQ(inName,"systemStar") ) { return systemStar; }
		if (HX_FIELD_EQ(inName,"plantLayer") ) { return plantLayer; }
		if (HX_FIELD_EQ(inName,"appleCount") ) { return appleCount; }
		if (HX_FIELD_EQ(inName,"pieMachine") ) { return pieMachine; }
		if (HX_FIELD_EQ(inName,"appleTruck") ) { return appleTruck; }
		if (HX_FIELD_EQ(inName,"sunClicked") ) { return sunClicked; }
		if (HX_FIELD_EQ(inName,"tutorialOn") ) { return tutorialOn; }
		if (HX_FIELD_EQ(inName,"coinsSound") ) { return coinsSound; }
		if (HX_FIELD_EQ(inName,"plantWeeds") ) { return plantWeeds_dyn(); }
		if (HX_FIELD_EQ(inName,"letItShine") ) { return letItShine_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"menuVisible") ) { return menuVisible; }
		if (HX_FIELD_EQ(inName,"buySignCost") ) { return buySignCost; }
		if (HX_FIELD_EQ(inName,"boardIsDown") ) { return boardIsDown; }
		if (HX_FIELD_EQ(inName,"truckButton") ) { return truckButton; }
		if (HX_FIELD_EQ(inName,"piePackIcon") ) { return piePackIcon; }
		if (HX_FIELD_EQ(inName,"placeObject") ) { return placeObject_dyn(); }
		if (HX_FIELD_EQ(inName,"treeClicked") ) { return treeClicked_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"machineLayer") ) { return machineLayer; }
		if (HX_FIELD_EQ(inName,"answerString") ) { return answerString; }
		if (HX_FIELD_EQ(inName,"asteroidBelt") ) { return asteroidBelt; }
		if (HX_FIELD_EQ(inName,"juiceMachine") ) { return juiceMachine; }
		if (HX_FIELD_EQ(inName,"addTreeArray") ) { return addTreeArray_dyn(); }
		if (HX_FIELD_EQ(inName,"submitAnswer") ) { return submitAnswer_dyn(); }
		if (HX_FIELD_EQ(inName,"electricBill") ) { return electricBill_dyn(); }
		if (HX_FIELD_EQ(inName,"turnWaterOff") ) { return turnWaterOff_dyn(); }
		if (HX_FIELD_EQ(inName,"destroySound") ) { return destroySound_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"piePerTenCost") ) { return piePerTenCost; }
		if (HX_FIELD_EQ(inName,"juicePackIcon") ) { return juicePackIcon; }
		if (HX_FIELD_EQ(inName,"overlaysInUse") ) { return overlaysInUse; }
		if (HX_FIELD_EQ(inName,"initPlaystate") ) { return initPlaystate_dyn(); }
		if (HX_FIELD_EQ(inName,"initTreeArray") ) { return initTreeArray_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"bottomeSprites") ) { return bottomeSprites; }
		if (HX_FIELD_EQ(inName,"floatingIsland") ) { return floatingIsland; }
		if (HX_FIELD_EQ(inName,"loadAppleTimer") ) { return loadAppleTimer; }
		if (HX_FIELD_EQ(inName,"pieMachineIcon") ) { return pieMachineIcon; }
		if (HX_FIELD_EQ(inName,"dropletClicked") ) { return dropletClicked; }
		if (HX_FIELD_EQ(inName,"buyButtonPopUp") ) { return buyButtonPopUp_dyn(); }
		if (HX_FIELD_EQ(inName,"destroyObjects") ) { return destroyObjects_dyn(); }
		if (HX_FIELD_EQ(inName,"applesForJuice") ) { return applesForJuice_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"aboveGrassLayer") ) { return aboveGrassLayer; }
		if (HX_FIELD_EQ(inName,"lastTileClicked") ) { return lastTileClicked; }
		if (HX_FIELD_EQ(inName,"tileBoardBorder") ) { return tileBoardBorder; }
		if (HX_FIELD_EQ(inName,"pieMachineArray") ) { return pieMachineArray; }
		if (HX_FIELD_EQ(inName,"applePerTenCost") ) { return applePerTenCost; }
		if (HX_FIELD_EQ(inName,"juicePerTenCost") ) { return juicePerTenCost; }
		if (HX_FIELD_EQ(inName,"currentTruckLvl") ) { return currentTruckLvl; }
		if (HX_FIELD_EQ(inName,"leaveTruckTimer") ) { return leaveTruckTimer; }
		if (HX_FIELD_EQ(inName,"foreGroundRocks") ) { return foreGroundRocks; }
		if (HX_FIELD_EQ(inName,"answerFieldText") ) { return answerFieldText; }
		if (HX_FIELD_EQ(inName,"marketOpenCheck") ) { return marketOpenCheck; }
		if (HX_FIELD_EQ(inName,"truckTutSection") ) { return truckTutSection; }
		if (HX_FIELD_EQ(inName,"callBackStopOne") ) { return callBackStopOne; }
		if (HX_FIELD_EQ(inName,"callBackStopTwo") ) { return callBackStopTwo; }
		if (HX_FIELD_EQ(inName,"tileBoarderFlip") ) { return tileBoarderFlip_dyn(); }
		if (HX_FIELD_EQ(inName,"marketTutOpened") ) { return marketTutOpened_dyn(); }
		if (HX_FIELD_EQ(inName,"restartGameOver") ) { return restartGameOver_dyn(); }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"backgroundSprite") ) { return backgroundSprite; }
		if (HX_FIELD_EQ(inName,"appleCountString") ) { return appleCountString; }
		if (HX_FIELD_EQ(inName,"appleDisplayLock") ) { return appleDisplayLock; }
		if (HX_FIELD_EQ(inName,"juiceMachineIcon") ) { return juiceMachineIcon; }
		if (HX_FIELD_EQ(inName,"alertButtonSound") ) { return alertButtonSound; }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"juiceMachineArray") ) { return juiceMachineArray; }
		if (HX_FIELD_EQ(inName,"currentTargetTree") ) { return currentTargetTree; }
		if (HX_FIELD_EQ(inName,"currentTileBorder") ) { return currentTileBorder; }
		if (HX_FIELD_EQ(inName,"plantingSeedSound") ) { return plantingSeedSound; }
		if (HX_FIELD_EQ(inName,"displayAppleCount") ) { return displayAppleCount_dyn(); }
		if (HX_FIELD_EQ(inName,"displayPadNumbers") ) { return displayPadNumbers_dyn(); }
		if (HX_FIELD_EQ(inName,"truckLoadingGoods") ) { return truckLoadingGoods_dyn(); }
		if (HX_FIELD_EQ(inName,"appleAndJuiceBill") ) { return appleAndJuiceBill_dyn(); }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"belowBottomSprites") ) { return belowBottomSprites; }
		if (HX_FIELD_EQ(inName,"appleForJuiceTimer") ) { return appleForJuiceTimer; }
		if (HX_FIELD_EQ(inName,"pieMachineIconDrag") ) { return pieMachineIconDrag; }
		if (HX_FIELD_EQ(inName,"correctAnswerSound") ) { return correctAnswerSound; }
		if (HX_FIELD_EQ(inName,"addPieMachineArray") ) { return addPieMachineArray_dyn(); }
		if (HX_FIELD_EQ(inName,"stopPieDragIfBoard") ) { return stopPieDragIfBoard_dyn(); }
		if (HX_FIELD_EQ(inName,"checkForAlertClick") ) { return checkForAlertClick_dyn(); }
		if (HX_FIELD_EQ(inName,"waterSprinklerBill") ) { return waterSprinklerBill_dyn(); }
		if (HX_FIELD_EQ(inName,"createAsteroidBelt") ) { return createAsteroidBelt_dyn(); }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"islandCreationTimer") ) { return islandCreationTimer; }
		if (HX_FIELD_EQ(inName,"appleCountFieldText") ) { return appleCountFieldText; }
		if (HX_FIELD_EQ(inName,"initPieMachineArray") ) { return initPieMachineArray_dyn(); }
		if (HX_FIELD_EQ(inName,"stopSeedDragIfBoard") ) { return stopSeedDragIfBoard_dyn(); }
		if (HX_FIELD_EQ(inName,"factoryElectricBill") ) { return factoryElectricBill_dyn(); }
		if (HX_FIELD_EQ(inName,"removeCallBackOnPad") ) { return removeCallBackOnPad_dyn(); }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"buySignCostFieldText") ) { return buySignCostFieldText; }
		if (HX_FIELD_EQ(inName,"juiceMachineIconDrag") ) { return juiceMachineIconDrag; }
		if (HX_FIELD_EQ(inName,"closeTreeGrowthCheck") ) { return closeTreeGrowthCheck; }
		if (HX_FIELD_EQ(inName,"closeAppleCountCheck") ) { return closeAppleCountCheck; }
		if (HX_FIELD_EQ(inName,"tutUnpauseAlertsDone") ) { return tutUnpauseAlertsDone; }
		if (HX_FIELD_EQ(inName,"incorrectAnswerSound") ) { return incorrectAnswerSound; }
		if (HX_FIELD_EQ(inName,"createFloatingIsland") ) { return createFloatingIsland_dyn(); }
		if (HX_FIELD_EQ(inName,"addFlyingBottleArray") ) { return addFlyingBottleArray_dyn(); }
		if (HX_FIELD_EQ(inName,"addJuiceMachineArray") ) { return addJuiceMachineArray_dyn(); }
		if (HX_FIELD_EQ(inName,"secondTreeTutMessage") ) { return secondTreeTutMessage_dyn(); }
		if (HX_FIELD_EQ(inName,"stopJuiceDragIfBoard") ) { return stopJuiceDragIfBoard_dyn(); }
		if (HX_FIELD_EQ(inName,"applesAndJuiceForPie") ) { return applesAndJuiceForPie_dyn(); }
		break;
	case 21:
		if (HX_FIELD_EQ(inName,"releaseAllClickEvents") ) { return releaseAllClickEvents_dyn(); }
		if (HX_FIELD_EQ(inName,"initJuiceMachineArray") ) { return initJuiceMachineArray_dyn(); }
		break;
	case 22:
		if (HX_FIELD_EQ(inName,"flyingJuiceBottleArray") ) { return flyingJuiceBottleArray; }
		if (HX_FIELD_EQ(inName,"registerAllAlertEvents") ) { return registerAllAlertEvents_dyn(); }
		if (HX_FIELD_EQ(inName,"currentTreeGrowthValue") ) { return currentTreeGrowthValue_dyn(); }
		break;
	case 23:
		if (HX_FIELD_EQ(inName,"currentTargetPieMachine") ) { return currentTargetPieMachine; }
		break;
	case 24:
		if (HX_FIELD_EQ(inName,"appleAndJuiceForPieTimer") ) { return appleAndJuiceForPieTimer; }
		if (HX_FIELD_EQ(inName,"tutorialWrongAnswersSkip") ) { return tutorialWrongAnswersSkip; }
		if (HX_FIELD_EQ(inName,"unregisterAllAlertEvents") ) { return unregisterAllAlertEvents_dyn(); }
		break;
	case 25:
		if (HX_FIELD_EQ(inName,"randomIslandCreationTimer") ) { return randomIslandCreationTimer; }
		if (HX_FIELD_EQ(inName,"currentTargetJuiceMachine") ) { return currentTargetJuiceMachine; }
		break;
	case 26:
		if (HX_FIELD_EQ(inName,"closeSecondTreeGrowthCheck") ) { return closeSecondTreeGrowthCheck; }
		if (HX_FIELD_EQ(inName,"initFlyingJuiceBottleArray") ) { return initFlyingJuiceBottleArray_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic PlayState_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 2:
		if (HX_FIELD_EQ(inName,"bb") ) { bb=inValue.Cast< ::ProblemBoard >(); return inValue; }
		break;
	case 3:
		if (HX_FIELD_EQ(inName,"sun") ) { sun=inValue.Cast< ::SunToken >(); return inValue; }
		break;
	case 4:
		if (HX_FIELD_EQ(inName,"weed") ) { weed=inValue.Cast< ::Weed >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tree") ) { tree=inValue.Cast< ::Tree >(); return inValue; }
		if (HX_FIELD_EQ(inName,"seed") ) { seed=inValue.Cast< ::Seed >(); return inValue; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"board") ) { board=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		if (HX_FIELD_EQ(inName,"cloud") ) { cloud=inValue.Cast< ::Cloud >(); return inValue; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"numPad") ) { numPad=inValue.Cast< ::NumberPad >(); return inValue; }
		if (HX_FIELD_EQ(inName,"bridge") ) { bridge=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"market") ) { market=inValue.Cast< ::Market >(); return inValue; }
		if (HX_FIELD_EQ(inName,"basket") ) { basket=inValue.Cast< ::AppleBasket >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"boarder") ) { boarder=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		if (HX_FIELD_EQ(inName,"buySign") ) { buySign=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"tutorial") ) { tutorial=inValue.Cast< ::Tutorial >(); return inValue; }
		if (HX_FIELD_EQ(inName,"seedDrag") ) { seedDrag=inValue.Cast< ::Seed >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"tileBoard") ) { tileBoard=inValue.Cast< ::TileBoard >(); return inValue; }
		if (HX_FIELD_EQ(inName,"treeArray") ) { treeArray=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		if (HX_FIELD_EQ(inName,"appleShed") ) { appleShed=inValue.Cast< ::AppleShed >(); return inValue; }
		if (HX_FIELD_EQ(inName,"ownedTile") ) { ownedTile=inValue.Cast< bool >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"menuButton") ) { menuButton=inValue.Cast< ::flixel::ui::FlxButton >(); return inValue; }
		if (HX_FIELD_EQ(inName,"menuScreen") ) { menuScreen=inValue.Cast< ::OptionsScreen >(); return inValue; }
		if (HX_FIELD_EQ(inName,"systemStar") ) { systemStar=inValue.Cast< ::SystemStar >(); return inValue; }
		if (HX_FIELD_EQ(inName,"plantLayer") ) { plantLayer=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"appleCount") ) { appleCount=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"pieMachine") ) { pieMachine=inValue.Cast< ::PieMachine >(); return inValue; }
		if (HX_FIELD_EQ(inName,"appleTruck") ) { appleTruck=inValue.Cast< ::AppleTruck >(); return inValue; }
		if (HX_FIELD_EQ(inName,"sunClicked") ) { sunClicked=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tutorialOn") ) { tutorialOn=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"coinsSound") ) { coinsSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"menuVisible") ) { menuVisible=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"buySignCost") ) { buySignCost=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"boardIsDown") ) { boardIsDown=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"truckButton") ) { truckButton=inValue.Cast< ::TruckButton >(); return inValue; }
		if (HX_FIELD_EQ(inName,"piePackIcon") ) { piePackIcon=inValue.Cast< ::PiePackIcon >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"machineLayer") ) { machineLayer=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"answerString") ) { answerString=inValue.Cast< ::String >(); return inValue; }
		if (HX_FIELD_EQ(inName,"asteroidBelt") ) { asteroidBelt=inValue.Cast< ::AsteroidBelt >(); return inValue; }
		if (HX_FIELD_EQ(inName,"juiceMachine") ) { juiceMachine=inValue.Cast< ::JuiceMachine >(); return inValue; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"piePerTenCost") ) { piePerTenCost=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"juicePackIcon") ) { juicePackIcon=inValue.Cast< ::JuicePackIcon >(); return inValue; }
		if (HX_FIELD_EQ(inName,"overlaysInUse") ) { overlaysInUse=inValue.Cast< bool >(); return inValue; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"bottomeSprites") ) { bottomeSprites=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"floatingIsland") ) { floatingIsland=inValue.Cast< ::Island >(); return inValue; }
		if (HX_FIELD_EQ(inName,"loadAppleTimer") ) { loadAppleTimer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"pieMachineIcon") ) { pieMachineIcon=inValue.Cast< ::PieIcon >(); return inValue; }
		if (HX_FIELD_EQ(inName,"dropletClicked") ) { dropletClicked=inValue.Cast< bool >(); return inValue; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"aboveGrassLayer") ) { aboveGrassLayer=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"lastTileClicked") ) { lastTileClicked=inValue.Cast< ::Tile >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tileBoardBorder") ) { tileBoardBorder=inValue.Cast< ::TileBoarder >(); return inValue; }
		if (HX_FIELD_EQ(inName,"pieMachineArray") ) { pieMachineArray=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		if (HX_FIELD_EQ(inName,"applePerTenCost") ) { applePerTenCost=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"juicePerTenCost") ) { juicePerTenCost=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"currentTruckLvl") ) { currentTruckLvl=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"leaveTruckTimer") ) { leaveTruckTimer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"foreGroundRocks") ) { foreGroundRocks=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"answerFieldText") ) { answerFieldText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		if (HX_FIELD_EQ(inName,"marketOpenCheck") ) { marketOpenCheck=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"truckTutSection") ) { truckTutSection=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"callBackStopOne") ) { callBackStopOne=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"callBackStopTwo") ) { callBackStopTwo=inValue.Cast< bool >(); return inValue; }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"backgroundSprite") ) { backgroundSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"appleCountString") ) { appleCountString=inValue.Cast< ::String >(); return inValue; }
		if (HX_FIELD_EQ(inName,"appleDisplayLock") ) { appleDisplayLock=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"juiceMachineIcon") ) { juiceMachineIcon=inValue.Cast< ::JuiceIcon >(); return inValue; }
		if (HX_FIELD_EQ(inName,"alertButtonSound") ) { alertButtonSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"juiceMachineArray") ) { juiceMachineArray=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		if (HX_FIELD_EQ(inName,"currentTargetTree") ) { currentTargetTree=inValue.Cast< ::Tree >(); return inValue; }
		if (HX_FIELD_EQ(inName,"currentTileBorder") ) { currentTileBorder=inValue.Cast< ::TileBoarder >(); return inValue; }
		if (HX_FIELD_EQ(inName,"plantingSeedSound") ) { plantingSeedSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"belowBottomSprites") ) { belowBottomSprites=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"appleForJuiceTimer") ) { appleForJuiceTimer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"pieMachineIconDrag") ) { pieMachineIconDrag=inValue.Cast< ::PieIcon >(); return inValue; }
		if (HX_FIELD_EQ(inName,"correctAnswerSound") ) { correctAnswerSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"islandCreationTimer") ) { islandCreationTimer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"appleCountFieldText") ) { appleCountFieldText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"buySignCostFieldText") ) { buySignCostFieldText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		if (HX_FIELD_EQ(inName,"juiceMachineIconDrag") ) { juiceMachineIconDrag=inValue.Cast< ::JuiceIcon >(); return inValue; }
		if (HX_FIELD_EQ(inName,"closeTreeGrowthCheck") ) { closeTreeGrowthCheck=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"closeAppleCountCheck") ) { closeAppleCountCheck=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tutUnpauseAlertsDone") ) { tutUnpauseAlertsDone=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"incorrectAnswerSound") ) { incorrectAnswerSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		break;
	case 22:
		if (HX_FIELD_EQ(inName,"flyingJuiceBottleArray") ) { flyingJuiceBottleArray=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		break;
	case 23:
		if (HX_FIELD_EQ(inName,"currentTargetPieMachine") ) { currentTargetPieMachine=inValue.Cast< ::PieMachine >(); return inValue; }
		break;
	case 24:
		if (HX_FIELD_EQ(inName,"appleAndJuiceForPieTimer") ) { appleAndJuiceForPieTimer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tutorialWrongAnswersSkip") ) { tutorialWrongAnswersSkip=inValue.Cast< bool >(); return inValue; }
		break;
	case 25:
		if (HX_FIELD_EQ(inName,"randomIslandCreationTimer") ) { randomIslandCreationTimer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"currentTargetJuiceMachine") ) { currentTargetJuiceMachine=inValue.Cast< ::JuiceMachine >(); return inValue; }
		break;
	case 26:
		if (HX_FIELD_EQ(inName,"closeSecondTreeGrowthCheck") ) { closeSecondTreeGrowthCheck=inValue.Cast< bool >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void PlayState_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("menuButton"));
	outFields->push(HX_CSTRING("menuScreen"));
	outFields->push(HX_CSTRING("tutorial"));
	outFields->push(HX_CSTRING("weed"));
	outFields->push(HX_CSTRING("systemStar"));
	outFields->push(HX_CSTRING("backgroundSprite"));
	outFields->push(HX_CSTRING("aboveGrassLayer"));
	outFields->push(HX_CSTRING("plantLayer"));
	outFields->push(HX_CSTRING("machineLayer"));
	outFields->push(HX_CSTRING("belowBottomSprites"));
	outFields->push(HX_CSTRING("bottomeSprites"));
	outFields->push(HX_CSTRING("floatingIsland"));
	outFields->push(HX_CSTRING("tileBoard"));
	outFields->push(HX_CSTRING("lastTileClicked"));
	outFields->push(HX_CSTRING("tileBoardBorder"));
	outFields->push(HX_CSTRING("board"));
	outFields->push(HX_CSTRING("boarder"));
	outFields->push(HX_CSTRING("treeArray"));
	outFields->push(HX_CSTRING("juiceMachineArray"));
	outFields->push(HX_CSTRING("pieMachineArray"));
	outFields->push(HX_CSTRING("flyingJuiceBottleArray"));
	outFields->push(HX_CSTRING("numPad"));
	outFields->push(HX_CSTRING("bb"));
	outFields->push(HX_CSTRING("menuVisible"));
	outFields->push(HX_CSTRING("appleCount"));
	outFields->push(HX_CSTRING("buySignCost"));
	outFields->push(HX_CSTRING("applePerTenCost"));
	outFields->push(HX_CSTRING("juicePerTenCost"));
	outFields->push(HX_CSTRING("piePerTenCost"));
	outFields->push(HX_CSTRING("currentTruckLvl"));
	outFields->push(HX_CSTRING("randomIslandCreationTimer"));
	outFields->push(HX_CSTRING("loadAppleTimer"));
	outFields->push(HX_CSTRING("leaveTruckTimer"));
	outFields->push(HX_CSTRING("appleForJuiceTimer"));
	outFields->push(HX_CSTRING("appleAndJuiceForPieTimer"));
	outFields->push(HX_CSTRING("islandCreationTimer"));
	outFields->push(HX_CSTRING("appleCountString"));
	outFields->push(HX_CSTRING("answerString"));
	outFields->push(HX_CSTRING("boardIsDown"));
	outFields->push(HX_CSTRING("appleDisplayLock"));
	outFields->push(HX_CSTRING("asteroidBelt"));
	outFields->push(HX_CSTRING("bridge"));
	outFields->push(HX_CSTRING("buySign"));
	outFields->push(HX_CSTRING("foreGroundRocks"));
	outFields->push(HX_CSTRING("answerFieldText"));
	outFields->push(HX_CSTRING("appleCountFieldText"));
	outFields->push(HX_CSTRING("buySignCostFieldText"));
	outFields->push(HX_CSTRING("market"));
	outFields->push(HX_CSTRING("truckButton"));
	outFields->push(HX_CSTRING("juiceMachine"));
	outFields->push(HX_CSTRING("pieMachine"));
	outFields->push(HX_CSTRING("appleShed"));
	outFields->push(HX_CSTRING("appleTruck"));
	outFields->push(HX_CSTRING("tree"));
	outFields->push(HX_CSTRING("basket"));
	outFields->push(HX_CSTRING("currentTargetTree"));
	outFields->push(HX_CSTRING("currentTargetJuiceMachine"));
	outFields->push(HX_CSTRING("currentTargetPieMachine"));
	outFields->push(HX_CSTRING("juiceMachineIcon"));
	outFields->push(HX_CSTRING("pieMachineIcon"));
	outFields->push(HX_CSTRING("juiceMachineIconDrag"));
	outFields->push(HX_CSTRING("pieMachineIconDrag"));
	outFields->push(HX_CSTRING("juicePackIcon"));
	outFields->push(HX_CSTRING("piePackIcon"));
	outFields->push(HX_CSTRING("seed"));
	outFields->push(HX_CSTRING("seedDrag"));
	outFields->push(HX_CSTRING("cloud"));
	outFields->push(HX_CSTRING("sun"));
	outFields->push(HX_CSTRING("currentTileBorder"));
	outFields->push(HX_CSTRING("overlaysInUse"));
	outFields->push(HX_CSTRING("sunClicked"));
	outFields->push(HX_CSTRING("dropletClicked"));
	outFields->push(HX_CSTRING("ownedTile"));
	outFields->push(HX_CSTRING("closeTreeGrowthCheck"));
	outFields->push(HX_CSTRING("closeAppleCountCheck"));
	outFields->push(HX_CSTRING("marketOpenCheck"));
	outFields->push(HX_CSTRING("tutorialOn"));
	outFields->push(HX_CSTRING("truckTutSection"));
	outFields->push(HX_CSTRING("callBackStopOne"));
	outFields->push(HX_CSTRING("callBackStopTwo"));
	outFields->push(HX_CSTRING("tutorialWrongAnswersSkip"));
	outFields->push(HX_CSTRING("closeSecondTreeGrowthCheck"));
	outFields->push(HX_CSTRING("tutUnpauseAlertsDone"));
	outFields->push(HX_CSTRING("coinsSound"));
	outFields->push(HX_CSTRING("alertButtonSound"));
	outFields->push(HX_CSTRING("correctAnswerSound"));
	outFields->push(HX_CSTRING("incorrectAnswerSound"));
	outFields->push(HX_CSTRING("plantingSeedSound"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::ui::FlxButton*/ ,(int)offsetof(PlayState_obj,menuButton),HX_CSTRING("menuButton")},
	{hx::fsObject /*::OptionsScreen*/ ,(int)offsetof(PlayState_obj,menuScreen),HX_CSTRING("menuScreen")},
	{hx::fsObject /*::Tutorial*/ ,(int)offsetof(PlayState_obj,tutorial),HX_CSTRING("tutorial")},
	{hx::fsObject /*::Weed*/ ,(int)offsetof(PlayState_obj,weed),HX_CSTRING("weed")},
	{hx::fsObject /*::SystemStar*/ ,(int)offsetof(PlayState_obj,systemStar),HX_CSTRING("systemStar")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(PlayState_obj,backgroundSprite),HX_CSTRING("backgroundSprite")},
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(PlayState_obj,aboveGrassLayer),HX_CSTRING("aboveGrassLayer")},
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(PlayState_obj,plantLayer),HX_CSTRING("plantLayer")},
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(PlayState_obj,machineLayer),HX_CSTRING("machineLayer")},
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(PlayState_obj,belowBottomSprites),HX_CSTRING("belowBottomSprites")},
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(PlayState_obj,bottomeSprites),HX_CSTRING("bottomeSprites")},
	{hx::fsObject /*::Island*/ ,(int)offsetof(PlayState_obj,floatingIsland),HX_CSTRING("floatingIsland")},
	{hx::fsObject /*::TileBoard*/ ,(int)offsetof(PlayState_obj,tileBoard),HX_CSTRING("tileBoard")},
	{hx::fsObject /*::Tile*/ ,(int)offsetof(PlayState_obj,lastTileClicked),HX_CSTRING("lastTileClicked")},
	{hx::fsObject /*::TileBoarder*/ ,(int)offsetof(PlayState_obj,tileBoardBorder),HX_CSTRING("tileBoardBorder")},
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(PlayState_obj,board),HX_CSTRING("board")},
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(PlayState_obj,boarder),HX_CSTRING("boarder")},
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(PlayState_obj,treeArray),HX_CSTRING("treeArray")},
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(PlayState_obj,juiceMachineArray),HX_CSTRING("juiceMachineArray")},
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(PlayState_obj,pieMachineArray),HX_CSTRING("pieMachineArray")},
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(PlayState_obj,flyingJuiceBottleArray),HX_CSTRING("flyingJuiceBottleArray")},
	{hx::fsObject /*::NumberPad*/ ,(int)offsetof(PlayState_obj,numPad),HX_CSTRING("numPad")},
	{hx::fsObject /*::ProblemBoard*/ ,(int)offsetof(PlayState_obj,bb),HX_CSTRING("bb")},
	{hx::fsInt,(int)offsetof(PlayState_obj,menuVisible),HX_CSTRING("menuVisible")},
	{hx::fsInt,(int)offsetof(PlayState_obj,appleCount),HX_CSTRING("appleCount")},
	{hx::fsInt,(int)offsetof(PlayState_obj,buySignCost),HX_CSTRING("buySignCost")},
	{hx::fsInt,(int)offsetof(PlayState_obj,applePerTenCost),HX_CSTRING("applePerTenCost")},
	{hx::fsInt,(int)offsetof(PlayState_obj,juicePerTenCost),HX_CSTRING("juicePerTenCost")},
	{hx::fsInt,(int)offsetof(PlayState_obj,piePerTenCost),HX_CSTRING("piePerTenCost")},
	{hx::fsInt,(int)offsetof(PlayState_obj,currentTruckLvl),HX_CSTRING("currentTruckLvl")},
	{hx::fsFloat,(int)offsetof(PlayState_obj,randomIslandCreationTimer),HX_CSTRING("randomIslandCreationTimer")},
	{hx::fsFloat,(int)offsetof(PlayState_obj,loadAppleTimer),HX_CSTRING("loadAppleTimer")},
	{hx::fsFloat,(int)offsetof(PlayState_obj,leaveTruckTimer),HX_CSTRING("leaveTruckTimer")},
	{hx::fsFloat,(int)offsetof(PlayState_obj,appleForJuiceTimer),HX_CSTRING("appleForJuiceTimer")},
	{hx::fsFloat,(int)offsetof(PlayState_obj,appleAndJuiceForPieTimer),HX_CSTRING("appleAndJuiceForPieTimer")},
	{hx::fsFloat,(int)offsetof(PlayState_obj,islandCreationTimer),HX_CSTRING("islandCreationTimer")},
	{hx::fsString,(int)offsetof(PlayState_obj,appleCountString),HX_CSTRING("appleCountString")},
	{hx::fsString,(int)offsetof(PlayState_obj,answerString),HX_CSTRING("answerString")},
	{hx::fsBool,(int)offsetof(PlayState_obj,boardIsDown),HX_CSTRING("boardIsDown")},
	{hx::fsBool,(int)offsetof(PlayState_obj,appleDisplayLock),HX_CSTRING("appleDisplayLock")},
	{hx::fsObject /*::AsteroidBelt*/ ,(int)offsetof(PlayState_obj,asteroidBelt),HX_CSTRING("asteroidBelt")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(PlayState_obj,bridge),HX_CSTRING("bridge")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(PlayState_obj,buySign),HX_CSTRING("buySign")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(PlayState_obj,foreGroundRocks),HX_CSTRING("foreGroundRocks")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(PlayState_obj,answerFieldText),HX_CSTRING("answerFieldText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(PlayState_obj,appleCountFieldText),HX_CSTRING("appleCountFieldText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(PlayState_obj,buySignCostFieldText),HX_CSTRING("buySignCostFieldText")},
	{hx::fsObject /*::Market*/ ,(int)offsetof(PlayState_obj,market),HX_CSTRING("market")},
	{hx::fsObject /*::TruckButton*/ ,(int)offsetof(PlayState_obj,truckButton),HX_CSTRING("truckButton")},
	{hx::fsObject /*::JuiceMachine*/ ,(int)offsetof(PlayState_obj,juiceMachine),HX_CSTRING("juiceMachine")},
	{hx::fsObject /*::PieMachine*/ ,(int)offsetof(PlayState_obj,pieMachine),HX_CSTRING("pieMachine")},
	{hx::fsObject /*::AppleShed*/ ,(int)offsetof(PlayState_obj,appleShed),HX_CSTRING("appleShed")},
	{hx::fsObject /*::AppleTruck*/ ,(int)offsetof(PlayState_obj,appleTruck),HX_CSTRING("appleTruck")},
	{hx::fsObject /*::Tree*/ ,(int)offsetof(PlayState_obj,tree),HX_CSTRING("tree")},
	{hx::fsObject /*::AppleBasket*/ ,(int)offsetof(PlayState_obj,basket),HX_CSTRING("basket")},
	{hx::fsObject /*::Tree*/ ,(int)offsetof(PlayState_obj,currentTargetTree),HX_CSTRING("currentTargetTree")},
	{hx::fsObject /*::JuiceMachine*/ ,(int)offsetof(PlayState_obj,currentTargetJuiceMachine),HX_CSTRING("currentTargetJuiceMachine")},
	{hx::fsObject /*::PieMachine*/ ,(int)offsetof(PlayState_obj,currentTargetPieMachine),HX_CSTRING("currentTargetPieMachine")},
	{hx::fsObject /*::JuiceIcon*/ ,(int)offsetof(PlayState_obj,juiceMachineIcon),HX_CSTRING("juiceMachineIcon")},
	{hx::fsObject /*::PieIcon*/ ,(int)offsetof(PlayState_obj,pieMachineIcon),HX_CSTRING("pieMachineIcon")},
	{hx::fsObject /*::JuiceIcon*/ ,(int)offsetof(PlayState_obj,juiceMachineIconDrag),HX_CSTRING("juiceMachineIconDrag")},
	{hx::fsObject /*::PieIcon*/ ,(int)offsetof(PlayState_obj,pieMachineIconDrag),HX_CSTRING("pieMachineIconDrag")},
	{hx::fsObject /*::JuicePackIcon*/ ,(int)offsetof(PlayState_obj,juicePackIcon),HX_CSTRING("juicePackIcon")},
	{hx::fsObject /*::PiePackIcon*/ ,(int)offsetof(PlayState_obj,piePackIcon),HX_CSTRING("piePackIcon")},
	{hx::fsObject /*::Seed*/ ,(int)offsetof(PlayState_obj,seed),HX_CSTRING("seed")},
	{hx::fsObject /*::Seed*/ ,(int)offsetof(PlayState_obj,seedDrag),HX_CSTRING("seedDrag")},
	{hx::fsObject /*::Cloud*/ ,(int)offsetof(PlayState_obj,cloud),HX_CSTRING("cloud")},
	{hx::fsObject /*::SunToken*/ ,(int)offsetof(PlayState_obj,sun),HX_CSTRING("sun")},
	{hx::fsObject /*::TileBoarder*/ ,(int)offsetof(PlayState_obj,currentTileBorder),HX_CSTRING("currentTileBorder")},
	{hx::fsBool,(int)offsetof(PlayState_obj,overlaysInUse),HX_CSTRING("overlaysInUse")},
	{hx::fsBool,(int)offsetof(PlayState_obj,sunClicked),HX_CSTRING("sunClicked")},
	{hx::fsBool,(int)offsetof(PlayState_obj,dropletClicked),HX_CSTRING("dropletClicked")},
	{hx::fsBool,(int)offsetof(PlayState_obj,ownedTile),HX_CSTRING("ownedTile")},
	{hx::fsBool,(int)offsetof(PlayState_obj,closeTreeGrowthCheck),HX_CSTRING("closeTreeGrowthCheck")},
	{hx::fsBool,(int)offsetof(PlayState_obj,closeAppleCountCheck),HX_CSTRING("closeAppleCountCheck")},
	{hx::fsBool,(int)offsetof(PlayState_obj,marketOpenCheck),HX_CSTRING("marketOpenCheck")},
	{hx::fsBool,(int)offsetof(PlayState_obj,tutorialOn),HX_CSTRING("tutorialOn")},
	{hx::fsBool,(int)offsetof(PlayState_obj,truckTutSection),HX_CSTRING("truckTutSection")},
	{hx::fsBool,(int)offsetof(PlayState_obj,callBackStopOne),HX_CSTRING("callBackStopOne")},
	{hx::fsBool,(int)offsetof(PlayState_obj,callBackStopTwo),HX_CSTRING("callBackStopTwo")},
	{hx::fsBool,(int)offsetof(PlayState_obj,tutorialWrongAnswersSkip),HX_CSTRING("tutorialWrongAnswersSkip")},
	{hx::fsBool,(int)offsetof(PlayState_obj,closeSecondTreeGrowthCheck),HX_CSTRING("closeSecondTreeGrowthCheck")},
	{hx::fsBool,(int)offsetof(PlayState_obj,tutUnpauseAlertsDone),HX_CSTRING("tutUnpauseAlertsDone")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(PlayState_obj,coinsSound),HX_CSTRING("coinsSound")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(PlayState_obj,alertButtonSound),HX_CSTRING("alertButtonSound")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(PlayState_obj,correctAnswerSound),HX_CSTRING("correctAnswerSound")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(PlayState_obj,incorrectAnswerSound),HX_CSTRING("incorrectAnswerSound")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(PlayState_obj,plantingSeedSound),HX_CSTRING("plantingSeedSound")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("menuButton"),
	HX_CSTRING("menuScreen"),
	HX_CSTRING("tutorial"),
	HX_CSTRING("weed"),
	HX_CSTRING("systemStar"),
	HX_CSTRING("backgroundSprite"),
	HX_CSTRING("aboveGrassLayer"),
	HX_CSTRING("plantLayer"),
	HX_CSTRING("machineLayer"),
	HX_CSTRING("belowBottomSprites"),
	HX_CSTRING("bottomeSprites"),
	HX_CSTRING("floatingIsland"),
	HX_CSTRING("tileBoard"),
	HX_CSTRING("lastTileClicked"),
	HX_CSTRING("tileBoardBorder"),
	HX_CSTRING("board"),
	HX_CSTRING("boarder"),
	HX_CSTRING("treeArray"),
	HX_CSTRING("juiceMachineArray"),
	HX_CSTRING("pieMachineArray"),
	HX_CSTRING("flyingJuiceBottleArray"),
	HX_CSTRING("numPad"),
	HX_CSTRING("bb"),
	HX_CSTRING("menuVisible"),
	HX_CSTRING("appleCount"),
	HX_CSTRING("buySignCost"),
	HX_CSTRING("applePerTenCost"),
	HX_CSTRING("juicePerTenCost"),
	HX_CSTRING("piePerTenCost"),
	HX_CSTRING("currentTruckLvl"),
	HX_CSTRING("randomIslandCreationTimer"),
	HX_CSTRING("loadAppleTimer"),
	HX_CSTRING("leaveTruckTimer"),
	HX_CSTRING("appleForJuiceTimer"),
	HX_CSTRING("appleAndJuiceForPieTimer"),
	HX_CSTRING("islandCreationTimer"),
	HX_CSTRING("appleCountString"),
	HX_CSTRING("answerString"),
	HX_CSTRING("boardIsDown"),
	HX_CSTRING("appleDisplayLock"),
	HX_CSTRING("asteroidBelt"),
	HX_CSTRING("bridge"),
	HX_CSTRING("buySign"),
	HX_CSTRING("foreGroundRocks"),
	HX_CSTRING("answerFieldText"),
	HX_CSTRING("appleCountFieldText"),
	HX_CSTRING("buySignCostFieldText"),
	HX_CSTRING("market"),
	HX_CSTRING("truckButton"),
	HX_CSTRING("juiceMachine"),
	HX_CSTRING("pieMachine"),
	HX_CSTRING("appleShed"),
	HX_CSTRING("appleTruck"),
	HX_CSTRING("tree"),
	HX_CSTRING("basket"),
	HX_CSTRING("currentTargetTree"),
	HX_CSTRING("currentTargetJuiceMachine"),
	HX_CSTRING("currentTargetPieMachine"),
	HX_CSTRING("juiceMachineIcon"),
	HX_CSTRING("pieMachineIcon"),
	HX_CSTRING("juiceMachineIconDrag"),
	HX_CSTRING("pieMachineIconDrag"),
	HX_CSTRING("juicePackIcon"),
	HX_CSTRING("piePackIcon"),
	HX_CSTRING("seed"),
	HX_CSTRING("seedDrag"),
	HX_CSTRING("cloud"),
	HX_CSTRING("sun"),
	HX_CSTRING("currentTileBorder"),
	HX_CSTRING("overlaysInUse"),
	HX_CSTRING("sunClicked"),
	HX_CSTRING("dropletClicked"),
	HX_CSTRING("ownedTile"),
	HX_CSTRING("closeTreeGrowthCheck"),
	HX_CSTRING("closeAppleCountCheck"),
	HX_CSTRING("marketOpenCheck"),
	HX_CSTRING("tutorialOn"),
	HX_CSTRING("truckTutSection"),
	HX_CSTRING("callBackStopOne"),
	HX_CSTRING("callBackStopTwo"),
	HX_CSTRING("tutorialWrongAnswersSkip"),
	HX_CSTRING("closeSecondTreeGrowthCheck"),
	HX_CSTRING("tutUnpauseAlertsDone"),
	HX_CSTRING("coinsSound"),
	HX_CSTRING("alertButtonSound"),
	HX_CSTRING("correctAnswerSound"),
	HX_CSTRING("incorrectAnswerSound"),
	HX_CSTRING("plantingSeedSound"),
	HX_CSTRING("create"),
	HX_CSTRING("initPlaystate"),
	HX_CSTRING("openMenu"),
	HX_CSTRING("releaseAllClickEvents"),
	HX_CSTRING("initTreeArray"),
	HX_CSTRING("plantWeeds"),
	HX_CSTRING("createFloatingIsland"),
	HX_CSTRING("initFlyingJuiceBottleArray"),
	HX_CSTRING("initJuiceMachineArray"),
	HX_CSTRING("initPieMachineArray"),
	HX_CSTRING("addFlyingBottleArray"),
	HX_CSTRING("addTreeArray"),
	HX_CSTRING("addJuiceMachineArray"),
	HX_CSTRING("addPieMachineArray"),
	HX_CSTRING("tileBoarderFlip"),
	HX_CSTRING("buyButtonPopUp"),
	HX_CSTRING("displayAppleCount"),
	HX_CSTRING("displayPadNumbers"),
	HX_CSTRING("placeObject"),
	HX_CSTRING("treeClicked"),
	HX_CSTRING("secondTreeTutMessage"),
	HX_CSTRING("buyLand"),
	HX_CSTRING("callTruck"),
	HX_CSTRING("truckLoadingGoods"),
	HX_CSTRING("stopSeedDragIfBoard"),
	HX_CSTRING("stopJuiceDragIfBoard"),
	HX_CSTRING("stopPieDragIfBoard"),
	HX_CSTRING("drawBoard"),
	HX_CSTRING("letItRain"),
	HX_CSTRING("letItShine"),
	HX_CSTRING("unregisterAllAlertEvents"),
	HX_CSTRING("registerAllAlertEvents"),
	HX_CSTRING("checkForAlertClick"),
	HX_CSTRING("submitAnswer"),
	HX_CSTRING("destroyObjects"),
	HX_CSTRING("waterBill"),
	HX_CSTRING("electricBill"),
	HX_CSTRING("appleBill"),
	HX_CSTRING("appleAndJuiceBill"),
	HX_CSTRING("turnWaterOff"),
	HX_CSTRING("applesAndJuiceForPie"),
	HX_CSTRING("applesForJuice"),
	HX_CSTRING("waterSprinklerBill"),
	HX_CSTRING("factoryElectricBill"),
	HX_CSTRING("removeCallBackOnPad"),
	HX_CSTRING("createAsteroidBelt"),
	HX_CSTRING("currentTreeGrowthValue"),
	HX_CSTRING("marketTutOpened"),
	HX_CSTRING("restartGameOver"),
	HX_CSTRING("destroySound"),
	HX_CSTRING("update"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(PlayState_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(PlayState_obj::__mClass,"__mClass");
};

#endif

Class PlayState_obj::__mClass;

void PlayState_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("PlayState"), hx::TCanCast< PlayState_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void PlayState_obj::__boot()
{
}

