#include <hxcpp.h>

#ifndef INCLUDED_NumberPad
#include <NumberPad.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_plugin_FlxPlugin
#include <flixel/plugin/FlxPlugin.h>
#endif
#ifndef INCLUDED_flixel_plugin_MouseEventManager
#include <flixel/plugin/MouseEventManager.h>
#endif

Void NumberPad_obj::__construct()
{
HX_STACK_FRAME("NumberPad","new",0xf42ee3dc,"NumberPad.new","NumberPad.hx",12,0xbc374b14)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(31)
	this->padScrollingIn = false;
	HX_STACK_LINE(30)
	this->answerButtonPressed = false;
	HX_STACK_LINE(29)
	this->answerDigits = HX_CSTRING("");
	HX_STACK_LINE(15)
	this->numPadGroup = ::flixel::group::FlxGroup_obj::__new(null());
	HX_STACK_LINE(37)
	super::__construct(null());
	HX_STACK_LINE(38)
	this->init();
	HX_STACK_LINE(39)
	this->registerEvent();
	HX_STACK_LINE(40)
	this->positionPieces();
	HX_STACK_LINE(41)
	this->numPadSetVisible();
}
;
	return null();
}

//NumberPad_obj::~NumberPad_obj() { }

Dynamic NumberPad_obj::__CreateEmpty() { return  new NumberPad_obj; }
hx::ObjectPtr< NumberPad_obj > NumberPad_obj::__new()
{  hx::ObjectPtr< NumberPad_obj > result = new NumberPad_obj();
	result->__construct();
	return result;}

Dynamic NumberPad_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< NumberPad_obj > result = new NumberPad_obj();
	result->__construct();
	return result;}

::String NumberPad_obj::getPlayerAnswerString( ){
	HX_STACK_FRAME("NumberPad","getPlayerAnswerString",0xb97c36a2,"NumberPad.getPlayerAnswerString","NumberPad.hx",47,0xbc374b14)
	HX_STACK_THIS(this)
	HX_STACK_LINE(47)
	return this->answerDigits;
}


HX_DEFINE_DYNAMIC_FUNC0(NumberPad_obj,getPlayerAnswerString,return )

bool NumberPad_obj::getAnswerButtonPressed( ){
	HX_STACK_FRAME("NumberPad","getAnswerButtonPressed",0xfe8ecd80,"NumberPad.getAnswerButtonPressed","NumberPad.hx",53,0xbc374b14)
	HX_STACK_THIS(this)
	HX_STACK_LINE(53)
	return this->answerButtonPressed;
}


HX_DEFINE_DYNAMIC_FUNC0(NumberPad_obj,getAnswerButtonPressed,return )

bool NumberPad_obj::getPadScrollingIn( ){
	HX_STACK_FRAME("NumberPad","getPadScrollingIn",0xc7cf7759,"NumberPad.getPadScrollingIn","NumberPad.hx",58,0xbc374b14)
	HX_STACK_THIS(this)
	HX_STACK_LINE(58)
	return this->padScrollingIn;
}


HX_DEFINE_DYNAMIC_FUNC0(NumberPad_obj,getPadScrollingIn,return )

Void NumberPad_obj::setAnswerButtonPressed( bool val){
{
		HX_STACK_FRAME("NumberPad","setAnswerButtonPressed",0x323a49f4,"NumberPad.setAnswerButtonPressed","NumberPad.hx",63,0xbc374b14)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(63)
		this->answerButtonPressed = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(NumberPad_obj,setAnswerButtonPressed,(void))

Void NumberPad_obj::setPadScrollingIn( bool val){
{
		HX_STACK_FRAME("NumberPad","setPadScrollingIn",0xeb3d4f65,"NumberPad.setPadScrollingIn","NumberPad.hx",69,0xbc374b14)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(69)
		this->padScrollingIn = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(NumberPad_obj,setPadScrollingIn,(void))

Void NumberPad_obj::resetAnswerDigits( ){
{
		HX_STACK_FRAME("NumberPad","resetAnswerDigits",0x7119f32f,"NumberPad.resetAnswerDigits","NumberPad.hx",75,0xbc374b14)
		HX_STACK_THIS(this)
		HX_STACK_LINE(75)
		this->answerDigits = HX_CSTRING("");
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(NumberPad_obj,resetAnswerDigits,(void))

Void NumberPad_obj::init( ){
{
		HX_STACK_FRAME("NumberPad","init",0xb1913414,"NumberPad.init","NumberPad.hx",79,0xbc374b14)
		HX_STACK_THIS(this)
		HX_STACK_LINE(81)
		::flixel::FlxSprite _g = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(81)
		this->numberPad = _g;
		HX_STACK_LINE(82)
		::flixel::FlxSprite _g1 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(82)
		this->numZero = _g1;
		HX_STACK_LINE(83)
		::flixel::FlxSprite _g2 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(83)
		this->numOne = _g2;
		HX_STACK_LINE(84)
		::flixel::FlxSprite _g3 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(84)
		this->numTwo = _g3;
		HX_STACK_LINE(85)
		::flixel::FlxSprite _g4 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g4,"_g4");
		HX_STACK_LINE(85)
		this->numThree = _g4;
		HX_STACK_LINE(86)
		::flixel::FlxSprite _g5 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g5,"_g5");
		HX_STACK_LINE(86)
		this->numFour = _g5;
		HX_STACK_LINE(87)
		::flixel::FlxSprite _g6 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g6,"_g6");
		HX_STACK_LINE(87)
		this->numFive = _g6;
		HX_STACK_LINE(88)
		::flixel::FlxSprite _g7 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g7,"_g7");
		HX_STACK_LINE(88)
		this->numSix = _g7;
		HX_STACK_LINE(89)
		::flixel::FlxSprite _g8 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g8,"_g8");
		HX_STACK_LINE(89)
		this->numSeven = _g8;
		HX_STACK_LINE(90)
		::flixel::FlxSprite _g9 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g9,"_g9");
		HX_STACK_LINE(90)
		this->numEight = _g9;
		HX_STACK_LINE(91)
		::flixel::FlxSprite _g10 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g10,"_g10");
		HX_STACK_LINE(91)
		this->numNine = _g10;
		HX_STACK_LINE(92)
		::flixel::FlxSprite _g11 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g11,"_g11");
		HX_STACK_LINE(92)
		this->answerButt = _g11;
		HX_STACK_LINE(93)
		::flixel::FlxSprite _g12 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g12,"_g12");
		HX_STACK_LINE(93)
		this->clearButt = _g12;
		HX_STACK_LINE(94)
		this->numberPad->loadGraphic(HX_CSTRING("assets/images/calcImage.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(95)
		this->numZero->loadGraphic(HX_CSTRING("assets/images/buttonZero.png"),false,(int)0,(int)0,false,HX_CSTRING("zero"));
		HX_STACK_LINE(96)
		this->numOne->loadGraphic(HX_CSTRING("assets/images/buttonOne.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(97)
		this->numTwo->loadGraphic(HX_CSTRING("assets/images/buttonTwo.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(98)
		this->numThree->loadGraphic(HX_CSTRING("assets/images/buttonThree.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(99)
		this->numFour->loadGraphic(HX_CSTRING("assets/images/buttonFour.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(100)
		this->numFive->loadGraphic(HX_CSTRING("assets/images/buttonFive.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(101)
		this->numSix->loadGraphic(HX_CSTRING("assets/images/buttonSix.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(102)
		this->numSeven->loadGraphic(HX_CSTRING("assets/images/buttonSeven.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(103)
		this->numEight->loadGraphic(HX_CSTRING("assets/images/buttonEight.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(104)
		this->numNine->loadGraphic(HX_CSTRING("assets/images/buttonNine.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(105)
		this->answerButt->loadGraphic(HX_CSTRING("assets/images/answerButton.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(106)
		this->clearButt->loadGraphic(HX_CSTRING("assets/images/buttonClear.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(107)
		this->numberPad->set_x((int)223);
		HX_STACK_LINE(108)
		this->numberPad->set_y((int)520);
		HX_STACK_LINE(109)
		this->numPadGroup->add(this->numberPad);
		HX_STACK_LINE(110)
		this->numPadGroup->add(this->numZero);
		HX_STACK_LINE(111)
		this->numPadGroup->add(this->numOne);
		HX_STACK_LINE(112)
		this->numPadGroup->add(this->numTwo);
		HX_STACK_LINE(113)
		this->numPadGroup->add(this->numThree);
		HX_STACK_LINE(114)
		this->numPadGroup->add(this->numFour);
		HX_STACK_LINE(115)
		this->numPadGroup->add(this->numFive);
		HX_STACK_LINE(116)
		this->numPadGroup->add(this->numSix);
		HX_STACK_LINE(117)
		this->numPadGroup->add(this->numSeven);
		HX_STACK_LINE(118)
		this->numPadGroup->add(this->numEight);
		HX_STACK_LINE(119)
		this->numPadGroup->add(this->numNine);
		HX_STACK_LINE(120)
		this->numPadGroup->add(this->answerButt);
		HX_STACK_LINE(121)
		this->numPadGroup->add(this->clearButt);
		HX_STACK_LINE(122)
		this->add(this->numPadGroup);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(NumberPad_obj,init,(void))

Float NumberPad_obj::getPadY( ){
	HX_STACK_FRAME("NumberPad","getPadY",0x9ce508d8,"NumberPad.getPadY","NumberPad.hx",128,0xbc374b14)
	HX_STACK_THIS(this)
	HX_STACK_LINE(128)
	return this->numberPad->y;
}


HX_DEFINE_DYNAMIC_FUNC0(NumberPad_obj,getPadY,return )

Void NumberPad_obj::registerEvent( ){
{
		HX_STACK_FRAME("NumberPad","registerEvent",0xcc7a5133,"NumberPad.registerEvent","NumberPad.hx",132,0xbc374b14)
		HX_STACK_THIS(this)
		HX_STACK_LINE(134)
		::flixel::plugin::MouseEventManager_obj::add(this->clearButt,this->padPress_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(135)
		::flixel::plugin::MouseEventManager_obj::add(this->answerButt,this->padPress_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(136)
		::flixel::plugin::MouseEventManager_obj::add(this->numZero,this->padPress_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(137)
		::flixel::plugin::MouseEventManager_obj::add(this->numOne,this->padPress_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(138)
		::flixel::plugin::MouseEventManager_obj::add(this->numTwo,this->padPress_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(139)
		::flixel::plugin::MouseEventManager_obj::add(this->numThree,this->padPress_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(140)
		::flixel::plugin::MouseEventManager_obj::add(this->numFour,this->padPress_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(141)
		::flixel::plugin::MouseEventManager_obj::add(this->numFive,this->padPress_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(142)
		::flixel::plugin::MouseEventManager_obj::add(this->numSix,this->padPress_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(143)
		::flixel::plugin::MouseEventManager_obj::add(this->numSeven,this->padPress_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(144)
		::flixel::plugin::MouseEventManager_obj::add(this->numEight,this->padPress_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(145)
		::flixel::plugin::MouseEventManager_obj::add(this->numNine,this->padPress_dyn(),null(),null(),null(),null(),null(),null());
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(NumberPad_obj,registerEvent,(void))

Void NumberPad_obj::unregisterNumPadEvents( ){
{
		HX_STACK_FRAME("NumberPad","unregisterNumPadEvents",0x5dfa6b86,"NumberPad.unregisterNumPadEvents","NumberPad.hx",149,0xbc374b14)
		HX_STACK_THIS(this)
		HX_STACK_LINE(150)
		::flixel::plugin::MouseEventManager_obj::remove(this->clearButt);
		HX_STACK_LINE(151)
		::flixel::plugin::MouseEventManager_obj::remove(this->answerButt);
		HX_STACK_LINE(152)
		::flixel::plugin::MouseEventManager_obj::remove(this->numZero);
		HX_STACK_LINE(153)
		::flixel::plugin::MouseEventManager_obj::remove(this->numOne);
		HX_STACK_LINE(154)
		::flixel::plugin::MouseEventManager_obj::remove(this->numTwo);
		HX_STACK_LINE(155)
		::flixel::plugin::MouseEventManager_obj::remove(this->numThree);
		HX_STACK_LINE(156)
		::flixel::plugin::MouseEventManager_obj::remove(this->numFour);
		HX_STACK_LINE(157)
		::flixel::plugin::MouseEventManager_obj::remove(this->numFive);
		HX_STACK_LINE(158)
		::flixel::plugin::MouseEventManager_obj::remove(this->numSix);
		HX_STACK_LINE(159)
		::flixel::plugin::MouseEventManager_obj::remove(this->numSeven);
		HX_STACK_LINE(160)
		::flixel::plugin::MouseEventManager_obj::remove(this->numEight);
		HX_STACK_LINE(161)
		::flixel::plugin::MouseEventManager_obj::remove(this->numNine);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(NumberPad_obj,unregisterNumPadEvents,(void))

Void NumberPad_obj::positionPieces( ){
{
		HX_STACK_FRAME("NumberPad","positionPieces",0x96ecb652,"NumberPad.positionPieces","NumberPad.hx",164,0xbc374b14)
		HX_STACK_THIS(this)
		HX_STACK_LINE(166)
		this->answerButt->set_x((this->numberPad->x + (int)129));
		HX_STACK_LINE(167)
		this->answerButt->set_y((this->numberPad->y + (int)18));
		HX_STACK_LINE(168)
		this->clearButt->set_x((this->numberPad->x + (int)135));
		HX_STACK_LINE(169)
		this->clearButt->set_y((this->numberPad->y + (int)230));
		HX_STACK_LINE(171)
		this->numOne->set_x((this->numberPad->x + (int)8));
		HX_STACK_LINE(172)
		this->numOne->set_y((this->numberPad->y + (int)50));
		HX_STACK_LINE(173)
		this->numTwo->set_x((this->numberPad->x + (int)72));
		HX_STACK_LINE(174)
		this->numTwo->set_y((this->numberPad->y + (int)50));
		HX_STACK_LINE(175)
		this->numThree->set_x((this->numberPad->x + (int)135));
		HX_STACK_LINE(176)
		this->numThree->set_y((this->numberPad->y + (int)50));
		HX_STACK_LINE(178)
		this->numFour->set_x((this->numberPad->x + (int)8));
		HX_STACK_LINE(179)
		this->numFour->set_y((this->numberPad->y + (int)110));
		HX_STACK_LINE(180)
		this->numFive->set_x((this->numberPad->x + (int)72));
		HX_STACK_LINE(181)
		this->numFive->set_y((this->numberPad->y + (int)110));
		HX_STACK_LINE(182)
		this->numSix->set_x((this->numberPad->x + (int)135));
		HX_STACK_LINE(183)
		this->numSix->set_y((this->numberPad->y + (int)110));
		HX_STACK_LINE(185)
		this->numSeven->set_x((this->numberPad->x + (int)8));
		HX_STACK_LINE(186)
		this->numSeven->set_y((this->numberPad->y + (int)170));
		HX_STACK_LINE(187)
		this->numEight->set_x((this->numberPad->x + (int)72));
		HX_STACK_LINE(188)
		this->numEight->set_y((this->numberPad->y + (int)170));
		HX_STACK_LINE(189)
		this->numNine->set_x((this->numberPad->x + (int)135));
		HX_STACK_LINE(190)
		this->numNine->set_y((this->numberPad->y + (int)170));
		HX_STACK_LINE(192)
		this->numZero->set_x((this->numberPad->x + (int)72));
		HX_STACK_LINE(193)
		this->numZero->set_y((this->numberPad->y + (int)230));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(NumberPad_obj,positionPieces,(void))

Void NumberPad_obj::numPadSetVisible( ){
{
		HX_STACK_FRAME("NumberPad","numPadSetVisible",0x332de501,"NumberPad.numPadSetVisible","NumberPad.hx",197,0xbc374b14)
		HX_STACK_THIS(this)
		HX_STACK_LINE(199)
		this->numberPad->set_alpha(.5);
		HX_STACK_LINE(200)
		this->numZero->set_alpha(.7);
		HX_STACK_LINE(201)
		this->numOne->set_alpha(.7);
		HX_STACK_LINE(202)
		this->numTwo->set_alpha(.7);
		HX_STACK_LINE(203)
		this->numThree->set_alpha(.7);
		HX_STACK_LINE(204)
		this->numFour->set_alpha(.7);
		HX_STACK_LINE(205)
		this->numFive->set_alpha(.7);
		HX_STACK_LINE(206)
		this->numSix->set_alpha(.7);
		HX_STACK_LINE(207)
		this->numSeven->set_alpha(.7);
		HX_STACK_LINE(208)
		this->numEight->set_alpha(.7);
		HX_STACK_LINE(209)
		this->numNine->set_alpha(.7);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(NumberPad_obj,numPadSetVisible,(void))

Void NumberPad_obj::scrolling( ){
{
		HX_STACK_FRAME("NumberPad","scrolling",0x446ad371,"NumberPad.scrolling","NumberPad.hx",215,0xbc374b14)
		HX_STACK_THIS(this)
		HX_STACK_LINE(215)
		bool _g = this->padScrollingIn;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(215)
		bool _switch_1 = (_g);
		if (  ( _switch_1==true)){
			HX_STACK_LINE(218)
			if (((this->numberPad->y >= (int)120))){
				HX_STACK_LINE(219)
				::flixel::FlxSprite _g1 = this->numberPad;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(219)
				_g1->set_y((_g1->y - (int)10));
			}
			HX_STACK_LINE(221)
			if (((this->numberPad->y < (int)120))){
				HX_STACK_LINE(222)
				this->numberPad->set_y((int)120);
			}
		}
		else if (  ( _switch_1==false)){
			HX_STACK_LINE(225)
			if (((this->numberPad->y <= (int)520))){
				HX_STACK_LINE(226)
				::flixel::FlxSprite _g1 = this->numberPad;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(226)
				_g1->set_y((_g1->y + (int)12));
			}
			HX_STACK_LINE(228)
			if (((this->numberPad->y > (int)520))){
				HX_STACK_LINE(229)
				this->numberPad->set_y((int)520);
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(NumberPad_obj,scrolling,(void))

Void NumberPad_obj::padPress( ::flixel::FlxSprite currentButton){
{
		HX_STACK_FRAME("NumberPad","padPress",0xad7a9034,"NumberPad.padPress","NumberPad.hx",234,0xbc374b14)
		HX_STACK_THIS(this)
		HX_STACK_ARG(currentButton,"currentButton")
		HX_STACK_LINE(236)
		if (((this->answerDigits.length <= (int)4))){
			HX_STACK_LINE(238)
			if (((currentButton == this->numZero))){
				HX_STACK_LINE(240)
				hx::AddEq(this->answerDigits,HX_CSTRING("0"));
			}
			HX_STACK_LINE(243)
			if (((currentButton == this->numOne))){
				HX_STACK_LINE(245)
				hx::AddEq(this->answerDigits,HX_CSTRING("1"));
			}
			HX_STACK_LINE(248)
			if (((currentButton == this->numTwo))){
				HX_STACK_LINE(250)
				hx::AddEq(this->answerDigits,HX_CSTRING("2"));
			}
			HX_STACK_LINE(253)
			if (((currentButton == this->numThree))){
				HX_STACK_LINE(255)
				hx::AddEq(this->answerDigits,HX_CSTRING("3"));
			}
			HX_STACK_LINE(258)
			if (((currentButton == this->numFour))){
				HX_STACK_LINE(260)
				hx::AddEq(this->answerDigits,HX_CSTRING("4"));
			}
			HX_STACK_LINE(263)
			if (((currentButton == this->numFive))){
				HX_STACK_LINE(265)
				hx::AddEq(this->answerDigits,HX_CSTRING("5"));
			}
			HX_STACK_LINE(268)
			if (((currentButton == this->numSix))){
				HX_STACK_LINE(270)
				hx::AddEq(this->answerDigits,HX_CSTRING("6"));
			}
			HX_STACK_LINE(273)
			if (((currentButton == this->numSeven))){
				HX_STACK_LINE(275)
				hx::AddEq(this->answerDigits,HX_CSTRING("7"));
			}
			HX_STACK_LINE(278)
			if (((currentButton == this->numEight))){
				HX_STACK_LINE(280)
				hx::AddEq(this->answerDigits,HX_CSTRING("8"));
			}
			HX_STACK_LINE(283)
			if (((currentButton == this->numNine))){
				HX_STACK_LINE(285)
				hx::AddEq(this->answerDigits,HX_CSTRING("9"));
			}
		}
		HX_STACK_LINE(290)
		if (((currentButton == this->answerButt))){
			HX_STACK_LINE(292)
			this->answerButtonPressed = true;
		}
		HX_STACK_LINE(295)
		if (((currentButton == this->clearButt))){
			HX_STACK_LINE(297)
			this->answerDigits = HX_CSTRING("");
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(NumberPad_obj,padPress,(void))

Void NumberPad_obj::draw( ){
{
		HX_STACK_FRAME("NumberPad","draw",0xae462508,"NumberPad.draw","NumberPad.hx",305,0xbc374b14)
		HX_STACK_THIS(this)
		HX_STACK_LINE(305)
		this->super::draw();
	}
return null();
}


Void NumberPad_obj::update( ){
{
		HX_STACK_FRAME("NumberPad","update",0x0f0ec80d,"NumberPad.update","NumberPad.hx",309,0xbc374b14)
		HX_STACK_THIS(this)
		HX_STACK_LINE(311)
		this->scrolling();
		HX_STACK_LINE(312)
		this->positionPieces();
		HX_STACK_LINE(313)
		this->padPress(null());
		HX_STACK_LINE(314)
		this->super::update();
	}
return null();
}


Void NumberPad_obj::destroy( ){
{
		HX_STACK_FRAME("NumberPad","destroy",0xa496ae76,"NumberPad.destroy","NumberPad.hx",318,0xbc374b14)
		HX_STACK_THIS(this)
		HX_STACK_LINE(319)
		this->numberPad = null();
		HX_STACK_LINE(320)
		this->numZero = null();
		HX_STACK_LINE(321)
		this->numOne = null();
		HX_STACK_LINE(322)
		this->numTwo = null();
		HX_STACK_LINE(323)
		this->numThree = null();
		HX_STACK_LINE(324)
		this->numFour = null();
		HX_STACK_LINE(325)
		this->numFive = null();
		HX_STACK_LINE(326)
		this->numSix = null();
		HX_STACK_LINE(327)
		this->numSeven = null();
		HX_STACK_LINE(328)
		this->numEight = null();
		HX_STACK_LINE(329)
		this->numNine = null();
		HX_STACK_LINE(330)
		this->answerButt = null();
		HX_STACK_LINE(331)
		this->clearButt = null();
		HX_STACK_LINE(332)
		this->super::destroy();
	}
return null();
}



NumberPad_obj::NumberPad_obj()
{
}

void NumberPad_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(NumberPad);
	HX_MARK_MEMBER_NAME(numPadGroup,"numPadGroup");
	HX_MARK_MEMBER_NAME(numberPad,"numberPad");
	HX_MARK_MEMBER_NAME(numZero,"numZero");
	HX_MARK_MEMBER_NAME(numOne,"numOne");
	HX_MARK_MEMBER_NAME(numTwo,"numTwo");
	HX_MARK_MEMBER_NAME(numThree,"numThree");
	HX_MARK_MEMBER_NAME(numFour,"numFour");
	HX_MARK_MEMBER_NAME(numFive,"numFive");
	HX_MARK_MEMBER_NAME(numSix,"numSix");
	HX_MARK_MEMBER_NAME(numSeven,"numSeven");
	HX_MARK_MEMBER_NAME(numEight,"numEight");
	HX_MARK_MEMBER_NAME(numNine,"numNine");
	HX_MARK_MEMBER_NAME(answerButt,"answerButt");
	HX_MARK_MEMBER_NAME(clearButt,"clearButt");
	HX_MARK_MEMBER_NAME(answerDigits,"answerDigits");
	HX_MARK_MEMBER_NAME(answerButtonPressed,"answerButtonPressed");
	HX_MARK_MEMBER_NAME(padScrollingIn,"padScrollingIn");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void NumberPad_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(numPadGroup,"numPadGroup");
	HX_VISIT_MEMBER_NAME(numberPad,"numberPad");
	HX_VISIT_MEMBER_NAME(numZero,"numZero");
	HX_VISIT_MEMBER_NAME(numOne,"numOne");
	HX_VISIT_MEMBER_NAME(numTwo,"numTwo");
	HX_VISIT_MEMBER_NAME(numThree,"numThree");
	HX_VISIT_MEMBER_NAME(numFour,"numFour");
	HX_VISIT_MEMBER_NAME(numFive,"numFive");
	HX_VISIT_MEMBER_NAME(numSix,"numSix");
	HX_VISIT_MEMBER_NAME(numSeven,"numSeven");
	HX_VISIT_MEMBER_NAME(numEight,"numEight");
	HX_VISIT_MEMBER_NAME(numNine,"numNine");
	HX_VISIT_MEMBER_NAME(answerButt,"answerButt");
	HX_VISIT_MEMBER_NAME(clearButt,"clearButt");
	HX_VISIT_MEMBER_NAME(answerDigits,"answerDigits");
	HX_VISIT_MEMBER_NAME(answerButtonPressed,"answerButtonPressed");
	HX_VISIT_MEMBER_NAME(padScrollingIn,"padScrollingIn");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic NumberPad_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"init") ) { return init_dyn(); }
		if (HX_FIELD_EQ(inName,"draw") ) { return draw_dyn(); }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"numOne") ) { return numOne; }
		if (HX_FIELD_EQ(inName,"numTwo") ) { return numTwo; }
		if (HX_FIELD_EQ(inName,"numSix") ) { return numSix; }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"numZero") ) { return numZero; }
		if (HX_FIELD_EQ(inName,"numFour") ) { return numFour; }
		if (HX_FIELD_EQ(inName,"numFive") ) { return numFive; }
		if (HX_FIELD_EQ(inName,"numNine") ) { return numNine; }
		if (HX_FIELD_EQ(inName,"getPadY") ) { return getPadY_dyn(); }
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"numThree") ) { return numThree; }
		if (HX_FIELD_EQ(inName,"numSeven") ) { return numSeven; }
		if (HX_FIELD_EQ(inName,"numEight") ) { return numEight; }
		if (HX_FIELD_EQ(inName,"padPress") ) { return padPress_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"numberPad") ) { return numberPad; }
		if (HX_FIELD_EQ(inName,"clearButt") ) { return clearButt; }
		if (HX_FIELD_EQ(inName,"scrolling") ) { return scrolling_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"answerButt") ) { return answerButt; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"numPadGroup") ) { return numPadGroup; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"answerDigits") ) { return answerDigits; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"registerEvent") ) { return registerEvent_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"padScrollingIn") ) { return padScrollingIn; }
		if (HX_FIELD_EQ(inName,"positionPieces") ) { return positionPieces_dyn(); }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"numPadSetVisible") ) { return numPadSetVisible_dyn(); }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"getPadScrollingIn") ) { return getPadScrollingIn_dyn(); }
		if (HX_FIELD_EQ(inName,"setPadScrollingIn") ) { return setPadScrollingIn_dyn(); }
		if (HX_FIELD_EQ(inName,"resetAnswerDigits") ) { return resetAnswerDigits_dyn(); }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"answerButtonPressed") ) { return answerButtonPressed; }
		break;
	case 21:
		if (HX_FIELD_EQ(inName,"getPlayerAnswerString") ) { return getPlayerAnswerString_dyn(); }
		break;
	case 22:
		if (HX_FIELD_EQ(inName,"getAnswerButtonPressed") ) { return getAnswerButtonPressed_dyn(); }
		if (HX_FIELD_EQ(inName,"setAnswerButtonPressed") ) { return setAnswerButtonPressed_dyn(); }
		if (HX_FIELD_EQ(inName,"unregisterNumPadEvents") ) { return unregisterNumPadEvents_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic NumberPad_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"numOne") ) { numOne=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"numTwo") ) { numTwo=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"numSix") ) { numSix=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"numZero") ) { numZero=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"numFour") ) { numFour=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"numFive") ) { numFive=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"numNine") ) { numNine=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"numThree") ) { numThree=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"numSeven") ) { numSeven=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"numEight") ) { numEight=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"numberPad") ) { numberPad=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"clearButt") ) { clearButt=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"answerButt") ) { answerButt=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"numPadGroup") ) { numPadGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"answerDigits") ) { answerDigits=inValue.Cast< ::String >(); return inValue; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"padScrollingIn") ) { padScrollingIn=inValue.Cast< bool >(); return inValue; }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"answerButtonPressed") ) { answerButtonPressed=inValue.Cast< bool >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void NumberPad_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("numPadGroup"));
	outFields->push(HX_CSTRING("numberPad"));
	outFields->push(HX_CSTRING("numZero"));
	outFields->push(HX_CSTRING("numOne"));
	outFields->push(HX_CSTRING("numTwo"));
	outFields->push(HX_CSTRING("numThree"));
	outFields->push(HX_CSTRING("numFour"));
	outFields->push(HX_CSTRING("numFive"));
	outFields->push(HX_CSTRING("numSix"));
	outFields->push(HX_CSTRING("numSeven"));
	outFields->push(HX_CSTRING("numEight"));
	outFields->push(HX_CSTRING("numNine"));
	outFields->push(HX_CSTRING("answerButt"));
	outFields->push(HX_CSTRING("clearButt"));
	outFields->push(HX_CSTRING("answerDigits"));
	outFields->push(HX_CSTRING("answerButtonPressed"));
	outFields->push(HX_CSTRING("padScrollingIn"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(NumberPad_obj,numPadGroup),HX_CSTRING("numPadGroup")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(NumberPad_obj,numberPad),HX_CSTRING("numberPad")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(NumberPad_obj,numZero),HX_CSTRING("numZero")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(NumberPad_obj,numOne),HX_CSTRING("numOne")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(NumberPad_obj,numTwo),HX_CSTRING("numTwo")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(NumberPad_obj,numThree),HX_CSTRING("numThree")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(NumberPad_obj,numFour),HX_CSTRING("numFour")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(NumberPad_obj,numFive),HX_CSTRING("numFive")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(NumberPad_obj,numSix),HX_CSTRING("numSix")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(NumberPad_obj,numSeven),HX_CSTRING("numSeven")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(NumberPad_obj,numEight),HX_CSTRING("numEight")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(NumberPad_obj,numNine),HX_CSTRING("numNine")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(NumberPad_obj,answerButt),HX_CSTRING("answerButt")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(NumberPad_obj,clearButt),HX_CSTRING("clearButt")},
	{hx::fsString,(int)offsetof(NumberPad_obj,answerDigits),HX_CSTRING("answerDigits")},
	{hx::fsBool,(int)offsetof(NumberPad_obj,answerButtonPressed),HX_CSTRING("answerButtonPressed")},
	{hx::fsBool,(int)offsetof(NumberPad_obj,padScrollingIn),HX_CSTRING("padScrollingIn")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("numPadGroup"),
	HX_CSTRING("numberPad"),
	HX_CSTRING("numZero"),
	HX_CSTRING("numOne"),
	HX_CSTRING("numTwo"),
	HX_CSTRING("numThree"),
	HX_CSTRING("numFour"),
	HX_CSTRING("numFive"),
	HX_CSTRING("numSix"),
	HX_CSTRING("numSeven"),
	HX_CSTRING("numEight"),
	HX_CSTRING("numNine"),
	HX_CSTRING("answerButt"),
	HX_CSTRING("clearButt"),
	HX_CSTRING("answerDigits"),
	HX_CSTRING("answerButtonPressed"),
	HX_CSTRING("padScrollingIn"),
	HX_CSTRING("getPlayerAnswerString"),
	HX_CSTRING("getAnswerButtonPressed"),
	HX_CSTRING("getPadScrollingIn"),
	HX_CSTRING("setAnswerButtonPressed"),
	HX_CSTRING("setPadScrollingIn"),
	HX_CSTRING("resetAnswerDigits"),
	HX_CSTRING("init"),
	HX_CSTRING("getPadY"),
	HX_CSTRING("registerEvent"),
	HX_CSTRING("unregisterNumPadEvents"),
	HX_CSTRING("positionPieces"),
	HX_CSTRING("numPadSetVisible"),
	HX_CSTRING("scrolling"),
	HX_CSTRING("padPress"),
	HX_CSTRING("draw"),
	HX_CSTRING("update"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(NumberPad_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(NumberPad_obj::__mClass,"__mClass");
};

#endif

Class NumberPad_obj::__mClass;

void NumberPad_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("NumberPad"), hx::TCanCast< NumberPad_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void NumberPad_obj::__boot()
{
}

