#include <hxcpp.h>

#ifndef INCLUDED_MarketItem
#include <MarketItem.h>
#endif
#ifndef INCLUDED_SeedItem
#include <SeedItem.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif

Void SeedItem_obj::__construct()
{
HX_STACK_FRAME("SeedItem","new",0xd70fd236,"SeedItem.new","SeedItem.hx",13,0xe31de5fa)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(14)
	super::__construct();
	HX_STACK_LINE(15)
	this->initMarketItems();
}
;
	return null();
}

//SeedItem_obj::~SeedItem_obj() { }

Dynamic SeedItem_obj::__CreateEmpty() { return  new SeedItem_obj; }
hx::ObjectPtr< SeedItem_obj > SeedItem_obj::__new()
{  hx::ObjectPtr< SeedItem_obj > result = new SeedItem_obj();
	result->__construct();
	return result;}

Dynamic SeedItem_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< SeedItem_obj > result = new SeedItem_obj();
	result->__construct();
	return result;}

Void SeedItem_obj::initMarketItems( ){
{
		HX_STACK_FRAME("SeedItem","initMarketItems",0xcdcdde4a,"SeedItem.initMarketItems","SeedItem.hx",18,0xe31de5fa)
		HX_STACK_THIS(this)
		HX_STACK_LINE(19)
		::flixel::FlxSprite _g = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(19)
		this->itemSprite = _g;
		HX_STACK_LINE(20)
		this->itemCost = (int)2;
		HX_STACK_LINE(21)
		this->itemName = HX_CSTRING("seedItem");
		HX_STACK_LINE(22)
		this->itemCount = (int)2;
		HX_STACK_LINE(23)
		this->itemSprite->loadGraphic(HX_CSTRING("assets/images/seedItemAnimation.png"),true,(int)30,(int)30,false,HX_CSTRING("seedItem"));
		HX_STACK_LINE(24)
		this->itemSprite->animation->add(HX_CSTRING("available"),Array_obj< int >::__new().Add((int)0),(int)1,false);
		HX_STACK_LINE(25)
		this->itemSprite->animation->add(HX_CSTRING("notAvailable"),Array_obj< int >::__new().Add((int)1),(int)1,false);
		HX_STACK_LINE(26)
		this->setItemAvailable(true);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(SeedItem_obj,initMarketItems,(void))

Void SeedItem_obj::setSeedItemCount( int val){
{
		HX_STACK_FRAME("SeedItem","setSeedItemCount",0xf554f673,"SeedItem.setSeedItemCount","SeedItem.hx",30,0xe31de5fa)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(30)
		this->itemCount = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SeedItem_obj,setSeedItemCount,(void))

Void SeedItem_obj::update( ){
{
		HX_STACK_FRAME("SeedItem","update",0xc473cdf3,"SeedItem.update","SeedItem.hx",33,0xe31de5fa)
		HX_STACK_THIS(this)
		HX_STACK_LINE(34)
		this->setAvailableAnimation();
		HX_STACK_LINE(35)
		this->super::update();
	}
return null();
}



SeedItem_obj::SeedItem_obj()
{
}

Dynamic SeedItem_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"initMarketItems") ) { return initMarketItems_dyn(); }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"setSeedItemCount") ) { return setSeedItemCount_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic SeedItem_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	return super::__SetField(inName,inValue,inCallProp);
}

void SeedItem_obj::__GetFields(Array< ::String> &outFields)
{
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo *sMemberStorageInfo = 0;
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("initMarketItems"),
	HX_CSTRING("setSeedItemCount"),
	HX_CSTRING("update"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(SeedItem_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(SeedItem_obj::__mClass,"__mClass");
};

#endif

Class SeedItem_obj::__mClass;

void SeedItem_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("SeedItem"), hx::TCanCast< SeedItem_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void SeedItem_obj::__boot()
{
}

