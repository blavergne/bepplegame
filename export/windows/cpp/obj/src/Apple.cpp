#include <hxcpp.h>

#ifndef INCLUDED_Apple
#include <Apple.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxPooled
#include <flixel/interfaces/IFlxPooled.h>
#endif
#ifndef INCLUDED_flixel_system_FlxSound
#include <flixel/system/FlxSound.h>
#endif
#ifndef INCLUDED_flixel_system_frontEnds_SoundFrontEnd
#include <flixel/system/frontEnds/SoundFrontEnd.h>
#endif
#ifndef INCLUDED_flixel_util_FlxDestroyUtil
#include <flixel/util/FlxDestroyUtil.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPoint
#include <flixel/util/FlxPoint.h>
#endif
#ifndef INCLUDED_flixel_util_FlxRandom
#include <flixel/util/FlxRandom.h>
#endif
#ifndef INCLUDED_haxe_Log
#include <haxe/Log.h>
#endif
#ifndef INCLUDED_openfl__v2_events_EventDispatcher
#include <openfl/_v2/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__v2_events_IEventDispatcher
#include <openfl/_v2/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__v2_media_SoundChannel
#include <openfl/_v2/media/SoundChannel.h>
#endif

Void Apple_obj::__construct()
{
HX_STACK_FRAME("Apple","new",0x3b80bb2c,"Apple.new","Apple.hx",19,0xf157cdc4)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(48)
	this->myRipeCountDown = (int)5;
	HX_STACK_LINE(44)
	this->dropRate = (int)300;
	HX_STACK_LINE(41)
	this->rotTimer = (int)15;
	HX_STACK_LINE(40)
	this->timeToDrop = (int)15;
	HX_STACK_LINE(39)
	this->stillOnTree = true;
	HX_STACK_LINE(38)
	this->leavesFallOnce = false;
	HX_STACK_LINE(37)
	this->inBasket = false;
	HX_STACK_LINE(36)
	this->flyToBasket = false;
	HX_STACK_LINE(35)
	this->iAmRotten = false;
	HX_STACK_LINE(34)
	this->rotLock = false;
	HX_STACK_LINE(33)
	this->appleDropped = false;
	HX_STACK_LINE(32)
	this->iAmDead = false;
	HX_STACK_LINE(31)
	this->iAmVisible = false;
	HX_STACK_LINE(30)
	this->iAmRipe = false;
	HX_STACK_LINE(29)
	this->appleGrowth = (int)-1;
	HX_STACK_LINE(52)
	super::__construct(null());
	HX_STACK_LINE(53)
	::flixel::FlxSprite _g = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(53)
	this->apple = _g;
	HX_STACK_LINE(54)
	::flixel::group::FlxGroup _g1 = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g1,"_g1");
	HX_STACK_LINE(54)
	this->appleGroup = _g1;
	HX_STACK_LINE(55)
	::flixel::FlxSprite _g2 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g2,"_g2");
	HX_STACK_LINE(55)
	this->pickedAppleLeaves = _g2;
	HX_STACK_LINE(56)
	this->basket = null();
	HX_STACK_LINE(57)
	this->apple->loadGraphic(HX_CSTRING("assets/images/animatedAppleB.png"),true,(int)15,(int)16,null(),null());
	HX_STACK_LINE(58)
	this->apple->animation->add(HX_CSTRING("appleStart"),Array_obj< int >::__new().Add((int)0),(int)1,false);
	HX_STACK_LINE(59)
	this->apple->animation->add(HX_CSTRING("appleRipen"),Array_obj< int >::__new().Add((int)1).Add((int)2).Add((int)3).Add((int)4).Add((int)5).Add((int)6).Add((int)7),(int)3,false);
	HX_STACK_LINE(60)
	this->apple->animation->add(HX_CSTRING("flyingApple"),Array_obj< int >::__new().Add((int)7),(int)1,false);
	HX_STACK_LINE(61)
	this->apple->animation->add(HX_CSTRING("appleRot"),Array_obj< int >::__new().Add((int)8).Add((int)9).Add((int)10).Add((int)11),(int)3,false);
	HX_STACK_LINE(62)
	this->apple->set_alpha((int)0);
	HX_STACK_LINE(63)
	this->pickedAppleLeaves->loadGraphic(HX_CSTRING("assets/images/pickedAppleLeaves.png"),true,(int)16,(int)16,null(),null());
	HX_STACK_LINE(64)
	this->pickedAppleLeaves->animation->add(HX_CSTRING("leavesFall"),Array_obj< int >::__new().Add((int)0).Add((int)1).Add((int)2).Add((int)3).Add((int)4),(int)5,false);
	HX_STACK_LINE(65)
	this->pickedAppleLeaves->set_alpha((int)0);
	HX_STACK_LINE(66)
	this->appleGroup->add(this->apple);
	HX_STACK_LINE(67)
	this->appleGroup->add(this->pickedAppleLeaves);
	HX_STACK_LINE(68)
	this->add(this->appleGroup);
}
;
	return null();
}

//Apple_obj::~Apple_obj() { }

Dynamic Apple_obj::__CreateEmpty() { return  new Apple_obj; }
hx::ObjectPtr< Apple_obj > Apple_obj::__new()
{  hx::ObjectPtr< Apple_obj > result = new Apple_obj();
	result->__construct();
	return result;}

Dynamic Apple_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Apple_obj > result = new Apple_obj();
	result->__construct();
	return result;}

int Apple_obj::getRandomX( ){
	HX_STACK_FRAME("Apple","getRandomX",0xa7e76db3,"Apple.getRandomX","Apple.hx",73,0xf157cdc4)
	HX_STACK_THIS(this)
	HX_STACK_LINE(75)
	int _g = ::flixel::util::FlxRandom_obj::intRanged((int)10,(int)64,null());		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(75)
	this->randomXPos = _g;
	HX_STACK_LINE(76)
	return this->randomXPos;
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,getRandomX,return )

int Apple_obj::getRandomY( ){
	HX_STACK_FRAME("Apple","getRandomY",0xa7e76db4,"Apple.getRandomY","Apple.hx",80,0xf157cdc4)
	HX_STACK_THIS(this)
	HX_STACK_LINE(82)
	int _g = ::flixel::util::FlxRandom_obj::intRanged((int)25,(int)36,null());		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(82)
	this->randomYPos = _g;
	HX_STACK_LINE(83)
	return this->randomYPos;
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,getRandomY,return )

Float Apple_obj::getMyXPos( ){
	HX_STACK_FRAME("Apple","getMyXPos",0x9cf3f68a,"Apple.getMyXPos","Apple.hx",89,0xf157cdc4)
	HX_STACK_THIS(this)
	HX_STACK_LINE(89)
	return this->apple->x;
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,getMyXPos,return )

bool Apple_obj::getAppleDropped( ){
	HX_STACK_FRAME("Apple","getAppleDropped",0xc697c848,"Apple.getAppleDropped","Apple.hx",94,0xf157cdc4)
	HX_STACK_THIS(this)
	HX_STACK_LINE(94)
	return this->appleDropped;
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,getAppleDropped,return )

Float Apple_obj::getMyYPos( ){
	HX_STACK_FRAME("Apple","getMyYPos",0x9d9d2d29,"Apple.getMyYPos","Apple.hx",99,0xf157cdc4)
	HX_STACK_THIS(this)
	HX_STACK_LINE(99)
	return this->apple->y;
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,getMyYPos,return )

::flixel::FlxSprite Apple_obj::getAppleSprite( ){
	HX_STACK_FRAME("Apple","getAppleSprite",0x229458bd,"Apple.getAppleSprite","Apple.hx",105,0xf157cdc4)
	HX_STACK_THIS(this)
	HX_STACK_LINE(105)
	return this->apple;
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,getAppleSprite,return )

Float Apple_obj::getRotTimer( ){
	HX_STACK_FRAME("Apple","getRotTimer",0xb488b3b0,"Apple.getRotTimer","Apple.hx",110,0xf157cdc4)
	HX_STACK_THIS(this)
	HX_STACK_LINE(110)
	return this->rotTimer;
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,getRotTimer,return )

bool Apple_obj::getAppleVisible( ){
	HX_STACK_FRAME("Apple","getAppleVisible",0x62d31cba,"Apple.getAppleVisible","Apple.hx",115,0xf157cdc4)
	HX_STACK_THIS(this)
	HX_STACK_LINE(115)
	return this->iAmVisible;
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,getAppleVisible,return )

bool Apple_obj::getIAmRotten( ){
	HX_STACK_FRAME("Apple","getIAmRotten",0x1acdabf9,"Apple.getIAmRotten","Apple.hx",120,0xf157cdc4)
	HX_STACK_THIS(this)
	HX_STACK_LINE(120)
	return this->iAmRotten;
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,getIAmRotten,return )

bool Apple_obj::getIAmRipe( ){
	HX_STACK_FRAME("Apple","getIAmRipe",0xc6e9be7f,"Apple.getIAmRipe","Apple.hx",125,0xf157cdc4)
	HX_STACK_THIS(this)
	HX_STACK_LINE(125)
	return this->iAmRipe;
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,getIAmRipe,return )

int Apple_obj::getAppleGrowth( ){
	HX_STACK_FRAME("Apple","getAppleGrowth",0x7b2828ff,"Apple.getAppleGrowth","Apple.hx",130,0xf157cdc4)
	HX_STACK_THIS(this)
	HX_STACK_LINE(130)
	return this->appleGrowth;
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,getAppleGrowth,return )

Float Apple_obj::getMyInitialY( ){
	HX_STACK_FRAME("Apple","getMyInitialY",0x92f608e3,"Apple.getMyInitialY","Apple.hx",134,0xf157cdc4)
	HX_STACK_THIS(this)
	HX_STACK_LINE(134)
	return this->myInitialY;
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,getMyInitialY,return )

bool Apple_obj::getIAmDead( ){
	HX_STACK_FRAME("Apple","getIAmDead",0xbda5abb7,"Apple.getIAmDead","Apple.hx",138,0xf157cdc4)
	HX_STACK_THIS(this)
	HX_STACK_LINE(138)
	return this->iAmDead;
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,getIAmDead,return )

bool Apple_obj::getInBasket( ){
	HX_STACK_FRAME("Apple","getInBasket",0x0d25578d,"Apple.getInBasket","Apple.hx",141,0xf157cdc4)
	HX_STACK_THIS(this)
	HX_STACK_LINE(141)
	return this->inBasket;
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,getInBasket,return )

Void Apple_obj::setMyInitialY( Float val){
{
		HX_STACK_FRAME("Apple","setMyInitialY",0xd7fbeaef,"Apple.setMyInitialY","Apple.hx",145,0xf157cdc4)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(145)
		this->myInitialY = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Apple_obj,setMyInitialY,(void))

Void Apple_obj::setDropRate( int val){
{
		HX_STACK_FRAME("Apple","setDropRate",0x2b265b3d,"Apple.setDropRate","Apple.hx",149,0xf157cdc4)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(149)
		this->dropRate = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Apple_obj,setDropRate,(void))

Void Apple_obj::setFlyToBasket( bool val){
{
		HX_STACK_FRAME("Apple","setFlyToBasket",0x0511f3e6,"Apple.setFlyToBasket","Apple.hx",152,0xf157cdc4)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(152)
		this->flyToBasket = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Apple_obj,setFlyToBasket,(void))

Void Apple_obj::ripenApple( ){
{
		HX_STACK_FRAME("Apple","ripenApple",0xb9b9272c,"Apple.ripenApple","Apple.hx",156,0xf157cdc4)
		HX_STACK_THIS(this)
		HX_STACK_LINE(158)
		hx::SubEq(this->myRipeCountDown,::flixel::FlxG_obj::elapsed);
		HX_STACK_LINE(160)
		if (((this->myRipeCountDown < (int)0))){
			HX_STACK_LINE(162)
			(this->appleGrowth)++;
			HX_STACK_LINE(163)
			this->growApple();
			HX_STACK_LINE(164)
			this->myRipeCountDown = (int)5;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,ripenApple,(void))

Void Apple_obj::pauseRipen( ){
{
		HX_STACK_FRAME("Apple","pauseRipen",0x31bac160,"Apple.pauseRipen","Apple.hx",171,0xf157cdc4)
		HX_STACK_THIS(this)
		HX_STACK_LINE(171)
		this->myRipeCountDown = (int)9999;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,pauseRipen,(void))

Void Apple_obj::unpauseRipen( ){
{
		HX_STACK_FRAME("Apple","unpauseRipen",0x773005f9,"Apple.unpauseRipen","Apple.hx",175,0xf157cdc4)
		HX_STACK_THIS(this)
		HX_STACK_LINE(175)
		this->myRipeCountDown = (int)5;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,unpauseRipen,(void))

Void Apple_obj::growApple( ){
{
		HX_STACK_FRAME("Apple","growApple",0x511a8013,"Apple.growApple","Apple.hx",180,0xf157cdc4)
		HX_STACK_THIS(this)
		HX_STACK_LINE(180)
		int _g = this->appleGrowth;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(180)
		switch( (int)(_g)){
			case (int)0: {
				HX_STACK_LINE(183)
				this->iAmVisible = true;
				HX_STACK_LINE(184)
				this->apple->animation->play(HX_CSTRING("appleStart"),null(),null());
			}
			;break;
			case (int)1: {
				HX_STACK_LINE(187)
				this->apple->animation->play(HX_CSTRING("appleRipen"),null(),null());
				HX_STACK_LINE(188)
				this->iAmRipe = true;
			}
			;break;
			case (int)2: {
				HX_STACK_LINE(191)
				this->apple->animation->play(HX_CSTRING("flyingApple"),null(),null());
			}
			;break;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,growApple,(void))

Void Apple_obj::revealApples( ){
{
		HX_STACK_FRAME("Apple","revealApples",0x4267207a,"Apple.revealApples","Apple.hx",198,0xf157cdc4)
		HX_STACK_THIS(this)
		HX_STACK_LINE(198)
		if (((bool(this->iAmVisible) && bool((this->apple->alpha <= (int)1))))){
			HX_STACK_LINE(199)
			::flixel::FlxSprite _g = this->apple;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(199)
			_g->set_alpha((_g->alpha + .01));
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,revealApples,(void))

Void Apple_obj::setAppleGrowth( int val){
{
		HX_STACK_FRAME("Apple","setAppleGrowth",0x9b481173,"Apple.setAppleGrowth","Apple.hx",205,0xf157cdc4)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(205)
		this->appleGrowth = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Apple_obj,setAppleGrowth,(void))

Void Apple_obj::setAppleX( Float xVal){
{
		HX_STACK_FRAME("Apple","setAppleX",0x9553aacc,"Apple.setAppleX","Apple.hx",210,0xf157cdc4)
		HX_STACK_THIS(this)
		HX_STACK_ARG(xVal,"xVal")
		HX_STACK_LINE(210)
		this->apple->set_x(xVal);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Apple_obj,setAppleX,(void))

Void Apple_obj::setAppleY( Float yVal){
{
		HX_STACK_FRAME("Apple","setAppleY",0x9553aacd,"Apple.setAppleY","Apple.hx",215,0xf157cdc4)
		HX_STACK_THIS(this)
		HX_STACK_ARG(yVal,"yVal")
		HX_STACK_LINE(215)
		this->apple->set_y(yVal);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Apple_obj,setAppleY,(void))

Void Apple_obj::setLeavesFallOnce( bool val){
{
		HX_STACK_FRAME("Apple","setLeavesFallOnce",0xcc53da66,"Apple.setLeavesFallOnce","Apple.hx",220,0xf157cdc4)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(220)
		this->leavesFallOnce = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Apple_obj,setLeavesFallOnce,(void))

Void Apple_obj::incAppleGrowth( ){
{
		HX_STACK_FRAME("Apple","incAppleGrowth",0x1690f1b7,"Apple.incAppleGrowth","Apple.hx",225,0xf157cdc4)
		HX_STACK_THIS(this)
		HX_STACK_LINE(225)
		(this->appleGrowth)++;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,incAppleGrowth,(void))

Void Apple_obj::stopFreeFall( ){
{
		HX_STACK_FRAME("Apple","stopFreeFall",0xc459251d,"Apple.stopFreeFall","Apple.hx",229,0xf157cdc4)
		HX_STACK_THIS(this)
		HX_STACK_LINE(230)
		this->apple->velocity->set_y((int)0);
		HX_STACK_LINE(231)
		this->run = -((((this->basket->x + (int)30) - this->apple->x)));
		HX_STACK_LINE(232)
		this->rise = -((((this->basket->y + (int)30) - this->apple->y)));
		HX_STACK_LINE(233)
		if ((this->stillOnTree)){
			HX_STACK_LINE(234)
			::flixel::system::FlxSound _g = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/fallenAppleSound.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(234)
			this->fallenAppleSound = _g;
			HX_STACK_LINE(235)
			this->fallenAppleSound->play(null());
			HX_STACK_LINE(236)
			this->stillOnTree = false;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,stopFreeFall,(void))

Void Apple_obj::dropApple( ){
{
		HX_STACK_FRAME("Apple","dropApple",0x0bc6be77,"Apple.dropApple","Apple.hx",241,0xf157cdc4)
		HX_STACK_THIS(this)
		HX_STACK_LINE(241)
		if ((this->iAmRipe)){
			HX_STACK_LINE(242)
			hx::SubEq(this->timeToDrop,::flixel::FlxG_obj::elapsed);
			HX_STACK_LINE(243)
			if (((this->timeToDrop < (int)0))){
				HX_STACK_LINE(244)
				this->appleDropped = true;
				HX_STACK_LINE(245)
				this->apple->acceleration->set_y(this->dropRate);
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,dropApple,(void))

Void Apple_obj::rotApple( ){
{
		HX_STACK_FRAME("Apple","rotApple",0x71a49177,"Apple.rotApple","Apple.hx",251,0xf157cdc4)
		HX_STACK_THIS(this)
		HX_STACK_LINE(251)
		if ((this->appleDropped)){
			HX_STACK_LINE(252)
			hx::SubEq(this->rotTimer,::flixel::FlxG_obj::elapsed);
			HX_STACK_LINE(253)
			if (((bool((this->rotTimer < (int)0)) && bool(!(this->rotLock))))){
				HX_STACK_LINE(254)
				this->apple->animation->play(HX_CSTRING("appleRot"),null(),null());
				HX_STACK_LINE(255)
				this->iAmRotten = true;
				HX_STACK_LINE(256)
				this->rotTimer = (int)5;
				HX_STACK_LINE(257)
				this->rotLock = true;
			}
			HX_STACK_LINE(259)
			if (((bool((this->rotTimer < (int)0)) && bool(this->rotLock)))){
				HX_STACK_LINE(260)
				this->iAmDead = true;
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,rotApple,(void))

Void Apple_obj::setRiseRun( ::flixel::FlxSprite basketSprite){
{
		HX_STACK_FRAME("Apple","setRiseRun",0xbfd411f4,"Apple.setRiseRun","Apple.hx",265,0xf157cdc4)
		HX_STACK_THIS(this)
		HX_STACK_ARG(basketSprite,"basketSprite")
		HX_STACK_LINE(266)
		this->basket = basketSprite;
		HX_STACK_LINE(267)
		if (((this->basket != null()))){
			HX_STACK_LINE(268)
			this->run = -((((this->basket->x + (int)30) - this->apple->x)));
			HX_STACK_LINE(269)
			this->rise = -((((this->basket->y + (int)30) - this->apple->y)));
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Apple_obj,setRiseRun,(void))

Void Apple_obj::sendAppleToBasket( ){
{
		HX_STACK_FRAME("Apple","sendAppleToBasket",0xabc554ff,"Apple.sendAppleToBasket","Apple.hx",274,0xf157cdc4)
		HX_STACK_THIS(this)
		HX_STACK_LINE(274)
		if (((bool(this->flyToBasket) && bool((this->basket != null()))))){
			HX_STACK_LINE(275)
			if (((this->apple != null()))){
				HX_STACK_LINE(276)
				if (((this->apple->y >= (this->basket->y + (int)30)))){
					HX_STACK_LINE(277)
					::flixel::FlxSprite _g = this->apple;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(277)
					_g->set_y((_g->y - (Float(this->rise) / Float((int)25))));
				}
				HX_STACK_LINE(279)
				if (((this->apple->x >= (this->basket->x + (int)30)))){
					HX_STACK_LINE(280)
					::flixel::FlxSprite _g = this->apple;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(280)
					_g->set_x((_g->x - (Float(this->run) / Float((int)25))));
				}
				HX_STACK_LINE(282)
				if (((bool((this->apple->x <= (this->basket->x + (int)32))) && bool((this->apple->y <= (this->basket->y + (int)32)))))){
					HX_STACK_LINE(283)
					this->inBasket = true;
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,sendAppleToBasket,(void))

Void Apple_obj::leavesFallAnimation( ){
{
		HX_STACK_FRAME("Apple","leavesFallAnimation",0xfe888899,"Apple.leavesFallAnimation","Apple.hx",289,0xf157cdc4)
		HX_STACK_THIS(this)
		HX_STACK_LINE(290)
		if (((bool(!(this->appleDropped)) && bool(this->leavesFallOnce)))){
			HX_STACK_LINE(291)
			::flixel::system::FlxSound _g = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/pickedLeavesSound.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(291)
			this->pickedLeavesSound = _g;
			HX_STACK_LINE(292)
			this->pickedLeavesSound->play(null());
			HX_STACK_LINE(293)
			this->pickedAppleLeaves->set_x((this->apple->x + (int)5));
			HX_STACK_LINE(294)
			this->pickedAppleLeaves->set_y((this->apple->y + (int)5));
			HX_STACK_LINE(295)
			this->pickedAppleLeaves->set_alpha((int)1);
			HX_STACK_LINE(296)
			this->pickedAppleLeaves->animation->play(HX_CSTRING("leavesFall"),null(),null());
			HX_STACK_LINE(297)
			this->leavesFallOnce = false;
		}
		HX_STACK_LINE(299)
		if (((bool(this->appleDropped) && bool(this->leavesFallOnce)))){
			HX_STACK_LINE(300)
			::flixel::system::FlxSound _g1 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/pickedApplesGround.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(300)
			this->pickedAppleGround = _g1;
			HX_STACK_LINE(301)
			this->pickedAppleGround->play(null());
			HX_STACK_LINE(302)
			this->leavesFallOnce = false;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,leavesFallAnimation,(void))

Void Apple_obj::resolveAnimation( ){
{
		HX_STACK_FRAME("Apple","resolveAnimation",0xe463442c,"Apple.resolveAnimation","Apple.hx",307,0xf157cdc4)
		HX_STACK_THIS(this)
		HX_STACK_LINE(307)
		if ((this->pickedAppleLeaves->animation->get_finished())){
			HX_STACK_LINE(308)
			this->pickedAppleLeaves->set_alpha((int)0);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,resolveAnimation,(void))

Void Apple_obj::destroySounds( ){
{
		HX_STACK_FRAME("Apple","destroySounds",0x699b232a,"Apple.destroySounds","Apple.hx",312,0xf157cdc4)
		HX_STACK_THIS(this)
		HX_STACK_LINE(313)
		if (((bool((this->pickedAppleGround != null())) && bool(!(((this->pickedAppleGround->_channel != null()))))))){
			HX_STACK_LINE(314)
			::flixel::system::FlxSound _g = ::flixel::util::FlxDestroyUtil_obj::destroy(this->pickedAppleGround);		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(314)
			this->pickedAppleGround = _g;
			HX_STACK_LINE(315)
			::haxe::Log_obj::trace(HX_CSTRING("destroying pickedAppleGroundSound"),hx::SourceInfo(HX_CSTRING("Apple.hx"),315,HX_CSTRING("Apple"),HX_CSTRING("destroySounds")));
		}
		HX_STACK_LINE(317)
		if (((bool((this->fallenAppleSound != null())) && bool(!(((this->fallenAppleSound->_channel != null()))))))){
			HX_STACK_LINE(318)
			::flixel::system::FlxSound _g1 = ::flixel::util::FlxDestroyUtil_obj::destroy(this->fallenAppleSound);		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(318)
			this->fallenAppleSound = _g1;
			HX_STACK_LINE(319)
			::haxe::Log_obj::trace(HX_CSTRING("destroying fallenAppleSound"),hx::SourceInfo(HX_CSTRING("Apple.hx"),319,HX_CSTRING("Apple"),HX_CSTRING("destroySounds")));
		}
		HX_STACK_LINE(321)
		if (((bool((this->pickedLeavesSound != null())) && bool(!(((this->pickedLeavesSound->_channel != null()))))))){
			HX_STACK_LINE(322)
			::flixel::system::FlxSound _g2 = ::flixel::util::FlxDestroyUtil_obj::destroy(this->pickedLeavesSound);		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(322)
			this->pickedLeavesSound = _g2;
			HX_STACK_LINE(323)
			::haxe::Log_obj::trace(HX_CSTRING("destroying pickedLeavesSound"),hx::SourceInfo(HX_CSTRING("Apple.hx"),323,HX_CSTRING("Apple"),HX_CSTRING("destroySounds")));
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Apple_obj,destroySounds,(void))

Void Apple_obj::update( ){
{
		HX_STACK_FRAME("Apple","update",0xc21e62bd,"Apple.update","Apple.hx",327,0xf157cdc4)
		HX_STACK_THIS(this)
		HX_STACK_LINE(329)
		this->rotApple();
		HX_STACK_LINE(330)
		this->dropApple();
		HX_STACK_LINE(331)
		this->revealApples();
		HX_STACK_LINE(332)
		this->sendAppleToBasket();
		HX_STACK_LINE(333)
		this->leavesFallAnimation();
		HX_STACK_LINE(334)
		this->resolveAnimation();
		HX_STACK_LINE(335)
		this->destroySounds();
		HX_STACK_LINE(336)
		this->super::update();
	}
return null();
}


Void Apple_obj::destroy( ){
{
		HX_STACK_FRAME("Apple","destroy",0x9f2e6dc6,"Apple.destroy","Apple.hx",340,0xf157cdc4)
		HX_STACK_THIS(this)
		HX_STACK_LINE(342)
		this->apple = null();
		HX_STACK_LINE(343)
		this->appleGroup = null();
		HX_STACK_LINE(344)
		this->basket = null();
		HX_STACK_LINE(345)
		this->pickedAppleLeaves = null();
		HX_STACK_LINE(346)
		this->pickedLeavesSound = null();
		HX_STACK_LINE(347)
		this->pickedAppleGround = null();
		HX_STACK_LINE(348)
		this->fallenAppleSound = null();
		HX_STACK_LINE(349)
		this->super::destroy();
	}
return null();
}



Apple_obj::Apple_obj()
{
}

void Apple_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Apple);
	HX_MARK_MEMBER_NAME(appleGroup,"appleGroup");
	HX_MARK_MEMBER_NAME(apple,"apple");
	HX_MARK_MEMBER_NAME(basket,"basket");
	HX_MARK_MEMBER_NAME(pickedAppleLeaves,"pickedAppleLeaves");
	HX_MARK_MEMBER_NAME(pickedLeavesSound,"pickedLeavesSound");
	HX_MARK_MEMBER_NAME(pickedAppleGround,"pickedAppleGround");
	HX_MARK_MEMBER_NAME(fallenAppleSound,"fallenAppleSound");
	HX_MARK_MEMBER_NAME(appleGrowth,"appleGrowth");
	HX_MARK_MEMBER_NAME(iAmRipe,"iAmRipe");
	HX_MARK_MEMBER_NAME(iAmVisible,"iAmVisible");
	HX_MARK_MEMBER_NAME(iAmDead,"iAmDead");
	HX_MARK_MEMBER_NAME(appleDropped,"appleDropped");
	HX_MARK_MEMBER_NAME(rotLock,"rotLock");
	HX_MARK_MEMBER_NAME(iAmRotten,"iAmRotten");
	HX_MARK_MEMBER_NAME(flyToBasket,"flyToBasket");
	HX_MARK_MEMBER_NAME(inBasket,"inBasket");
	HX_MARK_MEMBER_NAME(leavesFallOnce,"leavesFallOnce");
	HX_MARK_MEMBER_NAME(stillOnTree,"stillOnTree");
	HX_MARK_MEMBER_NAME(timeToDrop,"timeToDrop");
	HX_MARK_MEMBER_NAME(rotTimer,"rotTimer");
	HX_MARK_MEMBER_NAME(myInitialY,"myInitialY");
	HX_MARK_MEMBER_NAME(randomXPos,"randomXPos");
	HX_MARK_MEMBER_NAME(dropRate,"dropRate");
	HX_MARK_MEMBER_NAME(rise,"rise");
	HX_MARK_MEMBER_NAME(run,"run");
	HX_MARK_MEMBER_NAME(randomYPos,"randomYPos");
	HX_MARK_MEMBER_NAME(myRipeCountDown,"myRipeCountDown");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void Apple_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(appleGroup,"appleGroup");
	HX_VISIT_MEMBER_NAME(apple,"apple");
	HX_VISIT_MEMBER_NAME(basket,"basket");
	HX_VISIT_MEMBER_NAME(pickedAppleLeaves,"pickedAppleLeaves");
	HX_VISIT_MEMBER_NAME(pickedLeavesSound,"pickedLeavesSound");
	HX_VISIT_MEMBER_NAME(pickedAppleGround,"pickedAppleGround");
	HX_VISIT_MEMBER_NAME(fallenAppleSound,"fallenAppleSound");
	HX_VISIT_MEMBER_NAME(appleGrowth,"appleGrowth");
	HX_VISIT_MEMBER_NAME(iAmRipe,"iAmRipe");
	HX_VISIT_MEMBER_NAME(iAmVisible,"iAmVisible");
	HX_VISIT_MEMBER_NAME(iAmDead,"iAmDead");
	HX_VISIT_MEMBER_NAME(appleDropped,"appleDropped");
	HX_VISIT_MEMBER_NAME(rotLock,"rotLock");
	HX_VISIT_MEMBER_NAME(iAmRotten,"iAmRotten");
	HX_VISIT_MEMBER_NAME(flyToBasket,"flyToBasket");
	HX_VISIT_MEMBER_NAME(inBasket,"inBasket");
	HX_VISIT_MEMBER_NAME(leavesFallOnce,"leavesFallOnce");
	HX_VISIT_MEMBER_NAME(stillOnTree,"stillOnTree");
	HX_VISIT_MEMBER_NAME(timeToDrop,"timeToDrop");
	HX_VISIT_MEMBER_NAME(rotTimer,"rotTimer");
	HX_VISIT_MEMBER_NAME(myInitialY,"myInitialY");
	HX_VISIT_MEMBER_NAME(randomXPos,"randomXPos");
	HX_VISIT_MEMBER_NAME(dropRate,"dropRate");
	HX_VISIT_MEMBER_NAME(rise,"rise");
	HX_VISIT_MEMBER_NAME(run,"run");
	HX_VISIT_MEMBER_NAME(randomYPos,"randomYPos");
	HX_VISIT_MEMBER_NAME(myRipeCountDown,"myRipeCountDown");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic Apple_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"run") ) { return run; }
		break;
	case 4:
		if (HX_FIELD_EQ(inName,"rise") ) { return rise; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"apple") ) { return apple; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"basket") ) { return basket; }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"iAmRipe") ) { return iAmRipe; }
		if (HX_FIELD_EQ(inName,"iAmDead") ) { return iAmDead; }
		if (HX_FIELD_EQ(inName,"rotLock") ) { return rotLock; }
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"inBasket") ) { return inBasket; }
		if (HX_FIELD_EQ(inName,"rotTimer") ) { return rotTimer; }
		if (HX_FIELD_EQ(inName,"dropRate") ) { return dropRate; }
		if (HX_FIELD_EQ(inName,"rotApple") ) { return rotApple_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"iAmRotten") ) { return iAmRotten; }
		if (HX_FIELD_EQ(inName,"getMyXPos") ) { return getMyXPos_dyn(); }
		if (HX_FIELD_EQ(inName,"getMyYPos") ) { return getMyYPos_dyn(); }
		if (HX_FIELD_EQ(inName,"growApple") ) { return growApple_dyn(); }
		if (HX_FIELD_EQ(inName,"setAppleX") ) { return setAppleX_dyn(); }
		if (HX_FIELD_EQ(inName,"setAppleY") ) { return setAppleY_dyn(); }
		if (HX_FIELD_EQ(inName,"dropApple") ) { return dropApple_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"appleGroup") ) { return appleGroup; }
		if (HX_FIELD_EQ(inName,"iAmVisible") ) { return iAmVisible; }
		if (HX_FIELD_EQ(inName,"timeToDrop") ) { return timeToDrop; }
		if (HX_FIELD_EQ(inName,"myInitialY") ) { return myInitialY; }
		if (HX_FIELD_EQ(inName,"randomXPos") ) { return randomXPos; }
		if (HX_FIELD_EQ(inName,"randomYPos") ) { return randomYPos; }
		if (HX_FIELD_EQ(inName,"getRandomX") ) { return getRandomX_dyn(); }
		if (HX_FIELD_EQ(inName,"getRandomY") ) { return getRandomY_dyn(); }
		if (HX_FIELD_EQ(inName,"getIAmRipe") ) { return getIAmRipe_dyn(); }
		if (HX_FIELD_EQ(inName,"getIAmDead") ) { return getIAmDead_dyn(); }
		if (HX_FIELD_EQ(inName,"ripenApple") ) { return ripenApple_dyn(); }
		if (HX_FIELD_EQ(inName,"pauseRipen") ) { return pauseRipen_dyn(); }
		if (HX_FIELD_EQ(inName,"setRiseRun") ) { return setRiseRun_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"appleGrowth") ) { return appleGrowth; }
		if (HX_FIELD_EQ(inName,"flyToBasket") ) { return flyToBasket; }
		if (HX_FIELD_EQ(inName,"stillOnTree") ) { return stillOnTree; }
		if (HX_FIELD_EQ(inName,"getRotTimer") ) { return getRotTimer_dyn(); }
		if (HX_FIELD_EQ(inName,"getInBasket") ) { return getInBasket_dyn(); }
		if (HX_FIELD_EQ(inName,"setDropRate") ) { return setDropRate_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"appleDropped") ) { return appleDropped; }
		if (HX_FIELD_EQ(inName,"getIAmRotten") ) { return getIAmRotten_dyn(); }
		if (HX_FIELD_EQ(inName,"unpauseRipen") ) { return unpauseRipen_dyn(); }
		if (HX_FIELD_EQ(inName,"revealApples") ) { return revealApples_dyn(); }
		if (HX_FIELD_EQ(inName,"stopFreeFall") ) { return stopFreeFall_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"getMyInitialY") ) { return getMyInitialY_dyn(); }
		if (HX_FIELD_EQ(inName,"setMyInitialY") ) { return setMyInitialY_dyn(); }
		if (HX_FIELD_EQ(inName,"destroySounds") ) { return destroySounds_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"leavesFallOnce") ) { return leavesFallOnce; }
		if (HX_FIELD_EQ(inName,"getAppleSprite") ) { return getAppleSprite_dyn(); }
		if (HX_FIELD_EQ(inName,"getAppleGrowth") ) { return getAppleGrowth_dyn(); }
		if (HX_FIELD_EQ(inName,"setFlyToBasket") ) { return setFlyToBasket_dyn(); }
		if (HX_FIELD_EQ(inName,"setAppleGrowth") ) { return setAppleGrowth_dyn(); }
		if (HX_FIELD_EQ(inName,"incAppleGrowth") ) { return incAppleGrowth_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"myRipeCountDown") ) { return myRipeCountDown; }
		if (HX_FIELD_EQ(inName,"getAppleDropped") ) { return getAppleDropped_dyn(); }
		if (HX_FIELD_EQ(inName,"getAppleVisible") ) { return getAppleVisible_dyn(); }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"fallenAppleSound") ) { return fallenAppleSound; }
		if (HX_FIELD_EQ(inName,"resolveAnimation") ) { return resolveAnimation_dyn(); }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"pickedAppleLeaves") ) { return pickedAppleLeaves; }
		if (HX_FIELD_EQ(inName,"pickedLeavesSound") ) { return pickedLeavesSound; }
		if (HX_FIELD_EQ(inName,"pickedAppleGround") ) { return pickedAppleGround; }
		if (HX_FIELD_EQ(inName,"setLeavesFallOnce") ) { return setLeavesFallOnce_dyn(); }
		if (HX_FIELD_EQ(inName,"sendAppleToBasket") ) { return sendAppleToBasket_dyn(); }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"leavesFallAnimation") ) { return leavesFallAnimation_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Apple_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"run") ) { run=inValue.Cast< Float >(); return inValue; }
		break;
	case 4:
		if (HX_FIELD_EQ(inName,"rise") ) { rise=inValue.Cast< Float >(); return inValue; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"apple") ) { apple=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"basket") ) { basket=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"iAmRipe") ) { iAmRipe=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"iAmDead") ) { iAmDead=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"rotLock") ) { rotLock=inValue.Cast< bool >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"inBasket") ) { inBasket=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"rotTimer") ) { rotTimer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"dropRate") ) { dropRate=inValue.Cast< int >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"iAmRotten") ) { iAmRotten=inValue.Cast< bool >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"appleGroup") ) { appleGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"iAmVisible") ) { iAmVisible=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"timeToDrop") ) { timeToDrop=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"myInitialY") ) { myInitialY=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"randomXPos") ) { randomXPos=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"randomYPos") ) { randomYPos=inValue.Cast< int >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"appleGrowth") ) { appleGrowth=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"flyToBasket") ) { flyToBasket=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"stillOnTree") ) { stillOnTree=inValue.Cast< bool >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"appleDropped") ) { appleDropped=inValue.Cast< bool >(); return inValue; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"leavesFallOnce") ) { leavesFallOnce=inValue.Cast< bool >(); return inValue; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"myRipeCountDown") ) { myRipeCountDown=inValue.Cast< Float >(); return inValue; }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"fallenAppleSound") ) { fallenAppleSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"pickedAppleLeaves") ) { pickedAppleLeaves=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"pickedLeavesSound") ) { pickedLeavesSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		if (HX_FIELD_EQ(inName,"pickedAppleGround") ) { pickedAppleGround=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Apple_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("appleGroup"));
	outFields->push(HX_CSTRING("apple"));
	outFields->push(HX_CSTRING("basket"));
	outFields->push(HX_CSTRING("pickedAppleLeaves"));
	outFields->push(HX_CSTRING("pickedLeavesSound"));
	outFields->push(HX_CSTRING("pickedAppleGround"));
	outFields->push(HX_CSTRING("fallenAppleSound"));
	outFields->push(HX_CSTRING("appleGrowth"));
	outFields->push(HX_CSTRING("iAmRipe"));
	outFields->push(HX_CSTRING("iAmVisible"));
	outFields->push(HX_CSTRING("iAmDead"));
	outFields->push(HX_CSTRING("appleDropped"));
	outFields->push(HX_CSTRING("rotLock"));
	outFields->push(HX_CSTRING("iAmRotten"));
	outFields->push(HX_CSTRING("flyToBasket"));
	outFields->push(HX_CSTRING("inBasket"));
	outFields->push(HX_CSTRING("leavesFallOnce"));
	outFields->push(HX_CSTRING("stillOnTree"));
	outFields->push(HX_CSTRING("timeToDrop"));
	outFields->push(HX_CSTRING("rotTimer"));
	outFields->push(HX_CSTRING("myInitialY"));
	outFields->push(HX_CSTRING("randomXPos"));
	outFields->push(HX_CSTRING("dropRate"));
	outFields->push(HX_CSTRING("rise"));
	outFields->push(HX_CSTRING("run"));
	outFields->push(HX_CSTRING("randomYPos"));
	outFields->push(HX_CSTRING("myRipeCountDown"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(Apple_obj,appleGroup),HX_CSTRING("appleGroup")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Apple_obj,apple),HX_CSTRING("apple")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Apple_obj,basket),HX_CSTRING("basket")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Apple_obj,pickedAppleLeaves),HX_CSTRING("pickedAppleLeaves")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(Apple_obj,pickedLeavesSound),HX_CSTRING("pickedLeavesSound")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(Apple_obj,pickedAppleGround),HX_CSTRING("pickedAppleGround")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(Apple_obj,fallenAppleSound),HX_CSTRING("fallenAppleSound")},
	{hx::fsInt,(int)offsetof(Apple_obj,appleGrowth),HX_CSTRING("appleGrowth")},
	{hx::fsBool,(int)offsetof(Apple_obj,iAmRipe),HX_CSTRING("iAmRipe")},
	{hx::fsBool,(int)offsetof(Apple_obj,iAmVisible),HX_CSTRING("iAmVisible")},
	{hx::fsBool,(int)offsetof(Apple_obj,iAmDead),HX_CSTRING("iAmDead")},
	{hx::fsBool,(int)offsetof(Apple_obj,appleDropped),HX_CSTRING("appleDropped")},
	{hx::fsBool,(int)offsetof(Apple_obj,rotLock),HX_CSTRING("rotLock")},
	{hx::fsBool,(int)offsetof(Apple_obj,iAmRotten),HX_CSTRING("iAmRotten")},
	{hx::fsBool,(int)offsetof(Apple_obj,flyToBasket),HX_CSTRING("flyToBasket")},
	{hx::fsBool,(int)offsetof(Apple_obj,inBasket),HX_CSTRING("inBasket")},
	{hx::fsBool,(int)offsetof(Apple_obj,leavesFallOnce),HX_CSTRING("leavesFallOnce")},
	{hx::fsBool,(int)offsetof(Apple_obj,stillOnTree),HX_CSTRING("stillOnTree")},
	{hx::fsFloat,(int)offsetof(Apple_obj,timeToDrop),HX_CSTRING("timeToDrop")},
	{hx::fsFloat,(int)offsetof(Apple_obj,rotTimer),HX_CSTRING("rotTimer")},
	{hx::fsFloat,(int)offsetof(Apple_obj,myInitialY),HX_CSTRING("myInitialY")},
	{hx::fsInt,(int)offsetof(Apple_obj,randomXPos),HX_CSTRING("randomXPos")},
	{hx::fsInt,(int)offsetof(Apple_obj,dropRate),HX_CSTRING("dropRate")},
	{hx::fsFloat,(int)offsetof(Apple_obj,rise),HX_CSTRING("rise")},
	{hx::fsFloat,(int)offsetof(Apple_obj,run),HX_CSTRING("run")},
	{hx::fsInt,(int)offsetof(Apple_obj,randomYPos),HX_CSTRING("randomYPos")},
	{hx::fsFloat,(int)offsetof(Apple_obj,myRipeCountDown),HX_CSTRING("myRipeCountDown")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("appleGroup"),
	HX_CSTRING("apple"),
	HX_CSTRING("basket"),
	HX_CSTRING("pickedAppleLeaves"),
	HX_CSTRING("pickedLeavesSound"),
	HX_CSTRING("pickedAppleGround"),
	HX_CSTRING("fallenAppleSound"),
	HX_CSTRING("appleGrowth"),
	HX_CSTRING("iAmRipe"),
	HX_CSTRING("iAmVisible"),
	HX_CSTRING("iAmDead"),
	HX_CSTRING("appleDropped"),
	HX_CSTRING("rotLock"),
	HX_CSTRING("iAmRotten"),
	HX_CSTRING("flyToBasket"),
	HX_CSTRING("inBasket"),
	HX_CSTRING("leavesFallOnce"),
	HX_CSTRING("stillOnTree"),
	HX_CSTRING("timeToDrop"),
	HX_CSTRING("rotTimer"),
	HX_CSTRING("myInitialY"),
	HX_CSTRING("randomXPos"),
	HX_CSTRING("dropRate"),
	HX_CSTRING("rise"),
	HX_CSTRING("run"),
	HX_CSTRING("randomYPos"),
	HX_CSTRING("myRipeCountDown"),
	HX_CSTRING("getRandomX"),
	HX_CSTRING("getRandomY"),
	HX_CSTRING("getMyXPos"),
	HX_CSTRING("getAppleDropped"),
	HX_CSTRING("getMyYPos"),
	HX_CSTRING("getAppleSprite"),
	HX_CSTRING("getRotTimer"),
	HX_CSTRING("getAppleVisible"),
	HX_CSTRING("getIAmRotten"),
	HX_CSTRING("getIAmRipe"),
	HX_CSTRING("getAppleGrowth"),
	HX_CSTRING("getMyInitialY"),
	HX_CSTRING("getIAmDead"),
	HX_CSTRING("getInBasket"),
	HX_CSTRING("setMyInitialY"),
	HX_CSTRING("setDropRate"),
	HX_CSTRING("setFlyToBasket"),
	HX_CSTRING("ripenApple"),
	HX_CSTRING("pauseRipen"),
	HX_CSTRING("unpauseRipen"),
	HX_CSTRING("growApple"),
	HX_CSTRING("revealApples"),
	HX_CSTRING("setAppleGrowth"),
	HX_CSTRING("setAppleX"),
	HX_CSTRING("setAppleY"),
	HX_CSTRING("setLeavesFallOnce"),
	HX_CSTRING("incAppleGrowth"),
	HX_CSTRING("stopFreeFall"),
	HX_CSTRING("dropApple"),
	HX_CSTRING("rotApple"),
	HX_CSTRING("setRiseRun"),
	HX_CSTRING("sendAppleToBasket"),
	HX_CSTRING("leavesFallAnimation"),
	HX_CSTRING("resolveAnimation"),
	HX_CSTRING("destroySounds"),
	HX_CSTRING("update"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Apple_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Apple_obj::__mClass,"__mClass");
};

#endif

Class Apple_obj::__mClass;

void Apple_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("Apple"), hx::TCanCast< Apple_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void Apple_obj::__boot()
{
}

