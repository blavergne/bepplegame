#include <hxcpp.h>

#ifndef INCLUDED_OptionsScreen
#include <OptionsScreen.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_plugin_FlxPlugin
#include <flixel/plugin/FlxPlugin.h>
#endif
#ifndef INCLUDED_flixel_plugin_MouseEventManager
#include <flixel/plugin/MouseEventManager.h>
#endif
#ifndef INCLUDED_flixel_ui_FlxButton
#include <flixel/ui/FlxButton.h>
#endif
#ifndef INCLUDED_flixel_ui_FlxTypedButton
#include <flixel/ui/FlxTypedButton.h>
#endif
#ifndef INCLUDED_openfl__v2_system_System
#include <openfl/_v2/system/System.h>
#endif

Void OptionsScreen_obj::__construct()
{
HX_STACK_FRAME("OptionsScreen","new",0xeed71b3c,"OptionsScreen.new","OptionsScreen.hx",16,0x95291fb4)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(23)
	this->gameRestart = false;
	HX_STACK_LINE(28)
	super::__construct(null());
	HX_STACK_LINE(29)
	::flixel::group::FlxGroup _g = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(29)
	this->optionsScreenGroup = _g;
	HX_STACK_LINE(30)
	::flixel::FlxSprite _g1 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
	HX_STACK_LINE(30)
	this->optionsScreenSprite = _g1;
	HX_STACK_LINE(31)
	::flixel::ui::FlxButton _g2 = ::flixel::ui::FlxButton_obj::__new((int)0,(int)0,HX_CSTRING("Exit Game"),null());		HX_STACK_VAR(_g2,"_g2");
	HX_STACK_LINE(31)
	this->exitButton = _g2;
	HX_STACK_LINE(32)
	::flixel::ui::FlxButton _g3 = ::flixel::ui::FlxButton_obj::__new((int)0,(int)0,HX_CSTRING("Reset"),null());		HX_STACK_VAR(_g3,"_g3");
	HX_STACK_LINE(32)
	this->resetButton = _g3;
	HX_STACK_LINE(33)
	this->optionsScreenSprite->loadGraphic(HX_CSTRING("assets/images/optionsScreen.png"),false,null(),null(),null(),null());
	HX_STACK_LINE(34)
	this->optionsScreenSprite->set_x(220.);
	HX_STACK_LINE(35)
	this->optionsScreenSprite->set_y(140.);
	HX_STACK_LINE(36)
	this->exitButton->set_x((this->optionsScreenSprite->x + (int)63));
	HX_STACK_LINE(37)
	this->exitButton->set_y((this->optionsScreenSprite->y + (int)60));
	HX_STACK_LINE(38)
	this->resetButton->set_x((this->optionsScreenSprite->x + (int)63));
	HX_STACK_LINE(39)
	this->resetButton->set_y((this->optionsScreenSprite->y + (int)20));
	HX_STACK_LINE(40)
	this->optionsScreenSprite->set_alpha((int)0);
	HX_STACK_LINE(41)
	::flixel::plugin::MouseEventManager_obj::add(this->exitButton,this->exitGame_dyn(),null(),null(),null(),null(),null(),null());
	HX_STACK_LINE(42)
	::flixel::plugin::MouseEventManager_obj::add(this->resetButton,this->resetGame_dyn(),null(),null(),null(),null(),null(),null());
	HX_STACK_LINE(43)
	this->optionsScreenGroup->add(this->optionsScreenSprite);
	HX_STACK_LINE(44)
	this->add(this->optionsScreenGroup);
}
;
	return null();
}

//OptionsScreen_obj::~OptionsScreen_obj() { }

Dynamic OptionsScreen_obj::__CreateEmpty() { return  new OptionsScreen_obj; }
hx::ObjectPtr< OptionsScreen_obj > OptionsScreen_obj::__new()
{  hx::ObjectPtr< OptionsScreen_obj > result = new OptionsScreen_obj();
	result->__construct();
	return result;}

Dynamic OptionsScreen_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< OptionsScreen_obj > result = new OptionsScreen_obj();
	result->__construct();
	return result;}

bool OptionsScreen_obj::getGameRestart( ){
	HX_STACK_FRAME("OptionsScreen","getGameRestart",0x9789bbab,"OptionsScreen.getGameRestart","OptionsScreen.hx",49,0x95291fb4)
	HX_STACK_THIS(this)
	HX_STACK_LINE(49)
	return this->gameRestart;
}


HX_DEFINE_DYNAMIC_FUNC0(OptionsScreen_obj,getGameRestart,return )

Void OptionsScreen_obj::setGameRestart( bool val){
{
		HX_STACK_FRAME("OptionsScreen","setGameRestart",0xb7a9a41f,"OptionsScreen.setGameRestart","OptionsScreen.hx",53,0x95291fb4)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(53)
		this->gameRestart = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(OptionsScreen_obj,setGameRestart,(void))

Void OptionsScreen_obj::menuVisible( int val){
{
		HX_STACK_FRAME("OptionsScreen","menuVisible",0xada7636f,"OptionsScreen.menuVisible","OptionsScreen.hx",57,0x95291fb4)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(57)
		if (((val == (int)0))){
			HX_STACK_LINE(58)
			this->optionsScreenSprite->set_alpha((int)0);
			HX_STACK_LINE(59)
			this->optionsScreenGroup->remove(this->exitButton,null());
			HX_STACK_LINE(60)
			this->optionsScreenGroup->remove(this->resetButton,null());
		}
		else{
			HX_STACK_LINE(63)
			this->optionsScreenSprite->set_alpha((int)1);
			HX_STACK_LINE(64)
			this->optionsScreenGroup->add(this->exitButton);
			HX_STACK_LINE(65)
			this->optionsScreenGroup->add(this->resetButton);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(OptionsScreen_obj,menuVisible,(void))

Void OptionsScreen_obj::exitGame( ::flixel::ui::FlxButton button){
{
		HX_STACK_FRAME("OptionsScreen","exitGame",0xf9f6d5d4,"OptionsScreen.exitGame","OptionsScreen.hx",69,0x95291fb4)
		HX_STACK_THIS(this)
		HX_STACK_ARG(button,"button")
		HX_STACK_LINE(70)
		::openfl::_v2::system::System_obj::exit((int)0);
		HX_STACK_LINE(71)
		this->destroy();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(OptionsScreen_obj,exitGame,(void))

Void OptionsScreen_obj::resetGame( ::flixel::ui::FlxButton button){
{
		HX_STACK_FRAME("OptionsScreen","resetGame",0xadd3d53d,"OptionsScreen.resetGame","OptionsScreen.hx",75,0x95291fb4)
		HX_STACK_THIS(this)
		HX_STACK_ARG(button,"button")
		HX_STACK_LINE(75)
		this->gameRestart = true;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(OptionsScreen_obj,resetGame,(void))

Void OptionsScreen_obj::destroy( ){
{
		HX_STACK_FRAME("OptionsScreen","destroy",0x639e15d6,"OptionsScreen.destroy","OptionsScreen.hx",78,0x95291fb4)
		HX_STACK_THIS(this)
		HX_STACK_LINE(79)
		this->destroy_dyn();
		HX_STACK_LINE(80)
		this->super::destroy();
	}
return null();
}



OptionsScreen_obj::OptionsScreen_obj()
{
}

void OptionsScreen_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(OptionsScreen);
	HX_MARK_MEMBER_NAME(optionsScreenGroup,"optionsScreenGroup");
	HX_MARK_MEMBER_NAME(optionsScreenSprite,"optionsScreenSprite");
	HX_MARK_MEMBER_NAME(exitButton,"exitButton");
	HX_MARK_MEMBER_NAME(resetButton,"resetButton");
	HX_MARK_MEMBER_NAME(gameRestart,"gameRestart");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void OptionsScreen_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(optionsScreenGroup,"optionsScreenGroup");
	HX_VISIT_MEMBER_NAME(optionsScreenSprite,"optionsScreenSprite");
	HX_VISIT_MEMBER_NAME(exitButton,"exitButton");
	HX_VISIT_MEMBER_NAME(resetButton,"resetButton");
	HX_VISIT_MEMBER_NAME(gameRestart,"gameRestart");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic OptionsScreen_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 7:
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"exitGame") ) { return exitGame_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"resetGame") ) { return resetGame_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"exitButton") ) { return exitButton; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"resetButton") ) { return resetButton; }
		if (HX_FIELD_EQ(inName,"gameRestart") ) { return gameRestart; }
		if (HX_FIELD_EQ(inName,"menuVisible") ) { return menuVisible_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"getGameRestart") ) { return getGameRestart_dyn(); }
		if (HX_FIELD_EQ(inName,"setGameRestart") ) { return setGameRestart_dyn(); }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"optionsScreenGroup") ) { return optionsScreenGroup; }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"optionsScreenSprite") ) { return optionsScreenSprite; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic OptionsScreen_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 10:
		if (HX_FIELD_EQ(inName,"exitButton") ) { exitButton=inValue.Cast< ::flixel::ui::FlxButton >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"resetButton") ) { resetButton=inValue.Cast< ::flixel::ui::FlxButton >(); return inValue; }
		if (HX_FIELD_EQ(inName,"gameRestart") ) { gameRestart=inValue.Cast< bool >(); return inValue; }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"optionsScreenGroup") ) { optionsScreenGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"optionsScreenSprite") ) { optionsScreenSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void OptionsScreen_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("optionsScreenGroup"));
	outFields->push(HX_CSTRING("optionsScreenSprite"));
	outFields->push(HX_CSTRING("exitButton"));
	outFields->push(HX_CSTRING("resetButton"));
	outFields->push(HX_CSTRING("gameRestart"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(OptionsScreen_obj,optionsScreenGroup),HX_CSTRING("optionsScreenGroup")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(OptionsScreen_obj,optionsScreenSprite),HX_CSTRING("optionsScreenSprite")},
	{hx::fsObject /*::flixel::ui::FlxButton*/ ,(int)offsetof(OptionsScreen_obj,exitButton),HX_CSTRING("exitButton")},
	{hx::fsObject /*::flixel::ui::FlxButton*/ ,(int)offsetof(OptionsScreen_obj,resetButton),HX_CSTRING("resetButton")},
	{hx::fsBool,(int)offsetof(OptionsScreen_obj,gameRestart),HX_CSTRING("gameRestart")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("optionsScreenGroup"),
	HX_CSTRING("optionsScreenSprite"),
	HX_CSTRING("exitButton"),
	HX_CSTRING("resetButton"),
	HX_CSTRING("gameRestart"),
	HX_CSTRING("getGameRestart"),
	HX_CSTRING("setGameRestart"),
	HX_CSTRING("menuVisible"),
	HX_CSTRING("exitGame"),
	HX_CSTRING("resetGame"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(OptionsScreen_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(OptionsScreen_obj::__mClass,"__mClass");
};

#endif

Class OptionsScreen_obj::__mClass;

void OptionsScreen_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("OptionsScreen"), hx::TCanCast< OptionsScreen_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void OptionsScreen_obj::__boot()
{
}

