#include <hxcpp.h>

#ifndef INCLUDED_Tile
#include <Tile.h>
#endif
#ifndef INCLUDED_TileBoard
#include <TileBoard.h>
#endif
#ifndef INCLUDED_TileBoarder
#include <TileBoarder.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif

Void TileBoard_obj::__construct(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y)
{
HX_STACK_FRAME("TileBoard","new",0x4e1f016a,"TileBoard.new","TileBoard.hx",9,0x3c0b9946)
HX_STACK_THIS(this)
HX_STACK_ARG(__o_X,"X")
HX_STACK_ARG(__o_Y,"Y")
Float X = __o_X.Default(0);
Float Y = __o_Y.Default(0);
{
	HX_STACK_LINE(15)
	this->tileNumber = (int)0;
	HX_STACK_LINE(14)
	this->boardSize = (int)3;
	HX_STACK_LINE(19)
	super::__construct(X,Y,null());
	HX_STACK_LINE(21)
	this->allocateMem();
}
;
	return null();
}

//TileBoard_obj::~TileBoard_obj() { }

Dynamic TileBoard_obj::__CreateEmpty() { return  new TileBoard_obj; }
hx::ObjectPtr< TileBoard_obj > TileBoard_obj::__new(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y)
{  hx::ObjectPtr< TileBoard_obj > result = new TileBoard_obj();
	result->__construct(__o_X,__o_Y);
	return result;}

Dynamic TileBoard_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< TileBoard_obj > result = new TileBoard_obj();
	result->__construct(inArgs[0],inArgs[1]);
	return result;}

Void TileBoard_obj::allocateMem( ){
{
		HX_STACK_FRAME("TileBoard","allocateMem",0xac9ed262,"TileBoard.allocateMem","TileBoard.hx",25,0x3c0b9946)
		HX_STACK_THIS(this)
		HX_STACK_LINE(27)
		Array< ::Dynamic > _g = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(27)
		this->boardArray = _g;
		HX_STACK_LINE(28)
		Array< ::Dynamic > _g1 = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(28)
		this->boardBoarderArray = _g1;
		HX_STACK_LINE(30)
		{
			HX_STACK_LINE(30)
			int _g11 = (int)0;		HX_STACK_VAR(_g11,"_g11");
			HX_STACK_LINE(30)
			int _g2 = this->boardSize;		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(30)
			while((true)){
				HX_STACK_LINE(30)
				if ((!(((_g11 < _g2))))){
					HX_STACK_LINE(30)
					break;
				}
				HX_STACK_LINE(30)
				int i = (_g11)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(32)
				this->boardArray[i] = Array_obj< ::Dynamic >::__new();
				HX_STACK_LINE(33)
				this->boardBoarderArray[i] = Array_obj< ::Dynamic >::__new();
				HX_STACK_LINE(35)
				{
					HX_STACK_LINE(35)
					int _g3 = (int)0;		HX_STACK_VAR(_g3,"_g3");
					HX_STACK_LINE(35)
					int _g21 = this->boardSize;		HX_STACK_VAR(_g21,"_g21");
					HX_STACK_LINE(35)
					while((true)){
						HX_STACK_LINE(35)
						if ((!(((_g3 < _g21))))){
							HX_STACK_LINE(35)
							break;
						}
						HX_STACK_LINE(35)
						int j = (_g3)++;		HX_STACK_VAR(j,"j");
						HX_STACK_LINE(37)
						this->boardArray->__get(i).StaticCast< Array< ::Dynamic > >()[j] = ::Tile_obj::__new(null(),null());
						HX_STACK_LINE(38)
						this->boardBoarderArray->__get(i).StaticCast< Array< ::Dynamic > >()[j] = ::TileBoarder_obj::__new(null(),null());
					}
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(TileBoard_obj,allocateMem,(void))

Array< ::Dynamic > TileBoard_obj::getBoardArray( ){
	HX_STACK_FRAME("TileBoard","getBoardArray",0x897c7ad3,"TileBoard.getBoardArray","TileBoard.hx",47,0x3c0b9946)
	HX_STACK_THIS(this)
	HX_STACK_LINE(47)
	return this->boardArray;
}


HX_DEFINE_DYNAMIC_FUNC0(TileBoard_obj,getBoardArray,return )

Array< ::Dynamic > TileBoard_obj::getBoardBoarderArray( ){
	HX_STACK_FRAME("TileBoard","getBoardBoarderArray",0x0d75820c,"TileBoard.getBoardBoarderArray","TileBoard.hx",53,0x3c0b9946)
	HX_STACK_THIS(this)
	HX_STACK_LINE(53)
	return this->boardBoarderArray;
}


HX_DEFINE_DYNAMIC_FUNC0(TileBoard_obj,getBoardBoarderArray,return )


TileBoard_obj::TileBoard_obj()
{
}

void TileBoard_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(TileBoard);
	HX_MARK_MEMBER_NAME(boardArray,"boardArray");
	HX_MARK_MEMBER_NAME(boardBoarderArray,"boardBoarderArray");
	HX_MARK_MEMBER_NAME(boardSize,"boardSize");
	HX_MARK_MEMBER_NAME(tileNumber,"tileNumber");
	::flixel::FlxSprite_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void TileBoard_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(boardArray,"boardArray");
	HX_VISIT_MEMBER_NAME(boardBoarderArray,"boardBoarderArray");
	HX_VISIT_MEMBER_NAME(boardSize,"boardSize");
	HX_VISIT_MEMBER_NAME(tileNumber,"tileNumber");
	::flixel::FlxSprite_obj::__Visit(HX_VISIT_ARG);
}

Dynamic TileBoard_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 9:
		if (HX_FIELD_EQ(inName,"boardSize") ) { return boardSize; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"boardArray") ) { return boardArray; }
		if (HX_FIELD_EQ(inName,"tileNumber") ) { return tileNumber; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"allocateMem") ) { return allocateMem_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"getBoardArray") ) { return getBoardArray_dyn(); }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"boardBoarderArray") ) { return boardBoarderArray; }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"getBoardBoarderArray") ) { return getBoardBoarderArray_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic TileBoard_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 9:
		if (HX_FIELD_EQ(inName,"boardSize") ) { boardSize=inValue.Cast< int >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"boardArray") ) { boardArray=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tileNumber") ) { tileNumber=inValue.Cast< int >(); return inValue; }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"boardBoarderArray") ) { boardBoarderArray=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void TileBoard_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("boardArray"));
	outFields->push(HX_CSTRING("boardBoarderArray"));
	outFields->push(HX_CSTRING("boardSize"));
	outFields->push(HX_CSTRING("tileNumber"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(TileBoard_obj,boardArray),HX_CSTRING("boardArray")},
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(TileBoard_obj,boardBoarderArray),HX_CSTRING("boardBoarderArray")},
	{hx::fsInt,(int)offsetof(TileBoard_obj,boardSize),HX_CSTRING("boardSize")},
	{hx::fsInt,(int)offsetof(TileBoard_obj,tileNumber),HX_CSTRING("tileNumber")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("boardArray"),
	HX_CSTRING("boardBoarderArray"),
	HX_CSTRING("boardSize"),
	HX_CSTRING("tileNumber"),
	HX_CSTRING("allocateMem"),
	HX_CSTRING("getBoardArray"),
	HX_CSTRING("getBoardBoarderArray"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(TileBoard_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(TileBoard_obj::__mClass,"__mClass");
};

#endif

Class TileBoard_obj::__mClass;

void TileBoard_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("TileBoard"), hx::TCanCast< TileBoard_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void TileBoard_obj::__boot()
{
}

