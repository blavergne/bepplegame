#include <hxcpp.h>

#ifndef INCLUDED_TileBoarder
#include <TileBoarder.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif

Void TileBoarder_obj::__construct(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y)
{
HX_STACK_FRAME("TileBoarder","new",0x7bcba257,"TileBoarder.new","TileBoarder.hx",11,0xba8734f9)
HX_STACK_THIS(this)
HX_STACK_ARG(__o_X,"X")
HX_STACK_ARG(__o_Y,"Y")
Float X = __o_X.Default(0);
Float Y = __o_Y.Default(0);
{
	HX_STACK_LINE(14)
	this->boarderStatus = HX_CSTRING("off");
	HX_STACK_LINE(18)
	super::__construct(X,Y,null());
	HX_STACK_LINE(19)
	this->loadGraphic(HX_CSTRING("assets/images/gameTileBoarder.png"),false,null(),null(),null(),null());
	HX_STACK_LINE(20)
	this->set_x((int)223);
	HX_STACK_LINE(21)
	this->set_y((int)120);
	HX_STACK_LINE(22)
	this->set_alpha(0.0);
}
;
	return null();
}

//TileBoarder_obj::~TileBoarder_obj() { }

Dynamic TileBoarder_obj::__CreateEmpty() { return  new TileBoarder_obj; }
hx::ObjectPtr< TileBoarder_obj > TileBoarder_obj::__new(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y)
{  hx::ObjectPtr< TileBoarder_obj > result = new TileBoarder_obj();
	result->__construct(__o_X,__o_Y);
	return result;}

Dynamic TileBoarder_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< TileBoarder_obj > result = new TileBoarder_obj();
	result->__construct(inArgs[0],inArgs[1]);
	return result;}

Void TileBoarder_obj::boarderSwitch( ::String val){
{
		HX_STACK_FRAME("TileBoarder","boarderSwitch",0x2351cf9e,"TileBoarder.boarderSwitch","TileBoarder.hx",28,0xba8734f9)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(28)
		::String _switch_1 = (val);
		if (  ( _switch_1==HX_CSTRING("on"))){
			HX_STACK_LINE(31)
			this->set_alpha(1.0);
			HX_STACK_LINE(32)
			this->boarderStatus = HX_CSTRING("on");
		}
		else if (  ( _switch_1==HX_CSTRING("off"))){
			HX_STACK_LINE(34)
			this->set_alpha(0.0);
			HX_STACK_LINE(35)
			this->boarderStatus = HX_CSTRING("off");
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(TileBoarder_obj,boarderSwitch,(void))

::String TileBoarder_obj::getBoarderStatus( ){
	HX_STACK_FRAME("TileBoarder","getBoarderStatus",0x88098958,"TileBoarder.getBoarderStatus","TileBoarder.hx",43,0xba8734f9)
	HX_STACK_THIS(this)
	HX_STACK_LINE(43)
	return this->boarderStatus;
}


HX_DEFINE_DYNAMIC_FUNC0(TileBoarder_obj,getBoarderStatus,return )

Void TileBoarder_obj::draw( ){
{
		HX_STACK_FRAME("TileBoarder","draw",0xcfd0122d,"TileBoarder.draw","TileBoarder.hx",49,0xba8734f9)
		HX_STACK_THIS(this)
		HX_STACK_LINE(49)
		this->super::draw();
	}
return null();
}


Void TileBoarder_obj::update( ){
{
		HX_STACK_FRAME("TileBoarder","update",0x18ca0872,"TileBoarder.update","TileBoarder.hx",55,0xba8734f9)
		HX_STACK_THIS(this)
		HX_STACK_LINE(55)
		this->super::update();
	}
return null();
}



TileBoarder_obj::TileBoarder_obj()
{
}

void TileBoarder_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(TileBoarder);
	HX_MARK_MEMBER_NAME(boarderStatus,"boarderStatus");
	::flixel::FlxSprite_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void TileBoarder_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(boarderStatus,"boarderStatus");
	::flixel::FlxSprite_obj::__Visit(HX_VISIT_ARG);
}

Dynamic TileBoarder_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"draw") ) { return draw_dyn(); }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"boarderStatus") ) { return boarderStatus; }
		if (HX_FIELD_EQ(inName,"boarderSwitch") ) { return boarderSwitch_dyn(); }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"getBoarderStatus") ) { return getBoarderStatus_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic TileBoarder_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 13:
		if (HX_FIELD_EQ(inName,"boarderStatus") ) { boarderStatus=inValue.Cast< ::String >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void TileBoarder_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("boarderStatus"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsString,(int)offsetof(TileBoarder_obj,boarderStatus),HX_CSTRING("boarderStatus")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("boarderStatus"),
	HX_CSTRING("boarderSwitch"),
	HX_CSTRING("getBoarderStatus"),
	HX_CSTRING("draw"),
	HX_CSTRING("update"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(TileBoarder_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(TileBoarder_obj::__mClass,"__mClass");
};

#endif

Class TileBoarder_obj::__mClass;

void TileBoarder_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("TileBoarder"), hx::TCanCast< TileBoarder_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void TileBoarder_obj::__boot()
{
}

