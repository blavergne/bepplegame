#include <hxcpp.h>

#ifndef INCLUDED_Std
#include <Std.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxCamera
#include <flixel/FlxCamera.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxGame
#include <flixel/FlxGame.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_FlxState
#include <flixel/FlxState.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_input_mouse_FlxMouse
#include <flixel/input/mouse/FlxMouse.h>
#endif
#ifndef INCLUDED_flixel_input_mouse_FlxMouseButton
#include <flixel/input/mouse/FlxMouseButton.h>
#endif
#ifndef INCLUDED_flixel_input_touch_FlxTouch
#include <flixel/input/touch/FlxTouch.h>
#endif
#ifndef INCLUDED_flixel_input_touch_FlxTouchManager
#include <flixel/input/touch/FlxTouchManager.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxInput
#include <flixel/interfaces/IFlxInput.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxPooled
#include <flixel/interfaces/IFlxPooled.h>
#endif
#ifndef INCLUDED_flixel_plugin_FlxPlugin
#include <flixel/plugin/FlxPlugin.h>
#endif
#ifndef INCLUDED_flixel_plugin_MouseEventManager
#include <flixel/plugin/MouseEventManager.h>
#endif
#ifndef INCLUDED_flixel_plugin__MouseEventManager_ObjectMouseData
#include <flixel/plugin/_MouseEventManager/ObjectMouseData.h>
#endif
#ifndef INCLUDED_flixel_system_frontEnds_PluginFrontEnd
#include <flixel/system/frontEnds/PluginFrontEnd.h>
#endif
#ifndef INCLUDED_flixel_util_FlxDestroyUtil
#include <flixel/util/FlxDestroyUtil.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPoint
#include <flixel/util/FlxPoint.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPool_flixel_util_FlxPoint
#include <flixel/util/FlxPool_flixel_util_FlxPoint.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_openfl__v2_display_DisplayObject
#include <openfl/_v2/display/DisplayObject.h>
#endif
#ifndef INCLUDED_openfl__v2_display_DisplayObjectContainer
#include <openfl/_v2/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_openfl__v2_display_IBitmapDrawable
#include <openfl/_v2/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_openfl__v2_display_InteractiveObject
#include <openfl/_v2/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_openfl__v2_display_Sprite
#include <openfl/_v2/display/Sprite.h>
#endif
#ifndef INCLUDED_openfl__v2_events_EventDispatcher
#include <openfl/_v2/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__v2_events_IEventDispatcher
#include <openfl/_v2/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_openfl_errors_Error
#include <openfl/errors/Error.h>
#endif
namespace flixel{
namespace plugin{

Void MouseEventManager_obj::__construct()
{
HX_STACK_FRAME("flixel.plugin.MouseEventManager","new",0x1e64e761,"flixel.plugin.MouseEventManager.new","flixel/plugin/MouseEventManager.hx",281,0x4767214f)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(282)
	super::__construct();
	HX_STACK_LINE(284)
	::flixel::util::FlxPoint _g;		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(284)
	{
		HX_STACK_LINE(284)
		Float X = (int)0;		HX_STACK_VAR(X,"X");
		HX_STACK_LINE(284)
		Float Y = (int)0;		HX_STACK_VAR(Y,"Y");
		HX_STACK_LINE(284)
		::flixel::util::FlxPoint point = ::flixel::util::FlxPoint_obj::_pool->get()->set(X,Y);		HX_STACK_VAR(point,"point");
		HX_STACK_LINE(284)
		point->_inPool = false;
		HX_STACK_LINE(284)
		_g = point;
	}
	HX_STACK_LINE(284)
	::flixel::plugin::MouseEventManager_obj::_point = _g;
	HX_STACK_LINE(286)
	if (((::flixel::plugin::MouseEventManager_obj::_registeredObjects != null()))){
		HX_STACK_LINE(288)
		this->clearRegistry();
	}
	HX_STACK_LINE(291)
	Array< ::Dynamic > _g1 = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(_g1,"_g1");
	HX_STACK_LINE(291)
	::flixel::plugin::MouseEventManager_obj::_registeredObjects = _g1;
	HX_STACK_LINE(292)
	Array< ::Dynamic > _g2 = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(_g2,"_g2");
	HX_STACK_LINE(292)
	::flixel::plugin::MouseEventManager_obj::_mouseOverObjects = _g2;
}
;
	return null();
}

//MouseEventManager_obj::~MouseEventManager_obj() { }

Dynamic MouseEventManager_obj::__CreateEmpty() { return  new MouseEventManager_obj; }
hx::ObjectPtr< MouseEventManager_obj > MouseEventManager_obj::__new()
{  hx::ObjectPtr< MouseEventManager_obj > result = new MouseEventManager_obj();
	result->__construct();
	return result;}

Dynamic MouseEventManager_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< MouseEventManager_obj > result = new MouseEventManager_obj();
	result->__construct();
	return result;}

Void MouseEventManager_obj::destroy( ){
{
		HX_STACK_FRAME("flixel.plugin.MouseEventManager","destroy",0xe9d5587b,"flixel.plugin.MouseEventManager.destroy","flixel/plugin/MouseEventManager.hx",296,0x4767214f)
		HX_STACK_THIS(this)
		HX_STACK_LINE(297)
		this->clearRegistry();
		HX_STACK_LINE(298)
		::flixel::util::FlxPoint _g = ::flixel::util::FlxDestroyUtil_obj::put(::flixel::plugin::MouseEventManager_obj::_point);		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(298)
		::flixel::plugin::MouseEventManager_obj::_point = _g;
		HX_STACK_LINE(299)
		this->super::destroy();
	}
return null();
}


Void MouseEventManager_obj::update( ){
{
		HX_STACK_FRAME("flixel.plugin.MouseEventManager","update",0xfe2605a8,"flixel.plugin.MouseEventManager.update","flixel/plugin/MouseEventManager.hx",303,0x4767214f)
		HX_STACK_THIS(this)
		HX_STACK_LINE(304)
		this->super::update();
		HX_STACK_LINE(306)
		Array< ::Dynamic > currentOverObjects = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(currentOverObjects,"currentOverObjects");
		HX_STACK_LINE(308)
		{
			HX_STACK_LINE(308)
			int _g = (int)0;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(308)
			Array< ::Dynamic > _g1 = ::flixel::plugin::MouseEventManager_obj::_registeredObjects;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(308)
			while((true)){
				HX_STACK_LINE(308)
				if ((!(((_g < _g1->length))))){
					HX_STACK_LINE(308)
					break;
				}
				HX_STACK_LINE(308)
				::flixel::plugin::_MouseEventManager::ObjectMouseData reg = _g1->__get(_g).StaticCast< ::flixel::plugin::_MouseEventManager::ObjectMouseData >();		HX_STACK_VAR(reg,"reg");
				HX_STACK_LINE(308)
				++(_g);
				HX_STACK_LINE(311)
				if (((reg->object->acceleration == null()))){
					HX_STACK_LINE(313)
					::flixel::plugin::MouseEventManager_obj::remove(reg->object);
					HX_STACK_LINE(314)
					continue;
				}
				HX_STACK_LINE(317)
				if (((bool((bool((bool(!(reg->object->alive)) || bool(!(reg->object->exists)))) || bool(!(reg->object->visible)))) || bool(!(reg->mouseEnabled))))){
					HX_STACK_LINE(319)
					continue;
				}
				HX_STACK_LINE(322)
				if ((this->checkOverlap(reg))){
					HX_STACK_LINE(324)
					currentOverObjects->push(reg);
					HX_STACK_LINE(326)
					if ((!(reg->mouseChildren))){
						HX_STACK_LINE(328)
						break;
					}
				}
			}
		}
		HX_STACK_LINE(334)
		{
			HX_STACK_LINE(334)
			int _g = (int)0;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(334)
			while((true)){
				HX_STACK_LINE(334)
				if ((!(((_g < currentOverObjects->length))))){
					HX_STACK_LINE(334)
					break;
				}
				HX_STACK_LINE(334)
				::flixel::plugin::_MouseEventManager::ObjectMouseData current = currentOverObjects->__get(_g).StaticCast< ::flixel::plugin::_MouseEventManager::ObjectMouseData >();		HX_STACK_VAR(current,"current");
				HX_STACK_LINE(334)
				++(_g);
				HX_STACK_LINE(336)
				if (((current->onMouseOver != null()))){
					struct _Function_4_1{
						inline static bool Block( ::flixel::plugin::_MouseEventManager::ObjectMouseData &current){
							HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","flixel/plugin/MouseEventManager.hx",338,0x4767214f)
							{
								HX_STACK_LINE(338)
								::flixel::plugin::_MouseEventManager::ObjectMouseData _g1 = ::flixel::plugin::MouseEventManager_obj::getRegister(current->object,::flixel::plugin::MouseEventManager_obj::_mouseOverObjects);		HX_STACK_VAR(_g1,"_g1");
								HX_STACK_LINE(338)
								return (_g1 == null());
							}
							return null();
						}
					};
					HX_STACK_LINE(338)
					if (((  (((bool(current->object->exists) && bool(current->object->visible)))) ? bool(_Function_4_1::Block(current)) : bool(false) ))){
						HX_STACK_LINE(340)
						current->onMouseOver(current->object);
					}
				}
			}
		}
		HX_STACK_LINE(346)
		{
			HX_STACK_LINE(346)
			int _g = (int)0;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(346)
			Array< ::Dynamic > _g1 = ::flixel::plugin::MouseEventManager_obj::_mouseOverObjects;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(346)
			while((true)){
				HX_STACK_LINE(346)
				if ((!(((_g < _g1->length))))){
					HX_STACK_LINE(346)
					break;
				}
				HX_STACK_LINE(346)
				::flixel::plugin::_MouseEventManager::ObjectMouseData over = _g1->__get(_g).StaticCast< ::flixel::plugin::_MouseEventManager::ObjectMouseData >();		HX_STACK_VAR(over,"over");
				HX_STACK_LINE(346)
				++(_g);
				HX_STACK_LINE(348)
				if (((over->onMouseOut != null()))){
					struct _Function_4_1{
						inline static bool Block( ::flixel::plugin::_MouseEventManager::ObjectMouseData &over,Array< ::Dynamic > &currentOverObjects){
							HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","flixel/plugin/MouseEventManager.hx",351,0x4767214f)
							{
								HX_STACK_LINE(351)
								::flixel::plugin::_MouseEventManager::ObjectMouseData _g11 = ::flixel::plugin::MouseEventManager_obj::getRegister(over->object,currentOverObjects);		HX_STACK_VAR(_g11,"_g11");
								HX_STACK_LINE(351)
								return (_g11 == null());
							}
							return null();
						}
					};
					HX_STACK_LINE(351)
					if (((  ((!(((bool(!(over->object->exists)) || bool(!(over->object->visible))))))) ? bool(_Function_4_1::Block(over,currentOverObjects)) : bool(true) ))){
						HX_STACK_LINE(353)
						over->onMouseOut(over->object);
					}
				}
			}
		}
		struct _Function_1_1{
			inline static bool Block( ){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","flixel/plugin/MouseEventManager.hx",360,0x4767214f)
				{
					HX_STACK_LINE(360)
					::flixel::input::mouse::FlxMouseButton _this = ::flixel::FlxG_obj::mouse->_leftButton;		HX_STACK_VAR(_this,"_this");
					HX_STACK_LINE(360)
					return (bool((_this->current == (int)2)) || bool((_this->current == (int)-2)));
				}
				return null();
			}
		};
		HX_STACK_LINE(360)
		if ((_Function_1_1::Block())){
			HX_STACK_LINE(362)
			int _g = (int)0;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(362)
			while((true)){
				HX_STACK_LINE(362)
				if ((!(((_g < currentOverObjects->length))))){
					HX_STACK_LINE(362)
					break;
				}
				HX_STACK_LINE(362)
				::flixel::plugin::_MouseEventManager::ObjectMouseData current = currentOverObjects->__get(_g).StaticCast< ::flixel::plugin::_MouseEventManager::ObjectMouseData >();		HX_STACK_VAR(current,"current");
				HX_STACK_LINE(362)
				++(_g);
				HX_STACK_LINE(364)
				if (((bool((bool((current->onMouseDown != null())) && bool(current->object->exists))) && bool(current->object->visible)))){
					HX_STACK_LINE(366)
					current->onMouseDown(current->object);
				}
			}
		}
		struct _Function_1_2{
			inline static bool Block( ){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","flixel/plugin/MouseEventManager.hx",372,0x4767214f)
				{
					HX_STACK_LINE(372)
					::flixel::input::mouse::FlxMouseButton _this = ::flixel::FlxG_obj::mouse->_leftButton;		HX_STACK_VAR(_this,"_this");
					HX_STACK_LINE(372)
					return (bool((_this->current == (int)-1)) || bool((_this->current == (int)-2)));
				}
				return null();
			}
		};
		HX_STACK_LINE(372)
		if ((_Function_1_2::Block())){
			HX_STACK_LINE(374)
			int _g = (int)0;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(374)
			while((true)){
				HX_STACK_LINE(374)
				if ((!(((_g < currentOverObjects->length))))){
					HX_STACK_LINE(374)
					break;
				}
				HX_STACK_LINE(374)
				::flixel::plugin::_MouseEventManager::ObjectMouseData current = currentOverObjects->__get(_g).StaticCast< ::flixel::plugin::_MouseEventManager::ObjectMouseData >();		HX_STACK_VAR(current,"current");
				HX_STACK_LINE(374)
				++(_g);
				HX_STACK_LINE(376)
				if (((bool((bool((current->onMouseUp != null())) && bool(current->object->exists))) && bool(current->object->visible)))){
					HX_STACK_LINE(378)
					current->onMouseUp(current->object);
				}
			}
		}
		HX_STACK_LINE(384)
		::flixel::plugin::MouseEventManager_obj::_mouseOverObjects = currentOverObjects;
	}
return null();
}


Void MouseEventManager_obj::clearRegistry( ){
{
		HX_STACK_FRAME("flixel.plugin.MouseEventManager","clearRegistry",0x3dcef6ab,"flixel.plugin.MouseEventManager.clearRegistry","flixel/plugin/MouseEventManager.hx",388,0x4767214f)
		HX_STACK_THIS(this)
		HX_STACK_LINE(389)
		::flixel::plugin::MouseEventManager_obj::_mouseOverObjects = null();
		HX_STACK_LINE(391)
		{
			HX_STACK_LINE(391)
			int _g = (int)0;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(391)
			Array< ::Dynamic > _g1 = ::flixel::plugin::MouseEventManager_obj::_registeredObjects;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(391)
			while((true)){
				HX_STACK_LINE(391)
				if ((!(((_g < _g1->length))))){
					HX_STACK_LINE(391)
					break;
				}
				HX_STACK_LINE(391)
				::flixel::plugin::_MouseEventManager::ObjectMouseData reg = _g1->__get(_g).StaticCast< ::flixel::plugin::_MouseEventManager::ObjectMouseData >();		HX_STACK_VAR(reg,"reg");
				HX_STACK_LINE(391)
				++(_g);
				HX_STACK_LINE(393)
				::flixel::plugin::MouseEventManager_obj::remove(reg->object);
			}
		}
		HX_STACK_LINE(396)
		::flixel::plugin::MouseEventManager_obj::_registeredObjects = null();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(MouseEventManager_obj,clearRegistry,(void))

bool MouseEventManager_obj::checkOverlap( ::flixel::plugin::_MouseEventManager::ObjectMouseData Register){
	HX_STACK_FRAME("flixel.plugin.MouseEventManager","checkOverlap",0x1f757c7e,"flixel.plugin.MouseEventManager.checkOverlap","flixel/plugin/MouseEventManager.hx",400,0x4767214f)
	HX_STACK_THIS(this)
	HX_STACK_ARG(Register,"Register")
	HX_STACK_LINE(401)
	{
		HX_STACK_LINE(401)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(401)
		Array< ::Dynamic > _g1 = Register->object->get_cameras();		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(401)
		while((true)){
			HX_STACK_LINE(401)
			if ((!(((_g < _g1->length))))){
				HX_STACK_LINE(401)
				break;
			}
			HX_STACK_LINE(401)
			::flixel::FlxCamera camera = _g1->__get(_g).StaticCast< ::flixel::FlxCamera >();		HX_STACK_VAR(camera,"camera");
			HX_STACK_LINE(401)
			++(_g);
			HX_STACK_LINE(404)
			::flixel::util::FlxPoint _g2 = ::flixel::FlxG_obj::mouse->getWorldPosition(camera,::flixel::plugin::MouseEventManager_obj::_point);		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(404)
			::flixel::plugin::MouseEventManager_obj::_point = _g2;
			struct _Function_3_1{
				inline static bool Block( ::flixel::plugin::_MouseEventManager::ObjectMouseData &Register,::flixel::FlxCamera &camera){
					HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","flixel/plugin/MouseEventManager.hx",406,0x4767214f)
					{
						HX_STACK_LINE(406)
						::flixel::util::FlxPoint Point = ::flixel::plugin::MouseEventManager_obj::_point;		HX_STACK_VAR(Point,"Point");
						struct _Function_4_1{
							inline static bool Block( ::flixel::util::FlxPoint &Point,::flixel::plugin::_MouseEventManager::ObjectMouseData &Register,::flixel::FlxCamera &camera){
								HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","flixel/plugin/MouseEventManager.hx",406,0x4767214f)
								{
									HX_STACK_LINE(406)
									::flixel::FlxSprite Sprite = Register->sprite;		HX_STACK_VAR(Sprite,"Sprite");
									HX_STACK_LINE(406)
									if (((Sprite->angle != (int)0))){
										HX_STACK_LINE(406)
										Float PivotX = (Sprite->x + Sprite->origin->x);		HX_STACK_VAR(PivotX,"PivotX");
										HX_STACK_LINE(406)
										Float PivotY = (Sprite->y + Sprite->origin->y);		HX_STACK_VAR(PivotY,"PivotY");
										HX_STACK_LINE(406)
										::flixel::util::FlxPoint point = Point;		HX_STACK_VAR(point,"point");
										HX_STACK_LINE(406)
										Float sin = (int)0;		HX_STACK_VAR(sin,"sin");
										HX_STACK_LINE(406)
										Float cos = (int)0;		HX_STACK_VAR(cos,"cos");
										HX_STACK_LINE(406)
										Float radians = (((Sprite->angle - (int)180)) * -(((Float(::Math_obj::PI) / Float((int)180)))));		HX_STACK_VAR(radians,"radians");
										HX_STACK_LINE(406)
										while((true)){
											HX_STACK_LINE(406)
											if ((!(((radians < -(::Math_obj::PI)))))){
												HX_STACK_LINE(406)
												break;
											}
											HX_STACK_LINE(406)
											hx::AddEq(radians,(::Math_obj::PI * (int)2));
										}
										HX_STACK_LINE(406)
										while((true)){
											HX_STACK_LINE(406)
											if ((!(((radians > ::Math_obj::PI))))){
												HX_STACK_LINE(406)
												break;
											}
											HX_STACK_LINE(406)
											radians = (radians - (::Math_obj::PI * (int)2));
										}
										HX_STACK_LINE(406)
										if (((radians < (int)0))){
											HX_STACK_LINE(406)
											sin = ((1.27323954 * radians) + ((.405284735 * radians) * radians));
											HX_STACK_LINE(406)
											if (((sin < (int)0))){
												HX_STACK_LINE(406)
												sin = ((.225 * (((sin * -(sin)) - sin))) + sin);
											}
											else{
												HX_STACK_LINE(406)
												sin = ((.225 * (((sin * sin) - sin))) + sin);
											}
										}
										else{
											HX_STACK_LINE(406)
											sin = ((1.27323954 * radians) - ((0.405284735 * radians) * radians));
											HX_STACK_LINE(406)
											if (((sin < (int)0))){
												HX_STACK_LINE(406)
												sin = ((.225 * (((sin * -(sin)) - sin))) + sin);
											}
											else{
												HX_STACK_LINE(406)
												sin = ((.225 * (((sin * sin) - sin))) + sin);
											}
										}
										HX_STACK_LINE(406)
										hx::AddEq(radians,(Float(::Math_obj::PI) / Float((int)2)));
										HX_STACK_LINE(406)
										if (((radians > ::Math_obj::PI))){
											HX_STACK_LINE(406)
											radians = (radians - (::Math_obj::PI * (int)2));
										}
										HX_STACK_LINE(406)
										if (((radians < (int)0))){
											HX_STACK_LINE(406)
											cos = ((1.27323954 * radians) + ((0.405284735 * radians) * radians));
											HX_STACK_LINE(406)
											if (((cos < (int)0))){
												HX_STACK_LINE(406)
												cos = ((.225 * (((cos * -(cos)) - cos))) + cos);
											}
											else{
												HX_STACK_LINE(406)
												cos = ((.225 * (((cos * cos) - cos))) + cos);
											}
										}
										else{
											HX_STACK_LINE(406)
											cos = ((1.27323954 * radians) - ((0.405284735 * radians) * radians));
											HX_STACK_LINE(406)
											if (((cos < (int)0))){
												HX_STACK_LINE(406)
												cos = ((.225 * (((cos * -(cos)) - cos))) + cos);
											}
											else{
												HX_STACK_LINE(406)
												cos = ((.225 * (((cos * cos) - cos))) + cos);
											}
										}
										HX_STACK_LINE(406)
										Float dx = (Point->x - PivotX);		HX_STACK_VAR(dx,"dx");
										HX_STACK_LINE(406)
										Float dy = (Point->y - PivotY);		HX_STACK_VAR(dy,"dy");
										HX_STACK_LINE(406)
										if (((point == null()))){
											HX_STACK_LINE(406)
											::flixel::util::FlxPoint _g11;		HX_STACK_VAR(_g11,"_g11");
											HX_STACK_LINE(406)
											{
												HX_STACK_LINE(406)
												Float X = (int)0;		HX_STACK_VAR(X,"X");
												HX_STACK_LINE(406)
												Float Y = (int)0;		HX_STACK_VAR(Y,"Y");
												HX_STACK_LINE(406)
												::flixel::util::FlxPoint point1 = ::flixel::util::FlxPoint_obj::_pool->get()->set(X,Y);		HX_STACK_VAR(point1,"point1");
												HX_STACK_LINE(406)
												point1->_inPool = false;
												HX_STACK_LINE(406)
												_g11 = point1;
											}
											HX_STACK_LINE(406)
											point = _g11;
										}
										HX_STACK_LINE(406)
										point->set_x(((PivotX + (cos * dx)) - (sin * dy)));
										HX_STACK_LINE(406)
										point->set_y(((PivotY - (sin * dx)) - (cos * dy)));
										HX_STACK_LINE(406)
										point;
									}
									HX_STACK_LINE(406)
									return Sprite->pixelsOverlapPoint(Point,(int)1,camera);
								}
								return null();
							}
						};
						HX_STACK_LINE(406)
						return (  (((bool(Register->pixelPerfect) && bool((Register->sprite != null()))))) ? bool(_Function_4_1::Block(Point,Register,camera)) : bool(Register->object->overlapsPoint(Point,true,camera)) );
					}
					return null();
				}
			};
			HX_STACK_LINE(406)
			if ((_Function_3_1::Block(Register,camera))){
				HX_STACK_LINE(408)
				return true;
			}
			HX_STACK_LINE(413)
			{
				HX_STACK_LINE(413)
				int _g21 = (int)0;		HX_STACK_VAR(_g21,"_g21");
				HX_STACK_LINE(413)
				Array< ::Dynamic > _g3 = ::flixel::FlxG_obj::touches->list;		HX_STACK_VAR(_g3,"_g3");
				HX_STACK_LINE(413)
				while((true)){
					HX_STACK_LINE(413)
					if ((!(((_g21 < _g3->length))))){
						HX_STACK_LINE(413)
						break;
					}
					HX_STACK_LINE(413)
					::flixel::input::touch::FlxTouch touch = _g3->__get(_g21).StaticCast< ::flixel::input::touch::FlxTouch >();		HX_STACK_VAR(touch,"touch");
					HX_STACK_LINE(413)
					++(_g21);
					HX_STACK_LINE(415)
					::flixel::util::FlxPoint _g22 = touch->getWorldPosition(camera,::flixel::plugin::MouseEventManager_obj::_point);		HX_STACK_VAR(_g22,"_g22");
					HX_STACK_LINE(415)
					::flixel::plugin::MouseEventManager_obj::_point = _g22;
					struct _Function_5_1{
						inline static bool Block( ::flixel::plugin::_MouseEventManager::ObjectMouseData &Register,::flixel::FlxCamera &camera){
							HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","flixel/plugin/MouseEventManager.hx",417,0x4767214f)
							{
								HX_STACK_LINE(417)
								::flixel::util::FlxPoint Point = ::flixel::plugin::MouseEventManager_obj::_point;		HX_STACK_VAR(Point,"Point");
								struct _Function_6_1{
									inline static bool Block( ::flixel::util::FlxPoint &Point,::flixel::plugin::_MouseEventManager::ObjectMouseData &Register,::flixel::FlxCamera &camera){
										HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","flixel/plugin/MouseEventManager.hx",417,0x4767214f)
										{
											HX_STACK_LINE(417)
											::flixel::FlxSprite Sprite = Register->sprite;		HX_STACK_VAR(Sprite,"Sprite");
											HX_STACK_LINE(417)
											if (((Sprite->angle != (int)0))){
												HX_STACK_LINE(417)
												Float PivotX = (Sprite->x + Sprite->origin->x);		HX_STACK_VAR(PivotX,"PivotX");
												HX_STACK_LINE(417)
												Float PivotY = (Sprite->y + Sprite->origin->y);		HX_STACK_VAR(PivotY,"PivotY");
												HX_STACK_LINE(417)
												::flixel::util::FlxPoint point = Point;		HX_STACK_VAR(point,"point");
												HX_STACK_LINE(417)
												Float sin = (int)0;		HX_STACK_VAR(sin,"sin");
												HX_STACK_LINE(417)
												Float cos = (int)0;		HX_STACK_VAR(cos,"cos");
												HX_STACK_LINE(417)
												Float radians = (((Sprite->angle - (int)180)) * -(((Float(::Math_obj::PI) / Float((int)180)))));		HX_STACK_VAR(radians,"radians");
												HX_STACK_LINE(417)
												while((true)){
													HX_STACK_LINE(417)
													if ((!(((radians < -(::Math_obj::PI)))))){
														HX_STACK_LINE(417)
														break;
													}
													HX_STACK_LINE(417)
													hx::AddEq(radians,(::Math_obj::PI * (int)2));
												}
												HX_STACK_LINE(417)
												while((true)){
													HX_STACK_LINE(417)
													if ((!(((radians > ::Math_obj::PI))))){
														HX_STACK_LINE(417)
														break;
													}
													HX_STACK_LINE(417)
													radians = (radians - (::Math_obj::PI * (int)2));
												}
												HX_STACK_LINE(417)
												if (((radians < (int)0))){
													HX_STACK_LINE(417)
													sin = ((1.27323954 * radians) + ((.405284735 * radians) * radians));
													HX_STACK_LINE(417)
													if (((sin < (int)0))){
														HX_STACK_LINE(417)
														sin = ((.225 * (((sin * -(sin)) - sin))) + sin);
													}
													else{
														HX_STACK_LINE(417)
														sin = ((.225 * (((sin * sin) - sin))) + sin);
													}
												}
												else{
													HX_STACK_LINE(417)
													sin = ((1.27323954 * radians) - ((0.405284735 * radians) * radians));
													HX_STACK_LINE(417)
													if (((sin < (int)0))){
														HX_STACK_LINE(417)
														sin = ((.225 * (((sin * -(sin)) - sin))) + sin);
													}
													else{
														HX_STACK_LINE(417)
														sin = ((.225 * (((sin * sin) - sin))) + sin);
													}
												}
												HX_STACK_LINE(417)
												hx::AddEq(radians,(Float(::Math_obj::PI) / Float((int)2)));
												HX_STACK_LINE(417)
												if (((radians > ::Math_obj::PI))){
													HX_STACK_LINE(417)
													radians = (radians - (::Math_obj::PI * (int)2));
												}
												HX_STACK_LINE(417)
												if (((radians < (int)0))){
													HX_STACK_LINE(417)
													cos = ((1.27323954 * radians) + ((0.405284735 * radians) * radians));
													HX_STACK_LINE(417)
													if (((cos < (int)0))){
														HX_STACK_LINE(417)
														cos = ((.225 * (((cos * -(cos)) - cos))) + cos);
													}
													else{
														HX_STACK_LINE(417)
														cos = ((.225 * (((cos * cos) - cos))) + cos);
													}
												}
												else{
													HX_STACK_LINE(417)
													cos = ((1.27323954 * radians) - ((0.405284735 * radians) * radians));
													HX_STACK_LINE(417)
													if (((cos < (int)0))){
														HX_STACK_LINE(417)
														cos = ((.225 * (((cos * -(cos)) - cos))) + cos);
													}
													else{
														HX_STACK_LINE(417)
														cos = ((.225 * (((cos * cos) - cos))) + cos);
													}
												}
												HX_STACK_LINE(417)
												Float dx = (Point->x - PivotX);		HX_STACK_VAR(dx,"dx");
												HX_STACK_LINE(417)
												Float dy = (Point->y - PivotY);		HX_STACK_VAR(dy,"dy");
												HX_STACK_LINE(417)
												if (((point == null()))){
													HX_STACK_LINE(417)
													::flixel::util::FlxPoint _g31;		HX_STACK_VAR(_g31,"_g31");
													HX_STACK_LINE(417)
													{
														HX_STACK_LINE(417)
														Float X = (int)0;		HX_STACK_VAR(X,"X");
														HX_STACK_LINE(417)
														Float Y = (int)0;		HX_STACK_VAR(Y,"Y");
														HX_STACK_LINE(417)
														::flixel::util::FlxPoint point1 = ::flixel::util::FlxPoint_obj::_pool->get()->set(X,Y);		HX_STACK_VAR(point1,"point1");
														HX_STACK_LINE(417)
														point1->_inPool = false;
														HX_STACK_LINE(417)
														_g31 = point1;
													}
													HX_STACK_LINE(417)
													point = _g31;
												}
												HX_STACK_LINE(417)
												point->set_x(((PivotX + (cos * dx)) - (sin * dy)));
												HX_STACK_LINE(417)
												point->set_y(((PivotY - (sin * dx)) - (cos * dy)));
												HX_STACK_LINE(417)
												point;
											}
											HX_STACK_LINE(417)
											return Sprite->pixelsOverlapPoint(Point,(int)1,camera);
										}
										return null();
									}
								};
								HX_STACK_LINE(417)
								return (  (((bool(Register->pixelPerfect) && bool((Register->sprite != null()))))) ? bool(_Function_6_1::Block(Point,Register,camera)) : bool(Register->object->overlapsPoint(Point,true,camera)) );
							}
							return null();
						}
					};
					HX_STACK_LINE(417)
					if ((_Function_5_1::Block(Register,camera))){
						HX_STACK_LINE(419)
						return true;
					}
				}
			}
		}
	}
	HX_STACK_LINE(425)
	return false;
}


HX_DEFINE_DYNAMIC_FUNC1(MouseEventManager_obj,checkOverlap,return )

bool MouseEventManager_obj::checkOverlapWithPoint( ::flixel::plugin::_MouseEventManager::ObjectMouseData Register,::flixel::util::FlxPoint Point,::flixel::FlxCamera Camera){
	HX_STACK_FRAME("flixel.plugin.MouseEventManager","checkOverlapWithPoint",0x6cf1fc0c,"flixel.plugin.MouseEventManager.checkOverlapWithPoint","flixel/plugin/MouseEventManager.hx",430,0x4767214f)
	HX_STACK_THIS(this)
	HX_STACK_ARG(Register,"Register")
	HX_STACK_ARG(Point,"Point")
	HX_STACK_ARG(Camera,"Camera")
	HX_STACK_LINE(430)
	if (((bool(Register->pixelPerfect) && bool((Register->sprite != null()))))){
		HX_STACK_LINE(432)
		::flixel::FlxSprite Sprite = Register->sprite;		HX_STACK_VAR(Sprite,"Sprite");
		HX_STACK_LINE(432)
		if (((Sprite->angle != (int)0))){
			HX_STACK_LINE(432)
			Float PivotX = (Sprite->x + Sprite->origin->x);		HX_STACK_VAR(PivotX,"PivotX");
			HX_STACK_LINE(432)
			Float PivotY = (Sprite->y + Sprite->origin->y);		HX_STACK_VAR(PivotY,"PivotY");
			HX_STACK_LINE(432)
			::flixel::util::FlxPoint point = Point;		HX_STACK_VAR(point,"point");
			HX_STACK_LINE(432)
			Float sin = (int)0;		HX_STACK_VAR(sin,"sin");
			HX_STACK_LINE(432)
			Float cos = (int)0;		HX_STACK_VAR(cos,"cos");
			HX_STACK_LINE(432)
			Float radians = (((Sprite->angle - (int)180)) * -(((Float(::Math_obj::PI) / Float((int)180)))));		HX_STACK_VAR(radians,"radians");
			HX_STACK_LINE(432)
			while((true)){
				HX_STACK_LINE(432)
				if ((!(((radians < -(::Math_obj::PI)))))){
					HX_STACK_LINE(432)
					break;
				}
				HX_STACK_LINE(432)
				hx::AddEq(radians,(::Math_obj::PI * (int)2));
			}
			HX_STACK_LINE(432)
			while((true)){
				HX_STACK_LINE(432)
				if ((!(((radians > ::Math_obj::PI))))){
					HX_STACK_LINE(432)
					break;
				}
				HX_STACK_LINE(432)
				radians = (radians - (::Math_obj::PI * (int)2));
			}
			HX_STACK_LINE(432)
			if (((radians < (int)0))){
				HX_STACK_LINE(432)
				sin = ((1.27323954 * radians) + ((.405284735 * radians) * radians));
				HX_STACK_LINE(432)
				if (((sin < (int)0))){
					HX_STACK_LINE(432)
					sin = ((.225 * (((sin * -(sin)) - sin))) + sin);
				}
				else{
					HX_STACK_LINE(432)
					sin = ((.225 * (((sin * sin) - sin))) + sin);
				}
			}
			else{
				HX_STACK_LINE(432)
				sin = ((1.27323954 * radians) - ((0.405284735 * radians) * radians));
				HX_STACK_LINE(432)
				if (((sin < (int)0))){
					HX_STACK_LINE(432)
					sin = ((.225 * (((sin * -(sin)) - sin))) + sin);
				}
				else{
					HX_STACK_LINE(432)
					sin = ((.225 * (((sin * sin) - sin))) + sin);
				}
			}
			HX_STACK_LINE(432)
			hx::AddEq(radians,(Float(::Math_obj::PI) / Float((int)2)));
			HX_STACK_LINE(432)
			if (((radians > ::Math_obj::PI))){
				HX_STACK_LINE(432)
				radians = (radians - (::Math_obj::PI * (int)2));
			}
			HX_STACK_LINE(432)
			if (((radians < (int)0))){
				HX_STACK_LINE(432)
				cos = ((1.27323954 * radians) + ((0.405284735 * radians) * radians));
				HX_STACK_LINE(432)
				if (((cos < (int)0))){
					HX_STACK_LINE(432)
					cos = ((.225 * (((cos * -(cos)) - cos))) + cos);
				}
				else{
					HX_STACK_LINE(432)
					cos = ((.225 * (((cos * cos) - cos))) + cos);
				}
			}
			else{
				HX_STACK_LINE(432)
				cos = ((1.27323954 * radians) - ((0.405284735 * radians) * radians));
				HX_STACK_LINE(432)
				if (((cos < (int)0))){
					HX_STACK_LINE(432)
					cos = ((.225 * (((cos * -(cos)) - cos))) + cos);
				}
				else{
					HX_STACK_LINE(432)
					cos = ((.225 * (((cos * cos) - cos))) + cos);
				}
			}
			HX_STACK_LINE(432)
			Float dx = (Point->x - PivotX);		HX_STACK_VAR(dx,"dx");
			HX_STACK_LINE(432)
			Float dy = (Point->y - PivotY);		HX_STACK_VAR(dy,"dy");
			HX_STACK_LINE(432)
			if (((point == null()))){
				HX_STACK_LINE(432)
				::flixel::util::FlxPoint _g;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(432)
				{
					HX_STACK_LINE(432)
					Float X = (int)0;		HX_STACK_VAR(X,"X");
					HX_STACK_LINE(432)
					Float Y = (int)0;		HX_STACK_VAR(Y,"Y");
					HX_STACK_LINE(432)
					::flixel::util::FlxPoint point1 = ::flixel::util::FlxPoint_obj::_pool->get()->set(X,Y);		HX_STACK_VAR(point1,"point1");
					HX_STACK_LINE(432)
					point1->_inPool = false;
					HX_STACK_LINE(432)
					_g = point1;
				}
				HX_STACK_LINE(432)
				point = _g;
			}
			HX_STACK_LINE(432)
			point->set_x(((PivotX + (cos * dx)) - (sin * dy)));
			HX_STACK_LINE(432)
			point->set_y(((PivotY - (sin * dx)) - (cos * dy)));
			HX_STACK_LINE(432)
			point;
		}
		HX_STACK_LINE(432)
		return Sprite->pixelsOverlapPoint(Point,(int)1,Camera);
	}
	else{
		HX_STACK_LINE(436)
		return Register->object->overlapsPoint(Point,true,Camera);
	}
	HX_STACK_LINE(430)
	return false;
}


HX_DEFINE_DYNAMIC_FUNC3(MouseEventManager_obj,checkOverlapWithPoint,return )

bool MouseEventManager_obj::checkPixelPerfectOverlap( ::flixel::util::FlxPoint Point,::flixel::FlxSprite Sprite,::flixel::FlxCamera Camera){
	HX_STACK_FRAME("flixel.plugin.MouseEventManager","checkPixelPerfectOverlap",0xc22d5a97,"flixel.plugin.MouseEventManager.checkPixelPerfectOverlap","flixel/plugin/MouseEventManager.hx",441,0x4767214f)
	HX_STACK_THIS(this)
	HX_STACK_ARG(Point,"Point")
	HX_STACK_ARG(Sprite,"Sprite")
	HX_STACK_ARG(Camera,"Camera")
	HX_STACK_LINE(442)
	if (((Sprite->angle != (int)0))){
		HX_STACK_LINE(444)
		Float PivotX = (Sprite->x + Sprite->origin->x);		HX_STACK_VAR(PivotX,"PivotX");
		HX_STACK_LINE(444)
		Float PivotY = (Sprite->y + Sprite->origin->y);		HX_STACK_VAR(PivotY,"PivotY");
		HX_STACK_LINE(444)
		::flixel::util::FlxPoint point = Point;		HX_STACK_VAR(point,"point");
		HX_STACK_LINE(444)
		Float sin = (int)0;		HX_STACK_VAR(sin,"sin");
		HX_STACK_LINE(444)
		Float cos = (int)0;		HX_STACK_VAR(cos,"cos");
		HX_STACK_LINE(444)
		Float radians = (((Sprite->angle - (int)180)) * -(((Float(::Math_obj::PI) / Float((int)180)))));		HX_STACK_VAR(radians,"radians");
		HX_STACK_LINE(444)
		while((true)){
			HX_STACK_LINE(444)
			if ((!(((radians < -(::Math_obj::PI)))))){
				HX_STACK_LINE(444)
				break;
			}
			HX_STACK_LINE(444)
			hx::AddEq(radians,(::Math_obj::PI * (int)2));
		}
		HX_STACK_LINE(444)
		while((true)){
			HX_STACK_LINE(444)
			if ((!(((radians > ::Math_obj::PI))))){
				HX_STACK_LINE(444)
				break;
			}
			HX_STACK_LINE(444)
			radians = (radians - (::Math_obj::PI * (int)2));
		}
		HX_STACK_LINE(444)
		if (((radians < (int)0))){
			HX_STACK_LINE(444)
			sin = ((1.27323954 * radians) + ((.405284735 * radians) * radians));
			HX_STACK_LINE(444)
			if (((sin < (int)0))){
				HX_STACK_LINE(444)
				sin = ((.225 * (((sin * -(sin)) - sin))) + sin);
			}
			else{
				HX_STACK_LINE(444)
				sin = ((.225 * (((sin * sin) - sin))) + sin);
			}
		}
		else{
			HX_STACK_LINE(444)
			sin = ((1.27323954 * radians) - ((0.405284735 * radians) * radians));
			HX_STACK_LINE(444)
			if (((sin < (int)0))){
				HX_STACK_LINE(444)
				sin = ((.225 * (((sin * -(sin)) - sin))) + sin);
			}
			else{
				HX_STACK_LINE(444)
				sin = ((.225 * (((sin * sin) - sin))) + sin);
			}
		}
		HX_STACK_LINE(444)
		hx::AddEq(radians,(Float(::Math_obj::PI) / Float((int)2)));
		HX_STACK_LINE(444)
		if (((radians > ::Math_obj::PI))){
			HX_STACK_LINE(444)
			radians = (radians - (::Math_obj::PI * (int)2));
		}
		HX_STACK_LINE(444)
		if (((radians < (int)0))){
			HX_STACK_LINE(444)
			cos = ((1.27323954 * radians) + ((0.405284735 * radians) * radians));
			HX_STACK_LINE(444)
			if (((cos < (int)0))){
				HX_STACK_LINE(444)
				cos = ((.225 * (((cos * -(cos)) - cos))) + cos);
			}
			else{
				HX_STACK_LINE(444)
				cos = ((.225 * (((cos * cos) - cos))) + cos);
			}
		}
		else{
			HX_STACK_LINE(444)
			cos = ((1.27323954 * radians) - ((0.405284735 * radians) * radians));
			HX_STACK_LINE(444)
			if (((cos < (int)0))){
				HX_STACK_LINE(444)
				cos = ((.225 * (((cos * -(cos)) - cos))) + cos);
			}
			else{
				HX_STACK_LINE(444)
				cos = ((.225 * (((cos * cos) - cos))) + cos);
			}
		}
		HX_STACK_LINE(444)
		Float dx = (Point->x - PivotX);		HX_STACK_VAR(dx,"dx");
		HX_STACK_LINE(444)
		Float dy = (Point->y - PivotY);		HX_STACK_VAR(dy,"dy");
		HX_STACK_LINE(444)
		if (((point == null()))){
			HX_STACK_LINE(444)
			::flixel::util::FlxPoint _g;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(444)
			{
				HX_STACK_LINE(444)
				Float X = (int)0;		HX_STACK_VAR(X,"X");
				HX_STACK_LINE(444)
				Float Y = (int)0;		HX_STACK_VAR(Y,"Y");
				HX_STACK_LINE(444)
				::flixel::util::FlxPoint point1 = ::flixel::util::FlxPoint_obj::_pool->get()->set(X,Y);		HX_STACK_VAR(point1,"point1");
				HX_STACK_LINE(444)
				point1->_inPool = false;
				HX_STACK_LINE(444)
				_g = point1;
			}
			HX_STACK_LINE(444)
			point = _g;
		}
		HX_STACK_LINE(444)
		point->set_x(((PivotX + (cos * dx)) - (sin * dy)));
		HX_STACK_LINE(444)
		point->set_y(((PivotY - (sin * dx)) - (cos * dy)));
		HX_STACK_LINE(444)
		point;
	}
	HX_STACK_LINE(446)
	return Sprite->pixelsOverlapPoint(Point,(int)1,Camera);
}


HX_DEFINE_DYNAMIC_FUNC3(MouseEventManager_obj,checkPixelPerfectOverlap,return )

Array< ::Dynamic > MouseEventManager_obj::_registeredObjects;

Array< ::Dynamic > MouseEventManager_obj::_mouseOverObjects;

::flixel::util::FlxPoint MouseEventManager_obj::_point;

Void MouseEventManager_obj::init( ){
{
		HX_STACK_FRAME("flixel.plugin.MouseEventManager","init",0x769e44ef,"flixel.plugin.MouseEventManager.init","flixel/plugin/MouseEventManager.hx",46,0x4767214f)
		HX_STACK_LINE(47)
		::flixel::plugin::FlxPlugin _g = ::flixel::FlxG_obj::plugins->get(hx::ClassOf< ::flixel::plugin::MouseEventManager >());		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(47)
		if (((_g == null()))){
			HX_STACK_LINE(48)
			::flixel::plugin::MouseEventManager _g1 = ::flixel::plugin::MouseEventManager_obj::__new();		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(48)
			::flixel::FlxG_obj::plugins->add_flixel_plugin_MouseEventManager(_g1);
		}
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC0(MouseEventManager_obj,init,(void))

Dynamic MouseEventManager_obj::add( Dynamic Object,Dynamic OnMouseDown,Dynamic OnMouseUp,Dynamic OnMouseOver,Dynamic OnMouseOut,hx::Null< bool >  __o_MouseChildren,hx::Null< bool >  __o_MouseEnabled,hx::Null< bool >  __o_PixelPerfect){
bool MouseChildren = __o_MouseChildren.Default(false);
bool MouseEnabled = __o_MouseEnabled.Default(true);
bool PixelPerfect = __o_PixelPerfect.Default(true);
	HX_STACK_FRAME("flixel.plugin.MouseEventManager","add",0x1e5b0922,"flixel.plugin.MouseEventManager.add","flixel/plugin/MouseEventManager.hx",64,0x4767214f)
	HX_STACK_ARG(Object,"Object")
	HX_STACK_ARG(OnMouseDown,"OnMouseDown")
	HX_STACK_ARG(OnMouseUp,"OnMouseUp")
	HX_STACK_ARG(OnMouseOver,"OnMouseOver")
	HX_STACK_ARG(OnMouseOut,"OnMouseOut")
	HX_STACK_ARG(MouseChildren,"MouseChildren")
	HX_STACK_ARG(MouseEnabled,"MouseEnabled")
	HX_STACK_ARG(PixelPerfect,"PixelPerfect")
{
		HX_STACK_LINE(65)
		{
			HX_STACK_LINE(65)
			::flixel::plugin::FlxPlugin _g = ::flixel::FlxG_obj::plugins->get(hx::ClassOf< ::flixel::plugin::MouseEventManager >());		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(65)
			if (((_g == null()))){
				HX_STACK_LINE(65)
				::flixel::plugin::MouseEventManager _g1 = ::flixel::plugin::MouseEventManager_obj::__new();		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(65)
				::flixel::FlxG_obj::plugins->add_flixel_plugin_MouseEventManager(_g1);
			}
		}
		HX_STACK_LINE(67)
		::flixel::plugin::_MouseEventManager::ObjectMouseData newReg = ::flixel::plugin::_MouseEventManager::ObjectMouseData_obj::__new(Object,OnMouseDown,OnMouseUp,OnMouseOver,OnMouseOut,MouseChildren,MouseEnabled,PixelPerfect);		HX_STACK_VAR(newReg,"newReg");
		HX_STACK_LINE(69)
		if ((::Std_obj::is(Object,hx::ClassOf< ::flixel::FlxSprite >()))){
			HX_STACK_LINE(71)
			newReg->sprite = Object;
		}
		HX_STACK_LINE(74)
		::flixel::plugin::MouseEventManager_obj::_registeredObjects->unshift(newReg);
		HX_STACK_LINE(75)
		return Object;
	}
}


STATIC_HX_DEFINE_DYNAMIC_FUNC8(MouseEventManager_obj,add,return )

Dynamic MouseEventManager_obj::remove( Dynamic Object){
	HX_STACK_FRAME("flixel.plugin.MouseEventManager","remove",0x7ba91be3,"flixel.plugin.MouseEventManager.remove","flixel/plugin/MouseEventManager.hx",82,0x4767214f)
	HX_STACK_ARG(Object,"Object")
	HX_STACK_LINE(83)
	{
		HX_STACK_LINE(83)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(83)
		Array< ::Dynamic > _g1 = ::flixel::plugin::MouseEventManager_obj::_registeredObjects;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(83)
		while((true)){
			HX_STACK_LINE(83)
			if ((!(((_g < _g1->length))))){
				HX_STACK_LINE(83)
				break;
			}
			HX_STACK_LINE(83)
			::flixel::plugin::_MouseEventManager::ObjectMouseData reg = _g1->__get(_g).StaticCast< ::flixel::plugin::_MouseEventManager::ObjectMouseData >();		HX_STACK_VAR(reg,"reg");
			HX_STACK_LINE(83)
			++(_g);
			HX_STACK_LINE(85)
			if (((reg->object == Object))){
				HX_STACK_LINE(87)
				reg->object = null();
				HX_STACK_LINE(88)
				reg->sprite = null();
				HX_STACK_LINE(89)
				reg->onMouseDown = null();
				HX_STACK_LINE(90)
				reg->onMouseUp = null();
				HX_STACK_LINE(91)
				reg->onMouseOver = null();
				HX_STACK_LINE(92)
				reg->onMouseOut = null();
				HX_STACK_LINE(93)
				::flixel::plugin::MouseEventManager_obj::_registeredObjects->remove(reg);
			}
		}
	}
	HX_STACK_LINE(96)
	return Object;
}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(MouseEventManager_obj,remove,return )

Void MouseEventManager_obj::reorder( ){
{
		HX_STACK_FRAME("flixel.plugin.MouseEventManager","reorder",0xe10a6f7c,"flixel.plugin.MouseEventManager.reorder","flixel/plugin/MouseEventManager.hx",107,0x4767214f)
		HX_STACK_LINE(108)
		Array< ::Dynamic > orderedObjects = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(orderedObjects,"orderedObjects");
		HX_STACK_LINE(109)
		Array< ::Dynamic > group = ::flixel::FlxG_obj::game->_state->members;		HX_STACK_VAR(group,"group");
		HX_STACK_LINE(111)
		::flixel::plugin::MouseEventManager_obj::traverseFlxGroup(::flixel::FlxG_obj::game->_state,orderedObjects);
		HX_STACK_LINE(113)
		orderedObjects->reverse();
		HX_STACK_LINE(114)
		::flixel::plugin::MouseEventManager_obj::_registeredObjects = orderedObjects;
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC0(MouseEventManager_obj,reorder,(void))

Void MouseEventManager_obj::setMouseDownCallback( Dynamic Object,Dynamic OnMouseDown){
{
		HX_STACK_FRAME("flixel.plugin.MouseEventManager","setMouseDownCallback",0xe5b0ebe9,"flixel.plugin.MouseEventManager.setMouseDownCallback","flixel/plugin/MouseEventManager.hx",123,0x4767214f)
		HX_STACK_ARG(Object,"Object")
		HX_STACK_ARG(OnMouseDown,"OnMouseDown")
		HX_STACK_LINE(124)
		::flixel::plugin::_MouseEventManager::ObjectMouseData reg = ::flixel::plugin::MouseEventManager_obj::getRegister(Object,null());		HX_STACK_VAR(reg,"reg");
		HX_STACK_LINE(126)
		if (((reg != null()))){
			HX_STACK_LINE(128)
			reg->onMouseDown = OnMouseDown;
		}
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC2(MouseEventManager_obj,setMouseDownCallback,(void))

Void MouseEventManager_obj::setMouseUpCallback( Dynamic Object,Dynamic OnMouseUp){
{
		HX_STACK_FRAME("flixel.plugin.MouseEventManager","setMouseUpCallback",0x9c77ee62,"flixel.plugin.MouseEventManager.setMouseUpCallback","flixel/plugin/MouseEventManager.hx",138,0x4767214f)
		HX_STACK_ARG(Object,"Object")
		HX_STACK_ARG(OnMouseUp,"OnMouseUp")
		HX_STACK_LINE(139)
		::flixel::plugin::_MouseEventManager::ObjectMouseData reg = ::flixel::plugin::MouseEventManager_obj::getRegister(Object,null());		HX_STACK_VAR(reg,"reg");
		HX_STACK_LINE(141)
		if (((reg != null()))){
			HX_STACK_LINE(143)
			reg->onMouseUp = OnMouseUp;
		}
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC2(MouseEventManager_obj,setMouseUpCallback,(void))

Void MouseEventManager_obj::setMouseOverCallback( Dynamic Object,Dynamic OnMouseOver){
{
		HX_STACK_FRAME("flixel.plugin.MouseEventManager","setMouseOverCallback",0x0588c6db,"flixel.plugin.MouseEventManager.setMouseOverCallback","flixel/plugin/MouseEventManager.hx",153,0x4767214f)
		HX_STACK_ARG(Object,"Object")
		HX_STACK_ARG(OnMouseOver,"OnMouseOver")
		HX_STACK_LINE(154)
		::flixel::plugin::_MouseEventManager::ObjectMouseData reg = ::flixel::plugin::MouseEventManager_obj::getRegister(Object,null());		HX_STACK_VAR(reg,"reg");
		HX_STACK_LINE(156)
		if (((reg != null()))){
			HX_STACK_LINE(158)
			reg->onMouseOver = OnMouseOver;
		}
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC2(MouseEventManager_obj,setMouseOverCallback,(void))

Void MouseEventManager_obj::setMouseOutCallback( Dynamic Object,Dynamic OnMouseOut){
{
		HX_STACK_FRAME("flixel.plugin.MouseEventManager","setMouseOutCallback",0xae3f9311,"flixel.plugin.MouseEventManager.setMouseOutCallback","flixel/plugin/MouseEventManager.hx",168,0x4767214f)
		HX_STACK_ARG(Object,"Object")
		HX_STACK_ARG(OnMouseOut,"OnMouseOut")
		HX_STACK_LINE(169)
		::flixel::plugin::_MouseEventManager::ObjectMouseData reg = ::flixel::plugin::MouseEventManager_obj::getRegister(Object,null());		HX_STACK_VAR(reg,"reg");
		HX_STACK_LINE(171)
		if (((reg != null()))){
			HX_STACK_LINE(173)
			reg->onMouseOut = OnMouseOut;
		}
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC2(MouseEventManager_obj,setMouseOutCallback,(void))

Void MouseEventManager_obj::setObjectMouseEnabled( Dynamic Object,bool MouseEnabled){
{
		HX_STACK_FRAME("flixel.plugin.MouseEventManager","setObjectMouseEnabled",0x435dfdbe,"flixel.plugin.MouseEventManager.setObjectMouseEnabled","flixel/plugin/MouseEventManager.hx",183,0x4767214f)
		HX_STACK_ARG(Object,"Object")
		HX_STACK_ARG(MouseEnabled,"MouseEnabled")
		HX_STACK_LINE(184)
		::flixel::plugin::_MouseEventManager::ObjectMouseData reg = ::flixel::plugin::MouseEventManager_obj::getRegister(Object,null());		HX_STACK_VAR(reg,"reg");
		HX_STACK_LINE(186)
		if (((reg != null()))){
			HX_STACK_LINE(188)
			reg->mouseEnabled = MouseEnabled;
		}
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC2(MouseEventManager_obj,setObjectMouseEnabled,(void))

bool MouseEventManager_obj::isObjectMouseEnabled( Dynamic Object){
	HX_STACK_FRAME("flixel.plugin.MouseEventManager","isObjectMouseEnabled",0x6b5d7ca4,"flixel.plugin.MouseEventManager.isObjectMouseEnabled","flixel/plugin/MouseEventManager.hx",196,0x4767214f)
	HX_STACK_ARG(Object,"Object")
	HX_STACK_LINE(197)
	::flixel::plugin::_MouseEventManager::ObjectMouseData reg = ::flixel::plugin::MouseEventManager_obj::getRegister(Object,null());		HX_STACK_VAR(reg,"reg");
	HX_STACK_LINE(199)
	if (((reg != null()))){
		HX_STACK_LINE(201)
		return reg->mouseEnabled;
	}
	else{
		HX_STACK_LINE(205)
		return false;
	}
	HX_STACK_LINE(199)
	return false;
}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(MouseEventManager_obj,isObjectMouseEnabled,return )

Void MouseEventManager_obj::setObjectMouseChildren( Dynamic Object,bool MouseChildren){
{
		HX_STACK_FRAME("flixel.plugin.MouseEventManager","setObjectMouseChildren",0x32973562,"flixel.plugin.MouseEventManager.setObjectMouseChildren","flixel/plugin/MouseEventManager.hx",215,0x4767214f)
		HX_STACK_ARG(Object,"Object")
		HX_STACK_ARG(MouseChildren,"MouseChildren")
		HX_STACK_LINE(216)
		::flixel::plugin::_MouseEventManager::ObjectMouseData reg = ::flixel::plugin::MouseEventManager_obj::getRegister(Object,null());		HX_STACK_VAR(reg,"reg");
		HX_STACK_LINE(218)
		if (((reg != null()))){
			HX_STACK_LINE(220)
			reg->mouseChildren = MouseChildren;
		}
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC2(MouseEventManager_obj,setObjectMouseChildren,(void))

bool MouseEventManager_obj::isObjectMouseChildren( Dynamic Object){
	HX_STACK_FRAME("flixel.plugin.MouseEventManager","isObjectMouseChildren",0x0a26bfbc,"flixel.plugin.MouseEventManager.isObjectMouseChildren","flixel/plugin/MouseEventManager.hx",228,0x4767214f)
	HX_STACK_ARG(Object,"Object")
	HX_STACK_LINE(229)
	::flixel::plugin::_MouseEventManager::ObjectMouseData reg = ::flixel::plugin::MouseEventManager_obj::getRegister(Object,null());		HX_STACK_VAR(reg,"reg");
	HX_STACK_LINE(231)
	if (((reg != null()))){
		HX_STACK_LINE(233)
		return reg->mouseChildren;
	}
	else{
		HX_STACK_LINE(237)
		HX_STACK_DO_THROW(::openfl::errors::Error_obj::__new(HX_CSTRING("MouseEventManager , isObjectMouseChildren() : object not found"),null()));
	}
	HX_STACK_LINE(231)
	return false;
}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(MouseEventManager_obj,isObjectMouseChildren,return )

Void MouseEventManager_obj::traverseFlxGroup( ::flixel::group::FlxGroup Group,Array< ::Dynamic > OrderedObjects){
{
		HX_STACK_FRAME("flixel.plugin.MouseEventManager","traverseFlxGroup",0xc9d3199e,"flixel.plugin.MouseEventManager.traverseFlxGroup","flixel/plugin/MouseEventManager.hx",243,0x4767214f)
		HX_STACK_ARG(Group,"Group")
		HX_STACK_ARG(OrderedObjects,"OrderedObjects")
		HX_STACK_LINE(243)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(243)
		Array< ::Dynamic > _g1 = Group->members;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(243)
		while((true)){
			HX_STACK_LINE(243)
			if ((!(((_g < _g1->length))))){
				HX_STACK_LINE(243)
				break;
			}
			HX_STACK_LINE(243)
			::flixel::FlxBasic basic = _g1->__get(_g).StaticCast< ::flixel::FlxBasic >();		HX_STACK_VAR(basic,"basic");
			HX_STACK_LINE(243)
			++(_g);
			HX_STACK_LINE(245)
			if ((::Std_obj::is(basic,hx::ClassOf< ::flixel::group::FlxGroup >()))){
				HX_STACK_LINE(247)
				::flixel::group::FlxGroup _g2;		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(247)
				_g2 = hx::TCast< flixel::group::FlxGroup >::cast(basic);
				HX_STACK_LINE(247)
				::flixel::plugin::MouseEventManager_obj::traverseFlxGroup(_g2,OrderedObjects);
			}
			HX_STACK_LINE(250)
			if ((::Std_obj::is(basic,hx::ClassOf< ::flixel::FlxSprite >()))){
				HX_STACK_LINE(252)
				::flixel::FlxSprite _g11;		HX_STACK_VAR(_g11,"_g11");
				HX_STACK_LINE(252)
				_g11 = hx::TCast< flixel::FlxSprite >::cast(basic);
				HX_STACK_LINE(252)
				::flixel::plugin::_MouseEventManager::ObjectMouseData reg = ::flixel::plugin::MouseEventManager_obj::getRegister(_g11,null());		HX_STACK_VAR(reg,"reg");
				HX_STACK_LINE(254)
				if (((reg != null()))){
					HX_STACK_LINE(256)
					OrderedObjects->push(reg);
				}
			}
		}
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC2(MouseEventManager_obj,traverseFlxGroup,(void))

::flixel::plugin::_MouseEventManager::ObjectMouseData MouseEventManager_obj::getRegister( Dynamic Object,Array< ::Dynamic > Register){
	HX_STACK_FRAME("flixel.plugin.MouseEventManager","getRegister",0x9104e91a,"flixel.plugin.MouseEventManager.getRegister","flixel/plugin/MouseEventManager.hx",263,0x4767214f)
	HX_STACK_ARG(Object,"Object")
	HX_STACK_ARG(Register,"Register")
	HX_STACK_LINE(264)
	if (((Register == null()))){
		HX_STACK_LINE(266)
		Register = ::flixel::plugin::MouseEventManager_obj::_registeredObjects;
	}
	HX_STACK_LINE(269)
	{
		HX_STACK_LINE(269)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(269)
		while((true)){
			HX_STACK_LINE(269)
			if ((!(((_g < Register->length))))){
				HX_STACK_LINE(269)
				break;
			}
			HX_STACK_LINE(269)
			::flixel::plugin::_MouseEventManager::ObjectMouseData reg = Register->__get(_g).StaticCast< ::flixel::plugin::_MouseEventManager::ObjectMouseData >();		HX_STACK_VAR(reg,"reg");
			HX_STACK_LINE(269)
			++(_g);
			HX_STACK_LINE(271)
			if (((reg->object == Object))){
				HX_STACK_LINE(273)
				return reg;
			}
		}
	}
	HX_STACK_LINE(277)
	return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC2(MouseEventManager_obj,getRegister,return )


MouseEventManager_obj::MouseEventManager_obj()
{
}

Dynamic MouseEventManager_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"add") ) { return add_dyn(); }
		break;
	case 4:
		if (HX_FIELD_EQ(inName,"init") ) { return init_dyn(); }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"_point") ) { return _point; }
		if (HX_FIELD_EQ(inName,"remove") ) { return remove_dyn(); }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"reorder") ) { return reorder_dyn(); }
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"getRegister") ) { return getRegister_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"checkOverlap") ) { return checkOverlap_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"clearRegistry") ) { return clearRegistry_dyn(); }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"traverseFlxGroup") ) { return traverseFlxGroup_dyn(); }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"_mouseOverObjects") ) { return _mouseOverObjects; }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"_registeredObjects") ) { return _registeredObjects; }
		if (HX_FIELD_EQ(inName,"setMouseUpCallback") ) { return setMouseUpCallback_dyn(); }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"setMouseOutCallback") ) { return setMouseOutCallback_dyn(); }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"setMouseDownCallback") ) { return setMouseDownCallback_dyn(); }
		if (HX_FIELD_EQ(inName,"setMouseOverCallback") ) { return setMouseOverCallback_dyn(); }
		if (HX_FIELD_EQ(inName,"isObjectMouseEnabled") ) { return isObjectMouseEnabled_dyn(); }
		break;
	case 21:
		if (HX_FIELD_EQ(inName,"setObjectMouseEnabled") ) { return setObjectMouseEnabled_dyn(); }
		if (HX_FIELD_EQ(inName,"isObjectMouseChildren") ) { return isObjectMouseChildren_dyn(); }
		if (HX_FIELD_EQ(inName,"checkOverlapWithPoint") ) { return checkOverlapWithPoint_dyn(); }
		break;
	case 22:
		if (HX_FIELD_EQ(inName,"setObjectMouseChildren") ) { return setObjectMouseChildren_dyn(); }
		break;
	case 24:
		if (HX_FIELD_EQ(inName,"checkPixelPerfectOverlap") ) { return checkPixelPerfectOverlap_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic MouseEventManager_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"_point") ) { _point=inValue.Cast< ::flixel::util::FlxPoint >(); return inValue; }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"_mouseOverObjects") ) { _mouseOverObjects=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"_registeredObjects") ) { _registeredObjects=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void MouseEventManager_obj::__GetFields(Array< ::String> &outFields)
{
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("_registeredObjects"),
	HX_CSTRING("_mouseOverObjects"),
	HX_CSTRING("_point"),
	HX_CSTRING("init"),
	HX_CSTRING("add"),
	HX_CSTRING("remove"),
	HX_CSTRING("reorder"),
	HX_CSTRING("setMouseDownCallback"),
	HX_CSTRING("setMouseUpCallback"),
	HX_CSTRING("setMouseOverCallback"),
	HX_CSTRING("setMouseOutCallback"),
	HX_CSTRING("setObjectMouseEnabled"),
	HX_CSTRING("isObjectMouseEnabled"),
	HX_CSTRING("setObjectMouseChildren"),
	HX_CSTRING("isObjectMouseChildren"),
	HX_CSTRING("traverseFlxGroup"),
	HX_CSTRING("getRegister"),
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo *sMemberStorageInfo = 0;
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("destroy"),
	HX_CSTRING("update"),
	HX_CSTRING("clearRegistry"),
	HX_CSTRING("checkOverlap"),
	HX_CSTRING("checkOverlapWithPoint"),
	HX_CSTRING("checkPixelPerfectOverlap"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(MouseEventManager_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(MouseEventManager_obj::_registeredObjects,"_registeredObjects");
	HX_MARK_MEMBER_NAME(MouseEventManager_obj::_mouseOverObjects,"_mouseOverObjects");
	HX_MARK_MEMBER_NAME(MouseEventManager_obj::_point,"_point");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(MouseEventManager_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(MouseEventManager_obj::_registeredObjects,"_registeredObjects");
	HX_VISIT_MEMBER_NAME(MouseEventManager_obj::_mouseOverObjects,"_mouseOverObjects");
	HX_VISIT_MEMBER_NAME(MouseEventManager_obj::_point,"_point");
};

#endif

Class MouseEventManager_obj::__mClass;

void MouseEventManager_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("flixel.plugin.MouseEventManager"), hx::TCanCast< MouseEventManager_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void MouseEventManager_obj::__boot()
{
}

} // end namespace flixel
} // end namespace plugin
