#include <hxcpp.h>

#ifndef INCLUDED_MenuState
#include <MenuState.h>
#endif
#ifndef INCLUDED_PlayState
#include <PlayState.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxGame
#include <flixel/FlxGame.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_FlxState
#include <flixel/FlxState.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_text_FlxText
#include <flixel/text/FlxText.h>
#endif
#ifndef INCLUDED_flixel_ui_FlxButton
#include <flixel/ui/FlxButton.h>
#endif
#ifndef INCLUDED_flixel_ui_FlxTypedButton
#include <flixel/ui/FlxTypedButton.h>
#endif
#ifndef INCLUDED_flixel_util_FlxDestroyUtil
#include <flixel/util/FlxDestroyUtil.h>
#endif
#ifndef INCLUDED_flixel_util_FlxSpriteUtil
#include <flixel/util/FlxSpriteUtil.h>
#endif
#ifndef INCLUDED_openfl__v2_display_DisplayObject
#include <openfl/_v2/display/DisplayObject.h>
#endif
#ifndef INCLUDED_openfl__v2_display_DisplayObjectContainer
#include <openfl/_v2/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_openfl__v2_display_IBitmapDrawable
#include <openfl/_v2/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_openfl__v2_display_InteractiveObject
#include <openfl/_v2/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_openfl__v2_display_Sprite
#include <openfl/_v2/display/Sprite.h>
#endif
#ifndef INCLUDED_openfl__v2_events_EventDispatcher
#include <openfl/_v2/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__v2_events_IEventDispatcher
#include <openfl/_v2/events/IEventDispatcher.h>
#endif

Void MenuState_obj::__construct(Dynamic MaxSize)
{
HX_STACK_FRAME("MenuState","new",0xe563b1c4,"MenuState.new","MenuState.hx",15,0xdfbcb22c)
HX_STACK_THIS(this)
HX_STACK_ARG(MaxSize,"MaxSize")
{
	HX_STACK_LINE(22)
	this->checkBoxBool = true;
	HX_STACK_LINE(15)
	super::__construct(MaxSize);
}
;
	return null();
}

//MenuState_obj::~MenuState_obj() { }

Dynamic MenuState_obj::__CreateEmpty() { return  new MenuState_obj; }
hx::ObjectPtr< MenuState_obj > MenuState_obj::__new(Dynamic MaxSize)
{  hx::ObjectPtr< MenuState_obj > result = new MenuState_obj();
	result->__construct(MaxSize);
	return result;}

Dynamic MenuState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< MenuState_obj > result = new MenuState_obj();
	result->__construct(inArgs[0]);
	return result;}

Void MenuState_obj::create( ){
{
		HX_STACK_FRAME("MenuState","create",0xe57b7c18,"MenuState.create","MenuState.hx",31,0xdfbcb22c)
		HX_STACK_THIS(this)
		HX_STACK_LINE(32)
		::flixel::ui::FlxButton _g = ::flixel::ui::FlxButton_obj::__new((int)0,(int)0,HX_CSTRING("START"),this->startGame_dyn());		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(32)
		this->playBtn = _g;
		HX_STACK_LINE(33)
		::flixel::text::FlxText _g1 = ::flixel::text::FlxText_obj::__new((int)0,(int)0,(int)100,HX_CSTRING("Bepple"),(int)20,false);		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(33)
		this->gameTitle = _g1;
		HX_STACK_LINE(34)
		::flixel::ui::FlxButton _g2 = ::flixel::ui::FlxButton_obj::__new((int)0,(int)0,HX_CSTRING("Tutorial"),this->tutCheck_dyn());		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(34)
		this->tutButton = _g2;
		HX_STACK_LINE(35)
		::flixel::FlxSprite _g3 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(35)
		this->checkBox = _g3;
		HX_STACK_LINE(36)
		this->checkBox->loadGraphic(HX_CSTRING("assets/images/tutCheckBoxAnimate.png"),true,(int)16,(int)16,null(),null());
		HX_STACK_LINE(37)
		this->checkBox->animation->add(HX_CSTRING("off"),Array_obj< int >::__new().Add((int)0),(int)1,false);
		HX_STACK_LINE(38)
		this->checkBox->animation->add(HX_CSTRING("on"),Array_obj< int >::__new().Add((int)1),(int)1,false);
		HX_STACK_LINE(39)
		::flixel::util::FlxSpriteUtil_obj::screenCenter(this->checkBox,null(),null());
		HX_STACK_LINE(40)
		{
			HX_STACK_LINE(40)
			::flixel::FlxSprite _g4 = this->checkBox;		HX_STACK_VAR(_g4,"_g4");
			HX_STACK_LINE(40)
			_g4->set_y((_g4->y + (int)25));
		}
		HX_STACK_LINE(41)
		{
			HX_STACK_LINE(41)
			::flixel::FlxSprite _g4 = this->checkBox;		HX_STACK_VAR(_g4,"_g4");
			HX_STACK_LINE(41)
			_g4->set_x((_g4->x + (int)60));
		}
		HX_STACK_LINE(42)
		this->gameTitle->set_alignment(HX_CSTRING("center"));
		HX_STACK_LINE(43)
		::flixel::util::FlxSpriteUtil_obj::screenCenter(this->gameTitle,true,true);
		HX_STACK_LINE(44)
		{
			HX_STACK_LINE(44)
			::flixel::text::FlxText _g4 = this->gameTitle;		HX_STACK_VAR(_g4,"_g4");
			HX_STACK_LINE(44)
			_g4->set_y((_g4->y - (int)30));
		}
		HX_STACK_LINE(45)
		::flixel::util::FlxSpriteUtil_obj::screenCenter(this->playBtn,null(),null());
		HX_STACK_LINE(46)
		::flixel::util::FlxSpriteUtil_obj::screenCenter(this->tutButton,null(),null());
		HX_STACK_LINE(47)
		{
			HX_STACK_LINE(47)
			::flixel::ui::FlxButton _g4 = this->tutButton;		HX_STACK_VAR(_g4,"_g4");
			HX_STACK_LINE(47)
			_g4->set_y((_g4->y + (int)25));
		}
		HX_STACK_LINE(48)
		this->add(this->gameTitle);
		HX_STACK_LINE(49)
		this->add(this->playBtn);
		HX_STACK_LINE(50)
		this->add(this->tutButton);
		HX_STACK_LINE(51)
		this->add(this->checkBox);
		HX_STACK_LINE(52)
		this->super::create();
	}
return null();
}


bool MenuState_obj::getTutorialOn( ){
	HX_STACK_FRAME("MenuState","getTutorialOn",0x6456daf7,"MenuState.getTutorialOn","MenuState.hx",57,0xdfbcb22c)
	HX_STACK_THIS(this)
	HX_STACK_LINE(57)
	return ::MenuState_obj::tutorialOn;
}


HX_DEFINE_DYNAMIC_FUNC0(MenuState_obj,getTutorialOn,return )

Void MenuState_obj::destroy( ){
{
		HX_STACK_FRAME("MenuState","destroy",0xf9ac905e,"MenuState.destroy","MenuState.hx",65,0xdfbcb22c)
		HX_STACK_THIS(this)
		HX_STACK_LINE(67)
		this->super::destroy();
		HX_STACK_LINE(68)
		::flixel::ui::FlxButton _g = ::flixel::util::FlxDestroyUtil_obj::destroy(this->playBtn);		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(68)
		this->playBtn = _g;
	}
return null();
}


Void MenuState_obj::update( ){
{
		HX_STACK_FRAME("MenuState","update",0xf0719b25,"MenuState.update","MenuState.hx",77,0xdfbcb22c)
		HX_STACK_THIS(this)
		HX_STACK_LINE(77)
		this->super::update();
	}
return null();
}


Void MenuState_obj::startGame( ){
{
		HX_STACK_FRAME("MenuState","startGame",0x336371d8,"MenuState.startGame","MenuState.hx",82,0xdfbcb22c)
		HX_STACK_THIS(this)
		HX_STACK_LINE(82)
		::flixel::FlxState State = ::PlayState_obj::__new(null());		HX_STACK_VAR(State,"State");
		HX_STACK_LINE(82)
		::flixel::FlxG_obj::game->_requestedState = State;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(MenuState_obj,startGame,(void))

Void MenuState_obj::tutCheck( ){
{
		HX_STACK_FRAME("MenuState","tutCheck",0xd2e57411,"MenuState.tutCheck","MenuState.hx",87,0xdfbcb22c)
		HX_STACK_THIS(this)
		HX_STACK_LINE(87)
		if ((this->checkBoxBool)){
			HX_STACK_LINE(88)
			this->checkBox->animation->play(HX_CSTRING("on"),null(),null());
			HX_STACK_LINE(89)
			::MenuState_obj::tutorialOn = true;
			HX_STACK_LINE(90)
			this->checkBoxBool = false;
		}
		else{
			HX_STACK_LINE(93)
			this->checkBox->animation->play(HX_CSTRING("off"),null(),null());
			HX_STACK_LINE(94)
			::MenuState_obj::tutorialOn = false;
			HX_STACK_LINE(95)
			this->checkBoxBool = true;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(MenuState_obj,tutCheck,(void))

bool MenuState_obj::tutorialOn;

int MenuState_obj::windowLength;

int MenuState_obj::windowWidth;


MenuState_obj::MenuState_obj()
{
}

void MenuState_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(MenuState);
	HX_MARK_MEMBER_NAME(playBtn,"playBtn");
	HX_MARK_MEMBER_NAME(tutButton,"tutButton");
	HX_MARK_MEMBER_NAME(gameTitle,"gameTitle");
	HX_MARK_MEMBER_NAME(checkBox,"checkBox");
	HX_MARK_MEMBER_NAME(checkBoxBool,"checkBoxBool");
	::flixel::FlxState_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void MenuState_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(playBtn,"playBtn");
	HX_VISIT_MEMBER_NAME(tutButton,"tutButton");
	HX_VISIT_MEMBER_NAME(gameTitle,"gameTitle");
	HX_VISIT_MEMBER_NAME(checkBox,"checkBox");
	HX_VISIT_MEMBER_NAME(checkBoxBool,"checkBoxBool");
	::flixel::FlxState_obj::__Visit(HX_VISIT_ARG);
}

Dynamic MenuState_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"create") ) { return create_dyn(); }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"playBtn") ) { return playBtn; }
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"checkBox") ) { return checkBox; }
		if (HX_FIELD_EQ(inName,"tutCheck") ) { return tutCheck_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"tutButton") ) { return tutButton; }
		if (HX_FIELD_EQ(inName,"gameTitle") ) { return gameTitle; }
		if (HX_FIELD_EQ(inName,"startGame") ) { return startGame_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"tutorialOn") ) { return tutorialOn; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"windowWidth") ) { return windowWidth; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"windowLength") ) { return windowLength; }
		if (HX_FIELD_EQ(inName,"checkBoxBool") ) { return checkBoxBool; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"getTutorialOn") ) { return getTutorialOn_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic MenuState_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 7:
		if (HX_FIELD_EQ(inName,"playBtn") ) { playBtn=inValue.Cast< ::flixel::ui::FlxButton >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"checkBox") ) { checkBox=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"tutButton") ) { tutButton=inValue.Cast< ::flixel::ui::FlxButton >(); return inValue; }
		if (HX_FIELD_EQ(inName,"gameTitle") ) { gameTitle=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"tutorialOn") ) { tutorialOn=inValue.Cast< bool >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"windowWidth") ) { windowWidth=inValue.Cast< int >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"windowLength") ) { windowLength=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"checkBoxBool") ) { checkBoxBool=inValue.Cast< bool >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void MenuState_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("playBtn"));
	outFields->push(HX_CSTRING("tutButton"));
	outFields->push(HX_CSTRING("gameTitle"));
	outFields->push(HX_CSTRING("checkBox"));
	outFields->push(HX_CSTRING("checkBoxBool"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("tutorialOn"),
	HX_CSTRING("windowLength"),
	HX_CSTRING("windowWidth"),
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::ui::FlxButton*/ ,(int)offsetof(MenuState_obj,playBtn),HX_CSTRING("playBtn")},
	{hx::fsObject /*::flixel::ui::FlxButton*/ ,(int)offsetof(MenuState_obj,tutButton),HX_CSTRING("tutButton")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(MenuState_obj,gameTitle),HX_CSTRING("gameTitle")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(MenuState_obj,checkBox),HX_CSTRING("checkBox")},
	{hx::fsBool,(int)offsetof(MenuState_obj,checkBoxBool),HX_CSTRING("checkBoxBool")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("playBtn"),
	HX_CSTRING("tutButton"),
	HX_CSTRING("gameTitle"),
	HX_CSTRING("checkBox"),
	HX_CSTRING("checkBoxBool"),
	HX_CSTRING("create"),
	HX_CSTRING("getTutorialOn"),
	HX_CSTRING("destroy"),
	HX_CSTRING("update"),
	HX_CSTRING("startGame"),
	HX_CSTRING("tutCheck"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(MenuState_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(MenuState_obj::tutorialOn,"tutorialOn");
	HX_MARK_MEMBER_NAME(MenuState_obj::windowLength,"windowLength");
	HX_MARK_MEMBER_NAME(MenuState_obj::windowWidth,"windowWidth");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(MenuState_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(MenuState_obj::tutorialOn,"tutorialOn");
	HX_VISIT_MEMBER_NAME(MenuState_obj::windowLength,"windowLength");
	HX_VISIT_MEMBER_NAME(MenuState_obj::windowWidth,"windowWidth");
};

#endif

Class MenuState_obj::__mClass;

void MenuState_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("MenuState"), hx::TCanCast< MenuState_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void MenuState_obj::__boot()
{
	windowLength= (int)640;
	windowWidth= (int)480;
}

