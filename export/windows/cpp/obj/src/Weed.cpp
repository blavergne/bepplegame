#include <hxcpp.h>

#ifndef INCLUDED_Weed
#include <Weed.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif

Void Weed_obj::__construct(int weedIndex)
{
HX_STACK_FRAME("Weed","new",0xecf5b77f,"Weed.new","Weed.hx",19,0xed8bfcd1)
HX_STACK_THIS(this)
HX_STACK_ARG(weedIndex,"weedIndex")
{
	HX_STACK_LINE(21)
	super::__construct(null());
	HX_STACK_LINE(22)
	this->init(weedIndex);
}
;
	return null();
}

//Weed_obj::~Weed_obj() { }

Dynamic Weed_obj::__CreateEmpty() { return  new Weed_obj; }
hx::ObjectPtr< Weed_obj > Weed_obj::__new(int weedIndex)
{  hx::ObjectPtr< Weed_obj > result = new Weed_obj();
	result->__construct(weedIndex);
	return result;}

Dynamic Weed_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Weed_obj > result = new Weed_obj();
	result->__construct(inArgs[0]);
	return result;}

Void Weed_obj::init( int weedIndex){
{
		HX_STACK_FRAME("Weed","init",0x66c38f11,"Weed.init","Weed.hx",26,0xed8bfcd1)
		HX_STACK_THIS(this)
		HX_STACK_ARG(weedIndex,"weedIndex")
		HX_STACK_LINE(27)
		int weedArrayIndex;		HX_STACK_VAR(weedArrayIndex,"weedArrayIndex");
		HX_STACK_LINE(28)
		Array< ::Dynamic > _g = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(28)
		this->weedArray = _g;
		HX_STACK_LINE(29)
		::flixel::group::FlxGroup _g1 = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(29)
		this->weedGroup = _g1;
		HX_STACK_LINE(30)
		::flixel::FlxSprite _g2 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(30)
		this->weedA = _g2;
		HX_STACK_LINE(31)
		::flixel::FlxSprite _g3 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(31)
		this->weedB = _g3;
		HX_STACK_LINE(32)
		::flixel::FlxSprite _g4 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g4,"_g4");
		HX_STACK_LINE(32)
		this->chosenWeed = _g4;
		HX_STACK_LINE(33)
		this->weedA->loadGraphic(HX_CSTRING("assets/images/weedA.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(34)
		this->weedB->loadGraphic(HX_CSTRING("assets/images/weedB.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(35)
		this->weedArray[(int)0] = this->weedA;
		HX_STACK_LINE(36)
		this->weedArray[(int)1] = this->weedB;
		HX_STACK_LINE(37)
		this->chosenWeed = this->weedArray->__get(weedIndex).StaticCast< ::flixel::FlxSprite >();
		HX_STACK_LINE(38)
		this->weedGroup->add(this->chosenWeed);
		HX_STACK_LINE(39)
		this->add(this->weedGroup);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Weed_obj,init,(void))

::flixel::FlxSprite Weed_obj::getChosenWeed( ){
	HX_STACK_FRAME("Weed","getChosenWeed",0x85daa4d4,"Weed.getChosenWeed","Weed.hx",43,0xed8bfcd1)
	HX_STACK_THIS(this)
	HX_STACK_LINE(43)
	return this->chosenWeed;
}


HX_DEFINE_DYNAMIC_FUNC0(Weed_obj,getChosenWeed,return )


Weed_obj::Weed_obj()
{
}

void Weed_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Weed);
	HX_MARK_MEMBER_NAME(weedArray,"weedArray");
	HX_MARK_MEMBER_NAME(weedGroup,"weedGroup");
	HX_MARK_MEMBER_NAME(weedA,"weedA");
	HX_MARK_MEMBER_NAME(weedB,"weedB");
	HX_MARK_MEMBER_NAME(chosenWeed,"chosenWeed");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void Weed_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(weedArray,"weedArray");
	HX_VISIT_MEMBER_NAME(weedGroup,"weedGroup");
	HX_VISIT_MEMBER_NAME(weedA,"weedA");
	HX_VISIT_MEMBER_NAME(weedB,"weedB");
	HX_VISIT_MEMBER_NAME(chosenWeed,"chosenWeed");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic Weed_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"init") ) { return init_dyn(); }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"weedA") ) { return weedA; }
		if (HX_FIELD_EQ(inName,"weedB") ) { return weedB; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"weedArray") ) { return weedArray; }
		if (HX_FIELD_EQ(inName,"weedGroup") ) { return weedGroup; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"chosenWeed") ) { return chosenWeed; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"getChosenWeed") ) { return getChosenWeed_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Weed_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"weedA") ) { weedA=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"weedB") ) { weedB=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"weedArray") ) { weedArray=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		if (HX_FIELD_EQ(inName,"weedGroup") ) { weedGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"chosenWeed") ) { chosenWeed=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Weed_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("weedArray"));
	outFields->push(HX_CSTRING("weedGroup"));
	outFields->push(HX_CSTRING("weedA"));
	outFields->push(HX_CSTRING("weedB"));
	outFields->push(HX_CSTRING("chosenWeed"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(Weed_obj,weedArray),HX_CSTRING("weedArray")},
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(Weed_obj,weedGroup),HX_CSTRING("weedGroup")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Weed_obj,weedA),HX_CSTRING("weedA")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Weed_obj,weedB),HX_CSTRING("weedB")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Weed_obj,chosenWeed),HX_CSTRING("chosenWeed")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("weedArray"),
	HX_CSTRING("weedGroup"),
	HX_CSTRING("weedA"),
	HX_CSTRING("weedB"),
	HX_CSTRING("chosenWeed"),
	HX_CSTRING("init"),
	HX_CSTRING("getChosenWeed"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Weed_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Weed_obj::__mClass,"__mClass");
};

#endif

Class Weed_obj::__mClass;

void Weed_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("Weed"), hx::TCanCast< Weed_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void Weed_obj::__boot()
{
}

