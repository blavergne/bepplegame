#include <hxcpp.h>

#ifndef INCLUDED_AsteroidBelt
#include <AsteroidBelt.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_haxe_Log
#include <haxe/Log.h>
#endif

Void AsteroidBelt_obj::__construct(int beltXPos)
{
HX_STACK_FRAME("AsteroidBelt","new",0xe70129f8,"AsteroidBelt.new","AsteroidBelt.hx",15,0x0a383c78)
HX_STACK_THIS(this)
HX_STACK_ARG(beltXPos,"beltXPos")
{
	HX_STACK_LINE(16)
	super::__construct(null());
	HX_STACK_LINE(17)
	::flixel::FlxSprite _g = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(17)
	this->asteroidBelt = _g;
	HX_STACK_LINE(18)
	this->asteroidBelt->loadGraphic(HX_CSTRING("assets/images/meteorField.png"),false,null(),null(),null(),null());
	HX_STACK_LINE(19)
	this->asteroidBelt->set_y((int)120);
	HX_STACK_LINE(20)
	this->asteroidBelt->set_x(beltXPos);
	HX_STACK_LINE(21)
	this->add(this->asteroidBelt);
}
;
	return null();
}

//AsteroidBelt_obj::~AsteroidBelt_obj() { }

Dynamic AsteroidBelt_obj::__CreateEmpty() { return  new AsteroidBelt_obj; }
hx::ObjectPtr< AsteroidBelt_obj > AsteroidBelt_obj::__new(int beltXPos)
{  hx::ObjectPtr< AsteroidBelt_obj > result = new AsteroidBelt_obj();
	result->__construct(beltXPos);
	return result;}

Dynamic AsteroidBelt_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< AsteroidBelt_obj > result = new AsteroidBelt_obj();
	result->__construct(inArgs[0]);
	return result;}

::flixel::FlxSprite AsteroidBelt_obj::getAsteroidBeltSprite( ){
	HX_STACK_FRAME("AsteroidBelt","getAsteroidBeltSprite",0xd0fca459,"AsteroidBelt.getAsteroidBeltSprite","AsteroidBelt.hx",25,0x0a383c78)
	HX_STACK_THIS(this)
	HX_STACK_LINE(25)
	return this->asteroidBelt;
}


HX_DEFINE_DYNAMIC_FUNC0(AsteroidBelt_obj,getAsteroidBeltSprite,return )

Void AsteroidBelt_obj::setAsteroidBeltSprite( int val){
{
		HX_STACK_FRAME("AsteroidBelt","setAsteroidBeltSprite",0x25057265,"AsteroidBelt.setAsteroidBeltSprite","AsteroidBelt.hx",29,0x0a383c78)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(29)
		this->asteroidBelt->set_x(val);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(AsteroidBelt_obj,setAsteroidBeltSprite,(void))

Void AsteroidBelt_obj::asteroidFloat( ){
{
		HX_STACK_FRAME("AsteroidBelt","asteroidFloat",0x4d8035f9,"AsteroidBelt.asteroidFloat","AsteroidBelt.hx",33,0x0a383c78)
		HX_STACK_THIS(this)
		HX_STACK_LINE(33)
		::flixel::FlxSprite _g = this->asteroidBelt;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(33)
		_g->set_x((_g->x + .25));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(AsteroidBelt_obj,asteroidFloat,(void))

Void AsteroidBelt_obj::destroyAsteroids( ){
{
		HX_STACK_FRAME("AsteroidBelt","destroyAsteroids",0xa23cede6,"AsteroidBelt.destroyAsteroids","AsteroidBelt.hx",37,0x0a383c78)
		HX_STACK_THIS(this)
		HX_STACK_LINE(37)
		if (((this->asteroidBelt->x >= (int)640))){
			HX_STACK_LINE(38)
			::haxe::Log_obj::trace(HX_CSTRING("destroy asteroid belt"),hx::SourceInfo(HX_CSTRING("AsteroidBelt.hx"),38,HX_CSTRING("AsteroidBelt"),HX_CSTRING("destroyAsteroids")));
			HX_STACK_LINE(39)
			this->destroy();
			HX_STACK_LINE(40)
			this->asteroidBelt = null();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(AsteroidBelt_obj,destroyAsteroids,(void))

Void AsteroidBelt_obj::destroy( ){
{
		HX_STACK_FRAME("AsteroidBelt","destroy",0x943c3292,"AsteroidBelt.destroy","AsteroidBelt.hx",44,0x0a383c78)
		HX_STACK_THIS(this)
		HX_STACK_LINE(45)
		this->asteroidBelt = null();
		HX_STACK_LINE(46)
		this->super::destroy();
	}
return null();
}


Void AsteroidBelt_obj::update( ){
{
		HX_STACK_FRAME("AsteroidBelt","update",0x82ee3b71,"AsteroidBelt.update","AsteroidBelt.hx",49,0x0a383c78)
		HX_STACK_THIS(this)
		HX_STACK_LINE(50)
		this->asteroidFloat();
		HX_STACK_LINE(51)
		this->destroyAsteroids();
		HX_STACK_LINE(52)
		if (((this->asteroidBelt != null()))){
			HX_STACK_LINE(53)
			this->super::update();
		}
	}
return null();
}



AsteroidBelt_obj::AsteroidBelt_obj()
{
}

void AsteroidBelt_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(AsteroidBelt);
	HX_MARK_MEMBER_NAME(asteroidBelt,"asteroidBelt");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void AsteroidBelt_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(asteroidBelt,"asteroidBelt");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic AsteroidBelt_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"asteroidBelt") ) { return asteroidBelt; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"asteroidFloat") ) { return asteroidFloat_dyn(); }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"destroyAsteroids") ) { return destroyAsteroids_dyn(); }
		break;
	case 21:
		if (HX_FIELD_EQ(inName,"getAsteroidBeltSprite") ) { return getAsteroidBeltSprite_dyn(); }
		if (HX_FIELD_EQ(inName,"setAsteroidBeltSprite") ) { return setAsteroidBeltSprite_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic AsteroidBelt_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 12:
		if (HX_FIELD_EQ(inName,"asteroidBelt") ) { asteroidBelt=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void AsteroidBelt_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("asteroidBelt"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(AsteroidBelt_obj,asteroidBelt),HX_CSTRING("asteroidBelt")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("asteroidBelt"),
	HX_CSTRING("getAsteroidBeltSprite"),
	HX_CSTRING("setAsteroidBeltSprite"),
	HX_CSTRING("asteroidFloat"),
	HX_CSTRING("destroyAsteroids"),
	HX_CSTRING("destroy"),
	HX_CSTRING("update"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(AsteroidBelt_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(AsteroidBelt_obj::__mClass,"__mClass");
};

#endif

Class AsteroidBelt_obj::__mClass;

void AsteroidBelt_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("AsteroidBelt"), hx::TCanCast< AsteroidBelt_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void AsteroidBelt_obj::__boot()
{
}

