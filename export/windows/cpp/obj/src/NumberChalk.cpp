#include <hxcpp.h>

#ifndef INCLUDED_NumberChalk
#include <NumberChalk.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxPooled
#include <flixel/interfaces/IFlxPooled.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPoint
#include <flixel/util/FlxPoint.h>
#endif
#ifndef INCLUDED_flixel_util_FlxRandom
#include <flixel/util/FlxRandom.h>
#endif

Void NumberChalk_obj::__construct()
{
HX_STACK_FRAME("NumberChalk","new",0xd17e61e4,"NumberChalk.new","NumberChalk.hx",29,0x6dff660c)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(30)
	super::__construct(null());
	HX_STACK_LINE(31)
	this->init();
}
;
	return null();
}

//NumberChalk_obj::~NumberChalk_obj() { }

Dynamic NumberChalk_obj::__CreateEmpty() { return  new NumberChalk_obj; }
hx::ObjectPtr< NumberChalk_obj > NumberChalk_obj::__new()
{  hx::ObjectPtr< NumberChalk_obj > result = new NumberChalk_obj();
	result->__construct();
	return result;}

Dynamic NumberChalk_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< NumberChalk_obj > result = new NumberChalk_obj();
	result->__construct();
	return result;}

Void NumberChalk_obj::init( ){
{
		HX_STACK_FRAME("NumberChalk","init",0x79cffd0c,"NumberChalk.init","NumberChalk.hx",35,0x6dff660c)
		HX_STACK_THIS(this)
		HX_STACK_LINE(37)
		Array< ::Dynamic > _g = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(37)
		this->numRandArray = _g;
		HX_STACK_LINE(38)
		::flixel::FlxSprite _g1 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(38)
		this->myRandNum = _g1;
		HX_STACK_LINE(39)
		::flixel::FlxSprite _g2 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(39)
		this->c0 = _g2;
		HX_STACK_LINE(40)
		::flixel::FlxSprite _g3 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(40)
		this->c1 = _g3;
		HX_STACK_LINE(41)
		::flixel::FlxSprite _g4 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g4,"_g4");
		HX_STACK_LINE(41)
		this->c2 = _g4;
		HX_STACK_LINE(42)
		::flixel::FlxSprite _g5 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g5,"_g5");
		HX_STACK_LINE(42)
		this->c3 = _g5;
		HX_STACK_LINE(43)
		::flixel::FlxSprite _g6 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g6,"_g6");
		HX_STACK_LINE(43)
		this->c4 = _g6;
		HX_STACK_LINE(44)
		::flixel::FlxSprite _g7 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g7,"_g7");
		HX_STACK_LINE(44)
		this->c5 = _g7;
		HX_STACK_LINE(45)
		::flixel::FlxSprite _g8 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g8,"_g8");
		HX_STACK_LINE(45)
		this->c6 = _g8;
		HX_STACK_LINE(46)
		::flixel::FlxSprite _g9 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g9,"_g9");
		HX_STACK_LINE(46)
		this->c7 = _g9;
		HX_STACK_LINE(47)
		::flixel::FlxSprite _g10 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g10,"_g10");
		HX_STACK_LINE(47)
		this->c8 = _g10;
		HX_STACK_LINE(48)
		::flixel::FlxSprite _g11 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g11,"_g11");
		HX_STACK_LINE(48)
		this->c9 = _g11;
		HX_STACK_LINE(49)
		this->c0->loadGraphic(HX_CSTRING("assets/images/chalkNumbers_0.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(50)
		this->c1->loadGraphic(HX_CSTRING("assets/images/chalkNumbers_1.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(51)
		this->c2->loadGraphic(HX_CSTRING("assets/images/chalkNumbers_2.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(52)
		this->c3->loadGraphic(HX_CSTRING("assets/images/chalkNumbers_3.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(53)
		this->c4->loadGraphic(HX_CSTRING("assets/images/chalkNumbers_4.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(54)
		this->c5->loadGraphic(HX_CSTRING("assets/images/chalkNumbers_5.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(55)
		this->c6->loadGraphic(HX_CSTRING("assets/images/chalkNumbers_6.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(56)
		this->c7->loadGraphic(HX_CSTRING("assets/images/chalkNumbers_7.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(57)
		this->c8->loadGraphic(HX_CSTRING("assets/images/chalkNumbers_8.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(58)
		this->c9->loadGraphic(HX_CSTRING("assets/images/chalkNumbers_9.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(59)
		this->c1->scale->set_y(.6);
		HX_STACK_LINE(60)
		this->c1->scale->set_x(.6);
		HX_STACK_LINE(61)
		this->c2->scale->set_y(.6);
		HX_STACK_LINE(62)
		this->c2->scale->set_x(.6);
		HX_STACK_LINE(63)
		this->c3->scale->set_y(.6);
		HX_STACK_LINE(64)
		this->c3->scale->set_x(.6);
		HX_STACK_LINE(65)
		this->c4->scale->set_y(.6);
		HX_STACK_LINE(66)
		this->c4->scale->set_x(.6);
		HX_STACK_LINE(67)
		this->c5->scale->set_y(.6);
		HX_STACK_LINE(68)
		this->c5->scale->set_x(.6);
		HX_STACK_LINE(69)
		this->c6->scale->set_y(.6);
		HX_STACK_LINE(70)
		this->c6->scale->set_x(.6);
		HX_STACK_LINE(71)
		this->c7->scale->set_y(.6);
		HX_STACK_LINE(72)
		this->c7->scale->set_x(.6);
		HX_STACK_LINE(73)
		this->c8->scale->set_y(.6);
		HX_STACK_LINE(74)
		this->c8->scale->set_x(.6);
		HX_STACK_LINE(75)
		this->c9->scale->set_y(.6);
		HX_STACK_LINE(76)
		this->c9->scale->set_x(.6);
		HX_STACK_LINE(77)
		this->c0->scale->set_y(.6);
		HX_STACK_LINE(78)
		this->c0->scale->set_x(.6);
		HX_STACK_LINE(79)
		this->numRandArray[(int)0] = this->c0;
		HX_STACK_LINE(80)
		this->numRandArray[(int)1] = this->c1;
		HX_STACK_LINE(81)
		this->numRandArray[(int)2] = this->c2;
		HX_STACK_LINE(82)
		this->numRandArray[(int)3] = this->c3;
		HX_STACK_LINE(83)
		this->numRandArray[(int)4] = this->c4;
		HX_STACK_LINE(84)
		this->numRandArray[(int)5] = this->c5;
		HX_STACK_LINE(85)
		this->numRandArray[(int)6] = this->c6;
		HX_STACK_LINE(86)
		this->numRandArray[(int)7] = this->c7;
		HX_STACK_LINE(87)
		this->numRandArray[(int)8] = this->c8;
		HX_STACK_LINE(88)
		this->numRandArray[(int)9] = this->c9;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(NumberChalk_obj,init,(void))

::flixel::FlxSprite NumberChalk_obj::getRandomNumber( ){
	HX_STACK_FRAME("NumberChalk","getRandomNumber",0x82cf1e66,"NumberChalk.getRandomNumber","NumberChalk.hx",92,0x6dff660c)
	HX_STACK_THIS(this)
	HX_STACK_LINE(94)
	int _g = ::flixel::util::FlxRandom_obj::intRanged((int)0,(int)9,null());		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(94)
	this->myIndex = _g;
	HX_STACK_LINE(95)
	this->myRandNum = this->numRandArray->__get(this->myIndex).StaticCast< ::flixel::FlxSprite >();
	HX_STACK_LINE(96)
	return this->myRandNum;
}


HX_DEFINE_DYNAMIC_FUNC0(NumberChalk_obj,getRandomNumber,return )

int NumberChalk_obj::getMyIndex( ){
	HX_STACK_FRAME("NumberChalk","getMyIndex",0xcf32310c,"NumberChalk.getMyIndex","NumberChalk.hx",102,0x6dff660c)
	HX_STACK_THIS(this)
	HX_STACK_LINE(102)
	return this->myIndex;
}


HX_DEFINE_DYNAMIC_FUNC0(NumberChalk_obj,getMyIndex,return )

Void NumberChalk_obj::destroy( ){
{
		HX_STACK_FRAME("NumberChalk","destroy",0xedb1d07e,"NumberChalk.destroy","NumberChalk.hx",106,0x6dff660c)
		HX_STACK_THIS(this)
		HX_STACK_LINE(107)
		this->numRandArray = null();
		HX_STACK_LINE(108)
		this->myRandNum = null();
		HX_STACK_LINE(109)
		this->c0 = null();
		HX_STACK_LINE(110)
		this->c1 = null();
		HX_STACK_LINE(111)
		this->c2 = null();
		HX_STACK_LINE(112)
		this->c3 = null();
		HX_STACK_LINE(113)
		this->c4 = null();
		HX_STACK_LINE(114)
		this->c5 = null();
		HX_STACK_LINE(115)
		this->c6 = null();
		HX_STACK_LINE(116)
		this->c7 = null();
		HX_STACK_LINE(117)
		this->c8 = null();
		HX_STACK_LINE(118)
		this->c9 = null();
		HX_STACK_LINE(119)
		this->super::destroy();
	}
return null();
}



NumberChalk_obj::NumberChalk_obj()
{
}

void NumberChalk_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(NumberChalk);
	HX_MARK_MEMBER_NAME(myRandNum,"myRandNum");
	HX_MARK_MEMBER_NAME(myIndex,"myIndex");
	HX_MARK_MEMBER_NAME(c0,"c0");
	HX_MARK_MEMBER_NAME(c1,"c1");
	HX_MARK_MEMBER_NAME(c2,"c2");
	HX_MARK_MEMBER_NAME(c3,"c3");
	HX_MARK_MEMBER_NAME(c4,"c4");
	HX_MARK_MEMBER_NAME(c5,"c5");
	HX_MARK_MEMBER_NAME(c6,"c6");
	HX_MARK_MEMBER_NAME(c7,"c7");
	HX_MARK_MEMBER_NAME(c8,"c8");
	HX_MARK_MEMBER_NAME(c9,"c9");
	HX_MARK_MEMBER_NAME(numRandArray,"numRandArray");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void NumberChalk_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(myRandNum,"myRandNum");
	HX_VISIT_MEMBER_NAME(myIndex,"myIndex");
	HX_VISIT_MEMBER_NAME(c0,"c0");
	HX_VISIT_MEMBER_NAME(c1,"c1");
	HX_VISIT_MEMBER_NAME(c2,"c2");
	HX_VISIT_MEMBER_NAME(c3,"c3");
	HX_VISIT_MEMBER_NAME(c4,"c4");
	HX_VISIT_MEMBER_NAME(c5,"c5");
	HX_VISIT_MEMBER_NAME(c6,"c6");
	HX_VISIT_MEMBER_NAME(c7,"c7");
	HX_VISIT_MEMBER_NAME(c8,"c8");
	HX_VISIT_MEMBER_NAME(c9,"c9");
	HX_VISIT_MEMBER_NAME(numRandArray,"numRandArray");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic NumberChalk_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 2:
		if (HX_FIELD_EQ(inName,"c0") ) { return c0; }
		if (HX_FIELD_EQ(inName,"c1") ) { return c1; }
		if (HX_FIELD_EQ(inName,"c2") ) { return c2; }
		if (HX_FIELD_EQ(inName,"c3") ) { return c3; }
		if (HX_FIELD_EQ(inName,"c4") ) { return c4; }
		if (HX_FIELD_EQ(inName,"c5") ) { return c5; }
		if (HX_FIELD_EQ(inName,"c6") ) { return c6; }
		if (HX_FIELD_EQ(inName,"c7") ) { return c7; }
		if (HX_FIELD_EQ(inName,"c8") ) { return c8; }
		if (HX_FIELD_EQ(inName,"c9") ) { return c9; }
		break;
	case 4:
		if (HX_FIELD_EQ(inName,"init") ) { return init_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"myIndex") ) { return myIndex; }
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"myRandNum") ) { return myRandNum; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"getMyIndex") ) { return getMyIndex_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"numRandArray") ) { return numRandArray; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"getRandomNumber") ) { return getRandomNumber_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic NumberChalk_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 2:
		if (HX_FIELD_EQ(inName,"c0") ) { c0=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"c1") ) { c1=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"c2") ) { c2=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"c3") ) { c3=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"c4") ) { c4=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"c5") ) { c5=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"c6") ) { c6=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"c7") ) { c7=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"c8") ) { c8=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"c9") ) { c9=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"myIndex") ) { myIndex=inValue.Cast< int >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"myRandNum") ) { myRandNum=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"numRandArray") ) { numRandArray=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void NumberChalk_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("myRandNum"));
	outFields->push(HX_CSTRING("myIndex"));
	outFields->push(HX_CSTRING("c0"));
	outFields->push(HX_CSTRING("c1"));
	outFields->push(HX_CSTRING("c2"));
	outFields->push(HX_CSTRING("c3"));
	outFields->push(HX_CSTRING("c4"));
	outFields->push(HX_CSTRING("c5"));
	outFields->push(HX_CSTRING("c6"));
	outFields->push(HX_CSTRING("c7"));
	outFields->push(HX_CSTRING("c8"));
	outFields->push(HX_CSTRING("c9"));
	outFields->push(HX_CSTRING("numRandArray"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(NumberChalk_obj,myRandNum),HX_CSTRING("myRandNum")},
	{hx::fsInt,(int)offsetof(NumberChalk_obj,myIndex),HX_CSTRING("myIndex")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(NumberChalk_obj,c0),HX_CSTRING("c0")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(NumberChalk_obj,c1),HX_CSTRING("c1")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(NumberChalk_obj,c2),HX_CSTRING("c2")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(NumberChalk_obj,c3),HX_CSTRING("c3")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(NumberChalk_obj,c4),HX_CSTRING("c4")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(NumberChalk_obj,c5),HX_CSTRING("c5")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(NumberChalk_obj,c6),HX_CSTRING("c6")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(NumberChalk_obj,c7),HX_CSTRING("c7")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(NumberChalk_obj,c8),HX_CSTRING("c8")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(NumberChalk_obj,c9),HX_CSTRING("c9")},
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(NumberChalk_obj,numRandArray),HX_CSTRING("numRandArray")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("myRandNum"),
	HX_CSTRING("myIndex"),
	HX_CSTRING("c0"),
	HX_CSTRING("c1"),
	HX_CSTRING("c2"),
	HX_CSTRING("c3"),
	HX_CSTRING("c4"),
	HX_CSTRING("c5"),
	HX_CSTRING("c6"),
	HX_CSTRING("c7"),
	HX_CSTRING("c8"),
	HX_CSTRING("c9"),
	HX_CSTRING("numRandArray"),
	HX_CSTRING("init"),
	HX_CSTRING("getRandomNumber"),
	HX_CSTRING("getMyIndex"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(NumberChalk_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(NumberChalk_obj::__mClass,"__mClass");
};

#endif

Class NumberChalk_obj::__mClass;

void NumberChalk_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("NumberChalk"), hx::TCanCast< NumberChalk_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void NumberChalk_obj::__boot()
{
}

