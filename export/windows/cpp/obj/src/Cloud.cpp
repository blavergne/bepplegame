#include <hxcpp.h>

#ifndef INCLUDED_Cloud
#include <Cloud.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_system_FlxSound
#include <flixel/system/FlxSound.h>
#endif
#ifndef INCLUDED_flixel_system_frontEnds_SoundFrontEnd
#include <flixel/system/frontEnds/SoundFrontEnd.h>
#endif
#ifndef INCLUDED_haxe_Log
#include <haxe/Log.h>
#endif

Void Cloud_obj::__construct()
{
HX_STACK_FRAME("Cloud","new",0xd8d3f8c7,"Cloud.new","Cloud.hx",13,0x84fd7c89)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(20)
	this->destroyCloud = false;
	HX_STACK_LINE(19)
	this->cloudDone = false;
	HX_STACK_LINE(16)
	this->cloudGroup = ::flixel::group::FlxGroup_obj::__new(null());
	HX_STACK_LINE(25)
	super::__construct(null());
	HX_STACK_LINE(26)
	::flixel::FlxSprite _g = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(26)
	this->cloud = _g;
	HX_STACK_LINE(27)
	this->cloud->loadGraphic(HX_CSTRING("assets/images/rainCloudAnimationA.png"),true,null(),null(),null(),null());
	HX_STACK_LINE(28)
	::flixel::system::FlxSound _g1 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/raining.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
	HX_STACK_LINE(28)
	this->rainSound = _g1;
	HX_STACK_LINE(29)
	this->rainSound->play(null());
	HX_STACK_LINE(30)
	this->cloud->animation->add(HX_CSTRING("rain"),Array_obj< int >::__new().Add((int)0).Add((int)1),(int)10,true);
	HX_STACK_LINE(31)
	this->cloud->animation->play(HX_CSTRING("rain"),null(),null());
	HX_STACK_LINE(32)
	this->cloud->set_alpha(0.0);
	HX_STACK_LINE(33)
	this->cloudGroup->add(this->cloud);
	HX_STACK_LINE(34)
	this->add(this->cloudGroup);
}
;
	return null();
}

//Cloud_obj::~Cloud_obj() { }

Dynamic Cloud_obj::__CreateEmpty() { return  new Cloud_obj; }
hx::ObjectPtr< Cloud_obj > Cloud_obj::__new()
{  hx::ObjectPtr< Cloud_obj > result = new Cloud_obj();
	result->__construct();
	return result;}

Dynamic Cloud_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Cloud_obj > result = new Cloud_obj();
	result->__construct();
	return result;}

Float Cloud_obj::getCloudAlpha( ){
	HX_STACK_FRAME("Cloud","getCloudAlpha",0x48794106,"Cloud.getCloudAlpha","Cloud.hx",40,0x84fd7c89)
	HX_STACK_THIS(this)
	HX_STACK_LINE(40)
	return this->cloud->alpha;
}


HX_DEFINE_DYNAMIC_FUNC0(Cloud_obj,getCloudAlpha,return )

Void Cloud_obj::setCloudX( Float xVal){
{
		HX_STACK_FRAME("Cloud","setCloudX",0xbc2b922c,"Cloud.setCloudX","Cloud.hx",46,0x84fd7c89)
		HX_STACK_THIS(this)
		HX_STACK_ARG(xVal,"xVal")
		HX_STACK_LINE(46)
		this->cloud->set_x(xVal);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Cloud_obj,setCloudX,(void))

Void Cloud_obj::setCloudY( Float yVal){
{
		HX_STACK_FRAME("Cloud","setCloudY",0xbc2b922d,"Cloud.setCloudY","Cloud.hx",51,0x84fd7c89)
		HX_STACK_THIS(this)
		HX_STACK_ARG(yVal,"yVal")
		HX_STACK_LINE(51)
		this->cloud->set_y(yVal);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Cloud_obj,setCloudY,(void))

Void Cloud_obj::fadeCloud( ){
{
		HX_STACK_FRAME("Cloud","fadeCloud",0x827c6200,"Cloud.fadeCloud","Cloud.hx",57,0x84fd7c89)
		HX_STACK_THIS(this)
		HX_STACK_LINE(57)
		if (((bool((this->cloud->alpha < (int)1)) && bool(!(this->cloudDone))))){
			HX_STACK_LINE(58)
			::flixel::FlxSprite _g = this->cloud;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(58)
			_g->set_alpha((_g->alpha + .01));
		}
		else{
			HX_STACK_LINE(61)
			this->cloudDone = true;
			HX_STACK_LINE(62)
			{
				HX_STACK_LINE(62)
				::flixel::FlxSprite _g = this->cloud;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(62)
				_g->set_alpha((_g->alpha - .005));
			}
			HX_STACK_LINE(63)
			if (((this->cloud->alpha <= (int)0))){
				HX_STACK_LINE(64)
				this->destroyCloud = true;
				HX_STACK_LINE(65)
				::haxe::Log_obj::trace(HX_CSTRING("destroy cloud"),hx::SourceInfo(HX_CSTRING("Cloud.hx"),65,HX_CSTRING("Cloud"),HX_CSTRING("fadeCloud")));
				HX_STACK_LINE(66)
				this->destroy();
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Cloud_obj,fadeCloud,(void))

Void Cloud_obj::update( ){
{
		HX_STACK_FRAME("Cloud","update",0xb3235802,"Cloud.update","Cloud.hx",72,0x84fd7c89)
		HX_STACK_THIS(this)
		HX_STACK_LINE(74)
		this->fadeCloud();
		HX_STACK_LINE(76)
		if ((!(this->destroyCloud))){
			HX_STACK_LINE(77)
			this->super::update();
		}
	}
return null();
}


Void Cloud_obj::destroy( ){
{
		HX_STACK_FRAME("Cloud","destroy",0x928014e1,"Cloud.destroy","Cloud.hx",81,0x84fd7c89)
		HX_STACK_THIS(this)
		HX_STACK_LINE(82)
		this->cloudGroup = null();
		HX_STACK_LINE(83)
		this->cloud = null();
		HX_STACK_LINE(84)
		this->rainSound = null();
		HX_STACK_LINE(85)
		this->destroy_dyn();
		HX_STACK_LINE(86)
		this->super::destroy();
	}
return null();
}



Cloud_obj::Cloud_obj()
{
}

void Cloud_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Cloud);
	HX_MARK_MEMBER_NAME(cloudGroup,"cloudGroup");
	HX_MARK_MEMBER_NAME(cloud,"cloud");
	HX_MARK_MEMBER_NAME(rainSound,"rainSound");
	HX_MARK_MEMBER_NAME(cloudDone,"cloudDone");
	HX_MARK_MEMBER_NAME(destroyCloud,"destroyCloud");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void Cloud_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(cloudGroup,"cloudGroup");
	HX_VISIT_MEMBER_NAME(cloud,"cloud");
	HX_VISIT_MEMBER_NAME(rainSound,"rainSound");
	HX_VISIT_MEMBER_NAME(cloudDone,"cloudDone");
	HX_VISIT_MEMBER_NAME(destroyCloud,"destroyCloud");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic Cloud_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"cloud") ) { return cloud; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"rainSound") ) { return rainSound; }
		if (HX_FIELD_EQ(inName,"cloudDone") ) { return cloudDone; }
		if (HX_FIELD_EQ(inName,"setCloudX") ) { return setCloudX_dyn(); }
		if (HX_FIELD_EQ(inName,"setCloudY") ) { return setCloudY_dyn(); }
		if (HX_FIELD_EQ(inName,"fadeCloud") ) { return fadeCloud_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"cloudGroup") ) { return cloudGroup; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"destroyCloud") ) { return destroyCloud; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"getCloudAlpha") ) { return getCloudAlpha_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Cloud_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"cloud") ) { cloud=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"rainSound") ) { rainSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		if (HX_FIELD_EQ(inName,"cloudDone") ) { cloudDone=inValue.Cast< bool >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"cloudGroup") ) { cloudGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"destroyCloud") ) { destroyCloud=inValue.Cast< bool >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Cloud_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("cloudGroup"));
	outFields->push(HX_CSTRING("cloud"));
	outFields->push(HX_CSTRING("rainSound"));
	outFields->push(HX_CSTRING("cloudDone"));
	outFields->push(HX_CSTRING("destroyCloud"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(Cloud_obj,cloudGroup),HX_CSTRING("cloudGroup")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Cloud_obj,cloud),HX_CSTRING("cloud")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(Cloud_obj,rainSound),HX_CSTRING("rainSound")},
	{hx::fsBool,(int)offsetof(Cloud_obj,cloudDone),HX_CSTRING("cloudDone")},
	{hx::fsBool,(int)offsetof(Cloud_obj,destroyCloud),HX_CSTRING("destroyCloud")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("cloudGroup"),
	HX_CSTRING("cloud"),
	HX_CSTRING("rainSound"),
	HX_CSTRING("cloudDone"),
	HX_CSTRING("destroyCloud"),
	HX_CSTRING("getCloudAlpha"),
	HX_CSTRING("setCloudX"),
	HX_CSTRING("setCloudY"),
	HX_CSTRING("fadeCloud"),
	HX_CSTRING("update"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Cloud_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Cloud_obj::__mClass,"__mClass");
};

#endif

Class Cloud_obj::__mClass;

void Cloud_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("Cloud"), hx::TCanCast< Cloud_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void Cloud_obj::__boot()
{
}

