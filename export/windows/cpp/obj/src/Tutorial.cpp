#include <hxcpp.h>

#ifndef INCLUDED_Tutorial
#include <Tutorial.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_plugin_FlxPlugin
#include <flixel/plugin/FlxPlugin.h>
#endif
#ifndef INCLUDED_flixel_plugin_MouseEventManager
#include <flixel/plugin/MouseEventManager.h>
#endif

Void Tutorial_obj::__construct(Float tutorialType)
{
HX_STACK_FRAME("Tutorial","new",0xae46a3b0,"Tutorial.new","Tutorial.hx",12,0x2eb361c0)
HX_STACK_THIS(this)
HX_STACK_ARG(tutorialType,"tutorialType")
{
	HX_STACK_LINE(40)
	this->handMovingToTile = false;
	HX_STACK_LINE(39)
	this->moveHandWithSeed = false;
	HX_STACK_LINE(38)
	this->handMovingToSeed = false;
	HX_STACK_LINE(36)
	this->currentMessageIndex = (int)0;
	HX_STACK_LINE(35)
	this->messageFadeDirection = (int)0;
	HX_STACK_LINE(33)
	this->fingerHoverTimer = (int)1;
	HX_STACK_LINE(32)
	this->fingerPressTimer = (int)1;
	HX_STACK_LINE(31)
	this->tutTimerTwo = (int)1;
	HX_STACK_LINE(30)
	this->tutTimerOne = (int)1;
	HX_STACK_LINE(44)
	super::__construct(null());
	HX_STACK_LINE(45)
	this->init(tutorialType);
}
;
	return null();
}

//Tutorial_obj::~Tutorial_obj() { }

Dynamic Tutorial_obj::__CreateEmpty() { return  new Tutorial_obj; }
hx::ObjectPtr< Tutorial_obj > Tutorial_obj::__new(Float tutorialType)
{  hx::ObjectPtr< Tutorial_obj > result = new Tutorial_obj();
	result->__construct(tutorialType);
	return result;}

Dynamic Tutorial_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Tutorial_obj > result = new Tutorial_obj();
	result->__construct(inArgs[0]);
	return result;}

Float Tutorial_obj::getCurrentTutSectionNumber( ){
	HX_STACK_FRAME("Tutorial","getCurrentTutSectionNumber",0x623a41ce,"Tutorial.getCurrentTutSectionNumber","Tutorial.hx",50,0x2eb361c0)
	HX_STACK_THIS(this)
	HX_STACK_LINE(50)
	return this->tutSectionNumber;
}


HX_DEFINE_DYNAMIC_FUNC0(Tutorial_obj,getCurrentTutSectionNumber,return )

Void Tutorial_obj::init( Float tutType){
{
		HX_STACK_FRAME("Tutorial","init",0xcc414dc0,"Tutorial.init","Tutorial.hx",53,0x2eb361c0)
		HX_STACK_THIS(this)
		HX_STACK_ARG(tutType,"tutType")
		HX_STACK_LINE(54)
		Array< ::Dynamic > _g = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(54)
		this->tutMessageArray = _g;
		HX_STACK_LINE(55)
		::flixel::group::FlxGroup _g1 = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(55)
		this->tutorialGroup = _g1;
		HX_STACK_LINE(56)
		::flixel::FlxSprite _g2 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(56)
		this->tutHand = _g2;
		HX_STACK_LINE(57)
		::flixel::FlxSprite _g3 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(57)
		this->tutSeed = _g3;
		HX_STACK_LINE(58)
		::flixel::FlxSprite _g4 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g4,"_g4");
		HX_STACK_LINE(58)
		this->tutMessageOne = _g4;
		HX_STACK_LINE(59)
		::flixel::FlxSprite _g5 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g5,"_g5");
		HX_STACK_LINE(59)
		this->tutMessageTwo = _g5;
		HX_STACK_LINE(60)
		::flixel::FlxSprite _g6 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g6,"_g6");
		HX_STACK_LINE(60)
		this->tutMessageThree = _g6;
		HX_STACK_LINE(61)
		::flixel::FlxSprite _g7 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g7,"_g7");
		HX_STACK_LINE(61)
		this->tutMessageFour = _g7;
		HX_STACK_LINE(62)
		::flixel::FlxSprite _g8 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g8,"_g8");
		HX_STACK_LINE(62)
		this->tutMessageFive = _g8;
		HX_STACK_LINE(63)
		::flixel::FlxSprite _g9 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g9,"_g9");
		HX_STACK_LINE(63)
		this->tutMessageSix = _g9;
		HX_STACK_LINE(64)
		::flixel::FlxSprite _g10 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g10,"_g10");
		HX_STACK_LINE(64)
		this->tutMessageSeven = _g10;
		HX_STACK_LINE(65)
		::flixel::FlxSprite _g11 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g11,"_g11");
		HX_STACK_LINE(65)
		this->tutMessageEight = _g11;
		HX_STACK_LINE(66)
		::flixel::FlxSprite _g12 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g12,"_g12");
		HX_STACK_LINE(66)
		this->tutMessageNine = _g12;
		HX_STACK_LINE(67)
		::flixel::FlxSprite _g13 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g13,"_g13");
		HX_STACK_LINE(67)
		this->tutMessageTen = _g13;
		HX_STACK_LINE(68)
		this->tutHand->loadGraphic(HX_CSTRING("assets/images/tutHandAnimation.png"),true,(int)30,(int)28,null(),null());
		HX_STACK_LINE(69)
		this->tutHand->animation->add(HX_CSTRING("handHover"),Array_obj< int >::__new().Add((int)0),(int)1,false);
		HX_STACK_LINE(70)
		this->tutHand->animation->add(HX_CSTRING("handClick"),Array_obj< int >::__new().Add((int)1),(int)1,false);
		HX_STACK_LINE(71)
		this->tutSeed->loadGraphic(HX_CSTRING("assets/images/tree_seed.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(72)
		this->tutMessageOne->loadGraphic(HX_CSTRING("assets/images/tutMessageOne.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(73)
		this->tutMessageTwo->loadGraphic(HX_CSTRING("assets/images/tutMessageTwo.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(74)
		this->tutMessageThree->loadGraphic(HX_CSTRING("assets/images/tutMessageThree.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(75)
		this->tutMessageFour->loadGraphic(HX_CSTRING("assets/images/tutMessageFour.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(76)
		this->tutMessageFive->loadGraphic(HX_CSTRING("assets/images/tutMessageFive.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(77)
		this->tutMessageSix->loadGraphic(HX_CSTRING("assets/images/tutMessageSix.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(78)
		this->tutMessageSeven->loadGraphic(HX_CSTRING("assets/images/tutMessageSeven.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(79)
		this->tutMessageEight->loadGraphic(HX_CSTRING("assets/images/tutMessageEight.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(80)
		this->tutMessageNine->loadGraphic(HX_CSTRING("assets/images/tutMessageNine.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(81)
		this->tutMessageTen->loadGraphic(HX_CSTRING("assets/images/tutMessageTen.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(82)
		this->tutMessageArray[(int)0] = this->tutMessageOne;
		HX_STACK_LINE(83)
		this->tutMessageArray[(int)1] = this->tutMessageTwo;
		HX_STACK_LINE(84)
		this->tutMessageArray[(int)2] = this->tutMessageThree;
		HX_STACK_LINE(85)
		this->tutMessageArray[(int)3] = this->tutMessageFour;
		HX_STACK_LINE(86)
		this->tutMessageArray[(int)4] = this->tutMessageFive;
		HX_STACK_LINE(87)
		this->tutMessageArray[(int)5] = this->tutMessageSix;
		HX_STACK_LINE(88)
		this->tutMessageArray[(int)6] = this->tutMessageSeven;
		HX_STACK_LINE(89)
		this->tutMessageArray[(int)7] = this->tutMessageEight;
		HX_STACK_LINE(90)
		this->tutMessageArray[(int)8] = this->tutMessageNine;
		HX_STACK_LINE(91)
		this->tutMessageArray[(int)9] = this->tutMessageTen;
		HX_STACK_LINE(92)
		this->tutHandPos(tutType);
		HX_STACK_LINE(93)
		this->tutMessagePos();
		HX_STACK_LINE(94)
		this->tutHand->set_alpha((int)0);
		HX_STACK_LINE(95)
		this->tutSeed->set_alpha((int)0);
		HX_STACK_LINE(96)
		this->tutMessageOne->set_alpha((int)0);
		HX_STACK_LINE(97)
		this->tutMessageTwo->set_alpha((int)0);
		HX_STACK_LINE(98)
		this->tutMessageThree->set_alpha((int)0);
		HX_STACK_LINE(99)
		this->tutMessageThree->set_alpha((int)0);
		HX_STACK_LINE(100)
		this->tutMessageFour->set_alpha((int)0);
		HX_STACK_LINE(101)
		this->tutMessageFive->set_alpha((int)0);
		HX_STACK_LINE(102)
		this->tutMessageSix->set_alpha((int)0);
		HX_STACK_LINE(103)
		this->tutMessageSeven->set_alpha((int)0);
		HX_STACK_LINE(104)
		this->tutMessageEight->set_alpha((int)0);
		HX_STACK_LINE(105)
		this->tutMessageNine->set_alpha((int)0);
		HX_STACK_LINE(106)
		this->tutMessageTen->set_alpha((int)0);
		HX_STACK_LINE(107)
		this->eventInit();
		HX_STACK_LINE(108)
		this->tutorialGroup->add(this->tutHand);
		HX_STACK_LINE(109)
		this->tutorialGroup->add(this->tutSeed);
		HX_STACK_LINE(110)
		this->setTutType(tutType);
		HX_STACK_LINE(111)
		this->add(this->tutorialGroup);
		HX_STACK_LINE(112)
		this->tutSectionNumber = tutType;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tutorial_obj,init,(void))

Void Tutorial_obj::getTutTreeGrowthVal( int growthValue){
{
		HX_STACK_FRAME("Tutorial","getTutTreeGrowthVal",0xe27353cf,"Tutorial.getTutTreeGrowthVal","Tutorial.hx",121,0x2eb361c0)
		HX_STACK_THIS(this)
		HX_STACK_ARG(growthValue,"growthValue")
		HX_STACK_LINE(121)
		this->tutTreeGrowthVal = growthValue;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tutorial_obj,getTutTreeGrowthVal,(void))

Void Tutorial_obj::eventInit( ){
{
		HX_STACK_FRAME("Tutorial","eventInit",0xd3d0739a,"Tutorial.eventInit","Tutorial.hx",127,0x2eb361c0)
		HX_STACK_THIS(this)
		HX_STACK_LINE(128)
		::flixel::plugin::MouseEventManager_obj::add(this->tutMessageOne,this->nextTutMessage_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(129)
		::flixel::plugin::MouseEventManager_obj::add(this->tutMessageSix,this->nextTutMessage_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(130)
		::flixel::plugin::MouseEventManager_obj::add(this->tutMessageFour,this->destroyCurrentMessage_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(131)
		::flixel::plugin::MouseEventManager_obj::add(this->tutMessageSeven,this->destroyCurrentMessage_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(132)
		::flixel::plugin::MouseEventManager_obj::add(this->tutMessageEight,this->nextTutMessage_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(133)
		::flixel::plugin::MouseEventManager_obj::add(this->tutMessageNine,this->destroyCurrentMessage_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(134)
		::flixel::plugin::MouseEventManager_obj::add(this->tutMessageTen,this->destroyCurrentMessage_dyn(),null(),null(),null(),null(),null(),null());
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tutorial_obj,eventInit,(void))

Void Tutorial_obj::nextTutMessage( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("Tutorial","nextTutMessage",0x9b6c07b7,"Tutorial.nextTutMessage","Tutorial.hx",137,0x2eb361c0)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(138)
		this->tutMessageArray->__get(this->currentMessageIndex).StaticCast< ::flixel::FlxSprite >()->destroy();
		HX_STACK_LINE(139)
		(this->currentMessageIndex)++;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tutorial_obj,nextTutMessage,(void))

Void Tutorial_obj::setTutType( Float tutType){
{
		HX_STACK_FRAME("Tutorial","setTutType",0x3cd8cb5b,"Tutorial.setTutType","Tutorial.hx",143,0x2eb361c0)
		HX_STACK_THIS(this)
		HX_STACK_ARG(tutType,"tutType")
		HX_STACK_LINE(143)
		if (((tutType == (int)0))){
			HX_STACK_LINE(144)
			this->tutorialGroup->add(this->tutMessageArray->__get((int)0).StaticCast< ::flixel::FlxSprite >());
			HX_STACK_LINE(145)
			this->tutorialGroup->add(this->tutMessageArray->__get((int)1).StaticCast< ::flixel::FlxSprite >());
		}
		else{
			HX_STACK_LINE(147)
			if (((tutType == (int)1))){
				HX_STACK_LINE(148)
				this->currentMessageIndex = (int)2;
				HX_STACK_LINE(149)
				this->tutorialGroup->add(this->tutMessageArray->__get(this->currentMessageIndex).StaticCast< ::flixel::FlxSprite >());
			}
			else{
				HX_STACK_LINE(151)
				if (((tutType == (int)2))){
					HX_STACK_LINE(152)
					this->currentMessageIndex = (int)3;
					HX_STACK_LINE(153)
					this->tutorialGroup->add(this->tutMessageArray->__get(this->currentMessageIndex).StaticCast< ::flixel::FlxSprite >());
				}
				else{
					HX_STACK_LINE(155)
					if (((tutType == (int)3))){
						HX_STACK_LINE(156)
						this->currentMessageIndex = (int)4;
						HX_STACK_LINE(157)
						this->tutorialGroup->add(this->tutMessageArray->__get(this->currentMessageIndex).StaticCast< ::flixel::FlxSprite >());
					}
					else{
						HX_STACK_LINE(159)
						if (((tutType == (int)4))){
							HX_STACK_LINE(160)
							this->currentMessageIndex = (int)5;
							HX_STACK_LINE(161)
							this->tutorialGroup->add(this->tutMessageArray->__get(this->currentMessageIndex).StaticCast< ::flixel::FlxSprite >());
							HX_STACK_LINE(162)
							this->tutorialGroup->add(this->tutMessageArray->__get((this->currentMessageIndex + (int)1)).StaticCast< ::flixel::FlxSprite >());
						}
						else{
							HX_STACK_LINE(164)
							if (((tutType == 1.5))){
								HX_STACK_LINE(165)
								this->currentMessageIndex = (int)7;
								HX_STACK_LINE(166)
								this->tutorialGroup->add(this->tutMessageArray->__get(this->currentMessageIndex).StaticCast< ::flixel::FlxSprite >());
								HX_STACK_LINE(167)
								this->tutorialGroup->add(this->tutMessageArray->__get((this->currentMessageIndex + (int)1)).StaticCast< ::flixel::FlxSprite >());
							}
							else{
								HX_STACK_LINE(169)
								if (((tutType == (int)5))){
									HX_STACK_LINE(170)
									this->currentMessageIndex = (int)9;
									HX_STACK_LINE(171)
									this->tutorialGroup->add(this->tutMessageArray->__get(this->currentMessageIndex).StaticCast< ::flixel::FlxSprite >());
								}
							}
						}
					}
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tutorial_obj,setTutType,(void))

Void Tutorial_obj::destroyCurrentMessage( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("Tutorial","destroyCurrentMessage",0x406a5478,"Tutorial.destroyCurrentMessage","Tutorial.hx",175,0x2eb361c0)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(176)
		if (((this->tutSectionNumber == (int)2))){
			HX_STACK_LINE(177)
			this->tutMessageArray->__get((int)3).StaticCast< ::flixel::FlxSprite >()->destroy();
		}
		HX_STACK_LINE(179)
		if (((this->tutSectionNumber == (int)3))){
			HX_STACK_LINE(180)
			this->tutMessageArray->__get((int)4).StaticCast< ::flixel::FlxSprite >()->destroy();
		}
		HX_STACK_LINE(182)
		if (((this->tutSectionNumber == (int)4))){
			HX_STACK_LINE(183)
			this->tutMessageArray->__get((int)6).StaticCast< ::flixel::FlxSprite >()->destroy();
		}
		HX_STACK_LINE(185)
		if (((this->tutSectionNumber == 1.5))){
			HX_STACK_LINE(186)
			this->tutMessageArray->__get((int)8).StaticCast< ::flixel::FlxSprite >()->destroy();
		}
		HX_STACK_LINE(188)
		if (((this->tutSectionNumber == (int)5))){
			HX_STACK_LINE(189)
			this->tutMessageArray->__get((int)9).StaticCast< ::flixel::FlxSprite >()->destroy();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tutorial_obj,destroyCurrentMessage,(void))

Void Tutorial_obj::tutHandPos( Float tutType){
{
		HX_STACK_FRAME("Tutorial","tutHandPos",0xd021dc82,"Tutorial.tutHandPos","Tutorial.hx",195,0x2eb361c0)
		HX_STACK_THIS(this)
		HX_STACK_ARG(tutType,"tutType")
		HX_STACK_LINE(195)
		if (((tutType == (int)0))){
			HX_STACK_LINE(196)
			this->tutHand->set_x((int)200);
			HX_STACK_LINE(197)
			this->tutHand->set_y((int)200);
		}
		else{
			HX_STACK_LINE(199)
			if (((tutType == (int)1))){
				HX_STACK_LINE(200)
				this->tutHand->set_x((int)210);
				HX_STACK_LINE(201)
				this->tutHand->set_y((int)255);
			}
			else{
				HX_STACK_LINE(203)
				if (((tutType == (int)3))){
					HX_STACK_LINE(204)
					this->tutHand->set_x((int)25);
					HX_STACK_LINE(205)
					this->tutHand->set_y((int)230);
				}
				else{
					HX_STACK_LINE(207)
					if (((tutType == (int)4))){
						HX_STACK_LINE(208)
						this->tutHand->set_x((int)625);
						HX_STACK_LINE(209)
						this->tutHand->set_y((int)50);
					}
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tutorial_obj,tutHandPos,(void))

Void Tutorial_obj::tutMessagePos( ){
{
		HX_STACK_FRAME("Tutorial","tutMessagePos",0xbe2e2310,"Tutorial.tutMessagePos","Tutorial.hx",213,0x2eb361c0)
		HX_STACK_THIS(this)
		HX_STACK_LINE(214)
		this->tutMessageOne->set_x((int)350);
		HX_STACK_LINE(215)
		this->tutMessageOne->set_y((int)20);
		HX_STACK_LINE(216)
		this->tutMessageTwo->set_x((int)350);
		HX_STACK_LINE(217)
		this->tutMessageTwo->set_y((int)20);
		HX_STACK_LINE(218)
		this->tutMessageThree->set_x((int)350);
		HX_STACK_LINE(219)
		this->tutMessageThree->set_y((int)20);
		HX_STACK_LINE(220)
		this->tutMessageFour->set_x((int)350);
		HX_STACK_LINE(221)
		this->tutMessageFour->set_y((int)20);
		HX_STACK_LINE(222)
		this->tutMessageFive->set_x((int)350);
		HX_STACK_LINE(223)
		this->tutMessageFive->set_y((int)20);
		HX_STACK_LINE(224)
		this->tutMessageSix->set_x((int)350);
		HX_STACK_LINE(225)
		this->tutMessageSix->set_y((int)20);
		HX_STACK_LINE(226)
		this->tutMessageSeven->set_x((int)350);
		HX_STACK_LINE(227)
		this->tutMessageSeven->set_y((int)20);
		HX_STACK_LINE(228)
		this->tutMessageEight->set_x((int)350);
		HX_STACK_LINE(229)
		this->tutMessageEight->set_y((int)20);
		HX_STACK_LINE(230)
		this->tutMessageNine->set_x((int)350);
		HX_STACK_LINE(231)
		this->tutMessageNine->set_y((int)20);
		HX_STACK_LINE(232)
		this->tutMessageTen->set_x((int)350);
		HX_STACK_LINE(233)
		this->tutMessageTen->set_y((int)20);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tutorial_obj,tutMessagePos,(void))

Void Tutorial_obj::tutorialRun( ){
{
		HX_STACK_FRAME("Tutorial","tutorialRun",0x1f8ac99d,"Tutorial.tutorialRun","Tutorial.hx",236,0x2eb361c0)
		HX_STACK_THIS(this)
		HX_STACK_LINE(237)
		this->messageFade(this->messageFadeDirection);
		HX_STACK_LINE(238)
		if (((this->tutSectionNumber == (int)0))){
			HX_STACK_LINE(239)
			hx::SubEq(this->tutTimerOne,::flixel::FlxG_obj::elapsed);
			HX_STACK_LINE(240)
			if (((bool((this->tutTimerOne <= (int)0)) && bool((this->tutMessageTwo->alpha >= (int)1))))){
				HX_STACK_LINE(241)
				this->tutHandFade((int)0);
			}
			HX_STACK_LINE(243)
			if ((this->handMovingToTile)){
				HX_STACK_LINE(244)
				this->grabSeed();
			}
		}
		HX_STACK_LINE(247)
		if (((this->tutSectionNumber == (int)1))){
			HX_STACK_LINE(248)
			hx::SubEq(this->tutTimerTwo,::flixel::FlxG_obj::elapsed);
			HX_STACK_LINE(249)
			if (((this->tutTimerTwo <= (int)0))){
				HX_STACK_LINE(250)
				this->tutHandFade((int)0);
			}
		}
		HX_STACK_LINE(253)
		if (((this->tutSectionNumber == (int)2))){
			HX_STACK_LINE(254)
			this->currentMessageIndex = (int)3;
		}
		HX_STACK_LINE(256)
		if (((this->tutSectionNumber == (int)3))){
			HX_STACK_LINE(257)
			this->currentMessageIndex = (int)4;
			HX_STACK_LINE(258)
			this->handClickTruckButton();
		}
		HX_STACK_LINE(260)
		if (((bool((this->tutSectionNumber == (int)4)) && bool((this->currentMessageIndex == (int)6))))){
			HX_STACK_LINE(261)
			this->handClickShopTab();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tutorial_obj,tutorialRun,(void))

Void Tutorial_obj::grabSeed( ){
{
		HX_STACK_FRAME("Tutorial","grabSeed",0xc364820d,"Tutorial.grabSeed","Tutorial.hx",265,0x2eb361c0)
		HX_STACK_THIS(this)
		HX_STACK_LINE(266)
		this->tutSeed->set_x((this->tutHand->x - (int)8));
		HX_STACK_LINE(267)
		this->tutSeed->set_y((this->tutHand->y - (int)6));
		HX_STACK_LINE(268)
		this->tutSeed->set_alpha(.5);
		HX_STACK_LINE(269)
		this->handMovingToTile = true;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tutorial_obj,grabSeed,(void))

Void Tutorial_obj::tutHandFade( int fadeDirection){
{
		HX_STACK_FRAME("Tutorial","tutHandFade",0x46d846ce,"Tutorial.tutHandFade","Tutorial.hx",273,0x2eb361c0)
		HX_STACK_THIS(this)
		HX_STACK_ARG(fadeDirection,"fadeDirection")
		HX_STACK_LINE(273)
		if (((fadeDirection == (int)0))){
			HX_STACK_LINE(274)
			if (((this->tutHand->alpha < (int)1))){
				HX_STACK_LINE(275)
				::flixel::FlxSprite _g = this->tutHand;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(275)
				_g->set_alpha((_g->alpha + .01));
			}
		}
		else{
			HX_STACK_LINE(279)
			if (((this->tutHand->alpha > (int)0))){
				HX_STACK_LINE(280)
				::flixel::FlxSprite _g = this->tutHand;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(280)
				_g->set_alpha((_g->alpha - .06));
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tutorial_obj,tutHandFade,(void))

Void Tutorial_obj::messageFade( int messageFadeDirection){
{
		HX_STACK_FRAME("Tutorial","messageFade",0x4b8aaa33,"Tutorial.messageFade","Tutorial.hx",286,0x2eb361c0)
		HX_STACK_THIS(this)
		HX_STACK_ARG(messageFadeDirection,"messageFadeDirection")
		HX_STACK_LINE(286)
		if (((messageFadeDirection == (int)0))){
			HX_STACK_LINE(287)
			if (((this->tutMessageArray->__get(this->currentMessageIndex).StaticCast< ::flixel::FlxSprite >()->alpha < (int)1))){
				HX_STACK_LINE(288)
				::flixel::FlxSprite _g = this->tutMessageArray->__get(this->currentMessageIndex).StaticCast< ::flixel::FlxSprite >();		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(288)
				_g->set_alpha((_g->alpha + .01));
			}
		}
		else{
			HX_STACK_LINE(292)
			if (((this->tutMessageArray->__get(this->currentMessageIndex).StaticCast< ::flixel::FlxSprite >()->alpha > (int)0))){
				HX_STACK_LINE(293)
				::flixel::FlxSprite _g = this->tutMessageArray->__get(this->currentMessageIndex).StaticCast< ::flixel::FlxSprite >();		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(293)
				_g->set_alpha((_g->alpha - .03));
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tutorial_obj,messageFade,(void))

Void Tutorial_obj::handClickTruckButton( ){
{
		HX_STACK_FRAME("Tutorial","handClickTruckButton",0x5a268bc8,"Tutorial.handClickTruckButton","Tutorial.hx",298,0x2eb361c0)
		HX_STACK_THIS(this)
		HX_STACK_LINE(299)
		this->tutHandFade((int)0);
		HX_STACK_LINE(300)
		if (((this->tutHand->alpha >= (int)1))){
			HX_STACK_LINE(301)
			if (((this->tutHand->x <= (int)60))){
				HX_STACK_LINE(302)
				::flixel::FlxSprite _g = this->tutHand;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(302)
				_g->set_x((_g->x + (int)1));
			}
			HX_STACK_LINE(304)
			if (((this->tutHand->x >= (int)60))){
				HX_STACK_LINE(305)
				hx::SubEq(this->fingerHoverTimer,::flixel::FlxG_obj::elapsed);
				HX_STACK_LINE(306)
				if (((this->fingerHoverTimer <= (int)0))){
					HX_STACK_LINE(307)
					hx::SubEq(this->fingerPressTimer,::flixel::FlxG_obj::elapsed);
					HX_STACK_LINE(308)
					this->tutHand->animation->play(HX_CSTRING("handClick"),null(),null());
					HX_STACK_LINE(309)
					if (((this->fingerPressTimer <= (int)0))){
						HX_STACK_LINE(310)
						this->tutHand->animation->play(HX_CSTRING("handHover"),null(),null());
						HX_STACK_LINE(311)
						this->fingerPressTimer = (int)1;
						HX_STACK_LINE(312)
						this->fingerHoverTimer = (int)1;
						HX_STACK_LINE(313)
						this->tutHand->set_x((int)25);
					}
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tutorial_obj,handClickTruckButton,(void))

Void Tutorial_obj::handClickShopTab( ){
{
		HX_STACK_FRAME("Tutorial","handClickShopTab",0x2a833c56,"Tutorial.handClickShopTab","Tutorial.hx",320,0x2eb361c0)
		HX_STACK_THIS(this)
		HX_STACK_LINE(321)
		this->tutHandFade((int)0);
		HX_STACK_LINE(322)
		if (((bool((this->tutHand->alpha >= (int)1)) && bool((this->currentMessageIndex == (int)6))))){
			HX_STACK_LINE(323)
			if (((this->tutHand->y >= (int)15))){
				HX_STACK_LINE(324)
				::flixel::FlxSprite _g = this->tutHand;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(324)
				_g->set_y((_g->y - (int)1));
			}
			HX_STACK_LINE(326)
			if (((this->tutHand->y <= (int)15))){
				HX_STACK_LINE(327)
				hx::SubEq(this->fingerHoverTimer,::flixel::FlxG_obj::elapsed);
				HX_STACK_LINE(328)
				if (((this->fingerHoverTimer <= (int)0))){
					HX_STACK_LINE(329)
					hx::SubEq(this->fingerPressTimer,::flixel::FlxG_obj::elapsed);
					HX_STACK_LINE(330)
					this->tutHand->animation->play(HX_CSTRING("handClick"),null(),null());
					HX_STACK_LINE(331)
					if (((this->fingerPressTimer <= (int)0))){
						HX_STACK_LINE(332)
						this->tutHand->animation->play(HX_CSTRING("handHover"),null(),null());
						HX_STACK_LINE(333)
						this->fingerPressTimer = (int)1;
						HX_STACK_LINE(334)
						this->fingerHoverTimer = (int)1;
						HX_STACK_LINE(335)
						this->tutHand->set_y((int)50);
					}
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tutorial_obj,handClickShopTab,(void))

Void Tutorial_obj::moveToSeed( ){
{
		HX_STACK_FRAME("Tutorial","moveToSeed",0x1a59a12d,"Tutorial.moveToSeed","Tutorial.hx",344,0x2eb361c0)
		HX_STACK_THIS(this)
		HX_STACK_LINE(344)
		if (((bool((bool((this->tutHand->alpha >= (int)1)) && bool((this->tutMessageTwo->alpha >= (int)1)))) && bool((this->tutSectionNumber == (int)0))))){
			HX_STACK_LINE(345)
			if (((bool((this->tutHand->x >= (int)37)) && bool(!(this->handMovingToTile))))){
				HX_STACK_LINE(346)
				{
					HX_STACK_LINE(346)
					::flixel::FlxSprite _g = this->tutHand;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(346)
					_g->set_x((_g->x - 1.5));
				}
				HX_STACK_LINE(347)
				this->tutHand->animation->play(HX_CSTRING("handHover"),null(),null());
			}
			HX_STACK_LINE(349)
			if (((bool((this->tutHand->y >= (int)85)) && bool(!(this->handMovingToTile))))){
				HX_STACK_LINE(350)
				::flixel::FlxSprite _g = this->tutHand;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(350)
				_g->set_y((_g->y - 1.5));
			}
			HX_STACK_LINE(352)
			if (((bool((this->tutHand->x <= (int)37)) && bool((this->tutHand->y <= (int)85))))){
				HX_STACK_LINE(353)
				this->handMovingToTile = true;
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tutorial_obj,moveToSeed,(void))

Void Tutorial_obj::moveToTile( ){
{
		HX_STACK_FRAME("Tutorial","moveToTile",0x1b05e6ea,"Tutorial.moveToTile","Tutorial.hx",359,0x2eb361c0)
		HX_STACK_THIS(this)
		HX_STACK_LINE(359)
		if (((this->tutSectionNumber == (int)0))){
			HX_STACK_LINE(361)
			if (((bool((this->tutHand->x <= (int)200)) && bool(this->handMovingToTile)))){
				HX_STACK_LINE(362)
				{
					HX_STACK_LINE(362)
					::flixel::FlxSprite _g = this->tutHand;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(362)
					_g->set_x((_g->x + 1.3));
				}
				HX_STACK_LINE(363)
				this->tutHand->animation->play(HX_CSTRING("handClick"),null(),null());
			}
			HX_STACK_LINE(365)
			if (((bool((this->tutHand->y <= (int)200)) && bool(this->handMovingToTile)))){
				HX_STACK_LINE(366)
				::flixel::FlxSprite _g = this->tutHand;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(366)
				_g->set_y((_g->y + 1.3));
			}
			HX_STACK_LINE(368)
			if (((bool((bool((this->tutHand->x >= (int)200)) && bool((this->tutHand->y >= (int)200)))) && bool(this->handMovingToTile)))){
				HX_STACK_LINE(369)
				this->tutSeed->set_alpha((int)0);
				HX_STACK_LINE(370)
				this->handMovingToTile = false;
				HX_STACK_LINE(371)
				this->tutTimerOne = (int)2;
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tutorial_obj,moveToTile,(void))

Void Tutorial_obj::moveToDropAlert( ){
{
		HX_STACK_FRAME("Tutorial","moveToDropAlert",0xe2735f71,"Tutorial.moveToDropAlert","Tutorial.hx",377,0x2eb361c0)
		HX_STACK_THIS(this)
		HX_STACK_LINE(377)
		if (((bool((this->tutHand->alpha >= (int)1)) && bool((this->tutSectionNumber == (int)1))))){
			HX_STACK_LINE(378)
			if (((this->tutHand->y >= (int)230))){
				HX_STACK_LINE(379)
				::flixel::FlxSprite _g = this->tutHand;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(379)
				_g->set_y((_g->y - (int)1));
			}
			HX_STACK_LINE(381)
			if (((this->tutHand->y <= (int)230))){
				HX_STACK_LINE(382)
				hx::SubEq(this->fingerHoverTimer,::flixel::FlxG_obj::elapsed);
				HX_STACK_LINE(383)
				if (((this->fingerHoverTimer <= (int)0))){
					HX_STACK_LINE(384)
					hx::SubEq(this->fingerPressTimer,::flixel::FlxG_obj::elapsed);
					HX_STACK_LINE(385)
					this->tutHand->animation->play(HX_CSTRING("handClick"),null(),null());
					HX_STACK_LINE(386)
					if (((this->fingerPressTimer <= (int)0))){
						HX_STACK_LINE(387)
						this->tutHand->animation->play(HX_CSTRING("handHover"),null(),null());
						HX_STACK_LINE(388)
						this->fingerPressTimer = (int)1;
						HX_STACK_LINE(389)
						this->fingerHoverTimer = (int)1;
						HX_STACK_LINE(390)
						this->tutHand->set_y((int)255);
					}
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tutorial_obj,moveToDropAlert,(void))

Void Tutorial_obj::update( ){
{
		HX_STACK_FRAME("Tutorial","update",0x4939a4b9,"Tutorial.update","Tutorial.hx",397,0x2eb361c0)
		HX_STACK_THIS(this)
		HX_STACK_LINE(398)
		this->tutorialRun();
		HX_STACK_LINE(399)
		this->moveToSeed();
		HX_STACK_LINE(400)
		this->moveToTile();
		HX_STACK_LINE(401)
		this->moveToDropAlert();
		HX_STACK_LINE(402)
		this->super::update();
	}
return null();
}


Void Tutorial_obj::destroy( ){
{
		HX_STACK_FRAME("Tutorial","destroy",0x4fece84a,"Tutorial.destroy","Tutorial.hx",405,0x2eb361c0)
		HX_STACK_THIS(this)
		HX_STACK_LINE(406)
		this->destroy_dyn();
		HX_STACK_LINE(407)
		this->super::destroy();
	}
return null();
}



Tutorial_obj::Tutorial_obj()
{
}

void Tutorial_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Tutorial);
	HX_MARK_MEMBER_NAME(tutMessageArray,"tutMessageArray");
	HX_MARK_MEMBER_NAME(tutorialGroup,"tutorialGroup");
	HX_MARK_MEMBER_NAME(tutHand,"tutHand");
	HX_MARK_MEMBER_NAME(tutSeed,"tutSeed");
	HX_MARK_MEMBER_NAME(tutMessageOne,"tutMessageOne");
	HX_MARK_MEMBER_NAME(tutMessageTwo,"tutMessageTwo");
	HX_MARK_MEMBER_NAME(tutMessageThree,"tutMessageThree");
	HX_MARK_MEMBER_NAME(tutMessageFour,"tutMessageFour");
	HX_MARK_MEMBER_NAME(tutMessageFive,"tutMessageFive");
	HX_MARK_MEMBER_NAME(tutMessageSix,"tutMessageSix");
	HX_MARK_MEMBER_NAME(tutMessageSeven,"tutMessageSeven");
	HX_MARK_MEMBER_NAME(tutMessageEight,"tutMessageEight");
	HX_MARK_MEMBER_NAME(tutMessageNine,"tutMessageNine");
	HX_MARK_MEMBER_NAME(tutMessageTen,"tutMessageTen");
	HX_MARK_MEMBER_NAME(tutTeacher,"tutTeacher");
	HX_MARK_MEMBER_NAME(tutTimerOne,"tutTimerOne");
	HX_MARK_MEMBER_NAME(tutTimerTwo,"tutTimerTwo");
	HX_MARK_MEMBER_NAME(fingerPressTimer,"fingerPressTimer");
	HX_MARK_MEMBER_NAME(fingerHoverTimer,"fingerHoverTimer");
	HX_MARK_MEMBER_NAME(tutSectionNumber,"tutSectionNumber");
	HX_MARK_MEMBER_NAME(messageFadeDirection,"messageFadeDirection");
	HX_MARK_MEMBER_NAME(currentMessageIndex,"currentMessageIndex");
	HX_MARK_MEMBER_NAME(tutTreeGrowthVal,"tutTreeGrowthVal");
	HX_MARK_MEMBER_NAME(handMovingToSeed,"handMovingToSeed");
	HX_MARK_MEMBER_NAME(moveHandWithSeed,"moveHandWithSeed");
	HX_MARK_MEMBER_NAME(handMovingToTile,"handMovingToTile");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void Tutorial_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(tutMessageArray,"tutMessageArray");
	HX_VISIT_MEMBER_NAME(tutorialGroup,"tutorialGroup");
	HX_VISIT_MEMBER_NAME(tutHand,"tutHand");
	HX_VISIT_MEMBER_NAME(tutSeed,"tutSeed");
	HX_VISIT_MEMBER_NAME(tutMessageOne,"tutMessageOne");
	HX_VISIT_MEMBER_NAME(tutMessageTwo,"tutMessageTwo");
	HX_VISIT_MEMBER_NAME(tutMessageThree,"tutMessageThree");
	HX_VISIT_MEMBER_NAME(tutMessageFour,"tutMessageFour");
	HX_VISIT_MEMBER_NAME(tutMessageFive,"tutMessageFive");
	HX_VISIT_MEMBER_NAME(tutMessageSix,"tutMessageSix");
	HX_VISIT_MEMBER_NAME(tutMessageSeven,"tutMessageSeven");
	HX_VISIT_MEMBER_NAME(tutMessageEight,"tutMessageEight");
	HX_VISIT_MEMBER_NAME(tutMessageNine,"tutMessageNine");
	HX_VISIT_MEMBER_NAME(tutMessageTen,"tutMessageTen");
	HX_VISIT_MEMBER_NAME(tutTeacher,"tutTeacher");
	HX_VISIT_MEMBER_NAME(tutTimerOne,"tutTimerOne");
	HX_VISIT_MEMBER_NAME(tutTimerTwo,"tutTimerTwo");
	HX_VISIT_MEMBER_NAME(fingerPressTimer,"fingerPressTimer");
	HX_VISIT_MEMBER_NAME(fingerHoverTimer,"fingerHoverTimer");
	HX_VISIT_MEMBER_NAME(tutSectionNumber,"tutSectionNumber");
	HX_VISIT_MEMBER_NAME(messageFadeDirection,"messageFadeDirection");
	HX_VISIT_MEMBER_NAME(currentMessageIndex,"currentMessageIndex");
	HX_VISIT_MEMBER_NAME(tutTreeGrowthVal,"tutTreeGrowthVal");
	HX_VISIT_MEMBER_NAME(handMovingToSeed,"handMovingToSeed");
	HX_VISIT_MEMBER_NAME(moveHandWithSeed,"moveHandWithSeed");
	HX_VISIT_MEMBER_NAME(handMovingToTile,"handMovingToTile");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic Tutorial_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"init") ) { return init_dyn(); }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"tutHand") ) { return tutHand; }
		if (HX_FIELD_EQ(inName,"tutSeed") ) { return tutSeed; }
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"grabSeed") ) { return grabSeed_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"eventInit") ) { return eventInit_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"tutTeacher") ) { return tutTeacher; }
		if (HX_FIELD_EQ(inName,"setTutType") ) { return setTutType_dyn(); }
		if (HX_FIELD_EQ(inName,"tutHandPos") ) { return tutHandPos_dyn(); }
		if (HX_FIELD_EQ(inName,"moveToSeed") ) { return moveToSeed_dyn(); }
		if (HX_FIELD_EQ(inName,"moveToTile") ) { return moveToTile_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"tutTimerOne") ) { return tutTimerOne; }
		if (HX_FIELD_EQ(inName,"tutTimerTwo") ) { return tutTimerTwo; }
		if (HX_FIELD_EQ(inName,"tutorialRun") ) { return tutorialRun_dyn(); }
		if (HX_FIELD_EQ(inName,"tutHandFade") ) { return tutHandFade_dyn(); }
		if (HX_FIELD_EQ(inName,"messageFade") ) { return messageFade_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"tutorialGroup") ) { return tutorialGroup; }
		if (HX_FIELD_EQ(inName,"tutMessageOne") ) { return tutMessageOne; }
		if (HX_FIELD_EQ(inName,"tutMessageTwo") ) { return tutMessageTwo; }
		if (HX_FIELD_EQ(inName,"tutMessageSix") ) { return tutMessageSix; }
		if (HX_FIELD_EQ(inName,"tutMessageTen") ) { return tutMessageTen; }
		if (HX_FIELD_EQ(inName,"tutMessagePos") ) { return tutMessagePos_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"tutMessageFour") ) { return tutMessageFour; }
		if (HX_FIELD_EQ(inName,"tutMessageFive") ) { return tutMessageFive; }
		if (HX_FIELD_EQ(inName,"tutMessageNine") ) { return tutMessageNine; }
		if (HX_FIELD_EQ(inName,"nextTutMessage") ) { return nextTutMessage_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"tutMessageArray") ) { return tutMessageArray; }
		if (HX_FIELD_EQ(inName,"tutMessageThree") ) { return tutMessageThree; }
		if (HX_FIELD_EQ(inName,"tutMessageSeven") ) { return tutMessageSeven; }
		if (HX_FIELD_EQ(inName,"tutMessageEight") ) { return tutMessageEight; }
		if (HX_FIELD_EQ(inName,"moveToDropAlert") ) { return moveToDropAlert_dyn(); }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"fingerPressTimer") ) { return fingerPressTimer; }
		if (HX_FIELD_EQ(inName,"fingerHoverTimer") ) { return fingerHoverTimer; }
		if (HX_FIELD_EQ(inName,"tutSectionNumber") ) { return tutSectionNumber; }
		if (HX_FIELD_EQ(inName,"tutTreeGrowthVal") ) { return tutTreeGrowthVal; }
		if (HX_FIELD_EQ(inName,"handMovingToSeed") ) { return handMovingToSeed; }
		if (HX_FIELD_EQ(inName,"moveHandWithSeed") ) { return moveHandWithSeed; }
		if (HX_FIELD_EQ(inName,"handMovingToTile") ) { return handMovingToTile; }
		if (HX_FIELD_EQ(inName,"handClickShopTab") ) { return handClickShopTab_dyn(); }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"currentMessageIndex") ) { return currentMessageIndex; }
		if (HX_FIELD_EQ(inName,"getTutTreeGrowthVal") ) { return getTutTreeGrowthVal_dyn(); }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"messageFadeDirection") ) { return messageFadeDirection; }
		if (HX_FIELD_EQ(inName,"handClickTruckButton") ) { return handClickTruckButton_dyn(); }
		break;
	case 21:
		if (HX_FIELD_EQ(inName,"destroyCurrentMessage") ) { return destroyCurrentMessage_dyn(); }
		break;
	case 26:
		if (HX_FIELD_EQ(inName,"getCurrentTutSectionNumber") ) { return getCurrentTutSectionNumber_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Tutorial_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 7:
		if (HX_FIELD_EQ(inName,"tutHand") ) { tutHand=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tutSeed") ) { tutSeed=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"tutTeacher") ) { tutTeacher=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"tutTimerOne") ) { tutTimerOne=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tutTimerTwo") ) { tutTimerTwo=inValue.Cast< Float >(); return inValue; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"tutorialGroup") ) { tutorialGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tutMessageOne") ) { tutMessageOne=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tutMessageTwo") ) { tutMessageTwo=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tutMessageSix") ) { tutMessageSix=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tutMessageTen") ) { tutMessageTen=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"tutMessageFour") ) { tutMessageFour=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tutMessageFive") ) { tutMessageFive=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tutMessageNine") ) { tutMessageNine=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"tutMessageArray") ) { tutMessageArray=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tutMessageThree") ) { tutMessageThree=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tutMessageSeven") ) { tutMessageSeven=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tutMessageEight") ) { tutMessageEight=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"fingerPressTimer") ) { fingerPressTimer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"fingerHoverTimer") ) { fingerHoverTimer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tutSectionNumber") ) { tutSectionNumber=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tutTreeGrowthVal") ) { tutTreeGrowthVal=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"handMovingToSeed") ) { handMovingToSeed=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"moveHandWithSeed") ) { moveHandWithSeed=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"handMovingToTile") ) { handMovingToTile=inValue.Cast< bool >(); return inValue; }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"currentMessageIndex") ) { currentMessageIndex=inValue.Cast< int >(); return inValue; }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"messageFadeDirection") ) { messageFadeDirection=inValue.Cast< int >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Tutorial_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("tutMessageArray"));
	outFields->push(HX_CSTRING("tutorialGroup"));
	outFields->push(HX_CSTRING("tutHand"));
	outFields->push(HX_CSTRING("tutSeed"));
	outFields->push(HX_CSTRING("tutMessageOne"));
	outFields->push(HX_CSTRING("tutMessageTwo"));
	outFields->push(HX_CSTRING("tutMessageThree"));
	outFields->push(HX_CSTRING("tutMessageFour"));
	outFields->push(HX_CSTRING("tutMessageFive"));
	outFields->push(HX_CSTRING("tutMessageSix"));
	outFields->push(HX_CSTRING("tutMessageSeven"));
	outFields->push(HX_CSTRING("tutMessageEight"));
	outFields->push(HX_CSTRING("tutMessageNine"));
	outFields->push(HX_CSTRING("tutMessageTen"));
	outFields->push(HX_CSTRING("tutTeacher"));
	outFields->push(HX_CSTRING("tutTimerOne"));
	outFields->push(HX_CSTRING("tutTimerTwo"));
	outFields->push(HX_CSTRING("fingerPressTimer"));
	outFields->push(HX_CSTRING("fingerHoverTimer"));
	outFields->push(HX_CSTRING("tutSectionNumber"));
	outFields->push(HX_CSTRING("messageFadeDirection"));
	outFields->push(HX_CSTRING("currentMessageIndex"));
	outFields->push(HX_CSTRING("tutTreeGrowthVal"));
	outFields->push(HX_CSTRING("handMovingToSeed"));
	outFields->push(HX_CSTRING("moveHandWithSeed"));
	outFields->push(HX_CSTRING("handMovingToTile"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(Tutorial_obj,tutMessageArray),HX_CSTRING("tutMessageArray")},
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(Tutorial_obj,tutorialGroup),HX_CSTRING("tutorialGroup")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Tutorial_obj,tutHand),HX_CSTRING("tutHand")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Tutorial_obj,tutSeed),HX_CSTRING("tutSeed")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Tutorial_obj,tutMessageOne),HX_CSTRING("tutMessageOne")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Tutorial_obj,tutMessageTwo),HX_CSTRING("tutMessageTwo")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Tutorial_obj,tutMessageThree),HX_CSTRING("tutMessageThree")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Tutorial_obj,tutMessageFour),HX_CSTRING("tutMessageFour")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Tutorial_obj,tutMessageFive),HX_CSTRING("tutMessageFive")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Tutorial_obj,tutMessageSix),HX_CSTRING("tutMessageSix")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Tutorial_obj,tutMessageSeven),HX_CSTRING("tutMessageSeven")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Tutorial_obj,tutMessageEight),HX_CSTRING("tutMessageEight")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Tutorial_obj,tutMessageNine),HX_CSTRING("tutMessageNine")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Tutorial_obj,tutMessageTen),HX_CSTRING("tutMessageTen")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Tutorial_obj,tutTeacher),HX_CSTRING("tutTeacher")},
	{hx::fsFloat,(int)offsetof(Tutorial_obj,tutTimerOne),HX_CSTRING("tutTimerOne")},
	{hx::fsFloat,(int)offsetof(Tutorial_obj,tutTimerTwo),HX_CSTRING("tutTimerTwo")},
	{hx::fsFloat,(int)offsetof(Tutorial_obj,fingerPressTimer),HX_CSTRING("fingerPressTimer")},
	{hx::fsFloat,(int)offsetof(Tutorial_obj,fingerHoverTimer),HX_CSTRING("fingerHoverTimer")},
	{hx::fsFloat,(int)offsetof(Tutorial_obj,tutSectionNumber),HX_CSTRING("tutSectionNumber")},
	{hx::fsInt,(int)offsetof(Tutorial_obj,messageFadeDirection),HX_CSTRING("messageFadeDirection")},
	{hx::fsInt,(int)offsetof(Tutorial_obj,currentMessageIndex),HX_CSTRING("currentMessageIndex")},
	{hx::fsInt,(int)offsetof(Tutorial_obj,tutTreeGrowthVal),HX_CSTRING("tutTreeGrowthVal")},
	{hx::fsBool,(int)offsetof(Tutorial_obj,handMovingToSeed),HX_CSTRING("handMovingToSeed")},
	{hx::fsBool,(int)offsetof(Tutorial_obj,moveHandWithSeed),HX_CSTRING("moveHandWithSeed")},
	{hx::fsBool,(int)offsetof(Tutorial_obj,handMovingToTile),HX_CSTRING("handMovingToTile")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("tutMessageArray"),
	HX_CSTRING("tutorialGroup"),
	HX_CSTRING("tutHand"),
	HX_CSTRING("tutSeed"),
	HX_CSTRING("tutMessageOne"),
	HX_CSTRING("tutMessageTwo"),
	HX_CSTRING("tutMessageThree"),
	HX_CSTRING("tutMessageFour"),
	HX_CSTRING("tutMessageFive"),
	HX_CSTRING("tutMessageSix"),
	HX_CSTRING("tutMessageSeven"),
	HX_CSTRING("tutMessageEight"),
	HX_CSTRING("tutMessageNine"),
	HX_CSTRING("tutMessageTen"),
	HX_CSTRING("tutTeacher"),
	HX_CSTRING("tutTimerOne"),
	HX_CSTRING("tutTimerTwo"),
	HX_CSTRING("fingerPressTimer"),
	HX_CSTRING("fingerHoverTimer"),
	HX_CSTRING("tutSectionNumber"),
	HX_CSTRING("messageFadeDirection"),
	HX_CSTRING("currentMessageIndex"),
	HX_CSTRING("tutTreeGrowthVal"),
	HX_CSTRING("handMovingToSeed"),
	HX_CSTRING("moveHandWithSeed"),
	HX_CSTRING("handMovingToTile"),
	HX_CSTRING("getCurrentTutSectionNumber"),
	HX_CSTRING("init"),
	HX_CSTRING("getTutTreeGrowthVal"),
	HX_CSTRING("eventInit"),
	HX_CSTRING("nextTutMessage"),
	HX_CSTRING("setTutType"),
	HX_CSTRING("destroyCurrentMessage"),
	HX_CSTRING("tutHandPos"),
	HX_CSTRING("tutMessagePos"),
	HX_CSTRING("tutorialRun"),
	HX_CSTRING("grabSeed"),
	HX_CSTRING("tutHandFade"),
	HX_CSTRING("messageFade"),
	HX_CSTRING("handClickTruckButton"),
	HX_CSTRING("handClickShopTab"),
	HX_CSTRING("moveToSeed"),
	HX_CSTRING("moveToTile"),
	HX_CSTRING("moveToDropAlert"),
	HX_CSTRING("update"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Tutorial_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Tutorial_obj::__mClass,"__mClass");
};

#endif

Class Tutorial_obj::__mClass;

void Tutorial_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("Tutorial"), hx::TCanCast< Tutorial_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void Tutorial_obj::__boot()
{
}

