#include <hxcpp.h>

#ifndef INCLUDED_PieIcon
#include <PieIcon.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_input_mouse_FlxMouse
#include <flixel/input/mouse/FlxMouse.h>
#endif
#ifndef INCLUDED_flixel_input_mouse_FlxMouseButton
#include <flixel/input/mouse/FlxMouseButton.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxInput
#include <flixel/interfaces/IFlxInput.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxPooled
#include <flixel/interfaces/IFlxPooled.h>
#endif
#ifndef INCLUDED_flixel_plugin_FlxPlugin
#include <flixel/plugin/FlxPlugin.h>
#endif
#ifndef INCLUDED_flixel_plugin_MouseEventManager
#include <flixel/plugin/MouseEventManager.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPoint
#include <flixel/util/FlxPoint.h>
#endif

Void PieIcon_obj::__construct(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y)
{
HX_STACK_FRAME("PieIcon","new",0x97e71137,"PieIcon.new","PieIcon.hx",11,0xd0774219)
HX_STACK_THIS(this)
HX_STACK_ARG(__o_X,"X")
HX_STACK_ARG(__o_Y,"Y")
Float X = __o_X.Default(0);
Float Y = __o_Y.Default(0);
{
	HX_STACK_LINE(16)
	this->homeY = (int)78;
	HX_STACK_LINE(15)
	this->homeX = (int)85;
	HX_STACK_LINE(14)
	this->dragging = false;
	HX_STACK_LINE(20)
	super::__construct(X,Y,null());
	HX_STACK_LINE(22)
	this->loadGraphic(HX_CSTRING("assets/images/pieIcon.png"),false,(int)24,(int)13,null(),null());
	HX_STACK_LINE(23)
	this->set_x(this->homeX);
	HX_STACK_LINE(24)
	this->set_y(this->homeY);
	HX_STACK_LINE(25)
	::flixel::plugin::MouseEventManager_obj::add(hx::ObjectPtr<OBJ_>(this),this->onDown_dyn(),this->onUp_dyn(),null(),null(),null(),null(),null());
}
;
	return null();
}

//PieIcon_obj::~PieIcon_obj() { }

Dynamic PieIcon_obj::__CreateEmpty() { return  new PieIcon_obj; }
hx::ObjectPtr< PieIcon_obj > PieIcon_obj::__new(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y)
{  hx::ObjectPtr< PieIcon_obj > result = new PieIcon_obj();
	result->__construct(__o_X,__o_Y);
	return result;}

Dynamic PieIcon_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< PieIcon_obj > result = new PieIcon_obj();
	result->__construct(inArgs[0],inArgs[1]);
	return result;}

Void PieIcon_obj::onDown( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("PieIcon","onDown",0xe709a0aa,"PieIcon.onDown","PieIcon.hx",31,0xd0774219)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(31)
		this->dragging = true;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PieIcon_obj,onDown,(void))

Void PieIcon_obj::onUp( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("PieIcon","onUp",0x52f7eca3,"PieIcon.onUp","PieIcon.hx",35,0xd0774219)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(37)
		this->dragging = false;
		HX_STACK_LINE(38)
		this->resetSeedPos();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PieIcon_obj,onUp,(void))

Void PieIcon_obj::dragSeed( bool temp){
{
		HX_STACK_FRAME("PieIcon","dragSeed",0x0bb03b4e,"PieIcon.dragSeed","PieIcon.hx",44,0xd0774219)
		HX_STACK_THIS(this)
		HX_STACK_ARG(temp,"temp")
		HX_STACK_LINE(44)
		if ((temp)){
			HX_STACK_LINE(46)
			this->set_x((::flixel::FlxG_obj::mouse->x - (int)8));
			HX_STACK_LINE(47)
			this->set_y((::flixel::FlxG_obj::mouse->y - (int)14));
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PieIcon_obj,dragSeed,(void))

Void PieIcon_obj::resetSeedPos( ){
{
		HX_STACK_FRAME("PieIcon","resetSeedPos",0x666c7edd,"PieIcon.resetSeedPos","PieIcon.hx",53,0xd0774219)
		HX_STACK_THIS(this)
		HX_STACK_LINE(55)
		this->set_x(this->homeX);
		HX_STACK_LINE(56)
		this->set_y(this->homeY);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PieIcon_obj,resetSeedPos,(void))

bool PieIcon_obj::getDragging( ){
	HX_STACK_FRAME("PieIcon","getDragging",0xc3126f5c,"PieIcon.getDragging","PieIcon.hx",62,0xd0774219)
	HX_STACK_THIS(this)
	HX_STACK_LINE(62)
	return this->dragging;
}


HX_DEFINE_DYNAMIC_FUNC0(PieIcon_obj,getDragging,return )

Void PieIcon_obj::setDragging( bool val){
{
		HX_STACK_FRAME("PieIcon","setDragging",0xcd7f7668,"PieIcon.setDragging","PieIcon.hx",68,0xd0774219)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(68)
		this->dragging = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PieIcon_obj,setDragging,(void))

Void PieIcon_obj::checkMouseState( ){
{
		HX_STACK_FRAME("PieIcon","checkMouseState",0xf74e77eb,"PieIcon.checkMouseState","PieIcon.hx",74,0xd0774219)
		HX_STACK_THIS(this)
		struct _Function_1_1{
			inline static bool Block( ){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PieIcon.hx",74,0xd0774219)
				{
					HX_STACK_LINE(74)
					::flixel::input::mouse::FlxMouseButton _this = ::flixel::FlxG_obj::mouse->_leftButton;		HX_STACK_VAR(_this,"_this");
					HX_STACK_LINE(74)
					return (bool((_this->current == (int)-1)) || bool((_this->current == (int)-2)));
				}
				return null();
			}
		};
		HX_STACK_LINE(74)
		if ((_Function_1_1::Block())){
			HX_STACK_LINE(76)
			this->dragging = false;
			HX_STACK_LINE(77)
			this->resetSeedPos();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PieIcon_obj,checkMouseState,(void))

Void PieIcon_obj::unregisterPieEvent( ){
{
		HX_STACK_FRAME("PieIcon","unregisterPieEvent",0x6417fb13,"PieIcon.unregisterPieEvent","PieIcon.hx",84,0xd0774219)
		HX_STACK_THIS(this)
		HX_STACK_LINE(84)
		::flixel::plugin::MouseEventManager_obj::remove(hx::ObjectPtr<OBJ_>(this));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PieIcon_obj,unregisterPieEvent,(void))

Void PieIcon_obj::registerPieEvent( ){
{
		HX_STACK_FRAME("PieIcon","registerPieEvent",0x3b160e7a,"PieIcon.registerPieEvent","PieIcon.hx",88,0xd0774219)
		HX_STACK_THIS(this)
		HX_STACK_LINE(88)
		::flixel::plugin::MouseEventManager_obj::add(hx::ObjectPtr<OBJ_>(this),this->onDown_dyn(),this->onUp_dyn(),null(),null(),null(),null(),null());
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PieIcon_obj,registerPieEvent,(void))

Void PieIcon_obj::update( ){
{
		HX_STACK_FRAME("PieIcon","update",0x89172592,"PieIcon.update","PieIcon.hx",91,0xd0774219)
		HX_STACK_THIS(this)
		HX_STACK_LINE(93)
		this->checkMouseState();
		HX_STACK_LINE(94)
		this->dragSeed(this->dragging);
		HX_STACK_LINE(95)
		this->super::update();
	}
return null();
}


Void PieIcon_obj::destroy( ){
{
		HX_STACK_FRAME("PieIcon","destroy",0xf1e02551,"PieIcon.destroy","PieIcon.hx",99,0xd0774219)
		HX_STACK_THIS(this)
		HX_STACK_LINE(101)
		::flixel::plugin::MouseEventManager_obj::remove(hx::ObjectPtr<OBJ_>(this));
		HX_STACK_LINE(102)
		this->super::destroy();
	}
return null();
}



PieIcon_obj::PieIcon_obj()
{
}

Dynamic PieIcon_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"onUp") ) { return onUp_dyn(); }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"homeX") ) { return homeX; }
		if (HX_FIELD_EQ(inName,"homeY") ) { return homeY; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"onDown") ) { return onDown_dyn(); }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"dragging") ) { return dragging; }
		if (HX_FIELD_EQ(inName,"dragSeed") ) { return dragSeed_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"getDragging") ) { return getDragging_dyn(); }
		if (HX_FIELD_EQ(inName,"setDragging") ) { return setDragging_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"resetSeedPos") ) { return resetSeedPos_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"checkMouseState") ) { return checkMouseState_dyn(); }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"registerPieEvent") ) { return registerPieEvent_dyn(); }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"unregisterPieEvent") ) { return unregisterPieEvent_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic PieIcon_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"homeX") ) { homeX=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"homeY") ) { homeY=inValue.Cast< int >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"dragging") ) { dragging=inValue.Cast< bool >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void PieIcon_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("dragging"));
	outFields->push(HX_CSTRING("homeX"));
	outFields->push(HX_CSTRING("homeY"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsBool,(int)offsetof(PieIcon_obj,dragging),HX_CSTRING("dragging")},
	{hx::fsInt,(int)offsetof(PieIcon_obj,homeX),HX_CSTRING("homeX")},
	{hx::fsInt,(int)offsetof(PieIcon_obj,homeY),HX_CSTRING("homeY")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("dragging"),
	HX_CSTRING("homeX"),
	HX_CSTRING("homeY"),
	HX_CSTRING("onDown"),
	HX_CSTRING("onUp"),
	HX_CSTRING("dragSeed"),
	HX_CSTRING("resetSeedPos"),
	HX_CSTRING("getDragging"),
	HX_CSTRING("setDragging"),
	HX_CSTRING("checkMouseState"),
	HX_CSTRING("unregisterPieEvent"),
	HX_CSTRING("registerPieEvent"),
	HX_CSTRING("update"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(PieIcon_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(PieIcon_obj::__mClass,"__mClass");
};

#endif

Class PieIcon_obj::__mClass;

void PieIcon_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("PieIcon"), hx::TCanCast< PieIcon_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void PieIcon_obj::__boot()
{
}

