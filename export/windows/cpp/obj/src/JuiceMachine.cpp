#include <hxcpp.h>

#ifndef INCLUDED_JuiceMachine
#include <JuiceMachine.h>
#endif
#ifndef INCLUDED_JuicePackIcon
#include <JuicePackIcon.h>
#endif
#ifndef INCLUDED_Tile
#include <Tile.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_plugin_FlxPlugin
#include <flixel/plugin/FlxPlugin.h>
#endif
#ifndef INCLUDED_flixel_plugin_MouseEventManager
#include <flixel/plugin/MouseEventManager.h>
#endif
#ifndef INCLUDED_flixel_system_FlxSound
#include <flixel/system/FlxSound.h>
#endif
#ifndef INCLUDED_flixel_system_frontEnds_SoundFrontEnd
#include <flixel/system/frontEnds/SoundFrontEnd.h>
#endif
#ifndef INCLUDED_flixel_tweens_FlxTween
#include <flixel/tweens/FlxTween.h>
#endif
#ifndef INCLUDED_flixel_tweens_misc_VarTween
#include <flixel/tweens/misc/VarTween.h>
#endif
#ifndef INCLUDED_flixel_util_FlxDestroyUtil
#include <flixel/util/FlxDestroyUtil.h>
#endif
#ifndef INCLUDED_haxe_Log
#include <haxe/Log.h>
#endif

Void JuiceMachine_obj::__construct(::Tile tileSprite)
{
HX_STACK_FRAME("JuiceMachine","new",0x2cb52499,"JuiceMachine.new","JuiceMachine.hx",15,0x463a30f7)
HX_STACK_THIS(this)
HX_STACK_ARG(tileSprite,"tileSprite")
{
	HX_STACK_LINE(42)
	this->inJuicePack = false;
	HX_STACK_LINE(41)
	this->bottleFlying = false;
	HX_STACK_LINE(40)
	this->animateRepairSprite = false;
	HX_STACK_LINE(39)
	this->repairClicked = false;
	HX_STACK_LINE(38)
	this->fillingBottle = false;
	HX_STACK_LINE(37)
	this->machineClicked = false;
	HX_STACK_LINE(36)
	this->machineAlertReset = false;
	HX_STACK_LINE(35)
	this->machineOn = false;
	HX_STACK_LINE(34)
	this->payElectricBill = false;
	HX_STACK_LINE(31)
	this->machineReliabilityTimer = (int)20;
	HX_STACK_LINE(30)
	this->electricUtilityTimer = (int)15;
	HX_STACK_LINE(47)
	super::__construct(null());
	HX_STACK_LINE(48)
	::flixel::group::FlxGroup _g = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(48)
	this->juiceMachineGroup = _g;
	HX_STACK_LINE(49)
	::flixel::FlxSprite _g1 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
	HX_STACK_LINE(49)
	this->juiceMachineSprite = _g1;
	HX_STACK_LINE(50)
	::flixel::FlxSprite _g2 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g2,"_g2");
	HX_STACK_LINE(50)
	this->juiceMachineBottleAnim = _g2;
	HX_STACK_LINE(51)
	::flixel::FlxSprite _g3 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g3,"_g3");
	HX_STACK_LINE(51)
	this->juiceMachineAlertSprite = _g3;
	HX_STACK_LINE(52)
	::flixel::FlxSprite _g4 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g4,"_g4");
	HX_STACK_LINE(52)
	this->juiceMachineFlyingBottle = _g4;
	HX_STACK_LINE(53)
	::flixel::FlxSprite _g5 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g5,"_g5");
	HX_STACK_LINE(53)
	this->minusOneSprite = _g5;
	HX_STACK_LINE(54)
	::flixel::FlxSprite _g6 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g6,"_g6");
	HX_STACK_LINE(54)
	this->repairAlertSprite = _g6;
	HX_STACK_LINE(55)
	this->myTile = tileSprite;
	HX_STACK_LINE(56)
	this->minusOneSprite->loadGraphic(HX_CSTRING("assets/images/minusOneImage.png"),false,null(),null(),null(),null());
	HX_STACK_LINE(57)
	this->minusOneSprite->set_alpha((int)0);
	HX_STACK_LINE(58)
	this->juiceMachineSprite->loadGraphic(HX_CSTRING("assets/images/juiceMachineAnimation.png"),true,(int)86,(int)84,null(),null());
	HX_STACK_LINE(59)
	this->juiceMachineSprite->animation->add(HX_CSTRING("machineOn"),Array_obj< int >::__new().Add((int)1).Add((int)2).Add((int)3).Add((int)4).Add((int)5),(int)5,true);
	HX_STACK_LINE(60)
	this->juiceMachineSprite->animation->add(HX_CSTRING("machineOff"),Array_obj< int >::__new().Add((int)0),(int)1,false);
	HX_STACK_LINE(61)
	this->juiceMachineSprite->animation->play(HX_CSTRING("machineOff"),null(),null());
	HX_STACK_LINE(62)
	this->juiceMachineAlertSprite->loadGraphic(HX_CSTRING("assets/images/juiceMachineSwitchAnimation.png"),true,(int)32,(int)18,null(),null());
	HX_STACK_LINE(63)
	this->juiceMachineAlertSprite->animation->add(HX_CSTRING("switchOn"),Array_obj< int >::__new().Add((int)1),(int)1,false);
	HX_STACK_LINE(64)
	this->juiceMachineAlertSprite->animation->add(HX_CSTRING("switchOff"),Array_obj< int >::__new().Add((int)0),(int)1,false);
	HX_STACK_LINE(65)
	this->juiceMachineAlertSprite->animation->play(HX_CSTRING("switchOn"),null(),null());
	HX_STACK_LINE(66)
	this->juiceMachineBottleAnim->loadGraphic(HX_CSTRING("assets/images/fillingBottleAnimation.png"),true,(int)10,(int)20,null(),null());
	HX_STACK_LINE(67)
	this->juiceMachineBottleAnim->animation->add(HX_CSTRING("fillingBottle"),Array_obj< int >::__new().Add((int)0).Add((int)1).Add((int)2).Add((int)3).Add((int)4).Add((int)5),(int)1,true);
	HX_STACK_LINE(68)
	this->juiceMachineBottleAnim->set_alpha((int)0);
	HX_STACK_LINE(69)
	this->juiceMachineFlyingBottle->loadGraphic(HX_CSTRING("assets/images/juiceMachineIcon.png"),null(),null(),null(),null(),null());
	HX_STACK_LINE(70)
	this->repairAlertSprite->loadGraphic(HX_CSTRING("assets/images/repairAnimationS.png"),true,(int)16,(int)16,null(),null());
	HX_STACK_LINE(71)
	this->repairAlertSprite->animation->add(HX_CSTRING("repairMe"),Array_obj< int >::__new().Add((int)0).Add((int)1).Add((int)2).Add((int)3).Add((int)4),(int)10,true);
	HX_STACK_LINE(72)
	this->repairAlertSprite->set_alpha((int)0);
	HX_STACK_LINE(73)
	this->juiceMachineFlyingBottle->set_alpha((int)0);
	HX_STACK_LINE(74)
	this->juiceMachineFlyingBottle->set_x((tileSprite->x + (int)90));
	HX_STACK_LINE(75)
	this->juiceMachineFlyingBottle->set_y((tileSprite->y + (int)5));
	HX_STACK_LINE(76)
	this->juiceMachineBottleAnim->set_x((tileSprite->x + (int)64));
	HX_STACK_LINE(77)
	this->juiceMachineBottleAnim->set_y((tileSprite->y + (int)50));
	HX_STACK_LINE(78)
	this->juiceMachineSprite->set_x((tileSprite->x + (int)57));
	HX_STACK_LINE(79)
	this->juiceMachineSprite->set_y((tileSprite->y - (int)15));
	HX_STACK_LINE(80)
	this->juiceMachineAlertSprite->set_x((tileSprite->x + (int)78));
	HX_STACK_LINE(81)
	this->juiceMachineAlertSprite->set_y((tileSprite->y + (int)3));
	HX_STACK_LINE(82)
	this->repairAlertSprite->set_x((tileSprite->x + (int)96));
	HX_STACK_LINE(83)
	this->repairAlertSprite->set_y((tileSprite->y + (int)67));
	HX_STACK_LINE(84)
	this->registerMachineEvents();
	HX_STACK_LINE(85)
	this->juiceMachineGroup->add(this->juiceMachineSprite);
	HX_STACK_LINE(86)
	this->juiceMachineGroup->add(this->juiceMachineAlertSprite);
	HX_STACK_LINE(87)
	this->juiceMachineGroup->add(this->juiceMachineBottleAnim);
	HX_STACK_LINE(88)
	this->juiceMachineGroup->add(this->minusOneSprite);
	HX_STACK_LINE(90)
	this->juiceMachineGroup->add(this->repairAlertSprite);
	HX_STACK_LINE(91)
	this->add(this->juiceMachineGroup);
}
;
	return null();
}

//JuiceMachine_obj::~JuiceMachine_obj() { }

Dynamic JuiceMachine_obj::__CreateEmpty() { return  new JuiceMachine_obj; }
hx::ObjectPtr< JuiceMachine_obj > JuiceMachine_obj::__new(::Tile tileSprite)
{  hx::ObjectPtr< JuiceMachine_obj > result = new JuiceMachine_obj();
	result->__construct(tileSprite);
	return result;}

Dynamic JuiceMachine_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< JuiceMachine_obj > result = new JuiceMachine_obj();
	result->__construct(inArgs[0]);
	return result;}

::Tile JuiceMachine_obj::getMyTile( ){
	HX_STACK_FRAME("JuiceMachine","getMyTile",0x6ceecae9,"JuiceMachine.getMyTile","JuiceMachine.hx",96,0x463a30f7)
	HX_STACK_THIS(this)
	HX_STACK_LINE(96)
	return this->myTile;
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceMachine_obj,getMyTile,return )

bool JuiceMachine_obj::getInJuicePack( ){
	HX_STACK_FRAME("JuiceMachine","getInJuicePack",0x71caf125,"JuiceMachine.getInJuicePack","JuiceMachine.hx",100,0x463a30f7)
	HX_STACK_THIS(this)
	HX_STACK_LINE(100)
	return this->inJuicePack;
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceMachine_obj,getInJuicePack,return )

::flixel::FlxSprite JuiceMachine_obj::getJuiceMachineFlyingBotte( ){
	HX_STACK_FRAME("JuiceMachine","getJuiceMachineFlyingBotte",0xc8a51e93,"JuiceMachine.getJuiceMachineFlyingBotte","JuiceMachine.hx",104,0x463a30f7)
	HX_STACK_THIS(this)
	HX_STACK_LINE(104)
	return this->juiceMachineFlyingBottle;
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceMachine_obj,getJuiceMachineFlyingBotte,return )

::flixel::FlxSprite JuiceMachine_obj::getJuiceMachineSprite( ){
	HX_STACK_FRAME("JuiceMachine","getJuiceMachineSprite",0x8911801b,"JuiceMachine.getJuiceMachineSprite","JuiceMachine.hx",108,0x463a30f7)
	HX_STACK_THIS(this)
	HX_STACK_LINE(108)
	return this->juiceMachineSprite;
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceMachine_obj,getJuiceMachineSprite,return )

Float JuiceMachine_obj::getMyRelativeX( ){
	HX_STACK_FRAME("JuiceMachine","getMyRelativeX",0x207e9071,"JuiceMachine.getMyRelativeX","JuiceMachine.hx",112,0x463a30f7)
	HX_STACK_THIS(this)
	HX_STACK_LINE(112)
	return this->myRelativeX;
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceMachine_obj,getMyRelativeX,return )

Float JuiceMachine_obj::getMyRelativeY( ){
	HX_STACK_FRAME("JuiceMachine","getMyRelativeY",0x207e9072,"JuiceMachine.getMyRelativeY","JuiceMachine.hx",116,0x463a30f7)
	HX_STACK_THIS(this)
	HX_STACK_LINE(116)
	return this->myRelativeY;
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceMachine_obj,getMyRelativeY,return )

bool JuiceMachine_obj::getMachineOn( ){
	HX_STACK_FRAME("JuiceMachine","getMachineOn",0xf3a629b7,"JuiceMachine.getMachineOn","JuiceMachine.hx",120,0x463a30f7)
	HX_STACK_THIS(this)
	HX_STACK_LINE(120)
	return this->machineOn;
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceMachine_obj,getMachineOn,return )

bool JuiceMachine_obj::getFillingBottle( ){
	HX_STACK_FRAME("JuiceMachine","getFillingBottle",0x3a696996,"JuiceMachine.getFillingBottle","JuiceMachine.hx",124,0x463a30f7)
	HX_STACK_THIS(this)
	HX_STACK_LINE(124)
	return this->fillingBottle;
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceMachine_obj,getFillingBottle,return )

bool JuiceMachine_obj::getPayElectricBill( ){
	HX_STACK_FRAME("JuiceMachine","getPayElectricBill",0xcb237bfd,"JuiceMachine.getPayElectricBill","JuiceMachine.hx",128,0x463a30f7)
	HX_STACK_THIS(this)
	HX_STACK_LINE(128)
	return this->payElectricBill;
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceMachine_obj,getPayElectricBill,return )

bool JuiceMachine_obj::getRepairClicked( ){
	HX_STACK_FRAME("JuiceMachine","getRepairClicked",0x8fcf628b,"JuiceMachine.getRepairClicked","JuiceMachine.hx",132,0x463a30f7)
	HX_STACK_THIS(this)
	HX_STACK_LINE(132)
	return this->repairClicked;
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceMachine_obj,getRepairClicked,return )

bool JuiceMachine_obj::getAnimateRepairSprite( ){
	HX_STACK_FRAME("JuiceMachine","getAnimateRepairSprite",0xcb86b1e4,"JuiceMachine.getAnimateRepairSprite","JuiceMachine.hx",136,0x463a30f7)
	HX_STACK_THIS(this)
	HX_STACK_LINE(136)
	return this->animateRepairSprite;
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceMachine_obj,getAnimateRepairSprite,return )

bool JuiceMachine_obj::getBottleFlying( ){
	HX_STACK_FRAME("JuiceMachine","getBottleFlying",0x567cbea4,"JuiceMachine.getBottleFlying","JuiceMachine.hx",140,0x463a30f7)
	HX_STACK_THIS(this)
	HX_STACK_LINE(140)
	return this->bottleFlying;
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceMachine_obj,getBottleFlying,return )

Void JuiceMachine_obj::setAnimateRepairSprite( bool val){
{
		HX_STACK_FRAME("JuiceMachine","setAnimateRepairSprite",0xff322e58,"JuiceMachine.setAnimateRepairSprite","JuiceMachine.hx",145,0x463a30f7)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(145)
		this->animateRepairSprite = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(JuiceMachine_obj,setAnimateRepairSprite,(void))

Void JuiceMachine_obj::setInJuicePack( bool val){
{
		HX_STACK_FRAME("JuiceMachine","setInJuicePack",0x91ead999,"JuiceMachine.setInJuicePack","JuiceMachine.hx",149,0x463a30f7)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(149)
		this->inJuicePack = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(JuiceMachine_obj,setInJuicePack,(void))

Void JuiceMachine_obj::setRepairClicked( bool val){
{
		HX_STACK_FRAME("JuiceMachine","setRepairClicked",0xe6114fff,"JuiceMachine.setRepairClicked","JuiceMachine.hx",153,0x463a30f7)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(153)
		this->repairClicked = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(JuiceMachine_obj,setRepairClicked,(void))

Void JuiceMachine_obj::setPayElectricBill( bool val){
{
		HX_STACK_FRAME("JuiceMachine","setPayElectricBill",0xa7d2ae71,"JuiceMachine.setPayElectricBill","JuiceMachine.hx",157,0x463a30f7)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(157)
		this->payElectricBill = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(JuiceMachine_obj,setPayElectricBill,(void))

Void JuiceMachine_obj::setFillingBottle( bool val){
{
		HX_STACK_FRAME("JuiceMachine","setFillingBottle",0x90ab570a,"JuiceMachine.setFillingBottle","JuiceMachine.hx",161,0x463a30f7)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(161)
		this->fillingBottle = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(JuiceMachine_obj,setFillingBottle,(void))

Void JuiceMachine_obj::setMachineOn( bool val){
{
		HX_STACK_FRAME("JuiceMachine","setMachineOn",0x089f4d2b,"JuiceMachine.setMachineOn","JuiceMachine.hx",165,0x463a30f7)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(165)
		this->machineOn = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(JuiceMachine_obj,setMachineOn,(void))

Void JuiceMachine_obj::setMyRelativeX( Float val){
{
		HX_STACK_FRAME("JuiceMachine","setMyRelativeX",0x409e78e5,"JuiceMachine.setMyRelativeX","JuiceMachine.hx",169,0x463a30f7)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(169)
		this->myRelativeX = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(JuiceMachine_obj,setMyRelativeX,(void))

Void JuiceMachine_obj::setMyRelativeY( Float val){
{
		HX_STACK_FRAME("JuiceMachine","setMyRelativeY",0x409e78e6,"JuiceMachine.setMyRelativeY","JuiceMachine.hx",173,0x463a30f7)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(173)
		this->myRelativeY = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(JuiceMachine_obj,setMyRelativeY,(void))

Void JuiceMachine_obj::setGoldCount( int val){
{
		HX_STACK_FRAME("JuiceMachine","setGoldCount",0x34083ab4,"JuiceMachine.setGoldCount","JuiceMachine.hx",177,0x463a30f7)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(177)
		this->goldCount = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(JuiceMachine_obj,setGoldCount,(void))

Void JuiceMachine_obj::setAppleCount( int val){
{
		HX_STACK_FRAME("JuiceMachine","setAppleCount",0x1461fcd0,"JuiceMachine.setAppleCount","JuiceMachine.hx",181,0x463a30f7)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(181)
		this->appleCount = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(JuiceMachine_obj,setAppleCount,(void))

Void JuiceMachine_obj::setBottleFlying( bool val){
{
		HX_STACK_FRAME("JuiceMachine","setBottleFlying",0x52483bb0,"JuiceMachine.setBottleFlying","JuiceMachine.hx",185,0x463a30f7)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(185)
		this->bottleFlying = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(JuiceMachine_obj,setBottleFlying,(void))

Void JuiceMachine_obj::registerMachineEvents( ){
{
		HX_STACK_FRAME("JuiceMachine","registerMachineEvents",0x0ca1dc16,"JuiceMachine.registerMachineEvents","JuiceMachine.hx",189,0x463a30f7)
		HX_STACK_THIS(this)
		HX_STACK_LINE(190)
		::flixel::plugin::MouseEventManager_obj::add(this->juiceMachineAlertSprite,this->turnMachineOn_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(191)
		::flixel::plugin::MouseEventManager_obj::add(this->repairAlertSprite,this->repairAlertClicked_dyn(),null(),null(),null(),null(),null(),null());
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceMachine_obj,registerMachineEvents,(void))

Void JuiceMachine_obj::unregisterMachineEvents( ){
{
		HX_STACK_FRAME("JuiceMachine","unregisterMachineEvents",0x09f8e15d,"JuiceMachine.unregisterMachineEvents","JuiceMachine.hx",194,0x463a30f7)
		HX_STACK_THIS(this)
		HX_STACK_LINE(195)
		::flixel::plugin::MouseEventManager_obj::remove(this->juiceMachineAlertSprite);
		HX_STACK_LINE(196)
		::flixel::plugin::MouseEventManager_obj::remove(this->repairAlertSprite);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceMachine_obj,unregisterMachineEvents,(void))

Void JuiceMachine_obj::repairAlertClicked( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("JuiceMachine","repairAlertClicked",0xfa8c9f7f,"JuiceMachine.repairAlertClicked","JuiceMachine.hx",200,0x463a30f7)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(200)
		this->repairClicked = true;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(JuiceMachine_obj,repairAlertClicked,(void))

Void JuiceMachine_obj::turnMachineOn( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("JuiceMachine","turnMachineOn",0xadd302c2,"JuiceMachine.turnMachineOn","JuiceMachine.hx",205,0x463a30f7)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(205)
		if ((!(this->machineOn))){
			HX_STACK_LINE(206)
			this->fillingBottle = true;
			HX_STACK_LINE(207)
			::flixel::system::FlxSound _g = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/juiceMachineSound.wav"),(int)1,true,null(),null(),null(),null());		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(207)
			this->juiceMachineSound = _g;
			HX_STACK_LINE(208)
			this->juiceMachineSound->play(null());
			HX_STACK_LINE(209)
			::flixel::system::FlxSound _g1 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/fillingJuiceBottleSound.wav"),(int)1,true,null(),null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(209)
			this->fillingJuiceBottleSound = _g1;
			HX_STACK_LINE(210)
			this->fillingJuiceBottleSound->play(null());
			HX_STACK_LINE(211)
			this->juiceMachineBottleAnim->animation->play(HX_CSTRING("fillingBottle"),null(),null());
			HX_STACK_LINE(212)
			this->juiceMachineAlertSprite->animation->play(HX_CSTRING("switchOff"),null(),null());
			HX_STACK_LINE(213)
			this->juiceMachineSprite->animation->play(HX_CSTRING("machineOn"),null(),null());
			HX_STACK_LINE(214)
			this->machineOn = true;
		}
		else{
			HX_STACK_LINE(217)
			this->fillingBottle = false;
			HX_STACK_LINE(218)
			this->juiceMachineAlertSprite->animation->play(HX_CSTRING("switchOn"),null(),null());
			HX_STACK_LINE(219)
			this->juiceMachineSprite->animation->play(HX_CSTRING("machineOff"),null(),null());
			HX_STACK_LINE(220)
			if (((bool((this->juiceMachineSound != null())) && bool((this->fillingJuiceBottleSound != null()))))){
				HX_STACK_LINE(221)
				{
					HX_STACK_LINE(221)
					::flixel::system::FlxSound _this = this->juiceMachineSound;		HX_STACK_VAR(_this,"_this");
					HX_STACK_LINE(221)
					_this->cleanup(_this->autoDestroy,true,true);
					HX_STACK_LINE(221)
					_this;
				}
				HX_STACK_LINE(222)
				{
					HX_STACK_LINE(222)
					::flixel::system::FlxSound _this = this->fillingJuiceBottleSound;		HX_STACK_VAR(_this,"_this");
					HX_STACK_LINE(222)
					_this->cleanup(_this->autoDestroy,true,true);
					HX_STACK_LINE(222)
					_this;
				}
				HX_STACK_LINE(223)
				::flixel::system::FlxSound _g2 = ::flixel::util::FlxDestroyUtil_obj::destroy(this->juiceMachineSound);		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(223)
				this->juiceMachineSound = _g2;
				HX_STACK_LINE(224)
				::flixel::system::FlxSound _g3 = ::flixel::util::FlxDestroyUtil_obj::destroy(this->fillingJuiceBottleSound);		HX_STACK_VAR(_g3,"_g3");
				HX_STACK_LINE(224)
				this->fillingJuiceBottleSound = _g3;
				HX_STACK_LINE(225)
				::haxe::Log_obj::trace(HX_CSTRING("destroying machine sounds"),hx::SourceInfo(HX_CSTRING("JuiceMachine.hx"),225,HX_CSTRING("JuiceMachine"),HX_CSTRING("turnMachineOn")));
			}
			HX_STACK_LINE(227)
			this->machineOn = false;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(JuiceMachine_obj,turnMachineOn,(void))

Void JuiceMachine_obj::bottleFadeIn( ){
{
		HX_STACK_FRAME("JuiceMachine","bottleFadeIn",0x64c2fa0e,"JuiceMachine.bottleFadeIn","JuiceMachine.hx",232,0x463a30f7)
		HX_STACK_THIS(this)
		HX_STACK_LINE(232)
		if ((this->fillingBottle)){
			HX_STACK_LINE(233)
			::flixel::FlxSprite _g = this->juiceMachineBottleAnim;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(233)
			_g->set_alpha((_g->alpha + .02));
		}
		else{
			HX_STACK_LINE(236)
			::flixel::FlxSprite _g = this->juiceMachineBottleAnim;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(236)
			_g->set_alpha((_g->alpha - .02));
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceMachine_obj,bottleFadeIn,(void))

Void JuiceMachine_obj::payElectricUtility( ){
{
		HX_STACK_FRAME("JuiceMachine","payElectricUtility",0xb103786e,"JuiceMachine.payElectricUtility","JuiceMachine.hx",241,0x463a30f7)
		HX_STACK_THIS(this)
		HX_STACK_LINE(241)
		if ((this->machineOn)){
			HX_STACK_LINE(242)
			hx::SubEq(this->electricUtilityTimer,::flixel::FlxG_obj::elapsed);
			HX_STACK_LINE(243)
			if (((this->electricUtilityTimer < (int)0))){
				HX_STACK_LINE(244)
				this->minusOneSprite->set_x((this->juiceMachineSprite->x + (int)30));
				HX_STACK_LINE(245)
				this->minusOneSprite->set_y((this->juiceMachineSprite->y + (int)5));
				HX_STACK_LINE(246)
				this->minusOneSprite->set_alpha((int)1);
				struct _Function_3_1{
					inline static Dynamic Block( hx::ObjectPtr< ::JuiceMachine_obj > __this){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","JuiceMachine.hx",247,0x463a30f7)
						{
							hx::Anon __result = hx::Anon_obj::Create();
							__result->Add(HX_CSTRING("alpha") , (int)0,false);
							__result->Add(HX_CSTRING("y") , (__this->minusOneSprite->y - (int)16),false);
							return __result;
						}
						return null();
					}
				};
				HX_STACK_LINE(247)
				::flixel::tweens::FlxTween_obj::tween(this->minusOneSprite,_Function_3_1::Block(this),(int)1,null());
				HX_STACK_LINE(248)
				this->payElectricBill = true;
				HX_STACK_LINE(249)
				this->electricUtilityTimer = (int)15;
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceMachine_obj,payElectricUtility,(void))

Void JuiceMachine_obj::machineReliability( ){
{
		HX_STACK_FRAME("JuiceMachine","machineReliability",0x189bea5a,"JuiceMachine.machineReliability","JuiceMachine.hx",255,0x463a30f7)
		HX_STACK_THIS(this)
		HX_STACK_LINE(255)
		if ((this->machineOn)){
			HX_STACK_LINE(256)
			hx::SubEq(this->machineReliabilityTimer,::flixel::FlxG_obj::elapsed);
			HX_STACK_LINE(257)
			if (((this->machineReliabilityTimer < (int)0))){
				HX_STACK_LINE(258)
				this->animateRepairSprite = true;
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceMachine_obj,machineReliability,(void))

Void JuiceMachine_obj::resetRepairAlertTimer( ){
{
		HX_STACK_FRAME("JuiceMachine","resetRepairAlertTimer",0x180790de,"JuiceMachine.resetRepairAlertTimer","JuiceMachine.hx",264,0x463a30f7)
		HX_STACK_THIS(this)
		HX_STACK_LINE(264)
		this->machineReliabilityTimer = (int)20;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceMachine_obj,resetRepairAlertTimer,(void))

Void JuiceMachine_obj::repairSpriteAnimation( ){
{
		HX_STACK_FRAME("JuiceMachine","repairSpriteAnimation",0x0e1911eb,"JuiceMachine.repairSpriteAnimation","JuiceMachine.hx",268,0x463a30f7)
		HX_STACK_THIS(this)
		HX_STACK_LINE(268)
		if ((this->animateRepairSprite)){
			HX_STACK_LINE(269)
			{
				HX_STACK_LINE(269)
				::flixel::FlxSprite _g = this->repairAlertSprite;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(269)
				_g->set_alpha((_g->alpha + .01));
			}
			HX_STACK_LINE(270)
			this->repairAlertSprite->animation->play(HX_CSTRING("repairMe"),null(),null());
			HX_STACK_LINE(271)
			this->machineReliabilityTimer = (int)-1;
			HX_STACK_LINE(272)
			::flixel::plugin::MouseEventManager_obj::remove(this->juiceMachineAlertSprite);
			HX_STACK_LINE(273)
			this->machineOn = true;
			HX_STACK_LINE(274)
			this->turnMachineOn(null());
		}
		else{
			HX_STACK_LINE(277)
			::flixel::FlxSprite _g = this->repairAlertSprite;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(277)
			_g->set_alpha((_g->alpha - .1));
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceMachine_obj,repairSpriteAnimation,(void))

Void JuiceMachine_obj::setRiseRun( ){
{
		HX_STACK_FRAME("JuiceMachine","setRiseRun",0x28696027,"JuiceMachine.setRiseRun","JuiceMachine.hx",281,0x463a30f7)
		HX_STACK_THIS(this)
		HX_STACK_LINE(282)
		this->run = -((((::JuicePackIcon_obj::JuicePackPosX + (int)20) - this->juiceMachineFlyingBottle->x)));
		HX_STACK_LINE(283)
		this->rise = -((((::JuicePackIcon_obj::JuicePackPosY + (int)20) - this->juiceMachineFlyingBottle->y)));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceMachine_obj,setRiseRun,(void))

Void JuiceMachine_obj::sendBottleToPack( ){
{
		HX_STACK_FRAME("JuiceMachine","sendBottleToPack",0x13517469,"JuiceMachine.sendBottleToPack","JuiceMachine.hx",287,0x463a30f7)
		HX_STACK_THIS(this)
		HX_STACK_LINE(287)
		if ((this->bottleFlying)){
			HX_STACK_LINE(288)
			this->juiceMachineFlyingBottle->set_alpha((int)1);
			HX_STACK_LINE(289)
			if (((this->juiceMachineFlyingBottle->y >= (::JuicePackIcon_obj::JuicePackPosY + (int)20)))){
				HX_STACK_LINE(290)
				::flixel::FlxSprite _g = this->juiceMachineFlyingBottle;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(290)
				_g->set_y((_g->y - (Float(this->rise) / Float((int)25))));
			}
			HX_STACK_LINE(292)
			if (((this->juiceMachineFlyingBottle->x >= (::JuicePackIcon_obj::JuicePackPosX + (int)20)))){
				HX_STACK_LINE(293)
				::flixel::FlxSprite _g = this->juiceMachineFlyingBottle;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(293)
				_g->set_x((_g->x - (Float(this->run) / Float((int)25))));
			}
			HX_STACK_LINE(295)
			if (((bool((this->juiceMachineFlyingBottle->x <= (::JuicePackIcon_obj::JuicePackPosX + (int)22))) && bool((this->juiceMachineFlyingBottle->y <= (::JuicePackIcon_obj::JuicePackPosY + (int)22)))))){
				HX_STACK_LINE(296)
				this->inJuicePack = true;
				HX_STACK_LINE(297)
				this->bottleFlying = false;
				HX_STACK_LINE(298)
				this->juiceMachineFlyingBottle->set_alpha((int)0);
				HX_STACK_LINE(299)
				this->juiceMachineFlyingBottle->set_x((this->myTile->x + (int)90));
				HX_STACK_LINE(300)
				this->juiceMachineFlyingBottle->set_y((this->myTile->y + (int)5));
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceMachine_obj,sendBottleToPack,(void))

Void JuiceMachine_obj::update( ){
{
		HX_STACK_FRAME("JuiceMachine","update",0x38dddb70,"JuiceMachine.update","JuiceMachine.hx",305,0x463a30f7)
		HX_STACK_THIS(this)
		HX_STACK_LINE(306)
		this->machineReliability();
		HX_STACK_LINE(307)
		this->repairSpriteAnimation();
		HX_STACK_LINE(308)
		this->payElectricUtility();
		HX_STACK_LINE(309)
		this->bottleFadeIn();
		HX_STACK_LINE(310)
		this->sendBottleToPack();
		HX_STACK_LINE(311)
		this->super::update();
	}
return null();
}


Void JuiceMachine_obj::destroy( ){
{
		HX_STACK_FRAME("JuiceMachine","destroy",0x0ff891b3,"JuiceMachine.destroy","JuiceMachine.hx",314,0x463a30f7)
		HX_STACK_THIS(this)
		HX_STACK_LINE(315)
		this->destroy_dyn();
		HX_STACK_LINE(316)
		this->super::destroy();
	}
return null();
}



JuiceMachine_obj::JuiceMachine_obj()
{
}

void JuiceMachine_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(JuiceMachine);
	HX_MARK_MEMBER_NAME(juiceMachineGroup,"juiceMachineGroup");
	HX_MARK_MEMBER_NAME(juiceMachineSprite,"juiceMachineSprite");
	HX_MARK_MEMBER_NAME(juiceMachineAlertSprite,"juiceMachineAlertSprite");
	HX_MARK_MEMBER_NAME(juiceMachineBottleAnim,"juiceMachineBottleAnim");
	HX_MARK_MEMBER_NAME(juiceMachineFlyingBottle,"juiceMachineFlyingBottle");
	HX_MARK_MEMBER_NAME(minusOneSprite,"minusOneSprite");
	HX_MARK_MEMBER_NAME(repairAlertSprite,"repairAlertSprite");
	HX_MARK_MEMBER_NAME(fillingJuiceBottleSound,"fillingJuiceBottleSound");
	HX_MARK_MEMBER_NAME(juiceMachineSound,"juiceMachineSound");
	HX_MARK_MEMBER_NAME(myRelativeX,"myRelativeX");
	HX_MARK_MEMBER_NAME(myRelativeY,"myRelativeY");
	HX_MARK_MEMBER_NAME(rise,"rise");
	HX_MARK_MEMBER_NAME(run,"run");
	HX_MARK_MEMBER_NAME(electricUtilityTimer,"electricUtilityTimer");
	HX_MARK_MEMBER_NAME(machineReliabilityTimer,"machineReliabilityTimer");
	HX_MARK_MEMBER_NAME(goldCount,"goldCount");
	HX_MARK_MEMBER_NAME(appleCount,"appleCount");
	HX_MARK_MEMBER_NAME(payElectricBill,"payElectricBill");
	HX_MARK_MEMBER_NAME(machineOn,"machineOn");
	HX_MARK_MEMBER_NAME(machineAlertReset,"machineAlertReset");
	HX_MARK_MEMBER_NAME(machineClicked,"machineClicked");
	HX_MARK_MEMBER_NAME(fillingBottle,"fillingBottle");
	HX_MARK_MEMBER_NAME(repairClicked,"repairClicked");
	HX_MARK_MEMBER_NAME(animateRepairSprite,"animateRepairSprite");
	HX_MARK_MEMBER_NAME(bottleFlying,"bottleFlying");
	HX_MARK_MEMBER_NAME(inJuicePack,"inJuicePack");
	HX_MARK_MEMBER_NAME(myTile,"myTile");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void JuiceMachine_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(juiceMachineGroup,"juiceMachineGroup");
	HX_VISIT_MEMBER_NAME(juiceMachineSprite,"juiceMachineSprite");
	HX_VISIT_MEMBER_NAME(juiceMachineAlertSprite,"juiceMachineAlertSprite");
	HX_VISIT_MEMBER_NAME(juiceMachineBottleAnim,"juiceMachineBottleAnim");
	HX_VISIT_MEMBER_NAME(juiceMachineFlyingBottle,"juiceMachineFlyingBottle");
	HX_VISIT_MEMBER_NAME(minusOneSprite,"minusOneSprite");
	HX_VISIT_MEMBER_NAME(repairAlertSprite,"repairAlertSprite");
	HX_VISIT_MEMBER_NAME(fillingJuiceBottleSound,"fillingJuiceBottleSound");
	HX_VISIT_MEMBER_NAME(juiceMachineSound,"juiceMachineSound");
	HX_VISIT_MEMBER_NAME(myRelativeX,"myRelativeX");
	HX_VISIT_MEMBER_NAME(myRelativeY,"myRelativeY");
	HX_VISIT_MEMBER_NAME(rise,"rise");
	HX_VISIT_MEMBER_NAME(run,"run");
	HX_VISIT_MEMBER_NAME(electricUtilityTimer,"electricUtilityTimer");
	HX_VISIT_MEMBER_NAME(machineReliabilityTimer,"machineReliabilityTimer");
	HX_VISIT_MEMBER_NAME(goldCount,"goldCount");
	HX_VISIT_MEMBER_NAME(appleCount,"appleCount");
	HX_VISIT_MEMBER_NAME(payElectricBill,"payElectricBill");
	HX_VISIT_MEMBER_NAME(machineOn,"machineOn");
	HX_VISIT_MEMBER_NAME(machineAlertReset,"machineAlertReset");
	HX_VISIT_MEMBER_NAME(machineClicked,"machineClicked");
	HX_VISIT_MEMBER_NAME(fillingBottle,"fillingBottle");
	HX_VISIT_MEMBER_NAME(repairClicked,"repairClicked");
	HX_VISIT_MEMBER_NAME(animateRepairSprite,"animateRepairSprite");
	HX_VISIT_MEMBER_NAME(bottleFlying,"bottleFlying");
	HX_VISIT_MEMBER_NAME(inJuicePack,"inJuicePack");
	HX_VISIT_MEMBER_NAME(myTile,"myTile");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic JuiceMachine_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"run") ) { return run; }
		break;
	case 4:
		if (HX_FIELD_EQ(inName,"rise") ) { return rise; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"myTile") ) { return myTile; }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"goldCount") ) { return goldCount; }
		if (HX_FIELD_EQ(inName,"machineOn") ) { return machineOn; }
		if (HX_FIELD_EQ(inName,"getMyTile") ) { return getMyTile_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"appleCount") ) { return appleCount; }
		if (HX_FIELD_EQ(inName,"setRiseRun") ) { return setRiseRun_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"myRelativeX") ) { return myRelativeX; }
		if (HX_FIELD_EQ(inName,"myRelativeY") ) { return myRelativeY; }
		if (HX_FIELD_EQ(inName,"inJuicePack") ) { return inJuicePack; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"bottleFlying") ) { return bottleFlying; }
		if (HX_FIELD_EQ(inName,"getMachineOn") ) { return getMachineOn_dyn(); }
		if (HX_FIELD_EQ(inName,"setMachineOn") ) { return setMachineOn_dyn(); }
		if (HX_FIELD_EQ(inName,"setGoldCount") ) { return setGoldCount_dyn(); }
		if (HX_FIELD_EQ(inName,"bottleFadeIn") ) { return bottleFadeIn_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"fillingBottle") ) { return fillingBottle; }
		if (HX_FIELD_EQ(inName,"repairClicked") ) { return repairClicked; }
		if (HX_FIELD_EQ(inName,"setAppleCount") ) { return setAppleCount_dyn(); }
		if (HX_FIELD_EQ(inName,"turnMachineOn") ) { return turnMachineOn_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"minusOneSprite") ) { return minusOneSprite; }
		if (HX_FIELD_EQ(inName,"machineClicked") ) { return machineClicked; }
		if (HX_FIELD_EQ(inName,"getInJuicePack") ) { return getInJuicePack_dyn(); }
		if (HX_FIELD_EQ(inName,"getMyRelativeX") ) { return getMyRelativeX_dyn(); }
		if (HX_FIELD_EQ(inName,"getMyRelativeY") ) { return getMyRelativeY_dyn(); }
		if (HX_FIELD_EQ(inName,"setInJuicePack") ) { return setInJuicePack_dyn(); }
		if (HX_FIELD_EQ(inName,"setMyRelativeX") ) { return setMyRelativeX_dyn(); }
		if (HX_FIELD_EQ(inName,"setMyRelativeY") ) { return setMyRelativeY_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"payElectricBill") ) { return payElectricBill; }
		if (HX_FIELD_EQ(inName,"getBottleFlying") ) { return getBottleFlying_dyn(); }
		if (HX_FIELD_EQ(inName,"setBottleFlying") ) { return setBottleFlying_dyn(); }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"getFillingBottle") ) { return getFillingBottle_dyn(); }
		if (HX_FIELD_EQ(inName,"getRepairClicked") ) { return getRepairClicked_dyn(); }
		if (HX_FIELD_EQ(inName,"setRepairClicked") ) { return setRepairClicked_dyn(); }
		if (HX_FIELD_EQ(inName,"setFillingBottle") ) { return setFillingBottle_dyn(); }
		if (HX_FIELD_EQ(inName,"sendBottleToPack") ) { return sendBottleToPack_dyn(); }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"juiceMachineGroup") ) { return juiceMachineGroup; }
		if (HX_FIELD_EQ(inName,"repairAlertSprite") ) { return repairAlertSprite; }
		if (HX_FIELD_EQ(inName,"juiceMachineSound") ) { return juiceMachineSound; }
		if (HX_FIELD_EQ(inName,"machineAlertReset") ) { return machineAlertReset; }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"juiceMachineSprite") ) { return juiceMachineSprite; }
		if (HX_FIELD_EQ(inName,"getPayElectricBill") ) { return getPayElectricBill_dyn(); }
		if (HX_FIELD_EQ(inName,"setPayElectricBill") ) { return setPayElectricBill_dyn(); }
		if (HX_FIELD_EQ(inName,"repairAlertClicked") ) { return repairAlertClicked_dyn(); }
		if (HX_FIELD_EQ(inName,"payElectricUtility") ) { return payElectricUtility_dyn(); }
		if (HX_FIELD_EQ(inName,"machineReliability") ) { return machineReliability_dyn(); }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"animateRepairSprite") ) { return animateRepairSprite; }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"electricUtilityTimer") ) { return electricUtilityTimer; }
		break;
	case 21:
		if (HX_FIELD_EQ(inName,"getJuiceMachineSprite") ) { return getJuiceMachineSprite_dyn(); }
		if (HX_FIELD_EQ(inName,"registerMachineEvents") ) { return registerMachineEvents_dyn(); }
		if (HX_FIELD_EQ(inName,"resetRepairAlertTimer") ) { return resetRepairAlertTimer_dyn(); }
		if (HX_FIELD_EQ(inName,"repairSpriteAnimation") ) { return repairSpriteAnimation_dyn(); }
		break;
	case 22:
		if (HX_FIELD_EQ(inName,"juiceMachineBottleAnim") ) { return juiceMachineBottleAnim; }
		if (HX_FIELD_EQ(inName,"getAnimateRepairSprite") ) { return getAnimateRepairSprite_dyn(); }
		if (HX_FIELD_EQ(inName,"setAnimateRepairSprite") ) { return setAnimateRepairSprite_dyn(); }
		break;
	case 23:
		if (HX_FIELD_EQ(inName,"juiceMachineAlertSprite") ) { return juiceMachineAlertSprite; }
		if (HX_FIELD_EQ(inName,"fillingJuiceBottleSound") ) { return fillingJuiceBottleSound; }
		if (HX_FIELD_EQ(inName,"machineReliabilityTimer") ) { return machineReliabilityTimer; }
		if (HX_FIELD_EQ(inName,"unregisterMachineEvents") ) { return unregisterMachineEvents_dyn(); }
		break;
	case 24:
		if (HX_FIELD_EQ(inName,"juiceMachineFlyingBottle") ) { return juiceMachineFlyingBottle; }
		break;
	case 26:
		if (HX_FIELD_EQ(inName,"getJuiceMachineFlyingBotte") ) { return getJuiceMachineFlyingBotte_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic JuiceMachine_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"run") ) { run=inValue.Cast< Float >(); return inValue; }
		break;
	case 4:
		if (HX_FIELD_EQ(inName,"rise") ) { rise=inValue.Cast< Float >(); return inValue; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"myTile") ) { myTile=inValue.Cast< ::Tile >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"goldCount") ) { goldCount=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"machineOn") ) { machineOn=inValue.Cast< bool >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"appleCount") ) { appleCount=inValue.Cast< int >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"myRelativeX") ) { myRelativeX=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"myRelativeY") ) { myRelativeY=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"inJuicePack") ) { inJuicePack=inValue.Cast< bool >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"bottleFlying") ) { bottleFlying=inValue.Cast< bool >(); return inValue; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"fillingBottle") ) { fillingBottle=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"repairClicked") ) { repairClicked=inValue.Cast< bool >(); return inValue; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"minusOneSprite") ) { minusOneSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"machineClicked") ) { machineClicked=inValue.Cast< bool >(); return inValue; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"payElectricBill") ) { payElectricBill=inValue.Cast< bool >(); return inValue; }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"juiceMachineGroup") ) { juiceMachineGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"repairAlertSprite") ) { repairAlertSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"juiceMachineSound") ) { juiceMachineSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		if (HX_FIELD_EQ(inName,"machineAlertReset") ) { machineAlertReset=inValue.Cast< bool >(); return inValue; }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"juiceMachineSprite") ) { juiceMachineSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"animateRepairSprite") ) { animateRepairSprite=inValue.Cast< bool >(); return inValue; }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"electricUtilityTimer") ) { electricUtilityTimer=inValue.Cast< Float >(); return inValue; }
		break;
	case 22:
		if (HX_FIELD_EQ(inName,"juiceMachineBottleAnim") ) { juiceMachineBottleAnim=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 23:
		if (HX_FIELD_EQ(inName,"juiceMachineAlertSprite") ) { juiceMachineAlertSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"fillingJuiceBottleSound") ) { fillingJuiceBottleSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		if (HX_FIELD_EQ(inName,"machineReliabilityTimer") ) { machineReliabilityTimer=inValue.Cast< Float >(); return inValue; }
		break;
	case 24:
		if (HX_FIELD_EQ(inName,"juiceMachineFlyingBottle") ) { juiceMachineFlyingBottle=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void JuiceMachine_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("juiceMachineGroup"));
	outFields->push(HX_CSTRING("juiceMachineSprite"));
	outFields->push(HX_CSTRING("juiceMachineAlertSprite"));
	outFields->push(HX_CSTRING("juiceMachineBottleAnim"));
	outFields->push(HX_CSTRING("juiceMachineFlyingBottle"));
	outFields->push(HX_CSTRING("minusOneSprite"));
	outFields->push(HX_CSTRING("repairAlertSprite"));
	outFields->push(HX_CSTRING("fillingJuiceBottleSound"));
	outFields->push(HX_CSTRING("juiceMachineSound"));
	outFields->push(HX_CSTRING("myRelativeX"));
	outFields->push(HX_CSTRING("myRelativeY"));
	outFields->push(HX_CSTRING("rise"));
	outFields->push(HX_CSTRING("run"));
	outFields->push(HX_CSTRING("electricUtilityTimer"));
	outFields->push(HX_CSTRING("machineReliabilityTimer"));
	outFields->push(HX_CSTRING("goldCount"));
	outFields->push(HX_CSTRING("appleCount"));
	outFields->push(HX_CSTRING("payElectricBill"));
	outFields->push(HX_CSTRING("machineOn"));
	outFields->push(HX_CSTRING("machineAlertReset"));
	outFields->push(HX_CSTRING("machineClicked"));
	outFields->push(HX_CSTRING("fillingBottle"));
	outFields->push(HX_CSTRING("repairClicked"));
	outFields->push(HX_CSTRING("animateRepairSprite"));
	outFields->push(HX_CSTRING("bottleFlying"));
	outFields->push(HX_CSTRING("inJuicePack"));
	outFields->push(HX_CSTRING("myTile"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(JuiceMachine_obj,juiceMachineGroup),HX_CSTRING("juiceMachineGroup")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(JuiceMachine_obj,juiceMachineSprite),HX_CSTRING("juiceMachineSprite")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(JuiceMachine_obj,juiceMachineAlertSprite),HX_CSTRING("juiceMachineAlertSprite")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(JuiceMachine_obj,juiceMachineBottleAnim),HX_CSTRING("juiceMachineBottleAnim")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(JuiceMachine_obj,juiceMachineFlyingBottle),HX_CSTRING("juiceMachineFlyingBottle")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(JuiceMachine_obj,minusOneSprite),HX_CSTRING("minusOneSprite")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(JuiceMachine_obj,repairAlertSprite),HX_CSTRING("repairAlertSprite")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(JuiceMachine_obj,fillingJuiceBottleSound),HX_CSTRING("fillingJuiceBottleSound")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(JuiceMachine_obj,juiceMachineSound),HX_CSTRING("juiceMachineSound")},
	{hx::fsFloat,(int)offsetof(JuiceMachine_obj,myRelativeX),HX_CSTRING("myRelativeX")},
	{hx::fsFloat,(int)offsetof(JuiceMachine_obj,myRelativeY),HX_CSTRING("myRelativeY")},
	{hx::fsFloat,(int)offsetof(JuiceMachine_obj,rise),HX_CSTRING("rise")},
	{hx::fsFloat,(int)offsetof(JuiceMachine_obj,run),HX_CSTRING("run")},
	{hx::fsFloat,(int)offsetof(JuiceMachine_obj,electricUtilityTimer),HX_CSTRING("electricUtilityTimer")},
	{hx::fsFloat,(int)offsetof(JuiceMachine_obj,machineReliabilityTimer),HX_CSTRING("machineReliabilityTimer")},
	{hx::fsInt,(int)offsetof(JuiceMachine_obj,goldCount),HX_CSTRING("goldCount")},
	{hx::fsInt,(int)offsetof(JuiceMachine_obj,appleCount),HX_CSTRING("appleCount")},
	{hx::fsBool,(int)offsetof(JuiceMachine_obj,payElectricBill),HX_CSTRING("payElectricBill")},
	{hx::fsBool,(int)offsetof(JuiceMachine_obj,machineOn),HX_CSTRING("machineOn")},
	{hx::fsBool,(int)offsetof(JuiceMachine_obj,machineAlertReset),HX_CSTRING("machineAlertReset")},
	{hx::fsBool,(int)offsetof(JuiceMachine_obj,machineClicked),HX_CSTRING("machineClicked")},
	{hx::fsBool,(int)offsetof(JuiceMachine_obj,fillingBottle),HX_CSTRING("fillingBottle")},
	{hx::fsBool,(int)offsetof(JuiceMachine_obj,repairClicked),HX_CSTRING("repairClicked")},
	{hx::fsBool,(int)offsetof(JuiceMachine_obj,animateRepairSprite),HX_CSTRING("animateRepairSprite")},
	{hx::fsBool,(int)offsetof(JuiceMachine_obj,bottleFlying),HX_CSTRING("bottleFlying")},
	{hx::fsBool,(int)offsetof(JuiceMachine_obj,inJuicePack),HX_CSTRING("inJuicePack")},
	{hx::fsObject /*::Tile*/ ,(int)offsetof(JuiceMachine_obj,myTile),HX_CSTRING("myTile")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("juiceMachineGroup"),
	HX_CSTRING("juiceMachineSprite"),
	HX_CSTRING("juiceMachineAlertSprite"),
	HX_CSTRING("juiceMachineBottleAnim"),
	HX_CSTRING("juiceMachineFlyingBottle"),
	HX_CSTRING("minusOneSprite"),
	HX_CSTRING("repairAlertSprite"),
	HX_CSTRING("fillingJuiceBottleSound"),
	HX_CSTRING("juiceMachineSound"),
	HX_CSTRING("myRelativeX"),
	HX_CSTRING("myRelativeY"),
	HX_CSTRING("rise"),
	HX_CSTRING("run"),
	HX_CSTRING("electricUtilityTimer"),
	HX_CSTRING("machineReliabilityTimer"),
	HX_CSTRING("goldCount"),
	HX_CSTRING("appleCount"),
	HX_CSTRING("payElectricBill"),
	HX_CSTRING("machineOn"),
	HX_CSTRING("machineAlertReset"),
	HX_CSTRING("machineClicked"),
	HX_CSTRING("fillingBottle"),
	HX_CSTRING("repairClicked"),
	HX_CSTRING("animateRepairSprite"),
	HX_CSTRING("bottleFlying"),
	HX_CSTRING("inJuicePack"),
	HX_CSTRING("myTile"),
	HX_CSTRING("getMyTile"),
	HX_CSTRING("getInJuicePack"),
	HX_CSTRING("getJuiceMachineFlyingBotte"),
	HX_CSTRING("getJuiceMachineSprite"),
	HX_CSTRING("getMyRelativeX"),
	HX_CSTRING("getMyRelativeY"),
	HX_CSTRING("getMachineOn"),
	HX_CSTRING("getFillingBottle"),
	HX_CSTRING("getPayElectricBill"),
	HX_CSTRING("getRepairClicked"),
	HX_CSTRING("getAnimateRepairSprite"),
	HX_CSTRING("getBottleFlying"),
	HX_CSTRING("setAnimateRepairSprite"),
	HX_CSTRING("setInJuicePack"),
	HX_CSTRING("setRepairClicked"),
	HX_CSTRING("setPayElectricBill"),
	HX_CSTRING("setFillingBottle"),
	HX_CSTRING("setMachineOn"),
	HX_CSTRING("setMyRelativeX"),
	HX_CSTRING("setMyRelativeY"),
	HX_CSTRING("setGoldCount"),
	HX_CSTRING("setAppleCount"),
	HX_CSTRING("setBottleFlying"),
	HX_CSTRING("registerMachineEvents"),
	HX_CSTRING("unregisterMachineEvents"),
	HX_CSTRING("repairAlertClicked"),
	HX_CSTRING("turnMachineOn"),
	HX_CSTRING("bottleFadeIn"),
	HX_CSTRING("payElectricUtility"),
	HX_CSTRING("machineReliability"),
	HX_CSTRING("resetRepairAlertTimer"),
	HX_CSTRING("repairSpriteAnimation"),
	HX_CSTRING("setRiseRun"),
	HX_CSTRING("sendBottleToPack"),
	HX_CSTRING("update"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(JuiceMachine_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(JuiceMachine_obj::__mClass,"__mClass");
};

#endif

Class JuiceMachine_obj::__mClass;

void JuiceMachine_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("JuiceMachine"), hx::TCanCast< JuiceMachine_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void JuiceMachine_obj::__boot()
{
}

