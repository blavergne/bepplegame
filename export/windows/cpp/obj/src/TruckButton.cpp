#include <hxcpp.h>

#ifndef INCLUDED_TruckButton
#include <TruckButton.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif

Void TruckButton_obj::__construct()
{
HX_STACK_FRAME("TruckButton","new",0x53410843,"TruckButton.new","TruckButton.hx",9,0x23f5f08d)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(15)
	this->buttonClickable = false;
	HX_STACK_LINE(14)
	this->buttonLevel = (int)0;
	HX_STACK_LINE(19)
	super::__construct(null());
	HX_STACK_LINE(20)
	this->initTruckButton();
}
;
	return null();
}

//TruckButton_obj::~TruckButton_obj() { }

Dynamic TruckButton_obj::__CreateEmpty() { return  new TruckButton_obj; }
hx::ObjectPtr< TruckButton_obj > TruckButton_obj::__new()
{  hx::ObjectPtr< TruckButton_obj > result = new TruckButton_obj();
	result->__construct();
	return result;}

Dynamic TruckButton_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< TruckButton_obj > result = new TruckButton_obj();
	result->__construct();
	return result;}

Void TruckButton_obj::initTruckButton( ){
{
		HX_STACK_FRAME("TruckButton","initTruckButton",0xc05baa24,"TruckButton.initTruckButton","TruckButton.hx",23,0x23f5f08d)
		HX_STACK_THIS(this)
		HX_STACK_LINE(24)
		::flixel::group::FlxGroup _g = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(24)
		this->truckButtonGroup = _g;
		HX_STACK_LINE(25)
		::flixel::FlxSprite _g1 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(25)
		this->truckButton = _g1;
		HX_STACK_LINE(26)
		this->truckButton->loadGraphic(HX_CSTRING("assets/images/appleTruckButtonOnAnimation.png"),true,(int)24,(int)16,null(),null());
		HX_STACK_LINE(27)
		this->truckButton->animation->add(HX_CSTRING("truckButtonA"),Array_obj< int >::__new().Add((int)0),(int)1,false);
		HX_STACK_LINE(28)
		this->truckButton->animation->add(HX_CSTRING("truckButtonB"),Array_obj< int >::__new().Add((int)5),(int)1,false);
		HX_STACK_LINE(29)
		this->truckButton->animation->add(HX_CSTRING("truckButtonC"),Array_obj< int >::__new().Add((int)0),(int)1,false);
		HX_STACK_LINE(30)
		this->truckButton->set_x((int)50);
		HX_STACK_LINE(31)
		this->truckButton->set_y((int)220);
		HX_STACK_LINE(32)
		this->truckButtonGroup->add(this->truckButton);
		HX_STACK_LINE(33)
		this->add(this->truckButtonGroup);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(TruckButton_obj,initTruckButton,(void))

::flixel::FlxSprite TruckButton_obj::getTruckButtonSprite( ){
	HX_STACK_FRAME("TruckButton","getTruckButtonSprite",0x45a61d1d,"TruckButton.getTruckButtonSprite","TruckButton.hx",37,0x23f5f08d)
	HX_STACK_THIS(this)
	HX_STACK_LINE(37)
	return this->truckButton;
}


HX_DEFINE_DYNAMIC_FUNC0(TruckButton_obj,getTruckButtonSprite,return )

bool TruckButton_obj::getButtonClickable( ){
	HX_STACK_FRAME("TruckButton","getButtonClickable",0x69551d17,"TruckButton.getButtonClickable","TruckButton.hx",41,0x23f5f08d)
	HX_STACK_THIS(this)
	HX_STACK_LINE(41)
	return this->buttonClickable;
}


HX_DEFINE_DYNAMIC_FUNC0(TruckButton_obj,getButtonClickable,return )

Void TruckButton_obj::setButtonClickable( bool val){
{
		HX_STACK_FRAME("TruckButton","setButtonClickable",0x46044f8b,"TruckButton.setButtonClickable","TruckButton.hx",45,0x23f5f08d)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(45)
		this->buttonClickable = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(TruckButton_obj,setButtonClickable,(void))

Void TruckButton_obj::setButtonLevel( int val){
{
		HX_STACK_FRAME("TruckButton","setButtonLevel",0x3f4a6ead,"TruckButton.setButtonLevel","TruckButton.hx",49,0x23f5f08d)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(49)
		this->buttonLevel = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(TruckButton_obj,setButtonLevel,(void))

Void TruckButton_obj::buttonSetting( ){
{
		HX_STACK_FRAME("TruckButton","buttonSetting",0x7177bb61,"TruckButton.buttonSetting","TruckButton.hx",53,0x23f5f08d)
		HX_STACK_THIS(this)
		HX_STACK_LINE(53)
		int _g = this->buttonLevel;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(53)
		switch( (int)(_g)){
			case (int)0: {
				HX_STACK_LINE(55)
				this->truckButton->animation->play(HX_CSTRING("truckButtonA"),null(),null());
			}
			;break;
			case (int)1: {
				HX_STACK_LINE(57)
				this->truckButton->animation->play(HX_CSTRING("truckButtonB"),null(),null());
			}
			;break;
			case (int)2: {
				HX_STACK_LINE(59)
				this->truckButton->animation->play(HX_CSTRING("truckButtonC"),null(),null());
			}
			;break;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(TruckButton_obj,buttonSetting,(void))

Void TruckButton_obj::update( ){
{
		HX_STACK_FRAME("TruckButton","update",0xf7001e06,"TruckButton.update","TruckButton.hx",65,0x23f5f08d)
		HX_STACK_THIS(this)
		HX_STACK_LINE(65)
		this->super::update();
	}
return null();
}



TruckButton_obj::TruckButton_obj()
{
}

void TruckButton_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(TruckButton);
	HX_MARK_MEMBER_NAME(truckButtonGroup,"truckButtonGroup");
	HX_MARK_MEMBER_NAME(truckButton,"truckButton");
	HX_MARK_MEMBER_NAME(buttonLevel,"buttonLevel");
	HX_MARK_MEMBER_NAME(buttonClickable,"buttonClickable");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void TruckButton_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(truckButtonGroup,"truckButtonGroup");
	HX_VISIT_MEMBER_NAME(truckButton,"truckButton");
	HX_VISIT_MEMBER_NAME(buttonLevel,"buttonLevel");
	HX_VISIT_MEMBER_NAME(buttonClickable,"buttonClickable");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic TruckButton_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"truckButton") ) { return truckButton; }
		if (HX_FIELD_EQ(inName,"buttonLevel") ) { return buttonLevel; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"buttonSetting") ) { return buttonSetting_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"setButtonLevel") ) { return setButtonLevel_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"buttonClickable") ) { return buttonClickable; }
		if (HX_FIELD_EQ(inName,"initTruckButton") ) { return initTruckButton_dyn(); }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"truckButtonGroup") ) { return truckButtonGroup; }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"getButtonClickable") ) { return getButtonClickable_dyn(); }
		if (HX_FIELD_EQ(inName,"setButtonClickable") ) { return setButtonClickable_dyn(); }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"getTruckButtonSprite") ) { return getTruckButtonSprite_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic TruckButton_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 11:
		if (HX_FIELD_EQ(inName,"truckButton") ) { truckButton=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"buttonLevel") ) { buttonLevel=inValue.Cast< int >(); return inValue; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"buttonClickable") ) { buttonClickable=inValue.Cast< bool >(); return inValue; }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"truckButtonGroup") ) { truckButtonGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void TruckButton_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("truckButtonGroup"));
	outFields->push(HX_CSTRING("truckButton"));
	outFields->push(HX_CSTRING("buttonLevel"));
	outFields->push(HX_CSTRING("buttonClickable"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(TruckButton_obj,truckButtonGroup),HX_CSTRING("truckButtonGroup")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(TruckButton_obj,truckButton),HX_CSTRING("truckButton")},
	{hx::fsInt,(int)offsetof(TruckButton_obj,buttonLevel),HX_CSTRING("buttonLevel")},
	{hx::fsBool,(int)offsetof(TruckButton_obj,buttonClickable),HX_CSTRING("buttonClickable")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("truckButtonGroup"),
	HX_CSTRING("truckButton"),
	HX_CSTRING("buttonLevel"),
	HX_CSTRING("buttonClickable"),
	HX_CSTRING("initTruckButton"),
	HX_CSTRING("getTruckButtonSprite"),
	HX_CSTRING("getButtonClickable"),
	HX_CSTRING("setButtonClickable"),
	HX_CSTRING("setButtonLevel"),
	HX_CSTRING("buttonSetting"),
	HX_CSTRING("update"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(TruckButton_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(TruckButton_obj::__mClass,"__mClass");
};

#endif

Class TruckButton_obj::__mClass;

void TruckButton_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("TruckButton"), hx::TCanCast< TruckButton_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void TruckButton_obj::__boot()
{
}

