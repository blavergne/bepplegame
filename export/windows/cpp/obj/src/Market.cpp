#include <hxcpp.h>

#ifndef INCLUDED_GoldCoin
#include <GoldCoin.h>
#endif
#ifndef INCLUDED_JuiceItem
#include <JuiceItem.h>
#endif
#ifndef INCLUDED_Market
#include <Market.h>
#endif
#ifndef INCLUDED_MarketItem
#include <MarketItem.h>
#endif
#ifndef INCLUDED_PieItem
#include <PieItem.h>
#endif
#ifndef INCLUDED_SeedItem
#include <SeedItem.h>
#endif
#ifndef INCLUDED_Std
#include <Std.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_plugin_FlxPlugin
#include <flixel/plugin/FlxPlugin.h>
#endif
#ifndef INCLUDED_flixel_plugin_MouseEventManager
#include <flixel/plugin/MouseEventManager.h>
#endif
#ifndef INCLUDED_flixel_system_FlxSound
#include <flixel/system/FlxSound.h>
#endif
#ifndef INCLUDED_flixel_system_frontEnds_SoundFrontEnd
#include <flixel/system/frontEnds/SoundFrontEnd.h>
#endif
#ifndef INCLUDED_flixel_text_FlxText
#include <flixel/text/FlxText.h>
#endif
#ifndef INCLUDED_flixel_util_FlxDestroyUtil
#include <flixel/util/FlxDestroyUtil.h>
#endif
#ifndef INCLUDED_haxe_Log
#include <haxe/Log.h>
#endif
#ifndef INCLUDED_openfl__v2_events_EventDispatcher
#include <openfl/_v2/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__v2_events_IEventDispatcher
#include <openfl/_v2/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__v2_media_SoundChannel
#include <openfl/_v2/media/SoundChannel.h>
#endif

Void Market_obj::__construct()
{
HX_STACK_FRAME("Market","new",0xe910a8ce,"Market.new","Market.hx",14,0xdfc0ea62)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(56)
	this->turn = true;
	HX_STACK_LINE(55)
	this->marketOpen = false;
	HX_STACK_LINE(54)
	this->electricBillTimer = (int)10;
	HX_STACK_LINE(53)
	this->waterBillTimer = (int)30;
	HX_STACK_LINE(52)
	this->replenishPieMachineSeedTimer = (int)300;
	HX_STACK_LINE(51)
	this->replenishJuiceMachineTimer = (int)300;
	HX_STACK_LINE(50)
	this->replenishSeedTimer = (int)180;
	HX_STACK_LINE(49)
	this->marketCloseTimer = (int)10;
	HX_STACK_LINE(45)
	this->seedCountString = HX_CSTRING("");
	HX_STACK_LINE(44)
	this->shiftY = (int)2;
	HX_STACK_LINE(43)
	this->shiftX = (int)72;
	HX_STACK_LINE(42)
	this->electricClientCount = (int)0;
	HX_STACK_LINE(41)
	this->waterClientCount = (int)0;
	HX_STACK_LINE(40)
	this->pieCount = (int)0;
	HX_STACK_LINE(39)
	this->juiceCount = (int)0;
	HX_STACK_LINE(38)
	this->seedCount = (int)1;
	HX_STACK_LINE(37)
	this->juicePack = (int)0;
	HX_STACK_LINE(36)
	this->piePack = (int)0;
	HX_STACK_LINE(34)
	this->coinCountString = HX_CSTRING("");
	HX_STACK_LINE(33)
	this->coinCount = (int)0;
	HX_STACK_LINE(62)
	super::__construct(null());
	HX_STACK_LINE(63)
	::flixel::group::FlxGroup _g = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(63)
	this->marketGroup = _g;
	HX_STACK_LINE(64)
	::flixel::FlxSprite _g1 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
	HX_STACK_LINE(64)
	this->marketSideBarSprite = _g1;
	HX_STACK_LINE(65)
	::SeedItem _g2 = ::SeedItem_obj::__new();		HX_STACK_VAR(_g2,"_g2");
	HX_STACK_LINE(65)
	this->seedItem = _g2;
	HX_STACK_LINE(66)
	::JuiceItem _g3 = ::JuiceItem_obj::__new();		HX_STACK_VAR(_g3,"_g3");
	HX_STACK_LINE(66)
	this->juiceItem = _g3;
	HX_STACK_LINE(67)
	::PieItem _g4 = ::PieItem_obj::__new();		HX_STACK_VAR(_g4,"_g4");
	HX_STACK_LINE(67)
	this->pieItem = _g4;
	HX_STACK_LINE(68)
	::GoldCoin _g5 = ::GoldCoin_obj::__new();		HX_STACK_VAR(_g5,"_g5");
	HX_STACK_LINE(68)
	this->coin = _g5;
	HX_STACK_LINE(69)
	::flixel::text::FlxText _g6 = ::flixel::text::FlxText_obj::__new((int)33,(int)90,(int)16,this->seedCountString,(int)8,false);		HX_STACK_VAR(_g6,"_g6");
	HX_STACK_LINE(69)
	this->seedCountText = _g6;
	HX_STACK_LINE(70)
	::String _g7 = ::Std_obj::string(this->juiceCount);		HX_STACK_VAR(_g7,"_g7");
	HX_STACK_LINE(70)
	::flixel::text::FlxText _g8 = ::flixel::text::FlxText_obj::__new((int)61,(int)90,(int)16,_g7,(int)8,false);		HX_STACK_VAR(_g8,"_g8");
	HX_STACK_LINE(70)
	this->juiceCountText = _g8;
	HX_STACK_LINE(71)
	::String _g9 = ::Std_obj::string(this->pieCount);		HX_STACK_VAR(_g9,"_g9");
	HX_STACK_LINE(71)
	::flixel::text::FlxText _g10 = ::flixel::text::FlxText_obj::__new((int)93,(int)90,(int)16,_g9,(int)8,false);		HX_STACK_VAR(_g10,"_g10");
	HX_STACK_LINE(71)
	this->pieCountText = _g10;
	HX_STACK_LINE(72)
	::String _g11 = ::Std_obj::string(this->juicePack);		HX_STACK_VAR(_g11,"_g11");
	HX_STACK_LINE(72)
	::flixel::text::FlxText _g12 = ::flixel::text::FlxText_obj::__new((int)180,(int)43,(int)32,_g11,(int)12,false);		HX_STACK_VAR(_g12,"_g12");
	HX_STACK_LINE(72)
	this->juicePackCountText = _g12;
	HX_STACK_LINE(73)
	::String _g13 = ::Std_obj::string(this->juicePack);		HX_STACK_VAR(_g13,"_g13");
	HX_STACK_LINE(73)
	::flixel::text::FlxText _g14 = ::flixel::text::FlxText_obj::__new((int)237,(int)43,(int)32,_g13,(int)12,false);		HX_STACK_VAR(_g14,"_g14");
	HX_STACK_LINE(73)
	this->piePackCountText = _g14;
	HX_STACK_LINE(74)
	this->seedCountText->set_color((int)268435455);
	HX_STACK_LINE(75)
	this->juiceCountText->set_color((int)268435455);
	HX_STACK_LINE(76)
	this->pieCountText->set_color((int)268435455);
	HX_STACK_LINE(77)
	this->piePackCountText->set_color((int)268435455);
	HX_STACK_LINE(78)
	this->juicePackCountText->set_color((int)268435455);
	HX_STACK_LINE(79)
	::flixel::text::FlxText _g15 = ::flixel::text::FlxText_obj::__new((int)53,(int)38,(int)64,this->coinCountString,(int)16,false);		HX_STACK_VAR(_g15,"_g15");
	HX_STACK_LINE(79)
	this->coinCountFieldText = _g15;
	HX_STACK_LINE(80)
	this->coinCountFieldText->set_color((int)268435455);
	HX_STACK_LINE(81)
	Array< ::Dynamic > _g16 = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(_g16,"_g16");
	HX_STACK_LINE(81)
	this->itemArray = _g16;
	HX_STACK_LINE(82)
	this->marketSideBarSprite->loadGraphic(HX_CSTRING("assets/images/marketSideBar.png"),false,null(),null(),null(),null());
	HX_STACK_LINE(83)
	this->marketSideBarSprite->set_x((int)575);
	HX_STACK_LINE(84)
	int _g17 = this->seedItem->getItemCount();		HX_STACK_VAR(_g17,"_g17");
	HX_STACK_LINE(84)
	::String _g18 = ::Std_obj::string(_g17);		HX_STACK_VAR(_g18,"_g18");
	HX_STACK_LINE(84)
	::flixel::text::FlxText _g19 = ::flixel::text::FlxText_obj::__new((this->marketSideBarSprite->x + (int)91),(this->marketSideBarSprite->y + (int)2),(int)32,_g18,(int)8,null());		HX_STACK_VAR(_g19,"_g19");
	HX_STACK_LINE(84)
	this->seedItemCountFieldText = _g19;
	HX_STACK_LINE(85)
	int _g20 = this->juiceItem->getItemCount();		HX_STACK_VAR(_g20,"_g20");
	HX_STACK_LINE(85)
	::String _g21 = ::Std_obj::string(_g20);		HX_STACK_VAR(_g21,"_g21");
	HX_STACK_LINE(85)
	::flixel::text::FlxText _g22 = ::flixel::text::FlxText_obj::__new((this->marketSideBarSprite->x + (int)91),(this->marketSideBarSprite->y + (int)38),(int)32,_g21,(int)8,null());		HX_STACK_VAR(_g22,"_g22");
	HX_STACK_LINE(85)
	this->juiceItemCountFieldText = _g22;
	HX_STACK_LINE(86)
	int _g23 = this->juiceItem->getItemCount();		HX_STACK_VAR(_g23,"_g23");
	HX_STACK_LINE(86)
	::String _g24 = ::Std_obj::string(_g23);		HX_STACK_VAR(_g24,"_g24");
	HX_STACK_LINE(86)
	::flixel::text::FlxText _g25 = ::flixel::text::FlxText_obj::__new((this->marketSideBarSprite->x + (int)91),(this->marketSideBarSprite->y + (int)74),(int)32,_g24,(int)8,null());		HX_STACK_VAR(_g25,"_g25");
	HX_STACK_LINE(86)
	this->pieItemCountFieldText = _g25;
	HX_STACK_LINE(87)
	int _g26 = this->seedItem->getItemCost();		HX_STACK_VAR(_g26,"_g26");
	HX_STACK_LINE(87)
	::String _g27 = ::Std_obj::string(_g26);		HX_STACK_VAR(_g27,"_g27");
	HX_STACK_LINE(87)
	::flixel::text::FlxText _g28 = ::flixel::text::FlxText_obj::__new((this->marketSideBarSprite->x + (int)73),(this->marketSideBarSprite->y + (int)2),(int)32,_g27,(int)8,null());		HX_STACK_VAR(_g28,"_g28");
	HX_STACK_LINE(87)
	this->seedItemCostFieldText = _g28;
	HX_STACK_LINE(88)
	int _g29 = this->juiceItem->getItemCost();		HX_STACK_VAR(_g29,"_g29");
	HX_STACK_LINE(88)
	::String _g30 = ::Std_obj::string(_g29);		HX_STACK_VAR(_g30,"_g30");
	HX_STACK_LINE(88)
	::flixel::text::FlxText _g31 = ::flixel::text::FlxText_obj::__new((this->marketSideBarSprite->x + (int)72),(this->marketSideBarSprite->y + (int)38),(int)32,_g30,(int)8,null());		HX_STACK_VAR(_g31,"_g31");
	HX_STACK_LINE(88)
	this->juiceItemCostFieldText = _g31;
	HX_STACK_LINE(89)
	int _g32 = this->juiceItem->getItemCost();		HX_STACK_VAR(_g32,"_g32");
	HX_STACK_LINE(89)
	::String _g33 = ::Std_obj::string(_g32);		HX_STACK_VAR(_g33,"_g33");
	HX_STACK_LINE(89)
	::flixel::text::FlxText _g34 = ::flixel::text::FlxText_obj::__new((this->marketSideBarSprite->x + (int)72),(this->marketSideBarSprite->y + (int)74),(int)32,_g33,(int)8,null());		HX_STACK_VAR(_g34,"_g34");
	HX_STACK_LINE(89)
	this->pieItemCostFieldText = _g34;
	HX_STACK_LINE(90)
	this->seedItemCountFieldText->set_alpha(.2);
	HX_STACK_LINE(91)
	this->juiceItemCountFieldText->set_alpha(.2);
	HX_STACK_LINE(92)
	this->pieItemCountFieldText->set_alpha(.2);
	HX_STACK_LINE(93)
	this->seedItemCountFieldText->set_color((int)0);
	HX_STACK_LINE(94)
	this->juiceItemCountFieldText->set_color((int)0);
	HX_STACK_LINE(95)
	this->pieItemCountFieldText->set_color((int)0);
	HX_STACK_LINE(96)
	this->seedItemCostFieldText->set_color((int)13421649);
	HX_STACK_LINE(97)
	this->juiceItemCostFieldText->set_color((int)13421649);
	HX_STACK_LINE(98)
	this->pieItemCostFieldText->set_color((int)13421649);
	HX_STACK_LINE(99)
	this->marketGroup->add(this->marketSideBarSprite);
	HX_STACK_LINE(100)
	this->marketGroup->add(this->coin);
	HX_STACK_LINE(101)
	this->fillItemArray();
	HX_STACK_LINE(102)
	this->registerEvents();
	HX_STACK_LINE(103)
	this->positionArrayItems();
	HX_STACK_LINE(104)
	this->add(this->marketGroup);
}
;
	return null();
}

//Market_obj::~Market_obj() { }

Dynamic Market_obj::__CreateEmpty() { return  new Market_obj; }
hx::ObjectPtr< Market_obj > Market_obj::__new()
{  hx::ObjectPtr< Market_obj > result = new Market_obj();
	result->__construct();
	return result;}

Dynamic Market_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Market_obj > result = new Market_obj();
	result->__construct();
	return result;}

bool Market_obj::getMarketOpen( ){
	HX_STACK_FRAME("Market","getMarketOpen",0x1a707dca,"Market.getMarketOpen","Market.hx",108,0xdfc0ea62)
	HX_STACK_THIS(this)
	HX_STACK_LINE(108)
	return this->marketOpen;
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,getMarketOpen,return )

int Market_obj::getCoinCount( ){
	HX_STACK_FRAME("Market","getCoinCount",0xeb9977da,"Market.getCoinCount","Market.hx",112,0xdfc0ea62)
	HX_STACK_THIS(this)
	HX_STACK_LINE(112)
	return this->coinCount;
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,getCoinCount,return )

int Market_obj::getSeedCount( ){
	HX_STACK_FRAME("Market","getSeedCount",0x38d01bfa,"Market.getSeedCount","Market.hx",116,0xdfc0ea62)
	HX_STACK_THIS(this)
	HX_STACK_LINE(116)
	return this->seedCount;
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,getSeedCount,return )

int Market_obj::getJuiceCount( ){
	HX_STACK_FRAME("Market","getJuiceCount",0xe383cb73,"Market.getJuiceCount","Market.hx",120,0xdfc0ea62)
	HX_STACK_THIS(this)
	HX_STACK_LINE(120)
	return this->juiceCount;
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,getJuiceCount,return )

int Market_obj::getPieCount( ){
	HX_STACK_FRAME("Market","getPieCount",0xd2ff8207,"Market.getPieCount","Market.hx",124,0xdfc0ea62)
	HX_STACK_THIS(this)
	HX_STACK_LINE(124)
	return this->pieCount;
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,getPieCount,return )

int Market_obj::getJuicePackCount( ){
	HX_STACK_FRAME("Market","getJuicePackCount",0x78db13da,"Market.getJuicePackCount","Market.hx",128,0xdfc0ea62)
	HX_STACK_THIS(this)
	HX_STACK_LINE(128)
	return this->juicePack;
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,getJuicePackCount,return )

int Market_obj::getPiePackCount( ){
	HX_STACK_FRAME("Market","getPiePackCount",0x66b7a46e,"Market.getPiePackCount","Market.hx",132,0xdfc0ea62)
	HX_STACK_THIS(this)
	HX_STACK_LINE(132)
	return this->piePack;
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,getPiePackCount,return )

Void Market_obj::stockJuicePacks( int val){
{
		HX_STACK_FRAME("Market","stockJuicePacks",0x5c3a5bde,"Market.stockJuicePacks","Market.hx",136,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(136)
		hx::AddEq(this->juicePack,val);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,stockJuicePacks,(void))

Void Market_obj::stockPiePacks( int val){
{
		HX_STACK_FRAME("Market","stockPiePacks",0xdebb2272,"Market.stockPiePacks","Market.hx",140,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(140)
		hx::AddEq(this->piePack,val);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,stockPiePacks,(void))

Void Market_obj::loadJuicePacksDec( ){
{
		HX_STACK_FRAME("Market","loadJuicePacksDec",0x6ea818d0,"Market.loadJuicePacksDec","Market.hx",144,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(144)
		hx::SubEq(this->juicePack,(int)10);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,loadJuicePacksDec,(void))

Void Market_obj::loadPiePacksDec( ){
{
		HX_STACK_FRAME("Market","loadPiePacksDec",0x5178bbbc,"Market.loadPiePacksDec","Market.hx",148,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(148)
		hx::SubEq(this->piePack,(int)10);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,loadPiePacksDec,(void))

Void Market_obj::setJuicePackCount( int val){
{
		HX_STACK_FRAME("Market","setJuicePackCount",0x9c48ebe6,"Market.setJuicePackCount","Market.hx",152,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(152)
		this->juicePack = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setJuicePackCount,(void))

Void Market_obj::setPiePackCount( int val){
{
		HX_STACK_FRAME("Market","setPiePackCount",0x6283217a,"Market.setPiePackCount","Market.hx",156,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(156)
		this->piePack = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setPiePackCount,(void))

Void Market_obj::setSeedCount( int val){
{
		HX_STACK_FRAME("Market","setSeedCount",0x4dc93f6e,"Market.setSeedCount","Market.hx",160,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(160)
		this->seedCount = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setSeedCount,(void))

Void Market_obj::setseedItemCount( int val){
{
		HX_STACK_FRAME("Market","setseedItemCount",0xd5ff90fb,"Market.setseedItemCount","Market.hx",164,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(164)
		this->seedItem->setSeedItemCount(val);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setseedItemCount,(void))

Void Market_obj::setPieCount( int val){
{
		HX_STACK_FRAME("Market","setPieCount",0xdd6c8913,"Market.setPieCount","Market.hx",168,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(168)
		this->pieCount = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setPieCount,(void))

Void Market_obj::setPieItemCount( int val){
{
		HX_STACK_FRAME("Market","setPieItemCount",0xfc896100,"Market.setPieItemCount","Market.hx",172,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(172)
		this->pieItem->setPieItemCount(val);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setPieItemCount,(void))

Void Market_obj::setJuiceCount( int val){
{
		HX_STACK_FRAME("Market","setJuiceCount",0x2889ad7f,"Market.setJuiceCount","Market.hx",176,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(176)
		this->juiceCount = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setJuiceCount,(void))

Void Market_obj::setJuiceItemCount( int val){
{
		HX_STACK_FRAME("Market","setJuiceItemCount",0x364f2b6c,"Market.setJuiceItemCount","Market.hx",180,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(180)
		this->juiceItem->setJuiceItemCount(val);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setJuiceItemCount,(void))

Void Market_obj::setCoinCount( int val){
{
		HX_STACK_FRAME("Market","setCoinCount",0x00929b4e,"Market.setCoinCount","Market.hx",184,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(184)
		this->coinCount = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setCoinCount,(void))

Void Market_obj::setWaterClientCount( int val){
{
		HX_STACK_FRAME("Market","setWaterClientCount",0x2fa483dd,"Market.setWaterClientCount","Market.hx",188,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(188)
		this->waterClientCount = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setWaterClientCount,(void))

Void Market_obj::setElectricClientCount( int val){
{
		HX_STACK_FRAME("Market","setElectricClientCount",0xadb90777,"Market.setElectricClientCount","Market.hx",192,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(192)
		this->electricClientCount = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setElectricClientCount,(void))

Void Market_obj::addSubtractCoins( ::String operation,int perTen){
{
		HX_STACK_FRAME("Market","addSubtractCoins",0xfdf613ff,"Market.addSubtractCoins","Market.hx",196,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(operation,"operation")
		HX_STACK_ARG(perTen,"perTen")
		HX_STACK_LINE(196)
		::String _switch_1 = (operation);
		if (  ( _switch_1==HX_CSTRING("+"))){
			HX_STACK_LINE(198)
			hx::AddEq(this->coinCount,perTen);
		}
		else if (  ( _switch_1==HX_CSTRING("-"))){
			HX_STACK_LINE(200)
			hx::SubEq(this->coinCount,perTen);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(Market_obj,addSubtractCoins,(void))

Void Market_obj::displayCoinCount( ){
{
		HX_STACK_FRAME("Market","displayCoinCount",0x761789ae,"Market.displayCoinCount","Market.hx",204,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(205)
		::String _g = ::Std_obj::string(this->coinCount);		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(205)
		this->coinCountFieldText->set_text(_g);
		HX_STACK_LINE(206)
		this->add(this->coinCountFieldText);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,displayCoinCount,(void))

Void Market_obj::displaySeedCount( ){
{
		HX_STACK_FRAME("Market","displaySeedCount",0xc34e2dce,"Market.displaySeedCount","Market.hx",209,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(211)
		::String _g = ::Std_obj::string(this->seedCount);		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(211)
		this->seedCountText->set_text(_g);
		HX_STACK_LINE(212)
		::String _g1 = ::Std_obj::string(this->juiceCount);		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(212)
		this->juiceCountText->set_text(_g1);
		HX_STACK_LINE(213)
		::String _g2 = ::Std_obj::string(this->pieCount);		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(213)
		this->pieCountText->set_text(_g2);
		HX_STACK_LINE(214)
		this->add(this->seedCountText);
		HX_STACK_LINE(215)
		this->add(this->juiceCountText);
		HX_STACK_LINE(216)
		this->add(this->pieCountText);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,displaySeedCount,(void))

Void Market_obj::displayItemCountText( ){
{
		HX_STACK_FRAME("Market","displayItemCountText",0x4217e1b9,"Market.displayItemCountText","Market.hx",220,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(221)
		int _g = this->seedItem->getItemCount();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(221)
		::String _g1 = ::Std_obj::string(_g);		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(221)
		this->seedItemCountFieldText->set_text(_g1);
		HX_STACK_LINE(222)
		int _g2 = this->juiceItem->getItemCount();		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(222)
		::String _g3 = ::Std_obj::string(_g2);		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(222)
		this->juiceItemCountFieldText->set_text(_g3);
		HX_STACK_LINE(223)
		int _g4 = this->seedItem->getItemCost();		HX_STACK_VAR(_g4,"_g4");
		HX_STACK_LINE(223)
		::String _g5 = ::Std_obj::string(_g4);		HX_STACK_VAR(_g5,"_g5");
		HX_STACK_LINE(223)
		this->seedItemCostFieldText->set_text(_g5);
		HX_STACK_LINE(224)
		int _g6 = this->juiceItem->getItemCost();		HX_STACK_VAR(_g6,"_g6");
		HX_STACK_LINE(224)
		::String _g7 = ::Std_obj::string(_g6);		HX_STACK_VAR(_g7,"_g7");
		HX_STACK_LINE(224)
		this->juiceItemCostFieldText->set_text(_g7);
		HX_STACK_LINE(225)
		int _g8 = this->pieItem->getItemCount();		HX_STACK_VAR(_g8,"_g8");
		HX_STACK_LINE(225)
		::String _g9 = ::Std_obj::string(_g8);		HX_STACK_VAR(_g9,"_g9");
		HX_STACK_LINE(225)
		this->pieItemCountFieldText->set_text(_g9);
		HX_STACK_LINE(226)
		int _g10 = this->pieItem->getItemCost();		HX_STACK_VAR(_g10,"_g10");
		HX_STACK_LINE(226)
		::String _g11 = ::Std_obj::string(_g10);		HX_STACK_VAR(_g11,"_g11");
		HX_STACK_LINE(226)
		this->pieItemCostFieldText->set_text(_g11);
		HX_STACK_LINE(227)
		::String _g12 = ::Std_obj::string(this->juicePack);		HX_STACK_VAR(_g12,"_g12");
		HX_STACK_LINE(227)
		this->juicePackCountText->set_text(_g12);
		HX_STACK_LINE(228)
		::String _g13 = ::Std_obj::string(this->piePack);		HX_STACK_VAR(_g13,"_g13");
		HX_STACK_LINE(228)
		this->piePackCountText->set_text(_g13);
		HX_STACK_LINE(229)
		this->marketGroup->add(this->juicePackCountText);
		HX_STACK_LINE(230)
		this->marketGroup->add(this->seedItemCostFieldText);
		HX_STACK_LINE(231)
		this->marketGroup->add(this->juiceItemCostFieldText);
		HX_STACK_LINE(232)
		this->marketGroup->add(this->juiceItemCountFieldText);
		HX_STACK_LINE(233)
		this->marketGroup->add(this->seedItemCountFieldText);
		HX_STACK_LINE(234)
		this->marketGroup->add(this->pieItemCostFieldText);
		HX_STACK_LINE(235)
		this->marketGroup->add(this->pieItemCountFieldText);
		HX_STACK_LINE(236)
		this->marketGroup->add(this->piePackCountText);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,displayItemCountText,(void))

Void Market_obj::decSeedCount( ){
{
		HX_STACK_FRAME("Market","decSeedCount",0xae9fdd4e,"Market.decSeedCount","Market.hx",240,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(240)
		(this->seedCount)--;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,decSeedCount,(void))

Void Market_obj::incSeedCount( ){
{
		HX_STACK_FRAME("Market","incSeedCount",0x7f2ec6b2,"Market.incSeedCount","Market.hx",244,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(244)
		(this->seedCount)++;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,incSeedCount,(void))

Void Market_obj::decjuiceCount( int val){
{
		HX_STACK_FRAME("Market","decjuiceCount",0x8ccc2f7f,"Market.decjuiceCount","Market.hx",248,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(248)
		hx::SubEq(this->juiceCount,val);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,decjuiceCount,(void))

Void Market_obj::decjuicePackCount( int val){
{
		HX_STACK_FRAME("Market","decjuicePackCount",0x60f46de6,"Market.decjuicePackCount","Market.hx",252,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(252)
		hx::SubEq(this->juicePack,val);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,decjuicePackCount,(void))

Void Market_obj::incJuiceCount( ){
{
		HX_STACK_FRAME("Market","incJuiceCount",0x2ffa81bb,"Market.incJuiceCount","Market.hx",256,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(256)
		(this->juiceCount)++;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,incJuiceCount,(void))

Void Market_obj::decPieCount( ){
{
		HX_STACK_FRAME("Market","decPieCount",0x4e5c4733,"Market.decPieCount","Market.hx",260,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(260)
		(this->pieCount)--;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,decPieCount,(void))

Void Market_obj::incPieCount( ){
{
		HX_STACK_FRAME("Market","incPieCount",0x9552964f,"Market.incPieCount","Market.hx",264,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(264)
		(this->pieCount)++;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,incPieCount,(void))

Void Market_obj::coinPaymentUtil( int val){
{
		HX_STACK_FRAME("Market","coinPaymentUtil",0xee8bc985,"Market.coinPaymentUtil","Market.hx",268,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(268)
		hx::SubEq(this->coinCount,val);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,coinPaymentUtil,(void))

Void Market_obj::positionArrayItems( ){
{
		HX_STACK_FRAME("Market","positionArrayItems",0x546a0902,"Market.positionArrayItems","Market.hx",272,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(272)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(272)
		int _g = this->itemArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(272)
		while((true)){
			HX_STACK_LINE(272)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(272)
				break;
			}
			HX_STACK_LINE(272)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(273)
			this->itemArray->__get(i).StaticCast< ::MarketItem >()->getItemSprite()->set_x((this->marketSideBarSprite->x + this->shiftX));
			HX_STACK_LINE(274)
			this->itemArray->__get(i).StaticCast< ::MarketItem >()->getItemSprite()->set_y((this->marketSideBarSprite->y + this->shiftY));
			HX_STACK_LINE(275)
			::flixel::FlxSprite _g2 = this->itemArray->__get(i).StaticCast< ::MarketItem >()->getItemSprite();		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(275)
			this->marketGroup->add(_g2);
			HX_STACK_LINE(276)
			hx::AddEq(this->shiftY,(int)36);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,positionArrayItems,(void))

Void Market_obj::fillItemArray( ){
{
		HX_STACK_FRAME("Market","fillItemArray",0xbc906a71,"Market.fillItemArray","Market.hx",280,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(281)
		this->itemArray[(int)0] = this->seedItem;
		HX_STACK_LINE(282)
		this->itemArray[(int)1] = this->juiceItem;
		HX_STACK_LINE(283)
		this->itemArray[(int)2] = this->pieItem;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,fillItemArray,(void))

Void Market_obj::registerEvents( ){
{
		HX_STACK_FRAME("Market","registerEvents",0xba2f182e,"Market.registerEvents","Market.hx",286,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(287)
		::flixel::plugin::MouseEventManager_obj::add(this->marketSideBarSprite,this->openMarket_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(288)
		{
			HX_STACK_LINE(288)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(288)
			int _g = this->itemArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(288)
			while((true)){
				HX_STACK_LINE(288)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(288)
					break;
				}
				HX_STACK_LINE(288)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(289)
				::flixel::FlxSprite _g2 = this->itemArray->__get(i).StaticCast< ::MarketItem >()->getItemSprite();		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(289)
				::flixel::plugin::MouseEventManager_obj::add(_g2,this->selectItem_dyn(),null(),null(),null(),null(),null(),null());
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,registerEvents,(void))

Void Market_obj::unregisterEvents( ){
{
		HX_STACK_FRAME("Market","unregisterEvents",0xa06340c7,"Market.unregisterEvents","Market.hx",293,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(294)
		::flixel::plugin::MouseEventManager_obj::remove(this->marketSideBarSprite);
		HX_STACK_LINE(295)
		{
			HX_STACK_LINE(295)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(295)
			int _g = this->itemArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(295)
			while((true)){
				HX_STACK_LINE(295)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(295)
					break;
				}
				HX_STACK_LINE(295)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(296)
				::flixel::FlxSprite _g2 = this->itemArray->__get(i).StaticCast< ::MarketItem >()->getItemSprite();		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(296)
				::flixel::plugin::MouseEventManager_obj::remove(_g2);
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,unregisterEvents,(void))

Void Market_obj::selectItem( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("Market","selectItem",0xbf166b81,"Market.selectItem","Market.hx",301,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(301)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(301)
		int _g = this->itemArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(301)
		while((true)){
			HX_STACK_LINE(301)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(301)
				break;
			}
			HX_STACK_LINE(301)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(302)
			::flixel::FlxSprite _g2 = this->itemArray->__get(i).StaticCast< ::MarketItem >()->getItemSprite();		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(302)
			if (((_g2 == sprite))){
				HX_STACK_LINE(303)
				this->marketCloseTimer = (int)10;
				HX_STACK_LINE(304)
				int _g11 = this->itemArray->__get(i).StaticCast< ::MarketItem >()->getItemCost();		HX_STACK_VAR(_g11,"_g11");
				struct _Function_3_1{
					inline static bool Block( int &i,hx::ObjectPtr< ::Market_obj > __this){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","Market.hx",304,0xdfc0ea62)
						{
							HX_STACK_LINE(304)
							int _g21 = __this->itemArray->__get(i).StaticCast< ::MarketItem >()->getItemCount();		HX_STACK_VAR(_g21,"_g21");
							HX_STACK_LINE(304)
							return (_g21 > (int)0);
						}
						return null();
					}
				};
				HX_STACK_LINE(304)
				if (((  (((this->coinCount >= _g11))) ? bool(_Function_3_1::Block(i,this)) : bool(false) ))){
					HX_STACK_LINE(305)
					this->itemArray->__get(i).StaticCast< ::MarketItem >()->decItemCount();
					HX_STACK_LINE(306)
					int _g3 = this->itemArray->__get(i).StaticCast< ::MarketItem >()->getItemCost();		HX_STACK_VAR(_g3,"_g3");
					HX_STACK_LINE(306)
					hx::SubEq(this->coinCount,_g3);
					HX_STACK_LINE(307)
					this->purchaseItem(this->itemArray->__get(i).StaticCast< ::MarketItem >());
					HX_STACK_LINE(308)
					int _g4 = this->itemArray->__get(i).StaticCast< ::MarketItem >()->getItemCount();		HX_STACK_VAR(_g4,"_g4");
					HX_STACK_LINE(308)
					if (((_g4 <= (int)0))){
						HX_STACK_LINE(309)
						this->itemArray->__get(i).StaticCast< ::MarketItem >()->setItemAvailable(false);
						HX_STACK_LINE(310)
						this->itemArray->__get(i).StaticCast< ::MarketItem >()->setAvailableAnimation();
					}
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,selectItem,(void))

Void Market_obj::purchaseItem( ::MarketItem item){
{
		HX_STACK_FRAME("Market","purchaseItem",0xc4100346,"Market.purchaseItem","Market.hx",318,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(item,"item")
		HX_STACK_LINE(318)
		::String _g = item->getItemName();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(318)
		::String _switch_2 = (_g);
		if (  ( _switch_2==HX_CSTRING("seedItem"))){
			HX_STACK_LINE(320)
			hx::AddEq(this->seedCount,(int)1);
		}
		else if (  ( _switch_2==HX_CSTRING("juiceItem"))){
			HX_STACK_LINE(322)
			hx::AddEq(this->juiceCount,(int)1);
		}
		else if (  ( _switch_2==HX_CSTRING("pieItem"))){
			HX_STACK_LINE(324)
			hx::AddEq(this->pieCount,(int)1);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,purchaseItem,(void))

Void Market_obj::openMarket( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("Market","openMarket",0x12c81278,"Market.openMarket","Market.hx",329,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(329)
		if ((this->turn)){
			HX_STACK_LINE(330)
			::flixel::system::FlxSound _g = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/marketSlideOpenSound.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(330)
			this->marketOpenSound = _g;
			HX_STACK_LINE(331)
			this->marketOpenSound->play(null());
			HX_STACK_LINE(332)
			this->marketOpen = true;
			HX_STACK_LINE(333)
			this->turn = false;
		}
		else{
			HX_STACK_LINE(336)
			::flixel::system::FlxSound _g1 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/marketSlideClosedSound.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(336)
			this->marketClosedSound = _g1;
			HX_STACK_LINE(337)
			this->marketClosedSound->play(null());
			HX_STACK_LINE(338)
			this->marketOpen = false;
			HX_STACK_LINE(339)
			this->turn = true;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,openMarket,(void))

Void Market_obj::slideMarketOut( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("Market","slideMarketOut",0x4a35cf13,"Market.slideMarketOut","Market.hx",344,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(344)
		if ((this->marketOpen)){
			HX_STACK_LINE(345)
			hx::SubEq(this->marketCloseTimer,::flixel::FlxG_obj::elapsed);
			HX_STACK_LINE(346)
			if (((this->marketSideBarSprite->x >= (int)535))){
				HX_STACK_LINE(347)
				{
					HX_STACK_LINE(347)
					::flixel::FlxSprite _g = this->marketSideBarSprite;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(347)
					_g->set_x((_g->x - (int)2));
				}
				HX_STACK_LINE(348)
				this->shiftItemsOpen();
			}
		}
		else{
			HX_STACK_LINE(352)
			if (((this->marketSideBarSprite->x <= (int)573))){
				HX_STACK_LINE(353)
				this->marketCloseTimer = (int)10;
				HX_STACK_LINE(354)
				{
					HX_STACK_LINE(354)
					::flixel::FlxSprite _g = this->marketSideBarSprite;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(354)
					_g->set_x((_g->x + (int)2));
				}
				HX_STACK_LINE(355)
				this->shiftItemsClose();
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,slideMarketOut,(void))

Void Market_obj::timedMarketClose( ){
{
		HX_STACK_FRAME("Market","timedMarketClose",0xc6c458b7,"Market.timedMarketClose","Market.hx",361,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(361)
		if (((this->marketCloseTimer < (int)0))){
			HX_STACK_LINE(362)
			this->marketCloseTimer = (int)10;
			HX_STACK_LINE(363)
			this->marketOpen = false;
			HX_STACK_LINE(364)
			this->turn = true;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,timedMarketClose,(void))

Void Market_obj::shiftItemsOpen( ){
{
		HX_STACK_FRAME("Market","shiftItemsOpen",0x13a6715a,"Market.shiftItemsOpen","Market.hx",368,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(369)
		{
			HX_STACK_LINE(369)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(369)
			int _g = this->itemArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(369)
			while((true)){
				HX_STACK_LINE(369)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(369)
					break;
				}
				HX_STACK_LINE(369)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(370)
				{
					HX_STACK_LINE(370)
					::flixel::FlxSprite _g2 = this->itemArray->__get(i).StaticCast< ::MarketItem >()->getItemSprite();		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(370)
					_g2->set_x((_g2->x - (int)2));
				}
			}
		}
		HX_STACK_LINE(372)
		{
			HX_STACK_LINE(372)
			::flixel::text::FlxText _g = this->seedItemCountFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(372)
			_g->set_x((_g->x - (int)2));
		}
		HX_STACK_LINE(373)
		{
			HX_STACK_LINE(373)
			::flixel::text::FlxText _g = this->juiceItemCountFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(373)
			_g->set_x((_g->x - (int)2));
		}
		HX_STACK_LINE(374)
		{
			HX_STACK_LINE(374)
			::flixel::text::FlxText _g = this->seedItemCostFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(374)
			_g->set_x((_g->x - (int)2));
		}
		HX_STACK_LINE(375)
		{
			HX_STACK_LINE(375)
			::flixel::text::FlxText _g = this->juiceItemCostFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(375)
			_g->set_x((_g->x - (int)2));
		}
		HX_STACK_LINE(376)
		{
			HX_STACK_LINE(376)
			::flixel::text::FlxText _g = this->pieItemCountFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(376)
			_g->set_x((_g->x - (int)2));
		}
		HX_STACK_LINE(377)
		{
			HX_STACK_LINE(377)
			::flixel::text::FlxText _g = this->pieItemCostFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(377)
			_g->set_x((_g->x - (int)2));
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,shiftItemsOpen,(void))

Void Market_obj::shiftItemsClose( ){
{
		HX_STACK_FRAME("Market","shiftItemsClose",0x32908828,"Market.shiftItemsClose","Market.hx",380,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(381)
		{
			HX_STACK_LINE(381)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(381)
			int _g = this->itemArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(381)
			while((true)){
				HX_STACK_LINE(381)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(381)
					break;
				}
				HX_STACK_LINE(381)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(382)
				{
					HX_STACK_LINE(382)
					::flixel::FlxSprite _g2 = this->itemArray->__get(i).StaticCast< ::MarketItem >()->getItemSprite();		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(382)
					_g2->set_x((_g2->x + (int)2));
				}
			}
		}
		HX_STACK_LINE(384)
		{
			HX_STACK_LINE(384)
			::flixel::text::FlxText _g = this->seedItemCountFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(384)
			_g->set_x((_g->x + (int)2));
		}
		HX_STACK_LINE(385)
		{
			HX_STACK_LINE(385)
			::flixel::text::FlxText _g = this->juiceItemCountFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(385)
			_g->set_x((_g->x + (int)2));
		}
		HX_STACK_LINE(386)
		{
			HX_STACK_LINE(386)
			::flixel::text::FlxText _g = this->seedItemCostFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(386)
			_g->set_x((_g->x + (int)2));
		}
		HX_STACK_LINE(387)
		{
			HX_STACK_LINE(387)
			::flixel::text::FlxText _g = this->juiceItemCostFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(387)
			_g->set_x((_g->x + (int)2));
		}
		HX_STACK_LINE(388)
		{
			HX_STACK_LINE(388)
			::flixel::text::FlxText _g = this->pieItemCountFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(388)
			_g->set_x((_g->x + (int)2));
		}
		HX_STACK_LINE(389)
		{
			HX_STACK_LINE(389)
			::flixel::text::FlxText _g = this->pieItemCostFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(389)
			_g->set_x((_g->x + (int)2));
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,shiftItemsClose,(void))

Void Market_obj::replenishItems( ){
{
		HX_STACK_FRAME("Market","replenishItems",0x44824ecc,"Market.replenishItems","Market.hx",392,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(393)
		hx::SubEq(this->replenishSeedTimer,::flixel::FlxG_obj::elapsed);
		HX_STACK_LINE(394)
		hx::SubEq(this->replenishJuiceMachineTimer,::flixel::FlxG_obj::elapsed);
		HX_STACK_LINE(395)
		hx::SubEq(this->replenishPieMachineSeedTimer,::flixel::FlxG_obj::elapsed);
		HX_STACK_LINE(396)
		int _g = this->itemArray->__get((int)0).StaticCast< ::MarketItem >()->getItemCount();		HX_STACK_VAR(_g,"_g");
		struct _Function_1_1{
			inline static bool Block( hx::ObjectPtr< ::Market_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","Market.hx",396,0xdfc0ea62)
				{
					HX_STACK_LINE(396)
					int _g1 = __this->itemArray->__get((int)1).StaticCast< ::MarketItem >()->getItemCount();		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(396)
					return (_g1 > (int)0);
				}
				return null();
			}
		};
		struct _Function_1_2{
			inline static bool Block( hx::ObjectPtr< ::Market_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","Market.hx",396,0xdfc0ea62)
				{
					HX_STACK_LINE(396)
					int _g2 = __this->itemArray->__get((int)2).StaticCast< ::MarketItem >()->getItemCount();		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(396)
					return (_g2 > (int)0);
				}
				return null();
			}
		};
		HX_STACK_LINE(396)
		if (((  ((!(((  ((!(((_g > (int)0))))) ? bool(_Function_1_1::Block(this)) : bool(true) ))))) ? bool(_Function_1_2::Block(this)) : bool(true) ))){
			HX_STACK_LINE(397)
			this->itemArray->__get((int)0).StaticCast< ::MarketItem >()->setItemAvailable(true);
			HX_STACK_LINE(398)
			this->itemArray->__get((int)0).StaticCast< ::MarketItem >()->setAvailableAnimation();
			HX_STACK_LINE(399)
			this->itemArray->__get((int)1).StaticCast< ::MarketItem >()->setItemAvailable(true);
			HX_STACK_LINE(400)
			this->itemArray->__get((int)1).StaticCast< ::MarketItem >()->setAvailableAnimation();
			HX_STACK_LINE(401)
			this->itemArray->__get((int)2).StaticCast< ::MarketItem >()->setItemAvailable(true);
			HX_STACK_LINE(402)
			this->itemArray->__get((int)2).StaticCast< ::MarketItem >()->setAvailableAnimation();
		}
		HX_STACK_LINE(404)
		int _g3 = this->itemArray->__get((int)0).StaticCast< ::MarketItem >()->getItemCount();		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(404)
		if (((_g3 == (int)0))){
			HX_STACK_LINE(405)
			this->itemArray->__get((int)0).StaticCast< ::MarketItem >()->setItemAvailable(false);
			HX_STACK_LINE(406)
			this->itemArray->__get((int)0).StaticCast< ::MarketItem >()->setAvailableAnimation();
		}
		HX_STACK_LINE(408)
		int _g4 = this->itemArray->__get((int)1).StaticCast< ::MarketItem >()->getItemCount();		HX_STACK_VAR(_g4,"_g4");
		HX_STACK_LINE(408)
		if (((_g4 == (int)0))){
			HX_STACK_LINE(409)
			this->itemArray->__get((int)1).StaticCast< ::MarketItem >()->setItemAvailable(false);
			HX_STACK_LINE(410)
			this->itemArray->__get((int)1).StaticCast< ::MarketItem >()->setAvailableAnimation();
		}
		HX_STACK_LINE(412)
		int _g5 = this->itemArray->__get((int)2).StaticCast< ::MarketItem >()->getItemCount();		HX_STACK_VAR(_g5,"_g5");
		HX_STACK_LINE(412)
		if (((_g5 == (int)0))){
			HX_STACK_LINE(413)
			this->itemArray->__get((int)2).StaticCast< ::MarketItem >()->setItemAvailable(false);
			HX_STACK_LINE(414)
			this->itemArray->__get((int)2).StaticCast< ::MarketItem >()->setAvailableAnimation();
		}
		HX_STACK_LINE(416)
		if (((this->replenishSeedTimer < (int)0))){
			HX_STACK_LINE(417)
			this->replenishSeedTimer = (int)180;
			HX_STACK_LINE(418)
			int _g6 = this->itemArray->__get((int)0).StaticCast< ::MarketItem >()->getItemCount();		HX_STACK_VAR(_g6,"_g6");
			HX_STACK_LINE(418)
			if (((_g6 < (int)2))){
				HX_STACK_LINE(419)
				this->itemArray->__get((int)0).StaticCast< ::MarketItem >()->incItemCount();
			}
		}
		HX_STACK_LINE(422)
		if (((this->replenishJuiceMachineTimer < (int)0))){
			HX_STACK_LINE(423)
			this->replenishJuiceMachineTimer = (int)300;
			HX_STACK_LINE(424)
			int _g7 = this->itemArray->__get((int)1).StaticCast< ::MarketItem >()->getItemCount();		HX_STACK_VAR(_g7,"_g7");
			HX_STACK_LINE(424)
			if (((_g7 < (int)1))){
				HX_STACK_LINE(425)
				this->itemArray->__get((int)1).StaticCast< ::MarketItem >()->incItemCount();
			}
		}
		HX_STACK_LINE(428)
		if (((this->replenishPieMachineSeedTimer < (int)0))){
			HX_STACK_LINE(429)
			this->replenishPieMachineSeedTimer = (int)300;
			HX_STACK_LINE(430)
			int _g8 = this->itemArray->__get((int)2).StaticCast< ::MarketItem >()->getItemCount();		HX_STACK_VAR(_g8,"_g8");
			HX_STACK_LINE(430)
			if (((_g8 < (int)1))){
				HX_STACK_LINE(431)
				this->itemArray->__get((int)2).StaticCast< ::MarketItem >()->incItemCount();
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,replenishItems,(void))

Void Market_obj::waterBillCoinPayment( ){
{
		HX_STACK_FRAME("Market","waterBillCoinPayment",0x4c31b229,"Market.waterBillCoinPayment","Market.hx",436,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(437)
		hx::SubEq(this->waterBillTimer,::flixel::FlxG_obj::elapsed);
		HX_STACK_LINE(438)
		if (((this->waterBillTimer < (int)0))){
			HX_STACK_LINE(439)
			hx::SubEq(this->coinCount,this->waterClientCount);
			HX_STACK_LINE(440)
			this->waterBillTimer = (int)30;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,waterBillCoinPayment,(void))

Void Market_obj::ElectricBillCoinPayment( ){
{
		HX_STACK_FRAME("Market","ElectricBillCoinPayment",0x47775c9f,"Market.ElectricBillCoinPayment","Market.hx",444,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(445)
		hx::SubEq(this->electricBillTimer,::flixel::FlxG_obj::elapsed);
		HX_STACK_LINE(446)
		if (((this->electricBillTimer < (int)0))){
			HX_STACK_LINE(447)
			hx::SubEq(this->coinCount,(this->electricClientCount * (int)2));
			HX_STACK_LINE(448)
			this->electricBillTimer = (int)10;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,ElectricBillCoinPayment,(void))

Void Market_obj::destroySound( ){
{
		HX_STACK_FRAME("Market","destroySound",0x07b42f47,"Market.destroySound","Market.hx",452,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(453)
		if (((bool((this->marketOpenSound != null())) && bool(!(((this->marketOpenSound->_channel != null()))))))){
			HX_STACK_LINE(454)
			::flixel::system::FlxSound _g = ::flixel::util::FlxDestroyUtil_obj::destroy(this->marketOpenSound);		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(454)
			this->marketOpenSound = _g;
			HX_STACK_LINE(455)
			this->marketOpenSound = null();
			HX_STACK_LINE(456)
			::haxe::Log_obj::trace(HX_CSTRING("destroying marketOpenSound"),hx::SourceInfo(HX_CSTRING("Market.hx"),456,HX_CSTRING("Market"),HX_CSTRING("destroySound")));
		}
		HX_STACK_LINE(458)
		if (((bool((this->marketClosedSound != null())) && bool(!(((this->marketClosedSound->_channel != null()))))))){
			HX_STACK_LINE(459)
			::flixel::system::FlxSound _g1 = ::flixel::util::FlxDestroyUtil_obj::destroy(this->marketClosedSound);		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(459)
			this->marketClosedSound = _g1;
			HX_STACK_LINE(460)
			this->marketClosedSound = null();
			HX_STACK_LINE(461)
			::haxe::Log_obj::trace(HX_CSTRING("destroying marketClosedSound"),hx::SourceInfo(HX_CSTRING("Market.hx"),461,HX_CSTRING("Market"),HX_CSTRING("destroySound")));
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,destroySound,(void))

Void Market_obj::update( ){
{
		HX_STACK_FRAME("Market","update",0xca95265b,"Market.update","Market.hx",465,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(466)
		this->slideMarketOut(null());
		HX_STACK_LINE(467)
		this->displayItemCountText();
		HX_STACK_LINE(468)
		this->replenishItems();
		HX_STACK_LINE(469)
		this->timedMarketClose();
		HX_STACK_LINE(470)
		this->displaySeedCount();
		HX_STACK_LINE(471)
		this->displayCoinCount();
		HX_STACK_LINE(472)
		this->destroySound();
		HX_STACK_LINE(473)
		this->super::update();
	}
return null();
}


Void Market_obj::destroy( ){
{
		HX_STACK_FRAME("Market","destroy",0xfea2d468,"Market.destroy","Market.hx",476,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(477)
		this->marketGroup = null();
		HX_STACK_LINE(478)
		this->marketSideBarSprite = null();
		HX_STACK_LINE(479)
		this->seedItem = null();
		HX_STACK_LINE(480)
		this->juiceItem = null();
		HX_STACK_LINE(481)
		this->pieItem = null();
		HX_STACK_LINE(482)
		this->coin = null();
		HX_STACK_LINE(483)
		this->seedCountText = null();
		HX_STACK_LINE(484)
		this->juiceCountText = null();
		HX_STACK_LINE(485)
		this->pieCountText = null();
		HX_STACK_LINE(486)
		this->juicePackCountText = null();
		HX_STACK_LINE(487)
		this->piePackCountText = null();
		HX_STACK_LINE(488)
		this->itemArray = null();
		HX_STACK_LINE(489)
		this->seedItemCountFieldText = null();
		HX_STACK_LINE(490)
		this->juiceItemCountFieldText = null();
		HX_STACK_LINE(491)
		this->pieItemCountFieldText = null();
		HX_STACK_LINE(492)
		this->seedItemCostFieldText = null();
		HX_STACK_LINE(493)
		this->juiceItemCostFieldText = null();
		HX_STACK_LINE(494)
		this->pieItemCostFieldText = null();
		HX_STACK_LINE(495)
		this->super::destroy();
	}
return null();
}



Market_obj::Market_obj()
{
}

void Market_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Market);
	HX_MARK_MEMBER_NAME(marketGroup,"marketGroup");
	HX_MARK_MEMBER_NAME(marketSideBarSprite,"marketSideBarSprite");
	HX_MARK_MEMBER_NAME(seedItem,"seedItem");
	HX_MARK_MEMBER_NAME(juiceItem,"juiceItem");
	HX_MARK_MEMBER_NAME(pieItem,"pieItem");
	HX_MARK_MEMBER_NAME(seedsAvailableText,"seedsAvailableText");
	HX_MARK_MEMBER_NAME(seedItemCountFieldText,"seedItemCountFieldText");
	HX_MARK_MEMBER_NAME(juiceItemCountFieldText,"juiceItemCountFieldText");
	HX_MARK_MEMBER_NAME(pieItemCountFieldText,"pieItemCountFieldText");
	HX_MARK_MEMBER_NAME(seedItemCostFieldText,"seedItemCostFieldText");
	HX_MARK_MEMBER_NAME(juiceItemCostFieldText,"juiceItemCostFieldText");
	HX_MARK_MEMBER_NAME(pieItemCostFieldText,"pieItemCostFieldText");
	HX_MARK_MEMBER_NAME(juicePackCountText,"juicePackCountText");
	HX_MARK_MEMBER_NAME(piePackCountText,"piePackCountText");
	HX_MARK_MEMBER_NAME(itemArray,"itemArray");
	HX_MARK_MEMBER_NAME(coin,"coin");
	HX_MARK_MEMBER_NAME(coinCount,"coinCount");
	HX_MARK_MEMBER_NAME(coinCountString,"coinCountString");
	HX_MARK_MEMBER_NAME(coinCountFieldText,"coinCountFieldText");
	HX_MARK_MEMBER_NAME(piePack,"piePack");
	HX_MARK_MEMBER_NAME(juicePack,"juicePack");
	HX_MARK_MEMBER_NAME(seedCount,"seedCount");
	HX_MARK_MEMBER_NAME(juiceCount,"juiceCount");
	HX_MARK_MEMBER_NAME(pieCount,"pieCount");
	HX_MARK_MEMBER_NAME(waterClientCount,"waterClientCount");
	HX_MARK_MEMBER_NAME(electricClientCount,"electricClientCount");
	HX_MARK_MEMBER_NAME(shiftX,"shiftX");
	HX_MARK_MEMBER_NAME(shiftY,"shiftY");
	HX_MARK_MEMBER_NAME(seedCountString,"seedCountString");
	HX_MARK_MEMBER_NAME(seedCountText,"seedCountText");
	HX_MARK_MEMBER_NAME(juiceCountText,"juiceCountText");
	HX_MARK_MEMBER_NAME(pieCountText,"pieCountText");
	HX_MARK_MEMBER_NAME(marketCloseTimer,"marketCloseTimer");
	HX_MARK_MEMBER_NAME(replenishSeedTimer,"replenishSeedTimer");
	HX_MARK_MEMBER_NAME(replenishJuiceMachineTimer,"replenishJuiceMachineTimer");
	HX_MARK_MEMBER_NAME(replenishPieMachineSeedTimer,"replenishPieMachineSeedTimer");
	HX_MARK_MEMBER_NAME(waterBillTimer,"waterBillTimer");
	HX_MARK_MEMBER_NAME(electricBillTimer,"electricBillTimer");
	HX_MARK_MEMBER_NAME(marketOpen,"marketOpen");
	HX_MARK_MEMBER_NAME(turn,"turn");
	HX_MARK_MEMBER_NAME(marketOpenSound,"marketOpenSound");
	HX_MARK_MEMBER_NAME(marketClosedSound,"marketClosedSound");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void Market_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(marketGroup,"marketGroup");
	HX_VISIT_MEMBER_NAME(marketSideBarSprite,"marketSideBarSprite");
	HX_VISIT_MEMBER_NAME(seedItem,"seedItem");
	HX_VISIT_MEMBER_NAME(juiceItem,"juiceItem");
	HX_VISIT_MEMBER_NAME(pieItem,"pieItem");
	HX_VISIT_MEMBER_NAME(seedsAvailableText,"seedsAvailableText");
	HX_VISIT_MEMBER_NAME(seedItemCountFieldText,"seedItemCountFieldText");
	HX_VISIT_MEMBER_NAME(juiceItemCountFieldText,"juiceItemCountFieldText");
	HX_VISIT_MEMBER_NAME(pieItemCountFieldText,"pieItemCountFieldText");
	HX_VISIT_MEMBER_NAME(seedItemCostFieldText,"seedItemCostFieldText");
	HX_VISIT_MEMBER_NAME(juiceItemCostFieldText,"juiceItemCostFieldText");
	HX_VISIT_MEMBER_NAME(pieItemCostFieldText,"pieItemCostFieldText");
	HX_VISIT_MEMBER_NAME(juicePackCountText,"juicePackCountText");
	HX_VISIT_MEMBER_NAME(piePackCountText,"piePackCountText");
	HX_VISIT_MEMBER_NAME(itemArray,"itemArray");
	HX_VISIT_MEMBER_NAME(coin,"coin");
	HX_VISIT_MEMBER_NAME(coinCount,"coinCount");
	HX_VISIT_MEMBER_NAME(coinCountString,"coinCountString");
	HX_VISIT_MEMBER_NAME(coinCountFieldText,"coinCountFieldText");
	HX_VISIT_MEMBER_NAME(piePack,"piePack");
	HX_VISIT_MEMBER_NAME(juicePack,"juicePack");
	HX_VISIT_MEMBER_NAME(seedCount,"seedCount");
	HX_VISIT_MEMBER_NAME(juiceCount,"juiceCount");
	HX_VISIT_MEMBER_NAME(pieCount,"pieCount");
	HX_VISIT_MEMBER_NAME(waterClientCount,"waterClientCount");
	HX_VISIT_MEMBER_NAME(electricClientCount,"electricClientCount");
	HX_VISIT_MEMBER_NAME(shiftX,"shiftX");
	HX_VISIT_MEMBER_NAME(shiftY,"shiftY");
	HX_VISIT_MEMBER_NAME(seedCountString,"seedCountString");
	HX_VISIT_MEMBER_NAME(seedCountText,"seedCountText");
	HX_VISIT_MEMBER_NAME(juiceCountText,"juiceCountText");
	HX_VISIT_MEMBER_NAME(pieCountText,"pieCountText");
	HX_VISIT_MEMBER_NAME(marketCloseTimer,"marketCloseTimer");
	HX_VISIT_MEMBER_NAME(replenishSeedTimer,"replenishSeedTimer");
	HX_VISIT_MEMBER_NAME(replenishJuiceMachineTimer,"replenishJuiceMachineTimer");
	HX_VISIT_MEMBER_NAME(replenishPieMachineSeedTimer,"replenishPieMachineSeedTimer");
	HX_VISIT_MEMBER_NAME(waterBillTimer,"waterBillTimer");
	HX_VISIT_MEMBER_NAME(electricBillTimer,"electricBillTimer");
	HX_VISIT_MEMBER_NAME(marketOpen,"marketOpen");
	HX_VISIT_MEMBER_NAME(turn,"turn");
	HX_VISIT_MEMBER_NAME(marketOpenSound,"marketOpenSound");
	HX_VISIT_MEMBER_NAME(marketClosedSound,"marketClosedSound");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic Market_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"coin") ) { return coin; }
		if (HX_FIELD_EQ(inName,"turn") ) { return turn; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"shiftX") ) { return shiftX; }
		if (HX_FIELD_EQ(inName,"shiftY") ) { return shiftY; }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"pieItem") ) { return pieItem; }
		if (HX_FIELD_EQ(inName,"piePack") ) { return piePack; }
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"seedItem") ) { return seedItem; }
		if (HX_FIELD_EQ(inName,"pieCount") ) { return pieCount; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"juiceItem") ) { return juiceItem; }
		if (HX_FIELD_EQ(inName,"itemArray") ) { return itemArray; }
		if (HX_FIELD_EQ(inName,"coinCount") ) { return coinCount; }
		if (HX_FIELD_EQ(inName,"juicePack") ) { return juicePack; }
		if (HX_FIELD_EQ(inName,"seedCount") ) { return seedCount; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"juiceCount") ) { return juiceCount; }
		if (HX_FIELD_EQ(inName,"marketOpen") ) { return marketOpen; }
		if (HX_FIELD_EQ(inName,"selectItem") ) { return selectItem_dyn(); }
		if (HX_FIELD_EQ(inName,"openMarket") ) { return openMarket_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"marketGroup") ) { return marketGroup; }
		if (HX_FIELD_EQ(inName,"getPieCount") ) { return getPieCount_dyn(); }
		if (HX_FIELD_EQ(inName,"setPieCount") ) { return setPieCount_dyn(); }
		if (HX_FIELD_EQ(inName,"decPieCount") ) { return decPieCount_dyn(); }
		if (HX_FIELD_EQ(inName,"incPieCount") ) { return incPieCount_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"pieCountText") ) { return pieCountText; }
		if (HX_FIELD_EQ(inName,"getCoinCount") ) { return getCoinCount_dyn(); }
		if (HX_FIELD_EQ(inName,"getSeedCount") ) { return getSeedCount_dyn(); }
		if (HX_FIELD_EQ(inName,"setSeedCount") ) { return setSeedCount_dyn(); }
		if (HX_FIELD_EQ(inName,"setCoinCount") ) { return setCoinCount_dyn(); }
		if (HX_FIELD_EQ(inName,"decSeedCount") ) { return decSeedCount_dyn(); }
		if (HX_FIELD_EQ(inName,"incSeedCount") ) { return incSeedCount_dyn(); }
		if (HX_FIELD_EQ(inName,"purchaseItem") ) { return purchaseItem_dyn(); }
		if (HX_FIELD_EQ(inName,"destroySound") ) { return destroySound_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"seedCountText") ) { return seedCountText; }
		if (HX_FIELD_EQ(inName,"getMarketOpen") ) { return getMarketOpen_dyn(); }
		if (HX_FIELD_EQ(inName,"getJuiceCount") ) { return getJuiceCount_dyn(); }
		if (HX_FIELD_EQ(inName,"stockPiePacks") ) { return stockPiePacks_dyn(); }
		if (HX_FIELD_EQ(inName,"setJuiceCount") ) { return setJuiceCount_dyn(); }
		if (HX_FIELD_EQ(inName,"decjuiceCount") ) { return decjuiceCount_dyn(); }
		if (HX_FIELD_EQ(inName,"incJuiceCount") ) { return incJuiceCount_dyn(); }
		if (HX_FIELD_EQ(inName,"fillItemArray") ) { return fillItemArray_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"juiceCountText") ) { return juiceCountText; }
		if (HX_FIELD_EQ(inName,"waterBillTimer") ) { return waterBillTimer; }
		if (HX_FIELD_EQ(inName,"registerEvents") ) { return registerEvents_dyn(); }
		if (HX_FIELD_EQ(inName,"slideMarketOut") ) { return slideMarketOut_dyn(); }
		if (HX_FIELD_EQ(inName,"shiftItemsOpen") ) { return shiftItemsOpen_dyn(); }
		if (HX_FIELD_EQ(inName,"replenishItems") ) { return replenishItems_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"coinCountString") ) { return coinCountString; }
		if (HX_FIELD_EQ(inName,"seedCountString") ) { return seedCountString; }
		if (HX_FIELD_EQ(inName,"marketOpenSound") ) { return marketOpenSound; }
		if (HX_FIELD_EQ(inName,"getPiePackCount") ) { return getPiePackCount_dyn(); }
		if (HX_FIELD_EQ(inName,"stockJuicePacks") ) { return stockJuicePacks_dyn(); }
		if (HX_FIELD_EQ(inName,"loadPiePacksDec") ) { return loadPiePacksDec_dyn(); }
		if (HX_FIELD_EQ(inName,"setPiePackCount") ) { return setPiePackCount_dyn(); }
		if (HX_FIELD_EQ(inName,"setPieItemCount") ) { return setPieItemCount_dyn(); }
		if (HX_FIELD_EQ(inName,"coinPaymentUtil") ) { return coinPaymentUtil_dyn(); }
		if (HX_FIELD_EQ(inName,"shiftItemsClose") ) { return shiftItemsClose_dyn(); }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"piePackCountText") ) { return piePackCountText; }
		if (HX_FIELD_EQ(inName,"waterClientCount") ) { return waterClientCount; }
		if (HX_FIELD_EQ(inName,"marketCloseTimer") ) { return marketCloseTimer; }
		if (HX_FIELD_EQ(inName,"setseedItemCount") ) { return setseedItemCount_dyn(); }
		if (HX_FIELD_EQ(inName,"addSubtractCoins") ) { return addSubtractCoins_dyn(); }
		if (HX_FIELD_EQ(inName,"displayCoinCount") ) { return displayCoinCount_dyn(); }
		if (HX_FIELD_EQ(inName,"displaySeedCount") ) { return displaySeedCount_dyn(); }
		if (HX_FIELD_EQ(inName,"unregisterEvents") ) { return unregisterEvents_dyn(); }
		if (HX_FIELD_EQ(inName,"timedMarketClose") ) { return timedMarketClose_dyn(); }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"electricBillTimer") ) { return electricBillTimer; }
		if (HX_FIELD_EQ(inName,"marketClosedSound") ) { return marketClosedSound; }
		if (HX_FIELD_EQ(inName,"getJuicePackCount") ) { return getJuicePackCount_dyn(); }
		if (HX_FIELD_EQ(inName,"loadJuicePacksDec") ) { return loadJuicePacksDec_dyn(); }
		if (HX_FIELD_EQ(inName,"setJuicePackCount") ) { return setJuicePackCount_dyn(); }
		if (HX_FIELD_EQ(inName,"setJuiceItemCount") ) { return setJuiceItemCount_dyn(); }
		if (HX_FIELD_EQ(inName,"decjuicePackCount") ) { return decjuicePackCount_dyn(); }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"seedsAvailableText") ) { return seedsAvailableText; }
		if (HX_FIELD_EQ(inName,"juicePackCountText") ) { return juicePackCountText; }
		if (HX_FIELD_EQ(inName,"coinCountFieldText") ) { return coinCountFieldText; }
		if (HX_FIELD_EQ(inName,"replenishSeedTimer") ) { return replenishSeedTimer; }
		if (HX_FIELD_EQ(inName,"positionArrayItems") ) { return positionArrayItems_dyn(); }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"marketSideBarSprite") ) { return marketSideBarSprite; }
		if (HX_FIELD_EQ(inName,"electricClientCount") ) { return electricClientCount; }
		if (HX_FIELD_EQ(inName,"setWaterClientCount") ) { return setWaterClientCount_dyn(); }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"pieItemCostFieldText") ) { return pieItemCostFieldText; }
		if (HX_FIELD_EQ(inName,"displayItemCountText") ) { return displayItemCountText_dyn(); }
		if (HX_FIELD_EQ(inName,"waterBillCoinPayment") ) { return waterBillCoinPayment_dyn(); }
		break;
	case 21:
		if (HX_FIELD_EQ(inName,"pieItemCountFieldText") ) { return pieItemCountFieldText; }
		if (HX_FIELD_EQ(inName,"seedItemCostFieldText") ) { return seedItemCostFieldText; }
		break;
	case 22:
		if (HX_FIELD_EQ(inName,"seedItemCountFieldText") ) { return seedItemCountFieldText; }
		if (HX_FIELD_EQ(inName,"juiceItemCostFieldText") ) { return juiceItemCostFieldText; }
		if (HX_FIELD_EQ(inName,"setElectricClientCount") ) { return setElectricClientCount_dyn(); }
		break;
	case 23:
		if (HX_FIELD_EQ(inName,"juiceItemCountFieldText") ) { return juiceItemCountFieldText; }
		if (HX_FIELD_EQ(inName,"ElectricBillCoinPayment") ) { return ElectricBillCoinPayment_dyn(); }
		break;
	case 26:
		if (HX_FIELD_EQ(inName,"replenishJuiceMachineTimer") ) { return replenishJuiceMachineTimer; }
		break;
	case 28:
		if (HX_FIELD_EQ(inName,"replenishPieMachineSeedTimer") ) { return replenishPieMachineSeedTimer; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Market_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"coin") ) { coin=inValue.Cast< ::GoldCoin >(); return inValue; }
		if (HX_FIELD_EQ(inName,"turn") ) { turn=inValue.Cast< bool >(); return inValue; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"shiftX") ) { shiftX=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"shiftY") ) { shiftY=inValue.Cast< int >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"pieItem") ) { pieItem=inValue.Cast< ::PieItem >(); return inValue; }
		if (HX_FIELD_EQ(inName,"piePack") ) { piePack=inValue.Cast< int >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"seedItem") ) { seedItem=inValue.Cast< ::SeedItem >(); return inValue; }
		if (HX_FIELD_EQ(inName,"pieCount") ) { pieCount=inValue.Cast< int >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"juiceItem") ) { juiceItem=inValue.Cast< ::JuiceItem >(); return inValue; }
		if (HX_FIELD_EQ(inName,"itemArray") ) { itemArray=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		if (HX_FIELD_EQ(inName,"coinCount") ) { coinCount=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"juicePack") ) { juicePack=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"seedCount") ) { seedCount=inValue.Cast< int >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"juiceCount") ) { juiceCount=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"marketOpen") ) { marketOpen=inValue.Cast< bool >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"marketGroup") ) { marketGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"pieCountText") ) { pieCountText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"seedCountText") ) { seedCountText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"juiceCountText") ) { juiceCountText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		if (HX_FIELD_EQ(inName,"waterBillTimer") ) { waterBillTimer=inValue.Cast< Float >(); return inValue; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"coinCountString") ) { coinCountString=inValue.Cast< ::String >(); return inValue; }
		if (HX_FIELD_EQ(inName,"seedCountString") ) { seedCountString=inValue.Cast< ::String >(); return inValue; }
		if (HX_FIELD_EQ(inName,"marketOpenSound") ) { marketOpenSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"piePackCountText") ) { piePackCountText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		if (HX_FIELD_EQ(inName,"waterClientCount") ) { waterClientCount=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"marketCloseTimer") ) { marketCloseTimer=inValue.Cast< Float >(); return inValue; }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"electricBillTimer") ) { electricBillTimer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"marketClosedSound") ) { marketClosedSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"seedsAvailableText") ) { seedsAvailableText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		if (HX_FIELD_EQ(inName,"juicePackCountText") ) { juicePackCountText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		if (HX_FIELD_EQ(inName,"coinCountFieldText") ) { coinCountFieldText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		if (HX_FIELD_EQ(inName,"replenishSeedTimer") ) { replenishSeedTimer=inValue.Cast< Float >(); return inValue; }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"marketSideBarSprite") ) { marketSideBarSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"electricClientCount") ) { electricClientCount=inValue.Cast< int >(); return inValue; }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"pieItemCostFieldText") ) { pieItemCostFieldText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		break;
	case 21:
		if (HX_FIELD_EQ(inName,"pieItemCountFieldText") ) { pieItemCountFieldText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		if (HX_FIELD_EQ(inName,"seedItemCostFieldText") ) { seedItemCostFieldText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		break;
	case 22:
		if (HX_FIELD_EQ(inName,"seedItemCountFieldText") ) { seedItemCountFieldText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		if (HX_FIELD_EQ(inName,"juiceItemCostFieldText") ) { juiceItemCostFieldText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		break;
	case 23:
		if (HX_FIELD_EQ(inName,"juiceItemCountFieldText") ) { juiceItemCountFieldText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		break;
	case 26:
		if (HX_FIELD_EQ(inName,"replenishJuiceMachineTimer") ) { replenishJuiceMachineTimer=inValue.Cast< Float >(); return inValue; }
		break;
	case 28:
		if (HX_FIELD_EQ(inName,"replenishPieMachineSeedTimer") ) { replenishPieMachineSeedTimer=inValue.Cast< Float >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Market_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("marketGroup"));
	outFields->push(HX_CSTRING("marketSideBarSprite"));
	outFields->push(HX_CSTRING("seedItem"));
	outFields->push(HX_CSTRING("juiceItem"));
	outFields->push(HX_CSTRING("pieItem"));
	outFields->push(HX_CSTRING("seedsAvailableText"));
	outFields->push(HX_CSTRING("seedItemCountFieldText"));
	outFields->push(HX_CSTRING("juiceItemCountFieldText"));
	outFields->push(HX_CSTRING("pieItemCountFieldText"));
	outFields->push(HX_CSTRING("seedItemCostFieldText"));
	outFields->push(HX_CSTRING("juiceItemCostFieldText"));
	outFields->push(HX_CSTRING("pieItemCostFieldText"));
	outFields->push(HX_CSTRING("juicePackCountText"));
	outFields->push(HX_CSTRING("piePackCountText"));
	outFields->push(HX_CSTRING("itemArray"));
	outFields->push(HX_CSTRING("coin"));
	outFields->push(HX_CSTRING("coinCount"));
	outFields->push(HX_CSTRING("coinCountString"));
	outFields->push(HX_CSTRING("coinCountFieldText"));
	outFields->push(HX_CSTRING("piePack"));
	outFields->push(HX_CSTRING("juicePack"));
	outFields->push(HX_CSTRING("seedCount"));
	outFields->push(HX_CSTRING("juiceCount"));
	outFields->push(HX_CSTRING("pieCount"));
	outFields->push(HX_CSTRING("waterClientCount"));
	outFields->push(HX_CSTRING("electricClientCount"));
	outFields->push(HX_CSTRING("shiftX"));
	outFields->push(HX_CSTRING("shiftY"));
	outFields->push(HX_CSTRING("seedCountString"));
	outFields->push(HX_CSTRING("seedCountText"));
	outFields->push(HX_CSTRING("juiceCountText"));
	outFields->push(HX_CSTRING("pieCountText"));
	outFields->push(HX_CSTRING("marketCloseTimer"));
	outFields->push(HX_CSTRING("replenishSeedTimer"));
	outFields->push(HX_CSTRING("replenishJuiceMachineTimer"));
	outFields->push(HX_CSTRING("replenishPieMachineSeedTimer"));
	outFields->push(HX_CSTRING("waterBillTimer"));
	outFields->push(HX_CSTRING("electricBillTimer"));
	outFields->push(HX_CSTRING("marketOpen"));
	outFields->push(HX_CSTRING("turn"));
	outFields->push(HX_CSTRING("marketOpenSound"));
	outFields->push(HX_CSTRING("marketClosedSound"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(Market_obj,marketGroup),HX_CSTRING("marketGroup")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Market_obj,marketSideBarSprite),HX_CSTRING("marketSideBarSprite")},
	{hx::fsObject /*::SeedItem*/ ,(int)offsetof(Market_obj,seedItem),HX_CSTRING("seedItem")},
	{hx::fsObject /*::JuiceItem*/ ,(int)offsetof(Market_obj,juiceItem),HX_CSTRING("juiceItem")},
	{hx::fsObject /*::PieItem*/ ,(int)offsetof(Market_obj,pieItem),HX_CSTRING("pieItem")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,seedsAvailableText),HX_CSTRING("seedsAvailableText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,seedItemCountFieldText),HX_CSTRING("seedItemCountFieldText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,juiceItemCountFieldText),HX_CSTRING("juiceItemCountFieldText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,pieItemCountFieldText),HX_CSTRING("pieItemCountFieldText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,seedItemCostFieldText),HX_CSTRING("seedItemCostFieldText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,juiceItemCostFieldText),HX_CSTRING("juiceItemCostFieldText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,pieItemCostFieldText),HX_CSTRING("pieItemCostFieldText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,juicePackCountText),HX_CSTRING("juicePackCountText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,piePackCountText),HX_CSTRING("piePackCountText")},
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(Market_obj,itemArray),HX_CSTRING("itemArray")},
	{hx::fsObject /*::GoldCoin*/ ,(int)offsetof(Market_obj,coin),HX_CSTRING("coin")},
	{hx::fsInt,(int)offsetof(Market_obj,coinCount),HX_CSTRING("coinCount")},
	{hx::fsString,(int)offsetof(Market_obj,coinCountString),HX_CSTRING("coinCountString")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,coinCountFieldText),HX_CSTRING("coinCountFieldText")},
	{hx::fsInt,(int)offsetof(Market_obj,piePack),HX_CSTRING("piePack")},
	{hx::fsInt,(int)offsetof(Market_obj,juicePack),HX_CSTRING("juicePack")},
	{hx::fsInt,(int)offsetof(Market_obj,seedCount),HX_CSTRING("seedCount")},
	{hx::fsInt,(int)offsetof(Market_obj,juiceCount),HX_CSTRING("juiceCount")},
	{hx::fsInt,(int)offsetof(Market_obj,pieCount),HX_CSTRING("pieCount")},
	{hx::fsInt,(int)offsetof(Market_obj,waterClientCount),HX_CSTRING("waterClientCount")},
	{hx::fsInt,(int)offsetof(Market_obj,electricClientCount),HX_CSTRING("electricClientCount")},
	{hx::fsInt,(int)offsetof(Market_obj,shiftX),HX_CSTRING("shiftX")},
	{hx::fsInt,(int)offsetof(Market_obj,shiftY),HX_CSTRING("shiftY")},
	{hx::fsString,(int)offsetof(Market_obj,seedCountString),HX_CSTRING("seedCountString")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,seedCountText),HX_CSTRING("seedCountText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,juiceCountText),HX_CSTRING("juiceCountText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,pieCountText),HX_CSTRING("pieCountText")},
	{hx::fsFloat,(int)offsetof(Market_obj,marketCloseTimer),HX_CSTRING("marketCloseTimer")},
	{hx::fsFloat,(int)offsetof(Market_obj,replenishSeedTimer),HX_CSTRING("replenishSeedTimer")},
	{hx::fsFloat,(int)offsetof(Market_obj,replenishJuiceMachineTimer),HX_CSTRING("replenishJuiceMachineTimer")},
	{hx::fsFloat,(int)offsetof(Market_obj,replenishPieMachineSeedTimer),HX_CSTRING("replenishPieMachineSeedTimer")},
	{hx::fsFloat,(int)offsetof(Market_obj,waterBillTimer),HX_CSTRING("waterBillTimer")},
	{hx::fsFloat,(int)offsetof(Market_obj,electricBillTimer),HX_CSTRING("electricBillTimer")},
	{hx::fsBool,(int)offsetof(Market_obj,marketOpen),HX_CSTRING("marketOpen")},
	{hx::fsBool,(int)offsetof(Market_obj,turn),HX_CSTRING("turn")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(Market_obj,marketOpenSound),HX_CSTRING("marketOpenSound")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(Market_obj,marketClosedSound),HX_CSTRING("marketClosedSound")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("marketGroup"),
	HX_CSTRING("marketSideBarSprite"),
	HX_CSTRING("seedItem"),
	HX_CSTRING("juiceItem"),
	HX_CSTRING("pieItem"),
	HX_CSTRING("seedsAvailableText"),
	HX_CSTRING("seedItemCountFieldText"),
	HX_CSTRING("juiceItemCountFieldText"),
	HX_CSTRING("pieItemCountFieldText"),
	HX_CSTRING("seedItemCostFieldText"),
	HX_CSTRING("juiceItemCostFieldText"),
	HX_CSTRING("pieItemCostFieldText"),
	HX_CSTRING("juicePackCountText"),
	HX_CSTRING("piePackCountText"),
	HX_CSTRING("itemArray"),
	HX_CSTRING("coin"),
	HX_CSTRING("coinCount"),
	HX_CSTRING("coinCountString"),
	HX_CSTRING("coinCountFieldText"),
	HX_CSTRING("piePack"),
	HX_CSTRING("juicePack"),
	HX_CSTRING("seedCount"),
	HX_CSTRING("juiceCount"),
	HX_CSTRING("pieCount"),
	HX_CSTRING("waterClientCount"),
	HX_CSTRING("electricClientCount"),
	HX_CSTRING("shiftX"),
	HX_CSTRING("shiftY"),
	HX_CSTRING("seedCountString"),
	HX_CSTRING("seedCountText"),
	HX_CSTRING("juiceCountText"),
	HX_CSTRING("pieCountText"),
	HX_CSTRING("marketCloseTimer"),
	HX_CSTRING("replenishSeedTimer"),
	HX_CSTRING("replenishJuiceMachineTimer"),
	HX_CSTRING("replenishPieMachineSeedTimer"),
	HX_CSTRING("waterBillTimer"),
	HX_CSTRING("electricBillTimer"),
	HX_CSTRING("marketOpen"),
	HX_CSTRING("turn"),
	HX_CSTRING("marketOpenSound"),
	HX_CSTRING("marketClosedSound"),
	HX_CSTRING("getMarketOpen"),
	HX_CSTRING("getCoinCount"),
	HX_CSTRING("getSeedCount"),
	HX_CSTRING("getJuiceCount"),
	HX_CSTRING("getPieCount"),
	HX_CSTRING("getJuicePackCount"),
	HX_CSTRING("getPiePackCount"),
	HX_CSTRING("stockJuicePacks"),
	HX_CSTRING("stockPiePacks"),
	HX_CSTRING("loadJuicePacksDec"),
	HX_CSTRING("loadPiePacksDec"),
	HX_CSTRING("setJuicePackCount"),
	HX_CSTRING("setPiePackCount"),
	HX_CSTRING("setSeedCount"),
	HX_CSTRING("setseedItemCount"),
	HX_CSTRING("setPieCount"),
	HX_CSTRING("setPieItemCount"),
	HX_CSTRING("setJuiceCount"),
	HX_CSTRING("setJuiceItemCount"),
	HX_CSTRING("setCoinCount"),
	HX_CSTRING("setWaterClientCount"),
	HX_CSTRING("setElectricClientCount"),
	HX_CSTRING("addSubtractCoins"),
	HX_CSTRING("displayCoinCount"),
	HX_CSTRING("displaySeedCount"),
	HX_CSTRING("displayItemCountText"),
	HX_CSTRING("decSeedCount"),
	HX_CSTRING("incSeedCount"),
	HX_CSTRING("decjuiceCount"),
	HX_CSTRING("decjuicePackCount"),
	HX_CSTRING("incJuiceCount"),
	HX_CSTRING("decPieCount"),
	HX_CSTRING("incPieCount"),
	HX_CSTRING("coinPaymentUtil"),
	HX_CSTRING("positionArrayItems"),
	HX_CSTRING("fillItemArray"),
	HX_CSTRING("registerEvents"),
	HX_CSTRING("unregisterEvents"),
	HX_CSTRING("selectItem"),
	HX_CSTRING("purchaseItem"),
	HX_CSTRING("openMarket"),
	HX_CSTRING("slideMarketOut"),
	HX_CSTRING("timedMarketClose"),
	HX_CSTRING("shiftItemsOpen"),
	HX_CSTRING("shiftItemsClose"),
	HX_CSTRING("replenishItems"),
	HX_CSTRING("waterBillCoinPayment"),
	HX_CSTRING("ElectricBillCoinPayment"),
	HX_CSTRING("destroySound"),
	HX_CSTRING("update"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Market_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Market_obj::__mClass,"__mClass");
};

#endif

Class Market_obj::__mClass;

void Market_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("Market"), hx::TCanCast< Market_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void Market_obj::__boot()
{
}

