#ifndef INCLUDED_SeedItem
#define INCLUDED_SeedItem

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <MarketItem.h>
HX_DECLARE_CLASS0(MarketItem)
HX_DECLARE_CLASS0(SeedItem)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  SeedItem_obj : public ::MarketItem_obj{
	public:
		typedef ::MarketItem_obj super;
		typedef SeedItem_obj OBJ_;
		SeedItem_obj();
		Void __construct();

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< SeedItem_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~SeedItem_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		::String __ToString() const { return HX_CSTRING("SeedItem"); }

		virtual Void initMarketItems( );
		Dynamic initMarketItems_dyn();

		virtual Void setSeedItemCount( int val);
		Dynamic setSeedItemCount_dyn();

		virtual Void update( );

};


#endif /* INCLUDED_SeedItem */ 
