#ifndef INCLUDED_Tile
#define INCLUDED_Tile

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/FlxSprite.h>
HX_DECLARE_CLASS0(Tile)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  Tile_obj : public ::flixel::FlxSprite_obj{
	public:
		typedef ::flixel::FlxSprite_obj super;
		typedef Tile_obj OBJ_;
		Tile_obj();
		Void __construct(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< Tile_obj > __new(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~Tile_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("Tile"); }

		bool hasObject;
		bool tileOwned;
		bool tileClicked;
		::flixel::FlxSprite buyMeSprite;
		virtual bool getTileOwned( );
		Dynamic getTileOwned_dyn();

		virtual bool getHasObject( );
		Dynamic getHasObject_dyn();

		virtual bool getTileClicked( );
		Dynamic getTileClicked_dyn();

		virtual Void setTileClicked( bool val);
		Dynamic setTileClicked_dyn();

		virtual Void setTileOwned( bool val);
		Dynamic setTileOwned_dyn();

		virtual Void setHasObject( bool val);
		Dynamic setHasObject_dyn();

		virtual Void setAsOwned( );
		Dynamic setAsOwned_dyn();

		virtual Void setAsUnowned( );
		Dynamic setAsUnowned_dyn();

		virtual Void update( );

};


#endif /* INCLUDED_Tile */ 
