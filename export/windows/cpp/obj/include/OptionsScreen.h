#ifndef INCLUDED_OptionsScreen
#define INCLUDED_OptionsScreen

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/group/FlxGroup.h>
HX_DECLARE_CLASS0(OptionsScreen)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,group,FlxGroup)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)
HX_DECLARE_CLASS2(flixel,ui,FlxButton)
HX_DECLARE_CLASS2(flixel,ui,FlxTypedButton)


class HXCPP_CLASS_ATTRIBUTES  OptionsScreen_obj : public ::flixel::group::FlxGroup_obj{
	public:
		typedef ::flixel::group::FlxGroup_obj super;
		typedef OptionsScreen_obj OBJ_;
		OptionsScreen_obj();
		Void __construct();

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< OptionsScreen_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~OptionsScreen_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("OptionsScreen"); }

		::flixel::group::FlxGroup optionsScreenGroup;
		::flixel::FlxSprite optionsScreenSprite;
		::flixel::ui::FlxButton exitButton;
		::flixel::ui::FlxButton resetButton;
		bool gameRestart;
		virtual bool getGameRestart( );
		Dynamic getGameRestart_dyn();

		virtual Void setGameRestart( bool val);
		Dynamic setGameRestart_dyn();

		virtual Void menuVisible( int val);
		Dynamic menuVisible_dyn();

		virtual Void exitGame( ::flixel::ui::FlxButton button);
		Dynamic exitGame_dyn();

		virtual Void resetGame( ::flixel::ui::FlxButton button);
		Dynamic resetGame_dyn();

		virtual Void destroy( );

};


#endif /* INCLUDED_OptionsScreen */ 
