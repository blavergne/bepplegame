#ifndef INCLUDED_NumberChalk
#define INCLUDED_NumberChalk

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/group/FlxGroup.h>
HX_DECLARE_CLASS0(NumberChalk)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,group,FlxGroup)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  NumberChalk_obj : public ::flixel::group::FlxGroup_obj{
	public:
		typedef ::flixel::group::FlxGroup_obj super;
		typedef NumberChalk_obj OBJ_;
		NumberChalk_obj();
		Void __construct();

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< NumberChalk_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~NumberChalk_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("NumberChalk"); }

		::flixel::FlxSprite myRandNum;
		int myIndex;
		::flixel::FlxSprite c0;
		::flixel::FlxSprite c1;
		::flixel::FlxSprite c2;
		::flixel::FlxSprite c3;
		::flixel::FlxSprite c4;
		::flixel::FlxSprite c5;
		::flixel::FlxSprite c6;
		::flixel::FlxSprite c7;
		::flixel::FlxSprite c8;
		::flixel::FlxSprite c9;
		Array< ::Dynamic > numRandArray;
		virtual Void init( );
		Dynamic init_dyn();

		virtual ::flixel::FlxSprite getRandomNumber( );
		Dynamic getRandomNumber_dyn();

		virtual int getMyIndex( );
		Dynamic getMyIndex_dyn();

		virtual Void destroy( );

};


#endif /* INCLUDED_NumberChalk */ 
