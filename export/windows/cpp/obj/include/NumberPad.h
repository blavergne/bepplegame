#ifndef INCLUDED_NumberPad
#define INCLUDED_NumberPad

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/group/FlxGroup.h>
HX_DECLARE_CLASS0(NumberPad)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,group,FlxGroup)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  NumberPad_obj : public ::flixel::group::FlxGroup_obj{
	public:
		typedef ::flixel::group::FlxGroup_obj super;
		typedef NumberPad_obj OBJ_;
		NumberPad_obj();
		Void __construct();

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< NumberPad_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~NumberPad_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("NumberPad"); }

		::flixel::group::FlxGroup numPadGroup;
		::flixel::FlxSprite numberPad;
		::flixel::FlxSprite numZero;
		::flixel::FlxSprite numOne;
		::flixel::FlxSprite numTwo;
		::flixel::FlxSprite numThree;
		::flixel::FlxSprite numFour;
		::flixel::FlxSprite numFive;
		::flixel::FlxSprite numSix;
		::flixel::FlxSprite numSeven;
		::flixel::FlxSprite numEight;
		::flixel::FlxSprite numNine;
		::flixel::FlxSprite answerButt;
		::flixel::FlxSprite clearButt;
		::String answerDigits;
		bool answerButtonPressed;
		bool padScrollingIn;
		virtual ::String getPlayerAnswerString( );
		Dynamic getPlayerAnswerString_dyn();

		virtual bool getAnswerButtonPressed( );
		Dynamic getAnswerButtonPressed_dyn();

		virtual bool getPadScrollingIn( );
		Dynamic getPadScrollingIn_dyn();

		virtual Void setAnswerButtonPressed( bool val);
		Dynamic setAnswerButtonPressed_dyn();

		virtual Void setPadScrollingIn( bool val);
		Dynamic setPadScrollingIn_dyn();

		virtual Void resetAnswerDigits( );
		Dynamic resetAnswerDigits_dyn();

		virtual Void init( );
		Dynamic init_dyn();

		virtual Float getPadY( );
		Dynamic getPadY_dyn();

		virtual Void registerEvent( );
		Dynamic registerEvent_dyn();

		virtual Void unregisterNumPadEvents( );
		Dynamic unregisterNumPadEvents_dyn();

		virtual Void positionPieces( );
		Dynamic positionPieces_dyn();

		virtual Void numPadSetVisible( );
		Dynamic numPadSetVisible_dyn();

		virtual Void scrolling( );
		Dynamic scrolling_dyn();

		virtual Void padPress( ::flixel::FlxSprite currentButton);
		Dynamic padPress_dyn();

		virtual Void draw( );

		virtual Void update( );

		virtual Void destroy( );

};


#endif /* INCLUDED_NumberPad */ 
