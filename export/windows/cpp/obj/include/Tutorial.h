#ifndef INCLUDED_Tutorial
#define INCLUDED_Tutorial

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/group/FlxGroup.h>
HX_DECLARE_CLASS0(Tutorial)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,group,FlxGroup)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  Tutorial_obj : public ::flixel::group::FlxGroup_obj{
	public:
		typedef ::flixel::group::FlxGroup_obj super;
		typedef Tutorial_obj OBJ_;
		Tutorial_obj();
		Void __construct(Float tutorialType);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< Tutorial_obj > __new(Float tutorialType);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~Tutorial_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("Tutorial"); }

		Array< ::Dynamic > tutMessageArray;
		::flixel::group::FlxGroup tutorialGroup;
		::flixel::FlxSprite tutHand;
		::flixel::FlxSprite tutSeed;
		::flixel::FlxSprite tutMessageOne;
		::flixel::FlxSprite tutMessageTwo;
		::flixel::FlxSprite tutMessageThree;
		::flixel::FlxSprite tutMessageFour;
		::flixel::FlxSprite tutMessageFive;
		::flixel::FlxSprite tutMessageSix;
		::flixel::FlxSprite tutMessageSeven;
		::flixel::FlxSprite tutMessageEight;
		::flixel::FlxSprite tutMessageNine;
		::flixel::FlxSprite tutMessageTen;
		::flixel::FlxSprite tutTeacher;
		Float tutTimerOne;
		Float tutTimerTwo;
		Float fingerPressTimer;
		Float fingerHoverTimer;
		Float tutSectionNumber;
		int messageFadeDirection;
		int currentMessageIndex;
		int tutTreeGrowthVal;
		bool handMovingToSeed;
		bool moveHandWithSeed;
		bool handMovingToTile;
		virtual Float getCurrentTutSectionNumber( );
		Dynamic getCurrentTutSectionNumber_dyn();

		virtual Void init( Float tutType);
		Dynamic init_dyn();

		virtual Void getTutTreeGrowthVal( int growthValue);
		Dynamic getTutTreeGrowthVal_dyn();

		virtual Void eventInit( );
		Dynamic eventInit_dyn();

		virtual Void nextTutMessage( ::flixel::FlxSprite sprite);
		Dynamic nextTutMessage_dyn();

		virtual Void setTutType( Float tutType);
		Dynamic setTutType_dyn();

		virtual Void destroyCurrentMessage( ::flixel::FlxSprite sprite);
		Dynamic destroyCurrentMessage_dyn();

		virtual Void tutHandPos( Float tutType);
		Dynamic tutHandPos_dyn();

		virtual Void tutMessagePos( );
		Dynamic tutMessagePos_dyn();

		virtual Void tutorialRun( );
		Dynamic tutorialRun_dyn();

		virtual Void grabSeed( );
		Dynamic grabSeed_dyn();

		virtual Void tutHandFade( int fadeDirection);
		Dynamic tutHandFade_dyn();

		virtual Void messageFade( int messageFadeDirection);
		Dynamic messageFade_dyn();

		virtual Void handClickTruckButton( );
		Dynamic handClickTruckButton_dyn();

		virtual Void handClickShopTab( );
		Dynamic handClickShopTab_dyn();

		virtual Void moveToSeed( );
		Dynamic moveToSeed_dyn();

		virtual Void moveToTile( );
		Dynamic moveToTile_dyn();

		virtual Void moveToDropAlert( );
		Dynamic moveToDropAlert_dyn();

		virtual Void update( );

		virtual Void destroy( );

};


#endif /* INCLUDED_Tutorial */ 
