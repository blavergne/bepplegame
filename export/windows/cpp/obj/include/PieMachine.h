#ifndef INCLUDED_PieMachine
#define INCLUDED_PieMachine

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/group/FlxGroup.h>
HX_DECLARE_CLASS0(PieMachine)
HX_DECLARE_CLASS0(Tile)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,group,FlxGroup)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  PieMachine_obj : public ::flixel::group::FlxGroup_obj{
	public:
		typedef ::flixel::group::FlxGroup_obj super;
		typedef PieMachine_obj OBJ_;
		PieMachine_obj();
		Void __construct(::Tile tileSprite);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< PieMachine_obj > __new(::Tile tileSprite);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~PieMachine_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("PieMachine"); }

		::flixel::group::FlxGroup pieMachineGroup;
		::flixel::FlxSprite pieMachineSprite;
		::flixel::FlxSprite pieMachineAlertSprite;
		::flixel::FlxSprite minusOneSprite;
		::flixel::FlxSprite repairAlertSprite;
		::flixel::FlxSprite pieMachineFlyingPastry;
		Float myRelativeX;
		Float myRelativeY;
		Float electricUtilityTimer;
		Float machineReliabilityTimer;
		Float rise;
		Float run;
		int goldCount;
		int appleCount;
		bool payElectricBill;
		bool machineOn;
		bool machineAlertReset;
		bool machineClicked;
		bool repairClicked;
		bool animateRepairSprite;
		bool pastryFlying;
		bool inPiePack;
		::Tile myTile;
		virtual ::flixel::FlxSprite getpieMachineSprite( );
		Dynamic getpieMachineSprite_dyn();

		virtual bool getInPiePack( );
		Dynamic getInPiePack_dyn();

		virtual Float getMyRelativeX( );
		Dynamic getMyRelativeX_dyn();

		virtual bool getPastryFlying( );
		Dynamic getPastryFlying_dyn();

		virtual ::flixel::FlxSprite getPieMachineFlyingPastry( );
		Dynamic getPieMachineFlyingPastry_dyn();

		virtual Float getMyRelativeY( );
		Dynamic getMyRelativeY_dyn();

		virtual bool getMachineOn( );
		Dynamic getMachineOn_dyn();

		virtual bool getPayElectricBill( );
		Dynamic getPayElectricBill_dyn();

		virtual bool getRepairClicked( );
		Dynamic getRepairClicked_dyn();

		virtual bool getAnimateRepairSprite( );
		Dynamic getAnimateRepairSprite_dyn();

		virtual Void setAnimateRepairSprite( bool val);
		Dynamic setAnimateRepairSprite_dyn();

		virtual Void setPastryFlying( bool val);
		Dynamic setPastryFlying_dyn();

		virtual Void setRepairClicked( bool val);
		Dynamic setRepairClicked_dyn();

		virtual Void setInPiePack( bool val);
		Dynamic setInPiePack_dyn();

		virtual Void setPayElectricBill( bool val);
		Dynamic setPayElectricBill_dyn();

		virtual Void setMachineOn( bool val);
		Dynamic setMachineOn_dyn();

		virtual Void setMyRelativeX( Float val);
		Dynamic setMyRelativeX_dyn();

		virtual Void setMyRelativeY( Float val);
		Dynamic setMyRelativeY_dyn();

		virtual Void setGoldCount( int val);
		Dynamic setGoldCount_dyn();

		virtual Void setAppleCount( int val);
		Dynamic setAppleCount_dyn();

		virtual Void registerMachineEvents( );
		Dynamic registerMachineEvents_dyn();

		virtual Void unregisterMachineEvents( );
		Dynamic unregisterMachineEvents_dyn();

		virtual Void repairAlertClicked( ::flixel::FlxSprite sprite);
		Dynamic repairAlertClicked_dyn();

		virtual Void turnMachineOn( ::flixel::FlxSprite sprite);
		Dynamic turnMachineOn_dyn();

		virtual Void payElectricUtility( );
		Dynamic payElectricUtility_dyn();

		virtual Void machineReliability( );
		Dynamic machineReliability_dyn();

		virtual Void resetRepairAlertTimer( );
		Dynamic resetRepairAlertTimer_dyn();

		virtual Void repairSpriteAnimation( );
		Dynamic repairSpriteAnimation_dyn();

		virtual Void setRiseRun( );
		Dynamic setRiseRun_dyn();

		virtual Void sendPastryToPack( );
		Dynamic sendPastryToPack_dyn();

		virtual Void update( );

		virtual Void destroy( );

};


#endif /* INCLUDED_PieMachine */ 
