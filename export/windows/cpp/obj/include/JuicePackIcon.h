#ifndef INCLUDED_JuicePackIcon
#define INCLUDED_JuicePackIcon

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/group/FlxGroup.h>
HX_DECLARE_CLASS0(JuicePackIcon)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,group,FlxGroup)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  JuicePackIcon_obj : public ::flixel::group::FlxGroup_obj{
	public:
		typedef ::flixel::group::FlxGroup_obj super;
		typedef JuicePackIcon_obj OBJ_;
		JuicePackIcon_obj();
		Void __construct();

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< JuicePackIcon_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~JuicePackIcon_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("JuicePackIcon"); }

		::flixel::group::FlxGroup juicePackGroup;
		::flixel::FlxSprite juicePackSprite;
		virtual ::flixel::FlxSprite getJuicePackSprite( );
		Dynamic getJuicePackSprite_dyn();

		static Float JuicePackPosX;
		static Float JuicePackPosY;
};


#endif /* INCLUDED_JuicePackIcon */ 
