#ifndef INCLUDED_AsteroidBelt
#define INCLUDED_AsteroidBelt

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/group/FlxGroup.h>
HX_DECLARE_CLASS0(AsteroidBelt)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,group,FlxGroup)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  AsteroidBelt_obj : public ::flixel::group::FlxGroup_obj{
	public:
		typedef ::flixel::group::FlxGroup_obj super;
		typedef AsteroidBelt_obj OBJ_;
		AsteroidBelt_obj();
		Void __construct(int beltXPos);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< AsteroidBelt_obj > __new(int beltXPos);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~AsteroidBelt_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("AsteroidBelt"); }

		::flixel::FlxSprite asteroidBelt;
		virtual ::flixel::FlxSprite getAsteroidBeltSprite( );
		Dynamic getAsteroidBeltSprite_dyn();

		virtual Void setAsteroidBeltSprite( int val);
		Dynamic setAsteroidBeltSprite_dyn();

		virtual Void asteroidFloat( );
		Dynamic asteroidFloat_dyn();

		virtual Void destroyAsteroids( );
		Dynamic destroyAsteroids_dyn();

		virtual Void destroy( );

		virtual Void update( );

};


#endif /* INCLUDED_AsteroidBelt */ 
