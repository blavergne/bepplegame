#ifndef INCLUDED_ProblemBoard
#define INCLUDED_ProblemBoard

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/group/FlxGroup.h>
HX_DECLARE_CLASS0(NumberChalk)
HX_DECLARE_CLASS0(ProblemBoard)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,group,FlxGroup)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  ProblemBoard_obj : public ::flixel::group::FlxGroup_obj{
	public:
		typedef ::flixel::group::FlxGroup_obj super;
		typedef ProblemBoard_obj OBJ_;
		ProblemBoard_obj();
		Void __construct();

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< ProblemBoard_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~ProblemBoard_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("ProblemBoard"); }

		::flixel::group::FlxGroup problemGroup;
		::flixel::FlxSprite blackBoard;
		::flixel::FlxSprite blackBoardCorrectBoarder;
		::flixel::FlxSprite blackBoardIncorrectBoarder;
		::flixel::FlxSprite singleDigitL;
		::flixel::FlxSprite singleDigitR;
		::flixel::FlxSprite operationVal;
		int problemAnswer;
		::String problemAnswerString;
		::flixel::FlxSprite equalsSign;
		::flixel::FlxSprite plusSign;
		::flixel::FlxSprite minusSign;
		::flixel::FlxSprite multiSign;
		::NumberChalk numberObjLeft;
		::NumberChalk numberObjRight;
		::flixel::FlxSprite leftSideNumber;
		::flixel::FlxSprite rightSideNumber;
		Array< ::Dynamic > numArray;
		Array< ::Dynamic > operationArray;
		int opShift;
		int opShiftX;
		virtual Void init( );
		Dynamic init_dyn();

		virtual Void boardVisibile( bool val);
		Dynamic boardVisibile_dyn();

		virtual int getProblemAnswer( );
		Dynamic getProblemAnswer_dyn();

		virtual ::String getProblemAnswerString( );
		Dynamic getProblemAnswerString_dyn();

		virtual Void resetProblemString( );
		Dynamic resetProblemString_dyn();

		virtual Float getBoardY( );
		Dynamic getBoardY_dyn();

		virtual bool getBoardInUse( );
		Dynamic getBoardInUse_dyn();

		virtual Void setBoardInUse( bool val);
		Dynamic setBoardInUse_dyn();

		virtual Void setBoarderColor( ::String val);
		Dynamic setBoarderColor_dyn();

		virtual Void boardScrollDown( );
		Dynamic boardScrollDown_dyn();

		virtual Void solveProblem( int operation,int leftRand,int rightRand);
		Dynamic solveProblem_dyn();

		virtual Void pickOperation( ::String opType);
		Dynamic pickOperation_dyn();

		virtual Void numberToString( Float number);
		Dynamic numberToString_dyn();

		virtual Void reverseAnswerString( );
		Dynamic reverseAnswerString_dyn();

		virtual Void updateBoardPieces( );
		Dynamic updateBoardPieces_dyn();

		virtual Void update( );

		virtual Void destroy( );

		static bool boardInUse;
};


#endif /* INCLUDED_ProblemBoard */ 
