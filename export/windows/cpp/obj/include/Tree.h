#ifndef INCLUDED_Tree
#define INCLUDED_Tree

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/group/FlxGroup.h>
HX_DECLARE_CLASS0(Apple)
HX_DECLARE_CLASS0(Mouse)
HX_DECLARE_CLASS0(Tile)
HX_DECLARE_CLASS0(Tree)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,group,FlxGroup)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)
HX_DECLARE_CLASS2(flixel,system,FlxSound)
HX_DECLARE_CLASS2(flixel,util,FlxTimer)


class HXCPP_CLASS_ATTRIBUTES  Tree_obj : public ::flixel::group::FlxGroup_obj{
	public:
		typedef ::flixel::group::FlxGroup_obj super;
		typedef Tree_obj OBJ_;
		Tree_obj();
		Void __construct(::flixel::FlxSprite basket);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< Tree_obj > __new(::flixel::FlxSprite basket);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~Tree_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("Tree"); }

		::flixel::group::FlxGroup treeGroup;
		Array< ::Dynamic > appleArray;
		::Apple apple;
		::Tile myTile;
		::Mouse mouse;
		::flixel::FlxSprite treeGround;
		::flixel::FlxSprite tree;
		::flixel::FlxSprite droplet;
		::flixel::FlxSprite dropletA;
		::flixel::FlxSprite dropletB;
		::flixel::FlxSprite sunA;
		::flixel::FlxSprite sunB;
		::flixel::FlxSprite sunC;
		::flixel::FlxSprite sprinklerSprite;
		::flixel::FlxSprite sprinklerButtonSprite;
		::flixel::FlxSprite minusOneSprite;
		::flixel::FlxSprite basketSprite;
		::flixel::system::FlxSound sprinklerSound;
		::flixel::system::FlxSound treeGrowthSound;
		::flixel::group::FlxGroup boardObj;
		::String answerString;
		int maxThirst;
		int minThirst;
		int treeGrowthPhase;
		int treeThirst;
		int treeSunlight;
		int appleCount;
		int appleIndex;
		int adultTree;
		int takenAppleIndex;
		int goldCount;
		Float treeThirstTimer;
		Float treeSunLightTimer;
		Float oldTreeSunLightTimer;
		Float waterUtlilityTimer;
		Float sprinklerOutTime;
		Float timeToTreeDeath;
		Float appleSeperator;
		Float mouseTimer;
		Float sprinklerAnimationTimer;
		::flixel::util::FlxTimer dropAlertTime;
		::flixel::util::FlxTimer sunlightAlertTime;
		bool oldSunTimerChanged;
		bool mouseInPlay;
		bool dropletClicked;
		bool sunClicked;
		bool treeClicked;
		bool appleLock;
		bool amIdeadYet;
		bool appleClicked;
		bool takenAppleCarried;
		bool checkingCollisions;
		bool sprinklerSystemOn;
		bool sprinklerOutPhase;
		bool sprinklerInPhase;
		bool extendedTime;
		bool payWaterBill;
		bool appleStored;
		bool deathRattle;
		virtual bool getTreeClicked( );
		Dynamic getTreeClicked_dyn();

		virtual ::flixel::FlxSprite getTreeSprite( );
		Dynamic getTreeSprite_dyn();

		virtual Float getTreeX( );
		Dynamic getTreeX_dyn();

		virtual Float getTreeY( );
		Dynamic getTreeY_dyn();

		virtual bool getAmIDeadYet( );
		Dynamic getAmIDeadYet_dyn();

		virtual int getTreeThirst( );
		Dynamic getTreeThirst_dyn();

		virtual bool getAppleStored( );
		Dynamic getAppleStored_dyn();

		virtual Void setAppleStored( bool val);
		Dynamic setAppleStored_dyn();

		virtual bool getDropletClicked( );
		Dynamic getDropletClicked_dyn();

		virtual bool getSunClicked( );
		Dynamic getSunClicked_dyn();

		virtual bool getUniversalAlertClicked( );
		Dynamic getUniversalAlertClicked_dyn();

		virtual ::Tile getMyTile( );
		Dynamic getMyTile_dyn();

		virtual bool getAppleClicked( );
		Dynamic getAppleClicked_dyn();

		virtual int getTreeGrowthPhase( );
		Dynamic getTreeGrowthPhase_dyn();

		virtual bool getSprinklerSystemOn( );
		Dynamic getSprinklerSystemOn_dyn();

		virtual ::flixel::FlxSprite getSprinklerButtonSprite( );
		Dynamic getSprinklerButtonSprite_dyn();

		virtual bool getPayWaterBill( );
		Dynamic getPayWaterBill_dyn();

		virtual Array< ::Dynamic > getAppleArray( );
		Dynamic getAppleArray_dyn();

		virtual Void setTreeThirstTut( int tutThirstTimer);
		Dynamic setTreeThirstTut_dyn();

		virtual Void setTreeSunlightTut( int tutSunlightTimer);
		Dynamic setTreeSunlightTut_dyn();

		virtual Void setTreeThistTutOff( );
		Dynamic setTreeThistTutOff_dyn();

		virtual Void setTreeSunlightTutOff( );
		Dynamic setTreeSunlightTutOff_dyn();

		virtual Void setPayWaterBill( bool val);
		Dynamic setPayWaterBill_dyn();

		virtual Void setSprinklerSystemOn( bool val);
		Dynamic setSprinklerSystemOn_dyn();

		virtual Void setTreeClicked( bool val);
		Dynamic setTreeClicked_dyn();

		virtual Void setAppleClicked( bool val);
		Dynamic setAppleClicked_dyn();

		virtual Void setMyTile( ::Tile val);
		Dynamic setMyTile_dyn();

		virtual Void setTreeGroundColor( bool owned);
		Dynamic setTreeGroundColor_dyn();

		virtual Void setTreeGrowthPhase( int val);
		Dynamic setTreeGrowthPhase_dyn();

		virtual Void setUniversalAlertClicked( bool val);
		Dynamic setUniversalAlertClicked_dyn();

		virtual Void setDropletClicked( bool val);
		Dynamic setDropletClicked_dyn();

		virtual Void setSunClicked( bool val);
		Dynamic setSunClicked_dyn();

		virtual Void setTreeX( Float xVal);
		Dynamic setTreeX_dyn();

		virtual Void setTreeY( Float yVal);
		Dynamic setTreeY_dyn();

		virtual Void setTreeWatered( );
		Dynamic setTreeWatered_dyn();

		virtual Void setTreeThirsty( ::flixel::util::FlxTimer timer);
		Dynamic setTreeThirsty_dyn();

		virtual Void setTreeSunLight( );
		Dynamic setTreeSunLight_dyn();

		virtual Void setOldTreeSunLightTimer( Float oldSunlightTimerVal);
		Dynamic setOldTreeSunLightTimer_dyn();

		virtual Void setThirstValue( int thirstVal);
		Dynamic setThirstValue_dyn();

		virtual Void setSunlightValue( int sunlightVal);
		Dynamic setSunlightValue_dyn();

		virtual Void setTreeNoLight( ::flixel::util::FlxTimer timer);
		Dynamic setTreeNoLight_dyn();

		virtual Void pauseAlertTimers( );
		Dynamic pauseAlertTimers_dyn();

		virtual Void unpauseAlertTimers( );
		Dynamic unpauseAlertTimers_dyn();

		virtual Void initAppleArray( );
		Dynamic initAppleArray_dyn();

		virtual Void resetDropTimer( );
		Dynamic resetDropTimer_dyn();

		virtual Void resetSunTimer( );
		Dynamic resetSunTimer_dyn();

		virtual Void registerTreeClickedMouseEvent( );
		Dynamic registerTreeClickedMouseEvent_dyn();

		virtual Void unregisterTreeClickedMouseEvent( );
		Dynamic unregisterTreeClickedMouseEvent_dyn();

		virtual Void registerAppleEvent( );
		Dynamic registerAppleEvent_dyn();

		virtual Void unregisterAppleEvent( );
		Dynamic unregisterAppleEvent_dyn();

		virtual Void registerEvents( );
		Dynamic registerEvents_dyn();

		virtual Void unregisterEvents( );
		Dynamic unregisterEvents_dyn();

		virtual Void openProblem( ::flixel::FlxSprite sprite);
		Dynamic openProblem_dyn();

		virtual Void purchaseSprinklerBuff( ::flixel::FlxSprite sprite);
		Dynamic purchaseSprinklerBuff_dyn();

		virtual Void treeThirstCheck( );
		Dynamic treeThirstCheck_dyn();

		virtual Void treeSunlightCheck( );
		Dynamic treeSunlightCheck_dyn();

		virtual Void warnThirst( bool val);
		Dynamic warnThirst_dyn();

		virtual Void warnSunlight( bool val);
		Dynamic warnSunlight_dyn();

		virtual Void appleIncubator( );
		Dynamic appleIncubator_dyn();

		virtual Void releaseAppleMem( );
		Dynamic releaseAppleMem_dyn();

		virtual Void killAllApples( );
		Dynamic killAllApples_dyn();

		virtual Void addAppleToArray( );
		Dynamic addAppleToArray_dyn();

		virtual Void appleStuff( );
		Dynamic appleStuff_dyn();

		virtual Void pickApple( ::flixel::FlxSprite sprite);
		Dynamic pickApple_dyn();

		virtual Void killDeadApple( );
		Dynamic killDeadApple_dyn();

		virtual bool checkAppleOverlap( );
		Dynamic checkAppleOverlap_dyn();

		virtual Void mouseAttack( );
		Dynamic mouseAttack_dyn();

		virtual bool checkForAppleRotTime( );
		Dynamic checkForAppleRotTime_dyn();

		virtual Void checkMouseDone( );
		Dynamic checkMouseDone_dyn();

		virtual Void checkForMouseCollisions( );
		Dynamic checkForMouseCollisions_dyn();

		virtual Void carryApple( );
		Dynamic carryApple_dyn();

		virtual Void incGrowTree( );
		Dynamic incGrowTree_dyn();

		virtual Void growTree( );
		Dynamic growTree_dyn();

		virtual Void positionAlerts( );
		Dynamic positionAlerts_dyn();

		virtual Void collideGroundCheck( );
		Dynamic collideGroundCheck_dyn();

		virtual Void sprinklerAnimation( );
		Dynamic sprinklerAnimation_dyn();

		virtual Void currentGoldCount( int gold);
		Dynamic currentGoldCount_dyn();

		virtual Void payWaterUtility( );
		Dynamic payWaterUtility_dyn();

		virtual Void destroySounds( );
		Dynamic destroySounds_dyn();

		virtual Void draw( );

		virtual Void kill( );

		virtual Void update( );

		virtual Void destroy( );

		static bool universalAlertClicked;
};


#endif /* INCLUDED_Tree */ 
