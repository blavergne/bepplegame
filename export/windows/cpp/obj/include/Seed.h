#ifndef INCLUDED_Seed
#define INCLUDED_Seed

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/FlxSprite.h>
HX_DECLARE_CLASS0(Seed)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  Seed_obj : public ::flixel::FlxSprite_obj{
	public:
		typedef ::flixel::FlxSprite_obj super;
		typedef Seed_obj OBJ_;
		Seed_obj();
		Void __construct(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< Seed_obj > __new(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~Seed_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		::String __ToString() const { return HX_CSTRING("Seed"); }

		bool dragging;
		int homeX;
		int homeY;
		virtual Void onDown( ::flixel::FlxSprite sprite);
		Dynamic onDown_dyn();

		virtual Void onUp( ::flixel::FlxSprite sprite);
		Dynamic onUp_dyn();

		virtual Void dragSeed( bool temp);
		Dynamic dragSeed_dyn();

		virtual Void resetSeedPos( );
		Dynamic resetSeedPos_dyn();

		virtual bool getDragging( );
		Dynamic getDragging_dyn();

		virtual Void setDragging( bool val);
		Dynamic setDragging_dyn();

		virtual Void checkMouseState( );
		Dynamic checkMouseState_dyn();

		virtual Void unregisterSeedEvent( );
		Dynamic unregisterSeedEvent_dyn();

		virtual Void registerSeedEvent( );
		Dynamic registerSeedEvent_dyn();

		virtual Void update( );

		virtual Void destroy( );

};


#endif /* INCLUDED_Seed */ 
