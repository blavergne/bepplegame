#ifndef INCLUDED_Market
#define INCLUDED_Market

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/group/FlxGroup.h>
HX_DECLARE_CLASS0(GoldCoin)
HX_DECLARE_CLASS0(JuiceItem)
HX_DECLARE_CLASS0(Market)
HX_DECLARE_CLASS0(MarketItem)
HX_DECLARE_CLASS0(PieItem)
HX_DECLARE_CLASS0(SeedItem)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,group,FlxGroup)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)
HX_DECLARE_CLASS2(flixel,system,FlxSound)
HX_DECLARE_CLASS2(flixel,text,FlxText)


class HXCPP_CLASS_ATTRIBUTES  Market_obj : public ::flixel::group::FlxGroup_obj{
	public:
		typedef ::flixel::group::FlxGroup_obj super;
		typedef Market_obj OBJ_;
		Market_obj();
		Void __construct();

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< Market_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~Market_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("Market"); }

		::flixel::group::FlxGroup marketGroup;
		::flixel::FlxSprite marketSideBarSprite;
		::SeedItem seedItem;
		::JuiceItem juiceItem;
		::PieItem pieItem;
		::flixel::text::FlxText seedsAvailableText;
		::flixel::text::FlxText seedItemCountFieldText;
		::flixel::text::FlxText juiceItemCountFieldText;
		::flixel::text::FlxText pieItemCountFieldText;
		::flixel::text::FlxText seedItemCostFieldText;
		::flixel::text::FlxText juiceItemCostFieldText;
		::flixel::text::FlxText pieItemCostFieldText;
		::flixel::text::FlxText juicePackCountText;
		::flixel::text::FlxText piePackCountText;
		Array< ::Dynamic > itemArray;
		::GoldCoin coin;
		int coinCount;
		::String coinCountString;
		::flixel::text::FlxText coinCountFieldText;
		int piePack;
		int juicePack;
		int seedCount;
		int juiceCount;
		int pieCount;
		int waterClientCount;
		int electricClientCount;
		int shiftX;
		int shiftY;
		::String seedCountString;
		::flixel::text::FlxText seedCountText;
		::flixel::text::FlxText juiceCountText;
		::flixel::text::FlxText pieCountText;
		Float marketCloseTimer;
		Float replenishSeedTimer;
		Float replenishJuiceMachineTimer;
		Float replenishPieMachineSeedTimer;
		Float waterBillTimer;
		Float electricBillTimer;
		bool marketOpen;
		bool turn;
		::flixel::system::FlxSound marketOpenSound;
		::flixel::system::FlxSound marketClosedSound;
		virtual bool getMarketOpen( );
		Dynamic getMarketOpen_dyn();

		virtual int getCoinCount( );
		Dynamic getCoinCount_dyn();

		virtual int getSeedCount( );
		Dynamic getSeedCount_dyn();

		virtual int getJuiceCount( );
		Dynamic getJuiceCount_dyn();

		virtual int getPieCount( );
		Dynamic getPieCount_dyn();

		virtual int getJuicePackCount( );
		Dynamic getJuicePackCount_dyn();

		virtual int getPiePackCount( );
		Dynamic getPiePackCount_dyn();

		virtual Void stockJuicePacks( int val);
		Dynamic stockJuicePacks_dyn();

		virtual Void stockPiePacks( int val);
		Dynamic stockPiePacks_dyn();

		virtual Void loadJuicePacksDec( );
		Dynamic loadJuicePacksDec_dyn();

		virtual Void loadPiePacksDec( );
		Dynamic loadPiePacksDec_dyn();

		virtual Void setJuicePackCount( int val);
		Dynamic setJuicePackCount_dyn();

		virtual Void setPiePackCount( int val);
		Dynamic setPiePackCount_dyn();

		virtual Void setSeedCount( int val);
		Dynamic setSeedCount_dyn();

		virtual Void setseedItemCount( int val);
		Dynamic setseedItemCount_dyn();

		virtual Void setPieCount( int val);
		Dynamic setPieCount_dyn();

		virtual Void setPieItemCount( int val);
		Dynamic setPieItemCount_dyn();

		virtual Void setJuiceCount( int val);
		Dynamic setJuiceCount_dyn();

		virtual Void setJuiceItemCount( int val);
		Dynamic setJuiceItemCount_dyn();

		virtual Void setCoinCount( int val);
		Dynamic setCoinCount_dyn();

		virtual Void setWaterClientCount( int val);
		Dynamic setWaterClientCount_dyn();

		virtual Void setElectricClientCount( int val);
		Dynamic setElectricClientCount_dyn();

		virtual Void addSubtractCoins( ::String operation,int perTen);
		Dynamic addSubtractCoins_dyn();

		virtual Void displayCoinCount( );
		Dynamic displayCoinCount_dyn();

		virtual Void displaySeedCount( );
		Dynamic displaySeedCount_dyn();

		virtual Void displayItemCountText( );
		Dynamic displayItemCountText_dyn();

		virtual Void decSeedCount( );
		Dynamic decSeedCount_dyn();

		virtual Void incSeedCount( );
		Dynamic incSeedCount_dyn();

		virtual Void decjuiceCount( int val);
		Dynamic decjuiceCount_dyn();

		virtual Void decjuicePackCount( int val);
		Dynamic decjuicePackCount_dyn();

		virtual Void incJuiceCount( );
		Dynamic incJuiceCount_dyn();

		virtual Void decPieCount( );
		Dynamic decPieCount_dyn();

		virtual Void incPieCount( );
		Dynamic incPieCount_dyn();

		virtual Void coinPaymentUtil( int val);
		Dynamic coinPaymentUtil_dyn();

		virtual Void positionArrayItems( );
		Dynamic positionArrayItems_dyn();

		virtual Void fillItemArray( );
		Dynamic fillItemArray_dyn();

		virtual Void registerEvents( );
		Dynamic registerEvents_dyn();

		virtual Void unregisterEvents( );
		Dynamic unregisterEvents_dyn();

		virtual Void selectItem( ::flixel::FlxSprite sprite);
		Dynamic selectItem_dyn();

		virtual Void purchaseItem( ::MarketItem item);
		Dynamic purchaseItem_dyn();

		virtual Void openMarket( ::flixel::FlxSprite sprite);
		Dynamic openMarket_dyn();

		virtual Void slideMarketOut( ::flixel::FlxSprite sprite);
		Dynamic slideMarketOut_dyn();

		virtual Void timedMarketClose( );
		Dynamic timedMarketClose_dyn();

		virtual Void shiftItemsOpen( );
		Dynamic shiftItemsOpen_dyn();

		virtual Void shiftItemsClose( );
		Dynamic shiftItemsClose_dyn();

		virtual Void replenishItems( );
		Dynamic replenishItems_dyn();

		virtual Void waterBillCoinPayment( );
		Dynamic waterBillCoinPayment_dyn();

		virtual Void ElectricBillCoinPayment( );
		Dynamic ElectricBillCoinPayment_dyn();

		virtual Void destroySound( );
		Dynamic destroySound_dyn();

		virtual Void update( );

		virtual Void destroy( );

};


#endif /* INCLUDED_Market */ 
