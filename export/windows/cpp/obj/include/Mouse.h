#ifndef INCLUDED_Mouse
#define INCLUDED_Mouse

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/group/FlxGroup.h>
HX_DECLARE_CLASS0(Mouse)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,group,FlxGroup)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)
HX_DECLARE_CLASS2(flixel,system,FlxSound)


class HXCPP_CLASS_ATTRIBUTES  Mouse_obj : public ::flixel::group::FlxGroup_obj{
	public:
		typedef ::flixel::group::FlxGroup_obj super;
		typedef Mouse_obj OBJ_;
		Mouse_obj();
		Void __construct();

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< Mouse_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~Mouse_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("Mouse"); }

		::flixel::group::FlxGroup mouseGroup;
		::flixel::FlxSprite mouse;
		::flixel::FlxSprite mouseHole;
		::flixel::system::FlxSound mouseHoleSound;
		::flixel::system::FlxSound mouseWalkSound;
		bool mouseWalking;
		bool turnAround;
		bool goHome;
		bool timeToDie;
		bool holeClosed;
		bool removeMe;
		bool firstPhase;
		bool secondPhase;
		bool thirdPhase;
		bool mouseCollision;
		bool timeToCloseHole;
		bool firstFinishRetired;
		bool secondFinishRetired;
		bool thirdFinishRetired;
		bool mouseAllDone;
		bool lastCheck;
		Float mouseWalkPause;
		Float mouseJumpPause;
		Float homeAnimationPause;
		int mouseMovementSpeed;
		virtual Void initMouse( );
		Dynamic initMouse_dyn();

		virtual Void mouseWork( );
		Dynamic mouseWork_dyn();

		virtual ::flixel::FlxSprite getMouseSprite( );
		Dynamic getMouseSprite_dyn();

		virtual bool getMouseAllDone( );
		Dynamic getMouseAllDone_dyn();

		virtual ::flixel::FlxSprite getHoleSprite( );
		Dynamic getHoleSprite_dyn();

		virtual Void setMouseCollision( bool val);
		Dynamic setMouseCollision_dyn();

		virtual Void destroy( );

		virtual Void faceRight( );
		Dynamic faceRight_dyn();

		virtual Void update( );

};


#endif /* INCLUDED_Mouse */ 
