#ifndef INCLUDED_JuiceIcon
#define INCLUDED_JuiceIcon

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/FlxSprite.h>
HX_DECLARE_CLASS0(JuiceIcon)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  JuiceIcon_obj : public ::flixel::FlxSprite_obj{
	public:
		typedef ::flixel::FlxSprite_obj super;
		typedef JuiceIcon_obj OBJ_;
		JuiceIcon_obj();
		Void __construct(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< JuiceIcon_obj > __new(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~JuiceIcon_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		::String __ToString() const { return HX_CSTRING("JuiceIcon"); }

		bool dragging;
		int homeX;
		int homeY;
		virtual Void onDown( ::flixel::FlxSprite sprite);
		Dynamic onDown_dyn();

		virtual Void onUp( ::flixel::FlxSprite sprite);
		Dynamic onUp_dyn();

		virtual Void dragSeed( bool temp);
		Dynamic dragSeed_dyn();

		virtual Void resetSeedPos( );
		Dynamic resetSeedPos_dyn();

		virtual bool getDragging( );
		Dynamic getDragging_dyn();

		virtual Void setDragging( bool val);
		Dynamic setDragging_dyn();

		virtual Void checkMouseState( );
		Dynamic checkMouseState_dyn();

		virtual Void unregisterJuiceEvent( );
		Dynamic unregisterJuiceEvent_dyn();

		virtual Void registerJuiceEvent( );
		Dynamic registerJuiceEvent_dyn();

		virtual Void update( );

		virtual Void destroy( );

};


#endif /* INCLUDED_JuiceIcon */ 
