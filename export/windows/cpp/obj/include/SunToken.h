#ifndef INCLUDED_SunToken
#define INCLUDED_SunToken

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/group/FlxGroup.h>
HX_DECLARE_CLASS0(SunToken)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,group,FlxGroup)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)
HX_DECLARE_CLASS2(flixel,system,FlxSound)


class HXCPP_CLASS_ATTRIBUTES  SunToken_obj : public ::flixel::group::FlxGroup_obj{
	public:
		typedef ::flixel::group::FlxGroup_obj super;
		typedef SunToken_obj OBJ_;
		SunToken_obj();
		Void __construct();

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< SunToken_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~SunToken_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("SunToken"); }

		::flixel::group::FlxGroup sunGroup;
		::flixel::FlxSprite sun;
		::flixel::system::FlxSound sunSound;
		bool sunDone;
		bool destroySun;
		virtual Float getSunAlpha( );
		Dynamic getSunAlpha_dyn();

		virtual Void setSunX( Float xVal);
		Dynamic setSunX_dyn();

		virtual Void setSunY( Float yVal);
		Dynamic setSunY_dyn();

		virtual Void fadeSun( );
		Dynamic fadeSun_dyn();

		virtual Void update( );

		virtual Void destroy( );

};


#endif /* INCLUDED_SunToken */ 
