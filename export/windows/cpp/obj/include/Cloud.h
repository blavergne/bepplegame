#ifndef INCLUDED_Cloud
#define INCLUDED_Cloud

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/group/FlxGroup.h>
HX_DECLARE_CLASS0(Cloud)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,group,FlxGroup)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)
HX_DECLARE_CLASS2(flixel,system,FlxSound)


class HXCPP_CLASS_ATTRIBUTES  Cloud_obj : public ::flixel::group::FlxGroup_obj{
	public:
		typedef ::flixel::group::FlxGroup_obj super;
		typedef Cloud_obj OBJ_;
		Cloud_obj();
		Void __construct();

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< Cloud_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~Cloud_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("Cloud"); }

		::flixel::group::FlxGroup cloudGroup;
		::flixel::FlxSprite cloud;
		::flixel::system::FlxSound rainSound;
		bool cloudDone;
		bool destroyCloud;
		virtual Float getCloudAlpha( );
		Dynamic getCloudAlpha_dyn();

		virtual Void setCloudX( Float xVal);
		Dynamic setCloudX_dyn();

		virtual Void setCloudY( Float yVal);
		Dynamic setCloudY_dyn();

		virtual Void fadeCloud( );
		Dynamic fadeCloud_dyn();

		virtual Void update( );

		virtual Void destroy( );

};


#endif /* INCLUDED_Cloud */ 
