#ifndef INCLUDED_MarketItem
#define INCLUDED_MarketItem

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/FlxSprite.h>
HX_DECLARE_CLASS0(MarketItem)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)
HX_DECLARE_CLASS2(flixel,text,FlxText)


class HXCPP_CLASS_ATTRIBUTES  MarketItem_obj : public ::flixel::FlxSprite_obj{
	public:
		typedef ::flixel::FlxSprite_obj super;
		typedef MarketItem_obj OBJ_;
		MarketItem_obj();
		Void __construct();

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< MarketItem_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~MarketItem_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("MarketItem"); }

		::flixel::FlxSprite itemSprite;
		::String itemName;
		int itemCost;
		int itemCount;
		bool itemAvailable;
		bool itemClicked;
		::flixel::text::FlxText itemCountFieldText;
		virtual ::flixel::FlxSprite getItemSprite( );
		Dynamic getItemSprite_dyn();

		virtual int getItemCost( );
		Dynamic getItemCost_dyn();

		virtual ::String getItemName( );
		Dynamic getItemName_dyn();

		virtual int getItemCount( );
		Dynamic getItemCount_dyn();

		virtual bool getItemClicked( );
		Dynamic getItemClicked_dyn();

		virtual Void decItemCost( );
		Dynamic decItemCost_dyn();

		virtual Void incItemCost( );
		Dynamic incItemCost_dyn();

		virtual Void decItemCount( );
		Dynamic decItemCount_dyn();

		virtual Void incItemCount( );
		Dynamic incItemCount_dyn();

		virtual Void setItemAvailable( bool available);
		Dynamic setItemAvailable_dyn();

		virtual bool getItemAvailable( );
		Dynamic getItemAvailable_dyn();

		virtual Void setAvailableAnimation( );
		Dynamic setAvailableAnimation_dyn();

};


#endif /* INCLUDED_MarketItem */ 
