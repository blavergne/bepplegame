#ifndef INCLUDED_flixel_input_android_FlxAndroidKeyList
#define INCLUDED_flixel_input_android_FlxAndroidKeyList

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS3(flixel,input,android,FlxAndroidKeyList)
namespace flixel{
namespace input{
namespace android{


class HXCPP_CLASS_ATTRIBUTES  FlxAndroidKeyList_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef FlxAndroidKeyList_obj OBJ_;
		FlxAndroidKeyList_obj();
		Void __construct(Dynamic CheckFunction);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< FlxAndroidKeyList_obj > __new(Dynamic CheckFunction);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~FlxAndroidKeyList_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("FlxAndroidKeyList"); }

		Dynamic check;
		Dynamic &check_dyn() { return check;}
		virtual bool get_BACK( );
		Dynamic get_BACK_dyn();

		virtual bool get_MENU( );
		Dynamic get_MENU_dyn();

};

} // end namespace flixel
} // end namespace input
} // end namespace android

#endif /* INCLUDED_flixel_input_android_FlxAndroidKeyList */ 
