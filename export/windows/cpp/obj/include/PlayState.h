#ifndef INCLUDED_PlayState
#define INCLUDED_PlayState

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/FlxState.h>
HX_DECLARE_CLASS0(AppleBasket)
HX_DECLARE_CLASS0(AppleShed)
HX_DECLARE_CLASS0(AppleTruck)
HX_DECLARE_CLASS0(AsteroidBelt)
HX_DECLARE_CLASS0(Cloud)
HX_DECLARE_CLASS0(Island)
HX_DECLARE_CLASS0(JuiceIcon)
HX_DECLARE_CLASS0(JuiceMachine)
HX_DECLARE_CLASS0(JuicePackIcon)
HX_DECLARE_CLASS0(Market)
HX_DECLARE_CLASS0(NumberPad)
HX_DECLARE_CLASS0(OptionsScreen)
HX_DECLARE_CLASS0(PieIcon)
HX_DECLARE_CLASS0(PieMachine)
HX_DECLARE_CLASS0(PiePackIcon)
HX_DECLARE_CLASS0(PlayState)
HX_DECLARE_CLASS0(ProblemBoard)
HX_DECLARE_CLASS0(Seed)
HX_DECLARE_CLASS0(SunToken)
HX_DECLARE_CLASS0(SystemStar)
HX_DECLARE_CLASS0(Tile)
HX_DECLARE_CLASS0(TileBoard)
HX_DECLARE_CLASS0(TileBoarder)
HX_DECLARE_CLASS0(Tree)
HX_DECLARE_CLASS0(TruckButton)
HX_DECLARE_CLASS0(Tutorial)
HX_DECLARE_CLASS0(Weed)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS1(flixel,FlxState)
HX_DECLARE_CLASS2(flixel,group,FlxGroup)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)
HX_DECLARE_CLASS2(flixel,system,FlxSound)
HX_DECLARE_CLASS2(flixel,text,FlxText)
HX_DECLARE_CLASS2(flixel,ui,FlxButton)
HX_DECLARE_CLASS2(flixel,ui,FlxTypedButton)


class HXCPP_CLASS_ATTRIBUTES  PlayState_obj : public ::flixel::FlxState_obj{
	public:
		typedef ::flixel::FlxState_obj super;
		typedef PlayState_obj OBJ_;
		PlayState_obj();
		Void __construct(Dynamic MaxSize);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< PlayState_obj > __new(Dynamic MaxSize);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~PlayState_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("PlayState"); }

		::flixel::ui::FlxButton menuButton;
		::OptionsScreen menuScreen;
		::Tutorial tutorial;
		::Weed weed;
		::SystemStar systemStar;
		::flixel::FlxSprite backgroundSprite;
		::flixel::group::FlxGroup aboveGrassLayer;
		::flixel::group::FlxGroup plantLayer;
		::flixel::group::FlxGroup machineLayer;
		::flixel::group::FlxGroup belowBottomSprites;
		::flixel::group::FlxGroup bottomeSprites;
		::Island floatingIsland;
		::TileBoard tileBoard;
		::Tile lastTileClicked;
		::TileBoarder tileBoardBorder;
		Array< ::Dynamic > board;
		Array< ::Dynamic > boarder;
		Array< ::Dynamic > treeArray;
		Array< ::Dynamic > juiceMachineArray;
		Array< ::Dynamic > pieMachineArray;
		Array< ::Dynamic > flyingJuiceBottleArray;
		::NumberPad numPad;
		::ProblemBoard bb;
		int menuVisible;
		int appleCount;
		int buySignCost;
		int applePerTenCost;
		int juicePerTenCost;
		int piePerTenCost;
		int currentTruckLvl;
		Float randomIslandCreationTimer;
		Float loadAppleTimer;
		Float leaveTruckTimer;
		Float appleForJuiceTimer;
		Float appleAndJuiceForPieTimer;
		Float islandCreationTimer;
		::String appleCountString;
		::String answerString;
		bool boardIsDown;
		bool appleDisplayLock;
		::AsteroidBelt asteroidBelt;
		::flixel::FlxSprite bridge;
		::flixel::FlxSprite buySign;
		::flixel::FlxSprite foreGroundRocks;
		::flixel::text::FlxText answerFieldText;
		::flixel::text::FlxText appleCountFieldText;
		::flixel::text::FlxText buySignCostFieldText;
		::Market market;
		::TruckButton truckButton;
		::JuiceMachine juiceMachine;
		::PieMachine pieMachine;
		::AppleShed appleShed;
		::AppleTruck appleTruck;
		::Tree tree;
		::AppleBasket basket;
		::Tree currentTargetTree;
		::JuiceMachine currentTargetJuiceMachine;
		::PieMachine currentTargetPieMachine;
		::JuiceIcon juiceMachineIcon;
		::PieIcon pieMachineIcon;
		::JuiceIcon juiceMachineIconDrag;
		::PieIcon pieMachineIconDrag;
		::JuicePackIcon juicePackIcon;
		::PiePackIcon piePackIcon;
		::Seed seed;
		::Seed seedDrag;
		::Cloud cloud;
		::SunToken sun;
		::TileBoarder currentTileBorder;
		bool overlaysInUse;
		bool sunClicked;
		bool dropletClicked;
		bool ownedTile;
		bool closeTreeGrowthCheck;
		bool closeAppleCountCheck;
		bool marketOpenCheck;
		bool tutorialOn;
		bool truckTutSection;
		bool callBackStopOne;
		bool callBackStopTwo;
		bool tutorialWrongAnswersSkip;
		bool closeSecondTreeGrowthCheck;
		bool tutUnpauseAlertsDone;
		::flixel::system::FlxSound coinsSound;
		::flixel::system::FlxSound alertButtonSound;
		::flixel::system::FlxSound correctAnswerSound;
		::flixel::system::FlxSound incorrectAnswerSound;
		::flixel::system::FlxSound plantingSeedSound;
		virtual Void create( );

		virtual Void initPlaystate( );
		Dynamic initPlaystate_dyn();

		virtual Void openMenu( );
		Dynamic openMenu_dyn();

		virtual Void releaseAllClickEvents( int val);
		Dynamic releaseAllClickEvents_dyn();

		virtual Void initTreeArray( );
		Dynamic initTreeArray_dyn();

		virtual Void plantWeeds( );
		Dynamic plantWeeds_dyn();

		virtual Void createFloatingIsland( );
		Dynamic createFloatingIsland_dyn();

		virtual Void initFlyingJuiceBottleArray( );
		Dynamic initFlyingJuiceBottleArray_dyn();

		virtual Void initJuiceMachineArray( );
		Dynamic initJuiceMachineArray_dyn();

		virtual Void initPieMachineArray( );
		Dynamic initPieMachineArray_dyn();

		virtual Void addFlyingBottleArray( ::JuiceIcon flyingBottle);
		Dynamic addFlyingBottleArray_dyn();

		virtual Void addTreeArray( ::Tree treeObj);
		Dynamic addTreeArray_dyn();

		virtual Void addJuiceMachineArray( ::JuiceMachine machineObj);
		Dynamic addJuiceMachineArray_dyn();

		virtual Void addPieMachineArray( ::PieMachine machineObj);
		Dynamic addPieMachineArray_dyn();

		virtual Void tileBoarderFlip( );
		Dynamic tileBoarderFlip_dyn();

		virtual Void buyButtonPopUp( ::Tile sprite);
		Dynamic buyButtonPopUp_dyn();

		virtual Void displayAppleCount( );
		Dynamic displayAppleCount_dyn();

		virtual Void displayPadNumbers( );
		Dynamic displayPadNumbers_dyn();

		virtual Void placeObject( ::Tile sprite);
		Dynamic placeObject_dyn();

		virtual Void treeClicked( ::flixel::FlxSprite sprite);
		Dynamic treeClicked_dyn();

		virtual Void secondTreeTutMessage( );
		Dynamic secondTreeTutMessage_dyn();

		virtual Void buyLand( ::flixel::FlxSprite sprite);
		Dynamic buyLand_dyn();

		virtual Void callTruck( ::flixel::FlxSprite sprite);
		Dynamic callTruck_dyn();

		virtual Void truckLoadingGoods( );
		Dynamic truckLoadingGoods_dyn();

		virtual Void stopSeedDragIfBoard( );
		Dynamic stopSeedDragIfBoard_dyn();

		virtual Void stopJuiceDragIfBoard( );
		Dynamic stopJuiceDragIfBoard_dyn();

		virtual Void stopPieDragIfBoard( );
		Dynamic stopPieDragIfBoard_dyn();

		virtual Void drawBoard( );
		Dynamic drawBoard_dyn();

		virtual Void letItRain( ::Tree treeObj);
		Dynamic letItRain_dyn();

		virtual Void letItShine( ::Tree treeObj);
		Dynamic letItShine_dyn();

		virtual Void unregisterAllAlertEvents( );
		Dynamic unregisterAllAlertEvents_dyn();

		virtual Void registerAllAlertEvents( );
		Dynamic registerAllAlertEvents_dyn();

		virtual Void checkForAlertClick( );
		Dynamic checkForAlertClick_dyn();

		virtual Void submitAnswer( );
		Dynamic submitAnswer_dyn();

		virtual Void destroyObjects( );
		Dynamic destroyObjects_dyn();

		virtual Void waterBill( );
		Dynamic waterBill_dyn();

		virtual Void electricBill( );
		Dynamic electricBill_dyn();

		virtual Void appleBill( int machinesInUse);
		Dynamic appleBill_dyn();

		virtual Void appleAndJuiceBill( int machinesInUse);
		Dynamic appleAndJuiceBill_dyn();

		virtual Void turnWaterOff( );
		Dynamic turnWaterOff_dyn();

		virtual Void applesAndJuiceForPie( );
		Dynamic applesAndJuiceForPie_dyn();

		virtual Void applesForJuice( );
		Dynamic applesForJuice_dyn();

		virtual Void waterSprinklerBill( );
		Dynamic waterSprinklerBill_dyn();

		virtual Void factoryElectricBill( );
		Dynamic factoryElectricBill_dyn();

		virtual Void removeCallBackOnPad( );
		Dynamic removeCallBackOnPad_dyn();

		virtual Void createAsteroidBelt( );
		Dynamic createAsteroidBelt_dyn();

		virtual Void currentTreeGrowthValue( );
		Dynamic currentTreeGrowthValue_dyn();

		virtual Void marketTutOpened( );
		Dynamic marketTutOpened_dyn();

		virtual Void restartGameOver( );
		Dynamic restartGameOver_dyn();

		virtual Void destroySound( );
		Dynamic destroySound_dyn();

		virtual Void update( );

		virtual Void destroy( );

};


#endif /* INCLUDED_PlayState */ 
