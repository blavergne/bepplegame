#include <hxcpp.h>

#ifndef INCLUDED_JuicePackIcon
#include <JuicePackIcon.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif

Void JuicePackIcon_obj::__construct()
{
HX_STACK_FRAME("JuicePackIcon","new",0xef12e044,"JuicePackIcon.new","JuicePackIcon.hx",18,0xae6ad3ac)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(19)
	super::__construct(null());
	HX_STACK_LINE(20)
	::flixel::group::FlxGroup _g = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(20)
	this->juicePackGroup = _g;
	HX_STACK_LINE(21)
	::flixel::FlxSprite _g1 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
	HX_STACK_LINE(21)
	this->juicePackSprite = _g1;
	HX_STACK_LINE(22)
	this->juicePackSprite->loadGraphic(HX_CSTRING("assets/images/packOfJuiceIcon.png"),false,null(),null(),null(),null());
	HX_STACK_LINE(23)
	this->juicePackSprite->set_x((int)150);
	HX_STACK_LINE(24)
	this->juicePackSprite->set_y((int)36);
	HX_STACK_LINE(25)
	::JuicePackIcon_obj::JuicePackPosX = this->juicePackSprite->x;
	HX_STACK_LINE(26)
	::JuicePackIcon_obj::JuicePackPosY = this->juicePackSprite->y;
	HX_STACK_LINE(27)
	this->juicePackGroup->add(this->juicePackSprite);
	HX_STACK_LINE(28)
	this->add(this->juicePackGroup);
}
;
	return null();
}

//JuicePackIcon_obj::~JuicePackIcon_obj() { }

Dynamic JuicePackIcon_obj::__CreateEmpty() { return  new JuicePackIcon_obj; }
hx::ObjectPtr< JuicePackIcon_obj > JuicePackIcon_obj::__new()
{  hx::ObjectPtr< JuicePackIcon_obj > result = new JuicePackIcon_obj();
	result->__construct();
	return result;}

Dynamic JuicePackIcon_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< JuicePackIcon_obj > result = new JuicePackIcon_obj();
	result->__construct();
	return result;}

::flixel::FlxSprite JuicePackIcon_obj::getJuicePackSprite( ){
	HX_STACK_FRAME("JuicePackIcon","getJuicePackSprite",0x7339d3e4,"JuicePackIcon.getJuicePackSprite","JuicePackIcon.hx",32,0xae6ad3ac)
	HX_STACK_THIS(this)
	HX_STACK_LINE(32)
	return this->juicePackSprite;
}


HX_DEFINE_DYNAMIC_FUNC0(JuicePackIcon_obj,getJuicePackSprite,return )

Float JuicePackIcon_obj::JuicePackPosX;

Float JuicePackIcon_obj::JuicePackPosY;


JuicePackIcon_obj::JuicePackIcon_obj()
{
}

void JuicePackIcon_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(JuicePackIcon);
	HX_MARK_MEMBER_NAME(juicePackGroup,"juicePackGroup");
	HX_MARK_MEMBER_NAME(juicePackSprite,"juicePackSprite");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void JuicePackIcon_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(juicePackGroup,"juicePackGroup");
	HX_VISIT_MEMBER_NAME(juicePackSprite,"juicePackSprite");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic JuicePackIcon_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 13:
		if (HX_FIELD_EQ(inName,"JuicePackPosX") ) { return JuicePackPosX; }
		if (HX_FIELD_EQ(inName,"JuicePackPosY") ) { return JuicePackPosY; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"juicePackGroup") ) { return juicePackGroup; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"juicePackSprite") ) { return juicePackSprite; }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"getJuicePackSprite") ) { return getJuicePackSprite_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic JuicePackIcon_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 13:
		if (HX_FIELD_EQ(inName,"JuicePackPosX") ) { JuicePackPosX=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"JuicePackPosY") ) { JuicePackPosY=inValue.Cast< Float >(); return inValue; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"juicePackGroup") ) { juicePackGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"juicePackSprite") ) { juicePackSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void JuicePackIcon_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("juicePackGroup"));
	outFields->push(HX_CSTRING("juicePackSprite"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("JuicePackPosX"),
	HX_CSTRING("JuicePackPosY"),
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(JuicePackIcon_obj,juicePackGroup),HX_CSTRING("juicePackGroup")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(JuicePackIcon_obj,juicePackSprite),HX_CSTRING("juicePackSprite")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("juicePackGroup"),
	HX_CSTRING("juicePackSprite"),
	HX_CSTRING("getJuicePackSprite"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(JuicePackIcon_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(JuicePackIcon_obj::JuicePackPosX,"JuicePackPosX");
	HX_MARK_MEMBER_NAME(JuicePackIcon_obj::JuicePackPosY,"JuicePackPosY");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(JuicePackIcon_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(JuicePackIcon_obj::JuicePackPosX,"JuicePackPosX");
	HX_VISIT_MEMBER_NAME(JuicePackIcon_obj::JuicePackPosY,"JuicePackPosY");
};

#endif

Class JuicePackIcon_obj::__mClass;

void JuicePackIcon_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("JuicePackIcon"), hx::TCanCast< JuicePackIcon_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void JuicePackIcon_obj::__boot()
{
}

