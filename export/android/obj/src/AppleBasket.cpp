#include <hxcpp.h>

#ifndef INCLUDED_Apple
#include <Apple.h>
#endif
#ifndef INCLUDED_AppleBasket
#include <AppleBasket.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif

Void AppleBasket_obj::__construct()
{
HX_STACK_FRAME("AppleBasket","new",0xf7817612,"AppleBasket.new","AppleBasket.hx",13,0xea26719e)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(22)
	this->appleFlying = false;
	HX_STACK_LINE(18)
	this->basketFull = (int)20;
	HX_STACK_LINE(27)
	super::__construct(null());
	HX_STACK_LINE(28)
	::flixel::group::FlxGroup _g = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(28)
	this->basketGroup = _g;
	HX_STACK_LINE(29)
	::flixel::FlxSprite _g1 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
	HX_STACK_LINE(29)
	this->basket = _g1;
	HX_STACK_LINE(30)
	Array< ::Dynamic > _g2 = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(_g2,"_g2");
	HX_STACK_LINE(30)
	this->flyingAppleArray = _g2;
	HX_STACK_LINE(31)
	this->basket->set_x((int)90);
	HX_STACK_LINE(32)
	this->basket->set_y((int)25);
	HX_STACK_LINE(33)
	this->initAppleArray();
	HX_STACK_LINE(34)
	this->basket->loadGraphic(HX_CSTRING("assets/images/basketAnimation.png"),true,(int)36,(int)36,null(),null());
	HX_STACK_LINE(35)
	this->basket->animation->add(HX_CSTRING("appleA"),Array_obj< int >::__new().Add((int)0),(int)1,false);
	HX_STACK_LINE(36)
	this->basket->animation->add(HX_CSTRING("appleB"),Array_obj< int >::__new().Add((int)1),(int)1,false);
	HX_STACK_LINE(37)
	this->basket->animation->add(HX_CSTRING("appleC"),Array_obj< int >::__new().Add((int)2),(int)1,false);
	HX_STACK_LINE(38)
	this->basket->animation->add(HX_CSTRING("appleD"),Array_obj< int >::__new().Add((int)3),(int)1,false);
	HX_STACK_LINE(39)
	this->basket->animation->play(HX_CSTRING("appleD"),null(),null());
	HX_STACK_LINE(40)
	::AppleBasket_obj::basketPosX = this->basket->x;
	HX_STACK_LINE(41)
	::AppleBasket_obj::basketPosY = this->basket->y;
	HX_STACK_LINE(42)
	this->basketGroup->add(this->basket);
	HX_STACK_LINE(43)
	this->add(this->basketGroup);
}
;
	return null();
}

//AppleBasket_obj::~AppleBasket_obj() { }

Dynamic AppleBasket_obj::__CreateEmpty() { return  new AppleBasket_obj; }
hx::ObjectPtr< AppleBasket_obj > AppleBasket_obj::__new()
{  hx::ObjectPtr< AppleBasket_obj > result = new AppleBasket_obj();
	result->__construct();
	return result;}

Dynamic AppleBasket_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< AppleBasket_obj > result = new AppleBasket_obj();
	result->__construct();
	return result;}

::flixel::FlxSprite AppleBasket_obj::getBasketSprite( ){
	HX_STACK_FRAME("AppleBasket","getBasketSprite",0x27073b53,"AppleBasket.getBasketSprite","AppleBasket.hx",48,0xea26719e)
	HX_STACK_THIS(this)
	HX_STACK_LINE(48)
	return this->basket;
}


HX_DEFINE_DYNAMIC_FUNC0(AppleBasket_obj,getBasketSprite,return )

int AppleBasket_obj::getBasketFullVal( ){
	HX_STACK_FRAME("AppleBasket","getBasketFullVal",0x6ffbd584,"AppleBasket.getBasketFullVal","AppleBasket.hx",52,0xea26719e)
	HX_STACK_THIS(this)
	HX_STACK_LINE(52)
	return this->basketFull;
}


HX_DEFINE_DYNAMIC_FUNC0(AppleBasket_obj,getBasketFullVal,return )

bool AppleBasket_obj::getAppleFlying( ){
	HX_STACK_FRAME("AppleBasket","getAppleFlying",0x9298d1e1,"AppleBasket.getAppleFlying","AppleBasket.hx",56,0xea26719e)
	HX_STACK_THIS(this)
	HX_STACK_LINE(56)
	return this->appleFlying;
}


HX_DEFINE_DYNAMIC_FUNC0(AppleBasket_obj,getAppleFlying,return )

::Apple AppleBasket_obj::getAppleSpriteFromBasket( ){
	HX_STACK_FRAME("AppleBasket","getAppleSpriteFromBasket",0xd0291bc7,"AppleBasket.getAppleSpriteFromBasket","AppleBasket.hx",60,0xea26719e)
	HX_STACK_THIS(this)
	HX_STACK_LINE(60)
	return this->flyingAppleArray->__get((int)0).StaticCast< ::Apple >();
}


HX_DEFINE_DYNAMIC_FUNC0(AppleBasket_obj,getAppleSpriteFromBasket,return )

Void AppleBasket_obj::setAppleFlying( bool val){
{
		HX_STACK_FRAME("AppleBasket","setAppleFlying",0xb2b8ba55,"AppleBasket.setAppleFlying","AppleBasket.hx",65,0xea26719e)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(65)
		this->appleFlying = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(AppleBasket_obj,setAppleFlying,(void))

Void AppleBasket_obj::initAppleArray( ){
{
		HX_STACK_FRAME("AppleBasket","initAppleArray",0xbd3d189d,"AppleBasket.initAppleArray","AppleBasket.hx",69,0xea26719e)
		HX_STACK_THIS(this)
		HX_STACK_LINE(69)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(69)
		while((true)){
			HX_STACK_LINE(69)
			if ((!(((_g < (int)9))))){
				HX_STACK_LINE(69)
				break;
			}
			HX_STACK_LINE(69)
			int i = (_g)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(70)
			this->flyingAppleArray[i] = ::Apple_obj::__new();
			HX_STACK_LINE(71)
			this->flyingAppleArray->__get(i).StaticCast< ::Apple >()->getAppleSprite()->set_alpha((int)1);
			HX_STACK_LINE(72)
			this->flyingAppleArray->__get(i).StaticCast< ::Apple >()->setAppleGrowth((int)2);
			HX_STACK_LINE(73)
			this->flyingAppleArray->__get(i).StaticCast< ::Apple >()->growApple();
			HX_STACK_LINE(74)
			this->flyingAppleArray->__get(i).StaticCast< ::Apple >()->getAppleSprite()->set_x((this->basket->x + (int)11));
			HX_STACK_LINE(75)
			this->flyingAppleArray->__get(i).StaticCast< ::Apple >()->getAppleSprite()->set_y((this->basket->y + (int)11));
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(AppleBasket_obj,initAppleArray,(void))

Void AppleBasket_obj::animateBasket( int appleCount){
{
		HX_STACK_FRAME("AppleBasket","animateBasket",0xba61a9f9,"AppleBasket.animateBasket","AppleBasket.hx",79,0xea26719e)
		HX_STACK_THIS(this)
		HX_STACK_ARG(appleCount,"appleCount")
		HX_STACK_LINE(80)
		int _g = ::Math_obj::floor((this->basketFull * .3));		HX_STACK_VAR(_g,"_g");
		struct _Function_1_1{
			inline static bool Block( int &appleCount,hx::ObjectPtr< ::AppleBasket_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","AppleBasket.hx",80,0xea26719e)
				{
					HX_STACK_LINE(80)
					int _g1 = ::Math_obj::floor((__this->basketFull * .7));		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(80)
					return (appleCount < _g1);
				}
				return null();
			}
		};
		HX_STACK_LINE(80)
		if (((  (((appleCount >= _g))) ? bool(_Function_1_1::Block(appleCount,this)) : bool(false) ))){
			HX_STACK_LINE(81)
			this->basket->animation->play(HX_CSTRING("appleB"),null(),null());
		}
		HX_STACK_LINE(83)
		int _g2 = ::Math_obj::floor((this->basketFull * .7));		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(83)
		if (((  (((appleCount >= _g2))) ? bool((appleCount < this->basketFull)) : bool(false) ))){
			HX_STACK_LINE(84)
			this->basket->animation->play(HX_CSTRING("appleC"),null(),null());
		}
		HX_STACK_LINE(86)
		if (((appleCount >= this->basketFull))){
			HX_STACK_LINE(87)
			this->basket->animation->play(HX_CSTRING("appleD"),null(),null());
		}
		HX_STACK_LINE(89)
		int _g3 = ::Math_obj::floor((this->basketFull * .3));		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(89)
		if (((appleCount < _g3))){
			HX_STACK_LINE(90)
			this->basket->animation->play(HX_CSTRING("appleA"),null(),null());
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(AppleBasket_obj,animateBasket,(void))

Void AppleBasket_obj::update( ){
{
		HX_STACK_FRAME("AppleBasket","update",0x0fd4fb97,"AppleBasket.update","AppleBasket.hx",96,0xea26719e)
		HX_STACK_THIS(this)
		HX_STACK_LINE(96)
		this->super::update();
	}
return null();
}


Float AppleBasket_obj::basketPosX;

Float AppleBasket_obj::basketPosY;


AppleBasket_obj::AppleBasket_obj()
{
}

void AppleBasket_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(AppleBasket);
	HX_MARK_MEMBER_NAME(basketGroup,"basketGroup");
	HX_MARK_MEMBER_NAME(basket,"basket");
	HX_MARK_MEMBER_NAME(basketFull,"basketFull");
	HX_MARK_MEMBER_NAME(flyingAppleArray,"flyingAppleArray");
	HX_MARK_MEMBER_NAME(appleFlying,"appleFlying");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void AppleBasket_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(basketGroup,"basketGroup");
	HX_VISIT_MEMBER_NAME(basket,"basket");
	HX_VISIT_MEMBER_NAME(basketFull,"basketFull");
	HX_VISIT_MEMBER_NAME(flyingAppleArray,"flyingAppleArray");
	HX_VISIT_MEMBER_NAME(appleFlying,"appleFlying");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic AppleBasket_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"basket") ) { return basket; }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"basketPosX") ) { return basketPosX; }
		if (HX_FIELD_EQ(inName,"basketPosY") ) { return basketPosY; }
		if (HX_FIELD_EQ(inName,"basketFull") ) { return basketFull; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"basketGroup") ) { return basketGroup; }
		if (HX_FIELD_EQ(inName,"appleFlying") ) { return appleFlying; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"animateBasket") ) { return animateBasket_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"getAppleFlying") ) { return getAppleFlying_dyn(); }
		if (HX_FIELD_EQ(inName,"setAppleFlying") ) { return setAppleFlying_dyn(); }
		if (HX_FIELD_EQ(inName,"initAppleArray") ) { return initAppleArray_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"getBasketSprite") ) { return getBasketSprite_dyn(); }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"flyingAppleArray") ) { return flyingAppleArray; }
		if (HX_FIELD_EQ(inName,"getBasketFullVal") ) { return getBasketFullVal_dyn(); }
		break;
	case 24:
		if (HX_FIELD_EQ(inName,"getAppleSpriteFromBasket") ) { return getAppleSpriteFromBasket_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic AppleBasket_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"basket") ) { basket=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"basketPosX") ) { basketPosX=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"basketPosY") ) { basketPosY=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"basketFull") ) { basketFull=inValue.Cast< int >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"basketGroup") ) { basketGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"appleFlying") ) { appleFlying=inValue.Cast< bool >(); return inValue; }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"flyingAppleArray") ) { flyingAppleArray=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void AppleBasket_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("basketGroup"));
	outFields->push(HX_CSTRING("basket"));
	outFields->push(HX_CSTRING("basketFull"));
	outFields->push(HX_CSTRING("flyingAppleArray"));
	outFields->push(HX_CSTRING("appleFlying"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("basketPosX"),
	HX_CSTRING("basketPosY"),
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(AppleBasket_obj,basketGroup),HX_CSTRING("basketGroup")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(AppleBasket_obj,basket),HX_CSTRING("basket")},
	{hx::fsInt,(int)offsetof(AppleBasket_obj,basketFull),HX_CSTRING("basketFull")},
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(AppleBasket_obj,flyingAppleArray),HX_CSTRING("flyingAppleArray")},
	{hx::fsBool,(int)offsetof(AppleBasket_obj,appleFlying),HX_CSTRING("appleFlying")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("basketGroup"),
	HX_CSTRING("basket"),
	HX_CSTRING("basketFull"),
	HX_CSTRING("flyingAppleArray"),
	HX_CSTRING("appleFlying"),
	HX_CSTRING("getBasketSprite"),
	HX_CSTRING("getBasketFullVal"),
	HX_CSTRING("getAppleFlying"),
	HX_CSTRING("getAppleSpriteFromBasket"),
	HX_CSTRING("setAppleFlying"),
	HX_CSTRING("initAppleArray"),
	HX_CSTRING("animateBasket"),
	HX_CSTRING("update"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(AppleBasket_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(AppleBasket_obj::basketPosX,"basketPosX");
	HX_MARK_MEMBER_NAME(AppleBasket_obj::basketPosY,"basketPosY");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(AppleBasket_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(AppleBasket_obj::basketPosX,"basketPosX");
	HX_VISIT_MEMBER_NAME(AppleBasket_obj::basketPosY,"basketPosY");
};

#endif

Class AppleBasket_obj::__mClass;

void AppleBasket_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("AppleBasket"), hx::TCanCast< AppleBasket_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void AppleBasket_obj::__boot()
{
}

