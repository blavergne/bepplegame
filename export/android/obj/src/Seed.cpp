#include <hxcpp.h>

#ifndef INCLUDED_Seed
#include <Seed.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_input_mouse_FlxMouse
#include <flixel/input/mouse/FlxMouse.h>
#endif
#ifndef INCLUDED_flixel_input_mouse_FlxMouseButton
#include <flixel/input/mouse/FlxMouseButton.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxInput
#include <flixel/interfaces/IFlxInput.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxPooled
#include <flixel/interfaces/IFlxPooled.h>
#endif
#ifndef INCLUDED_flixel_plugin_FlxPlugin
#include <flixel/plugin/FlxPlugin.h>
#endif
#ifndef INCLUDED_flixel_plugin_MouseEventManager
#include <flixel/plugin/MouseEventManager.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPoint
#include <flixel/util/FlxPoint.h>
#endif

Void Seed_obj::__construct(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y,bool dragType)
{
HX_STACK_FRAME("Seed","new",0x082bef03,"Seed.new","Seed.hx",11,0x473621cd)
HX_STACK_THIS(this)
HX_STACK_ARG(__o_X,"X")
HX_STACK_ARG(__o_Y,"Y")
HX_STACK_ARG(dragType,"dragType")
Float X = __o_X.Default(0);
Float Y = __o_Y.Default(0);
{
	HX_STACK_LINE(18)
	this->homeY = (int)70;
	HX_STACK_LINE(17)
	this->homeX = (int)-26;
	HX_STACK_LINE(16)
	this->isMousedOver = true;
	HX_STACK_LINE(14)
	this->dragging = false;
	HX_STACK_LINE(22)
	super::__construct(X,Y,null());
	HX_STACK_LINE(23)
	this->dragType = dragType;
	HX_STACK_LINE(24)
	this->loadGraphic(HX_CSTRING("assets/images/tree_seed.png"),false,(int)18,(int)21,null(),null());
	HX_STACK_LINE(25)
	this->set_x(this->homeX);
	HX_STACK_LINE(26)
	this->set_y(this->homeY);
	HX_STACK_LINE(27)
	if ((dragType)){
		HX_STACK_LINE(28)
		this->homeX = (int)139;
		HX_STACK_LINE(29)
		this->homeY = (int)70;
	}
	HX_STACK_LINE(31)
	::flixel::plugin::MouseEventManager_obj::add(hx::ObjectPtr<OBJ_>(this),this->onDown_dyn(),this->onUp_dyn(),null(),null(),null(),null(),null());
}
;
	return null();
}

//Seed_obj::~Seed_obj() { }

Dynamic Seed_obj::__CreateEmpty() { return  new Seed_obj; }
hx::ObjectPtr< Seed_obj > Seed_obj::__new(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y,bool dragType)
{  hx::ObjectPtr< Seed_obj > result = new Seed_obj();
	result->__construct(__o_X,__o_Y,dragType);
	return result;}

Dynamic Seed_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Seed_obj > result = new Seed_obj();
	result->__construct(inArgs[0],inArgs[1],inArgs[2]);
	return result;}

Void Seed_obj::onDown( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("Seed","onDown",0x94446a5e,"Seed.onDown","Seed.hx",37,0x473621cd)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(37)
		this->dragging = true;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Seed_obj,onDown,(void))

Void Seed_obj::onUp( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("Seed","onUp",0x1ef52157,"Seed.onUp","Seed.hx",41,0x473621cd)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(42)
		this->dragging = false;
		HX_STACK_LINE(43)
		this->resetSeedPos();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Seed_obj,onUp,(void))

Void Seed_obj::dragSeed( bool temp){
{
		HX_STACK_FRAME("Seed","dragSeed",0x9477da02,"Seed.dragSeed","Seed.hx",49,0x473621cd)
		HX_STACK_THIS(this)
		HX_STACK_ARG(temp,"temp")
		HX_STACK_LINE(49)
		if ((temp)){
			HX_STACK_LINE(51)
			this->set_x((::flixel::FlxG_obj::mouse->x - (int)10));
			HX_STACK_LINE(52)
			this->set_y((::flixel::FlxG_obj::mouse->y - (int)10));
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Seed_obj,dragSeed,(void))

Void Seed_obj::resetSeedPos( ){
{
		HX_STACK_FRAME("Seed","resetSeedPos",0x527b8791,"Seed.resetSeedPos","Seed.hx",59,0x473621cd)
		HX_STACK_THIS(this)
		HX_STACK_LINE(59)
		if ((this->dragType)){
			HX_STACK_LINE(60)
			this->set_x(this->homeX);
			HX_STACK_LINE(61)
			this->set_y(this->homeY);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Seed_obj,resetSeedPos,(void))

bool Seed_obj::getDragging( ){
	HX_STACK_FRAME("Seed","getDragging",0x975bf928,"Seed.getDragging","Seed.hx",67,0x473621cd)
	HX_STACK_THIS(this)
	HX_STACK_LINE(67)
	return this->dragging;
}


HX_DEFINE_DYNAMIC_FUNC0(Seed_obj,getDragging,return )

Void Seed_obj::setDragging( bool val){
{
		HX_STACK_FRAME("Seed","setDragging",0xa1c90034,"Seed.setDragging","Seed.hx",73,0x473621cd)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(73)
		this->dragging = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Seed_obj,setDragging,(void))

Void Seed_obj::checkMouseState( ){
{
		HX_STACK_FRAME("Seed","checkMouseState",0x7f4ed7b7,"Seed.checkMouseState","Seed.hx",79,0x473621cd)
		HX_STACK_THIS(this)
		struct _Function_1_1{
			inline static bool Block( ){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","Seed.hx",79,0x473621cd)
				{
					HX_STACK_LINE(79)
					::flixel::input::mouse::FlxMouseButton _this = ::flixel::FlxG_obj::mouse->_leftButton;		HX_STACK_VAR(_this,"_this");
					HX_STACK_LINE(79)
					return (bool((_this->current == (int)-1)) || bool((_this->current == (int)-2)));
				}
				return null();
			}
		};
		HX_STACK_LINE(79)
		if ((_Function_1_1::Block())){
			HX_STACK_LINE(80)
			this->dragging = false;
			HX_STACK_LINE(81)
			this->resetSeedPos();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Seed_obj,checkMouseState,(void))

Void Seed_obj::unregisterSeedEvent( ){
{
		HX_STACK_FRAME("Seed","unregisterSeedEvent",0x0d6be830,"Seed.unregisterSeedEvent","Seed.hx",87,0x473621cd)
		HX_STACK_THIS(this)
		HX_STACK_LINE(87)
		::flixel::plugin::MouseEventManager_obj::remove(hx::ObjectPtr<OBJ_>(this));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Seed_obj,unregisterSeedEvent,(void))

Void Seed_obj::registerSeedEvent( ){
{
		HX_STACK_FRAME("Seed","registerSeedEvent",0x169583e9,"Seed.registerSeedEvent","Seed.hx",91,0x473621cd)
		HX_STACK_THIS(this)
		HX_STACK_LINE(91)
		::flixel::plugin::MouseEventManager_obj::add(hx::ObjectPtr<OBJ_>(this),this->onDown_dyn(),this->onUp_dyn(),null(),null(),null(),null(),null());
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Seed_obj,registerSeedEvent,(void))

Void Seed_obj::update( ){
{
		HX_STACK_FRAME("Seed","update",0x3651ef46,"Seed.update","Seed.hx",94,0x473621cd)
		HX_STACK_THIS(this)
		HX_STACK_LINE(96)
		this->checkMouseState();
		HX_STACK_LINE(97)
		this->dragSeed(this->dragging);
		HX_STACK_LINE(98)
		this->super::update();
	}
return null();
}


Void Seed_obj::destroy( ){
{
		HX_STACK_FRAME("Seed","destroy",0xd815d91d,"Seed.destroy","Seed.hx",102,0x473621cd)
		HX_STACK_THIS(this)
		HX_STACK_LINE(104)
		::flixel::plugin::MouseEventManager_obj::remove(hx::ObjectPtr<OBJ_>(this));
		HX_STACK_LINE(105)
		this->super::destroy();
	}
return null();
}



Seed_obj::Seed_obj()
{
}

Dynamic Seed_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"onUp") ) { return onUp_dyn(); }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"homeX") ) { return homeX; }
		if (HX_FIELD_EQ(inName,"homeY") ) { return homeY; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"onDown") ) { return onDown_dyn(); }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"dragging") ) { return dragging; }
		if (HX_FIELD_EQ(inName,"dragType") ) { return dragType; }
		if (HX_FIELD_EQ(inName,"dragSeed") ) { return dragSeed_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"getDragging") ) { return getDragging_dyn(); }
		if (HX_FIELD_EQ(inName,"setDragging") ) { return setDragging_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"isMousedOver") ) { return isMousedOver; }
		if (HX_FIELD_EQ(inName,"resetSeedPos") ) { return resetSeedPos_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"checkMouseState") ) { return checkMouseState_dyn(); }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"registerSeedEvent") ) { return registerSeedEvent_dyn(); }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"unregisterSeedEvent") ) { return unregisterSeedEvent_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Seed_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"homeX") ) { homeX=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"homeY") ) { homeY=inValue.Cast< int >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"dragging") ) { dragging=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"dragType") ) { dragType=inValue.Cast< bool >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"isMousedOver") ) { isMousedOver=inValue.Cast< bool >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Seed_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("dragging"));
	outFields->push(HX_CSTRING("dragType"));
	outFields->push(HX_CSTRING("isMousedOver"));
	outFields->push(HX_CSTRING("homeX"));
	outFields->push(HX_CSTRING("homeY"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsBool,(int)offsetof(Seed_obj,dragging),HX_CSTRING("dragging")},
	{hx::fsBool,(int)offsetof(Seed_obj,dragType),HX_CSTRING("dragType")},
	{hx::fsBool,(int)offsetof(Seed_obj,isMousedOver),HX_CSTRING("isMousedOver")},
	{hx::fsInt,(int)offsetof(Seed_obj,homeX),HX_CSTRING("homeX")},
	{hx::fsInt,(int)offsetof(Seed_obj,homeY),HX_CSTRING("homeY")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("dragging"),
	HX_CSTRING("dragType"),
	HX_CSTRING("isMousedOver"),
	HX_CSTRING("homeX"),
	HX_CSTRING("homeY"),
	HX_CSTRING("onDown"),
	HX_CSTRING("onUp"),
	HX_CSTRING("dragSeed"),
	HX_CSTRING("resetSeedPos"),
	HX_CSTRING("getDragging"),
	HX_CSTRING("setDragging"),
	HX_CSTRING("checkMouseState"),
	HX_CSTRING("unregisterSeedEvent"),
	HX_CSTRING("registerSeedEvent"),
	HX_CSTRING("update"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Seed_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Seed_obj::__mClass,"__mClass");
};

#endif

Class Seed_obj::__mClass;

void Seed_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("Seed"), hx::TCanCast< Seed_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void Seed_obj::__boot()
{
}

