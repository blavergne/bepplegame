#include <hxcpp.h>

#ifndef INCLUDED_KoiItem
#include <KoiItem.h>
#endif
#ifndef INCLUDED_MarketItem
#include <MarketItem.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_haxe_Log
#include <haxe/Log.h>
#endif

Void KoiItem_obj::__construct()
{
HX_STACK_FRAME("KoiItem","new",0x11081eca,"KoiItem.new","KoiItem.hx",13,0xeedb47e6)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(14)
	super::__construct();
	HX_STACK_LINE(15)
	this->initMarketItems();
}
;
	return null();
}

//KoiItem_obj::~KoiItem_obj() { }

Dynamic KoiItem_obj::__CreateEmpty() { return  new KoiItem_obj; }
hx::ObjectPtr< KoiItem_obj > KoiItem_obj::__new()
{  hx::ObjectPtr< KoiItem_obj > result = new KoiItem_obj();
	result->__construct();
	return result;}

Dynamic KoiItem_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< KoiItem_obj > result = new KoiItem_obj();
	result->__construct();
	return result;}

Void KoiItem_obj::initMarketItems( ){
{
		HX_STACK_FRAME("KoiItem","initMarketItems",0x9450b8de,"KoiItem.initMarketItems","KoiItem.hx",18,0xeedb47e6)
		HX_STACK_THIS(this)
		HX_STACK_LINE(19)
		::flixel::FlxSprite _g = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(19)
		this->itemSprite = _g;
		HX_STACK_LINE(20)
		this->itemCost = (int)35;
		HX_STACK_LINE(21)
		this->itemName = HX_CSTRING("koiItem");
		HX_STACK_LINE(22)
		this->itemCount = (int)1;
		HX_STACK_LINE(23)
		this->itemSprite->loadGraphic(HX_CSTRING("assets/images/fishItemAnimation.png"),true,(int)30,(int)30,false,HX_CSTRING("koiItem"));
		HX_STACK_LINE(24)
		this->itemSprite->animation->add(HX_CSTRING("available"),Array_obj< int >::__new().Add((int)0),(int)1,false);
		HX_STACK_LINE(25)
		this->itemSprite->animation->add(HX_CSTRING("notAvailable"),Array_obj< int >::__new().Add((int)1),(int)1,false);
		HX_STACK_LINE(26)
		this->setItemAvailable(true);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(KoiItem_obj,initMarketItems,(void))

Void KoiItem_obj::setFishItemCount( int val){
{
		HX_STACK_FRAME("KoiItem","setFishItemCount",0x6adc7158,"KoiItem.setFishItemCount","KoiItem.hx",30,0xeedb47e6)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(30)
		this->itemCount = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(KoiItem_obj,setFishItemCount,(void))

Void KoiItem_obj::update( ){
{
		HX_STACK_FRAME("KoiItem","update",0xb38695df,"KoiItem.update","KoiItem.hx",33,0xeedb47e6)
		HX_STACK_THIS(this)
		HX_STACK_LINE(34)
		::haxe::Log_obj::trace(this->itemCount,hx::SourceInfo(HX_CSTRING("KoiItem.hx"),34,HX_CSTRING("KoiItem"),HX_CSTRING("update")));
		HX_STACK_LINE(35)
		this->setAvailableAnimation();
		HX_STACK_LINE(36)
		this->super::update();
	}
return null();
}



KoiItem_obj::KoiItem_obj()
{
}

Dynamic KoiItem_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"initMarketItems") ) { return initMarketItems_dyn(); }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"setFishItemCount") ) { return setFishItemCount_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic KoiItem_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	return super::__SetField(inName,inValue,inCallProp);
}

void KoiItem_obj::__GetFields(Array< ::String> &outFields)
{
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo *sMemberStorageInfo = 0;
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("initMarketItems"),
	HX_CSTRING("setFishItemCount"),
	HX_CSTRING("update"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(KoiItem_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(KoiItem_obj::__mClass,"__mClass");
};

#endif

Class KoiItem_obj::__mClass;

void KoiItem_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("KoiItem"), hx::TCanCast< KoiItem_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void KoiItem_obj::__boot()
{
}

