#include <hxcpp.h>

#ifndef INCLUDED_AppleTruck
#include <AppleTruck.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_system_FlxSound
#include <flixel/system/FlxSound.h>
#endif
#ifndef INCLUDED_flixel_system_frontEnds_SoundFrontEnd
#include <flixel/system/frontEnds/SoundFrontEnd.h>
#endif
#ifndef INCLUDED_flixel_util_FlxDestroyUtil
#include <flixel/util/FlxDestroyUtil.h>
#endif
#ifndef INCLUDED_openfl__v2_events_EventDispatcher
#include <openfl/_v2/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__v2_events_IEventDispatcher
#include <openfl/_v2/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__v2_media_SoundChannel
#include <openfl/_v2/media/SoundChannel.h>
#endif

Void AppleTruck_obj::__construct()
{
HX_STACK_FRAME("AppleTruck","new",0xf21c8b77,"AppleTruck.new","AppleTruck.hx",14,0x8bfdcfd9)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(25)
	this->truckSoundReady = true;
	HX_STACK_LINE(24)
	this->truckIsReset = false;
	HX_STACK_LINE(23)
	this->loadingApples = false;
	HX_STACK_LINE(22)
	this->pickUpReady = false;
	HX_STACK_LINE(19)
	this->truckTravelTime = (int)5;
	HX_STACK_LINE(30)
	super::__construct(null());
	HX_STACK_LINE(31)
	::flixel::FlxSprite _g = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(31)
	this->appleTruckSprite = _g;
	HX_STACK_LINE(32)
	::flixel::group::FlxGroup _g1 = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g1,"_g1");
	HX_STACK_LINE(32)
	this->appleTruckGroup = _g1;
	HX_STACK_LINE(33)
	this->appleTruckSprite->loadGraphic(HX_CSTRING("assets/images/appleTruck.png"),false,null(),null(),null(),null());
	HX_STACK_LINE(34)
	this->appleTruckSprite->set_x((int)-55);
	HX_STACK_LINE(35)
	this->appleTruckSprite->set_y((int)305);
	HX_STACK_LINE(36)
	this->setTruckSize((int)1);
	HX_STACK_LINE(37)
	this->appleTruckGroup->add(this->appleTruckSprite);
	HX_STACK_LINE(38)
	this->add(this->appleTruckGroup);
}
;
	return null();
}

//AppleTruck_obj::~AppleTruck_obj() { }

Dynamic AppleTruck_obj::__CreateEmpty() { return  new AppleTruck_obj; }
hx::ObjectPtr< AppleTruck_obj > AppleTruck_obj::__new()
{  hx::ObjectPtr< AppleTruck_obj > result = new AppleTruck_obj();
	result->__construct();
	return result;}

Dynamic AppleTruck_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< AppleTruck_obj > result = new AppleTruck_obj();
	result->__construct();
	return result;}

bool AppleTruck_obj::getLoadingApples( ){
	HX_STACK_FRAME("AppleTruck","getLoadingApples",0x8da5fd88,"AppleTruck.getLoadingApples","AppleTruck.hx",42,0x8bfdcfd9)
	HX_STACK_THIS(this)
	HX_STACK_LINE(42)
	return this->loadingApples;
}


HX_DEFINE_DYNAMIC_FUNC0(AppleTruck_obj,getLoadingApples,return )

int AppleTruck_obj::getAppleTruckCapacity( ){
	HX_STACK_FRAME("AppleTruck","getAppleTruckCapacity",0xaa2e794c,"AppleTruck.getAppleTruckCapacity","AppleTruck.hx",46,0x8bfdcfd9)
	HX_STACK_THIS(this)
	HX_STACK_LINE(46)
	return this->truckCapacity;
}


HX_DEFINE_DYNAMIC_FUNC0(AppleTruck_obj,getAppleTruckCapacity,return )

bool AppleTruck_obj::getTruckIsReset( ){
	HX_STACK_FRAME("AppleTruck","getTruckIsReset",0x5c8d72f3,"AppleTruck.getTruckIsReset","AppleTruck.hx",50,0x8bfdcfd9)
	HX_STACK_THIS(this)
	HX_STACK_LINE(50)
	return this->truckIsReset;
}


HX_DEFINE_DYNAMIC_FUNC0(AppleTruck_obj,getTruckIsReset,return )

Void AppleTruck_obj::decTruckCapacity( ){
{
		HX_STACK_FRAME("AppleTruck","decTruckCapacity",0xbefd3760,"AppleTruck.decTruckCapacity","AppleTruck.hx",54,0x8bfdcfd9)
		HX_STACK_THIS(this)
		HX_STACK_LINE(54)
		hx::SubEq(this->truckCapacity,(int)10);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(AppleTruck_obj,decTruckCapacity,(void))

Void AppleTruck_obj::setTruckCapacity( int val){
{
		HX_STACK_FRAME("AppleTruck","setTruckCapacity",0x58522980,"AppleTruck.setTruckCapacity","AppleTruck.hx",58,0x8bfdcfd9)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(58)
		this->truckCapacity = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(AppleTruck_obj,setTruckCapacity,(void))

Void AppleTruck_obj::setTruckSize( int val){
{
		HX_STACK_FRAME("AppleTruck","setTruckSize",0xdd53ae07,"AppleTruck.setTruckSize","AppleTruck.hx",62,0x8bfdcfd9)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(62)
		switch( (int)(val)){
			case (int)0: {
				HX_STACK_LINE(64)
				this->truckCapacity = (int)10;
			}
			;break;
			case (int)1: {
				HX_STACK_LINE(66)
				this->truckCapacity = (int)20;
			}
			;break;
			case (int)2: {
				HX_STACK_LINE(68)
				this->truckCapacity = (int)30;
			}
			;break;
			case (int)3: {
				HX_STACK_LINE(70)
				this->truckCapacity = (int)40;
			}
			;break;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(AppleTruck_obj,setTruckSize,(void))

Void AppleTruck_obj::setLoadingApples( bool val){
{
		HX_STACK_FRAME("AppleTruck","setLoadingApples",0xe3e7eafc,"AppleTruck.setLoadingApples","AppleTruck.hx",75,0x8bfdcfd9)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(75)
		this->loadingApples = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(AppleTruck_obj,setLoadingApples,(void))

Void AppleTruck_obj::setPickUpReady( bool val){
{
		HX_STACK_FRAME("AppleTruck","setPickUpReady",0x840886ee,"AppleTruck.setPickUpReady","AppleTruck.hx",79,0x8bfdcfd9)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(79)
		this->pickUpReady = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(AppleTruck_obj,setPickUpReady,(void))

Void AppleTruck_obj::truckPickup( ){
{
		HX_STACK_FRAME("AppleTruck","truckPickup",0xe6cf6492,"AppleTruck.truckPickup","AppleTruck.hx",83,0x8bfdcfd9)
		HX_STACK_THIS(this)
		HX_STACK_LINE(83)
		if ((this->pickUpReady)){
			HX_STACK_LINE(84)
			hx::SubEq(this->truckTravelTime,::flixel::FlxG_obj::elapsed);
			HX_STACK_LINE(85)
			if ((this->truckSoundReady)){
				HX_STACK_LINE(86)
				::flixel::system::FlxSound _g = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/truckSound.wav"),.7,null(),null(),null(),null(),null());		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(86)
				this->truckSound = _g;
				HX_STACK_LINE(87)
				this->truckSound->play(null());
				HX_STACK_LINE(88)
				this->truckSoundReady = false;
			}
			HX_STACK_LINE(90)
			if (((bool((this->appleTruckSprite->x < (int)35)) && bool((this->truckTravelTime < (int)0))))){
				HX_STACK_LINE(91)
				this->truckIsReset = false;
				HX_STACK_LINE(92)
				{
					HX_STACK_LINE(92)
					::flixel::FlxSprite _g = this->appleTruckSprite;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(92)
					_g->set_x((_g->x + (int)1));
				}
				HX_STACK_LINE(93)
				{
					HX_STACK_LINE(93)
					::flixel::FlxSprite _g = this->appleTruckSprite;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(93)
					_g->set_y((_g->y - .5));
				}
			}
			HX_STACK_LINE(95)
			if (((this->appleTruckSprite->x == (int)35))){
				HX_STACK_LINE(96)
				this->loadingApples = true;
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(AppleTruck_obj,truckPickup,(void))

Void AppleTruck_obj::truckGoHome( ){
{
		HX_STACK_FRAME("AppleTruck","truckGoHome",0xb02adf7d,"AppleTruck.truckGoHome","AppleTruck.hx",102,0x8bfdcfd9)
		HX_STACK_THIS(this)
		HX_STACK_LINE(102)
		if ((!(this->pickUpReady))){
			HX_STACK_LINE(103)
			if (((bool((this->appleTruckSprite->x > (int)-55)) && bool((this->appleTruckSprite->y < (int)370))))){
				HX_STACK_LINE(104)
				{
					HX_STACK_LINE(104)
					::flixel::FlxSprite _g = this->appleTruckSprite;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(104)
					_g->set_x((_g->x - (int)1));
				}
				HX_STACK_LINE(105)
				{
					HX_STACK_LINE(105)
					::flixel::FlxSprite _g = this->appleTruckSprite;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(105)
					_g->set_y((_g->y + .5));
				}
				HX_STACK_LINE(106)
				this->truckTravelTime = (int)5;
			}
			HX_STACK_LINE(108)
			if (((this->appleTruckSprite->x == (int)-55))){
				HX_STACK_LINE(109)
				this->truckIsReset = true;
				HX_STACK_LINE(110)
				this->truckSoundReady = true;
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(AppleTruck_obj,truckGoHome,(void))

Void AppleTruck_obj::destroySounds( ){
{
		HX_STACK_FRAME("AppleTruck","destroySounds",0x08445f35,"AppleTruck.destroySounds","AppleTruck.hx",116,0x8bfdcfd9)
		HX_STACK_THIS(this)
		HX_STACK_LINE(116)
		if (((bool((this->truckSound != null())) && bool(!(((this->truckSound->_channel != null()))))))){
			HX_STACK_LINE(117)
			try
			{
			HX_STACK_CATCHABLE(Dynamic, 0);
			{
				HX_STACK_LINE(118)
				::flixel::system::FlxSound _g = ::flixel::util::FlxDestroyUtil_obj::destroy(this->truckSound);		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(118)
				this->truckSound = _g;
			}
			}
			catch(Dynamic __e){
				{
					HX_STACK_BEGIN_CATCH
					Dynamic e = __e;{
					}
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(AppleTruck_obj,destroySounds,(void))

Void AppleTruck_obj::destroy( ){
{
		HX_STACK_FRAME("AppleTruck","destroy",0xc6ffbf91,"AppleTruck.destroy","AppleTruck.hx",127,0x8bfdcfd9)
		HX_STACK_THIS(this)
		HX_STACK_LINE(128)
		this->appleTruckSprite = null();
		HX_STACK_LINE(129)
		this->appleTruckGroup = null();
		HX_STACK_LINE(130)
		this->truckSound = null();
		HX_STACK_LINE(131)
		this->super::destroy();
	}
return null();
}


Void AppleTruck_obj::update( ){
{
		HX_STACK_FRAME("AppleTruck","update",0x8c579352,"AppleTruck.update","AppleTruck.hx",134,0x8bfdcfd9)
		HX_STACK_THIS(this)
		HX_STACK_LINE(135)
		this->truckGoHome();
		HX_STACK_LINE(136)
		this->truckPickup();
		HX_STACK_LINE(137)
		this->destroySounds();
		HX_STACK_LINE(138)
		this->super::update();
	}
return null();
}



AppleTruck_obj::AppleTruck_obj()
{
}

void AppleTruck_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(AppleTruck);
	HX_MARK_MEMBER_NAME(appleTruckGroup,"appleTruckGroup");
	HX_MARK_MEMBER_NAME(appleTruckSprite,"appleTruckSprite");
	HX_MARK_MEMBER_NAME(truckTravelTime,"truckTravelTime");
	HX_MARK_MEMBER_NAME(truckCapacity,"truckCapacity");
	HX_MARK_MEMBER_NAME(truckSize,"truckSize");
	HX_MARK_MEMBER_NAME(pickUpReady,"pickUpReady");
	HX_MARK_MEMBER_NAME(loadingApples,"loadingApples");
	HX_MARK_MEMBER_NAME(truckIsReset,"truckIsReset");
	HX_MARK_MEMBER_NAME(truckSoundReady,"truckSoundReady");
	HX_MARK_MEMBER_NAME(truckSound,"truckSound");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void AppleTruck_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(appleTruckGroup,"appleTruckGroup");
	HX_VISIT_MEMBER_NAME(appleTruckSprite,"appleTruckSprite");
	HX_VISIT_MEMBER_NAME(truckTravelTime,"truckTravelTime");
	HX_VISIT_MEMBER_NAME(truckCapacity,"truckCapacity");
	HX_VISIT_MEMBER_NAME(truckSize,"truckSize");
	HX_VISIT_MEMBER_NAME(pickUpReady,"pickUpReady");
	HX_VISIT_MEMBER_NAME(loadingApples,"loadingApples");
	HX_VISIT_MEMBER_NAME(truckIsReset,"truckIsReset");
	HX_VISIT_MEMBER_NAME(truckSoundReady,"truckSoundReady");
	HX_VISIT_MEMBER_NAME(truckSound,"truckSound");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic AppleTruck_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"truckSize") ) { return truckSize; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"truckSound") ) { return truckSound; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"pickUpReady") ) { return pickUpReady; }
		if (HX_FIELD_EQ(inName,"truckPickup") ) { return truckPickup_dyn(); }
		if (HX_FIELD_EQ(inName,"truckGoHome") ) { return truckGoHome_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"truckIsReset") ) { return truckIsReset; }
		if (HX_FIELD_EQ(inName,"setTruckSize") ) { return setTruckSize_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"truckCapacity") ) { return truckCapacity; }
		if (HX_FIELD_EQ(inName,"loadingApples") ) { return loadingApples; }
		if (HX_FIELD_EQ(inName,"destroySounds") ) { return destroySounds_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"setPickUpReady") ) { return setPickUpReady_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"appleTruckGroup") ) { return appleTruckGroup; }
		if (HX_FIELD_EQ(inName,"truckTravelTime") ) { return truckTravelTime; }
		if (HX_FIELD_EQ(inName,"truckSoundReady") ) { return truckSoundReady; }
		if (HX_FIELD_EQ(inName,"getTruckIsReset") ) { return getTruckIsReset_dyn(); }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"appleTruckSprite") ) { return appleTruckSprite; }
		if (HX_FIELD_EQ(inName,"getLoadingApples") ) { return getLoadingApples_dyn(); }
		if (HX_FIELD_EQ(inName,"decTruckCapacity") ) { return decTruckCapacity_dyn(); }
		if (HX_FIELD_EQ(inName,"setTruckCapacity") ) { return setTruckCapacity_dyn(); }
		if (HX_FIELD_EQ(inName,"setLoadingApples") ) { return setLoadingApples_dyn(); }
		break;
	case 21:
		if (HX_FIELD_EQ(inName,"getAppleTruckCapacity") ) { return getAppleTruckCapacity_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic AppleTruck_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 9:
		if (HX_FIELD_EQ(inName,"truckSize") ) { truckSize=inValue.Cast< int >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"truckSound") ) { truckSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"pickUpReady") ) { pickUpReady=inValue.Cast< bool >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"truckIsReset") ) { truckIsReset=inValue.Cast< bool >(); return inValue; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"truckCapacity") ) { truckCapacity=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"loadingApples") ) { loadingApples=inValue.Cast< bool >(); return inValue; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"appleTruckGroup") ) { appleTruckGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"truckTravelTime") ) { truckTravelTime=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"truckSoundReady") ) { truckSoundReady=inValue.Cast< bool >(); return inValue; }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"appleTruckSprite") ) { appleTruckSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void AppleTruck_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("appleTruckGroup"));
	outFields->push(HX_CSTRING("appleTruckSprite"));
	outFields->push(HX_CSTRING("truckTravelTime"));
	outFields->push(HX_CSTRING("truckCapacity"));
	outFields->push(HX_CSTRING("truckSize"));
	outFields->push(HX_CSTRING("pickUpReady"));
	outFields->push(HX_CSTRING("loadingApples"));
	outFields->push(HX_CSTRING("truckIsReset"));
	outFields->push(HX_CSTRING("truckSoundReady"));
	outFields->push(HX_CSTRING("truckSound"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(AppleTruck_obj,appleTruckGroup),HX_CSTRING("appleTruckGroup")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(AppleTruck_obj,appleTruckSprite),HX_CSTRING("appleTruckSprite")},
	{hx::fsFloat,(int)offsetof(AppleTruck_obj,truckTravelTime),HX_CSTRING("truckTravelTime")},
	{hx::fsInt,(int)offsetof(AppleTruck_obj,truckCapacity),HX_CSTRING("truckCapacity")},
	{hx::fsInt,(int)offsetof(AppleTruck_obj,truckSize),HX_CSTRING("truckSize")},
	{hx::fsBool,(int)offsetof(AppleTruck_obj,pickUpReady),HX_CSTRING("pickUpReady")},
	{hx::fsBool,(int)offsetof(AppleTruck_obj,loadingApples),HX_CSTRING("loadingApples")},
	{hx::fsBool,(int)offsetof(AppleTruck_obj,truckIsReset),HX_CSTRING("truckIsReset")},
	{hx::fsBool,(int)offsetof(AppleTruck_obj,truckSoundReady),HX_CSTRING("truckSoundReady")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(AppleTruck_obj,truckSound),HX_CSTRING("truckSound")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("appleTruckGroup"),
	HX_CSTRING("appleTruckSprite"),
	HX_CSTRING("truckTravelTime"),
	HX_CSTRING("truckCapacity"),
	HX_CSTRING("truckSize"),
	HX_CSTRING("pickUpReady"),
	HX_CSTRING("loadingApples"),
	HX_CSTRING("truckIsReset"),
	HX_CSTRING("truckSoundReady"),
	HX_CSTRING("truckSound"),
	HX_CSTRING("getLoadingApples"),
	HX_CSTRING("getAppleTruckCapacity"),
	HX_CSTRING("getTruckIsReset"),
	HX_CSTRING("decTruckCapacity"),
	HX_CSTRING("setTruckCapacity"),
	HX_CSTRING("setTruckSize"),
	HX_CSTRING("setLoadingApples"),
	HX_CSTRING("setPickUpReady"),
	HX_CSTRING("truckPickup"),
	HX_CSTRING("truckGoHome"),
	HX_CSTRING("destroySounds"),
	HX_CSTRING("destroy"),
	HX_CSTRING("update"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(AppleTruck_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(AppleTruck_obj::__mClass,"__mClass");
};

#endif

Class AppleTruck_obj::__mClass;

void AppleTruck_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("AppleTruck"), hx::TCanCast< AppleTruck_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void AppleTruck_obj::__boot()
{
}

