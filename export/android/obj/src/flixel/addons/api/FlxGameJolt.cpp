#include <hxcpp.h>

#ifndef INCLUDED_EReg
#include <EReg.h>
#endif
#ifndef INCLUDED_IMap
#include <IMap.h>
#endif
#ifndef INCLUDED_Std
#include <Std.h>
#endif
#ifndef INCLUDED_flixel_addons_api_FlxGameJolt
#include <flixel/addons/api/FlxGameJolt.h>
#endif
#ifndef INCLUDED_haxe_crypto_Md5
#include <haxe/crypto/Md5.h>
#endif
#ifndef INCLUDED_haxe_crypto_Sha1
#include <haxe/crypto/Sha1.h>
#endif
#ifndef INCLUDED_haxe_ds_StringMap
#include <haxe/ds/StringMap.h>
#endif
#ifndef INCLUDED_openfl__v2_display_BitmapData
#include <openfl/_v2/display/BitmapData.h>
#endif
#ifndef INCLUDED_openfl__v2_display_DisplayObject
#include <openfl/_v2/display/DisplayObject.h>
#endif
#ifndef INCLUDED_openfl__v2_display_DisplayObjectContainer
#include <openfl/_v2/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_openfl__v2_display_IBitmapDrawable
#include <openfl/_v2/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_openfl__v2_display_InteractiveObject
#include <openfl/_v2/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_openfl__v2_display_Loader
#include <openfl/_v2/display/Loader.h>
#endif
#ifndef INCLUDED_openfl__v2_display_LoaderInfo
#include <openfl/_v2/display/LoaderInfo.h>
#endif
#ifndef INCLUDED_openfl__v2_display_Sprite
#include <openfl/_v2/display/Sprite.h>
#endif
#ifndef INCLUDED_openfl__v2_events_Event
#include <openfl/_v2/events/Event.h>
#endif
#ifndef INCLUDED_openfl__v2_events_EventDispatcher
#include <openfl/_v2/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__v2_events_IEventDispatcher
#include <openfl/_v2/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__v2_net_URLLoader
#include <openfl/_v2/net/URLLoader.h>
#endif
#ifndef INCLUDED_openfl__v2_net_URLRequest
#include <openfl/_v2/net/URLRequest.h>
#endif
#ifndef INCLUDED_openfl_system_LoaderContext
#include <openfl/system/LoaderContext.h>
#endif
namespace flixel{
namespace addons{
namespace api{

Void FlxGameJolt_obj::__construct()
{
	return null();
}

//FlxGameJolt_obj::~FlxGameJolt_obj() { }

Dynamic FlxGameJolt_obj::__CreateEmpty() { return  new FlxGameJolt_obj; }
hx::ObjectPtr< FlxGameJolt_obj > FlxGameJolt_obj::__new()
{  hx::ObjectPtr< FlxGameJolt_obj > result = new FlxGameJolt_obj();
	result->__construct();
	return result;}

Dynamic FlxGameJolt_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< FlxGameJolt_obj > result = new FlxGameJolt_obj();
	result->__construct();
	return result;}

int FlxGameJolt_obj::hashType;

bool FlxGameJolt_obj::verbose;

bool FlxGameJolt_obj::initialized;

bool FlxGameJolt_obj::get_initialized( ){
	HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","get_initialized",0xa55800d1,"flixel.addons.api.FlxGameJolt.get_initialized","flixel/addons/api/FlxGameJolt.hx",58,0x83b7688b)
	HX_STACK_LINE(58)
	return ::flixel::addons::api::FlxGameJolt_obj::_initialized;
}


STATIC_HX_DEFINE_DYNAMIC_FUNC0(FlxGameJolt_obj,get_initialized,return )

int FlxGameJolt_obj::HASH_MD5;

int FlxGameJolt_obj::HASH_SHA1;

int FlxGameJolt_obj::TROPHIES_MISSING;

int FlxGameJolt_obj::TROPHIES_ACHIEVED;

Dynamic FlxGameJolt_obj::_callBack;

int FlxGameJolt_obj::_gameID;

::String FlxGameJolt_obj::_privateKey;

::String FlxGameJolt_obj::_userName;

::String FlxGameJolt_obj::_userToken;

::String FlxGameJolt_obj::_idURL;

bool FlxGameJolt_obj::_initialized;

bool FlxGameJolt_obj::_verifyAuth;

bool FlxGameJolt_obj::_getImage;

::openfl::_v2::net::URLLoader FlxGameJolt_obj::_loader;

::String FlxGameJolt_obj::URL_API;

::String FlxGameJolt_obj::RETURN_TYPE;

::String FlxGameJolt_obj::URL_GAME_ID;

::String FlxGameJolt_obj::URL_USER_NAME;

::String FlxGameJolt_obj::URL_USER_TOKEN;

Void FlxGameJolt_obj::init( int GameID,::String PrivateKey,hx::Null< bool >  __o_AutoAuth,::String UserName,::String UserToken,Dynamic Callback){
bool AutoAuth = __o_AutoAuth.Default(false);
	HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","init",0xa534c92a,"flixel.addons.api.FlxGameJolt.init","flixel/addons/api/FlxGameJolt.hx",156,0x83b7688b)
	HX_STACK_ARG(GameID,"GameID")
	HX_STACK_ARG(PrivateKey,"PrivateKey")
	HX_STACK_ARG(AutoAuth,"AutoAuth")
	HX_STACK_ARG(UserName,"UserName")
	HX_STACK_ARG(UserToken,"UserToken")
	HX_STACK_ARG(Callback,"Callback")
{
		HX_STACK_LINE(157)
		if (((bool((::flixel::addons::api::FlxGameJolt_obj::_gameID != (int)0)) && bool((::flixel::addons::api::FlxGameJolt_obj::_privateKey != HX_CSTRING("")))))){
			HX_STACK_LINE(157)
			return null();
		}
		HX_STACK_LINE(159)
		::flixel::addons::api::FlxGameJolt_obj::_gameID = GameID;
		HX_STACK_LINE(160)
		::flixel::addons::api::FlxGameJolt_obj::_privateKey = PrivateKey;
		HX_STACK_LINE(165)
		if ((AutoAuth)){
			HX_STACK_LINE(166)
			if (((bool((UserName != null())) && bool((UserToken != null()))))){
				HX_STACK_LINE(167)
				::flixel::addons::api::FlxGameJolt_obj::authUser(UserName,UserToken,Callback);
			}
			else{
				struct _Function_3_1{
					inline static bool Block( ){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","flixel/addons/api/FlxGameJolt.hx",168,0x83b7688b)
						{
							HX_STACK_LINE(168)
							return (  ((!(::flixel::addons::api::FlxGameJolt_obj::get_isEmbeddedFlash()))) ? bool(::flixel::addons::api::FlxGameJolt_obj::get_isQuickPlay()) : bool(true) );
						}
						return null();
					}
				};
				HX_STACK_LINE(168)
				if (((  (((bool((UserName == null())) || bool((UserToken == null()))))) ? bool(_Function_3_1::Block()) : bool(false) ))){
					HX_STACK_LINE(169)
					::flixel::addons::api::FlxGameJolt_obj::authUser(null(),null(),Callback);
				}
				else{
					HX_STACK_LINE(171)
					Callback(false);
				}
			}
		}
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC6(FlxGameJolt_obj,init,(void))

Void FlxGameJolt_obj::fetchUser( Dynamic UserID,::String UserName,Array< int > UserIDs,Dynamic Callback){
{
		HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","fetchUser",0x20c38b4b,"flixel.addons.api.FlxGameJolt.fetchUser","flixel/addons/api/FlxGameJolt.hx",186,0x83b7688b)
		HX_STACK_ARG(UserID,"UserID")
		HX_STACK_ARG(UserName,"UserName")
		HX_STACK_ARG(UserIDs,"UserIDs")
		HX_STACK_ARG(Callback,"Callback")
		HX_STACK_LINE(187)
		::String tempURL = (HX_CSTRING("http://gamejolt.com/api/game/v1/users/?format=keypair&game_id=") + ::flixel::addons::api::FlxGameJolt_obj::_gameID);		HX_STACK_VAR(tempURL,"tempURL");
		HX_STACK_LINE(189)
		if (((bool((UserID != null())) && bool((UserID != (int)0))))){
			HX_STACK_LINE(190)
			::String _g = ::Std_obj::string(UserID);		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(190)
			::String _g1 = (HX_CSTRING("&user_id=") + _g);		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(190)
			hx::AddEq(tempURL,_g1);
		}
		else{
			HX_STACK_LINE(191)
			if (((bool((UserName != null())) && bool((UserName != HX_CSTRING("")))))){
				HX_STACK_LINE(192)
				hx::AddEq(tempURL,(HX_CSTRING("&username=") + UserName));
			}
			else{
				HX_STACK_LINE(193)
				if (((bool((UserIDs != null())) && bool((UserIDs != Array_obj< int >::__new()))))){
					HX_STACK_LINE(194)
					hx::AddEq(tempURL,HX_CSTRING("&user_id="));
					HX_STACK_LINE(196)
					{
						HX_STACK_LINE(196)
						int _g = (int)0;		HX_STACK_VAR(_g,"_g");
						HX_STACK_LINE(196)
						while((true)){
							HX_STACK_LINE(196)
							if ((!(((_g < UserIDs->length))))){
								HX_STACK_LINE(196)
								break;
							}
							HX_STACK_LINE(196)
							int id = UserIDs->__get(_g);		HX_STACK_VAR(id,"id");
							HX_STACK_LINE(196)
							++(_g);
							HX_STACK_LINE(197)
							::String _g2 = ::Std_obj::string(id);		HX_STACK_VAR(_g2,"_g2");
							HX_STACK_LINE(197)
							::String _g3 = (_g2 + HX_CSTRING(","));		HX_STACK_VAR(_g3,"_g3");
							HX_STACK_LINE(197)
							hx::AddEq(tempURL,_g3);
						}
					}
					HX_STACK_LINE(200)
					::String _g4 = tempURL.substr((int)0,(tempURL.length - (int)1));		HX_STACK_VAR(_g4,"_g4");
					HX_STACK_LINE(200)
					tempURL = _g4;
				}
				else{
					HX_STACK_LINE(202)
					return null();
				}
			}
		}
		HX_STACK_LINE(205)
		::flixel::addons::api::FlxGameJolt_obj::sendLoaderRequest(tempURL,Callback);
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC4(FlxGameJolt_obj,fetchUser,(void))

Void FlxGameJolt_obj::authUser( ::String UserName,::String UserToken,Dynamic Callback){
{
		HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","authUser",0x17c914ed,"flixel.addons.api.FlxGameJolt.authUser","flixel/addons/api/FlxGameJolt.hx",217,0x83b7688b)
		HX_STACK_ARG(UserName,"UserName")
		HX_STACK_ARG(UserToken,"UserToken")
		HX_STACK_ARG(Callback,"Callback")
		HX_STACK_LINE(218)
		if (((  ((!((!(::flixel::addons::api::FlxGameJolt_obj::get_gameInit()))))) ? bool((bool((::flixel::addons::api::FlxGameJolt_obj::_userName != null())) && bool((::flixel::addons::api::FlxGameJolt_obj::_userToken != null())))) : bool(true) ))){
			HX_STACK_LINE(219)
			Callback(false);
			HX_STACK_LINE(220)
			return null();
		}
		HX_STACK_LINE(223)
		if (((bool((UserName == null())) || bool((UserToken == null()))))){
		}
		else{
			HX_STACK_LINE(248)
			::flixel::addons::api::FlxGameJolt_obj::_userName = UserName;
			HX_STACK_LINE(249)
			::flixel::addons::api::FlxGameJolt_obj::_userToken = UserToken;
		}
		HX_STACK_LINE(254)
		if (((bool((::flixel::addons::api::FlxGameJolt_obj::_userName != null())) && bool((::flixel::addons::api::FlxGameJolt_obj::_userToken != null()))))){
			HX_STACK_LINE(255)
			::flixel::addons::api::FlxGameJolt_obj::_idURL = (((((HX_CSTRING("&game_id=") + ::flixel::addons::api::FlxGameJolt_obj::_gameID) + HX_CSTRING("&username=")) + ::flixel::addons::api::FlxGameJolt_obj::_userName) + HX_CSTRING("&user_token=")) + ::flixel::addons::api::FlxGameJolt_obj::_userToken);
			HX_STACK_LINE(256)
			::flixel::addons::api::FlxGameJolt_obj::_verifyAuth = true;
			HX_STACK_LINE(257)
			::flixel::addons::api::FlxGameJolt_obj::sendLoaderRequest((HX_CSTRING("http://gamejolt.com/api/game/v1/users/auth/?format=keypair") + ::flixel::addons::api::FlxGameJolt_obj::_idURL),Callback);
		}
		else{
		}
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC3(FlxGameJolt_obj,authUser,(void))

Void FlxGameJolt_obj::openSession( Dynamic Callback){
{
		HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","openSession",0x6aed71d2,"flixel.addons.api.FlxGameJolt.openSession","flixel/addons/api/FlxGameJolt.hx",272,0x83b7688b)
		HX_STACK_ARG(Callback,"Callback")
		HX_STACK_LINE(273)
		if ((!(::flixel::addons::api::FlxGameJolt_obj::get_authenticated()))){
			HX_STACK_LINE(273)
			return null();
		}
		HX_STACK_LINE(275)
		::flixel::addons::api::FlxGameJolt_obj::sendLoaderRequest((HX_CSTRING("http://gamejolt.com/api/game/v1/sessions/open/?format=keypair") + ::flixel::addons::api::FlxGameJolt_obj::_idURL),Callback);
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(FlxGameJolt_obj,openSession,(void))

Void FlxGameJolt_obj::pingSession( hx::Null< bool >  __o_Active,Dynamic Callback){
bool Active = __o_Active.Default(true);
	HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","pingSession",0x83de282a,"flixel.addons.api.FlxGameJolt.pingSession","flixel/addons/api/FlxGameJolt.hx",286,0x83b7688b)
	HX_STACK_ARG(Active,"Active")
	HX_STACK_ARG(Callback,"Callback")
{
		HX_STACK_LINE(287)
		if ((!(::flixel::addons::api::FlxGameJolt_obj::get_authenticated()))){
			HX_STACK_LINE(287)
			return null();
		}
		HX_STACK_LINE(289)
		::String tempURL = ((HX_CSTRING("http://gamejolt.com/api/game/v1/sessions/ping/?format=keypair") + ::flixel::addons::api::FlxGameJolt_obj::_idURL) + HX_CSTRING("&active="));		HX_STACK_VAR(tempURL,"tempURL");
		HX_STACK_LINE(291)
		if ((Active)){
			HX_STACK_LINE(292)
			hx::AddEq(tempURL,HX_CSTRING("active"));
		}
		else{
			HX_STACK_LINE(294)
			hx::AddEq(tempURL,HX_CSTRING("idle"));
		}
		HX_STACK_LINE(297)
		::flixel::addons::api::FlxGameJolt_obj::sendLoaderRequest(tempURL,Callback);
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC2(FlxGameJolt_obj,pingSession,(void))

Void FlxGameJolt_obj::closeSession( Dynamic Callback){
{
		HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","closeSession",0x34a54698,"flixel.addons.api.FlxGameJolt.closeSession","flixel/addons/api/FlxGameJolt.hx",307,0x83b7688b)
		HX_STACK_ARG(Callback,"Callback")
		HX_STACK_LINE(308)
		if ((!(::flixel::addons::api::FlxGameJolt_obj::get_authenticated()))){
			HX_STACK_LINE(308)
			return null();
		}
		HX_STACK_LINE(310)
		::flixel::addons::api::FlxGameJolt_obj::sendLoaderRequest((HX_CSTRING("http://gamejolt.com/api/game/v1/sessions/close/?format=keypair") + ::flixel::addons::api::FlxGameJolt_obj::_idURL),Callback);
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(FlxGameJolt_obj,closeSession,(void))

Void FlxGameJolt_obj::fetchTrophy( hx::Null< int >  __o_DataType,Dynamic Callback){
int DataType = __o_DataType.Default(0);
	HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","fetchTrophy",0x8e0e3bf0,"flixel.addons.api.FlxGameJolt.fetchTrophy","flixel/addons/api/FlxGameJolt.hx",321,0x83b7688b)
	HX_STACK_ARG(DataType,"DataType")
	HX_STACK_ARG(Callback,"Callback")
{
		HX_STACK_LINE(322)
		if ((!(::flixel::addons::api::FlxGameJolt_obj::get_authenticated()))){
			HX_STACK_LINE(322)
			return null();
		}
		HX_STACK_LINE(324)
		::String tempURL = (HX_CSTRING("http://gamejolt.com/api/game/v1/trophies/?format=keypair") + ::flixel::addons::api::FlxGameJolt_obj::_idURL);		HX_STACK_VAR(tempURL,"tempURL");
		HX_STACK_LINE(326)
		switch( (int)(DataType)){
			case (int)0: {
				HX_STACK_LINE(328)
				hx::AddEq(tempURL,HX_CSTRING("&achieved="));
			}
			;break;
			case (int)-1: {
				HX_STACK_LINE(330)
				hx::AddEq(tempURL,HX_CSTRING("&achieved=false"));
			}
			;break;
			case (int)-2: {
				HX_STACK_LINE(332)
				hx::AddEq(tempURL,HX_CSTRING("&achieved=true"));
			}
			;break;
			default: {
				HX_STACK_LINE(334)
				::String _g = ::Std_obj::string(DataType);		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(334)
				::String _g1 = (HX_CSTRING("&trophy_id=") + _g);		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(334)
				hx::AddEq(tempURL,_g1);
			}
		}
		HX_STACK_LINE(337)
		::flixel::addons::api::FlxGameJolt_obj::sendLoaderRequest(tempURL,Callback);
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC2(FlxGameJolt_obj,fetchTrophy,(void))

Void FlxGameJolt_obj::addTrophy( int TrophyID,Dynamic Callback){
{
		HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","addTrophy",0x03574b97,"flixel.addons.api.FlxGameJolt.addTrophy","flixel/addons/api/FlxGameJolt.hx",348,0x83b7688b)
		HX_STACK_ARG(TrophyID,"TrophyID")
		HX_STACK_ARG(Callback,"Callback")
		HX_STACK_LINE(349)
		if ((!(::flixel::addons::api::FlxGameJolt_obj::get_authenticated()))){
			HX_STACK_LINE(349)
			return null();
		}
		HX_STACK_LINE(351)
		::flixel::addons::api::FlxGameJolt_obj::sendLoaderRequest((((HX_CSTRING("http://gamejolt.com/api/game/v1/trophies/add-achieved/?format=keypair") + ::flixel::addons::api::FlxGameJolt_obj::_idURL) + HX_CSTRING("&trophy_id=")) + TrophyID),Callback);
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC2(FlxGameJolt_obj,addTrophy,(void))

Void FlxGameJolt_obj::fetchScore( Dynamic Limit,Dynamic Callback){
{
		HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","fetchScore",0x58fd5a52,"flixel.addons.api.FlxGameJolt.fetchScore","flixel/addons/api/FlxGameJolt.hx",362,0x83b7688b)
		HX_STACK_ARG(Limit,"Limit")
		HX_STACK_ARG(Callback,"Callback")
		HX_STACK_LINE(363)
		if ((!(::flixel::addons::api::FlxGameJolt_obj::get_gameInit()))){
			HX_STACK_LINE(363)
			return null();
		}
		HX_STACK_LINE(365)
		::String tempURL = (HX_CSTRING("http://gamejolt.com/api/game/v1/scores/?format=keypair&game_id=") + ::flixel::addons::api::FlxGameJolt_obj::_gameID);		HX_STACK_VAR(tempURL,"tempURL");
		HX_STACK_LINE(367)
		if ((!(::flixel::addons::api::FlxGameJolt_obj::_initialized))){
			HX_STACK_LINE(368)
			if (((Limit == null()))){
				HX_STACK_LINE(369)
				hx::AddEq(tempURL,HX_CSTRING("&limit=10"));
			}
			else{
				HX_STACK_LINE(371)
				::String _g = ::Std_obj::string(Limit);		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(371)
				::String _g1 = (HX_CSTRING("&limit=") + _g);		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(371)
				hx::AddEq(tempURL,_g1);
			}
		}
		else{
			HX_STACK_LINE(373)
			if (((Limit != null()))){
				HX_STACK_LINE(374)
				::String _g2 = ::Std_obj::string(Limit);		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(374)
				::String _g3 = (HX_CSTRING("&limit=") + _g2);		HX_STACK_VAR(_g3,"_g3");
				HX_STACK_LINE(374)
				hx::AddEq(tempURL,_g3);
			}
			else{
				HX_STACK_LINE(376)
				hx::AddEq(tempURL,::flixel::addons::api::FlxGameJolt_obj::_idURL);
			}
		}
		HX_STACK_LINE(379)
		::flixel::addons::api::FlxGameJolt_obj::sendLoaderRequest(tempURL,Callback);
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC2(FlxGameJolt_obj,fetchScore,(void))

Void FlxGameJolt_obj::addScore( ::String Score,Float Sort,Dynamic TableID,hx::Null< bool >  __o_AllowGuest,::String GuestName,::String ExtraData,Dynamic Callback){
bool AllowGuest = __o_AllowGuest.Default(false);
	HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","addScore",0xe0fa3c8b,"flixel.addons.api.FlxGameJolt.addScore","flixel/addons/api/FlxGameJolt.hx",397,0x83b7688b)
	HX_STACK_ARG(Score,"Score")
	HX_STACK_ARG(Sort,"Sort")
	HX_STACK_ARG(TableID,"TableID")
	HX_STACK_ARG(AllowGuest,"AllowGuest")
	HX_STACK_ARG(GuestName,"GuestName")
	HX_STACK_ARG(ExtraData,"ExtraData")
	HX_STACK_ARG(Callback,"Callback")
{
		HX_STACK_LINE(398)
		if ((!(::flixel::addons::api::FlxGameJolt_obj::get_gameInit()))){
			HX_STACK_LINE(398)
			return null();
		}
		HX_STACK_LINE(400)
		if (((  ((!(::flixel::addons::api::FlxGameJolt_obj::get_authenticated()))) ? bool(!(AllowGuest)) : bool(false) ))){
			HX_STACK_LINE(400)
			return null();
		}
		HX_STACK_LINE(402)
		::String _g = ::Std_obj::string(Sort);		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(402)
		::String tempURL = (((((HX_CSTRING("http://gamejolt.com/api/game/v1/scores/add/?format=keypair&game_id=") + ::flixel::addons::api::FlxGameJolt_obj::_gameID) + HX_CSTRING("&score=")) + Score) + HX_CSTRING("&sort=")) + _g);		HX_STACK_VAR(tempURL,"tempURL");
		HX_STACK_LINE(406)
		if (((bool((bool(AllowGuest) && bool((GuestName != null())))) && bool((GuestName != HX_CSTRING("")))))){
			HX_STACK_LINE(407)
			hx::AddEq(tempURL,(HX_CSTRING("&guest=") + GuestName));
		}
		else{
			HX_STACK_LINE(409)
			hx::AddEq(tempURL,(((HX_CSTRING("&username=") + ::flixel::addons::api::FlxGameJolt_obj::_userName) + HX_CSTRING("&user_token=")) + ::flixel::addons::api::FlxGameJolt_obj::_userToken));
		}
		HX_STACK_LINE(412)
		if (((bool((ExtraData != null())) && bool((ExtraData != HX_CSTRING("")))))){
			HX_STACK_LINE(413)
			hx::AddEq(tempURL,(HX_CSTRING("&extra_data=") + ExtraData));
		}
		HX_STACK_LINE(416)
		if (((bool((TableID != null())) && bool((TableID != (int)0))))){
			HX_STACK_LINE(417)
			hx::AddEq(tempURL,(HX_CSTRING("&table_id=") + TableID));
		}
		HX_STACK_LINE(420)
		::flixel::addons::api::FlxGameJolt_obj::sendLoaderRequest(tempURL,Callback);
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC7(FlxGameJolt_obj,addScore,(void))

Void FlxGameJolt_obj::getTables( Dynamic Callback){
{
		HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","getTables",0xf51ef181,"flixel.addons.api.FlxGameJolt.getTables","flixel/addons/api/FlxGameJolt.hx",430,0x83b7688b)
		HX_STACK_ARG(Callback,"Callback")
		HX_STACK_LINE(431)
		if ((!(::flixel::addons::api::FlxGameJolt_obj::get_gameInit()))){
			HX_STACK_LINE(431)
			return null();
		}
		HX_STACK_LINE(433)
		::flixel::addons::api::FlxGameJolt_obj::sendLoaderRequest((HX_CSTRING("http://gamejolt.com/api/game/v1/scores/tables/?format=keypair&game_id=") + ::flixel::addons::api::FlxGameJolt_obj::_gameID),Callback);
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(FlxGameJolt_obj,getTables,(void))

Void FlxGameJolt_obj::fetchData( ::String Key,hx::Null< bool >  __o_User,Dynamic Callback){
bool User = __o_User.Default(true);
	HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","fetchData",0x15794f2a,"flixel.addons.api.FlxGameJolt.fetchData","flixel/addons/api/FlxGameJolt.hx",445,0x83b7688b)
	HX_STACK_ARG(Key,"Key")
	HX_STACK_ARG(User,"User")
	HX_STACK_ARG(Callback,"Callback")
{
		HX_STACK_LINE(446)
		if ((!(::flixel::addons::api::FlxGameJolt_obj::get_gameInit()))){
			HX_STACK_LINE(446)
			return null();
		}
		HX_STACK_LINE(447)
		if (((  ((User)) ? bool(!(::flixel::addons::api::FlxGameJolt_obj::get_authenticated())) : bool(false) ))){
			HX_STACK_LINE(447)
			return null();
		}
		HX_STACK_LINE(449)
		::String tempURL = (HX_CSTRING("http://gamejolt.com/api/game/v1/data-store/?format=keypair&key=") + Key);		HX_STACK_VAR(tempURL,"tempURL");
		HX_STACK_LINE(451)
		if ((User)){
			HX_STACK_LINE(452)
			hx::AddEq(tempURL,::flixel::addons::api::FlxGameJolt_obj::_idURL);
		}
		else{
			HX_STACK_LINE(454)
			hx::AddEq(tempURL,(HX_CSTRING("&game_id=") + ::flixel::addons::api::FlxGameJolt_obj::_gameID));
		}
		HX_STACK_LINE(457)
		::flixel::addons::api::FlxGameJolt_obj::sendLoaderRequest(tempURL,Callback);
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC3(FlxGameJolt_obj,fetchData,(void))

Void FlxGameJolt_obj::setData( ::String Key,::String Value,hx::Null< bool >  __o_User,Dynamic Callback){
bool User = __o_User.Default(true);
	HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","setData",0xc78a5512,"flixel.addons.api.FlxGameJolt.setData","flixel/addons/api/FlxGameJolt.hx",471,0x83b7688b)
	HX_STACK_ARG(Key,"Key")
	HX_STACK_ARG(Value,"Value")
	HX_STACK_ARG(User,"User")
	HX_STACK_ARG(Callback,"Callback")
{
		HX_STACK_LINE(472)
		if ((!(::flixel::addons::api::FlxGameJolt_obj::get_gameInit()))){
			HX_STACK_LINE(472)
			return null();
		}
		HX_STACK_LINE(473)
		if (((  ((User)) ? bool(!(::flixel::addons::api::FlxGameJolt_obj::get_authenticated())) : bool(false) ))){
			HX_STACK_LINE(473)
			return null();
		}
		HX_STACK_LINE(475)
		::String tempURL = (((HX_CSTRING("http://gamejolt.com/api/game/v1/data-store/set/?format=keypair&key=") + Key) + HX_CSTRING("&data=")) + Value);		HX_STACK_VAR(tempURL,"tempURL");
		HX_STACK_LINE(477)
		if ((User)){
			HX_STACK_LINE(478)
			hx::AddEq(tempURL,::flixel::addons::api::FlxGameJolt_obj::_idURL);
		}
		else{
			HX_STACK_LINE(480)
			hx::AddEq(tempURL,(HX_CSTRING("&game_id=") + ::flixel::addons::api::FlxGameJolt_obj::_gameID));
		}
		HX_STACK_LINE(483)
		::flixel::addons::api::FlxGameJolt_obj::sendLoaderRequest(tempURL,Callback);
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC4(FlxGameJolt_obj,setData,(void))

Void FlxGameJolt_obj::updateData( ::String Key,::String Operation,::String Value,hx::Null< bool >  __o_User,Dynamic Callback){
bool User = __o_User.Default(true);
	HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","updateData",0xdc5e5e6d,"flixel.addons.api.FlxGameJolt.updateData","flixel/addons/api/FlxGameJolt.hx",498,0x83b7688b)
	HX_STACK_ARG(Key,"Key")
	HX_STACK_ARG(Operation,"Operation")
	HX_STACK_ARG(Value,"Value")
	HX_STACK_ARG(User,"User")
	HX_STACK_ARG(Callback,"Callback")
{
		HX_STACK_LINE(499)
		if ((!(::flixel::addons::api::FlxGameJolt_obj::get_gameInit()))){
			HX_STACK_LINE(499)
			return null();
		}
		HX_STACK_LINE(500)
		if (((  ((User)) ? bool(!(::flixel::addons::api::FlxGameJolt_obj::get_authenticated())) : bool(false) ))){
			HX_STACK_LINE(500)
			return null();
		}
		HX_STACK_LINE(502)
		::String tempURL = (((((HX_CSTRING("http://gamejolt.com/api/game/v1/data-store/update/?format=keypair&key=") + Key) + HX_CSTRING("&operation=")) + Operation) + HX_CSTRING("&value=")) + Value);		HX_STACK_VAR(tempURL,"tempURL");
		HX_STACK_LINE(504)
		if ((User)){
			HX_STACK_LINE(505)
			hx::AddEq(tempURL,::flixel::addons::api::FlxGameJolt_obj::_idURL);
		}
		else{
			HX_STACK_LINE(507)
			hx::AddEq(tempURL,(HX_CSTRING("&game_id=") + ::flixel::addons::api::FlxGameJolt_obj::_gameID));
		}
		HX_STACK_LINE(510)
		::flixel::addons::api::FlxGameJolt_obj::sendLoaderRequest(tempURL,Callback);
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC5(FlxGameJolt_obj,updateData,(void))

Void FlxGameJolt_obj::removeData( ::String Key,hx::Null< bool >  __o_User,Dynamic Callback){
bool User = __o_User.Default(true);
	HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","removeData",0xa0c8ae28,"flixel.addons.api.FlxGameJolt.removeData","flixel/addons/api/FlxGameJolt.hx",522,0x83b7688b)
	HX_STACK_ARG(Key,"Key")
	HX_STACK_ARG(User,"User")
	HX_STACK_ARG(Callback,"Callback")
{
		HX_STACK_LINE(523)
		if ((!(::flixel::addons::api::FlxGameJolt_obj::get_gameInit()))){
			HX_STACK_LINE(523)
			return null();
		}
		HX_STACK_LINE(524)
		if (((  ((User)) ? bool(!(::flixel::addons::api::FlxGameJolt_obj::get_authenticated())) : bool(false) ))){
			HX_STACK_LINE(524)
			return null();
		}
		HX_STACK_LINE(526)
		::String tempURL = (HX_CSTRING("http://gamejolt.com/api/game/v1/data-store/remove/?format=keypair&key=") + Key);		HX_STACK_VAR(tempURL,"tempURL");
		HX_STACK_LINE(528)
		if ((User)){
			HX_STACK_LINE(529)
			hx::AddEq(tempURL,::flixel::addons::api::FlxGameJolt_obj::_idURL);
		}
		else{
			HX_STACK_LINE(531)
			hx::AddEq(tempURL,(HX_CSTRING("&game_id=") + ::flixel::addons::api::FlxGameJolt_obj::_gameID));
		}
		HX_STACK_LINE(534)
		::flixel::addons::api::FlxGameJolt_obj::sendLoaderRequest(tempURL,Callback);
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC3(FlxGameJolt_obj,removeData,(void))

Void FlxGameJolt_obj::getAllKeys( hx::Null< bool >  __o_User,Dynamic Callback){
bool User = __o_User.Default(true);
	HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","getAllKeys",0x81fb4f59,"flixel.addons.api.FlxGameJolt.getAllKeys","flixel/addons/api/FlxGameJolt.hx",545,0x83b7688b)
	HX_STACK_ARG(User,"User")
	HX_STACK_ARG(Callback,"Callback")
{
		HX_STACK_LINE(546)
		if ((!(::flixel::addons::api::FlxGameJolt_obj::get_gameInit()))){
			HX_STACK_LINE(546)
			return null();
		}
		HX_STACK_LINE(547)
		if (((  ((User)) ? bool(!(::flixel::addons::api::FlxGameJolt_obj::get_authenticated())) : bool(false) ))){
			HX_STACK_LINE(547)
			return null();
		}
		HX_STACK_LINE(549)
		::String tempURL = HX_CSTRING("http://gamejolt.com/api/game/v1/data-store/get-keys/?format=keypair");		HX_STACK_VAR(tempURL,"tempURL");
		HX_STACK_LINE(551)
		if ((User)){
			HX_STACK_LINE(552)
			hx::AddEq(tempURL,::flixel::addons::api::FlxGameJolt_obj::_idURL);
		}
		else{
			HX_STACK_LINE(554)
			hx::AddEq(tempURL,(HX_CSTRING("&game_id=") + ::flixel::addons::api::FlxGameJolt_obj::_gameID));
		}
		HX_STACK_LINE(557)
		::flixel::addons::api::FlxGameJolt_obj::sendLoaderRequest(tempURL,Callback);
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC2(FlxGameJolt_obj,getAllKeys,(void))

Void FlxGameJolt_obj::sendLoaderRequest( ::String URLString,Dynamic Callback){
{
		HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","sendLoaderRequest",0xf82976ba,"flixel.addons.api.FlxGameJolt.sendLoaderRequest","flixel/addons/api/FlxGameJolt.hx",567,0x83b7688b)
		HX_STACK_ARG(URLString,"URLString")
		HX_STACK_ARG(Callback,"Callback")
		HX_STACK_LINE(568)
		::String _g = ::flixel::addons::api::FlxGameJolt_obj::encryptURL(URLString);		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(568)
		::String _g1 = ((URLString + HX_CSTRING("&signature=")) + _g);		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(568)
		::openfl::_v2::net::URLRequest request = ::openfl::_v2::net::URLRequest_obj::__new(_g1);		HX_STACK_VAR(request,"request");
		HX_STACK_LINE(569)
		request->method = HX_CSTRING("POST");
		HX_STACK_LINE(571)
		::flixel::addons::api::FlxGameJolt_obj::_callBack = Callback;
		HX_STACK_LINE(573)
		if (((::flixel::addons::api::FlxGameJolt_obj::_loader == null()))){
			HX_STACK_LINE(574)
			::openfl::_v2::net::URLLoader _g2 = ::openfl::_v2::net::URLLoader_obj::__new(null());		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(574)
			::flixel::addons::api::FlxGameJolt_obj::_loader = _g2;
		}
		HX_STACK_LINE(583)
		::flixel::addons::api::FlxGameJolt_obj::_loader->addEventListener(::openfl::_v2::events::Event_obj::COMPLETE,::flixel::addons::api::FlxGameJolt_obj::parseData_dyn(),null(),null(),null());
		HX_STACK_LINE(584)
		::flixel::addons::api::FlxGameJolt_obj::_loader->load(request);
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC2(FlxGameJolt_obj,sendLoaderRequest,(void))

Void FlxGameJolt_obj::parseData( ::openfl::_v2::events::Event e){
{
		HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","parseData",0xc674bba3,"flixel.addons.api.FlxGameJolt.parseData","flixel/addons/api/FlxGameJolt.hx",596,0x83b7688b)
		HX_STACK_ARG(e,"e")
		HX_STACK_LINE(597)
		::flixel::addons::api::FlxGameJolt_obj::_loader->removeEventListener(::openfl::_v2::events::Event_obj::COMPLETE,::flixel::addons::api::FlxGameJolt_obj::parseData_dyn(),null());
		HX_STACK_LINE(599)
		::String _g = ::Std_obj::string(e->get_currentTarget()->__Field(HX_CSTRING("data"),true));		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(599)
		if (((_g == HX_CSTRING("")))){
			HX_STACK_LINE(604)
			return null();
		}
		HX_STACK_LINE(607)
		::haxe::ds::StringMap returnMap = ::haxe::ds::StringMap_obj::__new();		HX_STACK_VAR(returnMap,"returnMap");
		HX_STACK_LINE(608)
		Array< ::String > stringArray = ::Std_obj::string(e->get_currentTarget()->__Field(HX_CSTRING("data"),true)).split(HX_CSTRING("\r"));		HX_STACK_VAR(stringArray,"stringArray");
		HX_STACK_LINE(611)
		::EReg r = ::EReg_obj::__new(HX_CSTRING("[\r\n\t\"]+"),HX_CSTRING("g"));		HX_STACK_VAR(r,"r");
		HX_STACK_LINE(613)
		{
			HX_STACK_LINE(613)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(613)
			while((true)){
				HX_STACK_LINE(613)
				if ((!(((_g1 < stringArray->length))))){
					HX_STACK_LINE(613)
					break;
				}
				HX_STACK_LINE(613)
				::String string = stringArray->__get(_g1);		HX_STACK_VAR(string,"string");
				HX_STACK_LINE(613)
				++(_g1);
				HX_STACK_LINE(615)
				::String _g11 = r->replace(string,HX_CSTRING(""));		HX_STACK_VAR(_g11,"_g11");
				HX_STACK_LINE(615)
				string = _g11;
				HX_STACK_LINE(616)
				if (((string.length > (int)1))){
					HX_STACK_LINE(617)
					int split = string.indexOf(HX_CSTRING(":"),null());		HX_STACK_VAR(split,"split");
					HX_STACK_LINE(618)
					::String _g2 = string.substring((int)0,split);		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(618)
					::String _g3 = string.substring((split + (int)1),string.length);		HX_STACK_VAR(_g3,"_g3");
					HX_STACK_LINE(618)
					::String temp_0 = _g2;		HX_STACK_VAR(temp_0,"temp_0");
					HX_STACK_LINE(618)
					::String temp_1 = _g3;		HX_STACK_VAR(temp_1,"temp_1");
					HX_STACK_LINE(619)
					returnMap->set(temp_0,temp_1);
				}
			}
		}
		HX_STACK_LINE(629)
		if ((::flixel::addons::api::FlxGameJolt_obj::_getImage)){
			HX_STACK_LINE(630)
			::flixel::addons::api::FlxGameJolt_obj::retrieveImage(returnMap);
			HX_STACK_LINE(631)
			return null();
		}
		HX_STACK_LINE(634)
		if (((bool((::flixel::addons::api::FlxGameJolt_obj::_callBack != null())) && bool(!(::flixel::addons::api::FlxGameJolt_obj::_verifyAuth))))){
			HX_STACK_LINE(635)
			::flixel::addons::api::FlxGameJolt_obj::_callBack(returnMap);
		}
		else{
			HX_STACK_LINE(636)
			if ((::flixel::addons::api::FlxGameJolt_obj::_verifyAuth)){
				HX_STACK_LINE(637)
				::flixel::addons::api::FlxGameJolt_obj::verifyAuthentication(returnMap);
			}
		}
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(FlxGameJolt_obj,parseData,(void))

Void FlxGameJolt_obj::verifyAuthentication( ::haxe::ds::StringMap ReturnMap){
{
		HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","verifyAuthentication",0xb6708ceb,"flixel.addons.api.FlxGameJolt.verifyAuthentication","flixel/addons/api/FlxGameJolt.hx",647,0x83b7688b)
		HX_STACK_ARG(ReturnMap,"ReturnMap")
		struct _Function_1_1{
			inline static bool Block( ::haxe::ds::StringMap &ReturnMap){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","flixel/addons/api/FlxGameJolt.hx",648,0x83b7688b)
				{
					HX_STACK_LINE(648)
					::String _g = ReturnMap->get(HX_CSTRING("success"));		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(648)
					return (_g == HX_CSTRING("true"));
				}
				return null();
			}
		};
		HX_STACK_LINE(648)
		if (((  ((ReturnMap->exists(HX_CSTRING("success")))) ? bool(_Function_1_1::Block(ReturnMap)) : bool(false) ))){
			HX_STACK_LINE(649)
			::flixel::addons::api::FlxGameJolt_obj::_initialized = true;
		}
		else{
			HX_STACK_LINE(651)
			::flixel::addons::api::FlxGameJolt_obj::_userName = null();
			HX_STACK_LINE(652)
			::flixel::addons::api::FlxGameJolt_obj::_userToken = null();
		}
		HX_STACK_LINE(655)
		::flixel::addons::api::FlxGameJolt_obj::_verifyAuth = false;
		HX_STACK_LINE(657)
		if (((::flixel::addons::api::FlxGameJolt_obj::_callBack != null()))){
			HX_STACK_LINE(658)
			::flixel::addons::api::FlxGameJolt_obj::_callBack(::flixel::addons::api::FlxGameJolt_obj::_initialized);
		}
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(FlxGameJolt_obj,verifyAuthentication,(void))

Void FlxGameJolt_obj::resetUser( ::String UserName,::String UserToken,Dynamic Callback){
{
		HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","resetUser",0x9004af60,"flixel.addons.api.FlxGameJolt.resetUser","flixel/addons/api/FlxGameJolt.hx",671,0x83b7688b)
		HX_STACK_ARG(UserName,"UserName")
		HX_STACK_ARG(UserToken,"UserToken")
		HX_STACK_ARG(Callback,"Callback")
		HX_STACK_LINE(672)
		::flixel::addons::api::FlxGameJolt_obj::_userName = UserName;
		HX_STACK_LINE(673)
		::flixel::addons::api::FlxGameJolt_obj::_userToken = UserToken;
		HX_STACK_LINE(675)
		::flixel::addons::api::FlxGameJolt_obj::_idURL = (((((HX_CSTRING("&game_id=") + ::flixel::addons::api::FlxGameJolt_obj::_gameID) + HX_CSTRING("&username=")) + ::flixel::addons::api::FlxGameJolt_obj::_userName) + HX_CSTRING("&user_token=")) + ::flixel::addons::api::FlxGameJolt_obj::_userToken);
		HX_STACK_LINE(676)
		::flixel::addons::api::FlxGameJolt_obj::_verifyAuth = true;
		HX_STACK_LINE(677)
		::flixel::addons::api::FlxGameJolt_obj::sendLoaderRequest((HX_CSTRING("http://gamejolt.com/api/game/v1/users/auth/?format=keypair") + ::flixel::addons::api::FlxGameJolt_obj::_idURL),Callback);
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC3(FlxGameJolt_obj,resetUser,(void))

Void FlxGameJolt_obj::fetchTrophyImage( int ID,Dynamic Callback){
{
		HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","fetchTrophyImage",0x13b90d4b,"flixel.addons.api.FlxGameJolt.fetchTrophyImage","flixel/addons/api/FlxGameJolt.hx",688,0x83b7688b)
		HX_STACK_ARG(ID,"ID")
		HX_STACK_ARG(Callback,"Callback")
		HX_STACK_LINE(689)
		if ((!(::flixel::addons::api::FlxGameJolt_obj::get_gameInit()))){
			HX_STACK_LINE(689)
			return null();
		}
		HX_STACK_LINE(690)
		::flixel::addons::api::FlxGameJolt_obj::_getImage = true;
		HX_STACK_LINE(691)
		::flixel::addons::api::FlxGameJolt_obj::fetchTrophy(ID,Callback);
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC2(FlxGameJolt_obj,fetchTrophyImage,(void))

Void FlxGameJolt_obj::fetchAvatarImage( Dynamic Callback){
{
		HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","fetchAvatarImage",0xf0613b82,"flixel.addons.api.FlxGameJolt.fetchAvatarImage","flixel/addons/api/FlxGameJolt.hx",702,0x83b7688b)
		HX_STACK_ARG(Callback,"Callback")
		HX_STACK_LINE(703)
		if ((!(::flixel::addons::api::FlxGameJolt_obj::get_gameInit()))){
			HX_STACK_LINE(703)
			return null();
		}
		HX_STACK_LINE(704)
		::flixel::addons::api::FlxGameJolt_obj::_getImage = true;
		HX_STACK_LINE(705)
		::flixel::addons::api::FlxGameJolt_obj::fetchUser((int)0,::flixel::addons::api::FlxGameJolt_obj::_userName,null(),Callback);
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(FlxGameJolt_obj,fetchAvatarImage,(void))

Void FlxGameJolt_obj::retrieveImage( ::haxe::ds::StringMap ImageMap){
{
		HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","retrieveImage",0x3916d1e5,"flixel.addons.api.FlxGameJolt.retrieveImage","flixel/addons/api/FlxGameJolt.hx",713,0x83b7688b)
		HX_STACK_ARG(ImageMap,"ImageMap")
		HX_STACK_LINE(713)
		if ((ImageMap->exists(HX_CSTRING("image_url")))){
			HX_STACK_LINE(714)
			::String _g = ImageMap->get(HX_CSTRING("image_url"));		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(714)
			::openfl::_v2::net::URLRequest request = ::openfl::_v2::net::URLRequest_obj::__new(_g);		HX_STACK_VAR(request,"request");
			HX_STACK_LINE(715)
			::openfl::_v2::display::Loader loader = ::openfl::_v2::display::Loader_obj::__new();		HX_STACK_VAR(loader,"loader");
			HX_STACK_LINE(716)
			loader->contentLoaderInfo->addEventListener(::openfl::_v2::events::Event_obj::COMPLETE,::flixel::addons::api::FlxGameJolt_obj::returnImage_dyn(),null(),null(),null());
			HX_STACK_LINE(717)
			loader->load(request,null());
		}
		else{
			HX_STACK_LINE(718)
			if ((ImageMap->exists(HX_CSTRING("avatar_url")))){
				HX_STACK_LINE(719)
				::String _g1 = ImageMap->get(HX_CSTRING("avatar_url"));		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(719)
				::openfl::_v2::net::URLRequest request = ::openfl::_v2::net::URLRequest_obj::__new(_g1);		HX_STACK_VAR(request,"request");
				HX_STACK_LINE(720)
				::openfl::_v2::display::Loader loader = ::openfl::_v2::display::Loader_obj::__new();		HX_STACK_VAR(loader,"loader");
				HX_STACK_LINE(721)
				loader->contentLoaderInfo->addEventListener(::openfl::_v2::events::Event_obj::COMPLETE,::flixel::addons::api::FlxGameJolt_obj::returnImage_dyn(),null(),null(),null());
				HX_STACK_LINE(722)
				loader->load(request,null());
			}
			else{
			}
		}
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(FlxGameJolt_obj,retrieveImage,(void))

Void FlxGameJolt_obj::returnImage( ::openfl::_v2::events::Event e){
{
		HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","returnImage",0x0659b5b1,"flixel.addons.api.FlxGameJolt.returnImage","flixel/addons/api/FlxGameJolt.hx",734,0x83b7688b)
		HX_STACK_ARG(e,"e")
		HX_STACK_LINE(735)
		if (((::flixel::addons::api::FlxGameJolt_obj::_callBack != null()))){
			HX_STACK_LINE(736)
			::flixel::addons::api::FlxGameJolt_obj::_callBack(e->get_currentTarget()->__Field(HX_CSTRING("content"),true)->__Field(HX_CSTRING("bitmapData"),true));
		}
		HX_STACK_LINE(739)
		::flixel::addons::api::FlxGameJolt_obj::_getImage = false;
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(FlxGameJolt_obj,returnImage,(void))

::String FlxGameJolt_obj::encryptURL( ::String Url){
	HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","encryptURL",0x3070b3a4,"flixel.addons.api.FlxGameJolt.encryptURL","flixel/addons/api/FlxGameJolt.hx",751,0x83b7688b)
	HX_STACK_ARG(Url,"Url")
	HX_STACK_LINE(751)
	if (((::flixel::addons::api::FlxGameJolt_obj::hashType == (int)1))){
		HX_STACK_LINE(752)
		return ::haxe::crypto::Sha1_obj::encode((Url + ::flixel::addons::api::FlxGameJolt_obj::_privateKey));
	}
	else{
		HX_STACK_LINE(754)
		return ::haxe::crypto::Md5_obj::encode((Url + ::flixel::addons::api::FlxGameJolt_obj::_privateKey));
	}
	HX_STACK_LINE(751)
	return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(FlxGameJolt_obj,encryptURL,return )

bool FlxGameJolt_obj::gameInit;

bool FlxGameJolt_obj::get_gameInit( ){
	HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","get_gameInit",0xbc372845,"flixel.addons.api.FlxGameJolt.get_gameInit","flixel/addons/api/FlxGameJolt.hx",766,0x83b7688b)
	HX_STACK_LINE(766)
	if (((bool((::flixel::addons::api::FlxGameJolt_obj::_gameID == (int)0)) || bool((::flixel::addons::api::FlxGameJolt_obj::_privateKey == HX_CSTRING("")))))){
		HX_STACK_LINE(771)
		return false;
	}
	else{
		HX_STACK_LINE(773)
		return true;
	}
	HX_STACK_LINE(766)
	return false;
}


STATIC_HX_DEFINE_DYNAMIC_FUNC0(FlxGameJolt_obj,get_gameInit,return )

bool FlxGameJolt_obj::authenticated;

bool FlxGameJolt_obj::get_authenticated( ){
	HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","get_authenticated",0x77e5cfac,"flixel.addons.api.FlxGameJolt.get_authenticated","flixel/addons/api/FlxGameJolt.hx",784,0x83b7688b)
	HX_STACK_LINE(785)
	if ((!(::flixel::addons::api::FlxGameJolt_obj::get_gameInit()))){
		HX_STACK_LINE(785)
		return false;
	}
	HX_STACK_LINE(787)
	if ((!(::flixel::addons::api::FlxGameJolt_obj::_initialized))){
		HX_STACK_LINE(792)
		return false;
	}
	else{
		HX_STACK_LINE(794)
		return true;
	}
	HX_STACK_LINE(787)
	return false;
}


STATIC_HX_DEFINE_DYNAMIC_FUNC0(FlxGameJolt_obj,get_authenticated,return )

::String FlxGameJolt_obj::username;

::String FlxGameJolt_obj::get_username( ){
	HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","get_username",0x2b417239,"flixel.addons.api.FlxGameJolt.get_username","flixel/addons/api/FlxGameJolt.hx",805,0x83b7688b)
	HX_STACK_LINE(805)
	if (((bool((bool(!(::flixel::addons::api::FlxGameJolt_obj::_initialized)) || bool((::flixel::addons::api::FlxGameJolt_obj::_userName == null())))) || bool((::flixel::addons::api::FlxGameJolt_obj::_userName == HX_CSTRING("")))))){
		HX_STACK_LINE(806)
		return HX_CSTRING("No user");
	}
	else{
		HX_STACK_LINE(808)
		return ::flixel::addons::api::FlxGameJolt_obj::_userName;
	}
	HX_STACK_LINE(805)
	return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC0(FlxGameJolt_obj,get_username,return )

::String FlxGameJolt_obj::usertoken;

::String FlxGameJolt_obj::get_usertoken( ){
	HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","get_usertoken",0x2ba9734b,"flixel.addons.api.FlxGameJolt.get_usertoken","flixel/addons/api/FlxGameJolt.hx",820,0x83b7688b)
	HX_STACK_LINE(820)
	if (((bool((bool(!(::flixel::addons::api::FlxGameJolt_obj::_initialized)) || bool((::flixel::addons::api::FlxGameJolt_obj::_userToken == null())))) || bool((::flixel::addons::api::FlxGameJolt_obj::_userToken == HX_CSTRING("")))))){
		HX_STACK_LINE(821)
		return HX_CSTRING("No token");
	}
	else{
		HX_STACK_LINE(823)
		return ::flixel::addons::api::FlxGameJolt_obj::_userToken;
	}
	HX_STACK_LINE(820)
	return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC0(FlxGameJolt_obj,get_usertoken,return )

bool FlxGameJolt_obj::isQuickPlay;

bool FlxGameJolt_obj::get_isQuickPlay( ){
	HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","get_isQuickPlay",0x021c1df4,"flixel.addons.api.FlxGameJolt.get_isQuickPlay","flixel/addons/api/FlxGameJolt.hx",837,0x83b7688b)
	HX_STACK_LINE(837)
	return false;
}


STATIC_HX_DEFINE_DYNAMIC_FUNC0(FlxGameJolt_obj,get_isQuickPlay,return )

bool FlxGameJolt_obj::isEmbeddedFlash;

bool FlxGameJolt_obj::get_isEmbeddedFlash( ){
	HX_STACK_FRAME("flixel.addons.api.FlxGameJolt","get_isEmbeddedFlash",0xb1c32fb9,"flixel.addons.api.FlxGameJolt.get_isEmbeddedFlash","flixel/addons/api/FlxGameJolt.hx",864,0x83b7688b)
	HX_STACK_LINE(864)
	return false;
}


STATIC_HX_DEFINE_DYNAMIC_FUNC0(FlxGameJolt_obj,get_isEmbeddedFlash,return )


FlxGameJolt_obj::FlxGameJolt_obj()
{
}

Dynamic FlxGameJolt_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"init") ) { return init_dyn(); }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"_idURL") ) { return _idURL; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"verbose") ) { return verbose; }
		if (HX_FIELD_EQ(inName,"_gameID") ) { return _gameID; }
		if (HX_FIELD_EQ(inName,"_loader") ) { return _loader; }
		if (HX_FIELD_EQ(inName,"setData") ) { return setData_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"hashType") ) { return hashType; }
		if (HX_FIELD_EQ(inName,"authUser") ) { return authUser_dyn(); }
		if (HX_FIELD_EQ(inName,"addScore") ) { return addScore_dyn(); }
		if (HX_FIELD_EQ(inName,"gameInit") ) { return inCallProp ? get_gameInit() : gameInit; }
		if (HX_FIELD_EQ(inName,"username") ) { return inCallProp ? get_username() : username; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"_callBack") ) { return _callBack; }
		if (HX_FIELD_EQ(inName,"_userName") ) { return _userName; }
		if (HX_FIELD_EQ(inName,"_getImage") ) { return _getImage; }
		if (HX_FIELD_EQ(inName,"fetchUser") ) { return fetchUser_dyn(); }
		if (HX_FIELD_EQ(inName,"addTrophy") ) { return addTrophy_dyn(); }
		if (HX_FIELD_EQ(inName,"getTables") ) { return getTables_dyn(); }
		if (HX_FIELD_EQ(inName,"fetchData") ) { return fetchData_dyn(); }
		if (HX_FIELD_EQ(inName,"parseData") ) { return parseData_dyn(); }
		if (HX_FIELD_EQ(inName,"resetUser") ) { return resetUser_dyn(); }
		if (HX_FIELD_EQ(inName,"usertoken") ) { return inCallProp ? get_usertoken() : usertoken; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"_userToken") ) { return _userToken; }
		if (HX_FIELD_EQ(inName,"fetchScore") ) { return fetchScore_dyn(); }
		if (HX_FIELD_EQ(inName,"updateData") ) { return updateData_dyn(); }
		if (HX_FIELD_EQ(inName,"removeData") ) { return removeData_dyn(); }
		if (HX_FIELD_EQ(inName,"getAllKeys") ) { return getAllKeys_dyn(); }
		if (HX_FIELD_EQ(inName,"encryptURL") ) { return encryptURL_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"initialized") ) { return inCallProp ? get_initialized() : initialized; }
		if (HX_FIELD_EQ(inName,"_privateKey") ) { return _privateKey; }
		if (HX_FIELD_EQ(inName,"_verifyAuth") ) { return _verifyAuth; }
		if (HX_FIELD_EQ(inName,"openSession") ) { return openSession_dyn(); }
		if (HX_FIELD_EQ(inName,"pingSession") ) { return pingSession_dyn(); }
		if (HX_FIELD_EQ(inName,"fetchTrophy") ) { return fetchTrophy_dyn(); }
		if (HX_FIELD_EQ(inName,"returnImage") ) { return returnImage_dyn(); }
		if (HX_FIELD_EQ(inName,"isQuickPlay") ) { return inCallProp ? get_isQuickPlay() : isQuickPlay; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"_initialized") ) { return _initialized; }
		if (HX_FIELD_EQ(inName,"closeSession") ) { return closeSession_dyn(); }
		if (HX_FIELD_EQ(inName,"get_gameInit") ) { return get_gameInit_dyn(); }
		if (HX_FIELD_EQ(inName,"get_username") ) { return get_username_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"retrieveImage") ) { return retrieveImage_dyn(); }
		if (HX_FIELD_EQ(inName,"authenticated") ) { return inCallProp ? get_authenticated() : authenticated; }
		if (HX_FIELD_EQ(inName,"get_usertoken") ) { return get_usertoken_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"get_initialized") ) { return get_initialized_dyn(); }
		if (HX_FIELD_EQ(inName,"get_isQuickPlay") ) { return get_isQuickPlay_dyn(); }
		if (HX_FIELD_EQ(inName,"isEmbeddedFlash") ) { return inCallProp ? get_isEmbeddedFlash() : isEmbeddedFlash; }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"fetchTrophyImage") ) { return fetchTrophyImage_dyn(); }
		if (HX_FIELD_EQ(inName,"fetchAvatarImage") ) { return fetchAvatarImage_dyn(); }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"sendLoaderRequest") ) { return sendLoaderRequest_dyn(); }
		if (HX_FIELD_EQ(inName,"get_authenticated") ) { return get_authenticated_dyn(); }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"get_isEmbeddedFlash") ) { return get_isEmbeddedFlash_dyn(); }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"verifyAuthentication") ) { return verifyAuthentication_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic FlxGameJolt_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"_idURL") ) { _idURL=inValue.Cast< ::String >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"verbose") ) { verbose=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"_gameID") ) { _gameID=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"_loader") ) { _loader=inValue.Cast< ::openfl::_v2::net::URLLoader >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"hashType") ) { hashType=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"gameInit") ) { gameInit=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"username") ) { username=inValue.Cast< ::String >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"_callBack") ) { _callBack=inValue.Cast< Dynamic >(); return inValue; }
		if (HX_FIELD_EQ(inName,"_userName") ) { _userName=inValue.Cast< ::String >(); return inValue; }
		if (HX_FIELD_EQ(inName,"_getImage") ) { _getImage=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"usertoken") ) { usertoken=inValue.Cast< ::String >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"_userToken") ) { _userToken=inValue.Cast< ::String >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"initialized") ) { initialized=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"_privateKey") ) { _privateKey=inValue.Cast< ::String >(); return inValue; }
		if (HX_FIELD_EQ(inName,"_verifyAuth") ) { _verifyAuth=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"isQuickPlay") ) { isQuickPlay=inValue.Cast< bool >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"_initialized") ) { _initialized=inValue.Cast< bool >(); return inValue; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"authenticated") ) { authenticated=inValue.Cast< bool >(); return inValue; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"isEmbeddedFlash") ) { isEmbeddedFlash=inValue.Cast< bool >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void FlxGameJolt_obj::__GetFields(Array< ::String> &outFields)
{
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("hashType"),
	HX_CSTRING("verbose"),
	HX_CSTRING("initialized"),
	HX_CSTRING("get_initialized"),
	HX_CSTRING("HASH_MD5"),
	HX_CSTRING("HASH_SHA1"),
	HX_CSTRING("TROPHIES_MISSING"),
	HX_CSTRING("TROPHIES_ACHIEVED"),
	HX_CSTRING("_callBack"),
	HX_CSTRING("_gameID"),
	HX_CSTRING("_privateKey"),
	HX_CSTRING("_userName"),
	HX_CSTRING("_userToken"),
	HX_CSTRING("_idURL"),
	HX_CSTRING("_initialized"),
	HX_CSTRING("_verifyAuth"),
	HX_CSTRING("_getImage"),
	HX_CSTRING("_loader"),
	HX_CSTRING("URL_API"),
	HX_CSTRING("RETURN_TYPE"),
	HX_CSTRING("URL_GAME_ID"),
	HX_CSTRING("URL_USER_NAME"),
	HX_CSTRING("URL_USER_TOKEN"),
	HX_CSTRING("init"),
	HX_CSTRING("fetchUser"),
	HX_CSTRING("authUser"),
	HX_CSTRING("openSession"),
	HX_CSTRING("pingSession"),
	HX_CSTRING("closeSession"),
	HX_CSTRING("fetchTrophy"),
	HX_CSTRING("addTrophy"),
	HX_CSTRING("fetchScore"),
	HX_CSTRING("addScore"),
	HX_CSTRING("getTables"),
	HX_CSTRING("fetchData"),
	HX_CSTRING("setData"),
	HX_CSTRING("updateData"),
	HX_CSTRING("removeData"),
	HX_CSTRING("getAllKeys"),
	HX_CSTRING("sendLoaderRequest"),
	HX_CSTRING("parseData"),
	HX_CSTRING("verifyAuthentication"),
	HX_CSTRING("resetUser"),
	HX_CSTRING("fetchTrophyImage"),
	HX_CSTRING("fetchAvatarImage"),
	HX_CSTRING("retrieveImage"),
	HX_CSTRING("returnImage"),
	HX_CSTRING("encryptURL"),
	HX_CSTRING("gameInit"),
	HX_CSTRING("get_gameInit"),
	HX_CSTRING("authenticated"),
	HX_CSTRING("get_authenticated"),
	HX_CSTRING("username"),
	HX_CSTRING("get_username"),
	HX_CSTRING("usertoken"),
	HX_CSTRING("get_usertoken"),
	HX_CSTRING("isQuickPlay"),
	HX_CSTRING("get_isQuickPlay"),
	HX_CSTRING("isEmbeddedFlash"),
	HX_CSTRING("get_isEmbeddedFlash"),
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo *sMemberStorageInfo = 0;
#endif

static ::String sMemberFields[] = {
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::hashType,"hashType");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::verbose,"verbose");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::initialized,"initialized");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::HASH_MD5,"HASH_MD5");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::HASH_SHA1,"HASH_SHA1");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::TROPHIES_MISSING,"TROPHIES_MISSING");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::TROPHIES_ACHIEVED,"TROPHIES_ACHIEVED");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::_callBack,"_callBack");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::_gameID,"_gameID");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::_privateKey,"_privateKey");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::_userName,"_userName");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::_userToken,"_userToken");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::_idURL,"_idURL");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::_initialized,"_initialized");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::_verifyAuth,"_verifyAuth");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::_getImage,"_getImage");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::_loader,"_loader");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::URL_API,"URL_API");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::RETURN_TYPE,"RETURN_TYPE");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::URL_GAME_ID,"URL_GAME_ID");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::URL_USER_NAME,"URL_USER_NAME");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::URL_USER_TOKEN,"URL_USER_TOKEN");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::gameInit,"gameInit");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::authenticated,"authenticated");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::username,"username");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::usertoken,"usertoken");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::isQuickPlay,"isQuickPlay");
	HX_MARK_MEMBER_NAME(FlxGameJolt_obj::isEmbeddedFlash,"isEmbeddedFlash");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::hashType,"hashType");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::verbose,"verbose");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::initialized,"initialized");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::HASH_MD5,"HASH_MD5");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::HASH_SHA1,"HASH_SHA1");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::TROPHIES_MISSING,"TROPHIES_MISSING");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::TROPHIES_ACHIEVED,"TROPHIES_ACHIEVED");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::_callBack,"_callBack");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::_gameID,"_gameID");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::_privateKey,"_privateKey");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::_userName,"_userName");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::_userToken,"_userToken");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::_idURL,"_idURL");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::_initialized,"_initialized");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::_verifyAuth,"_verifyAuth");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::_getImage,"_getImage");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::_loader,"_loader");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::URL_API,"URL_API");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::RETURN_TYPE,"RETURN_TYPE");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::URL_GAME_ID,"URL_GAME_ID");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::URL_USER_NAME,"URL_USER_NAME");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::URL_USER_TOKEN,"URL_USER_TOKEN");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::gameInit,"gameInit");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::authenticated,"authenticated");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::username,"username");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::usertoken,"usertoken");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::isQuickPlay,"isQuickPlay");
	HX_VISIT_MEMBER_NAME(FlxGameJolt_obj::isEmbeddedFlash,"isEmbeddedFlash");
};

#endif

Class FlxGameJolt_obj::__mClass;

void FlxGameJolt_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("flixel.addons.api.FlxGameJolt"), hx::TCanCast< FlxGameJolt_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void FlxGameJolt_obj::__boot()
{
	hashType= (int)0;
	verbose= false;
	HASH_MD5= (int)0;
	HASH_SHA1= (int)1;
	TROPHIES_MISSING= (int)-1;
	TROPHIES_ACHIEVED= (int)-2;
	_gameID= (int)0;
	_privateKey= HX_CSTRING("");
	_initialized= false;
	_verifyAuth= false;
	_getImage= false;
	URL_API= HX_CSTRING("http://gamejolt.com/api/game/v1/");
	RETURN_TYPE= HX_CSTRING("?format=keypair");
	URL_GAME_ID= HX_CSTRING("&game_id=");
	URL_USER_NAME= HX_CSTRING("&username=");
	URL_USER_TOKEN= HX_CSTRING("&user_token=");
}

} // end namespace flixel
} // end namespace addons
} // end namespace api
