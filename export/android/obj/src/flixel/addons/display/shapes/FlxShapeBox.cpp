#include <hxcpp.h>

#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_addons_display_shapes_FlxShape
#include <flixel/addons/display/shapes/FlxShape.h>
#endif
#ifndef INCLUDED_flixel_addons_display_shapes_FlxShapeBox
#include <flixel/addons/display/shapes/FlxShapeBox.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_util_FlxSpriteUtil
#include <flixel/util/FlxSpriteUtil.h>
#endif
#ifndef INCLUDED_openfl__v2_geom_Matrix
#include <openfl/_v2/geom/Matrix.h>
#endif
namespace flixel{
namespace addons{
namespace display{
namespace shapes{

Void FlxShapeBox_obj::__construct(Float X,Float Y,Float W,Float H,Dynamic LineStyle_,Dynamic FillStyle_)
{
HX_STACK_FRAME("flixel.addons.display.shapes.FlxShapeBox","new",0xcf34fac7,"flixel.addons.display.shapes.FlxShapeBox.new","flixel/addons/display/shapes/FlxShapeBox.hx",20,0x82ef1e89)
HX_STACK_THIS(this)
HX_STACK_ARG(X,"X")
HX_STACK_ARG(Y,"Y")
HX_STACK_ARG(W,"W")
HX_STACK_ARG(H,"H")
HX_STACK_ARG(LineStyle_,"LineStyle_")
HX_STACK_ARG(FillStyle_,"FillStyle_")
{
	HX_STACK_LINE(21)
	this->shape_id = HX_CSTRING("box");
	HX_STACK_LINE(23)
	Float strokeBuffer = LineStyle_->__Field(HX_CSTRING("thickness"),true);		HX_STACK_VAR(strokeBuffer,"strokeBuffer");
	HX_STACK_LINE(24)
	{
		HX_STACK_LINE(24)
		this->shapeWidth = W;
		HX_STACK_LINE(24)
		this->shapeDirty = true;
		HX_STACK_LINE(24)
		this->shapeWidth;
	}
	HX_STACK_LINE(25)
	{
		HX_STACK_LINE(25)
		this->shapeHeight = H;
		HX_STACK_LINE(25)
		this->shapeDirty = true;
		HX_STACK_LINE(25)
		this->shapeHeight;
	}
	HX_STACK_LINE(27)
	Float w = (this->shapeWidth + strokeBuffer);		HX_STACK_VAR(w,"w");
	HX_STACK_LINE(28)
	Float h = (this->shapeHeight + strokeBuffer);		HX_STACK_VAR(h,"h");
	HX_STACK_LINE(30)
	if (((w <= (int)0))){
		HX_STACK_LINE(31)
		w = strokeBuffer;
	}
	HX_STACK_LINE(32)
	if (((h <= (int)0))){
		HX_STACK_LINE(33)
		h = strokeBuffer;
	}
	HX_STACK_LINE(35)
	super::__construct(X,Y,w,h,LineStyle_,FillStyle_,this->shapeWidth,this->shapeHeight);
}
;
	return null();
}

//FlxShapeBox_obj::~FlxShapeBox_obj() { }

Dynamic FlxShapeBox_obj::__CreateEmpty() { return  new FlxShapeBox_obj; }
hx::ObjectPtr< FlxShapeBox_obj > FlxShapeBox_obj::__new(Float X,Float Y,Float W,Float H,Dynamic LineStyle_,Dynamic FillStyle_)
{  hx::ObjectPtr< FlxShapeBox_obj > result = new FlxShapeBox_obj();
	result->__construct(X,Y,W,H,LineStyle_,FillStyle_);
	return result;}

Dynamic FlxShapeBox_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< FlxShapeBox_obj > result = new FlxShapeBox_obj();
	result->__construct(inArgs[0],inArgs[1],inArgs[2],inArgs[3],inArgs[4],inArgs[5]);
	return result;}

Void FlxShapeBox_obj::drawSpecificShape( ::openfl::_v2::geom::Matrix matrix){
{
		HX_STACK_FRAME("flixel.addons.display.shapes.FlxShapeBox","drawSpecificShape",0x546d2f12,"flixel.addons.display.shapes.FlxShapeBox.drawSpecificShape","flixel/addons/display/shapes/FlxShapeBox.hx",40,0x82ef1e89)
		HX_STACK_THIS(this)
		HX_STACK_ARG(matrix,"matrix")
		HX_STACK_LINE(40)
		::flixel::util::FlxSpriteUtil_obj::drawRect(hx::ObjectPtr<OBJ_>(this),(Float(this->lineStyle->__Field(HX_CSTRING("thickness"),true)) / Float((int)2)),(Float(this->lineStyle->__Field(HX_CSTRING("thickness"),true)) / Float((int)2)),this->shapeWidth,this->shapeHeight,this->fillStyle->__Field(HX_CSTRING("color"),true),this->lineStyle,null(),null());
	}
return null();
}


Float FlxShapeBox_obj::set_shapeWidth( Float f){
	HX_STACK_FRAME("flixel.addons.display.shapes.FlxShapeBox","set_shapeWidth",0x6d78ecdb,"flixel.addons.display.shapes.FlxShapeBox.set_shapeWidth","flixel/addons/display/shapes/FlxShapeBox.hx",44,0x82ef1e89)
	HX_STACK_THIS(this)
	HX_STACK_ARG(f,"f")
	HX_STACK_LINE(45)
	this->shapeWidth = f;
	HX_STACK_LINE(46)
	this->shapeDirty = true;
	HX_STACK_LINE(47)
	return this->shapeWidth;
}


HX_DEFINE_DYNAMIC_FUNC1(FlxShapeBox_obj,set_shapeWidth,return )

Float FlxShapeBox_obj::set_shapeHeight( Float f){
	HX_STACK_FRAME("flixel.addons.display.shapes.FlxShapeBox","set_shapeHeight",0x12a5cb72,"flixel.addons.display.shapes.FlxShapeBox.set_shapeHeight","flixel/addons/display/shapes/FlxShapeBox.hx",51,0x82ef1e89)
	HX_STACK_THIS(this)
	HX_STACK_ARG(f,"f")
	HX_STACK_LINE(52)
	this->shapeHeight = f;
	HX_STACK_LINE(53)
	this->shapeDirty = true;
	HX_STACK_LINE(54)
	return this->shapeHeight;
}


HX_DEFINE_DYNAMIC_FUNC1(FlxShapeBox_obj,set_shapeHeight,return )


FlxShapeBox_obj::FlxShapeBox_obj()
{
}

Dynamic FlxShapeBox_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 10:
		if (HX_FIELD_EQ(inName,"shapeWidth") ) { return shapeWidth; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"shapeHeight") ) { return shapeHeight; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"set_shapeWidth") ) { return set_shapeWidth_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"set_shapeHeight") ) { return set_shapeHeight_dyn(); }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"drawSpecificShape") ) { return drawSpecificShape_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic FlxShapeBox_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 10:
		if (HX_FIELD_EQ(inName,"shapeWidth") ) { if (inCallProp) return set_shapeWidth(inValue);shapeWidth=inValue.Cast< Float >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"shapeHeight") ) { if (inCallProp) return set_shapeHeight(inValue);shapeHeight=inValue.Cast< Float >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void FlxShapeBox_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("shapeWidth"));
	outFields->push(HX_CSTRING("shapeHeight"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsFloat,(int)offsetof(FlxShapeBox_obj,shapeWidth),HX_CSTRING("shapeWidth")},
	{hx::fsFloat,(int)offsetof(FlxShapeBox_obj,shapeHeight),HX_CSTRING("shapeHeight")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("shapeWidth"),
	HX_CSTRING("shapeHeight"),
	HX_CSTRING("drawSpecificShape"),
	HX_CSTRING("set_shapeWidth"),
	HX_CSTRING("set_shapeHeight"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(FlxShapeBox_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(FlxShapeBox_obj::__mClass,"__mClass");
};

#endif

Class FlxShapeBox_obj::__mClass;

void FlxShapeBox_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("flixel.addons.display.shapes.FlxShapeBox"), hx::TCanCast< FlxShapeBox_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void FlxShapeBox_obj::__boot()
{
}

} // end namespace flixel
} // end namespace addons
} // end namespace display
} // end namespace shapes
