#include <hxcpp.h>

#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxCamera
#include <flixel/FlxCamera.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_addons_display_FlxSkewedSprite
#include <flixel/addons/display/FlxSkewedSprite.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxPooled
#include <flixel/interfaces/IFlxPooled.h>
#endif
#ifndef INCLUDED_flixel_system_layer_DrawStackItem
#include <flixel/system/layer/DrawStackItem.h>
#endif
#ifndef INCLUDED_flixel_system_layer_frames_FlxFrame
#include <flixel/system/layer/frames/FlxFrame.h>
#endif
#ifndef INCLUDED_flixel_util_FlxDestroyUtil
#include <flixel/util/FlxDestroyUtil.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPoint
#include <flixel/util/FlxPoint.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPool_flixel_util_FlxPoint
#include <flixel/util/FlxPool_flixel_util_FlxPoint.h>
#endif
#ifndef INCLUDED_flixel_util_loaders_CachedGraphics
#include <flixel/util/loaders/CachedGraphics.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_openfl__v2_display_BlendMode
#include <openfl/_v2/display/BlendMode.h>
#endif
#ifndef INCLUDED_openfl__v2_geom_Matrix
#include <openfl/_v2/geom/Matrix.h>
#endif
namespace flixel{
namespace addons{
namespace display{

Void FlxSkewedSprite_obj::__construct(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y,Dynamic SimpleGraphic)
{
HX_STACK_FRAME("flixel.addons.display.FlxSkewedSprite","new",0x1b5fbddd,"flixel.addons.display.FlxSkewedSprite.new","flixel/addons/display/FlxSkewedSprite.hx",18,0xe6411a94)
HX_STACK_THIS(this)
HX_STACK_ARG(__o_X,"X")
HX_STACK_ARG(__o_Y,"Y")
HX_STACK_ARG(SimpleGraphic,"SimpleGraphic")
Float X = __o_X.Default(0);
Float Y = __o_Y.Default(0);
{
	HX_STACK_LINE(32)
	this->matrixExposed = false;
	HX_STACK_LINE(41)
	super::__construct(X,Y,SimpleGraphic);
	HX_STACK_LINE(43)
	::flixel::util::FlxPoint _g;		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(43)
	{
		HX_STACK_LINE(43)
		Float X1 = (int)0;		HX_STACK_VAR(X1,"X1");
		HX_STACK_LINE(43)
		Float Y1 = (int)0;		HX_STACK_VAR(Y1,"Y1");
		HX_STACK_LINE(43)
		::flixel::util::FlxPoint point = ::flixel::util::FlxPoint_obj::_pool->get()->set(X1,Y1);		HX_STACK_VAR(point,"point");
		HX_STACK_LINE(43)
		point->_inPool = false;
		HX_STACK_LINE(43)
		_g = point;
	}
	HX_STACK_LINE(43)
	this->skew = _g;
	HX_STACK_LINE(44)
	::openfl::_v2::geom::Matrix _g1 = ::openfl::_v2::geom::Matrix_obj::__new(null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
	HX_STACK_LINE(44)
	this->_skewMatrix = _g1;
	HX_STACK_LINE(45)
	::openfl::_v2::geom::Matrix _g2 = ::openfl::_v2::geom::Matrix_obj::__new(null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g2,"_g2");
	HX_STACK_LINE(45)
	this->transformMatrix = _g2;
}
;
	return null();
}

//FlxSkewedSprite_obj::~FlxSkewedSprite_obj() { }

Dynamic FlxSkewedSprite_obj::__CreateEmpty() { return  new FlxSkewedSprite_obj; }
hx::ObjectPtr< FlxSkewedSprite_obj > FlxSkewedSprite_obj::__new(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y,Dynamic SimpleGraphic)
{  hx::ObjectPtr< FlxSkewedSprite_obj > result = new FlxSkewedSprite_obj();
	result->__construct(__o_X,__o_Y,SimpleGraphic);
	return result;}

Dynamic FlxSkewedSprite_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< FlxSkewedSprite_obj > result = new FlxSkewedSprite_obj();
	result->__construct(inArgs[0],inArgs[1],inArgs[2]);
	return result;}

Void FlxSkewedSprite_obj::destroy( ){
{
		HX_STACK_FRAME("flixel.addons.display.FlxSkewedSprite","destroy",0x11a31cf7,"flixel.addons.display.FlxSkewedSprite.destroy","flixel/addons/display/FlxSkewedSprite.hx",54,0xe6411a94)
		HX_STACK_THIS(this)
		HX_STACK_LINE(55)
		::flixel::util::FlxPoint _g = ::flixel::util::FlxDestroyUtil_obj::put(this->skew);		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(55)
		this->skew = _g;
		HX_STACK_LINE(56)
		this->_skewMatrix = null();
		HX_STACK_LINE(57)
		this->transformMatrix = null();
		HX_STACK_LINE(59)
		this->super::destroy();
	}
return null();
}


Void FlxSkewedSprite_obj::draw( ){
{
		HX_STACK_FRAME("flixel.addons.display.FlxSkewedSprite","draw",0xd1d40be7,"flixel.addons.display.FlxSkewedSprite.draw","flixel/addons/display/FlxSkewedSprite.hx",63,0xe6411a94)
		HX_STACK_THIS(this)
		HX_STACK_LINE(64)
		if ((this->dirty)){
			HX_STACK_LINE(66)
			this->calcFrame(null());
		}
		HX_STACK_LINE(70)
		::flixel::system::layer::DrawStackItem drawItem;		HX_STACK_VAR(drawItem,"drawItem");
		HX_STACK_LINE(71)
		Array< Float > currDrawData;		HX_STACK_VAR(currDrawData,"currDrawData");
		HX_STACK_LINE(72)
		int currIndex;		HX_STACK_VAR(currIndex,"currIndex");
		HX_STACK_LINE(75)
		Float radians;		HX_STACK_VAR(radians,"radians");
		HX_STACK_LINE(76)
		Float cos;		HX_STACK_VAR(cos,"cos");
		HX_STACK_LINE(77)
		Float sin;		HX_STACK_VAR(sin,"sin");
		HX_STACK_LINE(79)
		{
			HX_STACK_LINE(79)
			int _g = (int)0;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(79)
			Array< ::Dynamic > _g1 = this->get_cameras();		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(79)
			while((true)){
				HX_STACK_LINE(79)
				if ((!(((_g < _g1->length))))){
					HX_STACK_LINE(79)
					break;
				}
				HX_STACK_LINE(79)
				::flixel::FlxCamera camera = _g1->__get(_g).StaticCast< ::flixel::FlxCamera >();		HX_STACK_VAR(camera,"camera");
				HX_STACK_LINE(79)
				++(_g);
				HX_STACK_LINE(81)
				if (((  ((!(((  ((!((!(this->isOnScreen(camera)))))) ? bool(!(camera->visible)) : bool(true) ))))) ? bool(!(camera->exists)) : bool(true) ))){
					HX_STACK_LINE(83)
					continue;
				}
				HX_STACK_LINE(87)
				::flixel::system::layer::DrawStackItem _g2 = camera->getDrawStackItem(this->cachedGraphics,this->isColored,this->_blendInt,this->antialiasing);		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(87)
				drawItem = _g2;
				HX_STACK_LINE(88)
				currDrawData = drawItem->drawData;
				HX_STACK_LINE(89)
				currIndex = drawItem->position;
				HX_STACK_LINE(91)
				this->_point->set_x(((this->x - (camera->scroll->x * this->scrollFactor->x)) - this->offset->x));
				HX_STACK_LINE(92)
				this->_point->set_y(((this->y - (camera->scroll->y * this->scrollFactor->y)) - this->offset->y));
				HX_STACK_LINE(94)
				this->_point->set_x((this->_point->x + this->origin->x));
				HX_STACK_LINE(95)
				this->_point->set_y((this->_point->y + this->origin->y));
				HX_STACK_LINE(127)
				Float csx = (int)1;		HX_STACK_VAR(csx,"csx");
				HX_STACK_LINE(128)
				Float ssy = (int)0;		HX_STACK_VAR(ssy,"ssy");
				HX_STACK_LINE(129)
				Float ssx = (int)0;		HX_STACK_VAR(ssx,"ssx");
				HX_STACK_LINE(130)
				Float csy = (int)1;		HX_STACK_VAR(csy,"csy");
				HX_STACK_LINE(132)
				Float x1 = (this->origin->x - this->frame->center->x);		HX_STACK_VAR(x1,"x1");
				HX_STACK_LINE(133)
				Float y1 = (this->origin->y - this->frame->center->y);		HX_STACK_VAR(y1,"y1");
				HX_STACK_LINE(135)
				Float x2 = x1;		HX_STACK_VAR(x2,"x2");
				HX_STACK_LINE(136)
				Float y2 = y1;		HX_STACK_VAR(y2,"y2");
				HX_STACK_LINE(138)
				Float sx = (this->scale->x * this->_facingHorizontalMult);		HX_STACK_VAR(sx,"sx");
				HX_STACK_LINE(139)
				Float sy = (this->scale->y * this->_facingVerticalMult);		HX_STACK_VAR(sy,"sy");
				HX_STACK_LINE(141)
				if ((this->isSimpleRender())){
					HX_STACK_LINE(143)
					if ((this->flipX)){
						HX_STACK_LINE(145)
						csx = -(csx);
					}
					HX_STACK_LINE(147)
					if ((this->flipY)){
						HX_STACK_LINE(149)
						csy = -(csy);
					}
				}
				else{
					HX_STACK_LINE(154)
					::openfl::_v2::geom::Matrix matrixToUse = this->_matrix;		HX_STACK_VAR(matrixToUse,"matrixToUse");
					HX_STACK_LINE(155)
					if ((!(this->matrixExposed))){
						HX_STACK_LINE(157)
						radians = (-(this->angle) * ((Float(::Math_obj::PI) / Float((int)180))));
						HX_STACK_LINE(159)
						this->_matrix->identity();
						HX_STACK_LINE(160)
						this->_matrix->rotate(-(radians));
						HX_STACK_LINE(161)
						this->_matrix->scale(sx,sy);
						HX_STACK_LINE(163)
						this->updateSkewMatrix();
					}
					else{
						HX_STACK_LINE(167)
						matrixToUse = this->transformMatrix;
					}
					HX_STACK_LINE(170)
					x2 = (((x1 * matrixToUse->a) + (y1 * matrixToUse->c)) + matrixToUse->tx);
					HX_STACK_LINE(171)
					y2 = (((x1 * matrixToUse->b) + (y1 * matrixToUse->d)) + matrixToUse->ty);
					HX_STACK_LINE(173)
					csx = matrixToUse->a;
					HX_STACK_LINE(174)
					ssy = matrixToUse->b;
					HX_STACK_LINE(175)
					ssx = matrixToUse->c;
					HX_STACK_LINE(176)
					csy = matrixToUse->d;
				}
				HX_STACK_LINE(179)
				int _g11 = (currIndex)++;		HX_STACK_VAR(_g11,"_g11");
				HX_STACK_LINE(179)
				currDrawData[_g11] = (this->_point->x - x2);
				HX_STACK_LINE(180)
				int _g21 = (currIndex)++;		HX_STACK_VAR(_g21,"_g21");
				HX_STACK_LINE(180)
				currDrawData[_g21] = (this->_point->y - y2);
				HX_STACK_LINE(182)
				int _g3 = (currIndex)++;		HX_STACK_VAR(_g3,"_g3");
				HX_STACK_LINE(182)
				currDrawData[_g3] = this->frame->tileID;
				HX_STACK_LINE(184)
				int _g4 = (currIndex)++;		HX_STACK_VAR(_g4,"_g4");
				HX_STACK_LINE(184)
				currDrawData[_g4] = csx;
				HX_STACK_LINE(185)
				int _g5 = (currIndex)++;		HX_STACK_VAR(_g5,"_g5");
				HX_STACK_LINE(185)
				currDrawData[_g5] = ssy;
				HX_STACK_LINE(186)
				int _g6 = (currIndex)++;		HX_STACK_VAR(_g6,"_g6");
				HX_STACK_LINE(186)
				currDrawData[_g6] = ssx;
				HX_STACK_LINE(187)
				int _g7 = (currIndex)++;		HX_STACK_VAR(_g7,"_g7");
				HX_STACK_LINE(187)
				currDrawData[_g7] = csy;
				HX_STACK_LINE(189)
				if ((this->isColored)){
					HX_STACK_LINE(191)
					int _g8 = (currIndex)++;		HX_STACK_VAR(_g8,"_g8");
					HX_STACK_LINE(191)
					currDrawData[_g8] = this->_red;
					HX_STACK_LINE(192)
					int _g9 = (currIndex)++;		HX_STACK_VAR(_g9,"_g9");
					HX_STACK_LINE(192)
					currDrawData[_g9] = this->_green;
					HX_STACK_LINE(193)
					int _g10 = (currIndex)++;		HX_STACK_VAR(_g10,"_g10");
					HX_STACK_LINE(193)
					currDrawData[_g10] = this->_blue;
				}
				HX_STACK_LINE(195)
				int _g111 = (currIndex)++;		HX_STACK_VAR(_g111,"_g111");
				HX_STACK_LINE(195)
				currDrawData[_g111] = this->alpha;
				HX_STACK_LINE(197)
				drawItem->position = currIndex;
			}
		}
	}
return null();
}


Void FlxSkewedSprite_obj::updateSkewMatrix( ){
{
		HX_STACK_FRAME("flixel.addons.display.FlxSkewedSprite","updateSkewMatrix",0xcaea18b7,"flixel.addons.display.FlxSkewedSprite.updateSkewMatrix","flixel/addons/display/FlxSkewedSprite.hx",207,0xe6411a94)
		HX_STACK_THIS(this)
		HX_STACK_LINE(207)
		if (((bool((this->skew->x != (int)0)) || bool((this->skew->y != (int)0))))){
			HX_STACK_LINE(209)
			this->_skewMatrix->identity();
			HX_STACK_LINE(211)
			Float _g = ::Math_obj::tan((this->skew->y * ((Float(::Math_obj::PI) / Float((int)180)))));		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(211)
			this->_skewMatrix->b = _g;
			HX_STACK_LINE(212)
			Float _g1 = ::Math_obj::tan((this->skew->x * ((Float(::Math_obj::PI) / Float((int)180)))));		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(212)
			this->_skewMatrix->c = _g1;
			HX_STACK_LINE(214)
			this->_matrix->concat(this->_skewMatrix);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(FlxSkewedSprite_obj,updateSkewMatrix,(void))

bool FlxSkewedSprite_obj::isSimpleRender( ){
	HX_STACK_FRAME("flixel.addons.display.FlxSkewedSprite","isSimpleRender",0x14f4add5,"flixel.addons.display.FlxSkewedSprite.isSimpleRender","flixel/addons/display/FlxSkewedSprite.hx",223,0xe6411a94)
	HX_STACK_THIS(this)
	HX_STACK_LINE(223)
	return (bool((bool((bool((bool((bool((bool(((bool((this->angle == (int)0)) || bool((this->bakedRotationAngle > (int)0))))) && bool((this->scale->x == (int)1)))) && bool((this->scale->y == (int)1)))) && bool((this->blend == null())))) && bool((this->skew->x == (int)0)))) && bool((this->skew->y == (int)0)))) && bool(this->pixelPerfectRender));
}



FlxSkewedSprite_obj::FlxSkewedSprite_obj()
{
}

void FlxSkewedSprite_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(FlxSkewedSprite);
	HX_MARK_MEMBER_NAME(skew,"skew");
	HX_MARK_MEMBER_NAME(transformMatrix,"transformMatrix");
	HX_MARK_MEMBER_NAME(matrixExposed,"matrixExposed");
	HX_MARK_MEMBER_NAME(_skewMatrix,"_skewMatrix");
	::flixel::FlxSprite_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void FlxSkewedSprite_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(skew,"skew");
	HX_VISIT_MEMBER_NAME(transformMatrix,"transformMatrix");
	HX_VISIT_MEMBER_NAME(matrixExposed,"matrixExposed");
	HX_VISIT_MEMBER_NAME(_skewMatrix,"_skewMatrix");
	::flixel::FlxSprite_obj::__Visit(HX_VISIT_ARG);
}

Dynamic FlxSkewedSprite_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"skew") ) { return skew; }
		if (HX_FIELD_EQ(inName,"draw") ) { return draw_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"_skewMatrix") ) { return _skewMatrix; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"matrixExposed") ) { return matrixExposed; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"isSimpleRender") ) { return isSimpleRender_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"transformMatrix") ) { return transformMatrix; }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"updateSkewMatrix") ) { return updateSkewMatrix_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic FlxSkewedSprite_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"skew") ) { skew=inValue.Cast< ::flixel::util::FlxPoint >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"_skewMatrix") ) { _skewMatrix=inValue.Cast< ::openfl::_v2::geom::Matrix >(); return inValue; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"matrixExposed") ) { matrixExposed=inValue.Cast< bool >(); return inValue; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"transformMatrix") ) { transformMatrix=inValue.Cast< ::openfl::_v2::geom::Matrix >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void FlxSkewedSprite_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("skew"));
	outFields->push(HX_CSTRING("transformMatrix"));
	outFields->push(HX_CSTRING("matrixExposed"));
	outFields->push(HX_CSTRING("_skewMatrix"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::util::FlxPoint*/ ,(int)offsetof(FlxSkewedSprite_obj,skew),HX_CSTRING("skew")},
	{hx::fsObject /*::openfl::_v2::geom::Matrix*/ ,(int)offsetof(FlxSkewedSprite_obj,transformMatrix),HX_CSTRING("transformMatrix")},
	{hx::fsBool,(int)offsetof(FlxSkewedSprite_obj,matrixExposed),HX_CSTRING("matrixExposed")},
	{hx::fsObject /*::openfl::_v2::geom::Matrix*/ ,(int)offsetof(FlxSkewedSprite_obj,_skewMatrix),HX_CSTRING("_skewMatrix")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("skew"),
	HX_CSTRING("transformMatrix"),
	HX_CSTRING("matrixExposed"),
	HX_CSTRING("_skewMatrix"),
	HX_CSTRING("destroy"),
	HX_CSTRING("draw"),
	HX_CSTRING("updateSkewMatrix"),
	HX_CSTRING("isSimpleRender"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(FlxSkewedSprite_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(FlxSkewedSprite_obj::__mClass,"__mClass");
};

#endif

Class FlxSkewedSprite_obj::__mClass;

void FlxSkewedSprite_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("flixel.addons.display.FlxSkewedSprite"), hx::TCanCast< FlxSkewedSprite_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void FlxSkewedSprite_obj::__boot()
{
}

} // end namespace flixel
} // end namespace addons
} // end namespace display
