#include <hxcpp.h>

#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_plugin__MouseEventManager_ObjectMouseData
#include <flixel/plugin/_MouseEventManager/ObjectMouseData.h>
#endif
namespace flixel{
namespace plugin{
namespace _MouseEventManager{

Void ObjectMouseData_obj::__construct(Dynamic object,Dynamic onMouseDown,Dynamic onMouseUp,Dynamic onMouseOver,Dynamic onMouseOut,bool mouseChildren,bool mouseEnabled,bool pixelPerfect)
{
HX_STACK_FRAME("flixel.plugin._MouseEventManager.ObjectMouseData","new",0xb9603eb6,"flixel.plugin._MouseEventManager.ObjectMouseData.new","flixel/plugin/MouseEventManager.hx",464,0x4767214f)
HX_STACK_THIS(this)
HX_STACK_ARG(object,"object")
HX_STACK_ARG(onMouseDown,"onMouseDown")
HX_STACK_ARG(onMouseUp,"onMouseUp")
HX_STACK_ARG(onMouseOver,"onMouseOver")
HX_STACK_ARG(onMouseOut,"onMouseOut")
HX_STACK_ARG(mouseChildren,"mouseChildren")
HX_STACK_ARG(mouseEnabled,"mouseEnabled")
HX_STACK_ARG(pixelPerfect,"pixelPerfect")
{
	HX_STACK_LINE(465)
	this->object = object;
	HX_STACK_LINE(466)
	this->onMouseDown = onMouseDown;
	HX_STACK_LINE(467)
	this->onMouseUp = onMouseUp;
	HX_STACK_LINE(468)
	this->onMouseOver = onMouseOver;
	HX_STACK_LINE(469)
	this->onMouseOut = onMouseOut;
	HX_STACK_LINE(470)
	this->mouseChildren = mouseChildren;
	HX_STACK_LINE(471)
	this->mouseEnabled = mouseEnabled;
	HX_STACK_LINE(472)
	this->pixelPerfect = pixelPerfect;
}
;
	return null();
}

//ObjectMouseData_obj::~ObjectMouseData_obj() { }

Dynamic ObjectMouseData_obj::__CreateEmpty() { return  new ObjectMouseData_obj; }
hx::ObjectPtr< ObjectMouseData_obj > ObjectMouseData_obj::__new(Dynamic object,Dynamic onMouseDown,Dynamic onMouseUp,Dynamic onMouseOver,Dynamic onMouseOut,bool mouseChildren,bool mouseEnabled,bool pixelPerfect)
{  hx::ObjectPtr< ObjectMouseData_obj > result = new ObjectMouseData_obj();
	result->__construct(object,onMouseDown,onMouseUp,onMouseOver,onMouseOut,mouseChildren,mouseEnabled,pixelPerfect);
	return result;}

Dynamic ObjectMouseData_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< ObjectMouseData_obj > result = new ObjectMouseData_obj();
	result->__construct(inArgs[0],inArgs[1],inArgs[2],inArgs[3],inArgs[4],inArgs[5],inArgs[6],inArgs[7]);
	return result;}


ObjectMouseData_obj::ObjectMouseData_obj()
{
}

void ObjectMouseData_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(ObjectMouseData);
	HX_MARK_MEMBER_NAME(object,"object");
	HX_MARK_MEMBER_NAME(onMouseDown,"onMouseDown");
	HX_MARK_MEMBER_NAME(onMouseUp,"onMouseUp");
	HX_MARK_MEMBER_NAME(onMouseOver,"onMouseOver");
	HX_MARK_MEMBER_NAME(onMouseOut,"onMouseOut");
	HX_MARK_MEMBER_NAME(mouseChildren,"mouseChildren");
	HX_MARK_MEMBER_NAME(mouseEnabled,"mouseEnabled");
	HX_MARK_MEMBER_NAME(pixelPerfect,"pixelPerfect");
	HX_MARK_MEMBER_NAME(sprite,"sprite");
	HX_MARK_END_CLASS();
}

void ObjectMouseData_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(object,"object");
	HX_VISIT_MEMBER_NAME(onMouseDown,"onMouseDown");
	HX_VISIT_MEMBER_NAME(onMouseUp,"onMouseUp");
	HX_VISIT_MEMBER_NAME(onMouseOver,"onMouseOver");
	HX_VISIT_MEMBER_NAME(onMouseOut,"onMouseOut");
	HX_VISIT_MEMBER_NAME(mouseChildren,"mouseChildren");
	HX_VISIT_MEMBER_NAME(mouseEnabled,"mouseEnabled");
	HX_VISIT_MEMBER_NAME(pixelPerfect,"pixelPerfect");
	HX_VISIT_MEMBER_NAME(sprite,"sprite");
}

Dynamic ObjectMouseData_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"object") ) { return object; }
		if (HX_FIELD_EQ(inName,"sprite") ) { return sprite; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"onMouseUp") ) { return onMouseUp; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"onMouseOut") ) { return onMouseOut; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"onMouseDown") ) { return onMouseDown; }
		if (HX_FIELD_EQ(inName,"onMouseOver") ) { return onMouseOver; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"mouseEnabled") ) { return mouseEnabled; }
		if (HX_FIELD_EQ(inName,"pixelPerfect") ) { return pixelPerfect; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"mouseChildren") ) { return mouseChildren; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic ObjectMouseData_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"object") ) { object=inValue.Cast< ::flixel::FlxObject >(); return inValue; }
		if (HX_FIELD_EQ(inName,"sprite") ) { sprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"onMouseUp") ) { onMouseUp=inValue.Cast< Dynamic >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"onMouseOut") ) { onMouseOut=inValue.Cast< Dynamic >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"onMouseDown") ) { onMouseDown=inValue.Cast< Dynamic >(); return inValue; }
		if (HX_FIELD_EQ(inName,"onMouseOver") ) { onMouseOver=inValue.Cast< Dynamic >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"mouseEnabled") ) { mouseEnabled=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"pixelPerfect") ) { pixelPerfect=inValue.Cast< bool >(); return inValue; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"mouseChildren") ) { mouseChildren=inValue.Cast< bool >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void ObjectMouseData_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("object"));
	outFields->push(HX_CSTRING("mouseChildren"));
	outFields->push(HX_CSTRING("mouseEnabled"));
	outFields->push(HX_CSTRING("pixelPerfect"));
	outFields->push(HX_CSTRING("sprite"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::FlxObject*/ ,(int)offsetof(ObjectMouseData_obj,object),HX_CSTRING("object")},
	{hx::fsObject /*Dynamic*/ ,(int)offsetof(ObjectMouseData_obj,onMouseDown),HX_CSTRING("onMouseDown")},
	{hx::fsObject /*Dynamic*/ ,(int)offsetof(ObjectMouseData_obj,onMouseUp),HX_CSTRING("onMouseUp")},
	{hx::fsObject /*Dynamic*/ ,(int)offsetof(ObjectMouseData_obj,onMouseOver),HX_CSTRING("onMouseOver")},
	{hx::fsObject /*Dynamic*/ ,(int)offsetof(ObjectMouseData_obj,onMouseOut),HX_CSTRING("onMouseOut")},
	{hx::fsBool,(int)offsetof(ObjectMouseData_obj,mouseChildren),HX_CSTRING("mouseChildren")},
	{hx::fsBool,(int)offsetof(ObjectMouseData_obj,mouseEnabled),HX_CSTRING("mouseEnabled")},
	{hx::fsBool,(int)offsetof(ObjectMouseData_obj,pixelPerfect),HX_CSTRING("pixelPerfect")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(ObjectMouseData_obj,sprite),HX_CSTRING("sprite")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("object"),
	HX_CSTRING("onMouseDown"),
	HX_CSTRING("onMouseUp"),
	HX_CSTRING("onMouseOver"),
	HX_CSTRING("onMouseOut"),
	HX_CSTRING("mouseChildren"),
	HX_CSTRING("mouseEnabled"),
	HX_CSTRING("pixelPerfect"),
	HX_CSTRING("sprite"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(ObjectMouseData_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(ObjectMouseData_obj::__mClass,"__mClass");
};

#endif

Class ObjectMouseData_obj::__mClass;

void ObjectMouseData_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("flixel.plugin._MouseEventManager.ObjectMouseData"), hx::TCanCast< ObjectMouseData_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void ObjectMouseData_obj::__boot()
{
}

} // end namespace flixel
} // end namespace plugin
} // end namespace _MouseEventManager
