#include <hxcpp.h>

#ifndef INCLUDED_flixel_input_android_FlxAndroidKeyList
#include <flixel/input/android/FlxAndroidKeyList.h>
#endif
namespace flixel{
namespace input{
namespace android{

Void FlxAndroidKeyList_obj::__construct(Dynamic CheckFunction)
{
HX_STACK_FRAME("flixel.input.android.FlxAndroidKeyList","new",0x85ddcc1d,"flixel.input.android.FlxAndroidKeyList.new","flixel/input/android/FlxAndroidKeyList.hx",13,0x85400236)
HX_STACK_THIS(this)
HX_STACK_ARG(CheckFunction,"CheckFunction")
{
	HX_STACK_LINE(13)
	this->check = CheckFunction;
}
;
	return null();
}

//FlxAndroidKeyList_obj::~FlxAndroidKeyList_obj() { }

Dynamic FlxAndroidKeyList_obj::__CreateEmpty() { return  new FlxAndroidKeyList_obj; }
hx::ObjectPtr< FlxAndroidKeyList_obj > FlxAndroidKeyList_obj::__new(Dynamic CheckFunction)
{  hx::ObjectPtr< FlxAndroidKeyList_obj > result = new FlxAndroidKeyList_obj();
	result->__construct(CheckFunction);
	return result;}

Dynamic FlxAndroidKeyList_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< FlxAndroidKeyList_obj > result = new FlxAndroidKeyList_obj();
	result->__construct(inArgs[0]);
	return result;}

bool FlxAndroidKeyList_obj::get_BACK( ){
	HX_STACK_FRAME("flixel.input.android.FlxAndroidKeyList","get_BACK",0x9a67b4d3,"flixel.input.android.FlxAndroidKeyList.get_BACK","flixel/input/android/FlxAndroidKeyList.hx",18,0x85400236)
	HX_STACK_THIS(this)
	HX_STACK_LINE(18)
	return this->check(HX_CSTRING("BACK"));
}


HX_DEFINE_DYNAMIC_FUNC0(FlxAndroidKeyList_obj,get_BACK,return )

bool FlxAndroidKeyList_obj::get_MENU( ){
	HX_STACK_FRAME("flixel.input.android.FlxAndroidKeyList","get_MENU",0xa1b0204b,"flixel.input.android.FlxAndroidKeyList.get_MENU","flixel/input/android/FlxAndroidKeyList.hx",19,0x85400236)
	HX_STACK_THIS(this)
	HX_STACK_LINE(19)
	return this->check(HX_CSTRING("MENU"));
}


HX_DEFINE_DYNAMIC_FUNC0(FlxAndroidKeyList_obj,get_MENU,return )


FlxAndroidKeyList_obj::FlxAndroidKeyList_obj()
{
}

void FlxAndroidKeyList_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(FlxAndroidKeyList);
	HX_MARK_MEMBER_NAME(check,"check");
	HX_MARK_END_CLASS();
}

void FlxAndroidKeyList_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(check,"check");
}

Dynamic FlxAndroidKeyList_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"BACK") ) { return get_BACK(); }
		if (HX_FIELD_EQ(inName,"MENU") ) { return get_MENU(); }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"check") ) { return check; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"get_BACK") ) { return get_BACK_dyn(); }
		if (HX_FIELD_EQ(inName,"get_MENU") ) { return get_MENU_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic FlxAndroidKeyList_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"check") ) { check=inValue.Cast< Dynamic >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void FlxAndroidKeyList_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("BACK"));
	outFields->push(HX_CSTRING("MENU"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*Dynamic*/ ,(int)offsetof(FlxAndroidKeyList_obj,check),HX_CSTRING("check")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("check"),
	HX_CSTRING("get_BACK"),
	HX_CSTRING("get_MENU"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(FlxAndroidKeyList_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(FlxAndroidKeyList_obj::__mClass,"__mClass");
};

#endif

Class FlxAndroidKeyList_obj::__mClass;

void FlxAndroidKeyList_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("flixel.input.android.FlxAndroidKeyList"), hx::TCanCast< FlxAndroidKeyList_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void FlxAndroidKeyList_obj::__boot()
{
}

} // end namespace flixel
} // end namespace input
} // end namespace android
