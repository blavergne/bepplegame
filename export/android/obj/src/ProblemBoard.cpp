#include <hxcpp.h>

#ifndef INCLUDED_NumberChalk
#include <NumberChalk.h>
#endif
#ifndef INCLUDED_ProblemBoard
#include <ProblemBoard.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxPooled
#include <flixel/interfaces/IFlxPooled.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPoint
#include <flixel/util/FlxPoint.h>
#endif
#ifndef INCLUDED_flixel_util_FlxRandom
#include <flixel/util/FlxRandom.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif

Void ProblemBoard_obj::__construct()
{
HX_STACK_FRAME("ProblemBoard","new",0x60641b59,"ProblemBoard.new","ProblemBoard.hx",13,0x2ae85237)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(37)
	this->opShiftX = (int)0;
	HX_STACK_LINE(36)
	this->opShift = (int)0;
	HX_STACK_LINE(35)
	this->boardInUse = false;
	HX_STACK_LINE(24)
	this->problemAnswerString = HX_CSTRING("");
	HX_STACK_LINE(41)
	super::__construct(null());
	HX_STACK_LINE(42)
	this->init();
}
;
	return null();
}

//ProblemBoard_obj::~ProblemBoard_obj() { }

Dynamic ProblemBoard_obj::__CreateEmpty() { return  new ProblemBoard_obj; }
hx::ObjectPtr< ProblemBoard_obj > ProblemBoard_obj::__new()
{  hx::ObjectPtr< ProblemBoard_obj > result = new ProblemBoard_obj();
	result->__construct();
	return result;}

Dynamic ProblemBoard_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< ProblemBoard_obj > result = new ProblemBoard_obj();
	result->__construct();
	return result;}

Void ProblemBoard_obj::init( ){
{
		HX_STACK_FRAME("ProblemBoard","init",0xf3ec89f7,"ProblemBoard.init","ProblemBoard.hx",46,0x2ae85237)
		HX_STACK_THIS(this)
		HX_STACK_LINE(48)
		Array< ::Dynamic > _g = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(48)
		this->numArray = _g;
		HX_STACK_LINE(49)
		Array< ::Dynamic > _g1 = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(49)
		this->operationArray = _g1;
		HX_STACK_LINE(50)
		::flixel::FlxSprite _g2 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(50)
		this->blackBoard = _g2;
		HX_STACK_LINE(51)
		::flixel::FlxSprite _g3 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(51)
		this->blackBoardCorrectBoarder = _g3;
		HX_STACK_LINE(52)
		::flixel::FlxSprite _g4 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g4,"_g4");
		HX_STACK_LINE(52)
		this->blackBoardIncorrectBoarder = _g4;
		HX_STACK_LINE(53)
		::flixel::FlxSprite _g5 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g5,"_g5");
		HX_STACK_LINE(53)
		this->equalsSign = _g5;
		HX_STACK_LINE(54)
		::flixel::FlxSprite _g6 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g6,"_g6");
		HX_STACK_LINE(54)
		this->plusSign = _g6;
		HX_STACK_LINE(55)
		::flixel::FlxSprite _g7 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g7,"_g7");
		HX_STACK_LINE(55)
		this->minusSign = _g7;
		HX_STACK_LINE(56)
		::flixel::FlxSprite _g8 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g8,"_g8");
		HX_STACK_LINE(56)
		this->multiSign = _g8;
		HX_STACK_LINE(57)
		::flixel::FlxSprite _g9 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g9,"_g9");
		HX_STACK_LINE(57)
		this->operationVal = _g9;
		HX_STACK_LINE(58)
		::NumberChalk _g10 = ::NumberChalk_obj::__new();		HX_STACK_VAR(_g10,"_g10");
		HX_STACK_LINE(58)
		this->numberObjLeft = _g10;
		HX_STACK_LINE(59)
		::NumberChalk _g11 = ::NumberChalk_obj::__new();		HX_STACK_VAR(_g11,"_g11");
		HX_STACK_LINE(59)
		this->numberObjRight = _g11;
		HX_STACK_LINE(60)
		::flixel::FlxSprite _g12 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g12,"_g12");
		HX_STACK_LINE(60)
		this->leftSideNumber = _g12;
		HX_STACK_LINE(61)
		::flixel::FlxSprite _g13 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g13,"_g13");
		HX_STACK_LINE(61)
		this->rightSideNumber = _g13;
		HX_STACK_LINE(62)
		this->blackBoard->loadGraphic(HX_CSTRING("assets/images/blackBoard.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(63)
		this->blackBoardCorrectBoarder->loadGraphic(HX_CSTRING("assets/images/blackBoardCorrectBorder.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(64)
		this->blackBoardIncorrectBoarder->loadGraphic(HX_CSTRING("assets/images/blackBoardWrongBorder.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(65)
		this->equalsSign->loadGraphic(HX_CSTRING("assets/images/chalkEquals.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(66)
		this->plusSign->loadGraphic(HX_CSTRING("assets/images/plusOperation.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(67)
		this->minusSign->loadGraphic(HX_CSTRING("assets/images/minusOperation.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(68)
		this->multiSign->loadGraphic(HX_CSTRING("assets/images/multiOperation.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(69)
		this->equalsSign->scale->set_x(.7);
		HX_STACK_LINE(70)
		this->equalsSign->scale->set_y(.7);
		HX_STACK_LINE(71)
		this->plusSign->scale->set_x(.7);
		HX_STACK_LINE(72)
		this->plusSign->scale->set_y(.7);
		HX_STACK_LINE(73)
		this->minusSign->scale->set_x(.7);
		HX_STACK_LINE(74)
		this->minusSign->scale->set_y(.7);
		HX_STACK_LINE(75)
		this->blackBoard->set_x((int)245);
		HX_STACK_LINE(76)
		{
			HX_STACK_LINE(76)
			::flixel::FlxSprite _g14 = this->blackBoard;		HX_STACK_VAR(_g14,"_g14");
			HX_STACK_LINE(76)
			_g14->set_y((_g14->y - (int)100));
		}
		HX_STACK_LINE(77)
		this->blackBoardCorrectBoarder->set_x((int)245);
		HX_STACK_LINE(78)
		{
			HX_STACK_LINE(78)
			::flixel::FlxSprite _g14 = this->blackBoardCorrectBoarder;		HX_STACK_VAR(_g14,"_g14");
			HX_STACK_LINE(78)
			_g14->set_y((_g14->y - (int)100));
		}
		HX_STACK_LINE(79)
		this->blackBoardIncorrectBoarder->set_x((int)245);
		HX_STACK_LINE(80)
		{
			HX_STACK_LINE(80)
			::flixel::FlxSprite _g14 = this->blackBoardIncorrectBoarder;		HX_STACK_VAR(_g14,"_g14");
			HX_STACK_LINE(80)
			_g14->set_y((_g14->y - (int)100));
		}
		HX_STACK_LINE(81)
		this->blackBoardCorrectBoarder->set_alpha((int)0);
		HX_STACK_LINE(82)
		this->blackBoardIncorrectBoarder->set_alpha((int)0);
		HX_STACK_LINE(83)
		this->operationArray[(int)0] = this->plusSign;
		HX_STACK_LINE(84)
		this->operationArray[(int)1] = this->minusSign;
		HX_STACK_LINE(86)
		this->add(this->blackBoard);
		HX_STACK_LINE(87)
		this->add(this->blackBoardCorrectBoarder);
		HX_STACK_LINE(88)
		this->add(this->blackBoardIncorrectBoarder);
		HX_STACK_LINE(89)
		this->add(this->equalsSign);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(ProblemBoard_obj,init,(void))

Void ProblemBoard_obj::boardVisibile( bool val){
{
		HX_STACK_FRAME("ProblemBoard","boardVisibile",0xbf492008,"ProblemBoard.boardVisibile","ProblemBoard.hx",95,0x2ae85237)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(95)
		bool _switch_1 = (val);
		if (  ( _switch_1==true)){
			HX_STACK_LINE(98)
			this->blackBoard->set_alpha((int)1);
		}
		else if (  ( _switch_1==false)){
			HX_STACK_LINE(100)
			this->blackBoard->set_alpha((int)0);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(ProblemBoard_obj,boardVisibile,(void))

int ProblemBoard_obj::getProblemAnswer( ){
	HX_STACK_FRAME("ProblemBoard","getProblemAnswer",0xe02fa92e,"ProblemBoard.getProblemAnswer","ProblemBoard.hx",108,0x2ae85237)
	HX_STACK_THIS(this)
	HX_STACK_LINE(108)
	return this->problemAnswer;
}


HX_DEFINE_DYNAMIC_FUNC0(ProblemBoard_obj,getProblemAnswer,return )

::String ProblemBoard_obj::getProblemAnswerString( ){
	HX_STACK_FRAME("ProblemBoard","getProblemAnswerString",0x17745c9f,"ProblemBoard.getProblemAnswerString","ProblemBoard.hx",114,0x2ae85237)
	HX_STACK_THIS(this)
	HX_STACK_LINE(114)
	return this->problemAnswerString;
}


HX_DEFINE_DYNAMIC_FUNC0(ProblemBoard_obj,getProblemAnswerString,return )

Void ProblemBoard_obj::resetProblemString( ){
{
		HX_STACK_FRAME("ProblemBoard","resetProblemString",0x484f6368,"ProblemBoard.resetProblemString","ProblemBoard.hx",120,0x2ae85237)
		HX_STACK_THIS(this)
		HX_STACK_LINE(120)
		this->problemAnswerString = HX_CSTRING("");
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(ProblemBoard_obj,resetProblemString,(void))

Float ProblemBoard_obj::getBoardY( ){
	HX_STACK_FRAME("ProblemBoard","getBoardY",0x48031fe2,"ProblemBoard.getBoardY","ProblemBoard.hx",126,0x2ae85237)
	HX_STACK_THIS(this)
	HX_STACK_LINE(126)
	return this->blackBoard->y;
}


HX_DEFINE_DYNAMIC_FUNC0(ProblemBoard_obj,getBoardY,return )

bool ProblemBoard_obj::getBoardInUse( ){
	HX_STACK_FRAME("ProblemBoard","getBoardInUse",0xa49acd4b,"ProblemBoard.getBoardInUse","ProblemBoard.hx",132,0x2ae85237)
	HX_STACK_THIS(this)
	HX_STACK_LINE(132)
	return this->boardInUse;
}


HX_DEFINE_DYNAMIC_FUNC0(ProblemBoard_obj,getBoardInUse,return )

Void ProblemBoard_obj::setBoardInUse( bool val){
{
		HX_STACK_FRAME("ProblemBoard","setBoardInUse",0xe9a0af57,"ProblemBoard.setBoardInUse","ProblemBoard.hx",138,0x2ae85237)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(138)
		this->boardInUse = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(ProblemBoard_obj,setBoardInUse,(void))

Void ProblemBoard_obj::setBoarderColor( ::String val){
{
		HX_STACK_FRAME("ProblemBoard","setBoarderColor",0xdc9d470b,"ProblemBoard.setBoarderColor","ProblemBoard.hx",143,0x2ae85237)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(143)
		if (((val == HX_CSTRING("correctAnswer")))){
			HX_STACK_LINE(144)
			this->blackBoardCorrectBoarder->set_alpha((int)1);
		}
		else{
			HX_STACK_LINE(147)
			this->blackBoardIncorrectBoarder->set_alpha((int)1);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(ProblemBoard_obj,setBoarderColor,(void))

Void ProblemBoard_obj::boardScrollDown( ){
{
		HX_STACK_FRAME("ProblemBoard","boardScrollDown",0x28fa79ce,"ProblemBoard.boardScrollDown","ProblemBoard.hx",153,0x2ae85237)
		HX_STACK_THIS(this)
		HX_STACK_LINE(153)
		bool _g = this->boardInUse;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(153)
		bool _switch_2 = (_g);
		if (  ( _switch_2==true)){
			HX_STACK_LINE(156)
			if (((this->blackBoard->y <= (int)30))){
				HX_STACK_LINE(157)
				{
					HX_STACK_LINE(157)
					::flixel::FlxSprite _g1 = this->blackBoard;		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(157)
					_g1->set_y((_g1->y + (int)3));
				}
				HX_STACK_LINE(158)
				{
					HX_STACK_LINE(158)
					::flixel::FlxSprite _g1 = this->blackBoardCorrectBoarder;		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(158)
					_g1->set_y((_g1->y + (int)3));
				}
				HX_STACK_LINE(159)
				{
					HX_STACK_LINE(159)
					::flixel::FlxSprite _g1 = this->blackBoardIncorrectBoarder;		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(159)
					_g1->set_y((_g1->y + (int)3));
				}
			}
		}
		else if (  ( _switch_2==false)){
			HX_STACK_LINE(162)
			if (((this->blackBoard->y >= (int)-80))){
				HX_STACK_LINE(163)
				{
					HX_STACK_LINE(163)
					::flixel::FlxSprite _g1 = this->blackBoard;		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(163)
					_g1->set_y((_g1->y - (int)6));
				}
				HX_STACK_LINE(164)
				{
					HX_STACK_LINE(164)
					::flixel::FlxSprite _g1 = this->blackBoardCorrectBoarder;		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(164)
					_g1->set_y((_g1->y - (int)6));
				}
				HX_STACK_LINE(165)
				{
					HX_STACK_LINE(165)
					::flixel::FlxSprite _g1 = this->blackBoardIncorrectBoarder;		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(165)
					_g1->set_y((_g1->y - (int)6));
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(ProblemBoard_obj,boardScrollDown,(void))

Void ProblemBoard_obj::solveProblem( int operation,int leftRand,int rightRand){
{
		HX_STACK_FRAME("ProblemBoard","solveProblem",0xc003a387,"ProblemBoard.solveProblem","ProblemBoard.hx",171,0x2ae85237)
		HX_STACK_THIS(this)
		HX_STACK_ARG(operation,"operation")
		HX_STACK_ARG(leftRand,"leftRand")
		HX_STACK_ARG(rightRand,"rightRand")
		HX_STACK_LINE(173)
		if (((operation == (int)1))){
			HX_STACK_LINE(175)
			if (((leftRand < rightRand))){
				HX_STACK_LINE(177)
				this->problemAnswer = (rightRand - leftRand);
			}
			else{
				HX_STACK_LINE(182)
				this->problemAnswer = (leftRand - rightRand);
			}
		}
		HX_STACK_LINE(187)
		if (((operation == (int)0))){
			HX_STACK_LINE(189)
			this->problemAnswer = (leftRand + rightRand);
		}
		HX_STACK_LINE(192)
		if (((operation == (int)2))){
			HX_STACK_LINE(193)
			this->problemAnswer = (leftRand * rightRand);
		}
		HX_STACK_LINE(196)
		this->numberToString(this->problemAnswer);
		HX_STACK_LINE(197)
		this->reverseAnswerString();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC3(ProblemBoard_obj,solveProblem,(void))

Void ProblemBoard_obj::pickOperation( ::String opType){
{
		HX_STACK_FRAME("ProblemBoard","pickOperation",0xaa58f33f,"ProblemBoard.pickOperation","ProblemBoard.hx",201,0x2ae85237)
		HX_STACK_THIS(this)
		HX_STACK_ARG(opType,"opType")
		HX_STACK_LINE(202)
		if (((opType == HX_CSTRING("tree")))){
			HX_STACK_LINE(203)
			this->opShiftX = (int)0;
			HX_STACK_LINE(204)
			::flixel::FlxSprite temp;		HX_STACK_VAR(temp,"temp");
			HX_STACK_LINE(205)
			int randNum = ::flixel::util::FlxRandom_obj::intRanged((int)0,(int)1,null());		HX_STACK_VAR(randNum,"randNum");
			HX_STACK_LINE(206)
			::flixel::FlxSprite _g = this->numberObjLeft->getRandomNumber();		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(206)
			this->leftSideNumber = _g;
			HX_STACK_LINE(207)
			::flixel::FlxSprite _g1 = this->numberObjRight->getRandomNumber();		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(207)
			this->rightSideNumber = _g1;
			HX_STACK_LINE(208)
			this->operationVal = this->operationArray->__get(randNum).StaticCast< ::flixel::FlxSprite >();
			HX_STACK_LINE(209)
			int leftRandNum = this->numberObjLeft->getMyIndex();		HX_STACK_VAR(leftRandNum,"leftRandNum");
			HX_STACK_LINE(210)
			int rightRandNum = this->numberObjRight->getMyIndex();		HX_STACK_VAR(rightRandNum,"rightRandNum");
			HX_STACK_LINE(211)
			if (((leftRandNum < rightRandNum))){
				HX_STACK_LINE(212)
				temp = this->leftSideNumber;
				HX_STACK_LINE(213)
				this->leftSideNumber = this->rightSideNumber;
				HX_STACK_LINE(214)
				this->rightSideNumber = temp;
			}
			HX_STACK_LINE(216)
			if (((randNum == (int)0))){
				HX_STACK_LINE(217)
				this->opShift = (int)25;
			}
			else{
				HX_STACK_LINE(220)
				this->opShift = (int)35;
			}
			HX_STACK_LINE(222)
			this->solveProblem(randNum,leftRandNum,rightRandNum);
			HX_STACK_LINE(223)
			this->add(this->operationVal);
			HX_STACK_LINE(224)
			this->add(this->leftSideNumber);
			HX_STACK_LINE(225)
			this->add(this->rightSideNumber);
		}
		HX_STACK_LINE(227)
		if (((opType == HX_CSTRING("machine")))){
			HX_STACK_LINE(228)
			::flixel::FlxSprite temp;		HX_STACK_VAR(temp,"temp");
			HX_STACK_LINE(229)
			int randNum = ::flixel::util::FlxRandom_obj::intRanged((int)0,(int)1,null());		HX_STACK_VAR(randNum,"randNum");
			HX_STACK_LINE(230)
			::flixel::FlxSprite _g2 = this->numberObjLeft->getRandomNumber();		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(230)
			this->leftSideNumber = _g2;
			HX_STACK_LINE(231)
			::flixel::FlxSprite _g3 = this->numberObjRight->getRandomNumber();		HX_STACK_VAR(_g3,"_g3");
			HX_STACK_LINE(231)
			this->rightSideNumber = _g3;
			HX_STACK_LINE(232)
			this->operationVal = this->multiSign;
			HX_STACK_LINE(233)
			int leftRandNum = this->numberObjLeft->getMyIndex();		HX_STACK_VAR(leftRandNum,"leftRandNum");
			HX_STACK_LINE(234)
			int rightRandNum = this->numberObjRight->getMyIndex();		HX_STACK_VAR(rightRandNum,"rightRandNum");
			HX_STACK_LINE(235)
			this->opShift = (int)32;
			HX_STACK_LINE(236)
			this->opShiftX = (int)5;
			HX_STACK_LINE(237)
			this->solveProblem((int)2,leftRandNum,rightRandNum);
			HX_STACK_LINE(238)
			this->add(this->operationVal);
			HX_STACK_LINE(239)
			this->add(this->leftSideNumber);
			HX_STACK_LINE(240)
			this->add(this->rightSideNumber);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(ProblemBoard_obj,pickOperation,(void))

Void ProblemBoard_obj::numberToString( Float number){
{
		HX_STACK_FRAME("ProblemBoard","numberToString",0x7cee1a3c,"ProblemBoard.numberToString","ProblemBoard.hx",244,0x2ae85237)
		HX_STACK_THIS(this)
		HX_STACK_ARG(number,"number")
		HX_STACK_LINE(246)
		if ((((Float(number) / Float((int)10)) < (int)1))){
			HX_STACK_LINE(248)
			{
				HX_STACK_LINE(248)
				Float _g = hx::Mod(number,(int)10);		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(248)
				switch( (int)(_g)){
					case (int)0: {
						HX_STACK_LINE(251)
						hx::AddEq(this->problemAnswerString,HX_CSTRING("0"));
					}
					;break;
					case (int)1: {
						HX_STACK_LINE(253)
						hx::AddEq(this->problemAnswerString,HX_CSTRING("1"));
					}
					;break;
					case (int)2: {
						HX_STACK_LINE(255)
						hx::AddEq(this->problemAnswerString,HX_CSTRING("2"));
					}
					;break;
					case (int)3: {
						HX_STACK_LINE(257)
						hx::AddEq(this->problemAnswerString,HX_CSTRING("3"));
					}
					;break;
					case (int)4: {
						HX_STACK_LINE(259)
						hx::AddEq(this->problemAnswerString,HX_CSTRING("4"));
					}
					;break;
					case (int)5: {
						HX_STACK_LINE(261)
						hx::AddEq(this->problemAnswerString,HX_CSTRING("5"));
					}
					;break;
					case (int)6: {
						HX_STACK_LINE(263)
						hx::AddEq(this->problemAnswerString,HX_CSTRING("6"));
					}
					;break;
					case (int)7: {
						HX_STACK_LINE(265)
						hx::AddEq(this->problemAnswerString,HX_CSTRING("7"));
					}
					;break;
					case (int)8: {
						HX_STACK_LINE(267)
						hx::AddEq(this->problemAnswerString,HX_CSTRING("8"));
					}
					;break;
					case (int)9: {
						HX_STACK_LINE(269)
						hx::AddEq(this->problemAnswerString,HX_CSTRING("9"));
					}
					;break;
				}
			}
			HX_STACK_LINE(272)
			return null();
		}
		HX_STACK_LINE(276)
		{
			HX_STACK_LINE(276)
			Float _g = hx::Mod(number,(int)10);		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(276)
			switch( (int)(_g)){
				case (int)0: {
					HX_STACK_LINE(279)
					hx::AddEq(this->problemAnswerString,HX_CSTRING("0"));
				}
				;break;
				case (int)1: {
					HX_STACK_LINE(281)
					hx::AddEq(this->problemAnswerString,HX_CSTRING("1"));
				}
				;break;
				case (int)2: {
					HX_STACK_LINE(283)
					hx::AddEq(this->problemAnswerString,HX_CSTRING("2"));
				}
				;break;
				case (int)3: {
					HX_STACK_LINE(285)
					hx::AddEq(this->problemAnswerString,HX_CSTRING("3"));
				}
				;break;
				case (int)4: {
					HX_STACK_LINE(287)
					hx::AddEq(this->problemAnswerString,HX_CSTRING("4"));
				}
				;break;
				case (int)5: {
					HX_STACK_LINE(289)
					hx::AddEq(this->problemAnswerString,HX_CSTRING("5"));
				}
				;break;
				case (int)6: {
					HX_STACK_LINE(291)
					hx::AddEq(this->problemAnswerString,HX_CSTRING("6"));
				}
				;break;
				case (int)7: {
					HX_STACK_LINE(293)
					hx::AddEq(this->problemAnswerString,HX_CSTRING("7"));
				}
				;break;
				case (int)8: {
					HX_STACK_LINE(295)
					hx::AddEq(this->problemAnswerString,HX_CSTRING("8"));
				}
				;break;
				case (int)9: {
					HX_STACK_LINE(297)
					hx::AddEq(this->problemAnswerString,HX_CSTRING("9"));
				}
				;break;
			}
		}
		HX_STACK_LINE(301)
		int _g = ::Math_obj::floor((Float(number) / Float((int)10)));		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(301)
		this->numberToString(_g);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(ProblemBoard_obj,numberToString,(void))

Void ProblemBoard_obj::reverseAnswerString( ){
{
		HX_STACK_FRAME("ProblemBoard","reverseAnswerString",0xfa7d0a2a,"ProblemBoard.reverseAnswerString","ProblemBoard.hx",306,0x2ae85237)
		HX_STACK_THIS(this)
		HX_STACK_LINE(308)
		int stringLength = this->problemAnswerString.length;		HX_STACK_VAR(stringLength,"stringLength");
		HX_STACK_LINE(309)
		::String tempString = HX_CSTRING("");		HX_STACK_VAR(tempString,"tempString");
		HX_STACK_LINE(310)
		while((true)){
			HX_STACK_LINE(310)
			if ((!(((stringLength >= (int)0))))){
				HX_STACK_LINE(310)
				break;
			}
			HX_STACK_LINE(312)
			::String _g = this->problemAnswerString.charAt(stringLength);		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(312)
			hx::AddEq(tempString,_g);
			HX_STACK_LINE(313)
			(stringLength)--;
		}
		HX_STACK_LINE(317)
		this->problemAnswerString = tempString;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(ProblemBoard_obj,reverseAnswerString,(void))

Void ProblemBoard_obj::updateBoardPieces( ){
{
		HX_STACK_FRAME("ProblemBoard","updateBoardPieces",0x73d4ab3b,"ProblemBoard.updateBoardPieces","ProblemBoard.hx",321,0x2ae85237)
		HX_STACK_THIS(this)
		HX_STACK_LINE(323)
		this->leftSideNumber->set_y(this->blackBoard->y);
		HX_STACK_LINE(324)
		this->leftSideNumber->set_x(this->blackBoard->x);
		HX_STACK_LINE(325)
		this->rightSideNumber->set_y(this->blackBoard->y);
		HX_STACK_LINE(326)
		this->rightSideNumber->set_x((this->blackBoard->x + (int)70));
		HX_STACK_LINE(327)
		this->equalsSign->set_x((this->blackBoard->x + (int)120));
		HX_STACK_LINE(328)
		this->equalsSign->set_y((this->blackBoard->y + (int)25));
		HX_STACK_LINE(329)
		this->operationVal->set_x(((this->blackBoard->x + (int)50) + this->opShiftX));
		HX_STACK_LINE(330)
		this->operationVal->set_y((this->blackBoard->y + this->opShift));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(ProblemBoard_obj,updateBoardPieces,(void))

Void ProblemBoard_obj::update( ){
{
		HX_STACK_FRAME("ProblemBoard","update",0x1f559cb0,"ProblemBoard.update","ProblemBoard.hx",334,0x2ae85237)
		HX_STACK_THIS(this)
		HX_STACK_LINE(336)
		this->boardScrollDown();
		HX_STACK_LINE(337)
		this->updateBoardPieces();
		HX_STACK_LINE(338)
		this->super::update();
	}
return null();
}


Void ProblemBoard_obj::destroy( ){
{
		HX_STACK_FRAME("ProblemBoard","destroy",0xd249e873,"ProblemBoard.destroy","ProblemBoard.hx",342,0x2ae85237)
		HX_STACK_THIS(this)
		HX_STACK_LINE(344)
		this->numArray = null();
		HX_STACK_LINE(345)
		this->operationArray = null();
		HX_STACK_LINE(346)
		this->blackBoard = null();
		HX_STACK_LINE(347)
		this->blackBoardCorrectBoarder = null();
		HX_STACK_LINE(348)
		this->blackBoardIncorrectBoarder = null();
		HX_STACK_LINE(349)
		this->equalsSign = null();
		HX_STACK_LINE(350)
		this->plusSign = null();
		HX_STACK_LINE(351)
		this->minusSign = null();
		HX_STACK_LINE(352)
		this->multiSign = null();
		HX_STACK_LINE(353)
		this->operationVal = null();
		HX_STACK_LINE(354)
		this->numberObjLeft = null();
		HX_STACK_LINE(355)
		this->numberObjRight = null();
		HX_STACK_LINE(356)
		this->leftSideNumber = null();
		HX_STACK_LINE(357)
		this->rightSideNumber = null();
		HX_STACK_LINE(358)
		this->super::destroy();
	}
return null();
}



ProblemBoard_obj::ProblemBoard_obj()
{
}

void ProblemBoard_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(ProblemBoard);
	HX_MARK_MEMBER_NAME(problemGroup,"problemGroup");
	HX_MARK_MEMBER_NAME(blackBoard,"blackBoard");
	HX_MARK_MEMBER_NAME(blackBoardCorrectBoarder,"blackBoardCorrectBoarder");
	HX_MARK_MEMBER_NAME(blackBoardIncorrectBoarder,"blackBoardIncorrectBoarder");
	HX_MARK_MEMBER_NAME(singleDigitL,"singleDigitL");
	HX_MARK_MEMBER_NAME(singleDigitR,"singleDigitR");
	HX_MARK_MEMBER_NAME(operationVal,"operationVal");
	HX_MARK_MEMBER_NAME(problemAnswer,"problemAnswer");
	HX_MARK_MEMBER_NAME(problemAnswerString,"problemAnswerString");
	HX_MARK_MEMBER_NAME(equalsSign,"equalsSign");
	HX_MARK_MEMBER_NAME(plusSign,"plusSign");
	HX_MARK_MEMBER_NAME(minusSign,"minusSign");
	HX_MARK_MEMBER_NAME(multiSign,"multiSign");
	HX_MARK_MEMBER_NAME(numberObjLeft,"numberObjLeft");
	HX_MARK_MEMBER_NAME(numberObjRight,"numberObjRight");
	HX_MARK_MEMBER_NAME(leftSideNumber,"leftSideNumber");
	HX_MARK_MEMBER_NAME(rightSideNumber,"rightSideNumber");
	HX_MARK_MEMBER_NAME(numArray,"numArray");
	HX_MARK_MEMBER_NAME(operationArray,"operationArray");
	HX_MARK_MEMBER_NAME(boardInUse,"boardInUse");
	HX_MARK_MEMBER_NAME(opShift,"opShift");
	HX_MARK_MEMBER_NAME(opShiftX,"opShiftX");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void ProblemBoard_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(problemGroup,"problemGroup");
	HX_VISIT_MEMBER_NAME(blackBoard,"blackBoard");
	HX_VISIT_MEMBER_NAME(blackBoardCorrectBoarder,"blackBoardCorrectBoarder");
	HX_VISIT_MEMBER_NAME(blackBoardIncorrectBoarder,"blackBoardIncorrectBoarder");
	HX_VISIT_MEMBER_NAME(singleDigitL,"singleDigitL");
	HX_VISIT_MEMBER_NAME(singleDigitR,"singleDigitR");
	HX_VISIT_MEMBER_NAME(operationVal,"operationVal");
	HX_VISIT_MEMBER_NAME(problemAnswer,"problemAnswer");
	HX_VISIT_MEMBER_NAME(problemAnswerString,"problemAnswerString");
	HX_VISIT_MEMBER_NAME(equalsSign,"equalsSign");
	HX_VISIT_MEMBER_NAME(plusSign,"plusSign");
	HX_VISIT_MEMBER_NAME(minusSign,"minusSign");
	HX_VISIT_MEMBER_NAME(multiSign,"multiSign");
	HX_VISIT_MEMBER_NAME(numberObjLeft,"numberObjLeft");
	HX_VISIT_MEMBER_NAME(numberObjRight,"numberObjRight");
	HX_VISIT_MEMBER_NAME(leftSideNumber,"leftSideNumber");
	HX_VISIT_MEMBER_NAME(rightSideNumber,"rightSideNumber");
	HX_VISIT_MEMBER_NAME(numArray,"numArray");
	HX_VISIT_MEMBER_NAME(operationArray,"operationArray");
	HX_VISIT_MEMBER_NAME(boardInUse,"boardInUse");
	HX_VISIT_MEMBER_NAME(opShift,"opShift");
	HX_VISIT_MEMBER_NAME(opShiftX,"opShiftX");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic ProblemBoard_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"init") ) { return init_dyn(); }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"opShift") ) { return opShift; }
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"plusSign") ) { return plusSign; }
		if (HX_FIELD_EQ(inName,"numArray") ) { return numArray; }
		if (HX_FIELD_EQ(inName,"opShiftX") ) { return opShiftX; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"minusSign") ) { return minusSign; }
		if (HX_FIELD_EQ(inName,"multiSign") ) { return multiSign; }
		if (HX_FIELD_EQ(inName,"getBoardY") ) { return getBoardY_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"blackBoard") ) { return blackBoard; }
		if (HX_FIELD_EQ(inName,"equalsSign") ) { return equalsSign; }
		if (HX_FIELD_EQ(inName,"boardInUse") ) { return boardInUse; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"problemGroup") ) { return problemGroup; }
		if (HX_FIELD_EQ(inName,"singleDigitL") ) { return singleDigitL; }
		if (HX_FIELD_EQ(inName,"singleDigitR") ) { return singleDigitR; }
		if (HX_FIELD_EQ(inName,"operationVal") ) { return operationVal; }
		if (HX_FIELD_EQ(inName,"solveProblem") ) { return solveProblem_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"problemAnswer") ) { return problemAnswer; }
		if (HX_FIELD_EQ(inName,"numberObjLeft") ) { return numberObjLeft; }
		if (HX_FIELD_EQ(inName,"boardVisibile") ) { return boardVisibile_dyn(); }
		if (HX_FIELD_EQ(inName,"getBoardInUse") ) { return getBoardInUse_dyn(); }
		if (HX_FIELD_EQ(inName,"setBoardInUse") ) { return setBoardInUse_dyn(); }
		if (HX_FIELD_EQ(inName,"pickOperation") ) { return pickOperation_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"numberObjRight") ) { return numberObjRight; }
		if (HX_FIELD_EQ(inName,"leftSideNumber") ) { return leftSideNumber; }
		if (HX_FIELD_EQ(inName,"operationArray") ) { return operationArray; }
		if (HX_FIELD_EQ(inName,"numberToString") ) { return numberToString_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"rightSideNumber") ) { return rightSideNumber; }
		if (HX_FIELD_EQ(inName,"setBoarderColor") ) { return setBoarderColor_dyn(); }
		if (HX_FIELD_EQ(inName,"boardScrollDown") ) { return boardScrollDown_dyn(); }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"getProblemAnswer") ) { return getProblemAnswer_dyn(); }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"updateBoardPieces") ) { return updateBoardPieces_dyn(); }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"resetProblemString") ) { return resetProblemString_dyn(); }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"problemAnswerString") ) { return problemAnswerString; }
		if (HX_FIELD_EQ(inName,"reverseAnswerString") ) { return reverseAnswerString_dyn(); }
		break;
	case 22:
		if (HX_FIELD_EQ(inName,"getProblemAnswerString") ) { return getProblemAnswerString_dyn(); }
		break;
	case 24:
		if (HX_FIELD_EQ(inName,"blackBoardCorrectBoarder") ) { return blackBoardCorrectBoarder; }
		break;
	case 26:
		if (HX_FIELD_EQ(inName,"blackBoardIncorrectBoarder") ) { return blackBoardIncorrectBoarder; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic ProblemBoard_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 7:
		if (HX_FIELD_EQ(inName,"opShift") ) { opShift=inValue.Cast< int >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"plusSign") ) { plusSign=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"numArray") ) { numArray=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		if (HX_FIELD_EQ(inName,"opShiftX") ) { opShiftX=inValue.Cast< int >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"minusSign") ) { minusSign=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"multiSign") ) { multiSign=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"blackBoard") ) { blackBoard=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"equalsSign") ) { equalsSign=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"boardInUse") ) { boardInUse=inValue.Cast< bool >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"problemGroup") ) { problemGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"singleDigitL") ) { singleDigitL=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"singleDigitR") ) { singleDigitR=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"operationVal") ) { operationVal=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"problemAnswer") ) { problemAnswer=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"numberObjLeft") ) { numberObjLeft=inValue.Cast< ::NumberChalk >(); return inValue; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"numberObjRight") ) { numberObjRight=inValue.Cast< ::NumberChalk >(); return inValue; }
		if (HX_FIELD_EQ(inName,"leftSideNumber") ) { leftSideNumber=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"operationArray") ) { operationArray=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"rightSideNumber") ) { rightSideNumber=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"problemAnswerString") ) { problemAnswerString=inValue.Cast< ::String >(); return inValue; }
		break;
	case 24:
		if (HX_FIELD_EQ(inName,"blackBoardCorrectBoarder") ) { blackBoardCorrectBoarder=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 26:
		if (HX_FIELD_EQ(inName,"blackBoardIncorrectBoarder") ) { blackBoardIncorrectBoarder=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void ProblemBoard_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("problemGroup"));
	outFields->push(HX_CSTRING("blackBoard"));
	outFields->push(HX_CSTRING("blackBoardCorrectBoarder"));
	outFields->push(HX_CSTRING("blackBoardIncorrectBoarder"));
	outFields->push(HX_CSTRING("singleDigitL"));
	outFields->push(HX_CSTRING("singleDigitR"));
	outFields->push(HX_CSTRING("operationVal"));
	outFields->push(HX_CSTRING("problemAnswer"));
	outFields->push(HX_CSTRING("problemAnswerString"));
	outFields->push(HX_CSTRING("equalsSign"));
	outFields->push(HX_CSTRING("plusSign"));
	outFields->push(HX_CSTRING("minusSign"));
	outFields->push(HX_CSTRING("multiSign"));
	outFields->push(HX_CSTRING("numberObjLeft"));
	outFields->push(HX_CSTRING("numberObjRight"));
	outFields->push(HX_CSTRING("leftSideNumber"));
	outFields->push(HX_CSTRING("rightSideNumber"));
	outFields->push(HX_CSTRING("numArray"));
	outFields->push(HX_CSTRING("operationArray"));
	outFields->push(HX_CSTRING("boardInUse"));
	outFields->push(HX_CSTRING("opShift"));
	outFields->push(HX_CSTRING("opShiftX"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(ProblemBoard_obj,problemGroup),HX_CSTRING("problemGroup")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(ProblemBoard_obj,blackBoard),HX_CSTRING("blackBoard")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(ProblemBoard_obj,blackBoardCorrectBoarder),HX_CSTRING("blackBoardCorrectBoarder")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(ProblemBoard_obj,blackBoardIncorrectBoarder),HX_CSTRING("blackBoardIncorrectBoarder")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(ProblemBoard_obj,singleDigitL),HX_CSTRING("singleDigitL")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(ProblemBoard_obj,singleDigitR),HX_CSTRING("singleDigitR")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(ProblemBoard_obj,operationVal),HX_CSTRING("operationVal")},
	{hx::fsInt,(int)offsetof(ProblemBoard_obj,problemAnswer),HX_CSTRING("problemAnswer")},
	{hx::fsString,(int)offsetof(ProblemBoard_obj,problemAnswerString),HX_CSTRING("problemAnswerString")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(ProblemBoard_obj,equalsSign),HX_CSTRING("equalsSign")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(ProblemBoard_obj,plusSign),HX_CSTRING("plusSign")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(ProblemBoard_obj,minusSign),HX_CSTRING("minusSign")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(ProblemBoard_obj,multiSign),HX_CSTRING("multiSign")},
	{hx::fsObject /*::NumberChalk*/ ,(int)offsetof(ProblemBoard_obj,numberObjLeft),HX_CSTRING("numberObjLeft")},
	{hx::fsObject /*::NumberChalk*/ ,(int)offsetof(ProblemBoard_obj,numberObjRight),HX_CSTRING("numberObjRight")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(ProblemBoard_obj,leftSideNumber),HX_CSTRING("leftSideNumber")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(ProblemBoard_obj,rightSideNumber),HX_CSTRING("rightSideNumber")},
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(ProblemBoard_obj,numArray),HX_CSTRING("numArray")},
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(ProblemBoard_obj,operationArray),HX_CSTRING("operationArray")},
	{hx::fsBool,(int)offsetof(ProblemBoard_obj,boardInUse),HX_CSTRING("boardInUse")},
	{hx::fsInt,(int)offsetof(ProblemBoard_obj,opShift),HX_CSTRING("opShift")},
	{hx::fsInt,(int)offsetof(ProblemBoard_obj,opShiftX),HX_CSTRING("opShiftX")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("problemGroup"),
	HX_CSTRING("blackBoard"),
	HX_CSTRING("blackBoardCorrectBoarder"),
	HX_CSTRING("blackBoardIncorrectBoarder"),
	HX_CSTRING("singleDigitL"),
	HX_CSTRING("singleDigitR"),
	HX_CSTRING("operationVal"),
	HX_CSTRING("problemAnswer"),
	HX_CSTRING("problemAnswerString"),
	HX_CSTRING("equalsSign"),
	HX_CSTRING("plusSign"),
	HX_CSTRING("minusSign"),
	HX_CSTRING("multiSign"),
	HX_CSTRING("numberObjLeft"),
	HX_CSTRING("numberObjRight"),
	HX_CSTRING("leftSideNumber"),
	HX_CSTRING("rightSideNumber"),
	HX_CSTRING("numArray"),
	HX_CSTRING("operationArray"),
	HX_CSTRING("boardInUse"),
	HX_CSTRING("opShift"),
	HX_CSTRING("opShiftX"),
	HX_CSTRING("init"),
	HX_CSTRING("boardVisibile"),
	HX_CSTRING("getProblemAnswer"),
	HX_CSTRING("getProblemAnswerString"),
	HX_CSTRING("resetProblemString"),
	HX_CSTRING("getBoardY"),
	HX_CSTRING("getBoardInUse"),
	HX_CSTRING("setBoardInUse"),
	HX_CSTRING("setBoarderColor"),
	HX_CSTRING("boardScrollDown"),
	HX_CSTRING("solveProblem"),
	HX_CSTRING("pickOperation"),
	HX_CSTRING("numberToString"),
	HX_CSTRING("reverseAnswerString"),
	HX_CSTRING("updateBoardPieces"),
	HX_CSTRING("update"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(ProblemBoard_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(ProblemBoard_obj::__mClass,"__mClass");
};

#endif

Class ProblemBoard_obj::__mClass;

void ProblemBoard_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("ProblemBoard"), hx::TCanCast< ProblemBoard_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void ProblemBoard_obj::__boot()
{
}

