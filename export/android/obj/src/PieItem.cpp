#include <hxcpp.h>

#ifndef INCLUDED_MarketItem
#include <MarketItem.h>
#endif
#ifndef INCLUDED_PieItem
#include <PieItem.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif

Void PieItem_obj::__construct()
{
HX_STACK_FRAME("PieItem","new",0xc7dbc3d1,"PieItem.new","PieItem.hx",13,0xd9dd60bf)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(14)
	super::__construct();
	HX_STACK_LINE(15)
	this->initMarketItems();
}
;
	return null();
}

//PieItem_obj::~PieItem_obj() { }

Dynamic PieItem_obj::__CreateEmpty() { return  new PieItem_obj; }
hx::ObjectPtr< PieItem_obj > PieItem_obj::__new()
{  hx::ObjectPtr< PieItem_obj > result = new PieItem_obj();
	result->__construct();
	return result;}

Dynamic PieItem_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< PieItem_obj > result = new PieItem_obj();
	result->__construct();
	return result;}

Void PieItem_obj::initMarketItems( ){
{
		HX_STACK_FRAME("PieItem","initMarketItems",0xb4cb4c65,"PieItem.initMarketItems","PieItem.hx",18,0xd9dd60bf)
		HX_STACK_THIS(this)
		HX_STACK_LINE(19)
		::flixel::FlxSprite _g = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(19)
		this->itemSprite = _g;
		HX_STACK_LINE(20)
		this->itemCost = (int)20;
		HX_STACK_LINE(21)
		this->itemName = HX_CSTRING("pieItem");
		HX_STACK_LINE(22)
		this->itemCount = (int)1;
		HX_STACK_LINE(23)
		this->itemSprite->loadGraphic(HX_CSTRING("assets/images/pieMachineItem.png"),true,(int)30,(int)30,false,HX_CSTRING("pieItem"));
		HX_STACK_LINE(24)
		this->itemSprite->animation->add(HX_CSTRING("available"),Array_obj< int >::__new().Add((int)0),(int)1,false);
		HX_STACK_LINE(25)
		this->itemSprite->animation->add(HX_CSTRING("notAvailable"),Array_obj< int >::__new().Add((int)1),(int)1,false);
		HX_STACK_LINE(26)
		this->setItemAvailable(true);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PieItem_obj,initMarketItems,(void))

Void PieItem_obj::setPieItemCount( int val){
{
		HX_STACK_FRAME("PieItem","setPieItemCount",0x7e527483,"PieItem.setPieItemCount","PieItem.hx",30,0xd9dd60bf)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(30)
		this->itemCount = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PieItem_obj,setPieItemCount,(void))

Void PieItem_obj::update( ){
{
		HX_STACK_FRAME("PieItem","update",0xd7688f38,"PieItem.update","PieItem.hx",33,0xd9dd60bf)
		HX_STACK_THIS(this)
		HX_STACK_LINE(34)
		this->setAvailableAnimation();
		HX_STACK_LINE(35)
		this->super::update();
	}
return null();
}



PieItem_obj::PieItem_obj()
{
}

Dynamic PieItem_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"initMarketItems") ) { return initMarketItems_dyn(); }
		if (HX_FIELD_EQ(inName,"setPieItemCount") ) { return setPieItemCount_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic PieItem_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	return super::__SetField(inName,inValue,inCallProp);
}

void PieItem_obj::__GetFields(Array< ::String> &outFields)
{
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo *sMemberStorageInfo = 0;
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("initMarketItems"),
	HX_CSTRING("setPieItemCount"),
	HX_CSTRING("update"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(PieItem_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(PieItem_obj::__mClass,"__mClass");
};

#endif

Class PieItem_obj::__mClass;

void PieItem_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("PieItem"), hx::TCanCast< PieItem_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void PieItem_obj::__boot()
{
}

