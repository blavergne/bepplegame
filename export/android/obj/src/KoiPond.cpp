#include <hxcpp.h>

#ifndef INCLUDED_KoiFish
#include <KoiFish.h>
#endif
#ifndef INCLUDED_KoiPond
#include <KoiPond.h>
#endif
#ifndef INCLUDED_Tile
#include <Tile.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_plugin_FlxPlugin
#include <flixel/plugin/FlxPlugin.h>
#endif
#ifndef INCLUDED_flixel_plugin_MouseEventManager
#include <flixel/plugin/MouseEventManager.h>
#endif
#ifndef INCLUDED_flixel_system_FlxSound
#include <flixel/system/FlxSound.h>
#endif
#ifndef INCLUDED_flixel_system_frontEnds_SoundFrontEnd
#include <flixel/system/frontEnds/SoundFrontEnd.h>
#endif
#ifndef INCLUDED_flixel_util_FlxDestroyUtil
#include <flixel/util/FlxDestroyUtil.h>
#endif
#ifndef INCLUDED_flixel_util_FlxRandom
#include <flixel/util/FlxRandom.h>
#endif

Void KoiPond_obj::__construct(::Tile tileSprite)
{
HX_STACK_FRAME("KoiPond","new",0x3c18f2ac,"KoiPond.new","KoiPond.hx",36,0x3ad10644)
HX_STACK_THIS(this)
HX_STACK_ARG(tileSprite,"tileSprite")
{
	HX_STACK_LINE(37)
	super::__construct(null());
	HX_STACK_LINE(38)
	::flixel::group::FlxGroup _g = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(38)
	this->koiPondGroup = _g;
	HX_STACK_LINE(39)
	::flixel::FlxSprite _g1 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
	HX_STACK_LINE(39)
	this->koiPondSprite = _g1;
	HX_STACK_LINE(40)
	::flixel::FlxSprite _g2 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g2,"_g2");
	HX_STACK_LINE(40)
	this->koiRock = _g2;
	HX_STACK_LINE(41)
	::flixel::FlxSprite _g3 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g3,"_g3");
	HX_STACK_LINE(41)
	this->koiPadSprite = _g3;
	HX_STACK_LINE(42)
	::flixel::FlxSprite _g4 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g4,"_g4");
	HX_STACK_LINE(42)
	this->koiPadSpriteB = _g4;
	HX_STACK_LINE(43)
	::flixel::FlxSprite _g5 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g5,"_g5");
	HX_STACK_LINE(43)
	this->koiPadSpriteC = _g5;
	HX_STACK_LINE(44)
	::flixel::FlxSprite _g6 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g6,"_g6");
	HX_STACK_LINE(44)
	this->koiSandSprite = _g6;
	HX_STACK_LINE(45)
	::flixel::plugin::MouseEventManager_obj::add(this->koiPondSprite,this->splashSounds_dyn(),null(),null(),null(),null(),null(),null());
	HX_STACK_LINE(46)
	::flixel::system::FlxSound _g7 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/koiPondSound.wav"),.05,true,null(),null(),null(),null());		HX_STACK_VAR(_g7,"_g7");
	HX_STACK_LINE(46)
	this->koiPondSound = _g7;
	HX_STACK_LINE(47)
	this->koiPondSound->play(null());
	HX_STACK_LINE(48)
	::flixel::system::FlxSound _g8 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/koiClicked0.wav"),.3,false,null(),null(),null(),null());		HX_STACK_VAR(_g8,"_g8");
	HX_STACK_LINE(48)
	this->koiPondClickedSound0 = _g8;
	HX_STACK_LINE(49)
	::flixel::system::FlxSound _g9 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/koiClicked1.wav"),.3,false,null(),null(),null(),null());		HX_STACK_VAR(_g9,"_g9");
	HX_STACK_LINE(49)
	this->koiPondClickedSound1 = _g9;
	HX_STACK_LINE(50)
	::flixel::system::FlxSound _g10 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/koiClicked2.wav"),.3,false,null(),null(),null(),null());		HX_STACK_VAR(_g10,"_g10");
	HX_STACK_LINE(50)
	this->koiPondClickedSound2 = _g10;
	HX_STACK_LINE(51)
	::flixel::system::FlxSound _g11 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/koiClicked4.wav"),.3,false,null(),null(),null(),null());		HX_STACK_VAR(_g11,"_g11");
	HX_STACK_LINE(51)
	this->koiPondClickedSound3 = _g11;
	HX_STACK_LINE(52)
	this->koiRock->loadGraphic(HX_CSTRING("assets/images/koiRock.png"),false,null(),null(),null(),null());
	HX_STACK_LINE(53)
	this->koiPadSprite->loadGraphic(HX_CSTRING("assets/images/koiPad.png"),false,null(),null(),null(),null());
	HX_STACK_LINE(54)
	this->koiPadSpriteB->loadGraphic(HX_CSTRING("assets/images/koiPad.png"),false,null(),null(),null(),null());
	HX_STACK_LINE(55)
	this->koiPadSpriteC->loadGraphic(HX_CSTRING("assets/images/koiPad.png"),false,null(),null(),null(),null());
	HX_STACK_LINE(56)
	this->koiSandSprite->loadGraphic(HX_CSTRING("assets/images/koiSand.png"),false,null(),null(),null(),null());
	HX_STACK_LINE(57)
	this->koiPondSprite->loadGraphic(HX_CSTRING("assets/images/koiPondAnimation.png"),true,(int)76,(int)72,null(),null());
	HX_STACK_LINE(58)
	this->koiPondSprite->animation->add(HX_CSTRING("koiPondAnimate"),Array_obj< int >::__new().Add((int)0).Add((int)1),(int)5,true);
	HX_STACK_LINE(59)
	this->koiPondSprite->animation->play(HX_CSTRING("koiPondAnimate"),null(),null());
	HX_STACK_LINE(60)
	this->koiPondSprite->set_x((tileSprite->x + (int)60));
	HX_STACK_LINE(61)
	this->koiPondSprite->set_y((tileSprite->y + (int)8));
	HX_STACK_LINE(62)
	this->koiSandSprite->set_x(this->koiPondSprite->x);
	HX_STACK_LINE(63)
	this->koiSandSprite->set_y(this->koiPondSprite->y);
	HX_STACK_LINE(64)
	this->koiRock->set_x((this->koiPondSprite->x + (int)75));
	HX_STACK_LINE(65)
	this->koiRock->set_y((this->koiPondSprite->y + (int)20));
	HX_STACK_LINE(66)
	this->koiPadSprite->set_x((this->koiPondSprite->x - (int)20));
	HX_STACK_LINE(67)
	this->koiPadSprite->set_y((this->koiPondSprite->y + (int)30));
	HX_STACK_LINE(68)
	this->koiPadSpriteB->set_x((this->koiPondSprite->x - (int)18));
	HX_STACK_LINE(69)
	this->koiPadSpriteB->set_y((this->koiPondSprite->y + (int)22));
	HX_STACK_LINE(70)
	this->koiPadSpriteC->set_x((this->koiPondSprite->x - (int)14));
	HX_STACK_LINE(71)
	this->koiPadSpriteC->set_y((this->koiPondSprite->y + (int)39));
	HX_STACK_LINE(72)
	::KoiFish _g12 = ::KoiFish_obj::__new(this->koiPondSprite->x,this->koiPondSprite->y);		HX_STACK_VAR(_g12,"_g12");
	HX_STACK_LINE(72)
	this->koiFish = _g12;
	HX_STACK_LINE(73)
	::KoiFish _g13 = ::KoiFish_obj::__new(this->koiPondSprite->x,this->koiPondSprite->y);		HX_STACK_VAR(_g13,"_g13");
	HX_STACK_LINE(73)
	this->koiFishB = _g13;
	HX_STACK_LINE(74)
	this->koiPondGroup->add(this->koiSandSprite);
	HX_STACK_LINE(75)
	this->koiPondGroup->add(this->koiPondSprite);
	HX_STACK_LINE(76)
	this->koiPondGroup->add(this->koiFish);
	HX_STACK_LINE(77)
	this->koiPondGroup->add(this->koiFishB);
	HX_STACK_LINE(78)
	this->koiPondGroup->add(this->koiRock);
	HX_STACK_LINE(79)
	this->koiPondGroup->add(this->koiPadSpriteB);
	HX_STACK_LINE(80)
	this->koiPondGroup->add(this->koiPadSpriteC);
	HX_STACK_LINE(81)
	this->koiPondGroup->add(this->koiPadSprite);
	HX_STACK_LINE(82)
	this->add(this->koiPondGroup);
}
;
	return null();
}

//KoiPond_obj::~KoiPond_obj() { }

Dynamic KoiPond_obj::__CreateEmpty() { return  new KoiPond_obj; }
hx::ObjectPtr< KoiPond_obj > KoiPond_obj::__new(::Tile tileSprite)
{  hx::ObjectPtr< KoiPond_obj > result = new KoiPond_obj();
	result->__construct(tileSprite);
	return result;}

Dynamic KoiPond_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< KoiPond_obj > result = new KoiPond_obj();
	result->__construct(inArgs[0]);
	return result;}

Void KoiPond_obj::splashSounds( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("KoiPond","splashSounds",0x91f759bf,"KoiPond.splashSounds","KoiPond.hx",86,0x3ad10644)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(87)
		int clickSound = ::flixel::util::FlxRandom_obj::intRanged((int)0,(int)3,null());		HX_STACK_VAR(clickSound,"clickSound");
		HX_STACK_LINE(88)
		switch( (int)(clickSound)){
			case (int)0: {
				HX_STACK_LINE(90)
				this->koiPondClickedSound0->play(null());
			}
			;break;
			case (int)1: {
				HX_STACK_LINE(92)
				this->koiPondClickedSound1->play(null());
			}
			;break;
			case (int)2: {
				HX_STACK_LINE(94)
				this->koiPondClickedSound2->play(null());
			}
			;break;
			case (int)3: {
				HX_STACK_LINE(96)
				this->koiPondClickedSound3->play(null());
			}
			;break;
		}
		HX_STACK_LINE(98)
		this->koiFish->fishScared();
		HX_STACK_LINE(99)
		this->koiFishB->fishScared();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(KoiPond_obj,splashSounds,(void))

Void KoiPond_obj::update( ){
{
		HX_STACK_FRAME("KoiPond","update",0xdfdddb3d,"KoiPond.update","KoiPond.hx",103,0x3ad10644)
		HX_STACK_THIS(this)
		HX_STACK_LINE(103)
		this->super::update();
	}
return null();
}


Void KoiPond_obj::destroy( ){
{
		HX_STACK_FRAME("KoiPond","destroy",0x88f86546,"KoiPond.destroy","KoiPond.hx",106,0x3ad10644)
		HX_STACK_THIS(this)
		HX_STACK_LINE(107)
		::flixel::system::FlxSound _g = ::flixel::util::FlxDestroyUtil_obj::destroy(this->koiPondSound);		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(107)
		this->koiPondSound = _g;
		HX_STACK_LINE(108)
		this->koiPondSprite = null();
		HX_STACK_LINE(109)
		this->koiFish = null();
		HX_STACK_LINE(110)
		this->koiFishB = null();
		HX_STACK_LINE(111)
		this->koiFishSprite = null();
		HX_STACK_LINE(112)
		this->koiPondSound = null();
		HX_STACK_LINE(113)
		this->koiPadSprite = null();
		HX_STACK_LINE(114)
		this->koiPadSpriteB = null();
		HX_STACK_LINE(115)
		this->koiPadSpriteC = null();
		HX_STACK_LINE(116)
		this->koiRock = null();
		HX_STACK_LINE(117)
		this->koiSandSprite = null();
		HX_STACK_LINE(118)
		this->koiPondClickedSound0 = null();
		HX_STACK_LINE(119)
		this->koiPondClickedSound1 = null();
		HX_STACK_LINE(120)
		this->koiPondClickedSound2 = null();
		HX_STACK_LINE(121)
		this->koiPondClickedSound3 = null();
		HX_STACK_LINE(122)
		this->koiPondGroup = null();
		HX_STACK_LINE(123)
		this->super::destroy();
	}
return null();
}



KoiPond_obj::KoiPond_obj()
{
}

void KoiPond_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(KoiPond);
	HX_MARK_MEMBER_NAME(koiPondGroup,"koiPondGroup");
	HX_MARK_MEMBER_NAME(koiPondSprite,"koiPondSprite");
	HX_MARK_MEMBER_NAME(koiRock,"koiRock");
	HX_MARK_MEMBER_NAME(koiFishSprite,"koiFishSprite");
	HX_MARK_MEMBER_NAME(koiPadSprite,"koiPadSprite");
	HX_MARK_MEMBER_NAME(koiPadSpriteB,"koiPadSpriteB");
	HX_MARK_MEMBER_NAME(koiPadSpriteC,"koiPadSpriteC");
	HX_MARK_MEMBER_NAME(koiSandSprite,"koiSandSprite");
	HX_MARK_MEMBER_NAME(koiFish,"koiFish");
	HX_MARK_MEMBER_NAME(koiFishB,"koiFishB");
	HX_MARK_MEMBER_NAME(koiPondSound,"koiPondSound");
	HX_MARK_MEMBER_NAME(koiPondClickedSound0,"koiPondClickedSound0");
	HX_MARK_MEMBER_NAME(koiPondClickedSound1,"koiPondClickedSound1");
	HX_MARK_MEMBER_NAME(koiPondClickedSound2,"koiPondClickedSound2");
	HX_MARK_MEMBER_NAME(koiPondClickedSound3,"koiPondClickedSound3");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void KoiPond_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(koiPondGroup,"koiPondGroup");
	HX_VISIT_MEMBER_NAME(koiPondSprite,"koiPondSprite");
	HX_VISIT_MEMBER_NAME(koiRock,"koiRock");
	HX_VISIT_MEMBER_NAME(koiFishSprite,"koiFishSprite");
	HX_VISIT_MEMBER_NAME(koiPadSprite,"koiPadSprite");
	HX_VISIT_MEMBER_NAME(koiPadSpriteB,"koiPadSpriteB");
	HX_VISIT_MEMBER_NAME(koiPadSpriteC,"koiPadSpriteC");
	HX_VISIT_MEMBER_NAME(koiSandSprite,"koiSandSprite");
	HX_VISIT_MEMBER_NAME(koiFish,"koiFish");
	HX_VISIT_MEMBER_NAME(koiFishB,"koiFishB");
	HX_VISIT_MEMBER_NAME(koiPondSound,"koiPondSound");
	HX_VISIT_MEMBER_NAME(koiPondClickedSound0,"koiPondClickedSound0");
	HX_VISIT_MEMBER_NAME(koiPondClickedSound1,"koiPondClickedSound1");
	HX_VISIT_MEMBER_NAME(koiPondClickedSound2,"koiPondClickedSound2");
	HX_VISIT_MEMBER_NAME(koiPondClickedSound3,"koiPondClickedSound3");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic KoiPond_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"koiRock") ) { return koiRock; }
		if (HX_FIELD_EQ(inName,"koiFish") ) { return koiFish; }
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"koiFishB") ) { return koiFishB; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"koiPondGroup") ) { return koiPondGroup; }
		if (HX_FIELD_EQ(inName,"koiPadSprite") ) { return koiPadSprite; }
		if (HX_FIELD_EQ(inName,"koiPondSound") ) { return koiPondSound; }
		if (HX_FIELD_EQ(inName,"splashSounds") ) { return splashSounds_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"koiPondSprite") ) { return koiPondSprite; }
		if (HX_FIELD_EQ(inName,"koiFishSprite") ) { return koiFishSprite; }
		if (HX_FIELD_EQ(inName,"koiPadSpriteB") ) { return koiPadSpriteB; }
		if (HX_FIELD_EQ(inName,"koiPadSpriteC") ) { return koiPadSpriteC; }
		if (HX_FIELD_EQ(inName,"koiSandSprite") ) { return koiSandSprite; }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"koiPondClickedSound0") ) { return koiPondClickedSound0; }
		if (HX_FIELD_EQ(inName,"koiPondClickedSound1") ) { return koiPondClickedSound1; }
		if (HX_FIELD_EQ(inName,"koiPondClickedSound2") ) { return koiPondClickedSound2; }
		if (HX_FIELD_EQ(inName,"koiPondClickedSound3") ) { return koiPondClickedSound3; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic KoiPond_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 7:
		if (HX_FIELD_EQ(inName,"koiRock") ) { koiRock=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"koiFish") ) { koiFish=inValue.Cast< ::KoiFish >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"koiFishB") ) { koiFishB=inValue.Cast< ::KoiFish >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"koiPondGroup") ) { koiPondGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"koiPadSprite") ) { koiPadSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"koiPondSound") ) { koiPondSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"koiPondSprite") ) { koiPondSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"koiFishSprite") ) { koiFishSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"koiPadSpriteB") ) { koiPadSpriteB=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"koiPadSpriteC") ) { koiPadSpriteC=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"koiSandSprite") ) { koiSandSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"koiPondClickedSound0") ) { koiPondClickedSound0=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		if (HX_FIELD_EQ(inName,"koiPondClickedSound1") ) { koiPondClickedSound1=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		if (HX_FIELD_EQ(inName,"koiPondClickedSound2") ) { koiPondClickedSound2=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		if (HX_FIELD_EQ(inName,"koiPondClickedSound3") ) { koiPondClickedSound3=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void KoiPond_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("koiPondGroup"));
	outFields->push(HX_CSTRING("koiPondSprite"));
	outFields->push(HX_CSTRING("koiRock"));
	outFields->push(HX_CSTRING("koiFishSprite"));
	outFields->push(HX_CSTRING("koiPadSprite"));
	outFields->push(HX_CSTRING("koiPadSpriteB"));
	outFields->push(HX_CSTRING("koiPadSpriteC"));
	outFields->push(HX_CSTRING("koiSandSprite"));
	outFields->push(HX_CSTRING("koiFish"));
	outFields->push(HX_CSTRING("koiFishB"));
	outFields->push(HX_CSTRING("koiPondSound"));
	outFields->push(HX_CSTRING("koiPondClickedSound0"));
	outFields->push(HX_CSTRING("koiPondClickedSound1"));
	outFields->push(HX_CSTRING("koiPondClickedSound2"));
	outFields->push(HX_CSTRING("koiPondClickedSound3"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(KoiPond_obj,koiPondGroup),HX_CSTRING("koiPondGroup")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(KoiPond_obj,koiPondSprite),HX_CSTRING("koiPondSprite")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(KoiPond_obj,koiRock),HX_CSTRING("koiRock")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(KoiPond_obj,koiFishSprite),HX_CSTRING("koiFishSprite")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(KoiPond_obj,koiPadSprite),HX_CSTRING("koiPadSprite")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(KoiPond_obj,koiPadSpriteB),HX_CSTRING("koiPadSpriteB")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(KoiPond_obj,koiPadSpriteC),HX_CSTRING("koiPadSpriteC")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(KoiPond_obj,koiSandSprite),HX_CSTRING("koiSandSprite")},
	{hx::fsObject /*::KoiFish*/ ,(int)offsetof(KoiPond_obj,koiFish),HX_CSTRING("koiFish")},
	{hx::fsObject /*::KoiFish*/ ,(int)offsetof(KoiPond_obj,koiFishB),HX_CSTRING("koiFishB")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(KoiPond_obj,koiPondSound),HX_CSTRING("koiPondSound")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(KoiPond_obj,koiPondClickedSound0),HX_CSTRING("koiPondClickedSound0")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(KoiPond_obj,koiPondClickedSound1),HX_CSTRING("koiPondClickedSound1")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(KoiPond_obj,koiPondClickedSound2),HX_CSTRING("koiPondClickedSound2")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(KoiPond_obj,koiPondClickedSound3),HX_CSTRING("koiPondClickedSound3")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("koiPondGroup"),
	HX_CSTRING("koiPondSprite"),
	HX_CSTRING("koiRock"),
	HX_CSTRING("koiFishSprite"),
	HX_CSTRING("koiPadSprite"),
	HX_CSTRING("koiPadSpriteB"),
	HX_CSTRING("koiPadSpriteC"),
	HX_CSTRING("koiSandSprite"),
	HX_CSTRING("koiFish"),
	HX_CSTRING("koiFishB"),
	HX_CSTRING("koiPondSound"),
	HX_CSTRING("koiPondClickedSound0"),
	HX_CSTRING("koiPondClickedSound1"),
	HX_CSTRING("koiPondClickedSound2"),
	HX_CSTRING("koiPondClickedSound3"),
	HX_CSTRING("splashSounds"),
	HX_CSTRING("update"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(KoiPond_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(KoiPond_obj::__mClass,"__mClass");
};

#endif

Class KoiPond_obj::__mClass;

void KoiPond_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("KoiPond"), hx::TCanCast< KoiPond_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void KoiPond_obj::__boot()
{
}

