#include <hxcpp.h>

#ifndef INCLUDED_Apple
#include <Apple.h>
#endif
#ifndef INCLUDED_Greenhouse
#include <Greenhouse.h>
#endif
#ifndef INCLUDED_MenuState
#include <MenuState.h>
#endif
#ifndef INCLUDED_Mouse
#include <Mouse.h>
#endif
#ifndef INCLUDED_Tile
#include <Tile.h>
#endif
#ifndef INCLUDED_Tree
#include <Tree.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxCamera
#include <flixel/FlxCamera.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_FlxState
#include <flixel/FlxState.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_plugin_FlxPlugin
#include <flixel/plugin/FlxPlugin.h>
#endif
#ifndef INCLUDED_flixel_plugin_MouseEventManager
#include <flixel/plugin/MouseEventManager.h>
#endif
#ifndef INCLUDED_flixel_system_FlxSound
#include <flixel/system/FlxSound.h>
#endif
#ifndef INCLUDED_flixel_system_frontEnds_SoundFrontEnd
#include <flixel/system/frontEnds/SoundFrontEnd.h>
#endif
#ifndef INCLUDED_flixel_tweens_FlxTween
#include <flixel/tweens/FlxTween.h>
#endif
#ifndef INCLUDED_flixel_tweens_misc_VarTween
#include <flixel/tweens/misc/VarTween.h>
#endif
#ifndef INCLUDED_flixel_util_FlxCollision
#include <flixel/util/FlxCollision.h>
#endif
#ifndef INCLUDED_flixel_util_FlxDestroyUtil
#include <flixel/util/FlxDestroyUtil.h>
#endif
#ifndef INCLUDED_flixel_util_FlxTimer
#include <flixel/util/FlxTimer.h>
#endif
#ifndef INCLUDED_openfl__v2_events_EventDispatcher
#include <openfl/_v2/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__v2_events_IEventDispatcher
#include <openfl/_v2/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__v2_media_SoundChannel
#include <openfl/_v2/media/SoundChannel.h>
#endif

Void Tree_obj::__construct(::flixel::FlxSprite basket)
{
HX_STACK_FRAME("Tree","new",0xf15bfd70,"Tree.new","Tree.hx",22,0xa4188000)
HX_STACK_THIS(this)
HX_STACK_ARG(basket,"basket")
{
	HX_STACK_LINE(92)
	this->sunAlertUsed = false;
	HX_STACK_LINE(91)
	this->dropAlertUsed = false;
	HX_STACK_LINE(90)
	this->greenHouseInPlay = false;
	HX_STACK_LINE(89)
	this->mathless = false;
	HX_STACK_LINE(88)
	this->deathRattle = false;
	HX_STACK_LINE(87)
	this->appleStored = false;
	HX_STACK_LINE(86)
	this->payWaterBill = false;
	HX_STACK_LINE(85)
	this->extendedTime = true;
	HX_STACK_LINE(84)
	this->sprinklerInPhase = true;
	HX_STACK_LINE(83)
	this->sprinklerOutPhase = true;
	HX_STACK_LINE(82)
	this->sprinklerSystemOn = false;
	HX_STACK_LINE(81)
	this->checkingCollisions = true;
	HX_STACK_LINE(80)
	this->takenAppleCarried = false;
	HX_STACK_LINE(79)
	this->appleClicked = false;
	HX_STACK_LINE(78)
	this->amIdeadYet = false;
	HX_STACK_LINE(77)
	this->appleLock = true;
	HX_STACK_LINE(76)
	this->treeClicked = false;
	HX_STACK_LINE(75)
	this->sunClicked = false;
	HX_STACK_LINE(74)
	this->dropletClicked = false;
	HX_STACK_LINE(73)
	this->mouseInPlay = false;
	HX_STACK_LINE(72)
	this->oldSunTimerChanged = false;
	HX_STACK_LINE(69)
	this->sprinklerAnimationTimer = (int)0;
	HX_STACK_LINE(68)
	this->dropletAlertCoolDown = (int)0;
	HX_STACK_LINE(67)
	this->sunAlertCoolDown = (int)0;
	HX_STACK_LINE(66)
	this->mouseTimer = (int)0;
	HX_STACK_LINE(65)
	this->appleSeperator = (int)0;
	HX_STACK_LINE(64)
	this->timeToTreeDeath = (int)10;
	HX_STACK_LINE(63)
	this->sprinklerOutTime = (int)2;
	HX_STACK_LINE(62)
	this->waterUtlilityTimer = (int)30;
	HX_STACK_LINE(61)
	this->oldTreeSunLightTimer = (int)0;
	HX_STACK_LINE(60)
	this->treeSunLightTimer = (int)10;
	HX_STACK_LINE(59)
	this->treeThirstTimer = (int)15;
	HX_STACK_LINE(58)
	this->alertSunPauseTimer = (int)1;
	HX_STACK_LINE(57)
	this->alertDropPauseTimer = (int)1;
	HX_STACK_LINE(54)
	this->adultTree = (int)6;
	HX_STACK_LINE(53)
	this->appleIndex = (int)0;
	HX_STACK_LINE(52)
	this->appleCount = (int)0;
	HX_STACK_LINE(51)
	this->treeSunlight = (int)0;
	HX_STACK_LINE(50)
	this->treeThirst = (int)1;
	HX_STACK_LINE(49)
	this->treeGrowthPhase = (int)0;
	HX_STACK_LINE(48)
	this->minThirst = (int)-3;
	HX_STACK_LINE(47)
	this->maxThirst = (int)3;
	HX_STACK_LINE(25)
	this->treeGroup = ::flixel::group::FlxGroup_obj::__new(null());
	HX_STACK_LINE(97)
	super::__construct(null());
	HX_STACK_LINE(98)
	Array< ::Dynamic > _g = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(98)
	this->appleArray = _g;
	HX_STACK_LINE(99)
	::flixel::util::FlxTimer _g1 = ::flixel::util::FlxTimer_obj::__new(null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
	HX_STACK_LINE(99)
	this->dropAlertTime = _g1;
	HX_STACK_LINE(100)
	::flixel::util::FlxTimer _g2 = ::flixel::util::FlxTimer_obj::__new(null(),null(),null());		HX_STACK_VAR(_g2,"_g2");
	HX_STACK_LINE(100)
	this->sunlightAlertTime = _g2;
	HX_STACK_LINE(101)
	::flixel::group::FlxGroup _g3 = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g3,"_g3");
	HX_STACK_LINE(101)
	this->boardObj = _g3;
	HX_STACK_LINE(102)
	::flixel::FlxSprite _g4 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g4,"_g4");
	HX_STACK_LINE(102)
	this->treeGround = _g4;
	HX_STACK_LINE(103)
	::flixel::FlxSprite _g5 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g5,"_g5");
	HX_STACK_LINE(103)
	this->tree = _g5;
	HX_STACK_LINE(104)
	::flixel::FlxSprite _g6 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g6,"_g6");
	HX_STACK_LINE(104)
	this->droplet = _g6;
	HX_STACK_LINE(105)
	::flixel::FlxSprite _g7 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g7,"_g7");
	HX_STACK_LINE(105)
	this->dropletA = _g7;
	HX_STACK_LINE(106)
	::flixel::FlxSprite _g8 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g8,"_g8");
	HX_STACK_LINE(106)
	this->dropletB = _g8;
	HX_STACK_LINE(107)
	::flixel::FlxSprite _g9 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g9,"_g9");
	HX_STACK_LINE(107)
	this->sunA = _g9;
	HX_STACK_LINE(108)
	::flixel::FlxSprite _g10 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g10,"_g10");
	HX_STACK_LINE(108)
	this->sunB = _g10;
	HX_STACK_LINE(109)
	::flixel::FlxSprite _g11 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g11,"_g11");
	HX_STACK_LINE(109)
	this->sunC = _g11;
	HX_STACK_LINE(110)
	::flixel::FlxSprite _g12 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g12,"_g12");
	HX_STACK_LINE(110)
	this->sprinklerSprite = _g12;
	HX_STACK_LINE(111)
	::flixel::FlxSprite _g13 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g13,"_g13");
	HX_STACK_LINE(111)
	this->minusOneSprite = _g13;
	HX_STACK_LINE(112)
	this->mathless = ::MenuState_obj::mathless;
	HX_STACK_LINE(113)
	this->basketSprite = basket;
	HX_STACK_LINE(114)
	this->minusOneSprite->loadGraphic(HX_CSTRING("assets/images/minusOneImage.png"),null(),null(),null(),null(),null());
	HX_STACK_LINE(115)
	this->minusOneSprite->set_alpha((int)0);
	HX_STACK_LINE(116)
	this->sprinklerSprite->loadGraphic(HX_CSTRING("assets/images/sprinklerAnimation.png"),true,(int)64,(int)64,null(),null());
	HX_STACK_LINE(117)
	this->sprinklerSprite->animation->add(HX_CSTRING("sprinklerOut"),Array_obj< int >::__new().Add((int)0).Add((int)1).Add((int)2).Add((int)3),(int)5,false);
	HX_STACK_LINE(118)
	this->sprinklerSprite->animation->add(HX_CSTRING("sprinklerSpray"),Array_obj< int >::__new().Add((int)4).Add((int)5).Add((int)6).Add((int)7).Add((int)8).Add((int)9),(int)10,true);
	HX_STACK_LINE(119)
	this->sprinklerSprite->animation->add(HX_CSTRING("sprinklerIn"),Array_obj< int >::__new().Add((int)3).Add((int)2).Add((int)1).Add((int)0),(int)10,false);
	HX_STACK_LINE(120)
	::flixel::FlxSprite _g14 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g14,"_g14");
	HX_STACK_LINE(120)
	this->sprinklerButtonSprite = _g14;
	HX_STACK_LINE(121)
	this->sprinklerButtonSprite->loadGraphic(HX_CSTRING("assets/images/sprinklerSwtichAnimation.png"),true,(int)16,(int)30,null(),null());
	HX_STACK_LINE(122)
	this->sprinklerButtonSprite->animation->add(HX_CSTRING("sprinklerSwitchOn"),Array_obj< int >::__new().Add((int)1),(int)1,false);
	HX_STACK_LINE(123)
	this->sprinklerButtonSprite->animation->add(HX_CSTRING("sprinklerSwitchOff"),Array_obj< int >::__new().Add((int)0),(int)1,false);
	HX_STACK_LINE(124)
	this->sprinklerButtonSprite->animation->play(HX_CSTRING("sprinklerSwitchOff"),null(),null());
	HX_STACK_LINE(125)
	::flixel::plugin::MouseEventManager_obj::add(this->sprinklerButtonSprite,this->purchaseSprinklerBuff_dyn(),null(),null(),null(),null(),null(),null());
	HX_STACK_LINE(126)
	this->sprinklerSprite->set_alpha((int)0);
	HX_STACK_LINE(127)
	this->sprinklerButtonSprite->set_alpha((int)0);
	HX_STACK_LINE(128)
	this->treeGround->makeGraphic((int)100,(int)2,null(),null(),null());
	HX_STACK_LINE(129)
	this->treeGround->set_alpha((int)1);
	HX_STACK_LINE(130)
	this->tree->loadGraphic(HX_CSTRING("assets/images/treeAnimationB.png"),true,(int)71,(int)74,null(),null());
	HX_STACK_LINE(131)
	this->droplet->loadGraphic(HX_CSTRING("assets/images/dropletsAnimationS.png"),true,(int)18,(int)18,null(),null());
	HX_STACK_LINE(132)
	this->droplet->animation->add(HX_CSTRING("droplet"),Array_obj< int >::__new().Add((int)0).Add((int)1).Add((int)2).Add((int)3).Add((int)4).Add((int)5).Add((int)6).Add((int)7).Add((int)8),(int)15,true);
	HX_STACK_LINE(133)
	this->dropletA->loadGraphic(HX_CSTRING("assets/images/dropletsAnimationT.png"),true,(int)18,(int)18,null(),null());
	HX_STACK_LINE(134)
	this->dropletA->animation->add(HX_CSTRING("dropletA"),Array_obj< int >::__new().Add((int)0).Add((int)1).Add((int)2).Add((int)3).Add((int)4).Add((int)5).Add((int)6).Add((int)7).Add((int)8),(int)15,true);
	HX_STACK_LINE(135)
	this->dropletB->loadGraphic(HX_CSTRING("assets/images/dropletsAnimationU.png"),true,(int)18,(int)18,null(),null());
	HX_STACK_LINE(136)
	this->dropletB->animation->add(HX_CSTRING("dropletB"),Array_obj< int >::__new().Add((int)0).Add((int)1).Add((int)2).Add((int)3).Add((int)4).Add((int)5).Add((int)6).Add((int)7).Add((int)8),(int)15,true);
	HX_STACK_LINE(137)
	this->sunA->loadGraphic(HX_CSTRING("assets/images/sunAnimationA.png"),true,(int)18,(int)18,null(),null());
	HX_STACK_LINE(138)
	this->sunA->animation->add(HX_CSTRING("sunA"),Array_obj< int >::__new().Add((int)0).Add((int)1).Add((int)2).Add((int)3).Add((int)4).Add((int)5),(int)10,true);
	HX_STACK_LINE(139)
	this->sunB->loadGraphic(HX_CSTRING("assets/images/sunAnimationB.png"),true,(int)18,(int)18,null(),null());
	HX_STACK_LINE(140)
	this->sunB->animation->add(HX_CSTRING("sunB"),Array_obj< int >::__new().Add((int)0).Add((int)1).Add((int)2).Add((int)3).Add((int)4).Add((int)5),(int)10,true);
	HX_STACK_LINE(141)
	this->sunC->loadGraphic(HX_CSTRING("assets/images/sunAnimationC.png"),true,(int)18,(int)18,null(),null());
	HX_STACK_LINE(142)
	this->sunC->animation->add(HX_CSTRING("sunC"),Array_obj< int >::__new().Add((int)0).Add((int)1).Add((int)2).Add((int)3).Add((int)4).Add((int)5),(int)10,true);
	HX_STACK_LINE(143)
	this->tree->animation->add(HX_CSTRING("0"),Array_obj< int >::__new().Add((int)0),(int)1,false);
	HX_STACK_LINE(144)
	this->tree->animation->add(HX_CSTRING("1"),Array_obj< int >::__new().Add((int)1).Add((int)2).Add((int)3),(int)1,false);
	HX_STACK_LINE(145)
	this->tree->animation->add(HX_CSTRING("2"),Array_obj< int >::__new().Add((int)4).Add((int)5).Add((int)6).Add((int)7),(int)1,false);
	HX_STACK_LINE(146)
	this->tree->animation->add(HX_CSTRING("3"),Array_obj< int >::__new().Add((int)8).Add((int)9).Add((int)10).Add((int)11),(int)1,false);
	HX_STACK_LINE(147)
	this->tree->animation->add(HX_CSTRING("4"),Array_obj< int >::__new().Add((int)12).Add((int)13).Add((int)14).Add((int)15),(int)1,false);
	HX_STACK_LINE(148)
	this->tree->animation->add(HX_CSTRING("5"),Array_obj< int >::__new().Add((int)16).Add((int)17).Add((int)18).Add((int)19),(int)1,false);
	HX_STACK_LINE(149)
	this->tree->animation->add(HX_CSTRING("6"),Array_obj< int >::__new().Add((int)20).Add((int)21).Add((int)22).Add((int)23),(int)1,false);
	HX_STACK_LINE(150)
	this->tree->animation->add(HX_CSTRING("7"),Array_obj< int >::__new().Add((int)24),(int)1,false);
	HX_STACK_LINE(151)
	this->tree->animation->add(HX_CSTRING("8"),Array_obj< int >::__new().Add((int)25),(int)1,false);
	HX_STACK_LINE(152)
	this->tree->animation->add(HX_CSTRING("9"),Array_obj< int >::__new().Add((int)26),(int)1,false);
	HX_STACK_LINE(153)
	this->droplet->set_alpha((int)0);
	HX_STACK_LINE(154)
	this->sunA->set_alpha((int)0);
	HX_STACK_LINE(155)
	this->sunB->set_alpha((int)0);
	HX_STACK_LINE(156)
	this->sunC->set_alpha((int)0);
	HX_STACK_LINE(157)
	this->initAppleArray();
	HX_STACK_LINE(158)
	this->registerAlertEvents();
	HX_STACK_LINE(159)
	this->growTree();
	HX_STACK_LINE(160)
	this->treeGroup->add(this->sprinklerButtonSprite);
	HX_STACK_LINE(161)
	this->treeGroup->add(this->tree);
	HX_STACK_LINE(162)
	this->treeGroup->add(this->dropletB);
	HX_STACK_LINE(163)
	this->treeGroup->add(this->dropletA);
	HX_STACK_LINE(164)
	this->treeGroup->add(this->droplet);
	HX_STACK_LINE(165)
	this->treeGroup->add(this->sunC);
	HX_STACK_LINE(166)
	this->treeGroup->add(this->sunB);
	HX_STACK_LINE(167)
	this->treeGroup->add(this->sunA);
	HX_STACK_LINE(168)
	this->treeGroup->add(this->boardObj);
	HX_STACK_LINE(169)
	this->treeGroup->add(this->treeGround);
	HX_STACK_LINE(170)
	this->treeGroup->add(this->minusOneSprite);
	HX_STACK_LINE(171)
	this->treeGroup->add(this->sprinklerSprite);
	HX_STACK_LINE(172)
	this->add(this->treeGroup);
	HX_STACK_LINE(173)
	this->dropAlertTime->start(this->treeThirstTimer,this->setTreeThirsty_dyn(),(int)0);
	HX_STACK_LINE(174)
	this->sunlightAlertTime->start(this->treeSunLightTimer,this->setTreeNoLight_dyn(),(int)0);
}
;
	return null();
}

//Tree_obj::~Tree_obj() { }

Dynamic Tree_obj::__CreateEmpty() { return  new Tree_obj; }
hx::ObjectPtr< Tree_obj > Tree_obj::__new(::flixel::FlxSprite basket)
{  hx::ObjectPtr< Tree_obj > result = new Tree_obj();
	result->__construct(basket);
	return result;}

Dynamic Tree_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Tree_obj > result = new Tree_obj();
	result->__construct(inArgs[0]);
	return result;}

bool Tree_obj::getTreeClicked( ){
	HX_STACK_FRAME("Tree","getTreeClicked",0xfd672ce3,"Tree.getTreeClicked","Tree.hx",178,0xa4188000)
	HX_STACK_THIS(this)
	HX_STACK_LINE(178)
	return this->treeClicked;
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,getTreeClicked,return )

::flixel::FlxSprite Tree_obj::getTreeSprite( ){
	HX_STACK_FRAME("Tree","getTreeSprite",0x1083c149,"Tree.getTreeSprite","Tree.hx",182,0xa4188000)
	HX_STACK_THIS(this)
	HX_STACK_LINE(182)
	return this->tree;
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,getTreeSprite,return )

Float Tree_obj::getTreeX( ){
	HX_STACK_FRAME("Tree","getTreeX",0x3ce421b4,"Tree.getTreeX","Tree.hx",187,0xa4188000)
	HX_STACK_THIS(this)
	HX_STACK_LINE(187)
	return this->tree->x;
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,getTreeX,return )

Float Tree_obj::getTreeY( ){
	HX_STACK_FRAME("Tree","getTreeY",0x3ce421b5,"Tree.getTreeY","Tree.hx",193,0xa4188000)
	HX_STACK_THIS(this)
	HX_STACK_LINE(193)
	return this->tree->y;
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,getTreeY,return )

bool Tree_obj::getAmIDeadYet( ){
	HX_STACK_FRAME("Tree","getAmIDeadYet",0x9499a1cd,"Tree.getAmIDeadYet","Tree.hx",198,0xa4188000)
	HX_STACK_THIS(this)
	HX_STACK_LINE(198)
	return this->amIdeadYet;
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,getAmIDeadYet,return )

int Tree_obj::getTreeThirst( ){
	HX_STACK_FRAME("Tree","getTreeThirst",0xd5be6182,"Tree.getTreeThirst","Tree.hx",203,0xa4188000)
	HX_STACK_THIS(this)
	HX_STACK_LINE(203)
	return this->treeThirst;
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,getTreeThirst,return )

bool Tree_obj::getAppleStored( ){
	HX_STACK_FRAME("Tree","getAppleStored",0x715aea57,"Tree.getAppleStored","Tree.hx",208,0xa4188000)
	HX_STACK_THIS(this)
	HX_STACK_LINE(208)
	return this->appleStored;
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,getAppleStored,return )

Void Tree_obj::setAppleStored( bool val){
{
		HX_STACK_FRAME("Tree","setAppleStored",0x917ad2cb,"Tree.setAppleStored","Tree.hx",212,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(212)
		this->appleStored = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tree_obj,setAppleStored,(void))

bool Tree_obj::getDropletClicked( ){
	HX_STACK_FRAME("Tree","getDropletClicked",0xb608d541,"Tree.getDropletClicked","Tree.hx",217,0xa4188000)
	HX_STACK_THIS(this)
	HX_STACK_LINE(217)
	return this->dropletClicked;
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,getDropletClicked,return )

bool Tree_obj::getSunClicked( ){
	HX_STACK_FRAME("Tree","getSunClicked",0x332efbc1,"Tree.getSunClicked","Tree.hx",223,0xa4188000)
	HX_STACK_THIS(this)
	HX_STACK_LINE(223)
	return this->sunClicked;
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,getSunClicked,return )

bool Tree_obj::getUniversalAlertClicked( ){
	HX_STACK_FRAME("Tree","getUniversalAlertClicked",0xb1bf8950,"Tree.getUniversalAlertClicked","Tree.hx",229,0xa4188000)
	HX_STACK_THIS(this)
	HX_STACK_LINE(229)
	return ::Tree_obj::universalAlertClicked;
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,getUniversalAlertClicked,return )

::Tile Tree_obj::getMyTile( ){
	HX_STACK_FRAME("Tree","getMyTile",0x3ace5f00,"Tree.getMyTile","Tree.hx",234,0xa4188000)
	HX_STACK_THIS(this)
	HX_STACK_LINE(234)
	return this->myTile;
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,getMyTile,return )

bool Tree_obj::getAppleClicked( ){
	HX_STACK_FRAME("Tree","getAppleClicked",0x73b2b333,"Tree.getAppleClicked","Tree.hx",238,0xa4188000)
	HX_STACK_THIS(this)
	HX_STACK_LINE(238)
	return this->appleClicked;
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,getAppleClicked,return )

int Tree_obj::getTreeGrowthPhase( ){
	HX_STACK_FRAME("Tree","getTreeGrowthPhase",0xeb8c8eb0,"Tree.getTreeGrowthPhase","Tree.hx",242,0xa4188000)
	HX_STACK_THIS(this)
	HX_STACK_LINE(242)
	return this->treeGrowthPhase;
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,getTreeGrowthPhase,return )

bool Tree_obj::getSprinklerSystemOn( ){
	HX_STACK_FRAME("Tree","getSprinklerSystemOn",0x48727f30,"Tree.getSprinklerSystemOn","Tree.hx",246,0xa4188000)
	HX_STACK_THIS(this)
	HX_STACK_LINE(246)
	return this->sprinklerSystemOn;
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,getSprinklerSystemOn,return )

::flixel::FlxSprite Tree_obj::getSprinklerButtonSprite( ){
	HX_STACK_FRAME("Tree","getSprinklerButtonSprite",0xc0e745d9,"Tree.getSprinklerButtonSprite","Tree.hx",250,0xa4188000)
	HX_STACK_THIS(this)
	HX_STACK_LINE(250)
	return this->sprinklerButtonSprite;
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,getSprinklerButtonSprite,return )

bool Tree_obj::getPayWaterBill( ){
	HX_STACK_FRAME("Tree","getPayWaterBill",0x92ce8bfc,"Tree.getPayWaterBill","Tree.hx",254,0xa4188000)
	HX_STACK_THIS(this)
	HX_STACK_LINE(254)
	return this->payWaterBill;
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,getPayWaterBill,return )

Array< ::Dynamic > Tree_obj::getAppleArray( ){
	HX_STACK_FRAME("Tree","getAppleArray",0xdff92f25,"Tree.getAppleArray","Tree.hx",258,0xa4188000)
	HX_STACK_THIS(this)
	HX_STACK_LINE(258)
	return this->appleArray;
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,getAppleArray,return )

Void Tree_obj::setTreeThirstTut( int tutThirstTimer){
{
		HX_STACK_FRAME("Tree","setTreeThirstTut",0x9f240ce5,"Tree.setTreeThirstTut","Tree.hx",264,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_ARG(tutThirstTimer,"tutThirstTimer")
		HX_STACK_LINE(265)
		this->dropAlertTime->cancel_dyn();
		HX_STACK_LINE(266)
		this->dropAlertTime->start(tutThirstTimer,this->setTreeThirsty_dyn(),(int)0);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tree_obj,setTreeThirstTut,(void))

Void Tree_obj::setTreeSunlightTut( int tutSunlightTimer){
{
		HX_STACK_FRAME("Tree","setTreeSunlightTut",0x752708d9,"Tree.setTreeSunlightTut","Tree.hx",269,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_ARG(tutSunlightTimer,"tutSunlightTimer")
		HX_STACK_LINE(270)
		this->sunlightAlertTime->cancel_dyn();
		HX_STACK_LINE(271)
		this->sunlightAlertTime->start(tutSunlightTimer,this->setTreeNoLight_dyn(),(int)0);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tree_obj,setTreeSunlightTut,(void))

Void Tree_obj::setTreeThistTutOff( ){
{
		HX_STACK_FRAME("Tree","setTreeThistTutOff",0x50bd6102,"Tree.setTreeThistTutOff","Tree.hx",275,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(275)
		this->dropAlertTime->start(this->treeThirstTimer,this->setTreeThirsty_dyn(),(int)0);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,setTreeThistTutOff,(void))

Void Tree_obj::setTreeSunlightTutOff( ){
{
		HX_STACK_FRAME("Tree","setTreeSunlightTutOff",0xd6999016,"Tree.setTreeSunlightTutOff","Tree.hx",279,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(279)
		this->sunlightAlertTime->start(this->treeSunLightTimer,this->setTreeNoLight_dyn(),(int)0);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,setTreeSunlightTutOff,(void))

Void Tree_obj::setPayWaterBill( bool val){
{
		HX_STACK_FRAME("Tree","setPayWaterBill",0x8e9a0908,"Tree.setPayWaterBill","Tree.hx",283,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(283)
		this->payWaterBill = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tree_obj,setPayWaterBill,(void))

Void Tree_obj::setSprinklerSystemOn( bool val){
{
		HX_STACK_FRAME("Tree","setSprinklerSystemOn",0x152a36a4,"Tree.setSprinklerSystemOn","Tree.hx",287,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(287)
		this->sprinklerSystemOn = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tree_obj,setSprinklerSystemOn,(void))

Void Tree_obj::setTreeClicked( bool val){
{
		HX_STACK_FRAME("Tree","setTreeClicked",0x1d871557,"Tree.setTreeClicked","Tree.hx",291,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(291)
		this->treeClicked = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tree_obj,setTreeClicked,(void))

Void Tree_obj::setAppleClicked( bool val){
{
		HX_STACK_FRAME("Tree","setAppleClicked",0x6f7e303f,"Tree.setAppleClicked","Tree.hx",295,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(295)
		this->appleClicked = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tree_obj,setAppleClicked,(void))

Void Tree_obj::setMyTile( ::Tile val){
{
		HX_STACK_FRAME("Tree","setMyTile",0x1e1f4b0c,"Tree.setMyTile","Tree.hx",299,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(299)
		this->myTile = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tree_obj,setMyTile,(void))

Void Tree_obj::setTreeGroundColor( bool owned){
{
		HX_STACK_FRAME("Tree","setTreeGroundColor",0x4b5bec0c,"Tree.setTreeGroundColor","Tree.hx",303,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_ARG(owned,"owned")
		HX_STACK_LINE(303)
		if ((owned)){
			HX_STACK_LINE(304)
			this->treeGround->set_color((int)4108077);
		}
		else{
			HX_STACK_LINE(307)
			this->treeGround->set_color((int)1010945);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tree_obj,setTreeGroundColor,(void))

Void Tree_obj::setTreeGrowthPhase( int val){
{
		HX_STACK_FRAME("Tree","setTreeGrowthPhase",0xc83bc124,"Tree.setTreeGrowthPhase","Tree.hx",312,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(312)
		this->treeGrowthPhase = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tree_obj,setTreeGrowthPhase,(void))

Void Tree_obj::setUniversalAlertClicked( bool val){
{
		HX_STACK_FRAME("Tree","setUniversalAlertClicked",0xc49a0ac4,"Tree.setUniversalAlertClicked","Tree.hx",317,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(317)
		::Tree_obj::universalAlertClicked = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tree_obj,setUniversalAlertClicked,(void))

Void Tree_obj::setDropletClicked( bool val){
{
		HX_STACK_FRAME("Tree","setDropletClicked",0xd976ad4d,"Tree.setDropletClicked","Tree.hx",323,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(323)
		this->dropletClicked = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tree_obj,setDropletClicked,(void))

Void Tree_obj::setSunClicked( bool val){
{
		HX_STACK_FRAME("Tree","setSunClicked",0x7834ddcd,"Tree.setSunClicked","Tree.hx",329,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(329)
		this->sunClicked = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tree_obj,setSunClicked,(void))

Void Tree_obj::setTreeX( Float xVal){
{
		HX_STACK_FRAME("Tree","setTreeX",0xeb417b28,"Tree.setTreeX","Tree.hx",335,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_ARG(xVal,"xVal")
		HX_STACK_LINE(335)
		this->tree->set_x(xVal);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tree_obj,setTreeX,(void))

Void Tree_obj::setTreeY( Float yVal){
{
		HX_STACK_FRAME("Tree","setTreeY",0xeb417b29,"Tree.setTreeY","Tree.hx",341,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_ARG(yVal,"yVal")
		HX_STACK_LINE(341)
		this->tree->set_y(yVal);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tree_obj,setTreeY,(void))

Void Tree_obj::setTreeWatered( ){
{
		HX_STACK_FRAME("Tree","setTreeWatered",0x4e0782e6,"Tree.setTreeWatered","Tree.hx",346,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(346)
		if (((this->treeThirst > (int)-3))){
			HX_STACK_LINE(347)
			(this->treeThirst)--;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,setTreeWatered,(void))

Void Tree_obj::setTreeThirsty( ::flixel::util::FlxTimer timer){
{
		HX_STACK_FRAME("Tree","setTreeThirsty",0x50f6d92b,"Tree.setTreeThirsty","Tree.hx",352,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_ARG(timer,"timer")
		HX_STACK_LINE(352)
		if (((this->treeThirst < (int)3))){
			HX_STACK_LINE(353)
			(this->treeThirst)++;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tree_obj,setTreeThirsty,(void))

Void Tree_obj::setTreeSunLight( ){
{
		HX_STACK_FRAME("Tree","setTreeSunLight",0x825476fa,"Tree.setTreeSunLight","Tree.hx",359,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(359)
		if (((this->treeSunlight > (int)-3))){
			HX_STACK_LINE(360)
			(this->treeSunlight)--;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,setTreeSunLight,(void))

Void Tree_obj::setOldTreeSunLightTimer( Float oldSunlightTimerVal){
{
		HX_STACK_FRAME("Tree","setOldTreeSunLightTimer",0xa83d94a8,"Tree.setOldTreeSunLightTimer","Tree.hx",365,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_ARG(oldSunlightTimerVal,"oldSunlightTimerVal")
		HX_STACK_LINE(365)
		this->oldTreeSunLightTimer = oldSunlightTimerVal;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tree_obj,setOldTreeSunLightTimer,(void))

Void Tree_obj::setThirstValue( int thirstVal){
{
		HX_STACK_FRAME("Tree","setThirstValue",0xdae65ac1,"Tree.setThirstValue","Tree.hx",369,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_ARG(thirstVal,"thirstVal")
		HX_STACK_LINE(369)
		this->treeThirst = thirstVal;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tree_obj,setThirstValue,(void))

Void Tree_obj::setSunlightValue( int sunlightVal){
{
		HX_STACK_FRAME("Tree","setSunlightValue",0xe11da735,"Tree.setSunlightValue","Tree.hx",373,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sunlightVal,"sunlightVal")
		HX_STACK_LINE(373)
		this->treeSunlight = sunlightVal;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tree_obj,setSunlightValue,(void))

Void Tree_obj::setTreeNoLight( ::flixel::util::FlxTimer timer){
{
		HX_STACK_FRAME("Tree","setTreeNoLight",0x6b5ab385,"Tree.setTreeNoLight","Tree.hx",377,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_ARG(timer,"timer")
		HX_STACK_LINE(377)
		if (((bool((this->treeSunlight < (int)3)) && bool(!(this->amIdeadYet))))){
			HX_STACK_LINE(378)
			(this->treeSunlight)++;
			HX_STACK_LINE(379)
			this->sunlightAlertTime->start(this->treeSunLightTimer,this->setTreeNoLight_dyn(),(int)0);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tree_obj,setTreeNoLight,(void))

Void Tree_obj::pauseAlertTimers( ){
{
		HX_STACK_FRAME("Tree","pauseAlertTimers",0x38cd8724,"Tree.pauseAlertTimers","Tree.hx",388,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(389)
		this->dropAlertTime->start((int)999,this->setTreeThirsty_dyn(),(int)0);
		HX_STACK_LINE(391)
		this->sunlightAlertTime->start((int)999,this->setTreeNoLight_dyn(),(int)0);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,pauseAlertTimers,(void))

Void Tree_obj::unpauseAlertTimers( ){
{
		HX_STACK_FRAME("Tree","unpauseAlertTimers",0x9afe0b7d,"Tree.unpauseAlertTimers","Tree.hx",394,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(395)
		this->dropAlertTime->start(this->treeThirstTimer,this->setTreeThirsty_dyn(),(int)0);
		HX_STACK_LINE(397)
		this->sunlightAlertTime->start(this->treeSunLightTimer,this->setTreeNoLight_dyn(),(int)0);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,unpauseAlertTimers,(void))

Void Tree_obj::initAppleArray( ){
{
		HX_STACK_FRAME("Tree","initAppleArray",0xd4bc31ff,"Tree.initAppleArray","Tree.hx",401,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(401)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(401)
		while((true)){
			HX_STACK_LINE(401)
			if ((!(((_g < (int)3))))){
				HX_STACK_LINE(401)
				break;
			}
			HX_STACK_LINE(401)
			int i = (_g)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(402)
			this->appleArray[i] = null();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,initAppleArray,(void))

Void Tree_obj::resetDropTimer( ){
{
		HX_STACK_FRAME("Tree","resetDropTimer",0x8a921e37,"Tree.resetDropTimer","Tree.hx",408,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(408)
		this->dropAlertTime->reset(null());
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,resetDropTimer,(void))

Void Tree_obj::resetSunTimer( ){
{
		HX_STACK_FRAME("Tree","resetSunTimer",0x34057898,"Tree.resetSunTimer","Tree.hx",414,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(414)
		this->sunlightAlertTime->reset(null());
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,resetSunTimer,(void))

Void Tree_obj::registerTreeClickedMouseEvent( ){
{
		HX_STACK_FRAME("Tree","registerTreeClickedMouseEvent",0x90bb37ab,"Tree.registerTreeClickedMouseEvent","Tree.hx",419,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(419)
		::flixel::plugin::MouseEventManager_obj::add(this->sprinklerButtonSprite,this->purchaseSprinklerBuff_dyn(),null(),null(),null(),null(),null(),null());
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,registerTreeClickedMouseEvent,(void))

Void Tree_obj::unregisterTreeClickedMouseEvent( ){
{
		HX_STACK_FRAME("Tree","unregisterTreeClickedMouseEvent",0x9dca2fb2,"Tree.unregisterTreeClickedMouseEvent","Tree.hx",423,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(423)
		::flixel::plugin::MouseEventManager_obj::remove(this->sprinklerButtonSprite);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,unregisterTreeClickedMouseEvent,(void))

Void Tree_obj::registerAppleEvent( ){
{
		HX_STACK_FRAME("Tree","registerAppleEvent",0xf7c7b593,"Tree.registerAppleEvent","Tree.hx",427,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(427)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(427)
		while((true)){
			HX_STACK_LINE(427)
			if ((!(((_g < (int)3))))){
				HX_STACK_LINE(427)
				break;
			}
			HX_STACK_LINE(427)
			int i = (_g)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(428)
			if (((this->appleArray->__get(i).StaticCast< ::Apple >() != null()))){
				HX_STACK_LINE(429)
				::flixel::FlxSprite _g1 = this->appleArray->__get(i).StaticCast< ::Apple >()->getAppleSprite();		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(429)
				::flixel::plugin::MouseEventManager_obj::add(_g1,this->pickApple_dyn(),null(),null(),null(),null(),null(),null());
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,registerAppleEvent,(void))

Void Tree_obj::unregisterAppleEvent( ){
{
		HX_STACK_FRAME("Tree","unregisterAppleEvent",0xaa64c22c,"Tree.unregisterAppleEvent","Tree.hx",435,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(435)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(435)
		while((true)){
			HX_STACK_LINE(435)
			if ((!(((_g < (int)3))))){
				HX_STACK_LINE(435)
				break;
			}
			HX_STACK_LINE(435)
			int i = (_g)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(436)
			if (((this->appleArray->__get(i).StaticCast< ::Apple >() != null()))){
				HX_STACK_LINE(437)
				::flixel::FlxSprite _g1 = this->appleArray->__get(i).StaticCast< ::Apple >()->getAppleSprite();		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(437)
				::flixel::plugin::MouseEventManager_obj::remove(_g1);
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,unregisterAppleEvent,(void))

Void Tree_obj::registerAlertEvents( ){
{
		HX_STACK_FRAME("Tree","registerAlertEvents",0x8c5e6b02,"Tree.registerAlertEvents","Tree.hx",442,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(443)
		::flixel::plugin::MouseEventManager_obj::add(this->droplet,this->openProblem_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(444)
		::flixel::plugin::MouseEventManager_obj::add(this->dropletA,this->openProblem_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(445)
		::flixel::plugin::MouseEventManager_obj::add(this->dropletB,this->openProblem_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(446)
		::flixel::plugin::MouseEventManager_obj::add(this->sunA,this->openProblem_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(447)
		::flixel::plugin::MouseEventManager_obj::add(this->sunB,this->openProblem_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(448)
		::flixel::plugin::MouseEventManager_obj::add(this->sunC,this->openProblem_dyn(),null(),null(),null(),null(),null(),null());
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,registerAlertEvents,(void))

Void Tree_obj::unregisterAlertEvents( ){
{
		HX_STACK_FRAME("Tree","unregisterAlertEvents",0x232c6449,"Tree.unregisterAlertEvents","Tree.hx",451,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(453)
		::flixel::plugin::MouseEventManager_obj::remove(this->droplet);
		HX_STACK_LINE(454)
		::flixel::plugin::MouseEventManager_obj::remove(this->dropletA);
		HX_STACK_LINE(455)
		::flixel::plugin::MouseEventManager_obj::remove(this->dropletB);
		HX_STACK_LINE(456)
		::flixel::plugin::MouseEventManager_obj::remove(this->sunA);
		HX_STACK_LINE(457)
		::flixel::plugin::MouseEventManager_obj::remove(this->sunB);
		HX_STACK_LINE(458)
		::flixel::plugin::MouseEventManager_obj::remove(this->sunC);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,unregisterAlertEvents,(void))

Void Tree_obj::registerSunAlertEvents( ){
{
		HX_STACK_FRAME("Tree","registerSunAlertEvents",0x26b5481c,"Tree.registerSunAlertEvents","Tree.hx",462,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(463)
		::flixel::plugin::MouseEventManager_obj::add(this->sunA,this->openProblem_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(464)
		::flixel::plugin::MouseEventManager_obj::add(this->sunB,this->openProblem_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(465)
		::flixel::plugin::MouseEventManager_obj::add(this->sunC,this->openProblem_dyn(),null(),null(),null(),null(),null(),null());
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,registerSunAlertEvents,(void))

Void Tree_obj::unregisterSunAlertEvents( ){
{
		HX_STACK_FRAME("Tree","unregisterSunAlertEvents",0xd3171535,"Tree.unregisterSunAlertEvents","Tree.hx",468,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(469)
		::flixel::plugin::MouseEventManager_obj::remove(this->sunA);
		HX_STACK_LINE(470)
		::flixel::plugin::MouseEventManager_obj::remove(this->sunB);
		HX_STACK_LINE(471)
		::flixel::plugin::MouseEventManager_obj::remove(this->sunC);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,unregisterSunAlertEvents,(void))

Void Tree_obj::registerDropletAlertEvents( ){
{
		HX_STACK_FRAME("Tree","registerDropletAlertEvents",0xd6f21c1c,"Tree.registerDropletAlertEvents","Tree.hx",474,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(475)
		::flixel::plugin::MouseEventManager_obj::add(this->droplet,this->openProblem_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(476)
		::flixel::plugin::MouseEventManager_obj::add(this->dropletA,this->openProblem_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(477)
		::flixel::plugin::MouseEventManager_obj::add(this->dropletB,this->openProblem_dyn(),null(),null(),null(),null(),null(),null());
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,registerDropletAlertEvents,(void))

Void Tree_obj::unregisterDropletAlertEvents( ){
{
		HX_STACK_FRAME("Tree","unregisterDropletAlertEvents",0x51c2e9b5,"Tree.unregisterDropletAlertEvents","Tree.hx",480,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(481)
		::flixel::plugin::MouseEventManager_obj::remove(this->droplet);
		HX_STACK_LINE(482)
		::flixel::plugin::MouseEventManager_obj::remove(this->dropletA);
		HX_STACK_LINE(483)
		::flixel::plugin::MouseEventManager_obj::remove(this->dropletB);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,unregisterDropletAlertEvents,(void))

Void Tree_obj::openProblem( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("Tree","openProblem",0x637bfdc5,"Tree.openProblem","Tree.hx",486,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(488)
		if (((bool((bool((sprite == this->droplet)) || bool((sprite == this->dropletA)))) || bool((sprite == this->dropletB))))){
			HX_STACK_LINE(489)
			this->dropletClicked = true;
			HX_STACK_LINE(490)
			this->dropAlertUsed = true;
		}
		HX_STACK_LINE(492)
		if (((bool((bool((sprite == this->sunA)) || bool((sprite == this->sunB)))) || bool((sprite == this->sunC))))){
			HX_STACK_LINE(493)
			this->sunClicked = true;
			HX_STACK_LINE(494)
			this->sunAlertUsed = true;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tree_obj,openProblem,(void))

Void Tree_obj::alertPause( ){
{
		HX_STACK_FRAME("Tree","alertPause",0xd29f56ea,"Tree.alertPause","Tree.hx",502,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(502)
		if ((this->mathless)){
			HX_STACK_LINE(503)
			if ((this->dropAlertUsed)){
				HX_STACK_LINE(504)
				this->unregisterDropletAlertEvents();
				HX_STACK_LINE(505)
				hx::SubEq(this->alertDropPauseTimer,::flixel::FlxG_obj::elapsed);
				HX_STACK_LINE(506)
				if (((this->alertDropPauseTimer <= (int)0))){
					HX_STACK_LINE(507)
					this->registerDropletAlertEvents();
					HX_STACK_LINE(508)
					this->dropAlertUsed = false;
					HX_STACK_LINE(509)
					this->alertDropPauseTimer = (int)1;
				}
			}
			HX_STACK_LINE(512)
			if ((this->sunAlertUsed)){
				HX_STACK_LINE(513)
				this->unregisterSunAlertEvents();
				HX_STACK_LINE(514)
				hx::SubEq(this->alertSunPauseTimer,::flixel::FlxG_obj::elapsed);
				HX_STACK_LINE(515)
				if (((this->alertSunPauseTimer <= (int)0))){
					HX_STACK_LINE(516)
					this->registerSunAlertEvents();
					HX_STACK_LINE(517)
					this->sunAlertUsed = false;
					HX_STACK_LINE(518)
					this->alertSunPauseTimer = (int)1;
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,alertPause,(void))

Void Tree_obj::purchaseSprinklerBuff( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("Tree","purchaseSprinklerBuff",0xa6e6002a,"Tree.purchaseSprinklerBuff","Tree.hx",525,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(525)
		if (((this->goldCount > (int)0))){
			HX_STACK_LINE(526)
			if ((!(this->sprinklerSystemOn))){
				HX_STACK_LINE(527)
				if ((this->sprinklerButtonSprite->animation->get_finished())){
					HX_STACK_LINE(528)
					::flixel::system::FlxSound _g = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/sprinklerButtonSound.wav"),.3,null(),null(),null(),null(),null());		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(528)
					this->sprinklerButtonSound = _g;
					HX_STACK_LINE(529)
					this->sprinklerButtonSound->play(null());
					HX_STACK_LINE(530)
					this->sprinklerButtonSprite->animation->play(HX_CSTRING("sprinklerSwitchOn"),null(),null());
					HX_STACK_LINE(531)
					this->sprinklerSystemOn = true;
				}
			}
			else{
				HX_STACK_LINE(535)
				if ((this->sprinklerButtonSprite->animation->get_finished())){
					HX_STACK_LINE(536)
					::flixel::system::FlxSound _g1 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/sprinklerButtonSound.wav"),.3,null(),null(),null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(536)
					this->sprinklerButtonSound = _g1;
					HX_STACK_LINE(537)
					this->sprinklerButtonSound->play(null());
					HX_STACK_LINE(538)
					this->sprinklerButtonSprite->animation->play(HX_CSTRING("sprinklerSwitchOff"),null(),null());
					HX_STACK_LINE(539)
					this->sprinklerSystemOn = false;
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tree_obj,purchaseSprinklerBuff,(void))

Void Tree_obj::treeThirstCheck( ){
{
		HX_STACK_FRAME("Tree","treeThirstCheck",0x84efcffc,"Tree.treeThirstCheck","Tree.hx",545,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(547)
		if (((this->treeThirst >= (int)1))){
			HX_STACK_LINE(549)
			this->warnThirst(true);
		}
		HX_STACK_LINE(552)
		if (((this->treeThirst <= (int)0))){
			HX_STACK_LINE(554)
			this->warnThirst(false);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,treeThirstCheck,(void))

Void Tree_obj::treeSunlightCheck( ){
{
		HX_STACK_FRAME("Tree","treeSunlightCheck",0xa9a0dd70,"Tree.treeSunlightCheck","Tree.hx",560,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(562)
		if (((this->treeSunlight >= (int)1))){
			HX_STACK_LINE(564)
			this->warnSunlight(true);
		}
		HX_STACK_LINE(567)
		if (((this->treeSunlight <= (int)0))){
			HX_STACK_LINE(569)
			this->warnSunlight(false);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,treeSunlightCheck,(void))

Void Tree_obj::warnThirst( bool val){
{
		HX_STACK_FRAME("Tree","warnThirst",0x9559e014,"Tree.warnThirst","Tree.hx",577,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(577)
		bool _switch_1 = (val);
		if (  ( _switch_1==true)){
			HX_STACK_LINE(580)
			this->droplet->animation->play(HX_CSTRING("droplet"),null(),null());
			HX_STACK_LINE(581)
			this->dropletA->animation->play(HX_CSTRING("dropletA"),null(),null());
			HX_STACK_LINE(582)
			this->dropletB->animation->play(HX_CSTRING("dropletB"),null(),null());
			HX_STACK_LINE(583)
			if (((this->treeThirst == (int)1))){
				HX_STACK_LINE(584)
				{
					HX_STACK_LINE(584)
					::flixel::FlxSprite _g = this->droplet;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(584)
					_g->set_alpha((_g->alpha + .01));
				}
				HX_STACK_LINE(585)
				{
					HX_STACK_LINE(585)
					::flixel::FlxSprite _g = this->dropletA;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(585)
					_g->set_alpha((_g->alpha - .1));
				}
				HX_STACK_LINE(586)
				{
					HX_STACK_LINE(586)
					::flixel::FlxSprite _g = this->dropletB;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(586)
					_g->set_alpha((_g->alpha - .1));
				}
			}
			HX_STACK_LINE(588)
			if (((this->treeThirst == (int)2))){
				HX_STACK_LINE(589)
				{
					HX_STACK_LINE(589)
					::flixel::FlxSprite _g = this->dropletA;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(589)
					_g->set_alpha((_g->alpha + .01));
				}
				HX_STACK_LINE(590)
				{
					HX_STACK_LINE(590)
					::flixel::FlxSprite _g = this->droplet;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(590)
					_g->set_alpha((_g->alpha - .1));
				}
				HX_STACK_LINE(591)
				{
					HX_STACK_LINE(591)
					::flixel::FlxSprite _g = this->dropletB;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(591)
					_g->set_alpha((_g->alpha - .1));
				}
			}
			HX_STACK_LINE(593)
			if (((this->treeThirst == (int)3))){
				HX_STACK_LINE(594)
				{
					HX_STACK_LINE(594)
					::flixel::FlxSprite _g = this->dropletB;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(594)
					_g->set_alpha((_g->alpha + .01));
				}
				HX_STACK_LINE(595)
				{
					HX_STACK_LINE(595)
					::flixel::FlxSprite _g = this->droplet;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(595)
					_g->set_alpha((_g->alpha - .1));
				}
				HX_STACK_LINE(596)
				{
					HX_STACK_LINE(596)
					::flixel::FlxSprite _g = this->dropletA;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(596)
					_g->set_alpha((_g->alpha - .1));
				}
			}
			HX_STACK_LINE(598)
			if (((this->treeThirst == (int)-99))){
				HX_STACK_LINE(599)
				{
					HX_STACK_LINE(599)
					::flixel::FlxSprite _g = this->dropletB;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(599)
					_g->set_alpha((_g->alpha - .1));
				}
				HX_STACK_LINE(600)
				{
					HX_STACK_LINE(600)
					::flixel::FlxSprite _g = this->droplet;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(600)
					_g->set_alpha((_g->alpha - .1));
				}
				HX_STACK_LINE(601)
				{
					HX_STACK_LINE(601)
					::flixel::FlxSprite _g = this->dropletA;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(601)
					_g->set_alpha((_g->alpha - .1));
				}
			}
		}
		else if (  ( _switch_1==false)){
			HX_STACK_LINE(604)
			{
				HX_STACK_LINE(604)
				::flixel::FlxSprite _g = this->dropletA;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(604)
				_g->set_alpha((_g->alpha - .1));
			}
			HX_STACK_LINE(605)
			{
				HX_STACK_LINE(605)
				::flixel::FlxSprite _g = this->dropletB;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(605)
				_g->set_alpha((_g->alpha - .1));
			}
			HX_STACK_LINE(606)
			{
				HX_STACK_LINE(606)
				::flixel::FlxSprite _g = this->droplet;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(606)
				_g->set_alpha((_g->alpha - .1));
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tree_obj,warnThirst,(void))

Void Tree_obj::warnSunlight( bool val){
{
		HX_STACK_FRAME("Tree","warnSunlight",0x6fc15120,"Tree.warnSunlight","Tree.hx",612,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(612)
		bool _switch_2 = (val);
		if (  ( _switch_2==true)){
			HX_STACK_LINE(615)
			this->sunA->animation->play(HX_CSTRING("sunA"),null(),null());
			HX_STACK_LINE(616)
			this->sunB->animation->play(HX_CSTRING("sunB"),null(),null());
			HX_STACK_LINE(617)
			this->sunC->animation->play(HX_CSTRING("sunC"),null(),null());
			HX_STACK_LINE(618)
			if (((this->treeSunlight == (int)1))){
				HX_STACK_LINE(619)
				{
					HX_STACK_LINE(619)
					::flixel::FlxSprite _g = this->sunA;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(619)
					_g->set_alpha((_g->alpha + .01));
				}
				HX_STACK_LINE(620)
				{
					HX_STACK_LINE(620)
					::flixel::FlxSprite _g = this->sunB;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(620)
					_g->set_alpha((_g->alpha - .1));
				}
				HX_STACK_LINE(621)
				{
					HX_STACK_LINE(621)
					::flixel::FlxSprite _g = this->sunC;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(621)
					_g->set_alpha((_g->alpha - .1));
				}
			}
			HX_STACK_LINE(623)
			if (((this->treeSunlight == (int)2))){
				HX_STACK_LINE(624)
				{
					HX_STACK_LINE(624)
					::flixel::FlxSprite _g = this->sunB;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(624)
					_g->set_alpha((_g->alpha + .01));
				}
				HX_STACK_LINE(625)
				{
					HX_STACK_LINE(625)
					::flixel::FlxSprite _g = this->sunA;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(625)
					_g->set_alpha((_g->alpha - .1));
				}
				HX_STACK_LINE(626)
				{
					HX_STACK_LINE(626)
					::flixel::FlxSprite _g = this->sunC;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(626)
					_g->set_alpha((_g->alpha - .1));
				}
			}
			HX_STACK_LINE(628)
			if (((this->treeSunlight == (int)3))){
				HX_STACK_LINE(629)
				{
					HX_STACK_LINE(629)
					::flixel::FlxSprite _g = this->sunC;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(629)
					_g->set_alpha((_g->alpha + .01));
				}
				HX_STACK_LINE(630)
				{
					HX_STACK_LINE(630)
					::flixel::FlxSprite _g = this->sunA;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(630)
					_g->set_alpha((_g->alpha - .1));
				}
				HX_STACK_LINE(631)
				{
					HX_STACK_LINE(631)
					::flixel::FlxSprite _g = this->sunB;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(631)
					_g->set_alpha((_g->alpha - .1));
				}
			}
		}
		else if (  ( _switch_2==false)){
			HX_STACK_LINE(635)
			{
				HX_STACK_LINE(635)
				::flixel::FlxSprite _g = this->sunA;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(635)
				_g->set_alpha((_g->alpha - .1));
			}
			HX_STACK_LINE(636)
			{
				HX_STACK_LINE(636)
				::flixel::FlxSprite _g = this->sunB;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(636)
				_g->set_alpha((_g->alpha - .1));
			}
			HX_STACK_LINE(637)
			{
				HX_STACK_LINE(637)
				::flixel::FlxSprite _g = this->sunC;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(637)
				_g->set_alpha((_g->alpha - .1));
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tree_obj,warnSunlight,(void))

Void Tree_obj::appleIncubator( ){
{
		HX_STACK_FRAME("Tree","appleIncubator",0x69dadd37,"Tree.appleIncubator","Tree.hx",641,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(642)
		this->releaseAppleMem();
		HX_STACK_LINE(643)
		if (((bool((bool((this->treeGrowthPhase == this->adultTree)) && bool((this->treeThirst < (int)1)))) && bool((this->treeSunlight < (int)1))))){
			HX_STACK_LINE(644)
			hx::SubEq(this->appleSeperator,::flixel::FlxG_obj::elapsed);
			HX_STACK_LINE(645)
			if (((bool((bool((this->appleCount < (int)3)) && bool((this->appleSeperator < (int)0)))) && bool(this->appleLock)))){
				HX_STACK_LINE(646)
				this->appleLock = false;
				HX_STACK_LINE(647)
				::Apple _g = ::Apple_obj::__new();		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(647)
				this->apple = _g;
				HX_STACK_LINE(648)
				::flixel::FlxSprite _g1 = this->apple->getAppleSprite();		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(648)
				::flixel::plugin::MouseEventManager_obj::add(_g1,this->pickApple_dyn(),null(),null(),null(),null(),null(),null());
				HX_STACK_LINE(649)
				Float _g2 = this->getTreeX();		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(649)
				int _g3 = this->apple->getRandomX();		HX_STACK_VAR(_g3,"_g3");
				HX_STACK_LINE(649)
				Float _g4 = (_g2 + _g3);		HX_STACK_VAR(_g4,"_g4");
				HX_STACK_LINE(649)
				this->apple->setAppleX(_g4);
				HX_STACK_LINE(650)
				Float _g5 = this->getTreeY();		HX_STACK_VAR(_g5,"_g5");
				HX_STACK_LINE(650)
				int _g6 = this->apple->getRandomY();		HX_STACK_VAR(_g6,"_g6");
				HX_STACK_LINE(650)
				Float _g7 = (_g5 + _g6);		HX_STACK_VAR(_g7,"_g7");
				HX_STACK_LINE(650)
				this->apple->setAppleY(_g7);
				HX_STACK_LINE(651)
				while((true)){
					HX_STACK_LINE(651)
					if ((!(this->checkAppleOverlap()))){
						HX_STACK_LINE(651)
						break;
					}
					HX_STACK_LINE(652)
					Float _g8 = this->getTreeX();		HX_STACK_VAR(_g8,"_g8");
					HX_STACK_LINE(652)
					int _g9 = this->apple->getRandomX();		HX_STACK_VAR(_g9,"_g9");
					HX_STACK_LINE(652)
					Float _g10 = (_g8 + _g9);		HX_STACK_VAR(_g10,"_g10");
					HX_STACK_LINE(652)
					this->apple->setAppleX(_g10);
					HX_STACK_LINE(653)
					Float _g11 = this->getTreeY();		HX_STACK_VAR(_g11,"_g11");
					HX_STACK_LINE(653)
					int _g12 = this->apple->getRandomY();		HX_STACK_VAR(_g12,"_g12");
					HX_STACK_LINE(653)
					Float _g13 = (_g11 + _g12);		HX_STACK_VAR(_g13,"_g13");
					HX_STACK_LINE(653)
					this->apple->setAppleY(_g13);
				}
				HX_STACK_LINE(655)
				this->apple->setRiseRun(this->basketSprite);
				HX_STACK_LINE(656)
				Float _g14 = this->apple->getMyYPos();		HX_STACK_VAR(_g14,"_g14");
				HX_STACK_LINE(656)
				this->apple->setMyInitialY(_g14);
				HX_STACK_LINE(657)
				this->collideGroundCheck();
				HX_STACK_LINE(658)
				this->addAppleToArray();
				HX_STACK_LINE(659)
				this->treeGroup->add(this->apple);
				HX_STACK_LINE(660)
				(this->appleCount)++;
				HX_STACK_LINE(661)
				this->appleSeperator = (int)5;
			}
			HX_STACK_LINE(664)
			this->appleStuff();
		}
		HX_STACK_LINE(667)
		if (((this->treeGrowthPhase >= this->adultTree))){
			HX_STACK_LINE(668)
			if (((bool((bool((bool((this->treeThirst > (int)1)) && bool((this->treeSunlight > (int)1)))) || bool(((bool((this->treeThirst > (int)2)) || bool((this->treeSunlight > (int)2))))))) || bool(this->deathRattle)))){
				HX_STACK_LINE(669)
				hx::SubEq(this->timeToTreeDeath,::flixel::FlxG_obj::elapsed);
				HX_STACK_LINE(670)
				if (((this->timeToTreeDeath < (int)0))){
					HX_STACK_LINE(671)
					(this->treeGrowthPhase)++;
					HX_STACK_LINE(672)
					this->deathRattle = true;
					HX_STACK_LINE(673)
					if (((this->treeGrowthPhase == (int)10))){
						HX_STACK_LINE(674)
						this->amIdeadYet = true;
					}
					HX_STACK_LINE(676)
					this->droplet->set_alpha((int)0);
					HX_STACK_LINE(677)
					this->dropletA->set_alpha((int)0);
					HX_STACK_LINE(678)
					this->dropletB->set_alpha((int)0);
					HX_STACK_LINE(679)
					this->sunA->set_alpha((int)0);
					HX_STACK_LINE(680)
					this->sunB->set_alpha((int)0);
					HX_STACK_LINE(681)
					this->sunC->set_alpha((int)0);
					HX_STACK_LINE(684)
					this->treeThirst = (int)-99;
					HX_STACK_LINE(685)
					this->treeSunlight = (int)-99;
					HX_STACK_LINE(686)
					this->killAllApples();
					HX_STACK_LINE(687)
					this->growTree();
					HX_STACK_LINE(688)
					this->myTile->setHasObject(false);
					HX_STACK_LINE(689)
					this->timeToTreeDeath = (int)10;
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,appleIncubator,(void))

Void Tree_obj::releaseAppleMem( ){
{
		HX_STACK_FRAME("Tree","releaseAppleMem",0x73b1fad2,"Tree.releaseAppleMem","Tree.hx",697,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(697)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(697)
		int _g = this->appleArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(697)
		while((true)){
			HX_STACK_LINE(697)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(697)
				break;
			}
			HX_STACK_LINE(697)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(698)
			if (((this->appleArray->__get(i).StaticCast< ::Apple >() != null()))){
				HX_STACK_LINE(699)
				if ((this->appleArray->__get(i).StaticCast< ::Apple >()->getInBasket())){
					HX_STACK_LINE(700)
					::flixel::FlxSprite _g2 = this->appleArray->__get(i).StaticCast< ::Apple >()->getAppleSprite();		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(700)
					::flixel::plugin::MouseEventManager_obj::remove(_g2);
					HX_STACK_LINE(701)
					this->appleArray->__get(i).StaticCast< ::Apple >()->getAppleSprite()->kill();
					HX_STACK_LINE(703)
					this->appleArray->__get(i).StaticCast< ::Apple >()->destroy();
					HX_STACK_LINE(704)
					this->appleArray[i] = null();
					HX_STACK_LINE(705)
					this->appleStored = true;
					HX_STACK_LINE(706)
					(this->appleCount)--;
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,releaseAppleMem,(void))

Void Tree_obj::killAllApples( ){
{
		HX_STACK_FRAME("Tree","killAllApples",0x4e10e36c,"Tree.killAllApples","Tree.hx",713,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(713)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(713)
		int _g = this->appleArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(713)
		while((true)){
			HX_STACK_LINE(713)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(713)
				break;
			}
			HX_STACK_LINE(713)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(714)
			if (((this->appleArray->__get(i).StaticCast< ::Apple >() != null()))){
				HX_STACK_LINE(715)
				this->appleArray->__get(i).StaticCast< ::Apple >()->destroy();
				HX_STACK_LINE(716)
				this->appleArray[i] = null();
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,killAllApples,(void))

Void Tree_obj::addAppleToArray( ){
{
		HX_STACK_FRAME("Tree","addAppleToArray",0xceff0cb5,"Tree.addAppleToArray","Tree.hx",722,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(722)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(722)
		int _g = this->appleArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(722)
		while((true)){
			HX_STACK_LINE(722)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(722)
				break;
			}
			HX_STACK_LINE(722)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(723)
			if (((this->appleArray->__get(i).StaticCast< ::Apple >() == null()))){
				HX_STACK_LINE(724)
				this->appleArray[i] = this->apple;
				HX_STACK_LINE(725)
				return null();
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,addAppleToArray,(void))

Void Tree_obj::appleStuff( ){
{
		HX_STACK_FRAME("Tree","appleStuff",0x6177702a,"Tree.appleStuff","Tree.hx",733,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(733)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(733)
		int _g = this->appleArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(733)
		while((true)){
			HX_STACK_LINE(733)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(733)
				break;
			}
			HX_STACK_LINE(733)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			struct _Function_2_1{
				inline static bool Block( int &i,hx::ObjectPtr< ::Tree_obj > __this){
					HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","Tree.hx",734,0xa4188000)
					{
						HX_STACK_LINE(734)
						int _g2 = __this->appleArray->__get(i).StaticCast< ::Apple >()->getAppleGrowth();		HX_STACK_VAR(_g2,"_g2");
						HX_STACK_LINE(734)
						return (_g2 < (int)1);
					}
					return null();
				}
			};
			HX_STACK_LINE(734)
			if (((  (((this->appleArray->__get(i).StaticCast< ::Apple >() != null()))) ? bool(_Function_2_1::Block(i,this)) : bool(false) ))){
				HX_STACK_LINE(735)
				this->appleArray->__get(i).StaticCast< ::Apple >()->ripenApple();
				HX_STACK_LINE(736)
				if ((this->appleArray->__get(i).StaticCast< ::Apple >()->getAppleVisible())){
					HX_STACK_LINE(737)
					this->appleLock = true;
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,appleStuff,(void))

Void Tree_obj::pickApple( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("Tree","pickApple",0x99f395c9,"Tree.pickApple","Tree.hx",744,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(744)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(744)
		int _g = this->appleArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(744)
		while((true)){
			HX_STACK_LINE(744)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(744)
				break;
			}
			HX_STACK_LINE(744)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(745)
			if (((this->appleArray->__get(i).StaticCast< ::Apple >() != null()))){
				HX_STACK_LINE(746)
				::flixel::FlxSprite _g2 = this->appleArray->__get(i).StaticCast< ::Apple >()->getAppleSprite();		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(746)
				if (((  (((_g2 == sprite))) ? bool(this->appleArray->__get(i).StaticCast< ::Apple >()->getIAmRipe()) : bool(false) ))){
					HX_STACK_LINE(751)
					this->appleArray->__get(i).StaticCast< ::Apple >()->setLeavesFallOnce(true);
					HX_STACK_LINE(752)
					this->appleArray->__get(i).StaticCast< ::Apple >()->setFlyToBasket(true);
					HX_STACK_LINE(753)
					this->appleClicked = true;
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tree_obj,pickApple,(void))

Void Tree_obj::killDeadApple( ){
{
		HX_STACK_FRAME("Tree","killDeadApple",0x5bc351a8,"Tree.killDeadApple","Tree.hx",760,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(760)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(760)
		int _g = this->appleArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(760)
		while((true)){
			HX_STACK_LINE(760)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(760)
				break;
			}
			HX_STACK_LINE(760)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(761)
			if (((this->appleArray->__get(i).StaticCast< ::Apple >() != null()))){
				HX_STACK_LINE(762)
				if ((this->appleArray->__get(i).StaticCast< ::Apple >()->getIAmDead())){
					HX_STACK_LINE(763)
					this->appleArray->__get(i).StaticCast< ::Apple >()->getAppleSprite()->kill();
					HX_STACK_LINE(764)
					this->appleArray->__get(i).StaticCast< ::Apple >()->destroy();
					HX_STACK_LINE(765)
					this->appleArray[i] = null();
					HX_STACK_LINE(766)
					(this->appleCount)--;
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,killDeadApple,(void))

bool Tree_obj::checkAppleOverlap( ){
	HX_STACK_FRAME("Tree","checkAppleOverlap",0x3a127f45,"Tree.checkAppleOverlap","Tree.hx",772,0xa4188000)
	HX_STACK_THIS(this)
	HX_STACK_LINE(774)
	{
		HX_STACK_LINE(774)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(774)
		int _g = this->appleArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(774)
		while((true)){
			HX_STACK_LINE(774)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(774)
				break;
			}
			HX_STACK_LINE(774)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			struct _Function_3_1{
				inline static bool Block( int &i,hx::ObjectPtr< ::Tree_obj > __this){
					HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","Tree.hx",776,0xa4188000)
					{
						HX_STACK_LINE(776)
						Float _g2 = __this->apple->getMyXPos();		HX_STACK_VAR(_g2,"_g2");
						HX_STACK_LINE(776)
						Float _g11 = __this->appleArray->__get(i).StaticCast< ::Apple >()->getMyXPos();		HX_STACK_VAR(_g11,"_g11");
						HX_STACK_LINE(776)
						Float _g21 = (_g11 - (int)10);		HX_STACK_VAR(_g21,"_g21");
						struct _Function_4_1{
							inline static bool Block( int &i,hx::ObjectPtr< ::Tree_obj > __this){
								HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","Tree.hx",776,0xa4188000)
								{
									HX_STACK_LINE(776)
									Float _g3 = __this->apple->getMyXPos();		HX_STACK_VAR(_g3,"_g3");
									HX_STACK_LINE(776)
									Float _g4 = __this->appleArray->__get(i).StaticCast< ::Apple >()->getMyXPos();		HX_STACK_VAR(_g4,"_g4");
									HX_STACK_LINE(776)
									Float _g5 = (_g4 + (int)10);		HX_STACK_VAR(_g5,"_g5");
									HX_STACK_LINE(776)
									return (_g3 <= _g5);
								}
								return null();
							}
						};
						struct _Function_4_2{
							inline static bool Block( int &i,hx::ObjectPtr< ::Tree_obj > __this){
								HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","Tree.hx",777,0xa4188000)
								{
									HX_STACK_LINE(777)
									Float _g6 = __this->apple->getMyYPos();		HX_STACK_VAR(_g6,"_g6");
									HX_STACK_LINE(777)
									Float _g7 = __this->appleArray->__get(i).StaticCast< ::Apple >()->getMyYPos();		HX_STACK_VAR(_g7,"_g7");
									HX_STACK_LINE(777)
									Float _g8 = (_g7 - (int)10);		HX_STACK_VAR(_g8,"_g8");
									struct _Function_5_1{
										inline static bool Block( int &i,hx::ObjectPtr< ::Tree_obj > __this){
											HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","Tree.hx",777,0xa4188000)
											{
												HX_STACK_LINE(777)
												Float _g9 = __this->apple->getMyYPos();		HX_STACK_VAR(_g9,"_g9");
												HX_STACK_LINE(777)
												Float _g10 = __this->appleArray->__get(i).StaticCast< ::Apple >()->getMyYPos();		HX_STACK_VAR(_g10,"_g10");
												HX_STACK_LINE(777)
												Float _g111 = (_g10 + (int)10);		HX_STACK_VAR(_g111,"_g111");
												HX_STACK_LINE(777)
												return (_g9 <= _g111);
											}
											return null();
										}
									};
									HX_STACK_LINE(777)
									return (  (((_g6 >= _g8))) ? bool(_Function_5_1::Block(i,__this)) : bool(false) );
								}
								return null();
							}
						};
						HX_STACK_LINE(776)
						return (  (((  (((_g2 >= _g21))) ? bool(_Function_4_1::Block(i,__this)) : bool(false) ))) ? bool(_Function_4_2::Block(i,__this)) : bool(false) );
					}
					return null();
				}
			};
			HX_STACK_LINE(776)
			if (((  (((this->appleArray->__get(i).StaticCast< ::Apple >() != null()))) ? bool(_Function_3_1::Block(i,this)) : bool(false) ))){
				HX_STACK_LINE(778)
				return true;
			}
		}
	}
	HX_STACK_LINE(782)
	return false;
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,checkAppleOverlap,return )

Void Tree_obj::mouseAttack( ){
{
		HX_STACK_FRAME("Tree","mouseAttack",0xad2becdd,"Tree.mouseAttack","Tree.hx",785,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(786)
		hx::SubEq(this->mouseTimer,::flixel::FlxG_obj::elapsed);
		HX_STACK_LINE(787)
		if (((  ((this->checkForAppleRotTime())) ? bool(!(this->mouseInPlay)) : bool(false) ))){
			HX_STACK_LINE(788)
			if (((this->mouseTimer < (int)0))){
				HX_STACK_LINE(789)
				this->mouseInPlay = true;
				HX_STACK_LINE(790)
				this->checkingCollisions = true;
				HX_STACK_LINE(791)
				::Mouse _g = ::Mouse_obj::__new();		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(791)
				this->mouse = _g;
				HX_STACK_LINE(792)
				Float _g1 = this->getTreeX();		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(792)
				Float _g2 = (_g1 + (int)80);		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(792)
				this->mouse->getMouseSprite()->set_x(_g2);
				HX_STACK_LINE(793)
				Float _g3 = this->getTreeY();		HX_STACK_VAR(_g3,"_g3");
				HX_STACK_LINE(793)
				Float _g4 = (_g3 + (int)61);		HX_STACK_VAR(_g4,"_g4");
				HX_STACK_LINE(793)
				this->mouse->getMouseSprite()->set_y(_g4);
				HX_STACK_LINE(794)
				Float _g5 = this->getTreeX();		HX_STACK_VAR(_g5,"_g5");
				HX_STACK_LINE(794)
				Float _g6 = (_g5 + (int)80);		HX_STACK_VAR(_g6,"_g6");
				HX_STACK_LINE(794)
				this->mouse->getHoleSprite()->set_x(_g6);
				HX_STACK_LINE(795)
				Float _g7 = this->getTreeY();		HX_STACK_VAR(_g7,"_g7");
				HX_STACK_LINE(795)
				Float _g8 = (_g7 + (int)61);		HX_STACK_VAR(_g8,"_g8");
				HX_STACK_LINE(795)
				this->mouse->getHoleSprite()->set_y(_g8);
				HX_STACK_LINE(796)
				this->add(this->mouse);
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,mouseAttack,(void))

bool Tree_obj::checkForAppleRotTime( ){
	HX_STACK_FRAME("Tree","checkForAppleRotTime",0x9801413b,"Tree.checkForAppleRotTime","Tree.hx",801,0xa4188000)
	HX_STACK_THIS(this)
	HX_STACK_LINE(802)
	{
		HX_STACK_LINE(802)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(802)
		int _g = this->appleArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(802)
		while((true)){
			HX_STACK_LINE(802)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(802)
				break;
			}
			HX_STACK_LINE(802)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(803)
			if (((this->appleArray->__get(i).StaticCast< ::Apple >() != null()))){
				HX_STACK_LINE(804)
				Float _g2 = this->appleArray->__get(i).StaticCast< ::Apple >()->getRotTimer();		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(804)
				if (((_g2 <= (int)10))){
					HX_STACK_LINE(805)
					return true;
				}
			}
		}
	}
	HX_STACK_LINE(809)
	return false;
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,checkForAppleRotTime,return )

Void Tree_obj::checkMouseDone( ){
{
		HX_STACK_FRAME("Tree","checkMouseDone",0x33ad644f,"Tree.checkMouseDone","Tree.hx",813,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(813)
		if ((this->mouseInPlay)){
			HX_STACK_LINE(814)
			if ((this->mouse->getMouseAllDone())){
				HX_STACK_LINE(815)
				this->mouse->destroy();
				HX_STACK_LINE(816)
				this->mouse = null();
				HX_STACK_LINE(817)
				this->mouseInPlay = false;
				HX_STACK_LINE(818)
				this->takenAppleCarried = false;
				HX_STACK_LINE(819)
				this->mouseTimer = (int)10;
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,checkMouseDone,(void))

Void Tree_obj::checkForMouseCollisions( ){
{
		HX_STACK_FRAME("Tree","checkForMouseCollisions",0x6e0d0d35,"Tree.checkForMouseCollisions","Tree.hx",825,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(825)
		if (((bool((bool(this->mouseInPlay) && bool(!(this->takenAppleCarried)))) && bool(this->checkingCollisions)))){
			HX_STACK_LINE(826)
			{
				HX_STACK_LINE(826)
				int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(826)
				int _g = this->appleArray->length;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(826)
				while((true)){
					HX_STACK_LINE(826)
					if ((!(((_g1 < _g))))){
						HX_STACK_LINE(826)
						break;
					}
					HX_STACK_LINE(826)
					int i = (_g1)++;		HX_STACK_VAR(i,"i");
					HX_STACK_LINE(827)
					if (((this->appleArray->__get(i).StaticCast< ::Apple >() != null()))){
						HX_STACK_LINE(828)
						::flixel::FlxSprite _g2 = this->mouse->getMouseSprite();		HX_STACK_VAR(_g2,"_g2");
						HX_STACK_LINE(828)
						::flixel::FlxSprite _g11 = this->appleArray->__get(i).StaticCast< ::Apple >()->getAppleSprite();		HX_STACK_VAR(_g11,"_g11");
						HX_STACK_LINE(828)
						if (((  ((::flixel::util::FlxCollision_obj::pixelPerfectCheck(_g2,_g11,null(),null()))) ? bool(!(this->appleArray->__get(i).StaticCast< ::Apple >()->getIAmRotten())) : bool(false) ))){
							HX_STACK_LINE(829)
							this->appleArray->__get(i).StaticCast< ::Apple >()->setDropRate((int)0);
							HX_STACK_LINE(830)
							this->appleArray->__get(i).StaticCast< ::Apple >()->stopFreeFall();
							HX_STACK_LINE(831)
							::flixel::FlxSprite _g21 = this->appleArray->__get(i).StaticCast< ::Apple >()->getAppleSprite();		HX_STACK_VAR(_g21,"_g21");
							HX_STACK_LINE(831)
							::flixel::plugin::MouseEventManager_obj::remove(_g21);
							HX_STACK_LINE(832)
							this->takenAppleIndex = i;
							HX_STACK_LINE(833)
							this->takenAppleCarried = true;
							HX_STACK_LINE(834)
							this->mouse->setMouseCollision(true);
						}
					}
				}
			}
			HX_STACK_LINE(838)
			if (((this->mouse->getMouseSprite()->x == (this->mouse->getHoleSprite()->x - (int)90)))){
				HX_STACK_LINE(839)
				this->mouse->faceRight();
				HX_STACK_LINE(840)
				this->mouse->setMouseCollision(true);
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,checkForMouseCollisions,(void))

Void Tree_obj::carryApple( ){
{
		HX_STACK_FRAME("Tree","carryApple",0x91110b6f,"Tree.carryApple","Tree.hx",846,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(846)
		if (((bool(this->takenAppleCarried) && bool(this->checkingCollisions)))){
			HX_STACK_LINE(847)
			if (((this->appleArray->__get(this->takenAppleIndex).StaticCast< ::Apple >() != null()))){
				HX_STACK_LINE(848)
				this->appleArray->__get(this->takenAppleIndex).StaticCast< ::Apple >()->getAppleSprite()->set_x((this->mouse->getMouseSprite()->x + (int)8));
				HX_STACK_LINE(849)
				this->appleArray->__get(this->takenAppleIndex).StaticCast< ::Apple >()->getAppleSprite()->set_y(this->mouse->getMouseSprite()->y);
				HX_STACK_LINE(850)
				if (((this->appleArray->__get(this->takenAppleIndex).StaticCast< ::Apple >()->getAppleSprite()->x == this->mouse->getHoleSprite()->x))){
					HX_STACK_LINE(851)
					this->checkingCollisions = false;
					HX_STACK_LINE(852)
					this->appleArray->__get(this->takenAppleIndex).StaticCast< ::Apple >()->destroy();
					HX_STACK_LINE(853)
					this->appleArray[this->takenAppleIndex] = null();
					HX_STACK_LINE(854)
					(this->appleCount)--;
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,carryApple,(void))

Void Tree_obj::incGrowTree( ){
{
		HX_STACK_FRAME("Tree","incGrowTree",0x65de3b1f,"Tree.incGrowTree","Tree.hx",862,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(862)
		if (((bool((bool((this->treeThirst < (int)1)) && bool((this->treeSunlight < (int)1)))) && bool((this->treeGrowthPhase < (int)6))))){
			HX_STACK_LINE(863)
			(this->treeGrowthPhase)++;
			HX_STACK_LINE(864)
			this->growTree();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,incGrowTree,(void))

Void Tree_obj::growTree( ){
{
		HX_STACK_FRAME("Tree","growTree",0x6cee2001,"Tree.growTree","Tree.hx",871,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(871)
		int _g = this->treeGrowthPhase;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(871)
		switch( (int)(_g)){
			case (int)0: {
				HX_STACK_LINE(874)
				this->tree->animation->play(HX_CSTRING("0"),null(),null());
			}
			;break;
			case (int)1: {
				HX_STACK_LINE(876)
				this->tree->animation->play(HX_CSTRING("1"),null(),null());
			}
			;break;
			case (int)2: {
				HX_STACK_LINE(880)
				this->tree->animation->play(HX_CSTRING("2"),null(),null());
			}
			;break;
			case (int)3: {
				HX_STACK_LINE(884)
				this->tree->animation->play(HX_CSTRING("3"),null(),null());
			}
			;break;
			case (int)4: {
				HX_STACK_LINE(888)
				this->tree->animation->play(HX_CSTRING("4"),null(),null());
			}
			;break;
			case (int)5: {
				HX_STACK_LINE(892)
				this->tree->animation->play(HX_CSTRING("5"),null(),null());
			}
			;break;
			case (int)6: {
				HX_STACK_LINE(896)
				this->tree->animation->play(HX_CSTRING("6"),null(),null());
			}
			;break;
			case (int)7: {
				HX_STACK_LINE(900)
				this->tree->animation->play(HX_CSTRING("7"),null(),null());
			}
			;break;
			case (int)8: {
				HX_STACK_LINE(902)
				this->tree->animation->play(HX_CSTRING("8"),null(),null());
			}
			;break;
			case (int)9: {
				HX_STACK_LINE(904)
				this->tree->animation->play(HX_CSTRING("9"),null(),null());
			}
			;break;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,growTree,(void))

Void Tree_obj::positionAlerts( ){
{
		HX_STACK_FRAME("Tree","positionAlerts",0x6e246010,"Tree.positionAlerts","Tree.hx",910,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(912)
		Float _g = this->getTreeX();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(912)
		Float _g1 = (_g + (int)9);		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(912)
		this->droplet->set_x(_g1);
		HX_STACK_LINE(913)
		Float _g2 = this->getTreeY();		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(913)
		Float _g3 = (_g2 + (int)80);		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(913)
		this->droplet->set_y(_g3);
		HX_STACK_LINE(914)
		Float _g4 = this->getTreeX();		HX_STACK_VAR(_g4,"_g4");
		HX_STACK_LINE(914)
		Float _g5 = (_g4 + (int)9);		HX_STACK_VAR(_g5,"_g5");
		HX_STACK_LINE(914)
		this->dropletA->set_x(_g5);
		HX_STACK_LINE(915)
		Float _g6 = this->getTreeY();		HX_STACK_VAR(_g6,"_g6");
		HX_STACK_LINE(915)
		Float _g7 = (_g6 + (int)80);		HX_STACK_VAR(_g7,"_g7");
		HX_STACK_LINE(915)
		this->dropletA->set_y(_g7);
		HX_STACK_LINE(916)
		Float _g8 = this->getTreeX();		HX_STACK_VAR(_g8,"_g8");
		HX_STACK_LINE(916)
		Float _g9 = (_g8 + (int)9);		HX_STACK_VAR(_g9,"_g9");
		HX_STACK_LINE(916)
		this->dropletB->set_x(_g9);
		HX_STACK_LINE(917)
		Float _g10 = this->getTreeY();		HX_STACK_VAR(_g10,"_g10");
		HX_STACK_LINE(917)
		Float _g11 = (_g10 + (int)80);		HX_STACK_VAR(_g11,"_g11");
		HX_STACK_LINE(917)
		this->dropletB->set_y(_g11);
		HX_STACK_LINE(918)
		Float _g12 = this->getTreeX();		HX_STACK_VAR(_g12,"_g12");
		HX_STACK_LINE(918)
		Float _g13 = (_g12 + (int)32);		HX_STACK_VAR(_g13,"_g13");
		HX_STACK_LINE(918)
		this->sunA->set_x(_g13);
		HX_STACK_LINE(919)
		Float _g14 = this->getTreeY();		HX_STACK_VAR(_g14,"_g14");
		HX_STACK_LINE(919)
		Float _g15 = (_g14 + (int)80);		HX_STACK_VAR(_g15,"_g15");
		HX_STACK_LINE(919)
		this->sunA->set_y(_g15);
		HX_STACK_LINE(920)
		Float _g16 = this->getTreeX();		HX_STACK_VAR(_g16,"_g16");
		HX_STACK_LINE(920)
		Float _g17 = (_g16 + (int)32);		HX_STACK_VAR(_g17,"_g17");
		HX_STACK_LINE(920)
		this->sunB->set_x(_g17);
		HX_STACK_LINE(921)
		Float _g18 = this->getTreeY();		HX_STACK_VAR(_g18,"_g18");
		HX_STACK_LINE(921)
		Float _g19 = (_g18 + (int)80);		HX_STACK_VAR(_g19,"_g19");
		HX_STACK_LINE(921)
		this->sunB->set_y(_g19);
		HX_STACK_LINE(922)
		Float _g20 = this->getTreeX();		HX_STACK_VAR(_g20,"_g20");
		HX_STACK_LINE(922)
		Float _g21 = (_g20 + (int)32);		HX_STACK_VAR(_g21,"_g21");
		HX_STACK_LINE(922)
		this->sunC->set_x(_g21);
		HX_STACK_LINE(923)
		Float _g22 = this->getTreeY();		HX_STACK_VAR(_g22,"_g22");
		HX_STACK_LINE(923)
		Float _g23 = (_g22 + (int)80);		HX_STACK_VAR(_g23,"_g23");
		HX_STACK_LINE(923)
		this->sunC->set_y(_g23);
		HX_STACK_LINE(924)
		Float _g24 = this->getTreeX();		HX_STACK_VAR(_g24,"_g24");
		HX_STACK_LINE(924)
		Float _g25 = (_g24 - (int)20);		HX_STACK_VAR(_g25,"_g25");
		HX_STACK_LINE(924)
		this->treeGround->set_x(_g25);
		HX_STACK_LINE(925)
		Float _g26 = this->getTreeY();		HX_STACK_VAR(_g26,"_g26");
		HX_STACK_LINE(925)
		Float _g27 = (_g26 + (int)75);		HX_STACK_VAR(_g27,"_g27");
		HX_STACK_LINE(925)
		this->treeGround->set_y(_g27);
		HX_STACK_LINE(926)
		Float _g28 = this->getTreeX();		HX_STACK_VAR(_g28,"_g28");
		HX_STACK_LINE(926)
		Float _g29 = (_g28 - (int)15);		HX_STACK_VAR(_g29,"_g29");
		HX_STACK_LINE(926)
		this->sprinklerSprite->set_x(_g29);
		HX_STACK_LINE(927)
		Float _g30 = this->getTreeY();		HX_STACK_VAR(_g30,"_g30");
		HX_STACK_LINE(927)
		Float _g31 = (_g30 + (int)20);		HX_STACK_VAR(_g31,"_g31");
		HX_STACK_LINE(927)
		this->sprinklerSprite->set_y(_g31);
		HX_STACK_LINE(928)
		Float _g32 = this->getTreeX();		HX_STACK_VAR(_g32,"_g32");
		HX_STACK_LINE(928)
		Float _g33 = (_g32 - (int)35);		HX_STACK_VAR(_g33,"_g33");
		HX_STACK_LINE(928)
		this->sprinklerButtonSprite->set_x(_g33);
		HX_STACK_LINE(929)
		Float _g34 = this->getTreeY();		HX_STACK_VAR(_g34,"_g34");
		HX_STACK_LINE(929)
		Float _g35 = (_g34 + (int)35);		HX_STACK_VAR(_g35,"_g35");
		HX_STACK_LINE(929)
		this->sprinklerButtonSprite->set_y(_g35);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,positionAlerts,(void))

Void Tree_obj::collideGroundCheck( ){
{
		HX_STACK_FRAME("Tree","collideGroundCheck",0xde3a9ed3,"Tree.collideGroundCheck","Tree.hx",933,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(933)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(933)
		int _g = this->appleArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(933)
		while((true)){
			HX_STACK_LINE(933)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(933)
				break;
			}
			HX_STACK_LINE(933)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(934)
			if (((this->appleArray->__get(i).StaticCast< ::Apple >() != null()))){
				HX_STACK_LINE(935)
				::flixel::FlxSprite _g2 = this->appleArray->__get(i).StaticCast< ::Apple >()->getAppleSprite();		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(935)
				if ((::flixel::util::FlxCollision_obj::pixelPerfectCheck(_g2,this->treeGround,null(),null()))){
					HX_STACK_LINE(936)
					this->appleArray->__get(i).StaticCast< ::Apple >()->stopFreeFall();
					HX_STACK_LINE(937)
					this->appleArray->__get(i).StaticCast< ::Apple >()->setDropRate((int)0);
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,collideGroundCheck,(void))

Void Tree_obj::sprinklerAnimation( ){
{
		HX_STACK_FRAME("Tree","sprinklerAnimation",0xaf0e112c,"Tree.sprinklerAnimation","Tree.hx",943,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(944)
		if (((this->treeGrowthPhase == (int)6))){
			HX_STACK_LINE(945)
			::flixel::FlxSprite _g = this->sprinklerButtonSprite;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(945)
			_g->set_alpha((_g->alpha + .1));
		}
		HX_STACK_LINE(947)
		if ((this->sprinklerSystemOn)){
			HX_STACK_LINE(948)
			hx::SubEq(this->sprinklerOutTime,::flixel::FlxG_obj::elapsed);
			HX_STACK_LINE(949)
			this->treeThirst = (int)0;
			HX_STACK_LINE(950)
			this->warnThirst(false);
			HX_STACK_LINE(951)
			if ((!(this->oldSunTimerChanged))){
				HX_STACK_LINE(952)
				{
					HX_STACK_LINE(952)
					::flixel::util::FlxTimer _this = this->sunlightAlertTime;		HX_STACK_VAR(_this,"_this");
					HX_STACK_LINE(952)
					this->oldTreeSunLightTimer = (_this->time - _this->_timeCounter);
				}
				HX_STACK_LINE(953)
				this->oldSunTimerChanged = true;
			}
			HX_STACK_LINE(955)
			this->treeSunLightTimer = (int)20;
			HX_STACK_LINE(956)
			if ((this->extendedTime)){
				HX_STACK_LINE(957)
				this->sunlightAlertTime->start(this->treeSunLightTimer,this->setTreeNoLight_dyn(),(int)0);
				HX_STACK_LINE(958)
				this->extendedTime = false;
			}
			HX_STACK_LINE(960)
			if ((this->sprinklerOutPhase)){
				HX_STACK_LINE(961)
				::flixel::system::FlxSound _g = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/sprinklerSound.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(961)
				this->sprinklerSound = _g;
				HX_STACK_LINE(962)
				this->sprinklerSprite->set_alpha((int)1);
				HX_STACK_LINE(963)
				this->sprinklerSprite->animation->play(HX_CSTRING("sprinklerOut"),null(),null());
				HX_STACK_LINE(964)
				this->sprinklerOutPhase = false;
			}
			HX_STACK_LINE(966)
			if (((this->sprinklerOutTime < (int)0))){
				HX_STACK_LINE(967)
				this->sprinklerSprite->animation->play(HX_CSTRING("sprinklerSpray"),null(),null());
				HX_STACK_LINE(968)
				this->sprinklerSound->play(null());
				HX_STACK_LINE(969)
				this->sprinklerOutTime = (int)-1;
			}
		}
		else{
			HX_STACK_LINE(973)
			if ((!(this->sprinklerOutPhase))){
				HX_STACK_LINE(974)
				this->sprinklerOutTime = (int)2;
				HX_STACK_LINE(975)
				this->sprinklerSprite->animation->play(HX_CSTRING("sprinklerIn"),null(),null());
				HX_STACK_LINE(976)
				{
					HX_STACK_LINE(976)
					::flixel::system::FlxSound _this = this->sprinklerSound;		HX_STACK_VAR(_this,"_this");
					HX_STACK_LINE(976)
					_this->cleanup(_this->autoDestroy,true,true);
					HX_STACK_LINE(976)
					_this;
				}
				HX_STACK_LINE(977)
				::flixel::system::FlxSound _g1 = ::flixel::util::FlxDestroyUtil_obj::destroy(this->sprinklerSound);		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(977)
				this->sprinklerSound = _g1;
				HX_STACK_LINE(978)
				this->sprinklerOutPhase = true;
				HX_STACK_LINE(979)
				this->oldSunTimerChanged = false;
				HX_STACK_LINE(980)
				this->treeSunLightTimer = (int)10;
				HX_STACK_LINE(981)
				this->sunlightAlertTime->start(this->oldTreeSunLightTimer,this->setTreeNoLight_dyn(),(int)0);
				HX_STACK_LINE(982)
				this->extendedTime = true;
				HX_STACK_LINE(983)
				if ((this->sprinklerInPhase)){
					HX_STACK_LINE(984)
					this->sprinklerSprite->animation->play(HX_CSTRING("sprinklerIn"),null(),null());
					HX_STACK_LINE(985)
					this->sprinklerButtonSprite->animation->play(HX_CSTRING("sprinklerOn"),null(),null());
					HX_STACK_LINE(986)
					this->sprinklerInPhase = false;
				}
			}
		}
		HX_STACK_LINE(990)
		if (((this->goldCount <= (int)0))){
			HX_STACK_LINE(991)
			this->sprinklerButtonSprite->animation->play(HX_CSTRING("sprinklerSwitchOff"),null(),null());
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,sprinklerAnimation,(void))

Void Tree_obj::currentGoldCount( int gold){
{
		HX_STACK_FRAME("Tree","currentGoldCount",0xaff4fb86,"Tree.currentGoldCount","Tree.hx",996,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_ARG(gold,"gold")
		HX_STACK_LINE(996)
		this->goldCount = gold;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tree_obj,currentGoldCount,(void))

Void Tree_obj::payWaterUtility( ){
{
		HX_STACK_FRAME("Tree","payWaterUtility",0x657216ad,"Tree.payWaterUtility","Tree.hx",1000,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1000)
		if ((this->sprinklerSystemOn)){
			HX_STACK_LINE(1001)
			hx::SubEq(this->waterUtlilityTimer,::flixel::FlxG_obj::elapsed);
			HX_STACK_LINE(1002)
			if (((this->waterUtlilityTimer < (int)0))){
				HX_STACK_LINE(1003)
				Float _g = this->getTreeX();		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(1003)
				Float _g1 = (_g + (int)18);		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(1003)
				this->minusOneSprite->set_x(_g1);
				HX_STACK_LINE(1004)
				Float _g2 = this->getTreeY();		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(1004)
				Float _g3 = (_g2 + (int)15);		HX_STACK_VAR(_g3,"_g3");
				HX_STACK_LINE(1004)
				this->minusOneSprite->set_y(_g3);
				HX_STACK_LINE(1005)
				this->minusOneSprite->set_alpha((int)1);
				struct _Function_3_1{
					inline static Dynamic Block( hx::ObjectPtr< ::Tree_obj > __this){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","Tree.hx",1006,0xa4188000)
						{
							hx::Anon __result = hx::Anon_obj::Create();
							__result->Add(HX_CSTRING("alpha") , (int)0,false);
							__result->Add(HX_CSTRING("y") , (__this->minusOneSprite->y - (int)16),false);
							return __result;
						}
						return null();
					}
				};
				HX_STACK_LINE(1006)
				::flixel::tweens::FlxTween_obj::tween(this->minusOneSprite,_Function_3_1::Block(this),(int)1,null());
				HX_STACK_LINE(1007)
				this->payWaterBill = true;
				HX_STACK_LINE(1008)
				this->waterUtlilityTimer = (int)30;
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,payWaterUtility,(void))

Void Tree_obj::destroySounds( ){
{
		HX_STACK_FRAME("Tree","destroySounds",0xb1f1626e,"Tree.destroySounds","Tree.hx",1013,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1014)
		if (((bool((this->treeGrowthSound != null())) && bool(!(((this->treeGrowthSound->_channel != null()))))))){
			HX_STACK_LINE(1015)
			::flixel::system::FlxSound _g = ::flixel::util::FlxDestroyUtil_obj::destroy(this->treeGrowthSound);		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(1015)
			this->treeGrowthSound = _g;
		}
		HX_STACK_LINE(1018)
		if (((bool((this->sprinklerButtonSound != null())) && bool(!(((this->sprinklerButtonSound->_channel != null()))))))){
			HX_STACK_LINE(1019)
			::flixel::system::FlxSound _g1 = ::flixel::util::FlxDestroyUtil_obj::destroy(this->sprinklerButtonSound);		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(1019)
			this->sprinklerButtonSound = _g1;
		}
		HX_STACK_LINE(1022)
		if (((bool((this->treeGrowthSound != null())) && bool(!(((this->treeGrowthSound->_channel != null()))))))){
			HX_STACK_LINE(1023)
			::flixel::system::FlxSound _g2 = ::flixel::util::FlxDestroyUtil_obj::destroy(this->treeGrowthSound);		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(1023)
			this->treeGrowthSound = _g2;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,destroySounds,(void))

Void Tree_obj::greenHouseEffect( ){
{
		HX_STACK_FRAME("Tree","greenHouseEffect",0x727d293e,"Tree.greenHouseEffect","Tree.hx",1029,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1029)
		if ((::Greenhouse_obj::greenHouseInPlay)){
			HX_STACK_LINE(1030)
			this->treeSunlight = (int)0;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,greenHouseEffect,(void))

Void Tree_obj::removeFallingAppleEvent( ){
{
		HX_STACK_FRAME("Tree","removeFallingAppleEvent",0x1cddcab3,"Tree.removeFallingAppleEvent","Tree.hx",1035,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1035)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(1035)
		int _g = this->appleArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(1035)
		while((true)){
			HX_STACK_LINE(1035)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(1035)
				break;
			}
			HX_STACK_LINE(1035)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(1036)
			if (((this->appleArray->__get(i).StaticCast< ::Apple >() != null()))){
				HX_STACK_LINE(1037)
				if ((this->appleArray->__get(i).StaticCast< ::Apple >()->getAppleFreeFall())){
					HX_STACK_LINE(1038)
					::flixel::FlxSprite _g2 = this->appleArray->__get(i).StaticCast< ::Apple >()->getAppleSprite();		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(1038)
					::flixel::plugin::MouseEventManager_obj::remove(_g2);
				}
				else{
					HX_STACK_LINE(1041)
					::flixel::FlxSprite _g11 = this->appleArray->__get(i).StaticCast< ::Apple >()->getAppleSprite();		HX_STACK_VAR(_g11,"_g11");
					HX_STACK_LINE(1041)
					::flixel::plugin::MouseEventManager_obj::add(_g11,this->pickApple_dyn(),null(),null(),null(),null(),null(),null());
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tree_obj,removeFallingAppleEvent,(void))

Void Tree_obj::draw( ){
{
		HX_STACK_FRAME("Tree","draw",0x388f6cf4,"Tree.draw","Tree.hx",1049,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1049)
		this->super::draw();
	}
return null();
}


Void Tree_obj::kill( ){
{
		HX_STACK_FRAME("Tree","kill",0x3d29208e,"Tree.kill","Tree.hx",1053,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1054)
		this->kill_dyn();
		HX_STACK_LINE(1055)
		this->super::kill();
	}
return null();
}


Void Tree_obj::update( ){
{
		HX_STACK_FRAME("Tree","update",0xb42ae2f9,"Tree.update","Tree.hx",1058,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1059)
		this->removeFallingAppleEvent();
		HX_STACK_LINE(1060)
		this->payWaterUtility();
		HX_STACK_LINE(1061)
		this->sprinklerAnimation();
		HX_STACK_LINE(1062)
		this->greenHouseEffect();
		HX_STACK_LINE(1063)
		this->checkMouseDone();
		HX_STACK_LINE(1064)
		this->carryApple();
		HX_STACK_LINE(1065)
		this->checkForMouseCollisions();
		HX_STACK_LINE(1066)
		this->killDeadApple();
		HX_STACK_LINE(1067)
		this->collideGroundCheck();
		HX_STACK_LINE(1068)
		this->appleIncubator();
		HX_STACK_LINE(1069)
		this->treeSunlightCheck();
		HX_STACK_LINE(1070)
		this->treeThirstCheck();
		HX_STACK_LINE(1071)
		this->mouseAttack();
		HX_STACK_LINE(1072)
		this->destroySounds();
		HX_STACK_LINE(1073)
		this->alertPause();
		HX_STACK_LINE(1074)
		this->super::update();
	}
return null();
}


Void Tree_obj::destroy( ){
{
		HX_STACK_FRAME("Tree","destroy",0x7812220a,"Tree.destroy","Tree.hx",1078,0xa4188000)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1080)
		this->unregisterTreeClickedMouseEvent();
		HX_STACK_LINE(1081)
		this->unregisterAppleEvent();
		HX_STACK_LINE(1082)
		this->unregisterAlertEvents();
		HX_STACK_LINE(1083)
		::flixel::system::FlxSound _g = ::flixel::util::FlxDestroyUtil_obj::destroy(this->sprinklerSound);		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(1083)
		this->sprinklerSound = _g;
		HX_STACK_LINE(1084)
		this->treeGroup = null();
		HX_STACK_LINE(1085)
		this->appleArray = null();
		HX_STACK_LINE(1086)
		this->dropAlertTime = null();
		HX_STACK_LINE(1087)
		this->sunlightAlertTime = null();
		HX_STACK_LINE(1088)
		this->boardObj = null();
		HX_STACK_LINE(1089)
		this->treeGround = null();
		HX_STACK_LINE(1090)
		this->tree = null();
		HX_STACK_LINE(1091)
		this->droplet = null();
		HX_STACK_LINE(1092)
		this->dropletA = null();
		HX_STACK_LINE(1093)
		this->dropletB = null();
		HX_STACK_LINE(1094)
		this->sunA = null();
		HX_STACK_LINE(1095)
		this->sunB = null();
		HX_STACK_LINE(1096)
		this->sunC = null();
		HX_STACK_LINE(1097)
		this->sprinklerSprite = null();
		HX_STACK_LINE(1098)
		this->minusOneSprite = null();
		HX_STACK_LINE(1099)
		this->basketSprite = null();
		HX_STACK_LINE(1100)
		this->mouse = null();
		HX_STACK_LINE(1101)
		this->sprinklerButtonSprite = null();
		HX_STACK_LINE(1102)
		this->myTile = null();
		HX_STACK_LINE(1103)
		this->sprinklerSound = null();
		HX_STACK_LINE(1104)
		this->treeGrowthSound = null();
		HX_STACK_LINE(1105)
		this->super::destroy();
	}
return null();
}


bool Tree_obj::universalAlertClicked;


Tree_obj::Tree_obj()
{
}

void Tree_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Tree);
	HX_MARK_MEMBER_NAME(treeGroup,"treeGroup");
	HX_MARK_MEMBER_NAME(appleArray,"appleArray");
	HX_MARK_MEMBER_NAME(apple,"apple");
	HX_MARK_MEMBER_NAME(myTile,"myTile");
	HX_MARK_MEMBER_NAME(mouse,"mouse");
	HX_MARK_MEMBER_NAME(treeGround,"treeGround");
	HX_MARK_MEMBER_NAME(tree,"tree");
	HX_MARK_MEMBER_NAME(droplet,"droplet");
	HX_MARK_MEMBER_NAME(dropletA,"dropletA");
	HX_MARK_MEMBER_NAME(dropletB,"dropletB");
	HX_MARK_MEMBER_NAME(sunA,"sunA");
	HX_MARK_MEMBER_NAME(sunB,"sunB");
	HX_MARK_MEMBER_NAME(sunC,"sunC");
	HX_MARK_MEMBER_NAME(sprinklerSprite,"sprinklerSprite");
	HX_MARK_MEMBER_NAME(sprinklerButtonSprite,"sprinklerButtonSprite");
	HX_MARK_MEMBER_NAME(minusOneSprite,"minusOneSprite");
	HX_MARK_MEMBER_NAME(basketSprite,"basketSprite");
	HX_MARK_MEMBER_NAME(sprinklerSound,"sprinklerSound");
	HX_MARK_MEMBER_NAME(treeGrowthSound,"treeGrowthSound");
	HX_MARK_MEMBER_NAME(sprinklerButtonSound,"sprinklerButtonSound");
	HX_MARK_MEMBER_NAME(boardObj,"boardObj");
	HX_MARK_MEMBER_NAME(answerString,"answerString");
	HX_MARK_MEMBER_NAME(maxThirst,"maxThirst");
	HX_MARK_MEMBER_NAME(minThirst,"minThirst");
	HX_MARK_MEMBER_NAME(treeGrowthPhase,"treeGrowthPhase");
	HX_MARK_MEMBER_NAME(treeThirst,"treeThirst");
	HX_MARK_MEMBER_NAME(treeSunlight,"treeSunlight");
	HX_MARK_MEMBER_NAME(appleCount,"appleCount");
	HX_MARK_MEMBER_NAME(appleIndex,"appleIndex");
	HX_MARK_MEMBER_NAME(adultTree,"adultTree");
	HX_MARK_MEMBER_NAME(takenAppleIndex,"takenAppleIndex");
	HX_MARK_MEMBER_NAME(goldCount,"goldCount");
	HX_MARK_MEMBER_NAME(alertDropPauseTimer,"alertDropPauseTimer");
	HX_MARK_MEMBER_NAME(alertSunPauseTimer,"alertSunPauseTimer");
	HX_MARK_MEMBER_NAME(treeThirstTimer,"treeThirstTimer");
	HX_MARK_MEMBER_NAME(treeSunLightTimer,"treeSunLightTimer");
	HX_MARK_MEMBER_NAME(oldTreeSunLightTimer,"oldTreeSunLightTimer");
	HX_MARK_MEMBER_NAME(waterUtlilityTimer,"waterUtlilityTimer");
	HX_MARK_MEMBER_NAME(sprinklerOutTime,"sprinklerOutTime");
	HX_MARK_MEMBER_NAME(timeToTreeDeath,"timeToTreeDeath");
	HX_MARK_MEMBER_NAME(appleSeperator,"appleSeperator");
	HX_MARK_MEMBER_NAME(mouseTimer,"mouseTimer");
	HX_MARK_MEMBER_NAME(sunAlertCoolDown,"sunAlertCoolDown");
	HX_MARK_MEMBER_NAME(dropletAlertCoolDown,"dropletAlertCoolDown");
	HX_MARK_MEMBER_NAME(sprinklerAnimationTimer,"sprinklerAnimationTimer");
	HX_MARK_MEMBER_NAME(dropAlertTime,"dropAlertTime");
	HX_MARK_MEMBER_NAME(sunlightAlertTime,"sunlightAlertTime");
	HX_MARK_MEMBER_NAME(oldSunTimerChanged,"oldSunTimerChanged");
	HX_MARK_MEMBER_NAME(mouseInPlay,"mouseInPlay");
	HX_MARK_MEMBER_NAME(dropletClicked,"dropletClicked");
	HX_MARK_MEMBER_NAME(sunClicked,"sunClicked");
	HX_MARK_MEMBER_NAME(treeClicked,"treeClicked");
	HX_MARK_MEMBER_NAME(appleLock,"appleLock");
	HX_MARK_MEMBER_NAME(amIdeadYet,"amIdeadYet");
	HX_MARK_MEMBER_NAME(appleClicked,"appleClicked");
	HX_MARK_MEMBER_NAME(takenAppleCarried,"takenAppleCarried");
	HX_MARK_MEMBER_NAME(checkingCollisions,"checkingCollisions");
	HX_MARK_MEMBER_NAME(sprinklerSystemOn,"sprinklerSystemOn");
	HX_MARK_MEMBER_NAME(sprinklerOutPhase,"sprinklerOutPhase");
	HX_MARK_MEMBER_NAME(sprinklerInPhase,"sprinklerInPhase");
	HX_MARK_MEMBER_NAME(extendedTime,"extendedTime");
	HX_MARK_MEMBER_NAME(payWaterBill,"payWaterBill");
	HX_MARK_MEMBER_NAME(appleStored,"appleStored");
	HX_MARK_MEMBER_NAME(deathRattle,"deathRattle");
	HX_MARK_MEMBER_NAME(mathless,"mathless");
	HX_MARK_MEMBER_NAME(greenHouseInPlay,"greenHouseInPlay");
	HX_MARK_MEMBER_NAME(dropAlertUsed,"dropAlertUsed");
	HX_MARK_MEMBER_NAME(sunAlertUsed,"sunAlertUsed");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void Tree_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(treeGroup,"treeGroup");
	HX_VISIT_MEMBER_NAME(appleArray,"appleArray");
	HX_VISIT_MEMBER_NAME(apple,"apple");
	HX_VISIT_MEMBER_NAME(myTile,"myTile");
	HX_VISIT_MEMBER_NAME(mouse,"mouse");
	HX_VISIT_MEMBER_NAME(treeGround,"treeGround");
	HX_VISIT_MEMBER_NAME(tree,"tree");
	HX_VISIT_MEMBER_NAME(droplet,"droplet");
	HX_VISIT_MEMBER_NAME(dropletA,"dropletA");
	HX_VISIT_MEMBER_NAME(dropletB,"dropletB");
	HX_VISIT_MEMBER_NAME(sunA,"sunA");
	HX_VISIT_MEMBER_NAME(sunB,"sunB");
	HX_VISIT_MEMBER_NAME(sunC,"sunC");
	HX_VISIT_MEMBER_NAME(sprinklerSprite,"sprinklerSprite");
	HX_VISIT_MEMBER_NAME(sprinklerButtonSprite,"sprinklerButtonSprite");
	HX_VISIT_MEMBER_NAME(minusOneSprite,"minusOneSprite");
	HX_VISIT_MEMBER_NAME(basketSprite,"basketSprite");
	HX_VISIT_MEMBER_NAME(sprinklerSound,"sprinklerSound");
	HX_VISIT_MEMBER_NAME(treeGrowthSound,"treeGrowthSound");
	HX_VISIT_MEMBER_NAME(sprinklerButtonSound,"sprinklerButtonSound");
	HX_VISIT_MEMBER_NAME(boardObj,"boardObj");
	HX_VISIT_MEMBER_NAME(answerString,"answerString");
	HX_VISIT_MEMBER_NAME(maxThirst,"maxThirst");
	HX_VISIT_MEMBER_NAME(minThirst,"minThirst");
	HX_VISIT_MEMBER_NAME(treeGrowthPhase,"treeGrowthPhase");
	HX_VISIT_MEMBER_NAME(treeThirst,"treeThirst");
	HX_VISIT_MEMBER_NAME(treeSunlight,"treeSunlight");
	HX_VISIT_MEMBER_NAME(appleCount,"appleCount");
	HX_VISIT_MEMBER_NAME(appleIndex,"appleIndex");
	HX_VISIT_MEMBER_NAME(adultTree,"adultTree");
	HX_VISIT_MEMBER_NAME(takenAppleIndex,"takenAppleIndex");
	HX_VISIT_MEMBER_NAME(goldCount,"goldCount");
	HX_VISIT_MEMBER_NAME(alertDropPauseTimer,"alertDropPauseTimer");
	HX_VISIT_MEMBER_NAME(alertSunPauseTimer,"alertSunPauseTimer");
	HX_VISIT_MEMBER_NAME(treeThirstTimer,"treeThirstTimer");
	HX_VISIT_MEMBER_NAME(treeSunLightTimer,"treeSunLightTimer");
	HX_VISIT_MEMBER_NAME(oldTreeSunLightTimer,"oldTreeSunLightTimer");
	HX_VISIT_MEMBER_NAME(waterUtlilityTimer,"waterUtlilityTimer");
	HX_VISIT_MEMBER_NAME(sprinklerOutTime,"sprinklerOutTime");
	HX_VISIT_MEMBER_NAME(timeToTreeDeath,"timeToTreeDeath");
	HX_VISIT_MEMBER_NAME(appleSeperator,"appleSeperator");
	HX_VISIT_MEMBER_NAME(mouseTimer,"mouseTimer");
	HX_VISIT_MEMBER_NAME(sunAlertCoolDown,"sunAlertCoolDown");
	HX_VISIT_MEMBER_NAME(dropletAlertCoolDown,"dropletAlertCoolDown");
	HX_VISIT_MEMBER_NAME(sprinklerAnimationTimer,"sprinklerAnimationTimer");
	HX_VISIT_MEMBER_NAME(dropAlertTime,"dropAlertTime");
	HX_VISIT_MEMBER_NAME(sunlightAlertTime,"sunlightAlertTime");
	HX_VISIT_MEMBER_NAME(oldSunTimerChanged,"oldSunTimerChanged");
	HX_VISIT_MEMBER_NAME(mouseInPlay,"mouseInPlay");
	HX_VISIT_MEMBER_NAME(dropletClicked,"dropletClicked");
	HX_VISIT_MEMBER_NAME(sunClicked,"sunClicked");
	HX_VISIT_MEMBER_NAME(treeClicked,"treeClicked");
	HX_VISIT_MEMBER_NAME(appleLock,"appleLock");
	HX_VISIT_MEMBER_NAME(amIdeadYet,"amIdeadYet");
	HX_VISIT_MEMBER_NAME(appleClicked,"appleClicked");
	HX_VISIT_MEMBER_NAME(takenAppleCarried,"takenAppleCarried");
	HX_VISIT_MEMBER_NAME(checkingCollisions,"checkingCollisions");
	HX_VISIT_MEMBER_NAME(sprinklerSystemOn,"sprinklerSystemOn");
	HX_VISIT_MEMBER_NAME(sprinklerOutPhase,"sprinklerOutPhase");
	HX_VISIT_MEMBER_NAME(sprinklerInPhase,"sprinklerInPhase");
	HX_VISIT_MEMBER_NAME(extendedTime,"extendedTime");
	HX_VISIT_MEMBER_NAME(payWaterBill,"payWaterBill");
	HX_VISIT_MEMBER_NAME(appleStored,"appleStored");
	HX_VISIT_MEMBER_NAME(deathRattle,"deathRattle");
	HX_VISIT_MEMBER_NAME(mathless,"mathless");
	HX_VISIT_MEMBER_NAME(greenHouseInPlay,"greenHouseInPlay");
	HX_VISIT_MEMBER_NAME(dropAlertUsed,"dropAlertUsed");
	HX_VISIT_MEMBER_NAME(sunAlertUsed,"sunAlertUsed");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic Tree_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"tree") ) { return tree; }
		if (HX_FIELD_EQ(inName,"sunA") ) { return sunA; }
		if (HX_FIELD_EQ(inName,"sunB") ) { return sunB; }
		if (HX_FIELD_EQ(inName,"sunC") ) { return sunC; }
		if (HX_FIELD_EQ(inName,"draw") ) { return draw_dyn(); }
		if (HX_FIELD_EQ(inName,"kill") ) { return kill_dyn(); }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"apple") ) { return apple; }
		if (HX_FIELD_EQ(inName,"mouse") ) { return mouse; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"myTile") ) { return myTile; }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"droplet") ) { return droplet; }
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"dropletA") ) { return dropletA; }
		if (HX_FIELD_EQ(inName,"dropletB") ) { return dropletB; }
		if (HX_FIELD_EQ(inName,"boardObj") ) { return boardObj; }
		if (HX_FIELD_EQ(inName,"mathless") ) { return mathless; }
		if (HX_FIELD_EQ(inName,"getTreeX") ) { return getTreeX_dyn(); }
		if (HX_FIELD_EQ(inName,"getTreeY") ) { return getTreeY_dyn(); }
		if (HX_FIELD_EQ(inName,"setTreeX") ) { return setTreeX_dyn(); }
		if (HX_FIELD_EQ(inName,"setTreeY") ) { return setTreeY_dyn(); }
		if (HX_FIELD_EQ(inName,"growTree") ) { return growTree_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"treeGroup") ) { return treeGroup; }
		if (HX_FIELD_EQ(inName,"maxThirst") ) { return maxThirst; }
		if (HX_FIELD_EQ(inName,"minThirst") ) { return minThirst; }
		if (HX_FIELD_EQ(inName,"adultTree") ) { return adultTree; }
		if (HX_FIELD_EQ(inName,"goldCount") ) { return goldCount; }
		if (HX_FIELD_EQ(inName,"appleLock") ) { return appleLock; }
		if (HX_FIELD_EQ(inName,"getMyTile") ) { return getMyTile_dyn(); }
		if (HX_FIELD_EQ(inName,"setMyTile") ) { return setMyTile_dyn(); }
		if (HX_FIELD_EQ(inName,"pickApple") ) { return pickApple_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"appleArray") ) { return appleArray; }
		if (HX_FIELD_EQ(inName,"treeGround") ) { return treeGround; }
		if (HX_FIELD_EQ(inName,"treeThirst") ) { return treeThirst; }
		if (HX_FIELD_EQ(inName,"appleCount") ) { return appleCount; }
		if (HX_FIELD_EQ(inName,"appleIndex") ) { return appleIndex; }
		if (HX_FIELD_EQ(inName,"mouseTimer") ) { return mouseTimer; }
		if (HX_FIELD_EQ(inName,"sunClicked") ) { return sunClicked; }
		if (HX_FIELD_EQ(inName,"amIdeadYet") ) { return amIdeadYet; }
		if (HX_FIELD_EQ(inName,"alertPause") ) { return alertPause_dyn(); }
		if (HX_FIELD_EQ(inName,"warnThirst") ) { return warnThirst_dyn(); }
		if (HX_FIELD_EQ(inName,"appleStuff") ) { return appleStuff_dyn(); }
		if (HX_FIELD_EQ(inName,"carryApple") ) { return carryApple_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"mouseInPlay") ) { return mouseInPlay; }
		if (HX_FIELD_EQ(inName,"treeClicked") ) { return treeClicked; }
		if (HX_FIELD_EQ(inName,"appleStored") ) { return appleStored; }
		if (HX_FIELD_EQ(inName,"deathRattle") ) { return deathRattle; }
		if (HX_FIELD_EQ(inName,"openProblem") ) { return openProblem_dyn(); }
		if (HX_FIELD_EQ(inName,"mouseAttack") ) { return mouseAttack_dyn(); }
		if (HX_FIELD_EQ(inName,"incGrowTree") ) { return incGrowTree_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"basketSprite") ) { return basketSprite; }
		if (HX_FIELD_EQ(inName,"answerString") ) { return answerString; }
		if (HX_FIELD_EQ(inName,"treeSunlight") ) { return treeSunlight; }
		if (HX_FIELD_EQ(inName,"appleClicked") ) { return appleClicked; }
		if (HX_FIELD_EQ(inName,"extendedTime") ) { return extendedTime; }
		if (HX_FIELD_EQ(inName,"payWaterBill") ) { return payWaterBill; }
		if (HX_FIELD_EQ(inName,"sunAlertUsed") ) { return sunAlertUsed; }
		if (HX_FIELD_EQ(inName,"warnSunlight") ) { return warnSunlight_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"dropAlertTime") ) { return dropAlertTime; }
		if (HX_FIELD_EQ(inName,"dropAlertUsed") ) { return dropAlertUsed; }
		if (HX_FIELD_EQ(inName,"getTreeSprite") ) { return getTreeSprite_dyn(); }
		if (HX_FIELD_EQ(inName,"getAmIDeadYet") ) { return getAmIDeadYet_dyn(); }
		if (HX_FIELD_EQ(inName,"getTreeThirst") ) { return getTreeThirst_dyn(); }
		if (HX_FIELD_EQ(inName,"getSunClicked") ) { return getSunClicked_dyn(); }
		if (HX_FIELD_EQ(inName,"getAppleArray") ) { return getAppleArray_dyn(); }
		if (HX_FIELD_EQ(inName,"setSunClicked") ) { return setSunClicked_dyn(); }
		if (HX_FIELD_EQ(inName,"resetSunTimer") ) { return resetSunTimer_dyn(); }
		if (HX_FIELD_EQ(inName,"killAllApples") ) { return killAllApples_dyn(); }
		if (HX_FIELD_EQ(inName,"killDeadApple") ) { return killDeadApple_dyn(); }
		if (HX_FIELD_EQ(inName,"destroySounds") ) { return destroySounds_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"minusOneSprite") ) { return minusOneSprite; }
		if (HX_FIELD_EQ(inName,"sprinklerSound") ) { return sprinklerSound; }
		if (HX_FIELD_EQ(inName,"appleSeperator") ) { return appleSeperator; }
		if (HX_FIELD_EQ(inName,"dropletClicked") ) { return dropletClicked; }
		if (HX_FIELD_EQ(inName,"getTreeClicked") ) { return getTreeClicked_dyn(); }
		if (HX_FIELD_EQ(inName,"getAppleStored") ) { return getAppleStored_dyn(); }
		if (HX_FIELD_EQ(inName,"setAppleStored") ) { return setAppleStored_dyn(); }
		if (HX_FIELD_EQ(inName,"setTreeClicked") ) { return setTreeClicked_dyn(); }
		if (HX_FIELD_EQ(inName,"setTreeWatered") ) { return setTreeWatered_dyn(); }
		if (HX_FIELD_EQ(inName,"setTreeThirsty") ) { return setTreeThirsty_dyn(); }
		if (HX_FIELD_EQ(inName,"setThirstValue") ) { return setThirstValue_dyn(); }
		if (HX_FIELD_EQ(inName,"setTreeNoLight") ) { return setTreeNoLight_dyn(); }
		if (HX_FIELD_EQ(inName,"initAppleArray") ) { return initAppleArray_dyn(); }
		if (HX_FIELD_EQ(inName,"resetDropTimer") ) { return resetDropTimer_dyn(); }
		if (HX_FIELD_EQ(inName,"appleIncubator") ) { return appleIncubator_dyn(); }
		if (HX_FIELD_EQ(inName,"checkMouseDone") ) { return checkMouseDone_dyn(); }
		if (HX_FIELD_EQ(inName,"positionAlerts") ) { return positionAlerts_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"sprinklerSprite") ) { return sprinklerSprite; }
		if (HX_FIELD_EQ(inName,"treeGrowthSound") ) { return treeGrowthSound; }
		if (HX_FIELD_EQ(inName,"treeGrowthPhase") ) { return treeGrowthPhase; }
		if (HX_FIELD_EQ(inName,"takenAppleIndex") ) { return takenAppleIndex; }
		if (HX_FIELD_EQ(inName,"treeThirstTimer") ) { return treeThirstTimer; }
		if (HX_FIELD_EQ(inName,"timeToTreeDeath") ) { return timeToTreeDeath; }
		if (HX_FIELD_EQ(inName,"getAppleClicked") ) { return getAppleClicked_dyn(); }
		if (HX_FIELD_EQ(inName,"getPayWaterBill") ) { return getPayWaterBill_dyn(); }
		if (HX_FIELD_EQ(inName,"setPayWaterBill") ) { return setPayWaterBill_dyn(); }
		if (HX_FIELD_EQ(inName,"setAppleClicked") ) { return setAppleClicked_dyn(); }
		if (HX_FIELD_EQ(inName,"setTreeSunLight") ) { return setTreeSunLight_dyn(); }
		if (HX_FIELD_EQ(inName,"treeThirstCheck") ) { return treeThirstCheck_dyn(); }
		if (HX_FIELD_EQ(inName,"releaseAppleMem") ) { return releaseAppleMem_dyn(); }
		if (HX_FIELD_EQ(inName,"addAppleToArray") ) { return addAppleToArray_dyn(); }
		if (HX_FIELD_EQ(inName,"payWaterUtility") ) { return payWaterUtility_dyn(); }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"sprinklerOutTime") ) { return sprinklerOutTime; }
		if (HX_FIELD_EQ(inName,"sunAlertCoolDown") ) { return sunAlertCoolDown; }
		if (HX_FIELD_EQ(inName,"sprinklerInPhase") ) { return sprinklerInPhase; }
		if (HX_FIELD_EQ(inName,"greenHouseInPlay") ) { return greenHouseInPlay; }
		if (HX_FIELD_EQ(inName,"setTreeThirstTut") ) { return setTreeThirstTut_dyn(); }
		if (HX_FIELD_EQ(inName,"setSunlightValue") ) { return setSunlightValue_dyn(); }
		if (HX_FIELD_EQ(inName,"pauseAlertTimers") ) { return pauseAlertTimers_dyn(); }
		if (HX_FIELD_EQ(inName,"currentGoldCount") ) { return currentGoldCount_dyn(); }
		if (HX_FIELD_EQ(inName,"greenHouseEffect") ) { return greenHouseEffect_dyn(); }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"treeSunLightTimer") ) { return treeSunLightTimer; }
		if (HX_FIELD_EQ(inName,"sunlightAlertTime") ) { return sunlightAlertTime; }
		if (HX_FIELD_EQ(inName,"takenAppleCarried") ) { return takenAppleCarried; }
		if (HX_FIELD_EQ(inName,"sprinklerSystemOn") ) { return sprinklerSystemOn; }
		if (HX_FIELD_EQ(inName,"sprinklerOutPhase") ) { return sprinklerOutPhase; }
		if (HX_FIELD_EQ(inName,"getDropletClicked") ) { return getDropletClicked_dyn(); }
		if (HX_FIELD_EQ(inName,"setDropletClicked") ) { return setDropletClicked_dyn(); }
		if (HX_FIELD_EQ(inName,"treeSunlightCheck") ) { return treeSunlightCheck_dyn(); }
		if (HX_FIELD_EQ(inName,"checkAppleOverlap") ) { return checkAppleOverlap_dyn(); }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"alertSunPauseTimer") ) { return alertSunPauseTimer; }
		if (HX_FIELD_EQ(inName,"waterUtlilityTimer") ) { return waterUtlilityTimer; }
		if (HX_FIELD_EQ(inName,"oldSunTimerChanged") ) { return oldSunTimerChanged; }
		if (HX_FIELD_EQ(inName,"checkingCollisions") ) { return checkingCollisions; }
		if (HX_FIELD_EQ(inName,"getTreeGrowthPhase") ) { return getTreeGrowthPhase_dyn(); }
		if (HX_FIELD_EQ(inName,"setTreeSunlightTut") ) { return setTreeSunlightTut_dyn(); }
		if (HX_FIELD_EQ(inName,"setTreeThistTutOff") ) { return setTreeThistTutOff_dyn(); }
		if (HX_FIELD_EQ(inName,"setTreeGroundColor") ) { return setTreeGroundColor_dyn(); }
		if (HX_FIELD_EQ(inName,"setTreeGrowthPhase") ) { return setTreeGrowthPhase_dyn(); }
		if (HX_FIELD_EQ(inName,"unpauseAlertTimers") ) { return unpauseAlertTimers_dyn(); }
		if (HX_FIELD_EQ(inName,"registerAppleEvent") ) { return registerAppleEvent_dyn(); }
		if (HX_FIELD_EQ(inName,"collideGroundCheck") ) { return collideGroundCheck_dyn(); }
		if (HX_FIELD_EQ(inName,"sprinklerAnimation") ) { return sprinklerAnimation_dyn(); }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"alertDropPauseTimer") ) { return alertDropPauseTimer; }
		if (HX_FIELD_EQ(inName,"registerAlertEvents") ) { return registerAlertEvents_dyn(); }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"sprinklerButtonSound") ) { return sprinklerButtonSound; }
		if (HX_FIELD_EQ(inName,"oldTreeSunLightTimer") ) { return oldTreeSunLightTimer; }
		if (HX_FIELD_EQ(inName,"dropletAlertCoolDown") ) { return dropletAlertCoolDown; }
		if (HX_FIELD_EQ(inName,"getSprinklerSystemOn") ) { return getSprinklerSystemOn_dyn(); }
		if (HX_FIELD_EQ(inName,"setSprinklerSystemOn") ) { return setSprinklerSystemOn_dyn(); }
		if (HX_FIELD_EQ(inName,"unregisterAppleEvent") ) { return unregisterAppleEvent_dyn(); }
		if (HX_FIELD_EQ(inName,"checkForAppleRotTime") ) { return checkForAppleRotTime_dyn(); }
		break;
	case 21:
		if (HX_FIELD_EQ(inName,"universalAlertClicked") ) { return universalAlertClicked; }
		if (HX_FIELD_EQ(inName,"sprinklerButtonSprite") ) { return sprinklerButtonSprite; }
		if (HX_FIELD_EQ(inName,"setTreeSunlightTutOff") ) { return setTreeSunlightTutOff_dyn(); }
		if (HX_FIELD_EQ(inName,"unregisterAlertEvents") ) { return unregisterAlertEvents_dyn(); }
		if (HX_FIELD_EQ(inName,"purchaseSprinklerBuff") ) { return purchaseSprinklerBuff_dyn(); }
		break;
	case 22:
		if (HX_FIELD_EQ(inName,"registerSunAlertEvents") ) { return registerSunAlertEvents_dyn(); }
		break;
	case 23:
		if (HX_FIELD_EQ(inName,"sprinklerAnimationTimer") ) { return sprinklerAnimationTimer; }
		if (HX_FIELD_EQ(inName,"setOldTreeSunLightTimer") ) { return setOldTreeSunLightTimer_dyn(); }
		if (HX_FIELD_EQ(inName,"checkForMouseCollisions") ) { return checkForMouseCollisions_dyn(); }
		if (HX_FIELD_EQ(inName,"removeFallingAppleEvent") ) { return removeFallingAppleEvent_dyn(); }
		break;
	case 24:
		if (HX_FIELD_EQ(inName,"getUniversalAlertClicked") ) { return getUniversalAlertClicked_dyn(); }
		if (HX_FIELD_EQ(inName,"getSprinklerButtonSprite") ) { return getSprinklerButtonSprite_dyn(); }
		if (HX_FIELD_EQ(inName,"setUniversalAlertClicked") ) { return setUniversalAlertClicked_dyn(); }
		if (HX_FIELD_EQ(inName,"unregisterSunAlertEvents") ) { return unregisterSunAlertEvents_dyn(); }
		break;
	case 26:
		if (HX_FIELD_EQ(inName,"registerDropletAlertEvents") ) { return registerDropletAlertEvents_dyn(); }
		break;
	case 28:
		if (HX_FIELD_EQ(inName,"unregisterDropletAlertEvents") ) { return unregisterDropletAlertEvents_dyn(); }
		break;
	case 29:
		if (HX_FIELD_EQ(inName,"registerTreeClickedMouseEvent") ) { return registerTreeClickedMouseEvent_dyn(); }
		break;
	case 31:
		if (HX_FIELD_EQ(inName,"unregisterTreeClickedMouseEvent") ) { return unregisterTreeClickedMouseEvent_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Tree_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"tree") ) { tree=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"sunA") ) { sunA=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"sunB") ) { sunB=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"sunC") ) { sunC=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"apple") ) { apple=inValue.Cast< ::Apple >(); return inValue; }
		if (HX_FIELD_EQ(inName,"mouse") ) { mouse=inValue.Cast< ::Mouse >(); return inValue; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"myTile") ) { myTile=inValue.Cast< ::Tile >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"droplet") ) { droplet=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"dropletA") ) { dropletA=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"dropletB") ) { dropletB=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"boardObj") ) { boardObj=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"mathless") ) { mathless=inValue.Cast< bool >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"treeGroup") ) { treeGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"maxThirst") ) { maxThirst=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"minThirst") ) { minThirst=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"adultTree") ) { adultTree=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"goldCount") ) { goldCount=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"appleLock") ) { appleLock=inValue.Cast< bool >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"appleArray") ) { appleArray=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		if (HX_FIELD_EQ(inName,"treeGround") ) { treeGround=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"treeThirst") ) { treeThirst=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"appleCount") ) { appleCount=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"appleIndex") ) { appleIndex=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"mouseTimer") ) { mouseTimer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"sunClicked") ) { sunClicked=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"amIdeadYet") ) { amIdeadYet=inValue.Cast< bool >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"mouseInPlay") ) { mouseInPlay=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"treeClicked") ) { treeClicked=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"appleStored") ) { appleStored=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"deathRattle") ) { deathRattle=inValue.Cast< bool >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"basketSprite") ) { basketSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"answerString") ) { answerString=inValue.Cast< ::String >(); return inValue; }
		if (HX_FIELD_EQ(inName,"treeSunlight") ) { treeSunlight=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"appleClicked") ) { appleClicked=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"extendedTime") ) { extendedTime=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"payWaterBill") ) { payWaterBill=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"sunAlertUsed") ) { sunAlertUsed=inValue.Cast< bool >(); return inValue; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"dropAlertTime") ) { dropAlertTime=inValue.Cast< ::flixel::util::FlxTimer >(); return inValue; }
		if (HX_FIELD_EQ(inName,"dropAlertUsed") ) { dropAlertUsed=inValue.Cast< bool >(); return inValue; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"minusOneSprite") ) { minusOneSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"sprinklerSound") ) { sprinklerSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		if (HX_FIELD_EQ(inName,"appleSeperator") ) { appleSeperator=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"dropletClicked") ) { dropletClicked=inValue.Cast< bool >(); return inValue; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"sprinklerSprite") ) { sprinklerSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"treeGrowthSound") ) { treeGrowthSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		if (HX_FIELD_EQ(inName,"treeGrowthPhase") ) { treeGrowthPhase=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"takenAppleIndex") ) { takenAppleIndex=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"treeThirstTimer") ) { treeThirstTimer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"timeToTreeDeath") ) { timeToTreeDeath=inValue.Cast< Float >(); return inValue; }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"sprinklerOutTime") ) { sprinklerOutTime=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"sunAlertCoolDown") ) { sunAlertCoolDown=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"sprinklerInPhase") ) { sprinklerInPhase=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"greenHouseInPlay") ) { greenHouseInPlay=inValue.Cast< bool >(); return inValue; }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"treeSunLightTimer") ) { treeSunLightTimer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"sunlightAlertTime") ) { sunlightAlertTime=inValue.Cast< ::flixel::util::FlxTimer >(); return inValue; }
		if (HX_FIELD_EQ(inName,"takenAppleCarried") ) { takenAppleCarried=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"sprinklerSystemOn") ) { sprinklerSystemOn=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"sprinklerOutPhase") ) { sprinklerOutPhase=inValue.Cast< bool >(); return inValue; }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"alertSunPauseTimer") ) { alertSunPauseTimer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"waterUtlilityTimer") ) { waterUtlilityTimer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"oldSunTimerChanged") ) { oldSunTimerChanged=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"checkingCollisions") ) { checkingCollisions=inValue.Cast< bool >(); return inValue; }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"alertDropPauseTimer") ) { alertDropPauseTimer=inValue.Cast< Float >(); return inValue; }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"sprinklerButtonSound") ) { sprinklerButtonSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		if (HX_FIELD_EQ(inName,"oldTreeSunLightTimer") ) { oldTreeSunLightTimer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"dropletAlertCoolDown") ) { dropletAlertCoolDown=inValue.Cast< Float >(); return inValue; }
		break;
	case 21:
		if (HX_FIELD_EQ(inName,"universalAlertClicked") ) { universalAlertClicked=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"sprinklerButtonSprite") ) { sprinklerButtonSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 23:
		if (HX_FIELD_EQ(inName,"sprinklerAnimationTimer") ) { sprinklerAnimationTimer=inValue.Cast< Float >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Tree_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("treeGroup"));
	outFields->push(HX_CSTRING("appleArray"));
	outFields->push(HX_CSTRING("apple"));
	outFields->push(HX_CSTRING("myTile"));
	outFields->push(HX_CSTRING("mouse"));
	outFields->push(HX_CSTRING("treeGround"));
	outFields->push(HX_CSTRING("tree"));
	outFields->push(HX_CSTRING("droplet"));
	outFields->push(HX_CSTRING("dropletA"));
	outFields->push(HX_CSTRING("dropletB"));
	outFields->push(HX_CSTRING("sunA"));
	outFields->push(HX_CSTRING("sunB"));
	outFields->push(HX_CSTRING("sunC"));
	outFields->push(HX_CSTRING("sprinklerSprite"));
	outFields->push(HX_CSTRING("sprinklerButtonSprite"));
	outFields->push(HX_CSTRING("minusOneSprite"));
	outFields->push(HX_CSTRING("basketSprite"));
	outFields->push(HX_CSTRING("sprinklerSound"));
	outFields->push(HX_CSTRING("treeGrowthSound"));
	outFields->push(HX_CSTRING("sprinklerButtonSound"));
	outFields->push(HX_CSTRING("boardObj"));
	outFields->push(HX_CSTRING("answerString"));
	outFields->push(HX_CSTRING("maxThirst"));
	outFields->push(HX_CSTRING("minThirst"));
	outFields->push(HX_CSTRING("treeGrowthPhase"));
	outFields->push(HX_CSTRING("treeThirst"));
	outFields->push(HX_CSTRING("treeSunlight"));
	outFields->push(HX_CSTRING("appleCount"));
	outFields->push(HX_CSTRING("appleIndex"));
	outFields->push(HX_CSTRING("adultTree"));
	outFields->push(HX_CSTRING("takenAppleIndex"));
	outFields->push(HX_CSTRING("goldCount"));
	outFields->push(HX_CSTRING("alertDropPauseTimer"));
	outFields->push(HX_CSTRING("alertSunPauseTimer"));
	outFields->push(HX_CSTRING("treeThirstTimer"));
	outFields->push(HX_CSTRING("treeSunLightTimer"));
	outFields->push(HX_CSTRING("oldTreeSunLightTimer"));
	outFields->push(HX_CSTRING("waterUtlilityTimer"));
	outFields->push(HX_CSTRING("sprinklerOutTime"));
	outFields->push(HX_CSTRING("timeToTreeDeath"));
	outFields->push(HX_CSTRING("appleSeperator"));
	outFields->push(HX_CSTRING("mouseTimer"));
	outFields->push(HX_CSTRING("sunAlertCoolDown"));
	outFields->push(HX_CSTRING("dropletAlertCoolDown"));
	outFields->push(HX_CSTRING("sprinklerAnimationTimer"));
	outFields->push(HX_CSTRING("dropAlertTime"));
	outFields->push(HX_CSTRING("sunlightAlertTime"));
	outFields->push(HX_CSTRING("oldSunTimerChanged"));
	outFields->push(HX_CSTRING("mouseInPlay"));
	outFields->push(HX_CSTRING("dropletClicked"));
	outFields->push(HX_CSTRING("sunClicked"));
	outFields->push(HX_CSTRING("treeClicked"));
	outFields->push(HX_CSTRING("appleLock"));
	outFields->push(HX_CSTRING("amIdeadYet"));
	outFields->push(HX_CSTRING("appleClicked"));
	outFields->push(HX_CSTRING("takenAppleCarried"));
	outFields->push(HX_CSTRING("checkingCollisions"));
	outFields->push(HX_CSTRING("sprinklerSystemOn"));
	outFields->push(HX_CSTRING("sprinklerOutPhase"));
	outFields->push(HX_CSTRING("sprinklerInPhase"));
	outFields->push(HX_CSTRING("extendedTime"));
	outFields->push(HX_CSTRING("payWaterBill"));
	outFields->push(HX_CSTRING("appleStored"));
	outFields->push(HX_CSTRING("deathRattle"));
	outFields->push(HX_CSTRING("mathless"));
	outFields->push(HX_CSTRING("greenHouseInPlay"));
	outFields->push(HX_CSTRING("dropAlertUsed"));
	outFields->push(HX_CSTRING("sunAlertUsed"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("universalAlertClicked"),
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(Tree_obj,treeGroup),HX_CSTRING("treeGroup")},
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(Tree_obj,appleArray),HX_CSTRING("appleArray")},
	{hx::fsObject /*::Apple*/ ,(int)offsetof(Tree_obj,apple),HX_CSTRING("apple")},
	{hx::fsObject /*::Tile*/ ,(int)offsetof(Tree_obj,myTile),HX_CSTRING("myTile")},
	{hx::fsObject /*::Mouse*/ ,(int)offsetof(Tree_obj,mouse),HX_CSTRING("mouse")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Tree_obj,treeGround),HX_CSTRING("treeGround")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Tree_obj,tree),HX_CSTRING("tree")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Tree_obj,droplet),HX_CSTRING("droplet")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Tree_obj,dropletA),HX_CSTRING("dropletA")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Tree_obj,dropletB),HX_CSTRING("dropletB")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Tree_obj,sunA),HX_CSTRING("sunA")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Tree_obj,sunB),HX_CSTRING("sunB")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Tree_obj,sunC),HX_CSTRING("sunC")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Tree_obj,sprinklerSprite),HX_CSTRING("sprinklerSprite")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Tree_obj,sprinklerButtonSprite),HX_CSTRING("sprinklerButtonSprite")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Tree_obj,minusOneSprite),HX_CSTRING("minusOneSprite")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Tree_obj,basketSprite),HX_CSTRING("basketSprite")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(Tree_obj,sprinklerSound),HX_CSTRING("sprinklerSound")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(Tree_obj,treeGrowthSound),HX_CSTRING("treeGrowthSound")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(Tree_obj,sprinklerButtonSound),HX_CSTRING("sprinklerButtonSound")},
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(Tree_obj,boardObj),HX_CSTRING("boardObj")},
	{hx::fsString,(int)offsetof(Tree_obj,answerString),HX_CSTRING("answerString")},
	{hx::fsInt,(int)offsetof(Tree_obj,maxThirst),HX_CSTRING("maxThirst")},
	{hx::fsInt,(int)offsetof(Tree_obj,minThirst),HX_CSTRING("minThirst")},
	{hx::fsInt,(int)offsetof(Tree_obj,treeGrowthPhase),HX_CSTRING("treeGrowthPhase")},
	{hx::fsInt,(int)offsetof(Tree_obj,treeThirst),HX_CSTRING("treeThirst")},
	{hx::fsInt,(int)offsetof(Tree_obj,treeSunlight),HX_CSTRING("treeSunlight")},
	{hx::fsInt,(int)offsetof(Tree_obj,appleCount),HX_CSTRING("appleCount")},
	{hx::fsInt,(int)offsetof(Tree_obj,appleIndex),HX_CSTRING("appleIndex")},
	{hx::fsInt,(int)offsetof(Tree_obj,adultTree),HX_CSTRING("adultTree")},
	{hx::fsInt,(int)offsetof(Tree_obj,takenAppleIndex),HX_CSTRING("takenAppleIndex")},
	{hx::fsInt,(int)offsetof(Tree_obj,goldCount),HX_CSTRING("goldCount")},
	{hx::fsFloat,(int)offsetof(Tree_obj,alertDropPauseTimer),HX_CSTRING("alertDropPauseTimer")},
	{hx::fsFloat,(int)offsetof(Tree_obj,alertSunPauseTimer),HX_CSTRING("alertSunPauseTimer")},
	{hx::fsFloat,(int)offsetof(Tree_obj,treeThirstTimer),HX_CSTRING("treeThirstTimer")},
	{hx::fsFloat,(int)offsetof(Tree_obj,treeSunLightTimer),HX_CSTRING("treeSunLightTimer")},
	{hx::fsFloat,(int)offsetof(Tree_obj,oldTreeSunLightTimer),HX_CSTRING("oldTreeSunLightTimer")},
	{hx::fsFloat,(int)offsetof(Tree_obj,waterUtlilityTimer),HX_CSTRING("waterUtlilityTimer")},
	{hx::fsFloat,(int)offsetof(Tree_obj,sprinklerOutTime),HX_CSTRING("sprinklerOutTime")},
	{hx::fsFloat,(int)offsetof(Tree_obj,timeToTreeDeath),HX_CSTRING("timeToTreeDeath")},
	{hx::fsFloat,(int)offsetof(Tree_obj,appleSeperator),HX_CSTRING("appleSeperator")},
	{hx::fsFloat,(int)offsetof(Tree_obj,mouseTimer),HX_CSTRING("mouseTimer")},
	{hx::fsFloat,(int)offsetof(Tree_obj,sunAlertCoolDown),HX_CSTRING("sunAlertCoolDown")},
	{hx::fsFloat,(int)offsetof(Tree_obj,dropletAlertCoolDown),HX_CSTRING("dropletAlertCoolDown")},
	{hx::fsFloat,(int)offsetof(Tree_obj,sprinklerAnimationTimer),HX_CSTRING("sprinklerAnimationTimer")},
	{hx::fsObject /*::flixel::util::FlxTimer*/ ,(int)offsetof(Tree_obj,dropAlertTime),HX_CSTRING("dropAlertTime")},
	{hx::fsObject /*::flixel::util::FlxTimer*/ ,(int)offsetof(Tree_obj,sunlightAlertTime),HX_CSTRING("sunlightAlertTime")},
	{hx::fsBool,(int)offsetof(Tree_obj,oldSunTimerChanged),HX_CSTRING("oldSunTimerChanged")},
	{hx::fsBool,(int)offsetof(Tree_obj,mouseInPlay),HX_CSTRING("mouseInPlay")},
	{hx::fsBool,(int)offsetof(Tree_obj,dropletClicked),HX_CSTRING("dropletClicked")},
	{hx::fsBool,(int)offsetof(Tree_obj,sunClicked),HX_CSTRING("sunClicked")},
	{hx::fsBool,(int)offsetof(Tree_obj,treeClicked),HX_CSTRING("treeClicked")},
	{hx::fsBool,(int)offsetof(Tree_obj,appleLock),HX_CSTRING("appleLock")},
	{hx::fsBool,(int)offsetof(Tree_obj,amIdeadYet),HX_CSTRING("amIdeadYet")},
	{hx::fsBool,(int)offsetof(Tree_obj,appleClicked),HX_CSTRING("appleClicked")},
	{hx::fsBool,(int)offsetof(Tree_obj,takenAppleCarried),HX_CSTRING("takenAppleCarried")},
	{hx::fsBool,(int)offsetof(Tree_obj,checkingCollisions),HX_CSTRING("checkingCollisions")},
	{hx::fsBool,(int)offsetof(Tree_obj,sprinklerSystemOn),HX_CSTRING("sprinklerSystemOn")},
	{hx::fsBool,(int)offsetof(Tree_obj,sprinklerOutPhase),HX_CSTRING("sprinklerOutPhase")},
	{hx::fsBool,(int)offsetof(Tree_obj,sprinklerInPhase),HX_CSTRING("sprinklerInPhase")},
	{hx::fsBool,(int)offsetof(Tree_obj,extendedTime),HX_CSTRING("extendedTime")},
	{hx::fsBool,(int)offsetof(Tree_obj,payWaterBill),HX_CSTRING("payWaterBill")},
	{hx::fsBool,(int)offsetof(Tree_obj,appleStored),HX_CSTRING("appleStored")},
	{hx::fsBool,(int)offsetof(Tree_obj,deathRattle),HX_CSTRING("deathRattle")},
	{hx::fsBool,(int)offsetof(Tree_obj,mathless),HX_CSTRING("mathless")},
	{hx::fsBool,(int)offsetof(Tree_obj,greenHouseInPlay),HX_CSTRING("greenHouseInPlay")},
	{hx::fsBool,(int)offsetof(Tree_obj,dropAlertUsed),HX_CSTRING("dropAlertUsed")},
	{hx::fsBool,(int)offsetof(Tree_obj,sunAlertUsed),HX_CSTRING("sunAlertUsed")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("treeGroup"),
	HX_CSTRING("appleArray"),
	HX_CSTRING("apple"),
	HX_CSTRING("myTile"),
	HX_CSTRING("mouse"),
	HX_CSTRING("treeGround"),
	HX_CSTRING("tree"),
	HX_CSTRING("droplet"),
	HX_CSTRING("dropletA"),
	HX_CSTRING("dropletB"),
	HX_CSTRING("sunA"),
	HX_CSTRING("sunB"),
	HX_CSTRING("sunC"),
	HX_CSTRING("sprinklerSprite"),
	HX_CSTRING("sprinklerButtonSprite"),
	HX_CSTRING("minusOneSprite"),
	HX_CSTRING("basketSprite"),
	HX_CSTRING("sprinklerSound"),
	HX_CSTRING("treeGrowthSound"),
	HX_CSTRING("sprinklerButtonSound"),
	HX_CSTRING("boardObj"),
	HX_CSTRING("answerString"),
	HX_CSTRING("maxThirst"),
	HX_CSTRING("minThirst"),
	HX_CSTRING("treeGrowthPhase"),
	HX_CSTRING("treeThirst"),
	HX_CSTRING("treeSunlight"),
	HX_CSTRING("appleCount"),
	HX_CSTRING("appleIndex"),
	HX_CSTRING("adultTree"),
	HX_CSTRING("takenAppleIndex"),
	HX_CSTRING("goldCount"),
	HX_CSTRING("alertDropPauseTimer"),
	HX_CSTRING("alertSunPauseTimer"),
	HX_CSTRING("treeThirstTimer"),
	HX_CSTRING("treeSunLightTimer"),
	HX_CSTRING("oldTreeSunLightTimer"),
	HX_CSTRING("waterUtlilityTimer"),
	HX_CSTRING("sprinklerOutTime"),
	HX_CSTRING("timeToTreeDeath"),
	HX_CSTRING("appleSeperator"),
	HX_CSTRING("mouseTimer"),
	HX_CSTRING("sunAlertCoolDown"),
	HX_CSTRING("dropletAlertCoolDown"),
	HX_CSTRING("sprinklerAnimationTimer"),
	HX_CSTRING("dropAlertTime"),
	HX_CSTRING("sunlightAlertTime"),
	HX_CSTRING("oldSunTimerChanged"),
	HX_CSTRING("mouseInPlay"),
	HX_CSTRING("dropletClicked"),
	HX_CSTRING("sunClicked"),
	HX_CSTRING("treeClicked"),
	HX_CSTRING("appleLock"),
	HX_CSTRING("amIdeadYet"),
	HX_CSTRING("appleClicked"),
	HX_CSTRING("takenAppleCarried"),
	HX_CSTRING("checkingCollisions"),
	HX_CSTRING("sprinklerSystemOn"),
	HX_CSTRING("sprinklerOutPhase"),
	HX_CSTRING("sprinklerInPhase"),
	HX_CSTRING("extendedTime"),
	HX_CSTRING("payWaterBill"),
	HX_CSTRING("appleStored"),
	HX_CSTRING("deathRattle"),
	HX_CSTRING("mathless"),
	HX_CSTRING("greenHouseInPlay"),
	HX_CSTRING("dropAlertUsed"),
	HX_CSTRING("sunAlertUsed"),
	HX_CSTRING("getTreeClicked"),
	HX_CSTRING("getTreeSprite"),
	HX_CSTRING("getTreeX"),
	HX_CSTRING("getTreeY"),
	HX_CSTRING("getAmIDeadYet"),
	HX_CSTRING("getTreeThirst"),
	HX_CSTRING("getAppleStored"),
	HX_CSTRING("setAppleStored"),
	HX_CSTRING("getDropletClicked"),
	HX_CSTRING("getSunClicked"),
	HX_CSTRING("getUniversalAlertClicked"),
	HX_CSTRING("getMyTile"),
	HX_CSTRING("getAppleClicked"),
	HX_CSTRING("getTreeGrowthPhase"),
	HX_CSTRING("getSprinklerSystemOn"),
	HX_CSTRING("getSprinklerButtonSprite"),
	HX_CSTRING("getPayWaterBill"),
	HX_CSTRING("getAppleArray"),
	HX_CSTRING("setTreeThirstTut"),
	HX_CSTRING("setTreeSunlightTut"),
	HX_CSTRING("setTreeThistTutOff"),
	HX_CSTRING("setTreeSunlightTutOff"),
	HX_CSTRING("setPayWaterBill"),
	HX_CSTRING("setSprinklerSystemOn"),
	HX_CSTRING("setTreeClicked"),
	HX_CSTRING("setAppleClicked"),
	HX_CSTRING("setMyTile"),
	HX_CSTRING("setTreeGroundColor"),
	HX_CSTRING("setTreeGrowthPhase"),
	HX_CSTRING("setUniversalAlertClicked"),
	HX_CSTRING("setDropletClicked"),
	HX_CSTRING("setSunClicked"),
	HX_CSTRING("setTreeX"),
	HX_CSTRING("setTreeY"),
	HX_CSTRING("setTreeWatered"),
	HX_CSTRING("setTreeThirsty"),
	HX_CSTRING("setTreeSunLight"),
	HX_CSTRING("setOldTreeSunLightTimer"),
	HX_CSTRING("setThirstValue"),
	HX_CSTRING("setSunlightValue"),
	HX_CSTRING("setTreeNoLight"),
	HX_CSTRING("pauseAlertTimers"),
	HX_CSTRING("unpauseAlertTimers"),
	HX_CSTRING("initAppleArray"),
	HX_CSTRING("resetDropTimer"),
	HX_CSTRING("resetSunTimer"),
	HX_CSTRING("registerTreeClickedMouseEvent"),
	HX_CSTRING("unregisterTreeClickedMouseEvent"),
	HX_CSTRING("registerAppleEvent"),
	HX_CSTRING("unregisterAppleEvent"),
	HX_CSTRING("registerAlertEvents"),
	HX_CSTRING("unregisterAlertEvents"),
	HX_CSTRING("registerSunAlertEvents"),
	HX_CSTRING("unregisterSunAlertEvents"),
	HX_CSTRING("registerDropletAlertEvents"),
	HX_CSTRING("unregisterDropletAlertEvents"),
	HX_CSTRING("openProblem"),
	HX_CSTRING("alertPause"),
	HX_CSTRING("purchaseSprinklerBuff"),
	HX_CSTRING("treeThirstCheck"),
	HX_CSTRING("treeSunlightCheck"),
	HX_CSTRING("warnThirst"),
	HX_CSTRING("warnSunlight"),
	HX_CSTRING("appleIncubator"),
	HX_CSTRING("releaseAppleMem"),
	HX_CSTRING("killAllApples"),
	HX_CSTRING("addAppleToArray"),
	HX_CSTRING("appleStuff"),
	HX_CSTRING("pickApple"),
	HX_CSTRING("killDeadApple"),
	HX_CSTRING("checkAppleOverlap"),
	HX_CSTRING("mouseAttack"),
	HX_CSTRING("checkForAppleRotTime"),
	HX_CSTRING("checkMouseDone"),
	HX_CSTRING("checkForMouseCollisions"),
	HX_CSTRING("carryApple"),
	HX_CSTRING("incGrowTree"),
	HX_CSTRING("growTree"),
	HX_CSTRING("positionAlerts"),
	HX_CSTRING("collideGroundCheck"),
	HX_CSTRING("sprinklerAnimation"),
	HX_CSTRING("currentGoldCount"),
	HX_CSTRING("payWaterUtility"),
	HX_CSTRING("destroySounds"),
	HX_CSTRING("greenHouseEffect"),
	HX_CSTRING("removeFallingAppleEvent"),
	HX_CSTRING("draw"),
	HX_CSTRING("kill"),
	HX_CSTRING("update"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Tree_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(Tree_obj::universalAlertClicked,"universalAlertClicked");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Tree_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(Tree_obj::universalAlertClicked,"universalAlertClicked");
};

#endif

Class Tree_obj::__mClass;

void Tree_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("Tree"), hx::TCanCast< Tree_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void Tree_obj::__boot()
{
	universalAlertClicked= false;
}

