#include <hxcpp.h>

#ifndef INCLUDED_GoldCoin
#include <GoldCoin.h>
#endif
#ifndef INCLUDED_GreenHouseItem
#include <GreenHouseItem.h>
#endif
#ifndef INCLUDED_JuiceItem
#include <JuiceItem.h>
#endif
#ifndef INCLUDED_KoiItem
#include <KoiItem.h>
#endif
#ifndef INCLUDED_Market
#include <Market.h>
#endif
#ifndef INCLUDED_MarketItem
#include <MarketItem.h>
#endif
#ifndef INCLUDED_PieItem
#include <PieItem.h>
#endif
#ifndef INCLUDED_SeedItem
#include <SeedItem.h>
#endif
#ifndef INCLUDED_Std
#include <Std.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_plugin_FlxPlugin
#include <flixel/plugin/FlxPlugin.h>
#endif
#ifndef INCLUDED_flixel_plugin_MouseEventManager
#include <flixel/plugin/MouseEventManager.h>
#endif
#ifndef INCLUDED_flixel_system_FlxSound
#include <flixel/system/FlxSound.h>
#endif
#ifndef INCLUDED_flixel_system_frontEnds_SoundFrontEnd
#include <flixel/system/frontEnds/SoundFrontEnd.h>
#endif
#ifndef INCLUDED_flixel_text_FlxText
#include <flixel/text/FlxText.h>
#endif
#ifndef INCLUDED_flixel_util_FlxDestroyUtil
#include <flixel/util/FlxDestroyUtil.h>
#endif
#ifndef INCLUDED_openfl__v2_events_EventDispatcher
#include <openfl/_v2/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__v2_events_IEventDispatcher
#include <openfl/_v2/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__v2_media_SoundChannel
#include <openfl/_v2/media/SoundChannel.h>
#endif

Void Market_obj::__construct()
{
HX_STACK_FRAME("Market","new",0xe910a8ce,"Market.new","Market.hx",14,0xdfc0ea62)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(69)
	this->closedSoundPlayed = false;
	HX_STACK_LINE(68)
	this->turn = true;
	HX_STACK_LINE(67)
	this->marketOpen = false;
	HX_STACK_LINE(66)
	this->electricBillTimer = (int)10;
	HX_STACK_LINE(65)
	this->waterBillTimer = (int)30;
	HX_STACK_LINE(64)
	this->replenishPieMachineSeedTimer = (int)300;
	HX_STACK_LINE(63)
	this->replenishJuiceMachineTimer = (int)300;
	HX_STACK_LINE(62)
	this->replenishSeedTimer = (int)180;
	HX_STACK_LINE(61)
	this->marketCloseTimer = (int)10;
	HX_STACK_LINE(55)
	this->seedCountString = HX_CSTRING("");
	HX_STACK_LINE(54)
	this->shiftY = (int)2;
	HX_STACK_LINE(53)
	this->shiftX = (int)41;
	HX_STACK_LINE(52)
	this->electricClientCount = (int)0;
	HX_STACK_LINE(51)
	this->waterClientCount = (int)0;
	HX_STACK_LINE(50)
	this->greenHouseCount = (int)0;
	HX_STACK_LINE(49)
	this->fishCount = (int)0;
	HX_STACK_LINE(48)
	this->pieCount = (int)0;
	HX_STACK_LINE(47)
	this->juiceCount = (int)0;
	HX_STACK_LINE(46)
	this->seedCount = (int)1;
	HX_STACK_LINE(45)
	this->juicePack = (int)0;
	HX_STACK_LINE(44)
	this->piePack = (int)0;
	HX_STACK_LINE(42)
	this->coinCountString = HX_CSTRING("");
	HX_STACK_LINE(41)
	this->coinCount = (int)90;
	HX_STACK_LINE(76)
	super::__construct(null());
	HX_STACK_LINE(77)
	::flixel::group::FlxGroup _g = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(77)
	this->marketGroup = _g;
	HX_STACK_LINE(78)
	::flixel::FlxSprite _g1 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
	HX_STACK_LINE(78)
	this->marketSideBarSprite = _g1;
	HX_STACK_LINE(79)
	::flixel::FlxSprite _g2 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g2,"_g2");
	HX_STACK_LINE(79)
	this->marketSideBarTabSprite = _g2;
	HX_STACK_LINE(80)
	::flixel::FlxSprite _g3 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g3,"_g3");
	HX_STACK_LINE(80)
	this->marketSideBarShaftSprite = _g3;
	HX_STACK_LINE(81)
	::SeedItem _g4 = ::SeedItem_obj::__new();		HX_STACK_VAR(_g4,"_g4");
	HX_STACK_LINE(81)
	this->seedItem = _g4;
	HX_STACK_LINE(82)
	::JuiceItem _g5 = ::JuiceItem_obj::__new();		HX_STACK_VAR(_g5,"_g5");
	HX_STACK_LINE(82)
	this->juiceItem = _g5;
	HX_STACK_LINE(83)
	::PieItem _g6 = ::PieItem_obj::__new();		HX_STACK_VAR(_g6,"_g6");
	HX_STACK_LINE(83)
	this->pieItem = _g6;
	HX_STACK_LINE(84)
	::GoldCoin _g7 = ::GoldCoin_obj::__new();		HX_STACK_VAR(_g7,"_g7");
	HX_STACK_LINE(84)
	this->coin = _g7;
	HX_STACK_LINE(85)
	::KoiItem _g8 = ::KoiItem_obj::__new();		HX_STACK_VAR(_g8,"_g8");
	HX_STACK_LINE(85)
	this->koiFishItem = _g8;
	HX_STACK_LINE(86)
	::GreenHouseItem _g9 = ::GreenHouseItem_obj::__new();		HX_STACK_VAR(_g9,"_g9");
	HX_STACK_LINE(86)
	this->greenHouseItem = _g9;
	HX_STACK_LINE(87)
	::flixel::text::FlxText _g10 = ::flixel::text::FlxText_obj::__new((int)-21,(int)90,(int)16,this->seedCountString,(int)8,false);		HX_STACK_VAR(_g10,"_g10");
	HX_STACK_LINE(87)
	this->seedCountText = _g10;
	HX_STACK_LINE(88)
	::String _g11 = ::Std_obj::string(this->juiceCount);		HX_STACK_VAR(_g11,"_g11");
	HX_STACK_LINE(88)
	::flixel::text::FlxText _g12 = ::flixel::text::FlxText_obj::__new((int)-45,(int)90,(int)16,_g11,(int)8,false);		HX_STACK_VAR(_g12,"_g12");
	HX_STACK_LINE(88)
	this->juiceCountText = _g12;
	HX_STACK_LINE(89)
	::String _g13 = ::Std_obj::string(this->pieCount);		HX_STACK_VAR(_g13,"_g13");
	HX_STACK_LINE(89)
	::flixel::text::FlxText _g14 = ::flixel::text::FlxText_obj::__new((int)-71,(int)90,(int)16,_g13,(int)8,false);		HX_STACK_VAR(_g14,"_g14");
	HX_STACK_LINE(89)
	this->pieCountText = _g14;
	HX_STACK_LINE(90)
	::String _g15 = ::Std_obj::string(this->fishCount);		HX_STACK_VAR(_g15,"_g15");
	HX_STACK_LINE(90)
	::flixel::text::FlxText _g16 = ::flixel::text::FlxText_obj::__new((int)-97,(int)90,(int)16,_g15,(int)8,false);		HX_STACK_VAR(_g16,"_g16");
	HX_STACK_LINE(90)
	this->fishCountText = _g16;
	HX_STACK_LINE(91)
	::String _g17 = ::Std_obj::string(this->greenHouseCount);		HX_STACK_VAR(_g17,"_g17");
	HX_STACK_LINE(91)
	::flixel::text::FlxText _g18 = ::flixel::text::FlxText_obj::__new((int)-119,(int)90,(int)16,_g17,(int)8,false);		HX_STACK_VAR(_g18,"_g18");
	HX_STACK_LINE(91)
	this->greenHouseCountText = _g18;
	HX_STACK_LINE(92)
	::String _g19 = ::Std_obj::string(this->juicePack);		HX_STACK_VAR(_g19,"_g19");
	HX_STACK_LINE(92)
	::flixel::text::FlxText _g20 = ::flixel::text::FlxText_obj::__new((int)180,(int)43,(int)32,_g19,(int)12,false);		HX_STACK_VAR(_g20,"_g20");
	HX_STACK_LINE(92)
	this->juicePackCountText = _g20;
	HX_STACK_LINE(93)
	::String _g21 = ::Std_obj::string(this->juicePack);		HX_STACK_VAR(_g21,"_g21");
	HX_STACK_LINE(93)
	::flixel::text::FlxText _g22 = ::flixel::text::FlxText_obj::__new((int)237,(int)43,(int)32,_g21,(int)12,false);		HX_STACK_VAR(_g22,"_g22");
	HX_STACK_LINE(93)
	this->piePackCountText = _g22;
	HX_STACK_LINE(94)
	this->seedCountText->set_color((int)268435455);
	HX_STACK_LINE(95)
	this->juiceCountText->set_color((int)268435455);
	HX_STACK_LINE(96)
	this->pieCountText->set_color((int)268435455);
	HX_STACK_LINE(97)
	this->fishCountText->set_color((int)268435455);
	HX_STACK_LINE(98)
	this->piePackCountText->set_color((int)268435455);
	HX_STACK_LINE(99)
	this->juicePackCountText->set_color((int)268435455);
	HX_STACK_LINE(100)
	this->greenHouseCountText->set_color((int)268435455);
	HX_STACK_LINE(101)
	::flixel::text::FlxText _g23 = ::flixel::text::FlxText_obj::__new((int)53,(int)38,(int)64,this->coinCountString,(int)16,false);		HX_STACK_VAR(_g23,"_g23");
	HX_STACK_LINE(101)
	this->coinCountFieldText = _g23;
	HX_STACK_LINE(102)
	this->coinCountFieldText->set_color((int)268435455);
	HX_STACK_LINE(103)
	Array< ::Dynamic > _g24 = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(_g24,"_g24");
	HX_STACK_LINE(103)
	this->itemArray = _g24;
	HX_STACK_LINE(104)
	this->marketSideBarSprite->loadGraphic(HX_CSTRING("assets/images/marketSideBar.png"),false,null(),null(),null(),null());
	HX_STACK_LINE(105)
	this->marketSideBarTabSprite->loadGraphic(HX_CSTRING("assets/images/marketSideBarTab.png"),false,null(),null(),null(),null());
	HX_STACK_LINE(106)
	this->marketSideBarShaftSprite->loadGraphic(HX_CSTRING("assets/images/marketSideBarShaft.png"),false,null(),null(),null(),null());
	HX_STACK_LINE(107)
	this->marketSideBarTabSprite->set_x((int)603);
	HX_STACK_LINE(108)
	this->marketSideBarShaftSprite->set_x((int)640);
	HX_STACK_LINE(109)
	int _g25 = this->seedItem->getItemCount();		HX_STACK_VAR(_g25,"_g25");
	HX_STACK_LINE(109)
	::String _g26 = ::Std_obj::string(_g25);		HX_STACK_VAR(_g26,"_g26");
	HX_STACK_LINE(109)
	::flixel::text::FlxText _g27 = ::flixel::text::FlxText_obj::__new((this->marketSideBarTabSprite->x + (int)60),(this->marketSideBarTabSprite->y + (int)2),(int)32,_g26,(int)8,null());		HX_STACK_VAR(_g27,"_g27");
	HX_STACK_LINE(109)
	this->seedItemCountFieldText = _g27;
	HX_STACK_LINE(110)
	int _g28 = this->juiceItem->getItemCount();		HX_STACK_VAR(_g28,"_g28");
	HX_STACK_LINE(110)
	::String _g29 = ::Std_obj::string(_g28);		HX_STACK_VAR(_g29,"_g29");
	HX_STACK_LINE(110)
	::flixel::text::FlxText _g30 = ::flixel::text::FlxText_obj::__new((this->marketSideBarTabSprite->x + (int)60),(this->marketSideBarTabSprite->y + (int)38),(int)32,_g29,(int)8,null());		HX_STACK_VAR(_g30,"_g30");
	HX_STACK_LINE(110)
	this->juiceItemCountFieldText = _g30;
	HX_STACK_LINE(111)
	int _g31 = this->juiceItem->getItemCount();		HX_STACK_VAR(_g31,"_g31");
	HX_STACK_LINE(111)
	::String _g32 = ::Std_obj::string(_g31);		HX_STACK_VAR(_g32,"_g32");
	HX_STACK_LINE(111)
	::flixel::text::FlxText _g33 = ::flixel::text::FlxText_obj::__new((this->marketSideBarTabSprite->x + (int)60),(this->marketSideBarTabSprite->y + (int)74),(int)32,_g32,(int)8,null());		HX_STACK_VAR(_g33,"_g33");
	HX_STACK_LINE(111)
	this->pieItemCountFieldText = _g33;
	HX_STACK_LINE(112)
	int _g34 = this->koiFishItem->getItemCount();		HX_STACK_VAR(_g34,"_g34");
	HX_STACK_LINE(112)
	::String _g35 = ::Std_obj::string(_g34);		HX_STACK_VAR(_g35,"_g35");
	HX_STACK_LINE(112)
	::flixel::text::FlxText _g36 = ::flixel::text::FlxText_obj::__new((this->marketSideBarTabSprite->x + (int)60),(this->marketSideBarTabSprite->y + (int)110),(int)32,_g35,(int)8,null());		HX_STACK_VAR(_g36,"_g36");
	HX_STACK_LINE(112)
	this->fishItemCountFieldText = _g36;
	HX_STACK_LINE(113)
	int _g37 = this->greenHouseItem->getItemCount();		HX_STACK_VAR(_g37,"_g37");
	HX_STACK_LINE(113)
	::String _g38 = ::Std_obj::string(_g37);		HX_STACK_VAR(_g38,"_g38");
	HX_STACK_LINE(113)
	::flixel::text::FlxText _g39 = ::flixel::text::FlxText_obj::__new((this->marketSideBarTabSprite->x + (int)60),(this->marketSideBarTabSprite->y + (int)146),(int)32,_g38,(int)8,null());		HX_STACK_VAR(_g39,"_g39");
	HX_STACK_LINE(113)
	this->greenHouseItemCountFieldText = _g39;
	HX_STACK_LINE(114)
	int _g40 = this->seedItem->getItemCost();		HX_STACK_VAR(_g40,"_g40");
	HX_STACK_LINE(114)
	::String _g41 = ::Std_obj::string(_g40);		HX_STACK_VAR(_g41,"_g41");
	HX_STACK_LINE(114)
	::flixel::text::FlxText _g42 = ::flixel::text::FlxText_obj::__new((this->marketSideBarTabSprite->x + (int)42),(this->marketSideBarTabSprite->y + (int)2),(int)32,_g41,(int)8,null());		HX_STACK_VAR(_g42,"_g42");
	HX_STACK_LINE(114)
	this->seedItemCostFieldText = _g42;
	HX_STACK_LINE(115)
	int _g43 = this->juiceItem->getItemCost();		HX_STACK_VAR(_g43,"_g43");
	HX_STACK_LINE(115)
	::String _g44 = ::Std_obj::string(_g43);		HX_STACK_VAR(_g44,"_g44");
	HX_STACK_LINE(115)
	::flixel::text::FlxText _g45 = ::flixel::text::FlxText_obj::__new((this->marketSideBarTabSprite->x + (int)41),(this->marketSideBarTabSprite->y + (int)38),(int)32,_g44,(int)8,null());		HX_STACK_VAR(_g45,"_g45");
	HX_STACK_LINE(115)
	this->juiceItemCostFieldText = _g45;
	HX_STACK_LINE(116)
	int _g46 = this->juiceItem->getItemCost();		HX_STACK_VAR(_g46,"_g46");
	HX_STACK_LINE(116)
	::String _g47 = ::Std_obj::string(_g46);		HX_STACK_VAR(_g47,"_g47");
	HX_STACK_LINE(116)
	::flixel::text::FlxText _g48 = ::flixel::text::FlxText_obj::__new((this->marketSideBarTabSprite->x + (int)41),(this->marketSideBarTabSprite->y + (int)74),(int)32,_g47,(int)8,null());		HX_STACK_VAR(_g48,"_g48");
	HX_STACK_LINE(116)
	this->pieItemCostFieldText = _g48;
	HX_STACK_LINE(117)
	int _g49 = this->koiFishItem->getItemCost();		HX_STACK_VAR(_g49,"_g49");
	HX_STACK_LINE(117)
	::String _g50 = ::Std_obj::string(_g49);		HX_STACK_VAR(_g50,"_g50");
	HX_STACK_LINE(117)
	::flixel::text::FlxText _g51 = ::flixel::text::FlxText_obj::__new((this->marketSideBarTabSprite->x + (int)41),(this->marketSideBarTabSprite->y + (int)110),(int)32,_g50,(int)8,null());		HX_STACK_VAR(_g51,"_g51");
	HX_STACK_LINE(117)
	this->fishItemCostFieldText = _g51;
	HX_STACK_LINE(118)
	int _g52 = this->greenHouseItem->getItemCost();		HX_STACK_VAR(_g52,"_g52");
	HX_STACK_LINE(118)
	::String _g53 = ::Std_obj::string(_g52);		HX_STACK_VAR(_g53,"_g53");
	HX_STACK_LINE(118)
	::flixel::text::FlxText _g54 = ::flixel::text::FlxText_obj::__new((this->marketSideBarTabSprite->x + (int)41),(this->marketSideBarTabSprite->y + (int)146),(int)32,_g53,(int)8,null());		HX_STACK_VAR(_g54,"_g54");
	HX_STACK_LINE(118)
	this->greenHouseItemCostFieldText = _g54;
	HX_STACK_LINE(119)
	this->seedItemCountFieldText->set_alpha(.2);
	HX_STACK_LINE(120)
	this->juiceItemCountFieldText->set_alpha(.2);
	HX_STACK_LINE(121)
	this->pieItemCountFieldText->set_alpha(.2);
	HX_STACK_LINE(122)
	this->fishItemCountFieldText->set_alpha(.2);
	HX_STACK_LINE(123)
	this->greenHouseItemCountFieldText->set_alpha(.2);
	HX_STACK_LINE(124)
	this->seedItemCountFieldText->set_color((int)0);
	HX_STACK_LINE(125)
	this->juiceItemCountFieldText->set_color((int)0);
	HX_STACK_LINE(126)
	this->pieItemCountFieldText->set_color((int)0);
	HX_STACK_LINE(127)
	this->fishItemCountFieldText->set_color((int)0);
	HX_STACK_LINE(128)
	this->greenHouseItemCountFieldText->set_color((int)0);
	HX_STACK_LINE(129)
	this->seedItemCostFieldText->set_color((int)13421649);
	HX_STACK_LINE(130)
	this->juiceItemCostFieldText->set_color((int)13421649);
	HX_STACK_LINE(131)
	this->pieItemCostFieldText->set_color((int)13421649);
	HX_STACK_LINE(132)
	this->fishItemCostFieldText->set_color((int)13421649);
	HX_STACK_LINE(133)
	this->greenHouseItemCostFieldText->set_color((int)13421649);
	HX_STACK_LINE(135)
	this->marketGroup->add(this->marketSideBarTabSprite);
	HX_STACK_LINE(136)
	this->marketGroup->add(this->marketSideBarShaftSprite);
	HX_STACK_LINE(137)
	this->marketGroup->add(this->coin);
	HX_STACK_LINE(138)
	this->fillItemArray();
	HX_STACK_LINE(139)
	this->registerEvents();
	HX_STACK_LINE(140)
	this->positionArrayItems();
	HX_STACK_LINE(141)
	this->add(this->marketGroup);
}
;
	return null();
}

//Market_obj::~Market_obj() { }

Dynamic Market_obj::__CreateEmpty() { return  new Market_obj; }
hx::ObjectPtr< Market_obj > Market_obj::__new()
{  hx::ObjectPtr< Market_obj > result = new Market_obj();
	result->__construct();
	return result;}

Dynamic Market_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Market_obj > result = new Market_obj();
	result->__construct();
	return result;}

bool Market_obj::getMarketOpen( ){
	HX_STACK_FRAME("Market","getMarketOpen",0x1a707dca,"Market.getMarketOpen","Market.hx",145,0xdfc0ea62)
	HX_STACK_THIS(this)
	HX_STACK_LINE(145)
	return this->marketOpen;
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,getMarketOpen,return )

int Market_obj::getCoinCount( ){
	HX_STACK_FRAME("Market","getCoinCount",0xeb9977da,"Market.getCoinCount","Market.hx",149,0xdfc0ea62)
	HX_STACK_THIS(this)
	HX_STACK_LINE(149)
	return this->coinCount;
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,getCoinCount,return )

int Market_obj::getSeedCount( ){
	HX_STACK_FRAME("Market","getSeedCount",0x38d01bfa,"Market.getSeedCount","Market.hx",153,0xdfc0ea62)
	HX_STACK_THIS(this)
	HX_STACK_LINE(153)
	return this->seedCount;
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,getSeedCount,return )

int Market_obj::getJuiceCount( ){
	HX_STACK_FRAME("Market","getJuiceCount",0xe383cb73,"Market.getJuiceCount","Market.hx",157,0xdfc0ea62)
	HX_STACK_THIS(this)
	HX_STACK_LINE(157)
	return this->juiceCount;
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,getJuiceCount,return )

int Market_obj::getPieCount( ){
	HX_STACK_FRAME("Market","getPieCount",0xd2ff8207,"Market.getPieCount","Market.hx",161,0xdfc0ea62)
	HX_STACK_THIS(this)
	HX_STACK_LINE(161)
	return this->pieCount;
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,getPieCount,return )

int Market_obj::getFishCount( ){
	HX_STACK_FRAME("Market","getFishCount",0x46f17f73,"Market.getFishCount","Market.hx",165,0xdfc0ea62)
	HX_STACK_THIS(this)
	HX_STACK_LINE(165)
	return this->fishCount;
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,getFishCount,return )

int Market_obj::getGreenHouseCount( ){
	HX_STACK_FRAME("Market","getGreenHouseCount",0x4bd0990e,"Market.getGreenHouseCount","Market.hx",169,0xdfc0ea62)
	HX_STACK_THIS(this)
	HX_STACK_LINE(169)
	return this->greenHouseCount;
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,getGreenHouseCount,return )

int Market_obj::getJuicePackCount( ){
	HX_STACK_FRAME("Market","getJuicePackCount",0x78db13da,"Market.getJuicePackCount","Market.hx",173,0xdfc0ea62)
	HX_STACK_THIS(this)
	HX_STACK_LINE(173)
	return this->juicePack;
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,getJuicePackCount,return )

int Market_obj::getPiePackCount( ){
	HX_STACK_FRAME("Market","getPiePackCount",0x66b7a46e,"Market.getPiePackCount","Market.hx",177,0xdfc0ea62)
	HX_STACK_THIS(this)
	HX_STACK_LINE(177)
	return this->piePack;
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,getPiePackCount,return )

Void Market_obj::stockJuicePacks( int val){
{
		HX_STACK_FRAME("Market","stockJuicePacks",0x5c3a5bde,"Market.stockJuicePacks","Market.hx",181,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(181)
		hx::AddEq(this->juicePack,val);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,stockJuicePacks,(void))

Void Market_obj::stockPiePacks( int val){
{
		HX_STACK_FRAME("Market","stockPiePacks",0xdebb2272,"Market.stockPiePacks","Market.hx",185,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(185)
		hx::AddEq(this->piePack,val);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,stockPiePacks,(void))

Void Market_obj::loadJuicePacksDec( ){
{
		HX_STACK_FRAME("Market","loadJuicePacksDec",0x6ea818d0,"Market.loadJuicePacksDec","Market.hx",189,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(189)
		hx::SubEq(this->juicePack,(int)10);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,loadJuicePacksDec,(void))

Void Market_obj::loadPiePacksDec( ){
{
		HX_STACK_FRAME("Market","loadPiePacksDec",0x5178bbbc,"Market.loadPiePacksDec","Market.hx",193,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(193)
		hx::SubEq(this->piePack,(int)10);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,loadPiePacksDec,(void))

Void Market_obj::setCountTextXClose( int val){
{
		HX_STACK_FRAME("Market","setCountTextXClose",0xa41afc2c,"Market.setCountTextXClose","Market.hx",196,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(197)
		{
			HX_STACK_LINE(197)
			::flixel::text::FlxText _g = this->seedCountText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(197)
			_g->set_x((_g->x - val));
		}
		HX_STACK_LINE(198)
		{
			HX_STACK_LINE(198)
			::flixel::text::FlxText _g = this->juiceCountText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(198)
			_g->set_x((_g->x - val));
		}
		HX_STACK_LINE(199)
		{
			HX_STACK_LINE(199)
			::flixel::text::FlxText _g = this->pieCountText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(199)
			_g->set_x((_g->x - val));
		}
		HX_STACK_LINE(200)
		{
			HX_STACK_LINE(200)
			::flixel::text::FlxText _g = this->fishCountText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(200)
			_g->set_x((_g->x - val));
		}
		HX_STACK_LINE(201)
		{
			HX_STACK_LINE(201)
			::flixel::text::FlxText _g = this->greenHouseCountText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(201)
			_g->set_x((_g->x - val));
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setCountTextXClose,(void))

Void Market_obj::setCountTextXOpen( int val){
{
		HX_STACK_FRAME("Market","setCountTextXOpen",0xbf3569d6,"Market.setCountTextXOpen","Market.hx",204,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(205)
		{
			HX_STACK_LINE(205)
			::flixel::text::FlxText _g = this->seedCountText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(205)
			_g->set_x((_g->x + val));
		}
		HX_STACK_LINE(206)
		{
			HX_STACK_LINE(206)
			::flixel::text::FlxText _g = this->juiceCountText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(206)
			_g->set_x((_g->x + val));
		}
		HX_STACK_LINE(207)
		{
			HX_STACK_LINE(207)
			::flixel::text::FlxText _g = this->pieCountText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(207)
			_g->set_x((_g->x + val));
		}
		HX_STACK_LINE(208)
		{
			HX_STACK_LINE(208)
			::flixel::text::FlxText _g = this->fishCountText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(208)
			_g->set_x((_g->x + val));
		}
		HX_STACK_LINE(209)
		{
			HX_STACK_LINE(209)
			::flixel::text::FlxText _g = this->greenHouseCountText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(209)
			_g->set_x((_g->x + val));
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setCountTextXOpen,(void))

Void Market_obj::setJuicePackCount( int val){
{
		HX_STACK_FRAME("Market","setJuicePackCount",0x9c48ebe6,"Market.setJuicePackCount","Market.hx",213,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(213)
		this->juicePack = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setJuicePackCount,(void))

Void Market_obj::setPiePackCount( int val){
{
		HX_STACK_FRAME("Market","setPiePackCount",0x6283217a,"Market.setPiePackCount","Market.hx",217,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(217)
		this->piePack = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setPiePackCount,(void))

Void Market_obj::setSeedCount( int val){
{
		HX_STACK_FRAME("Market","setSeedCount",0x4dc93f6e,"Market.setSeedCount","Market.hx",221,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(221)
		this->seedCount = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setSeedCount,(void))

Void Market_obj::setseedItemCount( int val){
{
		HX_STACK_FRAME("Market","setseedItemCount",0xd5ff90fb,"Market.setseedItemCount","Market.hx",225,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(225)
		this->seedItem->setSeedItemCount(val);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setseedItemCount,(void))

Void Market_obj::setKoiPondItemCount( int val){
{
		HX_STACK_FRAME("Market","setKoiPondItemCount",0x7fb94ab2,"Market.setKoiPondItemCount","Market.hx",229,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(229)
		this->koiFishItem->setFishItemCount(val);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setKoiPondItemCount,(void))

Void Market_obj::setGreenHouseItemCount( int val){
{
		HX_STACK_FRAME("Market","setGreenHouseItemCount",0x28e006ef,"Market.setGreenHouseItemCount","Market.hx",233,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(233)
		this->greenHouseItem->setGreenHouseItemCount(val);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setGreenHouseItemCount,(void))

Void Market_obj::setPieCount( int val){
{
		HX_STACK_FRAME("Market","setPieCount",0xdd6c8913,"Market.setPieCount","Market.hx",237,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(237)
		this->pieCount = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setPieCount,(void))

Void Market_obj::setFishCount( int val){
{
		HX_STACK_FRAME("Market","setFishCount",0x5beaa2e7,"Market.setFishCount","Market.hx",241,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(241)
		this->fishCount = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setFishCount,(void))

Void Market_obj::setGreenHouseCount( int val){
{
		HX_STACK_FRAME("Market","setGreenHouseCount",0x287fcb82,"Market.setGreenHouseCount","Market.hx",245,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(245)
		this->greenHouseCount = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setGreenHouseCount,(void))

Void Market_obj::setPieItemCount( int val){
{
		HX_STACK_FRAME("Market","setPieItemCount",0xfc896100,"Market.setPieItemCount","Market.hx",249,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(249)
		this->pieItem->setPieItemCount(val);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setPieItemCount,(void))

Void Market_obj::setJuiceCount( int val){
{
		HX_STACK_FRAME("Market","setJuiceCount",0x2889ad7f,"Market.setJuiceCount","Market.hx",253,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(253)
		this->juiceCount = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setJuiceCount,(void))

Void Market_obj::setJuiceItemCount( int val){
{
		HX_STACK_FRAME("Market","setJuiceItemCount",0x364f2b6c,"Market.setJuiceItemCount","Market.hx",257,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(257)
		this->juiceItem->setJuiceItemCount(val);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setJuiceItemCount,(void))

Void Market_obj::setCoinCount( int val){
{
		HX_STACK_FRAME("Market","setCoinCount",0x00929b4e,"Market.setCoinCount","Market.hx",261,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(261)
		this->coinCount = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setCoinCount,(void))

Void Market_obj::setWaterClientCount( int val){
{
		HX_STACK_FRAME("Market","setWaterClientCount",0x2fa483dd,"Market.setWaterClientCount","Market.hx",265,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(265)
		this->waterClientCount = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setWaterClientCount,(void))

Void Market_obj::setElectricClientCount( int val){
{
		HX_STACK_FRAME("Market","setElectricClientCount",0xadb90777,"Market.setElectricClientCount","Market.hx",269,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(269)
		this->electricClientCount = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,setElectricClientCount,(void))

Void Market_obj::addSubtractCoins( ::String operation,int perTen){
{
		HX_STACK_FRAME("Market","addSubtractCoins",0xfdf613ff,"Market.addSubtractCoins","Market.hx",273,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(operation,"operation")
		HX_STACK_ARG(perTen,"perTen")
		HX_STACK_LINE(273)
		::String _switch_1 = (operation);
		if (  ( _switch_1==HX_CSTRING("+"))){
			HX_STACK_LINE(275)
			hx::AddEq(this->coinCount,perTen);
		}
		else if (  ( _switch_1==HX_CSTRING("-"))){
			HX_STACK_LINE(277)
			hx::SubEq(this->coinCount,perTen);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(Market_obj,addSubtractCoins,(void))

Void Market_obj::displayCoinCount( ){
{
		HX_STACK_FRAME("Market","displayCoinCount",0x761789ae,"Market.displayCoinCount","Market.hx",281,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(282)
		::String _g = ::Std_obj::string(this->coinCount);		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(282)
		this->coinCountFieldText->set_text(_g);
		HX_STACK_LINE(283)
		this->add(this->coinCountFieldText);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,displayCoinCount,(void))

Void Market_obj::displaySeedCount( ){
{
		HX_STACK_FRAME("Market","displaySeedCount",0xc34e2dce,"Market.displaySeedCount","Market.hx",286,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(288)
		::String _g = ::Std_obj::string(this->seedCount);		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(288)
		this->seedCountText->set_text(_g);
		HX_STACK_LINE(289)
		::String _g1 = ::Std_obj::string(this->juiceCount);		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(289)
		this->juiceCountText->set_text(_g1);
		HX_STACK_LINE(290)
		::String _g2 = ::Std_obj::string(this->pieCount);		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(290)
		this->pieCountText->set_text(_g2);
		HX_STACK_LINE(291)
		::String _g3 = ::Std_obj::string(this->fishCount);		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(291)
		this->fishCountText->set_text(_g3);
		HX_STACK_LINE(292)
		::String _g4 = ::Std_obj::string(this->greenHouseCount);		HX_STACK_VAR(_g4,"_g4");
		HX_STACK_LINE(292)
		this->greenHouseCountText->set_text(_g4);
		HX_STACK_LINE(294)
		this->add(this->seedCountText);
		HX_STACK_LINE(295)
		this->add(this->juiceCountText);
		HX_STACK_LINE(296)
		this->add(this->pieCountText);
		HX_STACK_LINE(297)
		this->add(this->fishCountText);
		HX_STACK_LINE(298)
		this->add(this->greenHouseCountText);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,displaySeedCount,(void))

Void Market_obj::displayItemCountText( ){
{
		HX_STACK_FRAME("Market","displayItemCountText",0x4217e1b9,"Market.displayItemCountText","Market.hx",301,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(302)
		int _g = this->seedItem->getItemCount();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(302)
		::String _g1 = ::Std_obj::string(_g);		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(302)
		this->seedItemCountFieldText->set_text(_g1);
		HX_STACK_LINE(303)
		int _g2 = this->juiceItem->getItemCount();		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(303)
		::String _g3 = ::Std_obj::string(_g2);		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(303)
		this->juiceItemCountFieldText->set_text(_g3);
		HX_STACK_LINE(304)
		int _g4 = this->koiFishItem->getItemCount();		HX_STACK_VAR(_g4,"_g4");
		HX_STACK_LINE(304)
		::String _g5 = ::Std_obj::string(_g4);		HX_STACK_VAR(_g5,"_g5");
		HX_STACK_LINE(304)
		this->fishItemCountFieldText->set_text(_g5);
		HX_STACK_LINE(305)
		int _g6 = this->seedItem->getItemCost();		HX_STACK_VAR(_g6,"_g6");
		HX_STACK_LINE(305)
		::String _g7 = ::Std_obj::string(_g6);		HX_STACK_VAR(_g7,"_g7");
		HX_STACK_LINE(305)
		this->seedItemCostFieldText->set_text(_g7);
		HX_STACK_LINE(306)
		int _g8 = this->juiceItem->getItemCost();		HX_STACK_VAR(_g8,"_g8");
		HX_STACK_LINE(306)
		::String _g9 = ::Std_obj::string(_g8);		HX_STACK_VAR(_g9,"_g9");
		HX_STACK_LINE(306)
		this->juiceItemCostFieldText->set_text(_g9);
		HX_STACK_LINE(307)
		int _g10 = this->pieItem->getItemCount();		HX_STACK_VAR(_g10,"_g10");
		HX_STACK_LINE(307)
		::String _g11 = ::Std_obj::string(_g10);		HX_STACK_VAR(_g11,"_g11");
		HX_STACK_LINE(307)
		this->pieItemCountFieldText->set_text(_g11);
		HX_STACK_LINE(308)
		int _g12 = this->pieItem->getItemCost();		HX_STACK_VAR(_g12,"_g12");
		HX_STACK_LINE(308)
		::String _g13 = ::Std_obj::string(_g12);		HX_STACK_VAR(_g13,"_g13");
		HX_STACK_LINE(308)
		this->pieItemCostFieldText->set_text(_g13);
		HX_STACK_LINE(309)
		int _g14 = this->koiFishItem->getItemCost();		HX_STACK_VAR(_g14,"_g14");
		HX_STACK_LINE(309)
		::String _g15 = ::Std_obj::string(_g14);		HX_STACK_VAR(_g15,"_g15");
		HX_STACK_LINE(309)
		this->fishItemCostFieldText->set_text(_g15);
		HX_STACK_LINE(310)
		::String _g16 = ::Std_obj::string(this->juicePack);		HX_STACK_VAR(_g16,"_g16");
		HX_STACK_LINE(310)
		this->juicePackCountText->set_text(_g16);
		HX_STACK_LINE(311)
		::String _g17 = ::Std_obj::string(this->piePack);		HX_STACK_VAR(_g17,"_g17");
		HX_STACK_LINE(311)
		this->piePackCountText->set_text(_g17);
		HX_STACK_LINE(312)
		int _g18 = this->greenHouseItem->getItemCost();		HX_STACK_VAR(_g18,"_g18");
		HX_STACK_LINE(312)
		::String _g19 = ::Std_obj::string(_g18);		HX_STACK_VAR(_g19,"_g19");
		HX_STACK_LINE(312)
		this->greenHouseItemCostFieldText->set_text(_g19);
		HX_STACK_LINE(313)
		int _g20 = this->greenHouseItem->getItemCount();		HX_STACK_VAR(_g20,"_g20");
		HX_STACK_LINE(313)
		::String _g21 = ::Std_obj::string(_g20);		HX_STACK_VAR(_g21,"_g21");
		HX_STACK_LINE(313)
		this->greenHouseItemCountFieldText->set_text(_g21);
		HX_STACK_LINE(314)
		this->marketGroup->add(this->juicePackCountText);
		HX_STACK_LINE(315)
		this->marketGroup->add(this->seedItemCostFieldText);
		HX_STACK_LINE(316)
		this->marketGroup->add(this->juiceItemCostFieldText);
		HX_STACK_LINE(317)
		this->marketGroup->add(this->juiceItemCountFieldText);
		HX_STACK_LINE(318)
		this->marketGroup->add(this->seedItemCountFieldText);
		HX_STACK_LINE(319)
		this->marketGroup->add(this->greenHouseItemCountFieldText);
		HX_STACK_LINE(320)
		this->marketGroup->add(this->pieItemCostFieldText);
		HX_STACK_LINE(321)
		this->marketGroup->add(this->pieItemCountFieldText);
		HX_STACK_LINE(322)
		this->marketGroup->add(this->fishItemCountFieldText);
		HX_STACK_LINE(323)
		this->marketGroup->add(this->fishItemCostFieldText);
		HX_STACK_LINE(324)
		this->marketGroup->add(this->greenHouseItemCostFieldText);
		HX_STACK_LINE(325)
		this->marketGroup->add(this->piePackCountText);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,displayItemCountText,(void))

Void Market_obj::decSeedCount( ){
{
		HX_STACK_FRAME("Market","decSeedCount",0xae9fdd4e,"Market.decSeedCount","Market.hx",329,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(329)
		(this->seedCount)--;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,decSeedCount,(void))

Void Market_obj::incSeedCount( ){
{
		HX_STACK_FRAME("Market","incSeedCount",0x7f2ec6b2,"Market.incSeedCount","Market.hx",333,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(333)
		(this->seedCount)++;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,incSeedCount,(void))

Void Market_obj::decjuiceCount( int val){
{
		HX_STACK_FRAME("Market","decjuiceCount",0x8ccc2f7f,"Market.decjuiceCount","Market.hx",337,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(337)
		hx::SubEq(this->juiceCount,val);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,decjuiceCount,(void))

Void Market_obj::decjuicePackCount( int val){
{
		HX_STACK_FRAME("Market","decjuicePackCount",0x60f46de6,"Market.decjuicePackCount","Market.hx",341,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(341)
		hx::SubEq(this->juicePack,val);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,decjuicePackCount,(void))

Void Market_obj::incJuiceCount( ){
{
		HX_STACK_FRAME("Market","incJuiceCount",0x2ffa81bb,"Market.incJuiceCount","Market.hx",345,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(345)
		(this->juiceCount)++;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,incJuiceCount,(void))

Void Market_obj::decPieCount( ){
{
		HX_STACK_FRAME("Market","decPieCount",0x4e5c4733,"Market.decPieCount","Market.hx",349,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(349)
		(this->pieCount)--;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,decPieCount,(void))

Void Market_obj::decFishCount( ){
{
		HX_STACK_FRAME("Market","decFishCount",0xbcc140c7,"Market.decFishCount","Market.hx",353,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(353)
		(this->fishCount)--;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,decFishCount,(void))

Void Market_obj::decGreenHouseCount( ){
{
		HX_STACK_FRAME("Market","decGreenHouseCount",0xda721162,"Market.decGreenHouseCount","Market.hx",357,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(357)
		(this->greenHouseCount)--;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,decGreenHouseCount,(void))

Void Market_obj::incPieCount( ){
{
		HX_STACK_FRAME("Market","incPieCount",0x9552964f,"Market.incPieCount","Market.hx",361,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(361)
		(this->pieCount)++;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,incPieCount,(void))

Void Market_obj::incfishCount( ){
{
		HX_STACK_FRAME("Market","incfishCount",0xeb7d4a4b,"Market.incfishCount","Market.hx",365,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(365)
		(this->fishCount)++;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,incfishCount,(void))

Void Market_obj::incGreenHouseCount( ){
{
		HX_STACK_FRAME("Market","incGreenHouseCount",0xd0f81dc6,"Market.incGreenHouseCount","Market.hx",369,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(369)
		(this->greenHouseCount)++;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,incGreenHouseCount,(void))

Void Market_obj::coinPaymentUtil( int val){
{
		HX_STACK_FRAME("Market","coinPaymentUtil",0xee8bc985,"Market.coinPaymentUtil","Market.hx",373,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(373)
		hx::SubEq(this->coinCount,val);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,coinPaymentUtil,(void))

Void Market_obj::positionArrayItems( ){
{
		HX_STACK_FRAME("Market","positionArrayItems",0x546a0902,"Market.positionArrayItems","Market.hx",377,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(377)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(377)
		int _g = this->itemArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(377)
		while((true)){
			HX_STACK_LINE(377)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(377)
				break;
			}
			HX_STACK_LINE(377)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(378)
			this->itemArray->__get(i).StaticCast< ::MarketItem >()->getItemSprite()->set_x((this->marketSideBarTabSprite->x + this->shiftX));
			HX_STACK_LINE(379)
			this->itemArray->__get(i).StaticCast< ::MarketItem >()->getItemSprite()->set_y((this->marketSideBarTabSprite->y + this->shiftY));
			HX_STACK_LINE(380)
			::flixel::FlxSprite _g2 = this->itemArray->__get(i).StaticCast< ::MarketItem >()->getItemSprite();		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(380)
			this->marketGroup->add(_g2);
			HX_STACK_LINE(381)
			hx::AddEq(this->shiftY,(int)36);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,positionArrayItems,(void))

Void Market_obj::fillItemArray( ){
{
		HX_STACK_FRAME("Market","fillItemArray",0xbc906a71,"Market.fillItemArray","Market.hx",385,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(386)
		this->itemArray[(int)0] = this->seedItem;
		HX_STACK_LINE(387)
		this->itemArray[(int)1] = this->juiceItem;
		HX_STACK_LINE(388)
		this->itemArray[(int)2] = this->pieItem;
		HX_STACK_LINE(389)
		this->itemArray[(int)3] = this->koiFishItem;
		HX_STACK_LINE(390)
		this->itemArray[(int)4] = this->greenHouseItem;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,fillItemArray,(void))

Void Market_obj::registerEvents( ){
{
		HX_STACK_FRAME("Market","registerEvents",0xba2f182e,"Market.registerEvents","Market.hx",393,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(394)
		::flixel::plugin::MouseEventManager_obj::add(this->marketSideBarTabSprite,this->openMarket_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(395)
		{
			HX_STACK_LINE(395)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(395)
			int _g = this->itemArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(395)
			while((true)){
				HX_STACK_LINE(395)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(395)
					break;
				}
				HX_STACK_LINE(395)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(396)
				::flixel::FlxSprite _g2 = this->itemArray->__get(i).StaticCast< ::MarketItem >()->getItemSprite();		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(396)
				::flixel::plugin::MouseEventManager_obj::add(_g2,this->selectItem_dyn(),null(),null(),null(),null(),null(),null());
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,registerEvents,(void))

Void Market_obj::registerItemEvents( ){
{
		HX_STACK_FRAME("Market","registerItemEvents",0x7c3f9021,"Market.registerItemEvents","Market.hx",401,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(401)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(401)
		int _g = this->itemArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(401)
		while((true)){
			HX_STACK_LINE(401)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(401)
				break;
			}
			HX_STACK_LINE(401)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(402)
			::flixel::FlxSprite _g2 = this->itemArray->__get(i).StaticCast< ::MarketItem >()->getItemSprite();		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(402)
			::flixel::plugin::MouseEventManager_obj::add(_g2,this->selectItem_dyn(),null(),null(),null(),null(),null(),null());
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,registerItemEvents,(void))

Void Market_obj::unregisterItemEvents( ){
{
		HX_STACK_FRAME("Market","unregisterItemEvents",0xabf6793a,"Market.unregisterItemEvents","Market.hx",407,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(407)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(407)
		int _g = this->itemArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(407)
		while((true)){
			HX_STACK_LINE(407)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(407)
				break;
			}
			HX_STACK_LINE(407)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(408)
			::flixel::FlxSprite _g2 = this->itemArray->__get(i).StaticCast< ::MarketItem >()->getItemSprite();		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(408)
			::flixel::plugin::MouseEventManager_obj::remove(_g2);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,unregisterItemEvents,(void))

Void Market_obj::unregisterEvents( ){
{
		HX_STACK_FRAME("Market","unregisterEvents",0xa06340c7,"Market.unregisterEvents","Market.hx",412,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(413)
		::flixel::plugin::MouseEventManager_obj::remove(this->marketSideBarTabSprite);
		HX_STACK_LINE(414)
		::flixel::plugin::MouseEventManager_obj::remove(this->marketSideBarShaftSprite);
		HX_STACK_LINE(415)
		{
			HX_STACK_LINE(415)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(415)
			int _g = this->itemArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(415)
			while((true)){
				HX_STACK_LINE(415)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(415)
					break;
				}
				HX_STACK_LINE(415)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(416)
				::flixel::FlxSprite _g2 = this->itemArray->__get(i).StaticCast< ::MarketItem >()->getItemSprite();		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(416)
				::flixel::plugin::MouseEventManager_obj::remove(_g2);
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,unregisterEvents,(void))

Void Market_obj::selectItem( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("Market","selectItem",0xbf166b81,"Market.selectItem","Market.hx",421,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(421)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(421)
		int _g = this->itemArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(421)
		while((true)){
			HX_STACK_LINE(421)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(421)
				break;
			}
			HX_STACK_LINE(421)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(422)
			::flixel::FlxSprite _g2 = this->itemArray->__get(i).StaticCast< ::MarketItem >()->getItemSprite();		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(422)
			if (((_g2 == sprite))){
				HX_STACK_LINE(423)
				this->marketCloseTimer = (int)10;
				HX_STACK_LINE(424)
				int _g11 = this->itemArray->__get(i).StaticCast< ::MarketItem >()->getItemCost();		HX_STACK_VAR(_g11,"_g11");
				struct _Function_3_1{
					inline static bool Block( int &i,hx::ObjectPtr< ::Market_obj > __this){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","Market.hx",424,0xdfc0ea62)
						{
							HX_STACK_LINE(424)
							int _g21 = __this->itemArray->__get(i).StaticCast< ::MarketItem >()->getItemCount();		HX_STACK_VAR(_g21,"_g21");
							HX_STACK_LINE(424)
							return (_g21 > (int)0);
						}
						return null();
					}
				};
				HX_STACK_LINE(424)
				if (((  (((this->coinCount >= _g11))) ? bool(_Function_3_1::Block(i,this)) : bool(false) ))){
					HX_STACK_LINE(425)
					::flixel::system::FlxSound _g3 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/cashRegisterSound.wav"),.3,null(),null(),null(),null(),null());		HX_STACK_VAR(_g3,"_g3");
					HX_STACK_LINE(425)
					this->purchaseItemSound = _g3;
					HX_STACK_LINE(426)
					this->purchaseItemSound->play(null());
					HX_STACK_LINE(427)
					this->itemArray->__get(i).StaticCast< ::MarketItem >()->decItemCount();
					HX_STACK_LINE(428)
					int _g4 = this->itemArray->__get(i).StaticCast< ::MarketItem >()->getItemCost();		HX_STACK_VAR(_g4,"_g4");
					HX_STACK_LINE(428)
					hx::SubEq(this->coinCount,_g4);
					HX_STACK_LINE(429)
					this->purchaseItem(this->itemArray->__get(i).StaticCast< ::MarketItem >());
					HX_STACK_LINE(430)
					int _g5 = this->itemArray->__get(i).StaticCast< ::MarketItem >()->getItemCount();		HX_STACK_VAR(_g5,"_g5");
					HX_STACK_LINE(430)
					if (((_g5 <= (int)0))){
						HX_STACK_LINE(431)
						this->itemArray->__get(i).StaticCast< ::MarketItem >()->setItemAvailable(false);
						HX_STACK_LINE(432)
						this->itemArray->__get(i).StaticCast< ::MarketItem >()->setAvailableAnimation();
					}
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,selectItem,(void))

Void Market_obj::purchaseItem( ::MarketItem item){
{
		HX_STACK_FRAME("Market","purchaseItem",0xc4100346,"Market.purchaseItem","Market.hx",440,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(item,"item")
		HX_STACK_LINE(440)
		::String _g = item->getItemName();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(440)
		::String _switch_2 = (_g);
		if (  ( _switch_2==HX_CSTRING("seedItem"))){
			HX_STACK_LINE(442)
			hx::AddEq(this->seedCount,(int)1);
		}
		else if (  ( _switch_2==HX_CSTRING("juiceItem"))){
			HX_STACK_LINE(444)
			hx::AddEq(this->juiceCount,(int)1);
		}
		else if (  ( _switch_2==HX_CSTRING("pieItem"))){
			HX_STACK_LINE(446)
			hx::AddEq(this->pieCount,(int)1);
		}
		else if (  ( _switch_2==HX_CSTRING("koiItem"))){
			HX_STACK_LINE(448)
			hx::AddEq(this->fishCount,(int)1);
		}
		else if (  ( _switch_2==HX_CSTRING("greenHouseItem"))){
			HX_STACK_LINE(450)
			hx::AddEq(this->greenHouseCount,(int)1);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,purchaseItem,(void))

Void Market_obj::openMarket( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("Market","openMarket",0x12c81278,"Market.openMarket","Market.hx",455,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(455)
		if ((this->turn)){
			HX_STACK_LINE(456)
			::flixel::system::FlxSound _g = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/marketSlideOpenSound.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(456)
			this->marketOpenSound = _g;
			HX_STACK_LINE(457)
			this->marketOpenSound->play(null());
			HX_STACK_LINE(458)
			this->marketOpen = true;
			HX_STACK_LINE(459)
			this->turn = false;
			HX_STACK_LINE(460)
			this->closedSoundPlayed = false;
		}
		else{
			HX_STACK_LINE(463)
			::flixel::system::FlxSound _g1 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/marketSlideClosedSound.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(463)
			this->marketClosedSound = _g1;
			HX_STACK_LINE(464)
			this->marketClosedSound->play(null());
			HX_STACK_LINE(465)
			this->marketOpen = false;
			HX_STACK_LINE(466)
			this->turn = true;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,openMarket,(void))

Void Market_obj::slideMarketOut( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("Market","slideMarketOut",0x4a35cf13,"Market.slideMarketOut","Market.hx",471,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(471)
		if ((this->marketOpen)){
			HX_STACK_LINE(472)
			this->registerItemEvents();
			HX_STACK_LINE(473)
			hx::SubEq(this->marketCloseTimer,::flixel::FlxG_obj::elapsed);
			HX_STACK_LINE(474)
			if (((this->marketSideBarTabSprite->x >= (int)565))){
				HX_STACK_LINE(475)
				{
					HX_STACK_LINE(475)
					::flixel::FlxSprite _g = this->marketSideBarTabSprite;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(475)
					_g->set_x((_g->x - (int)2));
				}
				HX_STACK_LINE(476)
				{
					HX_STACK_LINE(476)
					::flixel::FlxSprite _g = this->marketSideBarShaftSprite;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(476)
					_g->set_x((_g->x - (int)2));
				}
				HX_STACK_LINE(477)
				this->shiftItemsOpen();
			}
		}
		else{
			HX_STACK_LINE(481)
			this->unregisterItemEvents();
			HX_STACK_LINE(482)
			if (((this->marketSideBarTabSprite->x <= (int)602))){
				HX_STACK_LINE(483)
				if ((!(this->closedSoundPlayed))){
					HX_STACK_LINE(484)
					::flixel::system::FlxSound _g = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/marketSlideClosedSound.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(484)
					this->marketClosedSound = _g;
					HX_STACK_LINE(485)
					this->marketClosedSound->play(null());
					HX_STACK_LINE(486)
					this->closedSoundPlayed = true;
				}
				HX_STACK_LINE(488)
				this->marketCloseTimer = (int)10;
				HX_STACK_LINE(489)
				{
					HX_STACK_LINE(489)
					::flixel::FlxSprite _g = this->marketSideBarTabSprite;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(489)
					_g->set_x((_g->x + (int)2));
				}
				HX_STACK_LINE(490)
				{
					HX_STACK_LINE(490)
					::flixel::FlxSprite _g = this->marketSideBarShaftSprite;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(490)
					_g->set_x((_g->x + (int)2));
				}
				HX_STACK_LINE(491)
				this->shiftItemsClose();
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Market_obj,slideMarketOut,(void))

Void Market_obj::timedMarketClose( ){
{
		HX_STACK_FRAME("Market","timedMarketClose",0xc6c458b7,"Market.timedMarketClose","Market.hx",497,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(497)
		if (((this->marketCloseTimer < (int)0))){
			HX_STACK_LINE(498)
			this->marketCloseTimer = (int)10;
			HX_STACK_LINE(499)
			this->marketOpen = false;
			HX_STACK_LINE(500)
			this->turn = true;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,timedMarketClose,(void))

Void Market_obj::shiftItemsOpen( ){
{
		HX_STACK_FRAME("Market","shiftItemsOpen",0x13a6715a,"Market.shiftItemsOpen","Market.hx",504,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(505)
		{
			HX_STACK_LINE(505)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(505)
			int _g = this->itemArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(505)
			while((true)){
				HX_STACK_LINE(505)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(505)
					break;
				}
				HX_STACK_LINE(505)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(506)
				{
					HX_STACK_LINE(506)
					::flixel::FlxSprite _g2 = this->itemArray->__get(i).StaticCast< ::MarketItem >()->getItemSprite();		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(506)
					_g2->set_x((_g2->x - (int)2));
				}
			}
		}
		HX_STACK_LINE(508)
		{
			HX_STACK_LINE(508)
			::flixel::text::FlxText _g = this->seedItemCountFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(508)
			_g->set_x((_g->x - (int)2));
		}
		HX_STACK_LINE(509)
		{
			HX_STACK_LINE(509)
			::flixel::text::FlxText _g = this->juiceItemCountFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(509)
			_g->set_x((_g->x - (int)2));
		}
		HX_STACK_LINE(510)
		{
			HX_STACK_LINE(510)
			::flixel::text::FlxText _g = this->seedItemCostFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(510)
			_g->set_x((_g->x - (int)2));
		}
		HX_STACK_LINE(511)
		{
			HX_STACK_LINE(511)
			::flixel::text::FlxText _g = this->juiceItemCostFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(511)
			_g->set_x((_g->x - (int)2));
		}
		HX_STACK_LINE(512)
		{
			HX_STACK_LINE(512)
			::flixel::text::FlxText _g = this->pieItemCountFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(512)
			_g->set_x((_g->x - (int)2));
		}
		HX_STACK_LINE(513)
		{
			HX_STACK_LINE(513)
			::flixel::text::FlxText _g = this->pieItemCostFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(513)
			_g->set_x((_g->x - (int)2));
		}
		HX_STACK_LINE(514)
		{
			HX_STACK_LINE(514)
			::flixel::text::FlxText _g = this->fishItemCostFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(514)
			_g->set_x((_g->x - (int)2));
		}
		HX_STACK_LINE(515)
		{
			HX_STACK_LINE(515)
			::flixel::text::FlxText _g = this->fishItemCountFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(515)
			_g->set_x((_g->x - (int)2));
		}
		HX_STACK_LINE(516)
		{
			HX_STACK_LINE(516)
			::flixel::text::FlxText _g = this->greenHouseItemCostFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(516)
			_g->set_x((_g->x - (int)2));
		}
		HX_STACK_LINE(517)
		{
			HX_STACK_LINE(517)
			::flixel::text::FlxText _g = this->greenHouseItemCountFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(517)
			_g->set_x((_g->x - (int)2));
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,shiftItemsOpen,(void))

Void Market_obj::shiftItemsClose( ){
{
		HX_STACK_FRAME("Market","shiftItemsClose",0x32908828,"Market.shiftItemsClose","Market.hx",520,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(521)
		{
			HX_STACK_LINE(521)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(521)
			int _g = this->itemArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(521)
			while((true)){
				HX_STACK_LINE(521)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(521)
					break;
				}
				HX_STACK_LINE(521)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(522)
				{
					HX_STACK_LINE(522)
					::flixel::FlxSprite _g2 = this->itemArray->__get(i).StaticCast< ::MarketItem >()->getItemSprite();		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(522)
					_g2->set_x((_g2->x + (int)2));
				}
			}
		}
		HX_STACK_LINE(524)
		{
			HX_STACK_LINE(524)
			::flixel::text::FlxText _g = this->seedItemCountFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(524)
			_g->set_x((_g->x + (int)2));
		}
		HX_STACK_LINE(525)
		{
			HX_STACK_LINE(525)
			::flixel::text::FlxText _g = this->juiceItemCountFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(525)
			_g->set_x((_g->x + (int)2));
		}
		HX_STACK_LINE(526)
		{
			HX_STACK_LINE(526)
			::flixel::text::FlxText _g = this->seedItemCostFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(526)
			_g->set_x((_g->x + (int)2));
		}
		HX_STACK_LINE(527)
		{
			HX_STACK_LINE(527)
			::flixel::text::FlxText _g = this->juiceItemCostFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(527)
			_g->set_x((_g->x + (int)2));
		}
		HX_STACK_LINE(528)
		{
			HX_STACK_LINE(528)
			::flixel::text::FlxText _g = this->pieItemCountFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(528)
			_g->set_x((_g->x + (int)2));
		}
		HX_STACK_LINE(529)
		{
			HX_STACK_LINE(529)
			::flixel::text::FlxText _g = this->pieItemCostFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(529)
			_g->set_x((_g->x + (int)2));
		}
		HX_STACK_LINE(530)
		{
			HX_STACK_LINE(530)
			::flixel::text::FlxText _g = this->fishItemCostFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(530)
			_g->set_x((_g->x + (int)2));
		}
		HX_STACK_LINE(531)
		{
			HX_STACK_LINE(531)
			::flixel::text::FlxText _g = this->fishItemCountFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(531)
			_g->set_x((_g->x + (int)2));
		}
		HX_STACK_LINE(532)
		{
			HX_STACK_LINE(532)
			::flixel::text::FlxText _g = this->greenHouseItemCostFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(532)
			_g->set_x((_g->x + (int)2));
		}
		HX_STACK_LINE(533)
		{
			HX_STACK_LINE(533)
			::flixel::text::FlxText _g = this->greenHouseItemCountFieldText;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(533)
			_g->set_x((_g->x + (int)2));
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,shiftItemsClose,(void))

Void Market_obj::replenishItems( ){
{
		HX_STACK_FRAME("Market","replenishItems",0x44824ecc,"Market.replenishItems","Market.hx",536,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(537)
		hx::SubEq(this->replenishSeedTimer,::flixel::FlxG_obj::elapsed);
		HX_STACK_LINE(538)
		hx::SubEq(this->replenishJuiceMachineTimer,::flixel::FlxG_obj::elapsed);
		HX_STACK_LINE(539)
		hx::SubEq(this->replenishPieMachineSeedTimer,::flixel::FlxG_obj::elapsed);
		HX_STACK_LINE(540)
		int _g = this->itemArray->__get((int)0).StaticCast< ::MarketItem >()->getItemCount();		HX_STACK_VAR(_g,"_g");
		struct _Function_1_1{
			inline static bool Block( hx::ObjectPtr< ::Market_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","Market.hx",540,0xdfc0ea62)
				{
					HX_STACK_LINE(540)
					int _g1 = __this->itemArray->__get((int)1).StaticCast< ::MarketItem >()->getItemCount();		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(540)
					return (_g1 > (int)0);
				}
				return null();
			}
		};
		struct _Function_1_2{
			inline static bool Block( hx::ObjectPtr< ::Market_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","Market.hx",540,0xdfc0ea62)
				{
					HX_STACK_LINE(540)
					int _g2 = __this->itemArray->__get((int)2).StaticCast< ::MarketItem >()->getItemCount();		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(540)
					return (_g2 > (int)0);
				}
				return null();
			}
		};
		HX_STACK_LINE(540)
		if (((  ((!(((  ((!(((_g > (int)0))))) ? bool(_Function_1_1::Block(this)) : bool(true) ))))) ? bool(_Function_1_2::Block(this)) : bool(true) ))){
			HX_STACK_LINE(541)
			this->itemArray->__get((int)0).StaticCast< ::MarketItem >()->setItemAvailable(true);
			HX_STACK_LINE(542)
			this->itemArray->__get((int)0).StaticCast< ::MarketItem >()->setAvailableAnimation();
			HX_STACK_LINE(543)
			this->itemArray->__get((int)1).StaticCast< ::MarketItem >()->setItemAvailable(true);
			HX_STACK_LINE(544)
			this->itemArray->__get((int)1).StaticCast< ::MarketItem >()->setAvailableAnimation();
			HX_STACK_LINE(545)
			this->itemArray->__get((int)2).StaticCast< ::MarketItem >()->setItemAvailable(true);
			HX_STACK_LINE(546)
			this->itemArray->__get((int)2).StaticCast< ::MarketItem >()->setAvailableAnimation();
		}
		HX_STACK_LINE(548)
		int _g3 = this->itemArray->__get((int)0).StaticCast< ::MarketItem >()->getItemCount();		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(548)
		if (((_g3 == (int)0))){
			HX_STACK_LINE(549)
			this->itemArray->__get((int)0).StaticCast< ::MarketItem >()->setItemAvailable(false);
			HX_STACK_LINE(550)
			this->itemArray->__get((int)0).StaticCast< ::MarketItem >()->setAvailableAnimation();
		}
		HX_STACK_LINE(552)
		int _g4 = this->itemArray->__get((int)1).StaticCast< ::MarketItem >()->getItemCount();		HX_STACK_VAR(_g4,"_g4");
		HX_STACK_LINE(552)
		if (((_g4 == (int)0))){
			HX_STACK_LINE(553)
			this->itemArray->__get((int)1).StaticCast< ::MarketItem >()->setItemAvailable(false);
			HX_STACK_LINE(554)
			this->itemArray->__get((int)1).StaticCast< ::MarketItem >()->setAvailableAnimation();
		}
		HX_STACK_LINE(556)
		int _g5 = this->itemArray->__get((int)2).StaticCast< ::MarketItem >()->getItemCount();		HX_STACK_VAR(_g5,"_g5");
		HX_STACK_LINE(556)
		if (((_g5 == (int)0))){
			HX_STACK_LINE(557)
			this->itemArray->__get((int)2).StaticCast< ::MarketItem >()->setItemAvailable(false);
			HX_STACK_LINE(558)
			this->itemArray->__get((int)2).StaticCast< ::MarketItem >()->setAvailableAnimation();
		}
		HX_STACK_LINE(560)
		if (((this->replenishSeedTimer < (int)0))){
			HX_STACK_LINE(561)
			this->replenishSeedTimer = (int)180;
			HX_STACK_LINE(562)
			int _g6 = this->itemArray->__get((int)0).StaticCast< ::MarketItem >()->getItemCount();		HX_STACK_VAR(_g6,"_g6");
			HX_STACK_LINE(562)
			if (((_g6 < (int)2))){
				HX_STACK_LINE(563)
				this->itemArray->__get((int)0).StaticCast< ::MarketItem >()->incItemCount();
			}
		}
		HX_STACK_LINE(566)
		if (((this->replenishJuiceMachineTimer < (int)0))){
			HX_STACK_LINE(567)
			this->replenishJuiceMachineTimer = (int)300;
			HX_STACK_LINE(568)
			int _g7 = this->itemArray->__get((int)1).StaticCast< ::MarketItem >()->getItemCount();		HX_STACK_VAR(_g7,"_g7");
			HX_STACK_LINE(568)
			if (((_g7 < (int)1))){
				HX_STACK_LINE(569)
				this->itemArray->__get((int)1).StaticCast< ::MarketItem >()->incItemCount();
			}
		}
		HX_STACK_LINE(572)
		if (((this->replenishPieMachineSeedTimer < (int)0))){
			HX_STACK_LINE(573)
			this->replenishPieMachineSeedTimer = (int)300;
			HX_STACK_LINE(574)
			int _g8 = this->itemArray->__get((int)2).StaticCast< ::MarketItem >()->getItemCount();		HX_STACK_VAR(_g8,"_g8");
			HX_STACK_LINE(574)
			if (((_g8 < (int)1))){
				HX_STACK_LINE(575)
				this->itemArray->__get((int)2).StaticCast< ::MarketItem >()->incItemCount();
			}
		}
		HX_STACK_LINE(578)
		int _g9 = this->koiFishItem->getItemCount();		HX_STACK_VAR(_g9,"_g9");
		HX_STACK_LINE(578)
		if (((_g9 > (int)0))){
			HX_STACK_LINE(579)
			this->koiFishItem->setItemAvailable(true);
			HX_STACK_LINE(580)
			this->koiFishItem->setAvailableAnimation();
		}
		HX_STACK_LINE(582)
		int _g10 = this->greenHouseItem->getItemCount();		HX_STACK_VAR(_g10,"_g10");
		HX_STACK_LINE(582)
		if (((_g10 > (int)0))){
			HX_STACK_LINE(583)
			this->greenHouseItem->setItemAvailable(true);
			HX_STACK_LINE(584)
			this->greenHouseItem->setAvailableAnimation();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,replenishItems,(void))

Void Market_obj::waterBillCoinPayment( ){
{
		HX_STACK_FRAME("Market","waterBillCoinPayment",0x4c31b229,"Market.waterBillCoinPayment","Market.hx",588,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(589)
		hx::SubEq(this->waterBillTimer,::flixel::FlxG_obj::elapsed);
		HX_STACK_LINE(590)
		if (((this->waterBillTimer < (int)0))){
			HX_STACK_LINE(591)
			hx::SubEq(this->coinCount,this->waterClientCount);
			HX_STACK_LINE(592)
			this->waterBillTimer = (int)30;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,waterBillCoinPayment,(void))

Void Market_obj::ElectricBillCoinPayment( ){
{
		HX_STACK_FRAME("Market","ElectricBillCoinPayment",0x47775c9f,"Market.ElectricBillCoinPayment","Market.hx",596,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(597)
		hx::SubEq(this->electricBillTimer,::flixel::FlxG_obj::elapsed);
		HX_STACK_LINE(598)
		if (((this->electricBillTimer < (int)0))){
			HX_STACK_LINE(599)
			hx::SubEq(this->coinCount,(this->electricClientCount * (int)2));
			HX_STACK_LINE(600)
			this->electricBillTimer = (int)10;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,ElectricBillCoinPayment,(void))

Void Market_obj::destroySound( ){
{
		HX_STACK_FRAME("Market","destroySound",0x07b42f47,"Market.destroySound","Market.hx",604,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(605)
		if (((bool((this->marketOpenSound != null())) && bool(!(((this->marketOpenSound->_channel != null()))))))){
			HX_STACK_LINE(606)
			::flixel::system::FlxSound _g = ::flixel::util::FlxDestroyUtil_obj::destroy(this->marketOpenSound);		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(606)
			this->marketOpenSound = _g;
			HX_STACK_LINE(607)
			this->marketOpenSound = null();
		}
		HX_STACK_LINE(610)
		if (((bool((this->marketClosedSound != null())) && bool(!(((this->marketClosedSound->_channel != null()))))))){
			HX_STACK_LINE(611)
			::flixel::system::FlxSound _g1 = ::flixel::util::FlxDestroyUtil_obj::destroy(this->marketClosedSound);		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(611)
			this->marketClosedSound = _g1;
			HX_STACK_LINE(612)
			this->marketClosedSound = null();
		}
		HX_STACK_LINE(615)
		if (((bool((this->purchaseItemSound != null())) && bool(!(((this->purchaseItemSound->_channel != null()))))))){
			HX_STACK_LINE(616)
			::flixel::system::FlxSound _g2 = ::flixel::util::FlxDestroyUtil_obj::destroy(this->purchaseItemSound);		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(616)
			this->purchaseItemSound = _g2;
			HX_STACK_LINE(617)
			this->purchaseItemSound = null();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Market_obj,destroySound,(void))

Void Market_obj::update( ){
{
		HX_STACK_FRAME("Market","update",0xca95265b,"Market.update","Market.hx",622,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(623)
		this->slideMarketOut(null());
		HX_STACK_LINE(624)
		this->displayItemCountText();
		HX_STACK_LINE(625)
		this->replenishItems();
		HX_STACK_LINE(626)
		this->timedMarketClose();
		HX_STACK_LINE(627)
		this->displaySeedCount();
		HX_STACK_LINE(628)
		this->displayCoinCount();
		HX_STACK_LINE(629)
		this->destroySound();
		HX_STACK_LINE(630)
		this->super::update();
	}
return null();
}


Void Market_obj::destroy( ){
{
		HX_STACK_FRAME("Market","destroy",0xfea2d468,"Market.destroy","Market.hx",633,0xdfc0ea62)
		HX_STACK_THIS(this)
		HX_STACK_LINE(634)
		this->marketGroup = null();
		HX_STACK_LINE(635)
		this->marketSideBarSprite = null();
		HX_STACK_LINE(636)
		this->marketSideBarTabSprite = null();
		HX_STACK_LINE(637)
		this->marketSideBarShaftSprite = null();
		HX_STACK_LINE(638)
		this->koiFishItem = null();
		HX_STACK_LINE(639)
		this->seedItem = null();
		HX_STACK_LINE(640)
		this->juiceItem = null();
		HX_STACK_LINE(641)
		this->pieItem = null();
		HX_STACK_LINE(642)
		this->coin = null();
		HX_STACK_LINE(643)
		this->seedCountText = null();
		HX_STACK_LINE(644)
		this->juiceCountText = null();
		HX_STACK_LINE(645)
		this->pieCountText = null();
		HX_STACK_LINE(646)
		this->fishCountText = null();
		HX_STACK_LINE(647)
		this->greenHouseCountText = null();
		HX_STACK_LINE(648)
		this->juicePackCountText = null();
		HX_STACK_LINE(649)
		this->piePackCountText = null();
		HX_STACK_LINE(650)
		this->greenHouseItem = null();
		HX_STACK_LINE(651)
		this->itemArray = null();
		HX_STACK_LINE(652)
		this->seedItemCountFieldText = null();
		HX_STACK_LINE(653)
		this->juiceItemCountFieldText = null();
		HX_STACK_LINE(654)
		this->pieItemCountFieldText = null();
		HX_STACK_LINE(655)
		this->seedItemCostFieldText = null();
		HX_STACK_LINE(656)
		this->juiceItemCostFieldText = null();
		HX_STACK_LINE(657)
		this->pieItemCostFieldText = null();
		HX_STACK_LINE(658)
		this->fishItemCostFieldText = null();
		HX_STACK_LINE(659)
		this->fishItemCountFieldText = null();
		HX_STACK_LINE(660)
		this->greenHouseItemCountFieldText = null();
		HX_STACK_LINE(661)
		this->greenHouseItemCostFieldText = null();
		HX_STACK_LINE(662)
		this->super::destroy();
	}
return null();
}



Market_obj::Market_obj()
{
}

void Market_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Market);
	HX_MARK_MEMBER_NAME(marketGroup,"marketGroup");
	HX_MARK_MEMBER_NAME(marketSideBarSprite,"marketSideBarSprite");
	HX_MARK_MEMBER_NAME(marketSideBarTabSprite,"marketSideBarTabSprite");
	HX_MARK_MEMBER_NAME(marketSideBarShaftSprite,"marketSideBarShaftSprite");
	HX_MARK_MEMBER_NAME(seedItem,"seedItem");
	HX_MARK_MEMBER_NAME(juiceItem,"juiceItem");
	HX_MARK_MEMBER_NAME(pieItem,"pieItem");
	HX_MARK_MEMBER_NAME(koiFishItem,"koiFishItem");
	HX_MARK_MEMBER_NAME(greenHouseItem,"greenHouseItem");
	HX_MARK_MEMBER_NAME(seedsAvailableText,"seedsAvailableText");
	HX_MARK_MEMBER_NAME(greenHouseItemCountFieldText,"greenHouseItemCountFieldText");
	HX_MARK_MEMBER_NAME(fishItemCountFieldText,"fishItemCountFieldText");
	HX_MARK_MEMBER_NAME(seedItemCountFieldText,"seedItemCountFieldText");
	HX_MARK_MEMBER_NAME(juiceItemCountFieldText,"juiceItemCountFieldText");
	HX_MARK_MEMBER_NAME(pieItemCountFieldText,"pieItemCountFieldText");
	HX_MARK_MEMBER_NAME(greenHouseItemCostFieldText,"greenHouseItemCostFieldText");
	HX_MARK_MEMBER_NAME(seedItemCostFieldText,"seedItemCostFieldText");
	HX_MARK_MEMBER_NAME(juiceItemCostFieldText,"juiceItemCostFieldText");
	HX_MARK_MEMBER_NAME(pieItemCostFieldText,"pieItemCostFieldText");
	HX_MARK_MEMBER_NAME(fishItemCostFieldText,"fishItemCostFieldText");
	HX_MARK_MEMBER_NAME(juicePackCountText,"juicePackCountText");
	HX_MARK_MEMBER_NAME(piePackCountText,"piePackCountText");
	HX_MARK_MEMBER_NAME(itemArray,"itemArray");
	HX_MARK_MEMBER_NAME(coin,"coin");
	HX_MARK_MEMBER_NAME(coinCount,"coinCount");
	HX_MARK_MEMBER_NAME(coinCountString,"coinCountString");
	HX_MARK_MEMBER_NAME(coinCountFieldText,"coinCountFieldText");
	HX_MARK_MEMBER_NAME(piePack,"piePack");
	HX_MARK_MEMBER_NAME(juicePack,"juicePack");
	HX_MARK_MEMBER_NAME(seedCount,"seedCount");
	HX_MARK_MEMBER_NAME(juiceCount,"juiceCount");
	HX_MARK_MEMBER_NAME(pieCount,"pieCount");
	HX_MARK_MEMBER_NAME(fishCount,"fishCount");
	HX_MARK_MEMBER_NAME(greenHouseCount,"greenHouseCount");
	HX_MARK_MEMBER_NAME(waterClientCount,"waterClientCount");
	HX_MARK_MEMBER_NAME(electricClientCount,"electricClientCount");
	HX_MARK_MEMBER_NAME(shiftX,"shiftX");
	HX_MARK_MEMBER_NAME(shiftY,"shiftY");
	HX_MARK_MEMBER_NAME(seedCountString,"seedCountString");
	HX_MARK_MEMBER_NAME(seedCountText,"seedCountText");
	HX_MARK_MEMBER_NAME(juiceCountText,"juiceCountText");
	HX_MARK_MEMBER_NAME(fishCountText,"fishCountText");
	HX_MARK_MEMBER_NAME(pieCountText,"pieCountText");
	HX_MARK_MEMBER_NAME(greenHouseCountText,"greenHouseCountText");
	HX_MARK_MEMBER_NAME(marketCloseTimer,"marketCloseTimer");
	HX_MARK_MEMBER_NAME(replenishSeedTimer,"replenishSeedTimer");
	HX_MARK_MEMBER_NAME(replenishJuiceMachineTimer,"replenishJuiceMachineTimer");
	HX_MARK_MEMBER_NAME(replenishPieMachineSeedTimer,"replenishPieMachineSeedTimer");
	HX_MARK_MEMBER_NAME(waterBillTimer,"waterBillTimer");
	HX_MARK_MEMBER_NAME(electricBillTimer,"electricBillTimer");
	HX_MARK_MEMBER_NAME(marketOpen,"marketOpen");
	HX_MARK_MEMBER_NAME(turn,"turn");
	HX_MARK_MEMBER_NAME(closedSoundPlayed,"closedSoundPlayed");
	HX_MARK_MEMBER_NAME(marketOpenSound,"marketOpenSound");
	HX_MARK_MEMBER_NAME(marketClosedSound,"marketClosedSound");
	HX_MARK_MEMBER_NAME(purchaseItemSound,"purchaseItemSound");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void Market_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(marketGroup,"marketGroup");
	HX_VISIT_MEMBER_NAME(marketSideBarSprite,"marketSideBarSprite");
	HX_VISIT_MEMBER_NAME(marketSideBarTabSprite,"marketSideBarTabSprite");
	HX_VISIT_MEMBER_NAME(marketSideBarShaftSprite,"marketSideBarShaftSprite");
	HX_VISIT_MEMBER_NAME(seedItem,"seedItem");
	HX_VISIT_MEMBER_NAME(juiceItem,"juiceItem");
	HX_VISIT_MEMBER_NAME(pieItem,"pieItem");
	HX_VISIT_MEMBER_NAME(koiFishItem,"koiFishItem");
	HX_VISIT_MEMBER_NAME(greenHouseItem,"greenHouseItem");
	HX_VISIT_MEMBER_NAME(seedsAvailableText,"seedsAvailableText");
	HX_VISIT_MEMBER_NAME(greenHouseItemCountFieldText,"greenHouseItemCountFieldText");
	HX_VISIT_MEMBER_NAME(fishItemCountFieldText,"fishItemCountFieldText");
	HX_VISIT_MEMBER_NAME(seedItemCountFieldText,"seedItemCountFieldText");
	HX_VISIT_MEMBER_NAME(juiceItemCountFieldText,"juiceItemCountFieldText");
	HX_VISIT_MEMBER_NAME(pieItemCountFieldText,"pieItemCountFieldText");
	HX_VISIT_MEMBER_NAME(greenHouseItemCostFieldText,"greenHouseItemCostFieldText");
	HX_VISIT_MEMBER_NAME(seedItemCostFieldText,"seedItemCostFieldText");
	HX_VISIT_MEMBER_NAME(juiceItemCostFieldText,"juiceItemCostFieldText");
	HX_VISIT_MEMBER_NAME(pieItemCostFieldText,"pieItemCostFieldText");
	HX_VISIT_MEMBER_NAME(fishItemCostFieldText,"fishItemCostFieldText");
	HX_VISIT_MEMBER_NAME(juicePackCountText,"juicePackCountText");
	HX_VISIT_MEMBER_NAME(piePackCountText,"piePackCountText");
	HX_VISIT_MEMBER_NAME(itemArray,"itemArray");
	HX_VISIT_MEMBER_NAME(coin,"coin");
	HX_VISIT_MEMBER_NAME(coinCount,"coinCount");
	HX_VISIT_MEMBER_NAME(coinCountString,"coinCountString");
	HX_VISIT_MEMBER_NAME(coinCountFieldText,"coinCountFieldText");
	HX_VISIT_MEMBER_NAME(piePack,"piePack");
	HX_VISIT_MEMBER_NAME(juicePack,"juicePack");
	HX_VISIT_MEMBER_NAME(seedCount,"seedCount");
	HX_VISIT_MEMBER_NAME(juiceCount,"juiceCount");
	HX_VISIT_MEMBER_NAME(pieCount,"pieCount");
	HX_VISIT_MEMBER_NAME(fishCount,"fishCount");
	HX_VISIT_MEMBER_NAME(greenHouseCount,"greenHouseCount");
	HX_VISIT_MEMBER_NAME(waterClientCount,"waterClientCount");
	HX_VISIT_MEMBER_NAME(electricClientCount,"electricClientCount");
	HX_VISIT_MEMBER_NAME(shiftX,"shiftX");
	HX_VISIT_MEMBER_NAME(shiftY,"shiftY");
	HX_VISIT_MEMBER_NAME(seedCountString,"seedCountString");
	HX_VISIT_MEMBER_NAME(seedCountText,"seedCountText");
	HX_VISIT_MEMBER_NAME(juiceCountText,"juiceCountText");
	HX_VISIT_MEMBER_NAME(fishCountText,"fishCountText");
	HX_VISIT_MEMBER_NAME(pieCountText,"pieCountText");
	HX_VISIT_MEMBER_NAME(greenHouseCountText,"greenHouseCountText");
	HX_VISIT_MEMBER_NAME(marketCloseTimer,"marketCloseTimer");
	HX_VISIT_MEMBER_NAME(replenishSeedTimer,"replenishSeedTimer");
	HX_VISIT_MEMBER_NAME(replenishJuiceMachineTimer,"replenishJuiceMachineTimer");
	HX_VISIT_MEMBER_NAME(replenishPieMachineSeedTimer,"replenishPieMachineSeedTimer");
	HX_VISIT_MEMBER_NAME(waterBillTimer,"waterBillTimer");
	HX_VISIT_MEMBER_NAME(electricBillTimer,"electricBillTimer");
	HX_VISIT_MEMBER_NAME(marketOpen,"marketOpen");
	HX_VISIT_MEMBER_NAME(turn,"turn");
	HX_VISIT_MEMBER_NAME(closedSoundPlayed,"closedSoundPlayed");
	HX_VISIT_MEMBER_NAME(marketOpenSound,"marketOpenSound");
	HX_VISIT_MEMBER_NAME(marketClosedSound,"marketClosedSound");
	HX_VISIT_MEMBER_NAME(purchaseItemSound,"purchaseItemSound");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic Market_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"coin") ) { return coin; }
		if (HX_FIELD_EQ(inName,"turn") ) { return turn; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"shiftX") ) { return shiftX; }
		if (HX_FIELD_EQ(inName,"shiftY") ) { return shiftY; }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"pieItem") ) { return pieItem; }
		if (HX_FIELD_EQ(inName,"piePack") ) { return piePack; }
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"seedItem") ) { return seedItem; }
		if (HX_FIELD_EQ(inName,"pieCount") ) { return pieCount; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"juiceItem") ) { return juiceItem; }
		if (HX_FIELD_EQ(inName,"itemArray") ) { return itemArray; }
		if (HX_FIELD_EQ(inName,"coinCount") ) { return coinCount; }
		if (HX_FIELD_EQ(inName,"juicePack") ) { return juicePack; }
		if (HX_FIELD_EQ(inName,"seedCount") ) { return seedCount; }
		if (HX_FIELD_EQ(inName,"fishCount") ) { return fishCount; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"juiceCount") ) { return juiceCount; }
		if (HX_FIELD_EQ(inName,"marketOpen") ) { return marketOpen; }
		if (HX_FIELD_EQ(inName,"selectItem") ) { return selectItem_dyn(); }
		if (HX_FIELD_EQ(inName,"openMarket") ) { return openMarket_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"marketGroup") ) { return marketGroup; }
		if (HX_FIELD_EQ(inName,"koiFishItem") ) { return koiFishItem; }
		if (HX_FIELD_EQ(inName,"getPieCount") ) { return getPieCount_dyn(); }
		if (HX_FIELD_EQ(inName,"setPieCount") ) { return setPieCount_dyn(); }
		if (HX_FIELD_EQ(inName,"decPieCount") ) { return decPieCount_dyn(); }
		if (HX_FIELD_EQ(inName,"incPieCount") ) { return incPieCount_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"pieCountText") ) { return pieCountText; }
		if (HX_FIELD_EQ(inName,"getCoinCount") ) { return getCoinCount_dyn(); }
		if (HX_FIELD_EQ(inName,"getSeedCount") ) { return getSeedCount_dyn(); }
		if (HX_FIELD_EQ(inName,"getFishCount") ) { return getFishCount_dyn(); }
		if (HX_FIELD_EQ(inName,"setSeedCount") ) { return setSeedCount_dyn(); }
		if (HX_FIELD_EQ(inName,"setFishCount") ) { return setFishCount_dyn(); }
		if (HX_FIELD_EQ(inName,"setCoinCount") ) { return setCoinCount_dyn(); }
		if (HX_FIELD_EQ(inName,"decSeedCount") ) { return decSeedCount_dyn(); }
		if (HX_FIELD_EQ(inName,"incSeedCount") ) { return incSeedCount_dyn(); }
		if (HX_FIELD_EQ(inName,"decFishCount") ) { return decFishCount_dyn(); }
		if (HX_FIELD_EQ(inName,"incfishCount") ) { return incfishCount_dyn(); }
		if (HX_FIELD_EQ(inName,"purchaseItem") ) { return purchaseItem_dyn(); }
		if (HX_FIELD_EQ(inName,"destroySound") ) { return destroySound_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"seedCountText") ) { return seedCountText; }
		if (HX_FIELD_EQ(inName,"fishCountText") ) { return fishCountText; }
		if (HX_FIELD_EQ(inName,"getMarketOpen") ) { return getMarketOpen_dyn(); }
		if (HX_FIELD_EQ(inName,"getJuiceCount") ) { return getJuiceCount_dyn(); }
		if (HX_FIELD_EQ(inName,"stockPiePacks") ) { return stockPiePacks_dyn(); }
		if (HX_FIELD_EQ(inName,"setJuiceCount") ) { return setJuiceCount_dyn(); }
		if (HX_FIELD_EQ(inName,"decjuiceCount") ) { return decjuiceCount_dyn(); }
		if (HX_FIELD_EQ(inName,"incJuiceCount") ) { return incJuiceCount_dyn(); }
		if (HX_FIELD_EQ(inName,"fillItemArray") ) { return fillItemArray_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"greenHouseItem") ) { return greenHouseItem; }
		if (HX_FIELD_EQ(inName,"juiceCountText") ) { return juiceCountText; }
		if (HX_FIELD_EQ(inName,"waterBillTimer") ) { return waterBillTimer; }
		if (HX_FIELD_EQ(inName,"registerEvents") ) { return registerEvents_dyn(); }
		if (HX_FIELD_EQ(inName,"slideMarketOut") ) { return slideMarketOut_dyn(); }
		if (HX_FIELD_EQ(inName,"shiftItemsOpen") ) { return shiftItemsOpen_dyn(); }
		if (HX_FIELD_EQ(inName,"replenishItems") ) { return replenishItems_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"coinCountString") ) { return coinCountString; }
		if (HX_FIELD_EQ(inName,"greenHouseCount") ) { return greenHouseCount; }
		if (HX_FIELD_EQ(inName,"seedCountString") ) { return seedCountString; }
		if (HX_FIELD_EQ(inName,"marketOpenSound") ) { return marketOpenSound; }
		if (HX_FIELD_EQ(inName,"getPiePackCount") ) { return getPiePackCount_dyn(); }
		if (HX_FIELD_EQ(inName,"stockJuicePacks") ) { return stockJuicePacks_dyn(); }
		if (HX_FIELD_EQ(inName,"loadPiePacksDec") ) { return loadPiePacksDec_dyn(); }
		if (HX_FIELD_EQ(inName,"setPiePackCount") ) { return setPiePackCount_dyn(); }
		if (HX_FIELD_EQ(inName,"setPieItemCount") ) { return setPieItemCount_dyn(); }
		if (HX_FIELD_EQ(inName,"coinPaymentUtil") ) { return coinPaymentUtil_dyn(); }
		if (HX_FIELD_EQ(inName,"shiftItemsClose") ) { return shiftItemsClose_dyn(); }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"piePackCountText") ) { return piePackCountText; }
		if (HX_FIELD_EQ(inName,"waterClientCount") ) { return waterClientCount; }
		if (HX_FIELD_EQ(inName,"marketCloseTimer") ) { return marketCloseTimer; }
		if (HX_FIELD_EQ(inName,"setseedItemCount") ) { return setseedItemCount_dyn(); }
		if (HX_FIELD_EQ(inName,"addSubtractCoins") ) { return addSubtractCoins_dyn(); }
		if (HX_FIELD_EQ(inName,"displayCoinCount") ) { return displayCoinCount_dyn(); }
		if (HX_FIELD_EQ(inName,"displaySeedCount") ) { return displaySeedCount_dyn(); }
		if (HX_FIELD_EQ(inName,"unregisterEvents") ) { return unregisterEvents_dyn(); }
		if (HX_FIELD_EQ(inName,"timedMarketClose") ) { return timedMarketClose_dyn(); }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"electricBillTimer") ) { return electricBillTimer; }
		if (HX_FIELD_EQ(inName,"closedSoundPlayed") ) { return closedSoundPlayed; }
		if (HX_FIELD_EQ(inName,"marketClosedSound") ) { return marketClosedSound; }
		if (HX_FIELD_EQ(inName,"purchaseItemSound") ) { return purchaseItemSound; }
		if (HX_FIELD_EQ(inName,"getJuicePackCount") ) { return getJuicePackCount_dyn(); }
		if (HX_FIELD_EQ(inName,"loadJuicePacksDec") ) { return loadJuicePacksDec_dyn(); }
		if (HX_FIELD_EQ(inName,"setCountTextXOpen") ) { return setCountTextXOpen_dyn(); }
		if (HX_FIELD_EQ(inName,"setJuicePackCount") ) { return setJuicePackCount_dyn(); }
		if (HX_FIELD_EQ(inName,"setJuiceItemCount") ) { return setJuiceItemCount_dyn(); }
		if (HX_FIELD_EQ(inName,"decjuicePackCount") ) { return decjuicePackCount_dyn(); }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"seedsAvailableText") ) { return seedsAvailableText; }
		if (HX_FIELD_EQ(inName,"juicePackCountText") ) { return juicePackCountText; }
		if (HX_FIELD_EQ(inName,"coinCountFieldText") ) { return coinCountFieldText; }
		if (HX_FIELD_EQ(inName,"replenishSeedTimer") ) { return replenishSeedTimer; }
		if (HX_FIELD_EQ(inName,"getGreenHouseCount") ) { return getGreenHouseCount_dyn(); }
		if (HX_FIELD_EQ(inName,"setCountTextXClose") ) { return setCountTextXClose_dyn(); }
		if (HX_FIELD_EQ(inName,"setGreenHouseCount") ) { return setGreenHouseCount_dyn(); }
		if (HX_FIELD_EQ(inName,"decGreenHouseCount") ) { return decGreenHouseCount_dyn(); }
		if (HX_FIELD_EQ(inName,"incGreenHouseCount") ) { return incGreenHouseCount_dyn(); }
		if (HX_FIELD_EQ(inName,"positionArrayItems") ) { return positionArrayItems_dyn(); }
		if (HX_FIELD_EQ(inName,"registerItemEvents") ) { return registerItemEvents_dyn(); }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"marketSideBarSprite") ) { return marketSideBarSprite; }
		if (HX_FIELD_EQ(inName,"electricClientCount") ) { return electricClientCount; }
		if (HX_FIELD_EQ(inName,"greenHouseCountText") ) { return greenHouseCountText; }
		if (HX_FIELD_EQ(inName,"setKoiPondItemCount") ) { return setKoiPondItemCount_dyn(); }
		if (HX_FIELD_EQ(inName,"setWaterClientCount") ) { return setWaterClientCount_dyn(); }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"pieItemCostFieldText") ) { return pieItemCostFieldText; }
		if (HX_FIELD_EQ(inName,"displayItemCountText") ) { return displayItemCountText_dyn(); }
		if (HX_FIELD_EQ(inName,"unregisterItemEvents") ) { return unregisterItemEvents_dyn(); }
		if (HX_FIELD_EQ(inName,"waterBillCoinPayment") ) { return waterBillCoinPayment_dyn(); }
		break;
	case 21:
		if (HX_FIELD_EQ(inName,"pieItemCountFieldText") ) { return pieItemCountFieldText; }
		if (HX_FIELD_EQ(inName,"seedItemCostFieldText") ) { return seedItemCostFieldText; }
		if (HX_FIELD_EQ(inName,"fishItemCostFieldText") ) { return fishItemCostFieldText; }
		break;
	case 22:
		if (HX_FIELD_EQ(inName,"marketSideBarTabSprite") ) { return marketSideBarTabSprite; }
		if (HX_FIELD_EQ(inName,"fishItemCountFieldText") ) { return fishItemCountFieldText; }
		if (HX_FIELD_EQ(inName,"seedItemCountFieldText") ) { return seedItemCountFieldText; }
		if (HX_FIELD_EQ(inName,"juiceItemCostFieldText") ) { return juiceItemCostFieldText; }
		if (HX_FIELD_EQ(inName,"setGreenHouseItemCount") ) { return setGreenHouseItemCount_dyn(); }
		if (HX_FIELD_EQ(inName,"setElectricClientCount") ) { return setElectricClientCount_dyn(); }
		break;
	case 23:
		if (HX_FIELD_EQ(inName,"juiceItemCountFieldText") ) { return juiceItemCountFieldText; }
		if (HX_FIELD_EQ(inName,"ElectricBillCoinPayment") ) { return ElectricBillCoinPayment_dyn(); }
		break;
	case 24:
		if (HX_FIELD_EQ(inName,"marketSideBarShaftSprite") ) { return marketSideBarShaftSprite; }
		break;
	case 26:
		if (HX_FIELD_EQ(inName,"replenishJuiceMachineTimer") ) { return replenishJuiceMachineTimer; }
		break;
	case 27:
		if (HX_FIELD_EQ(inName,"greenHouseItemCostFieldText") ) { return greenHouseItemCostFieldText; }
		break;
	case 28:
		if (HX_FIELD_EQ(inName,"greenHouseItemCountFieldText") ) { return greenHouseItemCountFieldText; }
		if (HX_FIELD_EQ(inName,"replenishPieMachineSeedTimer") ) { return replenishPieMachineSeedTimer; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Market_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"coin") ) { coin=inValue.Cast< ::GoldCoin >(); return inValue; }
		if (HX_FIELD_EQ(inName,"turn") ) { turn=inValue.Cast< bool >(); return inValue; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"shiftX") ) { shiftX=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"shiftY") ) { shiftY=inValue.Cast< int >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"pieItem") ) { pieItem=inValue.Cast< ::PieItem >(); return inValue; }
		if (HX_FIELD_EQ(inName,"piePack") ) { piePack=inValue.Cast< int >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"seedItem") ) { seedItem=inValue.Cast< ::SeedItem >(); return inValue; }
		if (HX_FIELD_EQ(inName,"pieCount") ) { pieCount=inValue.Cast< int >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"juiceItem") ) { juiceItem=inValue.Cast< ::JuiceItem >(); return inValue; }
		if (HX_FIELD_EQ(inName,"itemArray") ) { itemArray=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		if (HX_FIELD_EQ(inName,"coinCount") ) { coinCount=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"juicePack") ) { juicePack=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"seedCount") ) { seedCount=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"fishCount") ) { fishCount=inValue.Cast< int >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"juiceCount") ) { juiceCount=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"marketOpen") ) { marketOpen=inValue.Cast< bool >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"marketGroup") ) { marketGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"koiFishItem") ) { koiFishItem=inValue.Cast< ::KoiItem >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"pieCountText") ) { pieCountText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"seedCountText") ) { seedCountText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		if (HX_FIELD_EQ(inName,"fishCountText") ) { fishCountText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"greenHouseItem") ) { greenHouseItem=inValue.Cast< ::GreenHouseItem >(); return inValue; }
		if (HX_FIELD_EQ(inName,"juiceCountText") ) { juiceCountText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		if (HX_FIELD_EQ(inName,"waterBillTimer") ) { waterBillTimer=inValue.Cast< Float >(); return inValue; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"coinCountString") ) { coinCountString=inValue.Cast< ::String >(); return inValue; }
		if (HX_FIELD_EQ(inName,"greenHouseCount") ) { greenHouseCount=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"seedCountString") ) { seedCountString=inValue.Cast< ::String >(); return inValue; }
		if (HX_FIELD_EQ(inName,"marketOpenSound") ) { marketOpenSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"piePackCountText") ) { piePackCountText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		if (HX_FIELD_EQ(inName,"waterClientCount") ) { waterClientCount=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"marketCloseTimer") ) { marketCloseTimer=inValue.Cast< Float >(); return inValue; }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"electricBillTimer") ) { electricBillTimer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"closedSoundPlayed") ) { closedSoundPlayed=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"marketClosedSound") ) { marketClosedSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		if (HX_FIELD_EQ(inName,"purchaseItemSound") ) { purchaseItemSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"seedsAvailableText") ) { seedsAvailableText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		if (HX_FIELD_EQ(inName,"juicePackCountText") ) { juicePackCountText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		if (HX_FIELD_EQ(inName,"coinCountFieldText") ) { coinCountFieldText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		if (HX_FIELD_EQ(inName,"replenishSeedTimer") ) { replenishSeedTimer=inValue.Cast< Float >(); return inValue; }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"marketSideBarSprite") ) { marketSideBarSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"electricClientCount") ) { electricClientCount=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"greenHouseCountText") ) { greenHouseCountText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"pieItemCostFieldText") ) { pieItemCostFieldText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		break;
	case 21:
		if (HX_FIELD_EQ(inName,"pieItemCountFieldText") ) { pieItemCountFieldText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		if (HX_FIELD_EQ(inName,"seedItemCostFieldText") ) { seedItemCostFieldText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		if (HX_FIELD_EQ(inName,"fishItemCostFieldText") ) { fishItemCostFieldText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		break;
	case 22:
		if (HX_FIELD_EQ(inName,"marketSideBarTabSprite") ) { marketSideBarTabSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"fishItemCountFieldText") ) { fishItemCountFieldText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		if (HX_FIELD_EQ(inName,"seedItemCountFieldText") ) { seedItemCountFieldText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		if (HX_FIELD_EQ(inName,"juiceItemCostFieldText") ) { juiceItemCostFieldText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		break;
	case 23:
		if (HX_FIELD_EQ(inName,"juiceItemCountFieldText") ) { juiceItemCountFieldText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		break;
	case 24:
		if (HX_FIELD_EQ(inName,"marketSideBarShaftSprite") ) { marketSideBarShaftSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 26:
		if (HX_FIELD_EQ(inName,"replenishJuiceMachineTimer") ) { replenishJuiceMachineTimer=inValue.Cast< Float >(); return inValue; }
		break;
	case 27:
		if (HX_FIELD_EQ(inName,"greenHouseItemCostFieldText") ) { greenHouseItemCostFieldText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		break;
	case 28:
		if (HX_FIELD_EQ(inName,"greenHouseItemCountFieldText") ) { greenHouseItemCountFieldText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		if (HX_FIELD_EQ(inName,"replenishPieMachineSeedTimer") ) { replenishPieMachineSeedTimer=inValue.Cast< Float >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Market_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("marketGroup"));
	outFields->push(HX_CSTRING("marketSideBarSprite"));
	outFields->push(HX_CSTRING("marketSideBarTabSprite"));
	outFields->push(HX_CSTRING("marketSideBarShaftSprite"));
	outFields->push(HX_CSTRING("seedItem"));
	outFields->push(HX_CSTRING("juiceItem"));
	outFields->push(HX_CSTRING("pieItem"));
	outFields->push(HX_CSTRING("koiFishItem"));
	outFields->push(HX_CSTRING("greenHouseItem"));
	outFields->push(HX_CSTRING("seedsAvailableText"));
	outFields->push(HX_CSTRING("greenHouseItemCountFieldText"));
	outFields->push(HX_CSTRING("fishItemCountFieldText"));
	outFields->push(HX_CSTRING("seedItemCountFieldText"));
	outFields->push(HX_CSTRING("juiceItemCountFieldText"));
	outFields->push(HX_CSTRING("pieItemCountFieldText"));
	outFields->push(HX_CSTRING("greenHouseItemCostFieldText"));
	outFields->push(HX_CSTRING("seedItemCostFieldText"));
	outFields->push(HX_CSTRING("juiceItemCostFieldText"));
	outFields->push(HX_CSTRING("pieItemCostFieldText"));
	outFields->push(HX_CSTRING("fishItemCostFieldText"));
	outFields->push(HX_CSTRING("juicePackCountText"));
	outFields->push(HX_CSTRING("piePackCountText"));
	outFields->push(HX_CSTRING("itemArray"));
	outFields->push(HX_CSTRING("coin"));
	outFields->push(HX_CSTRING("coinCount"));
	outFields->push(HX_CSTRING("coinCountString"));
	outFields->push(HX_CSTRING("coinCountFieldText"));
	outFields->push(HX_CSTRING("piePack"));
	outFields->push(HX_CSTRING("juicePack"));
	outFields->push(HX_CSTRING("seedCount"));
	outFields->push(HX_CSTRING("juiceCount"));
	outFields->push(HX_CSTRING("pieCount"));
	outFields->push(HX_CSTRING("fishCount"));
	outFields->push(HX_CSTRING("greenHouseCount"));
	outFields->push(HX_CSTRING("waterClientCount"));
	outFields->push(HX_CSTRING("electricClientCount"));
	outFields->push(HX_CSTRING("shiftX"));
	outFields->push(HX_CSTRING("shiftY"));
	outFields->push(HX_CSTRING("seedCountString"));
	outFields->push(HX_CSTRING("seedCountText"));
	outFields->push(HX_CSTRING("juiceCountText"));
	outFields->push(HX_CSTRING("fishCountText"));
	outFields->push(HX_CSTRING("pieCountText"));
	outFields->push(HX_CSTRING("greenHouseCountText"));
	outFields->push(HX_CSTRING("marketCloseTimer"));
	outFields->push(HX_CSTRING("replenishSeedTimer"));
	outFields->push(HX_CSTRING("replenishJuiceMachineTimer"));
	outFields->push(HX_CSTRING("replenishPieMachineSeedTimer"));
	outFields->push(HX_CSTRING("waterBillTimer"));
	outFields->push(HX_CSTRING("electricBillTimer"));
	outFields->push(HX_CSTRING("marketOpen"));
	outFields->push(HX_CSTRING("turn"));
	outFields->push(HX_CSTRING("closedSoundPlayed"));
	outFields->push(HX_CSTRING("marketOpenSound"));
	outFields->push(HX_CSTRING("marketClosedSound"));
	outFields->push(HX_CSTRING("purchaseItemSound"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(Market_obj,marketGroup),HX_CSTRING("marketGroup")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Market_obj,marketSideBarSprite),HX_CSTRING("marketSideBarSprite")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Market_obj,marketSideBarTabSprite),HX_CSTRING("marketSideBarTabSprite")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Market_obj,marketSideBarShaftSprite),HX_CSTRING("marketSideBarShaftSprite")},
	{hx::fsObject /*::SeedItem*/ ,(int)offsetof(Market_obj,seedItem),HX_CSTRING("seedItem")},
	{hx::fsObject /*::JuiceItem*/ ,(int)offsetof(Market_obj,juiceItem),HX_CSTRING("juiceItem")},
	{hx::fsObject /*::PieItem*/ ,(int)offsetof(Market_obj,pieItem),HX_CSTRING("pieItem")},
	{hx::fsObject /*::KoiItem*/ ,(int)offsetof(Market_obj,koiFishItem),HX_CSTRING("koiFishItem")},
	{hx::fsObject /*::GreenHouseItem*/ ,(int)offsetof(Market_obj,greenHouseItem),HX_CSTRING("greenHouseItem")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,seedsAvailableText),HX_CSTRING("seedsAvailableText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,greenHouseItemCountFieldText),HX_CSTRING("greenHouseItemCountFieldText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,fishItemCountFieldText),HX_CSTRING("fishItemCountFieldText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,seedItemCountFieldText),HX_CSTRING("seedItemCountFieldText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,juiceItemCountFieldText),HX_CSTRING("juiceItemCountFieldText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,pieItemCountFieldText),HX_CSTRING("pieItemCountFieldText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,greenHouseItemCostFieldText),HX_CSTRING("greenHouseItemCostFieldText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,seedItemCostFieldText),HX_CSTRING("seedItemCostFieldText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,juiceItemCostFieldText),HX_CSTRING("juiceItemCostFieldText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,pieItemCostFieldText),HX_CSTRING("pieItemCostFieldText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,fishItemCostFieldText),HX_CSTRING("fishItemCostFieldText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,juicePackCountText),HX_CSTRING("juicePackCountText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,piePackCountText),HX_CSTRING("piePackCountText")},
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(Market_obj,itemArray),HX_CSTRING("itemArray")},
	{hx::fsObject /*::GoldCoin*/ ,(int)offsetof(Market_obj,coin),HX_CSTRING("coin")},
	{hx::fsInt,(int)offsetof(Market_obj,coinCount),HX_CSTRING("coinCount")},
	{hx::fsString,(int)offsetof(Market_obj,coinCountString),HX_CSTRING("coinCountString")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,coinCountFieldText),HX_CSTRING("coinCountFieldText")},
	{hx::fsInt,(int)offsetof(Market_obj,piePack),HX_CSTRING("piePack")},
	{hx::fsInt,(int)offsetof(Market_obj,juicePack),HX_CSTRING("juicePack")},
	{hx::fsInt,(int)offsetof(Market_obj,seedCount),HX_CSTRING("seedCount")},
	{hx::fsInt,(int)offsetof(Market_obj,juiceCount),HX_CSTRING("juiceCount")},
	{hx::fsInt,(int)offsetof(Market_obj,pieCount),HX_CSTRING("pieCount")},
	{hx::fsInt,(int)offsetof(Market_obj,fishCount),HX_CSTRING("fishCount")},
	{hx::fsInt,(int)offsetof(Market_obj,greenHouseCount),HX_CSTRING("greenHouseCount")},
	{hx::fsInt,(int)offsetof(Market_obj,waterClientCount),HX_CSTRING("waterClientCount")},
	{hx::fsInt,(int)offsetof(Market_obj,electricClientCount),HX_CSTRING("electricClientCount")},
	{hx::fsInt,(int)offsetof(Market_obj,shiftX),HX_CSTRING("shiftX")},
	{hx::fsInt,(int)offsetof(Market_obj,shiftY),HX_CSTRING("shiftY")},
	{hx::fsString,(int)offsetof(Market_obj,seedCountString),HX_CSTRING("seedCountString")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,seedCountText),HX_CSTRING("seedCountText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,juiceCountText),HX_CSTRING("juiceCountText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,fishCountText),HX_CSTRING("fishCountText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,pieCountText),HX_CSTRING("pieCountText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(Market_obj,greenHouseCountText),HX_CSTRING("greenHouseCountText")},
	{hx::fsFloat,(int)offsetof(Market_obj,marketCloseTimer),HX_CSTRING("marketCloseTimer")},
	{hx::fsFloat,(int)offsetof(Market_obj,replenishSeedTimer),HX_CSTRING("replenishSeedTimer")},
	{hx::fsFloat,(int)offsetof(Market_obj,replenishJuiceMachineTimer),HX_CSTRING("replenishJuiceMachineTimer")},
	{hx::fsFloat,(int)offsetof(Market_obj,replenishPieMachineSeedTimer),HX_CSTRING("replenishPieMachineSeedTimer")},
	{hx::fsFloat,(int)offsetof(Market_obj,waterBillTimer),HX_CSTRING("waterBillTimer")},
	{hx::fsFloat,(int)offsetof(Market_obj,electricBillTimer),HX_CSTRING("electricBillTimer")},
	{hx::fsBool,(int)offsetof(Market_obj,marketOpen),HX_CSTRING("marketOpen")},
	{hx::fsBool,(int)offsetof(Market_obj,turn),HX_CSTRING("turn")},
	{hx::fsBool,(int)offsetof(Market_obj,closedSoundPlayed),HX_CSTRING("closedSoundPlayed")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(Market_obj,marketOpenSound),HX_CSTRING("marketOpenSound")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(Market_obj,marketClosedSound),HX_CSTRING("marketClosedSound")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(Market_obj,purchaseItemSound),HX_CSTRING("purchaseItemSound")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("marketGroup"),
	HX_CSTRING("marketSideBarSprite"),
	HX_CSTRING("marketSideBarTabSprite"),
	HX_CSTRING("marketSideBarShaftSprite"),
	HX_CSTRING("seedItem"),
	HX_CSTRING("juiceItem"),
	HX_CSTRING("pieItem"),
	HX_CSTRING("koiFishItem"),
	HX_CSTRING("greenHouseItem"),
	HX_CSTRING("seedsAvailableText"),
	HX_CSTRING("greenHouseItemCountFieldText"),
	HX_CSTRING("fishItemCountFieldText"),
	HX_CSTRING("seedItemCountFieldText"),
	HX_CSTRING("juiceItemCountFieldText"),
	HX_CSTRING("pieItemCountFieldText"),
	HX_CSTRING("greenHouseItemCostFieldText"),
	HX_CSTRING("seedItemCostFieldText"),
	HX_CSTRING("juiceItemCostFieldText"),
	HX_CSTRING("pieItemCostFieldText"),
	HX_CSTRING("fishItemCostFieldText"),
	HX_CSTRING("juicePackCountText"),
	HX_CSTRING("piePackCountText"),
	HX_CSTRING("itemArray"),
	HX_CSTRING("coin"),
	HX_CSTRING("coinCount"),
	HX_CSTRING("coinCountString"),
	HX_CSTRING("coinCountFieldText"),
	HX_CSTRING("piePack"),
	HX_CSTRING("juicePack"),
	HX_CSTRING("seedCount"),
	HX_CSTRING("juiceCount"),
	HX_CSTRING("pieCount"),
	HX_CSTRING("fishCount"),
	HX_CSTRING("greenHouseCount"),
	HX_CSTRING("waterClientCount"),
	HX_CSTRING("electricClientCount"),
	HX_CSTRING("shiftX"),
	HX_CSTRING("shiftY"),
	HX_CSTRING("seedCountString"),
	HX_CSTRING("seedCountText"),
	HX_CSTRING("juiceCountText"),
	HX_CSTRING("fishCountText"),
	HX_CSTRING("pieCountText"),
	HX_CSTRING("greenHouseCountText"),
	HX_CSTRING("marketCloseTimer"),
	HX_CSTRING("replenishSeedTimer"),
	HX_CSTRING("replenishJuiceMachineTimer"),
	HX_CSTRING("replenishPieMachineSeedTimer"),
	HX_CSTRING("waterBillTimer"),
	HX_CSTRING("electricBillTimer"),
	HX_CSTRING("marketOpen"),
	HX_CSTRING("turn"),
	HX_CSTRING("closedSoundPlayed"),
	HX_CSTRING("marketOpenSound"),
	HX_CSTRING("marketClosedSound"),
	HX_CSTRING("purchaseItemSound"),
	HX_CSTRING("getMarketOpen"),
	HX_CSTRING("getCoinCount"),
	HX_CSTRING("getSeedCount"),
	HX_CSTRING("getJuiceCount"),
	HX_CSTRING("getPieCount"),
	HX_CSTRING("getFishCount"),
	HX_CSTRING("getGreenHouseCount"),
	HX_CSTRING("getJuicePackCount"),
	HX_CSTRING("getPiePackCount"),
	HX_CSTRING("stockJuicePacks"),
	HX_CSTRING("stockPiePacks"),
	HX_CSTRING("loadJuicePacksDec"),
	HX_CSTRING("loadPiePacksDec"),
	HX_CSTRING("setCountTextXClose"),
	HX_CSTRING("setCountTextXOpen"),
	HX_CSTRING("setJuicePackCount"),
	HX_CSTRING("setPiePackCount"),
	HX_CSTRING("setSeedCount"),
	HX_CSTRING("setseedItemCount"),
	HX_CSTRING("setKoiPondItemCount"),
	HX_CSTRING("setGreenHouseItemCount"),
	HX_CSTRING("setPieCount"),
	HX_CSTRING("setFishCount"),
	HX_CSTRING("setGreenHouseCount"),
	HX_CSTRING("setPieItemCount"),
	HX_CSTRING("setJuiceCount"),
	HX_CSTRING("setJuiceItemCount"),
	HX_CSTRING("setCoinCount"),
	HX_CSTRING("setWaterClientCount"),
	HX_CSTRING("setElectricClientCount"),
	HX_CSTRING("addSubtractCoins"),
	HX_CSTRING("displayCoinCount"),
	HX_CSTRING("displaySeedCount"),
	HX_CSTRING("displayItemCountText"),
	HX_CSTRING("decSeedCount"),
	HX_CSTRING("incSeedCount"),
	HX_CSTRING("decjuiceCount"),
	HX_CSTRING("decjuicePackCount"),
	HX_CSTRING("incJuiceCount"),
	HX_CSTRING("decPieCount"),
	HX_CSTRING("decFishCount"),
	HX_CSTRING("decGreenHouseCount"),
	HX_CSTRING("incPieCount"),
	HX_CSTRING("incfishCount"),
	HX_CSTRING("incGreenHouseCount"),
	HX_CSTRING("coinPaymentUtil"),
	HX_CSTRING("positionArrayItems"),
	HX_CSTRING("fillItemArray"),
	HX_CSTRING("registerEvents"),
	HX_CSTRING("registerItemEvents"),
	HX_CSTRING("unregisterItemEvents"),
	HX_CSTRING("unregisterEvents"),
	HX_CSTRING("selectItem"),
	HX_CSTRING("purchaseItem"),
	HX_CSTRING("openMarket"),
	HX_CSTRING("slideMarketOut"),
	HX_CSTRING("timedMarketClose"),
	HX_CSTRING("shiftItemsOpen"),
	HX_CSTRING("shiftItemsClose"),
	HX_CSTRING("replenishItems"),
	HX_CSTRING("waterBillCoinPayment"),
	HX_CSTRING("ElectricBillCoinPayment"),
	HX_CSTRING("destroySound"),
	HX_CSTRING("update"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Market_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Market_obj::__mClass,"__mClass");
};

#endif

Class Market_obj::__mClass;

void Market_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("Market"), hx::TCanCast< Market_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void Market_obj::__boot()
{
}

