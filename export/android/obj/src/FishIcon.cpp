#include <hxcpp.h>

#ifndef INCLUDED_FishIcon
#include <FishIcon.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_input_mouse_FlxMouse
#include <flixel/input/mouse/FlxMouse.h>
#endif
#ifndef INCLUDED_flixel_input_mouse_FlxMouseButton
#include <flixel/input/mouse/FlxMouseButton.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxInput
#include <flixel/interfaces/IFlxInput.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxPooled
#include <flixel/interfaces/IFlxPooled.h>
#endif
#ifndef INCLUDED_flixel_plugin_FlxPlugin
#include <flixel/plugin/FlxPlugin.h>
#endif
#ifndef INCLUDED_flixel_plugin_MouseEventManager
#include <flixel/plugin/MouseEventManager.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPoint
#include <flixel/util/FlxPoint.h>
#endif

Void FishIcon_obj::__construct(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y,bool dragType)
{
HX_STACK_FRAME("FishIcon","new",0x23b46dc3,"FishIcon.new","FishIcon.hx",11,0xbd93bb0d)
HX_STACK_THIS(this)
HX_STACK_ARG(__o_X,"X")
HX_STACK_ARG(__o_Y,"Y")
HX_STACK_ARG(dragType,"dragType")
Float X = __o_X.Default(0);
Float Y = __o_Y.Default(0);
{
	HX_STACK_LINE(18)
	this->homeY = (int)72;
	HX_STACK_LINE(17)
	this->homeX = (int)-99;
	HX_STACK_LINE(16)
	this->isMousedOver = false;
	HX_STACK_LINE(14)
	this->dragging = false;
	HX_STACK_LINE(22)
	super::__construct(X,Y,null());
	HX_STACK_LINE(23)
	this->dragType = dragType;
	HX_STACK_LINE(24)
	this->loadGraphic(HX_CSTRING("assets/images/fishIcon.png"),null(),null(),null(),null(),null());
	HX_STACK_LINE(25)
	this->set_x(this->homeX);
	HX_STACK_LINE(26)
	this->set_y(this->homeY);
	HX_STACK_LINE(27)
	if ((dragType)){
		HX_STACK_LINE(28)
		this->homeX = (int)66;
		HX_STACK_LINE(29)
		this->homeY = (int)72;
	}
	HX_STACK_LINE(31)
	::flixel::plugin::MouseEventManager_obj::add(hx::ObjectPtr<OBJ_>(this),this->onDown_dyn(),this->onUp_dyn(),null(),null(),null(),null(),null());
}
;
	return null();
}

//FishIcon_obj::~FishIcon_obj() { }

Dynamic FishIcon_obj::__CreateEmpty() { return  new FishIcon_obj; }
hx::ObjectPtr< FishIcon_obj > FishIcon_obj::__new(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y,bool dragType)
{  hx::ObjectPtr< FishIcon_obj > result = new FishIcon_obj();
	result->__construct(__o_X,__o_Y,dragType);
	return result;}

Dynamic FishIcon_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< FishIcon_obj > result = new FishIcon_obj();
	result->__construct(inArgs[0],inArgs[1],inArgs[2]);
	return result;}

Void FishIcon_obj::onDown( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("FishIcon","onDown",0x2587a39e,"FishIcon.onDown","FishIcon.hx",36,0xbd93bb0d)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(36)
		this->dragging = true;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(FishIcon_obj,onDown,(void))

Void FishIcon_obj::onUp( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("FishIcon","onUp",0x1adb8a97,"FishIcon.onUp","FishIcon.hx",40,0xbd93bb0d)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(42)
		this->dragging = false;
		HX_STACK_LINE(43)
		this->resetSeedPos();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(FishIcon_obj,onUp,(void))

Void FishIcon_obj::dragSeed( bool temp){
{
		HX_STACK_FRAME("FishIcon","dragSeed",0x67ebe342,"FishIcon.dragSeed","FishIcon.hx",49,0xbd93bb0d)
		HX_STACK_THIS(this)
		HX_STACK_ARG(temp,"temp")
		HX_STACK_LINE(49)
		if ((temp)){
			HX_STACK_LINE(51)
			this->set_x((::flixel::FlxG_obj::mouse->x - (int)5));
			HX_STACK_LINE(52)
			this->set_y((::flixel::FlxG_obj::mouse->y - (int)8));
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(FishIcon_obj,dragSeed,(void))

Void FishIcon_obj::resetSeedPos( ){
{
		HX_STACK_FRAME("FishIcon","resetSeedPos",0x64cd30d1,"FishIcon.resetSeedPos","FishIcon.hx",59,0xbd93bb0d)
		HX_STACK_THIS(this)
		HX_STACK_LINE(59)
		if ((this->dragType)){
			HX_STACK_LINE(60)
			this->set_x(this->homeX);
			HX_STACK_LINE(61)
			this->set_y(this->homeY);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(FishIcon_obj,resetSeedPos,(void))

bool FishIcon_obj::getDragging( ){
	HX_STACK_FRAME("FishIcon","getDragging",0x81a137e8,"FishIcon.getDragging","FishIcon.hx",67,0xbd93bb0d)
	HX_STACK_THIS(this)
	HX_STACK_LINE(67)
	return this->dragging;
}


HX_DEFINE_DYNAMIC_FUNC0(FishIcon_obj,getDragging,return )

Void FishIcon_obj::setDragging( bool val){
{
		HX_STACK_FRAME("FishIcon","setDragging",0x8c0e3ef4,"FishIcon.setDragging","FishIcon.hx",73,0xbd93bb0d)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(73)
		this->dragging = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(FishIcon_obj,setDragging,(void))

Void FishIcon_obj::checkMouseState( ){
{
		HX_STACK_FRAME("FishIcon","checkMouseState",0xd4fa7677,"FishIcon.checkMouseState","FishIcon.hx",79,0xbd93bb0d)
		HX_STACK_THIS(this)
		struct _Function_1_1{
			inline static bool Block( ){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","FishIcon.hx",79,0xbd93bb0d)
				{
					HX_STACK_LINE(79)
					::flixel::input::mouse::FlxMouseButton _this = ::flixel::FlxG_obj::mouse->_leftButton;		HX_STACK_VAR(_this,"_this");
					HX_STACK_LINE(79)
					return (bool((_this->current == (int)-1)) || bool((_this->current == (int)-2)));
				}
				return null();
			}
		};
		HX_STACK_LINE(79)
		if ((_Function_1_1::Block())){
			HX_STACK_LINE(81)
			this->dragging = false;
			HX_STACK_LINE(82)
			this->resetSeedPos();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(FishIcon_obj,checkMouseState,(void))

Void FishIcon_obj::unregisterFishEvent( ){
{
		HX_STACK_FRAME("FishIcon","unregisterFishEvent",0xff4f4a69,"FishIcon.unregisterFishEvent","FishIcon.hx",89,0xbd93bb0d)
		HX_STACK_THIS(this)
		HX_STACK_LINE(89)
		::flixel::plugin::MouseEventManager_obj::remove(hx::ObjectPtr<OBJ_>(this));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(FishIcon_obj,unregisterFishEvent,(void))

Void FishIcon_obj::registerFishEvent( ){
{
		HX_STACK_FRAME("FishIcon","registerFishEvent",0xf397b622,"FishIcon.registerFishEvent","FishIcon.hx",93,0xbd93bb0d)
		HX_STACK_THIS(this)
		HX_STACK_LINE(93)
		::flixel::plugin::MouseEventManager_obj::add(hx::ObjectPtr<OBJ_>(this),this->onDown_dyn(),this->onUp_dyn(),null(),null(),null(),null(),null());
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(FishIcon_obj,registerFishEvent,(void))

Void FishIcon_obj::update( ){
{
		HX_STACK_FRAME("FishIcon","update",0xc7952886,"FishIcon.update","FishIcon.hx",96,0xbd93bb0d)
		HX_STACK_THIS(this)
		HX_STACK_LINE(98)
		this->checkMouseState();
		HX_STACK_LINE(99)
		this->dragSeed(this->dragging);
		HX_STACK_LINE(100)
		this->super::update();
	}
return null();
}


Void FishIcon_obj::destroy( ){
{
		HX_STACK_FRAME("FishIcon","destroy",0x61a4b7dd,"FishIcon.destroy","FishIcon.hx",104,0xbd93bb0d)
		HX_STACK_THIS(this)
		HX_STACK_LINE(106)
		::flixel::plugin::MouseEventManager_obj::remove(hx::ObjectPtr<OBJ_>(this));
		HX_STACK_LINE(107)
		this->super::destroy();
	}
return null();
}



FishIcon_obj::FishIcon_obj()
{
}

Dynamic FishIcon_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"onUp") ) { return onUp_dyn(); }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"homeX") ) { return homeX; }
		if (HX_FIELD_EQ(inName,"homeY") ) { return homeY; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"onDown") ) { return onDown_dyn(); }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"dragging") ) { return dragging; }
		if (HX_FIELD_EQ(inName,"dragType") ) { return dragType; }
		if (HX_FIELD_EQ(inName,"dragSeed") ) { return dragSeed_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"getDragging") ) { return getDragging_dyn(); }
		if (HX_FIELD_EQ(inName,"setDragging") ) { return setDragging_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"isMousedOver") ) { return isMousedOver; }
		if (HX_FIELD_EQ(inName,"resetSeedPos") ) { return resetSeedPos_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"checkMouseState") ) { return checkMouseState_dyn(); }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"registerFishEvent") ) { return registerFishEvent_dyn(); }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"unregisterFishEvent") ) { return unregisterFishEvent_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic FishIcon_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"homeX") ) { homeX=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"homeY") ) { homeY=inValue.Cast< int >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"dragging") ) { dragging=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"dragType") ) { dragType=inValue.Cast< bool >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"isMousedOver") ) { isMousedOver=inValue.Cast< bool >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void FishIcon_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("dragging"));
	outFields->push(HX_CSTRING("dragType"));
	outFields->push(HX_CSTRING("isMousedOver"));
	outFields->push(HX_CSTRING("homeX"));
	outFields->push(HX_CSTRING("homeY"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsBool,(int)offsetof(FishIcon_obj,dragging),HX_CSTRING("dragging")},
	{hx::fsBool,(int)offsetof(FishIcon_obj,dragType),HX_CSTRING("dragType")},
	{hx::fsBool,(int)offsetof(FishIcon_obj,isMousedOver),HX_CSTRING("isMousedOver")},
	{hx::fsInt,(int)offsetof(FishIcon_obj,homeX),HX_CSTRING("homeX")},
	{hx::fsInt,(int)offsetof(FishIcon_obj,homeY),HX_CSTRING("homeY")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("dragging"),
	HX_CSTRING("dragType"),
	HX_CSTRING("isMousedOver"),
	HX_CSTRING("homeX"),
	HX_CSTRING("homeY"),
	HX_CSTRING("onDown"),
	HX_CSTRING("onUp"),
	HX_CSTRING("dragSeed"),
	HX_CSTRING("resetSeedPos"),
	HX_CSTRING("getDragging"),
	HX_CSTRING("setDragging"),
	HX_CSTRING("checkMouseState"),
	HX_CSTRING("unregisterFishEvent"),
	HX_CSTRING("registerFishEvent"),
	HX_CSTRING("update"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(FishIcon_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(FishIcon_obj::__mClass,"__mClass");
};

#endif

Class FishIcon_obj::__mClass;

void FishIcon_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("FishIcon"), hx::TCanCast< FishIcon_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void FishIcon_obj::__boot()
{
}

