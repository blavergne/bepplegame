#include <hxcpp.h>

#ifndef INCLUDED_Greenhouse
#include <Greenhouse.h>
#endif
#ifndef INCLUDED_MenuState
#include <MenuState.h>
#endif
#ifndef INCLUDED_OptionsScreen
#include <OptionsScreen.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxGame
#include <flixel/FlxGame.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_FlxState
#include <flixel/FlxState.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_plugin_FlxPlugin
#include <flixel/plugin/FlxPlugin.h>
#endif
#ifndef INCLUDED_flixel_plugin_MouseEventManager
#include <flixel/plugin/MouseEventManager.h>
#endif
#ifndef INCLUDED_flixel_ui_FlxButton
#include <flixel/ui/FlxButton.h>
#endif
#ifndef INCLUDED_flixel_ui_FlxTypedButton
#include <flixel/ui/FlxTypedButton.h>
#endif
#ifndef INCLUDED_openfl__v2_display_DisplayObject
#include <openfl/_v2/display/DisplayObject.h>
#endif
#ifndef INCLUDED_openfl__v2_display_DisplayObjectContainer
#include <openfl/_v2/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_openfl__v2_display_IBitmapDrawable
#include <openfl/_v2/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_openfl__v2_display_InteractiveObject
#include <openfl/_v2/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_openfl__v2_display_Sprite
#include <openfl/_v2/display/Sprite.h>
#endif
#ifndef INCLUDED_openfl__v2_events_EventDispatcher
#include <openfl/_v2/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__v2_events_IEventDispatcher
#include <openfl/_v2/events/IEventDispatcher.h>
#endif

Void OptionsScreen_obj::__construct()
{
HX_STACK_FRAME("OptionsScreen","new",0xeed71b3c,"OptionsScreen.new","OptionsScreen.hx",17,0x95291fb4)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(26)
	this->exitingGame = false;
	HX_STACK_LINE(25)
	this->gameRestart = false;
	HX_STACK_LINE(31)
	super::__construct(null());
	HX_STACK_LINE(32)
	::flixel::group::FlxGroup _g = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(32)
	this->optionsScreenGroup = _g;
	HX_STACK_LINE(33)
	::flixel::FlxSprite _g1 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
	HX_STACK_LINE(33)
	this->optionsScreenSprite = _g1;
	HX_STACK_LINE(34)
	::flixel::ui::FlxButton _g2 = ::flixel::ui::FlxButton_obj::__new((int)0,(int)0,HX_CSTRING("Exit Game"),null());		HX_STACK_VAR(_g2,"_g2");
	HX_STACK_LINE(34)
	this->exitButton = _g2;
	HX_STACK_LINE(35)
	::flixel::ui::FlxButton _g3 = ::flixel::ui::FlxButton_obj::__new((int)0,(int)0,HX_CSTRING("Reset"),null());		HX_STACK_VAR(_g3,"_g3");
	HX_STACK_LINE(35)
	this->resetButton = _g3;
	HX_STACK_LINE(36)
	::flixel::ui::FlxButton _g4 = ::flixel::ui::FlxButton_obj::__new((int)0,(int)0,HX_CSTRING("Main Menu"),null());		HX_STACK_VAR(_g4,"_g4");
	HX_STACK_LINE(36)
	this->mainMenuButton = _g4;
	HX_STACK_LINE(37)
	this->optionsScreenSprite->loadGraphic(HX_CSTRING("assets/images/optionsScreen.png"),false,null(),null(),null(),null());
	HX_STACK_LINE(38)
	this->optionsScreenSprite->set_x(220.);
	HX_STACK_LINE(39)
	this->optionsScreenSprite->set_y(140.);
	HX_STACK_LINE(40)
	this->resetButton->set_x((this->optionsScreenSprite->x + (int)63));
	HX_STACK_LINE(41)
	this->resetButton->set_y((this->optionsScreenSprite->y + (int)10));
	HX_STACK_LINE(42)
	this->mainMenuButton->set_x((this->optionsScreenSprite->x + (int)63));
	HX_STACK_LINE(43)
	this->mainMenuButton->set_y((this->optionsScreenSprite->y + (int)40));
	HX_STACK_LINE(44)
	this->exitButton->set_x((this->optionsScreenSprite->x + (int)63));
	HX_STACK_LINE(45)
	this->exitButton->set_y((this->optionsScreenSprite->y + (int)70));
	HX_STACK_LINE(46)
	this->optionsScreenSprite->set_alpha((int)0);
	HX_STACK_LINE(48)
	::flixel::plugin::MouseEventManager_obj::add(this->resetButton,this->resetGame_dyn(),null(),null(),null(),null(),null(),null());
	HX_STACK_LINE(49)
	::flixel::plugin::MouseEventManager_obj::add(this->mainMenuButton,this->returnToMain_dyn(),null(),null(),null(),null(),null(),null());
	HX_STACK_LINE(50)
	this->optionsScreenGroup->add(this->optionsScreenSprite);
	HX_STACK_LINE(51)
	this->add(this->optionsScreenGroup);
}
;
	return null();
}

//OptionsScreen_obj::~OptionsScreen_obj() { }

Dynamic OptionsScreen_obj::__CreateEmpty() { return  new OptionsScreen_obj; }
hx::ObjectPtr< OptionsScreen_obj > OptionsScreen_obj::__new()
{  hx::ObjectPtr< OptionsScreen_obj > result = new OptionsScreen_obj();
	result->__construct();
	return result;}

Dynamic OptionsScreen_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< OptionsScreen_obj > result = new OptionsScreen_obj();
	result->__construct();
	return result;}

bool OptionsScreen_obj::getGameRestart( ){
	HX_STACK_FRAME("OptionsScreen","getGameRestart",0x9789bbab,"OptionsScreen.getGameRestart","OptionsScreen.hx",56,0x95291fb4)
	HX_STACK_THIS(this)
	HX_STACK_LINE(56)
	return this->gameRestart;
}


HX_DEFINE_DYNAMIC_FUNC0(OptionsScreen_obj,getGameRestart,return )

bool OptionsScreen_obj::getGameEnd( ){
	HX_STACK_FRAME("OptionsScreen","getGameEnd",0xccfad9b7,"OptionsScreen.getGameEnd","OptionsScreen.hx",60,0x95291fb4)
	HX_STACK_THIS(this)
	HX_STACK_LINE(60)
	return this->exitingGame;
}


HX_DEFINE_DYNAMIC_FUNC0(OptionsScreen_obj,getGameEnd,return )

Void OptionsScreen_obj::setGameRestart( bool val){
{
		HX_STACK_FRAME("OptionsScreen","setGameRestart",0xb7a9a41f,"OptionsScreen.setGameRestart","OptionsScreen.hx",64,0x95291fb4)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(64)
		this->gameRestart = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(OptionsScreen_obj,setGameRestart,(void))

Void OptionsScreen_obj::menuVisible( int val){
{
		HX_STACK_FRAME("OptionsScreen","menuVisible",0xada7636f,"OptionsScreen.menuVisible","OptionsScreen.hx",68,0x95291fb4)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(68)
		if (((val == (int)0))){
			HX_STACK_LINE(69)
			this->optionsScreenSprite->set_alpha((int)0);
			HX_STACK_LINE(71)
			this->optionsScreenGroup->remove(this->resetButton,null());
			HX_STACK_LINE(72)
			this->optionsScreenGroup->remove(this->mainMenuButton,null());
		}
		else{
			HX_STACK_LINE(75)
			this->optionsScreenSprite->set_alpha((int)1);
			HX_STACK_LINE(77)
			this->optionsScreenGroup->add(this->resetButton);
			HX_STACK_LINE(78)
			this->optionsScreenGroup->add(this->mainMenuButton);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(OptionsScreen_obj,menuVisible,(void))

Void OptionsScreen_obj::exitGame( ::flixel::ui::FlxButton button){
{
		HX_STACK_FRAME("OptionsScreen","exitGame",0xf9f6d5d4,"OptionsScreen.exitGame","OptionsScreen.hx",83,0x95291fb4)
		HX_STACK_THIS(this)
		HX_STACK_ARG(button,"button")
		HX_STACK_LINE(83)
		this->exitingGame = true;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(OptionsScreen_obj,exitGame,(void))

Void OptionsScreen_obj::resetGame( ::flixel::ui::FlxButton button){
{
		HX_STACK_FRAME("OptionsScreen","resetGame",0xadd3d53d,"OptionsScreen.resetGame","OptionsScreen.hx",87,0x95291fb4)
		HX_STACK_THIS(this)
		HX_STACK_ARG(button,"button")
		HX_STACK_LINE(87)
		this->gameRestart = true;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(OptionsScreen_obj,resetGame,(void))

Void OptionsScreen_obj::returnToMain( ::flixel::ui::FlxButton button){
{
		HX_STACK_FRAME("OptionsScreen","returnToMain",0x2228d9c8,"OptionsScreen.returnToMain","OptionsScreen.hx",90,0x95291fb4)
		HX_STACK_THIS(this)
		HX_STACK_ARG(button,"button")
		HX_STACK_LINE(91)
		::Greenhouse_obj::greenHouseInPlay = false;
		HX_STACK_LINE(92)
		{
			HX_STACK_LINE(92)
			::flixel::FlxState State = ::MenuState_obj::__new(null());		HX_STACK_VAR(State,"State");
			HX_STACK_LINE(92)
			::flixel::FlxG_obj::game->_requestedState = State;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(OptionsScreen_obj,returnToMain,(void))

Void OptionsScreen_obj::destroy( ){
{
		HX_STACK_FRAME("OptionsScreen","destroy",0x639e15d6,"OptionsScreen.destroy","OptionsScreen.hx",95,0x95291fb4)
		HX_STACK_THIS(this)
		HX_STACK_LINE(96)
		this->destroy_dyn();
		HX_STACK_LINE(97)
		this->super::destroy();
	}
return null();
}



OptionsScreen_obj::OptionsScreen_obj()
{
}

void OptionsScreen_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(OptionsScreen);
	HX_MARK_MEMBER_NAME(optionsScreenGroup,"optionsScreenGroup");
	HX_MARK_MEMBER_NAME(optionsScreenSprite,"optionsScreenSprite");
	HX_MARK_MEMBER_NAME(exitButton,"exitButton");
	HX_MARK_MEMBER_NAME(resetButton,"resetButton");
	HX_MARK_MEMBER_NAME(mainMenuButton,"mainMenuButton");
	HX_MARK_MEMBER_NAME(gameRestart,"gameRestart");
	HX_MARK_MEMBER_NAME(exitingGame,"exitingGame");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void OptionsScreen_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(optionsScreenGroup,"optionsScreenGroup");
	HX_VISIT_MEMBER_NAME(optionsScreenSprite,"optionsScreenSprite");
	HX_VISIT_MEMBER_NAME(exitButton,"exitButton");
	HX_VISIT_MEMBER_NAME(resetButton,"resetButton");
	HX_VISIT_MEMBER_NAME(mainMenuButton,"mainMenuButton");
	HX_VISIT_MEMBER_NAME(gameRestart,"gameRestart");
	HX_VISIT_MEMBER_NAME(exitingGame,"exitingGame");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic OptionsScreen_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 7:
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"exitGame") ) { return exitGame_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"resetGame") ) { return resetGame_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"exitButton") ) { return exitButton; }
		if (HX_FIELD_EQ(inName,"getGameEnd") ) { return getGameEnd_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"resetButton") ) { return resetButton; }
		if (HX_FIELD_EQ(inName,"gameRestart") ) { return gameRestart; }
		if (HX_FIELD_EQ(inName,"exitingGame") ) { return exitingGame; }
		if (HX_FIELD_EQ(inName,"menuVisible") ) { return menuVisible_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"returnToMain") ) { return returnToMain_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"mainMenuButton") ) { return mainMenuButton; }
		if (HX_FIELD_EQ(inName,"getGameRestart") ) { return getGameRestart_dyn(); }
		if (HX_FIELD_EQ(inName,"setGameRestart") ) { return setGameRestart_dyn(); }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"optionsScreenGroup") ) { return optionsScreenGroup; }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"optionsScreenSprite") ) { return optionsScreenSprite; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic OptionsScreen_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 10:
		if (HX_FIELD_EQ(inName,"exitButton") ) { exitButton=inValue.Cast< ::flixel::ui::FlxButton >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"resetButton") ) { resetButton=inValue.Cast< ::flixel::ui::FlxButton >(); return inValue; }
		if (HX_FIELD_EQ(inName,"gameRestart") ) { gameRestart=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"exitingGame") ) { exitingGame=inValue.Cast< bool >(); return inValue; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"mainMenuButton") ) { mainMenuButton=inValue.Cast< ::flixel::ui::FlxButton >(); return inValue; }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"optionsScreenGroup") ) { optionsScreenGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"optionsScreenSprite") ) { optionsScreenSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void OptionsScreen_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("optionsScreenGroup"));
	outFields->push(HX_CSTRING("optionsScreenSprite"));
	outFields->push(HX_CSTRING("exitButton"));
	outFields->push(HX_CSTRING("resetButton"));
	outFields->push(HX_CSTRING("mainMenuButton"));
	outFields->push(HX_CSTRING("gameRestart"));
	outFields->push(HX_CSTRING("exitingGame"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(OptionsScreen_obj,optionsScreenGroup),HX_CSTRING("optionsScreenGroup")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(OptionsScreen_obj,optionsScreenSprite),HX_CSTRING("optionsScreenSprite")},
	{hx::fsObject /*::flixel::ui::FlxButton*/ ,(int)offsetof(OptionsScreen_obj,exitButton),HX_CSTRING("exitButton")},
	{hx::fsObject /*::flixel::ui::FlxButton*/ ,(int)offsetof(OptionsScreen_obj,resetButton),HX_CSTRING("resetButton")},
	{hx::fsObject /*::flixel::ui::FlxButton*/ ,(int)offsetof(OptionsScreen_obj,mainMenuButton),HX_CSTRING("mainMenuButton")},
	{hx::fsBool,(int)offsetof(OptionsScreen_obj,gameRestart),HX_CSTRING("gameRestart")},
	{hx::fsBool,(int)offsetof(OptionsScreen_obj,exitingGame),HX_CSTRING("exitingGame")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("optionsScreenGroup"),
	HX_CSTRING("optionsScreenSprite"),
	HX_CSTRING("exitButton"),
	HX_CSTRING("resetButton"),
	HX_CSTRING("mainMenuButton"),
	HX_CSTRING("gameRestart"),
	HX_CSTRING("exitingGame"),
	HX_CSTRING("getGameRestart"),
	HX_CSTRING("getGameEnd"),
	HX_CSTRING("setGameRestart"),
	HX_CSTRING("menuVisible"),
	HX_CSTRING("exitGame"),
	HX_CSTRING("resetGame"),
	HX_CSTRING("returnToMain"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(OptionsScreen_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(OptionsScreen_obj::__mClass,"__mClass");
};

#endif

Class OptionsScreen_obj::__mClass;

void OptionsScreen_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("OptionsScreen"), hx::TCanCast< OptionsScreen_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void OptionsScreen_obj::__boot()
{
}

