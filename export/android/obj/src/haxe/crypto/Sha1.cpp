#include <hxcpp.h>

#ifndef INCLUDED_haxe_crypto_Sha1
#include <haxe/crypto/Sha1.h>
#endif
namespace haxe{
namespace crypto{

Void Sha1_obj::__construct()
{
HX_STACK_FRAME("haxe.crypto.Sha1","new",0xdb44b09c,"haxe.crypto.Sha1.new","C:\\HaxeToolkit\\haxe\\std/haxe/crypto/Sha1.hx",55,0x2f3077fb)
HX_STACK_THIS(this)
{
}
;
	return null();
}

//Sha1_obj::~Sha1_obj() { }

Dynamic Sha1_obj::__CreateEmpty() { return  new Sha1_obj; }
hx::ObjectPtr< Sha1_obj > Sha1_obj::__new()
{  hx::ObjectPtr< Sha1_obj > result = new Sha1_obj();
	result->__construct();
	return result;}

Dynamic Sha1_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Sha1_obj > result = new Sha1_obj();
	result->__construct();
	return result;}

Array< int > Sha1_obj::doEncode( Array< int > x){
	HX_STACK_FRAME("haxe.crypto.Sha1","doEncode",0xfb483e45,"haxe.crypto.Sha1.doEncode","C:\\HaxeToolkit\\haxe\\std/haxe/crypto/Sha1.hx",58,0x2f3077fb)
	HX_STACK_THIS(this)
	HX_STACK_ARG(x,"x")
	HX_STACK_LINE(59)
	Array< int > w = Array_obj< int >::__new();		HX_STACK_VAR(w,"w");
	HX_STACK_LINE(61)
	int a = (int)1732584193;		HX_STACK_VAR(a,"a");
	HX_STACK_LINE(62)
	int b = (int)-271733879;		HX_STACK_VAR(b,"b");
	HX_STACK_LINE(63)
	int c = (int)-1732584194;		HX_STACK_VAR(c,"c");
	HX_STACK_LINE(64)
	int d = (int)271733878;		HX_STACK_VAR(d,"d");
	HX_STACK_LINE(65)
	int e = (int)-1009589776;		HX_STACK_VAR(e,"e");
	HX_STACK_LINE(67)
	int i = (int)0;		HX_STACK_VAR(i,"i");
	HX_STACK_LINE(68)
	while((true)){
		HX_STACK_LINE(68)
		if ((!(((i < x->length))))){
			HX_STACK_LINE(68)
			break;
		}
		HX_STACK_LINE(69)
		int olda = a;		HX_STACK_VAR(olda,"olda");
		HX_STACK_LINE(70)
		int oldb = b;		HX_STACK_VAR(oldb,"oldb");
		HX_STACK_LINE(71)
		int oldc = c;		HX_STACK_VAR(oldc,"oldc");
		HX_STACK_LINE(72)
		int oldd = d;		HX_STACK_VAR(oldd,"oldd");
		HX_STACK_LINE(73)
		int olde = e;		HX_STACK_VAR(olde,"olde");
		HX_STACK_LINE(75)
		int j = (int)0;		HX_STACK_VAR(j,"j");
		HX_STACK_LINE(76)
		while((true)){
			HX_STACK_LINE(76)
			if ((!(((j < (int)80))))){
				HX_STACK_LINE(76)
				break;
			}
			HX_STACK_LINE(77)
			if (((j < (int)16))){
				HX_STACK_LINE(78)
				w[j] = x->__get((i + j));
			}
			else{
				HX_STACK_LINE(80)
				int num = (int((int((int(w->__get((j - (int)3))) ^ int(w->__get((j - (int)8))))) ^ int(w->__get((j - (int)14))))) ^ int(w->__get((j - (int)16))));		HX_STACK_VAR(num,"num");
				HX_STACK_LINE(80)
				w[j] = (int((int(num) << int((int)1))) | int(hx::UShr(num,(int)31)));
			}
			HX_STACK_LINE(81)
			int _g = this->ft(j,b,c,d);		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(81)
			int _g1 = (((int((int(a) << int((int)5))) | int(hx::UShr(a,(int)27)))) + _g);		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(81)
			int _g2 = (_g1 + e);		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(81)
			int _g3 = (_g2 + w->__get(j));		HX_STACK_VAR(_g3,"_g3");
			HX_STACK_LINE(81)
			int _g4 = this->kt(j);		HX_STACK_VAR(_g4,"_g4");
			HX_STACK_LINE(81)
			int t = (_g3 + _g4);		HX_STACK_VAR(t,"t");
			HX_STACK_LINE(82)
			e = d;
			HX_STACK_LINE(83)
			d = c;
			HX_STACK_LINE(84)
			c = (int((int(b) << int((int)30))) | int(hx::UShr(b,(int)2)));
			HX_STACK_LINE(85)
			b = a;
			HX_STACK_LINE(86)
			a = t;
			HX_STACK_LINE(87)
			(j)++;
		}
		HX_STACK_LINE(89)
		hx::AddEq(a,olda);
		HX_STACK_LINE(90)
		hx::AddEq(b,oldb);
		HX_STACK_LINE(91)
		hx::AddEq(c,oldc);
		HX_STACK_LINE(92)
		hx::AddEq(d,oldd);
		HX_STACK_LINE(93)
		hx::AddEq(e,olde);
		HX_STACK_LINE(94)
		hx::AddEq(i,(int)16);
	}
	HX_STACK_LINE(96)
	return Array_obj< int >::__new().Add(a).Add(b).Add(c).Add(d).Add(e);
}


HX_DEFINE_DYNAMIC_FUNC1(Sha1_obj,doEncode,return )

int Sha1_obj::ft( int t,int b,int c,int d){
	HX_STACK_FRAME("haxe.crypto.Sha1","ft",0x7aab5492,"haxe.crypto.Sha1.ft","C:\\HaxeToolkit\\haxe\\std/haxe/crypto/Sha1.hx",148,0x2f3077fb)
	HX_STACK_THIS(this)
	HX_STACK_ARG(t,"t")
	HX_STACK_ARG(b,"b")
	HX_STACK_ARG(c,"c")
	HX_STACK_ARG(d,"d")
	HX_STACK_LINE(149)
	if (((t < (int)20))){
		HX_STACK_LINE(149)
		return (int((int(b) & int(c))) | int((int(~(int)(b)) & int(d))));
	}
	HX_STACK_LINE(150)
	if (((t < (int)40))){
		HX_STACK_LINE(150)
		return (int((int(b) ^ int(c))) ^ int(d));
	}
	HX_STACK_LINE(151)
	if (((t < (int)60))){
		HX_STACK_LINE(151)
		return (int((int((int(b) & int(c))) | int((int(b) & int(d))))) | int((int(c) & int(d))));
	}
	HX_STACK_LINE(152)
	return (int((int(b) ^ int(c))) ^ int(d));
}


HX_DEFINE_DYNAMIC_FUNC4(Sha1_obj,ft,return )

int Sha1_obj::kt( int t){
	HX_STACK_FRAME("haxe.crypto.Sha1","kt",0x7aab58ed,"haxe.crypto.Sha1.kt","C:\\HaxeToolkit\\haxe\\std/haxe/crypto/Sha1.hx",158,0x2f3077fb)
	HX_STACK_THIS(this)
	HX_STACK_ARG(t,"t")
	HX_STACK_LINE(159)
	if (((t < (int)20))){
		HX_STACK_LINE(160)
		return (int)1518500249;
	}
	HX_STACK_LINE(161)
	if (((t < (int)40))){
		HX_STACK_LINE(162)
		return (int)1859775393;
	}
	HX_STACK_LINE(163)
	if (((t < (int)60))){
		HX_STACK_LINE(164)
		return (int)-1894007588;
	}
	HX_STACK_LINE(165)
	return (int)-899497514;
}


HX_DEFINE_DYNAMIC_FUNC1(Sha1_obj,kt,return )

::String Sha1_obj::hex( Array< int > a){
	HX_STACK_FRAME("haxe.crypto.Sha1","hex",0xdb402317,"haxe.crypto.Sha1.hex","C:\\HaxeToolkit\\haxe\\std/haxe/crypto/Sha1.hx",168,0x2f3077fb)
	HX_STACK_THIS(this)
	HX_STACK_ARG(a,"a")
	HX_STACK_LINE(169)
	::String str = HX_CSTRING("");		HX_STACK_VAR(str,"str");
	HX_STACK_LINE(170)
	::String hex_chr = HX_CSTRING("0123456789abcdef");		HX_STACK_VAR(hex_chr,"hex_chr");
	HX_STACK_LINE(171)
	{
		HX_STACK_LINE(171)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(171)
		while((true)){
			HX_STACK_LINE(171)
			if ((!(((_g < a->length))))){
				HX_STACK_LINE(171)
				break;
			}
			HX_STACK_LINE(171)
			int num = a->__get(_g);		HX_STACK_VAR(num,"num");
			HX_STACK_LINE(171)
			++(_g);
			HX_STACK_LINE(172)
			int j = (int)7;		HX_STACK_VAR(j,"j");
			HX_STACK_LINE(173)
			while((true)){
				HX_STACK_LINE(173)
				if ((!(((j >= (int)0))))){
					HX_STACK_LINE(173)
					break;
				}
				HX_STACK_LINE(174)
				::String _g1 = hex_chr.charAt((int(hx::UShr(num,((int(j) << int((int)2))))) & int((int)15)));		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(174)
				hx::AddEq(str,_g1);
				HX_STACK_LINE(175)
				(j)--;
			}
		}
	}
	HX_STACK_LINE(178)
	return str;
}


HX_DEFINE_DYNAMIC_FUNC1(Sha1_obj,hex,return )

::String Sha1_obj::encode( ::String s){
	HX_STACK_FRAME("haxe.crypto.Sha1","encode",0xcc67df5a,"haxe.crypto.Sha1.encode","C:\\HaxeToolkit\\haxe\\std/haxe/crypto/Sha1.hx",26,0x2f3077fb)
	HX_STACK_ARG(s,"s")
	HX_STACK_LINE(30)
	::haxe::crypto::Sha1 sh = ::haxe::crypto::Sha1_obj::__new();		HX_STACK_VAR(sh,"sh");
	HX_STACK_LINE(31)
	Array< int > _g = ::haxe::crypto::Sha1_obj::str2blks(s);		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(31)
	Array< int > h = sh->doEncode(_g);		HX_STACK_VAR(h,"h");
	HX_STACK_LINE(32)
	return sh->hex(h);
}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(Sha1_obj,encode,return )

Array< int > Sha1_obj::str2blks( ::String s){
	HX_STACK_FRAME("haxe.crypto.Sha1","str2blks",0x96813717,"haxe.crypto.Sha1.str2blks","C:\\HaxeToolkit\\haxe\\std/haxe/crypto/Sha1.hx",104,0x2f3077fb)
	HX_STACK_ARG(s,"s")
	HX_STACK_LINE(105)
	int nblk = (((int((s.length + (int)8)) >> int((int)6))) + (int)1);		HX_STACK_VAR(nblk,"nblk");
	HX_STACK_LINE(106)
	Array< int > blks = Array_obj< int >::__new();		HX_STACK_VAR(blks,"blks");
	HX_STACK_LINE(108)
	{
		HX_STACK_LINE(108)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(108)
		int _g = (nblk * (int)16);		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(108)
		while((true)){
			HX_STACK_LINE(108)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(108)
				break;
			}
			HX_STACK_LINE(108)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(109)
			blks[i] = (int)0;
		}
	}
	HX_STACK_LINE(110)
	{
		HX_STACK_LINE(110)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(110)
		int _g = s.length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(110)
		while((true)){
			HX_STACK_LINE(110)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(110)
				break;
			}
			HX_STACK_LINE(110)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(111)
			int p = (int(i) >> int((int)2));		HX_STACK_VAR(p,"p");
			HX_STACK_LINE(112)
			Dynamic _g2 = s.charCodeAt(i);		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(112)
			hx::OrEq(blks[p],(int(_g2) << int(((int)24 - ((int(((int(i) & int((int)3)))) << int((int)3)))))));
		}
	}
	HX_STACK_LINE(114)
	int i = s.length;		HX_STACK_VAR(i,"i");
	HX_STACK_LINE(115)
	int p = (int(i) >> int((int)2));		HX_STACK_VAR(p,"p");
	HX_STACK_LINE(116)
	hx::OrEq(blks[p],(int((int)128) << int(((int)24 - ((int(((int(i) & int((int)3)))) << int((int)3)))))));
	HX_STACK_LINE(117)
	blks[((nblk * (int)16) - (int)1)] = (s.length * (int)8);
	HX_STACK_LINE(118)
	return blks;
}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(Sha1_obj,str2blks,return )


Sha1_obj::Sha1_obj()
{
}

Dynamic Sha1_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 2:
		if (HX_FIELD_EQ(inName,"ft") ) { return ft_dyn(); }
		if (HX_FIELD_EQ(inName,"kt") ) { return kt_dyn(); }
		break;
	case 3:
		if (HX_FIELD_EQ(inName,"hex") ) { return hex_dyn(); }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"encode") ) { return encode_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"str2blks") ) { return str2blks_dyn(); }
		if (HX_FIELD_EQ(inName,"doEncode") ) { return doEncode_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Sha1_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	return super::__SetField(inName,inValue,inCallProp);
}

void Sha1_obj::__GetFields(Array< ::String> &outFields)
{
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("encode"),
	HX_CSTRING("str2blks"),
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo *sMemberStorageInfo = 0;
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("doEncode"),
	HX_CSTRING("ft"),
	HX_CSTRING("kt"),
	HX_CSTRING("hex"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Sha1_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Sha1_obj::__mClass,"__mClass");
};

#endif

Class Sha1_obj::__mClass;

void Sha1_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("haxe.crypto.Sha1"), hx::TCanCast< Sha1_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void Sha1_obj::__boot()
{
}

} // end namespace haxe
} // end namespace crypto
