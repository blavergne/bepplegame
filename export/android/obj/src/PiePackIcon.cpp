#include <hxcpp.h>

#ifndef INCLUDED_PiePackIcon
#include <PiePackIcon.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif

Void PiePackIcon_obj::__construct()
{
HX_STACK_FRAME("PiePackIcon","new",0x0f12bb30,"PiePackIcon.new","PiePackIcon.hx",18,0xa314ba40)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(19)
	super::__construct(null());
	HX_STACK_LINE(20)
	::flixel::group::FlxGroup _g = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(20)
	this->piePackGroup = _g;
	HX_STACK_LINE(21)
	::flixel::FlxSprite _g1 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
	HX_STACK_LINE(21)
	this->piePackSprite = _g1;
	HX_STACK_LINE(22)
	this->piePackSprite->loadGraphic(HX_CSTRING("assets/images/packOfPiesIcon.png"),false,null(),null(),null(),null());
	HX_STACK_LINE(23)
	this->piePackSprite->set_x((int)207);
	HX_STACK_LINE(24)
	this->piePackSprite->set_y((int)36);
	HX_STACK_LINE(25)
	::PiePackIcon_obj::piePackPosX = this->piePackSprite->x;
	HX_STACK_LINE(26)
	::PiePackIcon_obj::piePackPosY = this->piePackSprite->y;
	HX_STACK_LINE(27)
	this->piePackGroup->add(this->piePackSprite);
	HX_STACK_LINE(28)
	this->add(this->piePackGroup);
}
;
	return null();
}

//PiePackIcon_obj::~PiePackIcon_obj() { }

Dynamic PiePackIcon_obj::__CreateEmpty() { return  new PiePackIcon_obj; }
hx::ObjectPtr< PiePackIcon_obj > PiePackIcon_obj::__new()
{  hx::ObjectPtr< PiePackIcon_obj > result = new PiePackIcon_obj();
	result->__construct();
	return result;}

Dynamic PiePackIcon_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< PiePackIcon_obj > result = new PiePackIcon_obj();
	result->__construct();
	return result;}

::flixel::FlxSprite PiePackIcon_obj::getPiePackSprite( ){
	HX_STACK_FRAME("PiePackIcon","getPiePackSprite",0xe809bfe4,"PiePackIcon.getPiePackSprite","PiePackIcon.hx",32,0xa314ba40)
	HX_STACK_THIS(this)
	HX_STACK_LINE(32)
	return this->piePackSprite;
}


HX_DEFINE_DYNAMIC_FUNC0(PiePackIcon_obj,getPiePackSprite,return )

Float PiePackIcon_obj::piePackPosX;

Float PiePackIcon_obj::piePackPosY;


PiePackIcon_obj::PiePackIcon_obj()
{
}

void PiePackIcon_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(PiePackIcon);
	HX_MARK_MEMBER_NAME(piePackGroup,"piePackGroup");
	HX_MARK_MEMBER_NAME(piePackSprite,"piePackSprite");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void PiePackIcon_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(piePackGroup,"piePackGroup");
	HX_VISIT_MEMBER_NAME(piePackSprite,"piePackSprite");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic PiePackIcon_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 11:
		if (HX_FIELD_EQ(inName,"piePackPosX") ) { return piePackPosX; }
		if (HX_FIELD_EQ(inName,"piePackPosY") ) { return piePackPosY; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"piePackGroup") ) { return piePackGroup; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"piePackSprite") ) { return piePackSprite; }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"getPiePackSprite") ) { return getPiePackSprite_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic PiePackIcon_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 11:
		if (HX_FIELD_EQ(inName,"piePackPosX") ) { piePackPosX=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"piePackPosY") ) { piePackPosY=inValue.Cast< Float >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"piePackGroup") ) { piePackGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"piePackSprite") ) { piePackSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void PiePackIcon_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("piePackGroup"));
	outFields->push(HX_CSTRING("piePackSprite"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("piePackPosX"),
	HX_CSTRING("piePackPosY"),
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(PiePackIcon_obj,piePackGroup),HX_CSTRING("piePackGroup")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(PiePackIcon_obj,piePackSprite),HX_CSTRING("piePackSprite")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("piePackGroup"),
	HX_CSTRING("piePackSprite"),
	HX_CSTRING("getPiePackSprite"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(PiePackIcon_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(PiePackIcon_obj::piePackPosX,"piePackPosX");
	HX_MARK_MEMBER_NAME(PiePackIcon_obj::piePackPosY,"piePackPosY");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(PiePackIcon_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(PiePackIcon_obj::piePackPosX,"piePackPosX");
	HX_VISIT_MEMBER_NAME(PiePackIcon_obj::piePackPosY,"piePackPosY");
};

#endif

Class PiePackIcon_obj::__mClass;

void PiePackIcon_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("PiePackIcon"), hx::TCanCast< PiePackIcon_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void PiePackIcon_obj::__boot()
{
}

