#include <hxcpp.h>

#ifndef INCLUDED_Greenhouse
#include <Greenhouse.h>
#endif
#ifndef INCLUDED_Tile
#include <Tile.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_system_FlxSound
#include <flixel/system/FlxSound.h>
#endif
#ifndef INCLUDED_flixel_system_frontEnds_SoundFrontEnd
#include <flixel/system/frontEnds/SoundFrontEnd.h>
#endif
#ifndef INCLUDED_flixel_util_FlxDestroyUtil
#include <flixel/util/FlxDestroyUtil.h>
#endif

Void Greenhouse_obj::__construct(::Tile tileSprite)
{
HX_STACK_FRAME("Greenhouse","new",0x3790358f,"Greenhouse.new","Greenhouse.hx",24,0x7e86f0c1)
HX_STACK_THIS(this)
HX_STACK_ARG(tileSprite,"tileSprite")
{
	HX_STACK_LINE(25)
	super::__construct(null());
	HX_STACK_LINE(26)
	::Greenhouse_obj::greenHouseInPlay = true;
	HX_STACK_LINE(27)
	::flixel::group::FlxGroup _g = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(27)
	this->greenHouseGroup = _g;
	HX_STACK_LINE(28)
	::flixel::FlxSprite _g1 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
	HX_STACK_LINE(28)
	this->greenHouseSprite = _g1;
	HX_STACK_LINE(29)
	::flixel::FlxSprite _g2 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g2,"_g2");
	HX_STACK_LINE(29)
	this->greenHouseShrub0 = _g2;
	HX_STACK_LINE(30)
	::flixel::FlxSprite _g3 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g3,"_g3");
	HX_STACK_LINE(30)
	this->greenHouseTree = _g3;
	HX_STACK_LINE(31)
	this->greenHouseSprite->loadGraphic(HX_CSTRING("assets/images/greenHouseAnimation.png"),true,(int)81,(int)69,null(),null());
	HX_STACK_LINE(32)
	this->greenHouseShrub0->loadGraphic(HX_CSTRING("assets/images/greenHouseShrub.png"),false,null(),null(),null(),null());
	HX_STACK_LINE(33)
	this->greenHouseTree->loadGraphic(HX_CSTRING("assets/images/greenHouseTreeAnimation.png"),true,(int)68,(int)83,null(),null());
	HX_STACK_LINE(34)
	this->greenHouseTree->animation->add(HX_CSTRING("chimesAnimate"),Array_obj< int >::__new().Add((int)0).Add((int)1).Add((int)2).Add((int)3),(int)2,true);
	HX_STACK_LINE(35)
	this->greenHouseTree->animation->play(HX_CSTRING("chimesAnimate"),null(),null());
	HX_STACK_LINE(36)
	::flixel::system::FlxSound _g4 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/greenHouseWindChimeSound.wav"),.1,true,null(),null(),null(),null());		HX_STACK_VAR(_g4,"_g4");
	HX_STACK_LINE(36)
	this->windChimeSound = _g4;
	HX_STACK_LINE(37)
	this->windChimeSound->play(null());
	HX_STACK_LINE(38)
	this->greenHouseSprite->set_x((tileSprite->x + (int)60));
	HX_STACK_LINE(39)
	this->greenHouseSprite->set_y(tileSprite->y);
	HX_STACK_LINE(40)
	this->greenHouseShrub0->set_x((this->greenHouseSprite->x + (int)65));
	HX_STACK_LINE(41)
	this->greenHouseShrub0->set_y((this->greenHouseSprite->y + (int)40));
	HX_STACK_LINE(42)
	this->greenHouseTree->set_x((this->greenHouseSprite->x - (int)35));
	HX_STACK_LINE(43)
	this->greenHouseTree->set_y((this->greenHouseSprite->y - (int)25));
	HX_STACK_LINE(44)
	this->greenHouseGroup->add(this->greenHouseSprite);
	HX_STACK_LINE(45)
	this->greenHouseGroup->add(this->greenHouseTree);
	HX_STACK_LINE(46)
	this->greenHouseGroup->add(this->greenHouseShrub0);
	HX_STACK_LINE(47)
	this->add(this->greenHouseGroup);
}
;
	return null();
}

//Greenhouse_obj::~Greenhouse_obj() { }

Dynamic Greenhouse_obj::__CreateEmpty() { return  new Greenhouse_obj; }
hx::ObjectPtr< Greenhouse_obj > Greenhouse_obj::__new(::Tile tileSprite)
{  hx::ObjectPtr< Greenhouse_obj > result = new Greenhouse_obj();
	result->__construct(tileSprite);
	return result;}

Dynamic Greenhouse_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Greenhouse_obj > result = new Greenhouse_obj();
	result->__construct(inArgs[0]);
	return result;}

Void Greenhouse_obj::setGreenHouseInPlay( bool val){
{
		HX_STACK_FRAME("Greenhouse","setGreenHouseInPlay",0x8ec24127,"Greenhouse.setGreenHouseInPlay","Greenhouse.hx",52,0x7e86f0c1)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(52)
		::Greenhouse_obj::greenHouseInPlay = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Greenhouse_obj,setGreenHouseInPlay,(void))

Void Greenhouse_obj::update( ){
{
		HX_STACK_FRAME("Greenhouse","update",0x5ee7483a,"Greenhouse.update","Greenhouse.hx",56,0x7e86f0c1)
		HX_STACK_THIS(this)
		HX_STACK_LINE(56)
		this->super::update();
	}
return null();
}


Void Greenhouse_obj::destroy( ){
{
		HX_STACK_FRAME("Greenhouse","destroy",0x322e55a9,"Greenhouse.destroy","Greenhouse.hx",59,0x7e86f0c1)
		HX_STACK_THIS(this)
		HX_STACK_LINE(60)
		::flixel::system::FlxSound _g = ::flixel::util::FlxDestroyUtil_obj::destroy(this->windChimeSound);		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(60)
		this->windChimeSound = _g;
		HX_STACK_LINE(61)
		this->greenHouseGroup = null();
		HX_STACK_LINE(62)
		this->greenHouseSprite = null();
		HX_STACK_LINE(63)
		this->greenHouseTree = null();
		HX_STACK_LINE(64)
		this->windChimeSound = null();
		HX_STACK_LINE(65)
		this->greenHouseShrub0 = null();
		HX_STACK_LINE(66)
		this->super::destroy();
	}
return null();
}


bool Greenhouse_obj::greenHouseInPlay;


Greenhouse_obj::Greenhouse_obj()
{
}

void Greenhouse_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Greenhouse);
	HX_MARK_MEMBER_NAME(greenHouseGroup,"greenHouseGroup");
	HX_MARK_MEMBER_NAME(greenHouseSprite,"greenHouseSprite");
	HX_MARK_MEMBER_NAME(greenHouseShrub0,"greenHouseShrub0");
	HX_MARK_MEMBER_NAME(greenHouseTree,"greenHouseTree");
	HX_MARK_MEMBER_NAME(windChimeSound,"windChimeSound");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void Greenhouse_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(greenHouseGroup,"greenHouseGroup");
	HX_VISIT_MEMBER_NAME(greenHouseSprite,"greenHouseSprite");
	HX_VISIT_MEMBER_NAME(greenHouseShrub0,"greenHouseShrub0");
	HX_VISIT_MEMBER_NAME(greenHouseTree,"greenHouseTree");
	HX_VISIT_MEMBER_NAME(windChimeSound,"windChimeSound");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic Greenhouse_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"greenHouseTree") ) { return greenHouseTree; }
		if (HX_FIELD_EQ(inName,"windChimeSound") ) { return windChimeSound; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"greenHouseGroup") ) { return greenHouseGroup; }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"greenHouseInPlay") ) { return greenHouseInPlay; }
		if (HX_FIELD_EQ(inName,"greenHouseSprite") ) { return greenHouseSprite; }
		if (HX_FIELD_EQ(inName,"greenHouseShrub0") ) { return greenHouseShrub0; }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"setGreenHouseInPlay") ) { return setGreenHouseInPlay_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Greenhouse_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 14:
		if (HX_FIELD_EQ(inName,"greenHouseTree") ) { greenHouseTree=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"windChimeSound") ) { windChimeSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"greenHouseGroup") ) { greenHouseGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"greenHouseInPlay") ) { greenHouseInPlay=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"greenHouseSprite") ) { greenHouseSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"greenHouseShrub0") ) { greenHouseShrub0=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Greenhouse_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("greenHouseGroup"));
	outFields->push(HX_CSTRING("greenHouseSprite"));
	outFields->push(HX_CSTRING("greenHouseShrub0"));
	outFields->push(HX_CSTRING("greenHouseTree"));
	outFields->push(HX_CSTRING("windChimeSound"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("greenHouseInPlay"),
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(Greenhouse_obj,greenHouseGroup),HX_CSTRING("greenHouseGroup")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Greenhouse_obj,greenHouseSprite),HX_CSTRING("greenHouseSprite")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Greenhouse_obj,greenHouseShrub0),HX_CSTRING("greenHouseShrub0")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Greenhouse_obj,greenHouseTree),HX_CSTRING("greenHouseTree")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(Greenhouse_obj,windChimeSound),HX_CSTRING("windChimeSound")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("greenHouseGroup"),
	HX_CSTRING("greenHouseSprite"),
	HX_CSTRING("greenHouseShrub0"),
	HX_CSTRING("greenHouseTree"),
	HX_CSTRING("windChimeSound"),
	HX_CSTRING("setGreenHouseInPlay"),
	HX_CSTRING("update"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Greenhouse_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(Greenhouse_obj::greenHouseInPlay,"greenHouseInPlay");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Greenhouse_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(Greenhouse_obj::greenHouseInPlay,"greenHouseInPlay");
};

#endif

Class Greenhouse_obj::__mClass;

void Greenhouse_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("Greenhouse"), hx::TCanCast< Greenhouse_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void Greenhouse_obj::__boot()
{
	greenHouseInPlay= false;
}

