#include <hxcpp.h>

#ifndef INCLUDED_Island
#include <Island.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_util_FlxRandom
#include <flixel/util/FlxRandom.h>
#endif

Void Island_obj::__construct()
{
HX_STACK_FRAME("Island","new",0x28a7d267,"Island.new","Island.hx",33,0xc47cb6e9)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(34)
	super::__construct(null());
	HX_STACK_LINE(35)
	this->init();
}
;
	return null();
}

//Island_obj::~Island_obj() { }

Dynamic Island_obj::__CreateEmpty() { return  new Island_obj; }
hx::ObjectPtr< Island_obj > Island_obj::__new()
{  hx::ObjectPtr< Island_obj > result = new Island_obj();
	result->__construct();
	return result;}

Dynamic Island_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Island_obj > result = new Island_obj();
	result->__construct();
	return result;}

Void Island_obj::init( ){
{
		HX_STACK_FRAME("Island","init",0x66e8ff29,"Island.init","Island.hx",38,0xc47cb6e9)
		HX_STACK_THIS(this)
		HX_STACK_LINE(40)
		::flixel::group::FlxGroup _g = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(40)
		this->islandGroup = _g;
		HX_STACK_LINE(41)
		Array< ::Dynamic > _g1 = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(41)
		this->islandArray = _g1;
		HX_STACK_LINE(42)
		::flixel::FlxSprite _g2 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(42)
		this->islandA = _g2;
		HX_STACK_LINE(43)
		::flixel::FlxSprite _g3 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(43)
		this->islandB = _g3;
		HX_STACK_LINE(44)
		::flixel::FlxSprite _g4 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g4,"_g4");
		HX_STACK_LINE(44)
		this->islandC = _g4;
		HX_STACK_LINE(45)
		::flixel::FlxSprite _g5 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g5,"_g5");
		HX_STACK_LINE(45)
		this->islandD = _g5;
		HX_STACK_LINE(46)
		::flixel::FlxSprite _g6 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g6,"_g6");
		HX_STACK_LINE(46)
		this->islandE = _g6;
		HX_STACK_LINE(47)
		::flixel::FlxSprite _g7 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g7,"_g7");
		HX_STACK_LINE(47)
		this->islandF = _g7;
		HX_STACK_LINE(48)
		::flixel::FlxSprite _g8 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g8,"_g8");
		HX_STACK_LINE(48)
		this->islandG = _g8;
		HX_STACK_LINE(49)
		::flixel::FlxSprite _g9 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g9,"_g9");
		HX_STACK_LINE(49)
		this->islandH = _g9;
		HX_STACK_LINE(50)
		::flixel::FlxSprite _g10 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g10,"_g10");
		HX_STACK_LINE(50)
		this->islandI = _g10;
		HX_STACK_LINE(51)
		::flixel::FlxSprite _g11 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g11,"_g11");
		HX_STACK_LINE(51)
		this->islandJ = _g11;
		HX_STACK_LINE(52)
		::flixel::FlxSprite _g12 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g12,"_g12");
		HX_STACK_LINE(52)
		this->islandK = _g12;
		HX_STACK_LINE(53)
		this->islandA->loadGraphic(HX_CSTRING("assets/images/backGroundMassA.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(54)
		this->islandB->loadGraphic(HX_CSTRING("assets/images/backGroundMassB.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(55)
		this->islandC->loadGraphic(HX_CSTRING("assets/images/backGroundMassC.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(56)
		this->islandD->loadGraphic(HX_CSTRING("assets/images/backGroundMassD.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(57)
		this->islandE->loadGraphic(HX_CSTRING("assets/images/backGroundMassE.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(58)
		this->islandF->loadGraphic(HX_CSTRING("assets/images/islandMouseAnimation.png"),true,(int)80,(int)52,null(),null());
		HX_STACK_LINE(59)
		this->islandF->animation->add(HX_CSTRING("spaceMouse"),Array_obj< int >::__new().Add((int)0).Add((int)1).Add((int)2).Add((int)3).Add((int)4).Add((int)5).Add((int)6).Add((int)7).Add((int)8),(int)10,true);
		HX_STACK_LINE(60)
		this->islandF->animation->play(HX_CSTRING("spaceMouse"),null(),null());
		HX_STACK_LINE(61)
		this->islandG->loadGraphic(HX_CSTRING("assets/images/backGroundMassG.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(62)
		this->islandH->loadGraphic(HX_CSTRING("assets/images/backGroundMassH.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(63)
		this->islandI->loadGraphic(HX_CSTRING("assets/images/rollingMoon.png"),true,(int)15,(int)15,null(),null());
		HX_STACK_LINE(64)
		this->islandI->animation->add(HX_CSTRING("rollingMoon"),Array_obj< int >::__new().Add((int)0).Add((int)1).Add((int)2).Add((int)3).Add((int)4).Add((int)5),(int)10,true);
		HX_STACK_LINE(65)
		this->islandI->animation->play(HX_CSTRING("rollingMoon"),null(),null());
		HX_STACK_LINE(66)
		this->islandJ->loadGraphic(HX_CSTRING("assets/images/upsideDownIsland.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(67)
		this->islandK->loadGraphic(HX_CSTRING("assets/images/sidewaysIsland.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(68)
		this->islandArray[(int)0] = this->islandA;
		HX_STACK_LINE(69)
		this->islandArray[(int)1] = this->islandB;
		HX_STACK_LINE(70)
		this->islandArray[(int)2] = this->islandC;
		HX_STACK_LINE(71)
		this->islandArray[(int)3] = this->islandD;
		HX_STACK_LINE(72)
		this->islandArray[(int)4] = this->islandE;
		HX_STACK_LINE(73)
		this->islandArray[(int)5] = this->islandF;
		HX_STACK_LINE(74)
		this->islandArray[(int)6] = this->islandG;
		HX_STACK_LINE(75)
		this->islandArray[(int)7] = this->islandH;
		HX_STACK_LINE(76)
		this->islandArray[(int)8] = this->islandI;
		HX_STACK_LINE(77)
		this->islandArray[(int)9] = this->islandJ;
		HX_STACK_LINE(78)
		this->islandArray[(int)10] = this->islandK;
		HX_STACK_LINE(80)
		int _g13 = ::flixel::util::FlxRandom_obj::intRanged((int)0,(int)10,null());		HX_STACK_VAR(_g13,"_g13");
		HX_STACK_LINE(80)
		this->randomIslandIndex = _g13;
		HX_STACK_LINE(81)
		int _g14 = ::flixel::util::FlxRandom_obj::intRanged((int)0,(int)400,null());		HX_STACK_VAR(_g14,"_g14");
		HX_STACK_LINE(81)
		this->randomYPosition = _g14;
		HX_STACK_LINE(83)
		this->chosenIsland = this->islandArray->__get(this->randomIslandIndex).StaticCast< ::flixel::FlxSprite >();
		HX_STACK_LINE(84)
		this->chosenIsland->set_y(this->randomYPosition);
		HX_STACK_LINE(85)
		Float _g15 = this->chosenIsland->get_width();		HX_STACK_VAR(_g15,"_g15");
		HX_STACK_LINE(85)
		if (((_g15 > (int)100))){
			HX_STACK_LINE(86)
			this->chosenIsland->set_x((int)-200);
		}
		else{
			HX_STACK_LINE(88)
			this->chosenIsland->set_x((int)-80);
		}
		HX_STACK_LINE(91)
		this->islandGroup->add(this->chosenIsland);
		HX_STACK_LINE(92)
		this->add(this->islandGroup);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Island_obj,init,(void))

::flixel::FlxSprite Island_obj::getIslandSprite( ){
	HX_STACK_FRAME("Island","getIslandSprite",0x950802f7,"Island.getIslandSprite","Island.hx",96,0xc47cb6e9)
	HX_STACK_THIS(this)
	HX_STACK_LINE(96)
	return this->chosenIsland;
}


HX_DEFINE_DYNAMIC_FUNC0(Island_obj,getIslandSprite,return )

Void Island_obj::islandSpeed( ){
{
		HX_STACK_FRAME("Island","islandSpeed",0x18e36579,"Island.islandSpeed","Island.hx",99,0xc47cb6e9)
		HX_STACK_THIS(this)
		HX_STACK_LINE(100)
		Float _g = this->chosenIsland->get_width();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(100)
		if (((_g <= (int)25))){
			HX_STACK_LINE(101)
			::flixel::FlxSprite _g1 = this->chosenIsland;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(101)
			_g1->set_x((_g1->x + .2));
		}
		else{
			HX_STACK_LINE(103)
			Float _g1 = this->chosenIsland->get_width();		HX_STACK_VAR(_g1,"_g1");
			struct _Function_2_1{
				inline static bool Block( hx::ObjectPtr< ::Island_obj > __this){
					HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","Island.hx",103,0xc47cb6e9)
					{
						HX_STACK_LINE(103)
						Float _g2 = __this->chosenIsland->get_width();		HX_STACK_VAR(_g2,"_g2");
						HX_STACK_LINE(103)
						return (_g2 > (int)25);
					}
					return null();
				}
			};
			HX_STACK_LINE(103)
			if (((  (((_g1 <= (int)29))) ? bool(_Function_2_1::Block(this)) : bool(false) ))){
				HX_STACK_LINE(104)
				::flixel::FlxSprite _g2 = this->chosenIsland;		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(104)
				_g2->set_x((_g2->x + .3));
			}
			else{
				HX_STACK_LINE(106)
				Float _g3 = this->chosenIsland->get_width();		HX_STACK_VAR(_g3,"_g3");
				struct _Function_3_1{
					inline static bool Block( hx::ObjectPtr< ::Island_obj > __this){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","Island.hx",106,0xc47cb6e9)
						{
							HX_STACK_LINE(106)
							Float _g4 = __this->chosenIsland->get_width();		HX_STACK_VAR(_g4,"_g4");
							HX_STACK_LINE(106)
							return (_g4 > (int)29);
						}
						return null();
					}
				};
				HX_STACK_LINE(106)
				if (((  (((_g3 <= (int)60))) ? bool(_Function_3_1::Block(this)) : bool(false) ))){
					HX_STACK_LINE(107)
					::flixel::FlxSprite _g2 = this->chosenIsland;		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(107)
					_g2->set_x((_g2->x + .6));
				}
				else{
					HX_STACK_LINE(109)
					Float _g5 = this->chosenIsland->get_width();		HX_STACK_VAR(_g5,"_g5");
					struct _Function_4_1{
						inline static bool Block( hx::ObjectPtr< ::Island_obj > __this){
							HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","Island.hx",109,0xc47cb6e9)
							{
								HX_STACK_LINE(109)
								Float _g6 = __this->chosenIsland->get_width();		HX_STACK_VAR(_g6,"_g6");
								HX_STACK_LINE(109)
								return (_g6 > (int)60);
							}
							return null();
						}
					};
					HX_STACK_LINE(109)
					if (((  (((_g5 <= (int)80))) ? bool(_Function_4_1::Block(this)) : bool(false) ))){
						HX_STACK_LINE(110)
						::flixel::FlxSprite _g2 = this->chosenIsland;		HX_STACK_VAR(_g2,"_g2");
						HX_STACK_LINE(110)
						_g2->set_x((_g2->x + .7));
					}
					else{
						HX_STACK_LINE(112)
						Float _g7 = this->chosenIsland->get_width();		HX_STACK_VAR(_g7,"_g7");
						struct _Function_5_1{
							inline static bool Block( hx::ObjectPtr< ::Island_obj > __this){
								HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","Island.hx",112,0xc47cb6e9)
								{
									HX_STACK_LINE(112)
									Float _g8 = __this->chosenIsland->get_width();		HX_STACK_VAR(_g8,"_g8");
									HX_STACK_LINE(112)
									return (_g8 > (int)80);
								}
								return null();
							}
						};
						HX_STACK_LINE(112)
						if (((  (((_g7 <= (int)198))) ? bool(_Function_5_1::Block(this)) : bool(false) ))){
							HX_STACK_LINE(113)
							::flixel::FlxSprite _g2 = this->chosenIsland;		HX_STACK_VAR(_g2,"_g2");
							HX_STACK_LINE(113)
							_g2->set_x((_g2->x + .8));
						}
						else{
							HX_STACK_LINE(116)
							::flixel::FlxSprite _g2 = this->chosenIsland;		HX_STACK_VAR(_g2,"_g2");
							HX_STACK_LINE(116)
							_g2->set_x((_g2->x + .4));
						}
					}
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Island_obj,islandSpeed,(void))

Void Island_obj::destroyIsland( ){
{
		HX_STACK_FRAME("Island","destroyIsland",0xe473a836,"Island.destroyIsland","Island.hx",122,0xc47cb6e9)
		HX_STACK_THIS(this)
		HX_STACK_LINE(122)
		if (((this->chosenIsland->x >= (int)700))){
			HX_STACK_LINE(124)
			this->destroy();
			HX_STACK_LINE(125)
			this->chosenIsland = null();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Island_obj,destroyIsland,(void))

Void Island_obj::update( ){
{
		HX_STACK_FRAME("Island","update",0xa23f4262,"Island.update","Island.hx",129,0xc47cb6e9)
		HX_STACK_THIS(this)
		HX_STACK_LINE(130)
		this->islandSpeed();
		HX_STACK_LINE(131)
		this->destroyIsland();
		HX_STACK_LINE(132)
		if (((this->chosenIsland != null()))){
			HX_STACK_LINE(133)
			this->super::update();
		}
	}
return null();
}


Void Island_obj::destroy( ){
{
		HX_STACK_FRAME("Island","destroy",0xdbd13e81,"Island.destroy","Island.hx",137,0xc47cb6e9)
		HX_STACK_THIS(this)
		HX_STACK_LINE(138)
		this->islandGroup = null();
		HX_STACK_LINE(139)
		this->islandArray = null();
		HX_STACK_LINE(140)
		this->islandA = null();
		HX_STACK_LINE(141)
		this->islandB = null();
		HX_STACK_LINE(142)
		this->islandB = null();
		HX_STACK_LINE(143)
		this->islandC = null();
		HX_STACK_LINE(144)
		this->islandD = null();
		HX_STACK_LINE(145)
		this->islandE = null();
		HX_STACK_LINE(146)
		this->islandF = null();
		HX_STACK_LINE(147)
		this->islandG = null();
		HX_STACK_LINE(148)
		this->islandH = null();
		HX_STACK_LINE(149)
		this->islandI = null();
		HX_STACK_LINE(150)
		this->islandJ = null();
		HX_STACK_LINE(151)
		this->islandK = null();
		HX_STACK_LINE(152)
		this->super::destroy();
	}
return null();
}



Island_obj::Island_obj()
{
}

void Island_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Island);
	HX_MARK_MEMBER_NAME(islandGroup,"islandGroup");
	HX_MARK_MEMBER_NAME(islandArray,"islandArray");
	HX_MARK_MEMBER_NAME(islandA,"islandA");
	HX_MARK_MEMBER_NAME(islandB,"islandB");
	HX_MARK_MEMBER_NAME(islandC,"islandC");
	HX_MARK_MEMBER_NAME(islandD,"islandD");
	HX_MARK_MEMBER_NAME(islandE,"islandE");
	HX_MARK_MEMBER_NAME(islandF,"islandF");
	HX_MARK_MEMBER_NAME(islandG,"islandG");
	HX_MARK_MEMBER_NAME(islandH,"islandH");
	HX_MARK_MEMBER_NAME(islandI,"islandI");
	HX_MARK_MEMBER_NAME(islandJ,"islandJ");
	HX_MARK_MEMBER_NAME(islandK,"islandK");
	HX_MARK_MEMBER_NAME(randomIslandIndex,"randomIslandIndex");
	HX_MARK_MEMBER_NAME(randomYPosition,"randomYPosition");
	HX_MARK_MEMBER_NAME(randomIslandSpeed,"randomIslandSpeed");
	HX_MARK_MEMBER_NAME(chosenIsland,"chosenIsland");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void Island_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(islandGroup,"islandGroup");
	HX_VISIT_MEMBER_NAME(islandArray,"islandArray");
	HX_VISIT_MEMBER_NAME(islandA,"islandA");
	HX_VISIT_MEMBER_NAME(islandB,"islandB");
	HX_VISIT_MEMBER_NAME(islandC,"islandC");
	HX_VISIT_MEMBER_NAME(islandD,"islandD");
	HX_VISIT_MEMBER_NAME(islandE,"islandE");
	HX_VISIT_MEMBER_NAME(islandF,"islandF");
	HX_VISIT_MEMBER_NAME(islandG,"islandG");
	HX_VISIT_MEMBER_NAME(islandH,"islandH");
	HX_VISIT_MEMBER_NAME(islandI,"islandI");
	HX_VISIT_MEMBER_NAME(islandJ,"islandJ");
	HX_VISIT_MEMBER_NAME(islandK,"islandK");
	HX_VISIT_MEMBER_NAME(randomIslandIndex,"randomIslandIndex");
	HX_VISIT_MEMBER_NAME(randomYPosition,"randomYPosition");
	HX_VISIT_MEMBER_NAME(randomIslandSpeed,"randomIslandSpeed");
	HX_VISIT_MEMBER_NAME(chosenIsland,"chosenIsland");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic Island_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"init") ) { return init_dyn(); }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"islandA") ) { return islandA; }
		if (HX_FIELD_EQ(inName,"islandB") ) { return islandB; }
		if (HX_FIELD_EQ(inName,"islandC") ) { return islandC; }
		if (HX_FIELD_EQ(inName,"islandD") ) { return islandD; }
		if (HX_FIELD_EQ(inName,"islandE") ) { return islandE; }
		if (HX_FIELD_EQ(inName,"islandF") ) { return islandF; }
		if (HX_FIELD_EQ(inName,"islandG") ) { return islandG; }
		if (HX_FIELD_EQ(inName,"islandH") ) { return islandH; }
		if (HX_FIELD_EQ(inName,"islandI") ) { return islandI; }
		if (HX_FIELD_EQ(inName,"islandJ") ) { return islandJ; }
		if (HX_FIELD_EQ(inName,"islandK") ) { return islandK; }
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"islandGroup") ) { return islandGroup; }
		if (HX_FIELD_EQ(inName,"islandArray") ) { return islandArray; }
		if (HX_FIELD_EQ(inName,"islandSpeed") ) { return islandSpeed_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"chosenIsland") ) { return chosenIsland; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"destroyIsland") ) { return destroyIsland_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"randomYPosition") ) { return randomYPosition; }
		if (HX_FIELD_EQ(inName,"getIslandSprite") ) { return getIslandSprite_dyn(); }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"randomIslandIndex") ) { return randomIslandIndex; }
		if (HX_FIELD_EQ(inName,"randomIslandSpeed") ) { return randomIslandSpeed; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Island_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 7:
		if (HX_FIELD_EQ(inName,"islandA") ) { islandA=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"islandB") ) { islandB=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"islandC") ) { islandC=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"islandD") ) { islandD=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"islandE") ) { islandE=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"islandF") ) { islandF=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"islandG") ) { islandG=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"islandH") ) { islandH=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"islandI") ) { islandI=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"islandJ") ) { islandJ=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"islandK") ) { islandK=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"islandGroup") ) { islandGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"islandArray") ) { islandArray=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"chosenIsland") ) { chosenIsland=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"randomYPosition") ) { randomYPosition=inValue.Cast< int >(); return inValue; }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"randomIslandIndex") ) { randomIslandIndex=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"randomIslandSpeed") ) { randomIslandSpeed=inValue.Cast< Float >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Island_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("islandGroup"));
	outFields->push(HX_CSTRING("islandArray"));
	outFields->push(HX_CSTRING("islandA"));
	outFields->push(HX_CSTRING("islandB"));
	outFields->push(HX_CSTRING("islandC"));
	outFields->push(HX_CSTRING("islandD"));
	outFields->push(HX_CSTRING("islandE"));
	outFields->push(HX_CSTRING("islandF"));
	outFields->push(HX_CSTRING("islandG"));
	outFields->push(HX_CSTRING("islandH"));
	outFields->push(HX_CSTRING("islandI"));
	outFields->push(HX_CSTRING("islandJ"));
	outFields->push(HX_CSTRING("islandK"));
	outFields->push(HX_CSTRING("randomIslandIndex"));
	outFields->push(HX_CSTRING("randomYPosition"));
	outFields->push(HX_CSTRING("randomIslandSpeed"));
	outFields->push(HX_CSTRING("chosenIsland"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(Island_obj,islandGroup),HX_CSTRING("islandGroup")},
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(Island_obj,islandArray),HX_CSTRING("islandArray")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Island_obj,islandA),HX_CSTRING("islandA")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Island_obj,islandB),HX_CSTRING("islandB")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Island_obj,islandC),HX_CSTRING("islandC")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Island_obj,islandD),HX_CSTRING("islandD")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Island_obj,islandE),HX_CSTRING("islandE")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Island_obj,islandF),HX_CSTRING("islandF")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Island_obj,islandG),HX_CSTRING("islandG")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Island_obj,islandH),HX_CSTRING("islandH")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Island_obj,islandI),HX_CSTRING("islandI")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Island_obj,islandJ),HX_CSTRING("islandJ")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Island_obj,islandK),HX_CSTRING("islandK")},
	{hx::fsInt,(int)offsetof(Island_obj,randomIslandIndex),HX_CSTRING("randomIslandIndex")},
	{hx::fsInt,(int)offsetof(Island_obj,randomYPosition),HX_CSTRING("randomYPosition")},
	{hx::fsFloat,(int)offsetof(Island_obj,randomIslandSpeed),HX_CSTRING("randomIslandSpeed")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Island_obj,chosenIsland),HX_CSTRING("chosenIsland")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("islandGroup"),
	HX_CSTRING("islandArray"),
	HX_CSTRING("islandA"),
	HX_CSTRING("islandB"),
	HX_CSTRING("islandC"),
	HX_CSTRING("islandD"),
	HX_CSTRING("islandE"),
	HX_CSTRING("islandF"),
	HX_CSTRING("islandG"),
	HX_CSTRING("islandH"),
	HX_CSTRING("islandI"),
	HX_CSTRING("islandJ"),
	HX_CSTRING("islandK"),
	HX_CSTRING("randomIslandIndex"),
	HX_CSTRING("randomYPosition"),
	HX_CSTRING("randomIslandSpeed"),
	HX_CSTRING("chosenIsland"),
	HX_CSTRING("init"),
	HX_CSTRING("getIslandSprite"),
	HX_CSTRING("islandSpeed"),
	HX_CSTRING("destroyIsland"),
	HX_CSTRING("update"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Island_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Island_obj::__mClass,"__mClass");
};

#endif

Class Island_obj::__mClass;

void Island_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("Island"), hx::TCanCast< Island_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void Island_obj::__boot()
{
}

