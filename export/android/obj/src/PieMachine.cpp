#include <hxcpp.h>

#ifndef INCLUDED_PieMachine
#include <PieMachine.h>
#endif
#ifndef INCLUDED_PiePackIcon
#include <PiePackIcon.h>
#endif
#ifndef INCLUDED_Tile
#include <Tile.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_plugin_FlxPlugin
#include <flixel/plugin/FlxPlugin.h>
#endif
#ifndef INCLUDED_flixel_plugin_MouseEventManager
#include <flixel/plugin/MouseEventManager.h>
#endif
#ifndef INCLUDED_flixel_system_FlxSound
#include <flixel/system/FlxSound.h>
#endif
#ifndef INCLUDED_flixel_system_frontEnds_SoundFrontEnd
#include <flixel/system/frontEnds/SoundFrontEnd.h>
#endif
#ifndef INCLUDED_flixel_tweens_FlxTween
#include <flixel/tweens/FlxTween.h>
#endif
#ifndef INCLUDED_flixel_tweens_misc_VarTween
#include <flixel/tweens/misc/VarTween.h>
#endif
#ifndef INCLUDED_flixel_util_FlxDestroyUtil
#include <flixel/util/FlxDestroyUtil.h>
#endif

Void PieMachine_obj::__construct(::Tile tileSprite)
{
HX_STACK_FRAME("PieMachine","new",0x215f0b2d,"PieMachine.new","PieMachine.hx",14,0xf6f738e3)
HX_STACK_THIS(this)
HX_STACK_ARG(tileSprite,"tileSprite")
{
	HX_STACK_LINE(40)
	this->inPiePack = false;
	HX_STACK_LINE(39)
	this->pastryFlying = false;
	HX_STACK_LINE(38)
	this->animateRepairSprite = false;
	HX_STACK_LINE(37)
	this->repairClicked = false;
	HX_STACK_LINE(36)
	this->machineClicked = false;
	HX_STACK_LINE(35)
	this->machineAlertReset = false;
	HX_STACK_LINE(34)
	this->machineOn = false;
	HX_STACK_LINE(33)
	this->payElectricBill = false;
	HX_STACK_LINE(28)
	this->machineReliabilityTimer = (int)15;
	HX_STACK_LINE(27)
	this->electricUtilityTimer = (int)10;
	HX_STACK_LINE(45)
	super::__construct(null());
	HX_STACK_LINE(46)
	this->myTile = tileSprite;
	HX_STACK_LINE(47)
	::flixel::group::FlxGroup _g = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(47)
	this->pieMachineGroup = _g;
	HX_STACK_LINE(48)
	::flixel::FlxSprite _g1 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
	HX_STACK_LINE(48)
	this->pieMachineSprite = _g1;
	HX_STACK_LINE(49)
	::flixel::FlxSprite _g2 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g2,"_g2");
	HX_STACK_LINE(49)
	this->pieMachineAlertSprite = _g2;
	HX_STACK_LINE(50)
	::flixel::FlxSprite _g3 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g3,"_g3");
	HX_STACK_LINE(50)
	this->minusOneSprite = _g3;
	HX_STACK_LINE(51)
	::flixel::FlxSprite _g4 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g4,"_g4");
	HX_STACK_LINE(51)
	this->repairAlertSprite = _g4;
	HX_STACK_LINE(52)
	::flixel::FlxSprite _g5 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g5,"_g5");
	HX_STACK_LINE(52)
	this->pieMachineFlyingPastry = _g5;
	HX_STACK_LINE(53)
	this->minusOneSprite->loadGraphic(HX_CSTRING("assets/images/minusOneImage.png"),false,null(),null(),null(),null());
	HX_STACK_LINE(54)
	this->minusOneSprite->set_alpha((int)0);
	HX_STACK_LINE(55)
	this->pieMachineSprite->loadGraphic(HX_CSTRING("assets/images/pieMachineAnimation.png"),true,(int)86,(int)84,null(),null());
	HX_STACK_LINE(56)
	this->pieMachineSprite->animation->add(HX_CSTRING("machineOn"),Array_obj< int >::__new().Add((int)0).Add((int)1).Add((int)2).Add((int)3).Add((int)4),(int)5,true);
	HX_STACK_LINE(57)
	this->pieMachineSprite->animation->add(HX_CSTRING("machineOff"),Array_obj< int >::__new().Add((int)0),(int)1,false);
	HX_STACK_LINE(58)
	this->pieMachineSprite->animation->play(HX_CSTRING("machineOff"),null(),null());
	HX_STACK_LINE(59)
	this->pieMachineAlertSprite->loadGraphic(HX_CSTRING("assets/images/juiceMachineSwitchAnimation.png"),true,(int)32,(int)18,null(),null());
	HX_STACK_LINE(60)
	this->pieMachineAlertSprite->animation->add(HX_CSTRING("switchOn"),Array_obj< int >::__new().Add((int)1),(int)1,false);
	HX_STACK_LINE(61)
	this->pieMachineAlertSprite->animation->add(HX_CSTRING("switchOff"),Array_obj< int >::__new().Add((int)0),(int)1,false);
	HX_STACK_LINE(62)
	this->pieMachineAlertSprite->animation->play(HX_CSTRING("switchOn"),null(),null());
	HX_STACK_LINE(63)
	this->repairAlertSprite->loadGraphic(HX_CSTRING("assets/images/repairAnimationS.png"),true,(int)16,(int)16,null(),null());
	HX_STACK_LINE(64)
	this->repairAlertSprite->animation->add(HX_CSTRING("repairMe"),Array_obj< int >::__new().Add((int)0).Add((int)1).Add((int)2).Add((int)3).Add((int)4),(int)10,true);
	HX_STACK_LINE(65)
	this->pieMachineFlyingPastry->loadGraphic(HX_CSTRING("assets/images/pieIcon.png"),null(),null(),null(),null(),null());
	HX_STACK_LINE(66)
	this->repairAlertSprite->set_alpha((int)0);
	HX_STACK_LINE(67)
	this->pieMachineFlyingPastry->set_x((tileSprite->x + (int)90));
	HX_STACK_LINE(68)
	this->pieMachineFlyingPastry->set_y((tileSprite->y + (int)5));
	HX_STACK_LINE(69)
	this->pieMachineSprite->set_x((tileSprite->x + (int)57));
	HX_STACK_LINE(70)
	this->pieMachineSprite->set_y((tileSprite->y - (int)15));
	HX_STACK_LINE(71)
	this->pieMachineAlertSprite->set_x((tileSprite->x + (int)78));
	HX_STACK_LINE(72)
	this->pieMachineAlertSprite->set_y((tileSprite->y + (int)3));
	HX_STACK_LINE(73)
	this->repairAlertSprite->set_x((tileSprite->x + (int)96));
	HX_STACK_LINE(74)
	this->repairAlertSprite->set_y((tileSprite->y + (int)67));
	HX_STACK_LINE(75)
	this->registerMachineEvents();
	HX_STACK_LINE(76)
	this->pieMachineGroup->add(this->pieMachineSprite);
	HX_STACK_LINE(77)
	this->pieMachineGroup->add(this->pieMachineAlertSprite);
	HX_STACK_LINE(78)
	this->pieMachineGroup->add(this->repairAlertSprite);
	HX_STACK_LINE(79)
	this->pieMachineGroup->add(this->minusOneSprite);
	HX_STACK_LINE(80)
	this->add(this->pieMachineGroup);
}
;
	return null();
}

//PieMachine_obj::~PieMachine_obj() { }

Dynamic PieMachine_obj::__CreateEmpty() { return  new PieMachine_obj; }
hx::ObjectPtr< PieMachine_obj > PieMachine_obj::__new(::Tile tileSprite)
{  hx::ObjectPtr< PieMachine_obj > result = new PieMachine_obj();
	result->__construct(tileSprite);
	return result;}

Dynamic PieMachine_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< PieMachine_obj > result = new PieMachine_obj();
	result->__construct(inArgs[0]);
	return result;}

::flixel::FlxSprite PieMachine_obj::getpieMachineSprite( ){
	HX_STACK_FRAME("PieMachine","getpieMachineSprite",0x42381463,"PieMachine.getpieMachineSprite","PieMachine.hx",85,0xf6f738e3)
	HX_STACK_THIS(this)
	HX_STACK_LINE(85)
	return this->pieMachineSprite;
}


HX_DEFINE_DYNAMIC_FUNC0(PieMachine_obj,getpieMachineSprite,return )

bool PieMachine_obj::getInPiePack( ){
	HX_STACK_FRAME("PieMachine","getInPiePack",0x3f38d3fd,"PieMachine.getInPiePack","PieMachine.hx",89,0xf6f738e3)
	HX_STACK_THIS(this)
	HX_STACK_LINE(89)
	return this->inPiePack;
}


HX_DEFINE_DYNAMIC_FUNC0(PieMachine_obj,getInPiePack,return )

Float PieMachine_obj::getMyRelativeX( ){
	HX_STACK_FRAME("PieMachine","getMyRelativeX",0x099dca5d,"PieMachine.getMyRelativeX","PieMachine.hx",93,0xf6f738e3)
	HX_STACK_THIS(this)
	HX_STACK_LINE(93)
	return this->myRelativeX;
}


HX_DEFINE_DYNAMIC_FUNC0(PieMachine_obj,getMyRelativeX,return )

bool PieMachine_obj::getPastryFlying( ){
	HX_STACK_FRAME("PieMachine","getPastryFlying",0xcb8fd04b,"PieMachine.getPastryFlying","PieMachine.hx",97,0xf6f738e3)
	HX_STACK_THIS(this)
	HX_STACK_LINE(97)
	return this->pastryFlying;
}


HX_DEFINE_DYNAMIC_FUNC0(PieMachine_obj,getPastryFlying,return )

::flixel::FlxSprite PieMachine_obj::getPieMachineFlyingPastry( ){
	HX_STACK_FRAME("PieMachine","getPieMachineFlyingPastry",0xfb1d13c6,"PieMachine.getPieMachineFlyingPastry","PieMachine.hx",101,0xf6f738e3)
	HX_STACK_THIS(this)
	HX_STACK_LINE(101)
	return this->pieMachineFlyingPastry;
}


HX_DEFINE_DYNAMIC_FUNC0(PieMachine_obj,getPieMachineFlyingPastry,return )

Float PieMachine_obj::getMyRelativeY( ){
	HX_STACK_FRAME("PieMachine","getMyRelativeY",0x099dca5e,"PieMachine.getMyRelativeY","PieMachine.hx",105,0xf6f738e3)
	HX_STACK_THIS(this)
	HX_STACK_LINE(105)
	return this->myRelativeY;
}


HX_DEFINE_DYNAMIC_FUNC0(PieMachine_obj,getMyRelativeY,return )

bool PieMachine_obj::getMachineOn( ){
	HX_STACK_FRAME("PieMachine","getMachineOn",0xb49cd0a3,"PieMachine.getMachineOn","PieMachine.hx",109,0xf6f738e3)
	HX_STACK_THIS(this)
	HX_STACK_LINE(109)
	return this->machineOn;
}


HX_DEFINE_DYNAMIC_FUNC0(PieMachine_obj,getMachineOn,return )

bool PieMachine_obj::getPayElectricBill( ){
	HX_STACK_FRAME("PieMachine","getPayElectricBill",0xe9641be9,"PieMachine.getPayElectricBill","PieMachine.hx",113,0xf6f738e3)
	HX_STACK_THIS(this)
	HX_STACK_LINE(113)
	return this->payElectricBill;
}


HX_DEFINE_DYNAMIC_FUNC0(PieMachine_obj,getPayElectricBill,return )

bool PieMachine_obj::getRepairClicked( ){
	HX_STACK_FRAME("PieMachine","getRepairClicked",0x6aa1ef77,"PieMachine.getRepairClicked","PieMachine.hx",117,0xf6f738e3)
	HX_STACK_THIS(this)
	HX_STACK_LINE(117)
	return this->repairClicked;
}


HX_DEFINE_DYNAMIC_FUNC0(PieMachine_obj,getRepairClicked,return )

bool PieMachine_obj::getAnimateRepairSprite( ){
	HX_STACK_FRAME("PieMachine","getAnimateRepairSprite",0xa293b7d0,"PieMachine.getAnimateRepairSprite","PieMachine.hx",121,0xf6f738e3)
	HX_STACK_THIS(this)
	HX_STACK_LINE(121)
	return this->animateRepairSprite;
}


HX_DEFINE_DYNAMIC_FUNC0(PieMachine_obj,getAnimateRepairSprite,return )

Void PieMachine_obj::setAnimateRepairSprite( bool val){
{
		HX_STACK_FRAME("PieMachine","setAnimateRepairSprite",0xd63f3444,"PieMachine.setAnimateRepairSprite","PieMachine.hx",125,0xf6f738e3)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(125)
		this->animateRepairSprite = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PieMachine_obj,setAnimateRepairSprite,(void))

Void PieMachine_obj::setPastryFlying( bool val){
{
		HX_STACK_FRAME("PieMachine","setPastryFlying",0xc75b4d57,"PieMachine.setPastryFlying","PieMachine.hx",129,0xf6f738e3)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(129)
		this->pastryFlying = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PieMachine_obj,setPastryFlying,(void))

Void PieMachine_obj::setRepairClicked( bool val){
{
		HX_STACK_FRAME("PieMachine","setRepairClicked",0xc0e3dceb,"PieMachine.setRepairClicked","PieMachine.hx",133,0xf6f738e3)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(133)
		this->repairClicked = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PieMachine_obj,setRepairClicked,(void))

Void PieMachine_obj::setInPiePack( bool val){
{
		HX_STACK_FRAME("PieMachine","setInPiePack",0x5431f771,"PieMachine.setInPiePack","PieMachine.hx",137,0xf6f738e3)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(137)
		this->inPiePack = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PieMachine_obj,setInPiePack,(void))

Void PieMachine_obj::setPayElectricBill( bool val){
{
		HX_STACK_FRAME("PieMachine","setPayElectricBill",0xc6134e5d,"PieMachine.setPayElectricBill","PieMachine.hx",141,0xf6f738e3)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(141)
		this->payElectricBill = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PieMachine_obj,setPayElectricBill,(void))

Void PieMachine_obj::setMachineOn( bool val){
{
		HX_STACK_FRAME("PieMachine","setMachineOn",0xc995f417,"PieMachine.setMachineOn","PieMachine.hx",145,0xf6f738e3)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(145)
		this->machineOn = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PieMachine_obj,setMachineOn,(void))

Void PieMachine_obj::setMyRelativeX( Float val){
{
		HX_STACK_FRAME("PieMachine","setMyRelativeX",0x29bdb2d1,"PieMachine.setMyRelativeX","PieMachine.hx",149,0xf6f738e3)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(149)
		this->myRelativeX = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PieMachine_obj,setMyRelativeX,(void))

Void PieMachine_obj::setMyRelativeY( Float val){
{
		HX_STACK_FRAME("PieMachine","setMyRelativeY",0x29bdb2d2,"PieMachine.setMyRelativeY","PieMachine.hx",153,0xf6f738e3)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(153)
		this->myRelativeY = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PieMachine_obj,setMyRelativeY,(void))

Void PieMachine_obj::setGoldCount( int val){
{
		HX_STACK_FRAME("PieMachine","setGoldCount",0xf4fee1a0,"PieMachine.setGoldCount","PieMachine.hx",157,0xf6f738e3)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(157)
		this->goldCount = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PieMachine_obj,setGoldCount,(void))

Void PieMachine_obj::setAppleCount( int val){
{
		HX_STACK_FRAME("PieMachine","setAppleCount",0x2b3d6464,"PieMachine.setAppleCount","PieMachine.hx",161,0xf6f738e3)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(161)
		this->appleCount = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PieMachine_obj,setAppleCount,(void))

Void PieMachine_obj::registerMachineEvents( ){
{
		HX_STACK_FRAME("PieMachine","registerMachineEvents",0x0b4cf7aa,"PieMachine.registerMachineEvents","PieMachine.hx",164,0xf6f738e3)
		HX_STACK_THIS(this)
		HX_STACK_LINE(165)
		::flixel::plugin::MouseEventManager_obj::add(this->pieMachineAlertSprite,this->turnMachineOn_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(166)
		::flixel::plugin::MouseEventManager_obj::add(this->repairAlertSprite,this->repairAlertClicked_dyn(),null(),null(),null(),null(),null(),null());
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PieMachine_obj,registerMachineEvents,(void))

Void PieMachine_obj::unregisterMachineEvents( ){
{
		HX_STACK_FRAME("PieMachine","unregisterMachineEvents",0x5e5109f1,"PieMachine.unregisterMachineEvents","PieMachine.hx",169,0xf6f738e3)
		HX_STACK_THIS(this)
		HX_STACK_LINE(170)
		::flixel::plugin::MouseEventManager_obj::remove(this->pieMachineAlertSprite);
		HX_STACK_LINE(171)
		::flixel::plugin::MouseEventManager_obj::remove(this->repairAlertSprite);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PieMachine_obj,unregisterMachineEvents,(void))

Void PieMachine_obj::registerMachineButtonEvent( ){
{
		HX_STACK_FRAME("PieMachine","registerMachineButtonEvent",0x49ab3297,"PieMachine.registerMachineButtonEvent","PieMachine.hx",175,0xf6f738e3)
		HX_STACK_THIS(this)
		HX_STACK_LINE(175)
		::flixel::plugin::MouseEventManager_obj::add(this->pieMachineAlertSprite,this->turnMachineOn_dyn(),null(),null(),null(),null(),null(),null());
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PieMachine_obj,registerMachineButtonEvent,(void))

Void PieMachine_obj::repairAlertClicked( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("PieMachine","repairAlertClicked",0x18cd3f6b,"PieMachine.repairAlertClicked","PieMachine.hx",179,0xf6f738e3)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(179)
		this->repairClicked = true;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PieMachine_obj,repairAlertClicked,(void))

Void PieMachine_obj::turnMachineOn( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("PieMachine","turnMachineOn",0xc4ae6a56,"PieMachine.turnMachineOn","PieMachine.hx",184,0xf6f738e3)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(184)
		if ((!(this->machineOn))){
			HX_STACK_LINE(185)
			::flixel::system::FlxSound _g = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/juiceMachineSound.wav"),.2,null(),null(),null(),null(),null());		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(185)
			this->pieMachineSound = _g;
			HX_STACK_LINE(186)
			this->pieMachineSound->play(null());
			HX_STACK_LINE(187)
			::flixel::system::FlxSound _g1 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/pieBakedSound.wav"),(int)1,true,null(),null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(187)
			this->pieBakingSound = _g1;
			HX_STACK_LINE(188)
			this->pieBakingSound->play(null());
			HX_STACK_LINE(189)
			this->pieMachineAlertSprite->animation->play(HX_CSTRING("switchOff"),null(),null());
			HX_STACK_LINE(190)
			this->pieMachineSprite->animation->play(HX_CSTRING("machineOn"),null(),null());
			HX_STACK_LINE(191)
			this->machineOn = true;
		}
		else{
			HX_STACK_LINE(194)
			this->pieMachineAlertSprite->animation->play(HX_CSTRING("switchOn"),null(),null());
			HX_STACK_LINE(195)
			this->pieMachineSprite->animation->play(HX_CSTRING("machineOff"),null(),null());
			HX_STACK_LINE(196)
			if (((bool((this->pieBakingSound != null())) && bool((this->pieMachineSound != null()))))){
				HX_STACK_LINE(197)
				{
					HX_STACK_LINE(197)
					::flixel::system::FlxSound _this = this->pieMachineSound;		HX_STACK_VAR(_this,"_this");
					HX_STACK_LINE(197)
					_this->cleanup(_this->autoDestroy,true,true);
					HX_STACK_LINE(197)
					_this;
				}
				HX_STACK_LINE(198)
				{
					HX_STACK_LINE(198)
					::flixel::system::FlxSound _this = this->pieBakingSound;		HX_STACK_VAR(_this,"_this");
					HX_STACK_LINE(198)
					_this->cleanup(_this->autoDestroy,true,true);
					HX_STACK_LINE(198)
					_this;
				}
				HX_STACK_LINE(199)
				::flixel::system::FlxSound _g2 = ::flixel::util::FlxDestroyUtil_obj::destroy(this->pieBakingSound);		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(199)
				this->pieBakingSound = _g2;
				HX_STACK_LINE(200)
				::flixel::system::FlxSound _g3 = ::flixel::util::FlxDestroyUtil_obj::destroy(this->pieMachineSound);		HX_STACK_VAR(_g3,"_g3");
				HX_STACK_LINE(200)
				this->pieMachineSound = _g3;
			}
			HX_STACK_LINE(202)
			this->machineOn = false;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PieMachine_obj,turnMachineOn,(void))

Void PieMachine_obj::payElectricUtility( ){
{
		HX_STACK_FRAME("PieMachine","payElectricUtility",0xcf44185a,"PieMachine.payElectricUtility","PieMachine.hx",207,0xf6f738e3)
		HX_STACK_THIS(this)
		HX_STACK_LINE(207)
		if ((this->machineOn)){
			HX_STACK_LINE(208)
			hx::SubEq(this->electricUtilityTimer,::flixel::FlxG_obj::elapsed);
			HX_STACK_LINE(209)
			if (((this->electricUtilityTimer < (int)0))){
				HX_STACK_LINE(210)
				this->minusOneSprite->set_x((this->pieMachineSprite->x + (int)30));
				HX_STACK_LINE(211)
				this->minusOneSprite->set_y((this->pieMachineSprite->y + (int)5));
				HX_STACK_LINE(212)
				this->minusOneSprite->set_alpha((int)1);
				struct _Function_3_1{
					inline static Dynamic Block( hx::ObjectPtr< ::PieMachine_obj > __this){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PieMachine.hx",213,0xf6f738e3)
						{
							hx::Anon __result = hx::Anon_obj::Create();
							__result->Add(HX_CSTRING("alpha") , (int)0,false);
							__result->Add(HX_CSTRING("y") , (__this->minusOneSprite->y - (int)16),false);
							return __result;
						}
						return null();
					}
				};
				HX_STACK_LINE(213)
				::flixel::tweens::FlxTween_obj::tween(this->minusOneSprite,_Function_3_1::Block(this),(int)1,null());
				HX_STACK_LINE(214)
				this->payElectricBill = true;
				HX_STACK_LINE(215)
				this->electricUtilityTimer = (int)10;
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PieMachine_obj,payElectricUtility,(void))

Void PieMachine_obj::machineReliability( ){
{
		HX_STACK_FRAME("PieMachine","machineReliability",0x36dc8a46,"PieMachine.machineReliability","PieMachine.hx",221,0xf6f738e3)
		HX_STACK_THIS(this)
		HX_STACK_LINE(221)
		if ((this->machineOn)){
			HX_STACK_LINE(222)
			hx::SubEq(this->machineReliabilityTimer,::flixel::FlxG_obj::elapsed);
			HX_STACK_LINE(223)
			if (((this->machineReliabilityTimer < (int)0))){
				HX_STACK_LINE(224)
				this->animateRepairSprite = true;
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PieMachine_obj,machineReliability,(void))

Void PieMachine_obj::resetRepairAlertTimer( ){
{
		HX_STACK_FRAME("PieMachine","resetRepairAlertTimer",0x16b2ac72,"PieMachine.resetRepairAlertTimer","PieMachine.hx",230,0xf6f738e3)
		HX_STACK_THIS(this)
		HX_STACK_LINE(230)
		this->machineReliabilityTimer = (int)15;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PieMachine_obj,resetRepairAlertTimer,(void))

Void PieMachine_obj::repairSpriteAnimation( ){
{
		HX_STACK_FRAME("PieMachine","repairSpriteAnimation",0x0cc42d7f,"PieMachine.repairSpriteAnimation","PieMachine.hx",234,0xf6f738e3)
		HX_STACK_THIS(this)
		HX_STACK_LINE(234)
		if ((this->animateRepairSprite)){
			HX_STACK_LINE(235)
			{
				HX_STACK_LINE(235)
				::flixel::FlxSprite _g = this->repairAlertSprite;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(235)
				_g->set_alpha((_g->alpha + .01));
			}
			HX_STACK_LINE(236)
			this->repairAlertSprite->animation->play(HX_CSTRING("repairMe"),null(),null());
			HX_STACK_LINE(237)
			this->machineReliabilityTimer = (int)-1;
			HX_STACK_LINE(238)
			::flixel::plugin::MouseEventManager_obj::remove(this->pieMachineAlertSprite);
			HX_STACK_LINE(239)
			this->machineOn = true;
			HX_STACK_LINE(240)
			this->turnMachineOn(null());
		}
		else{
			HX_STACK_LINE(243)
			::flixel::FlxSprite _g = this->repairAlertSprite;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(243)
			_g->set_alpha((_g->alpha - .1));
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PieMachine_obj,repairSpriteAnimation,(void))

Void PieMachine_obj::setRiseRun( ){
{
		HX_STACK_FRAME("PieMachine","setRiseRun",0x6e923413,"PieMachine.setRiseRun","PieMachine.hx",247,0xf6f738e3)
		HX_STACK_THIS(this)
		HX_STACK_LINE(248)
		this->run = -((((::PiePackIcon_obj::piePackPosX + (int)20) - this->pieMachineFlyingPastry->x)));
		HX_STACK_LINE(249)
		this->rise = -((((::PiePackIcon_obj::piePackPosY + (int)20) - this->pieMachineFlyingPastry->y)));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PieMachine_obj,setRiseRun,(void))

Void PieMachine_obj::sendPastryToPack( ){
{
		HX_STACK_FRAME("PieMachine","sendPastryToPack",0x51039e68,"PieMachine.sendPastryToPack","PieMachine.hx",253,0xf6f738e3)
		HX_STACK_THIS(this)
		HX_STACK_LINE(253)
		if ((this->pastryFlying)){
			HX_STACK_LINE(254)
			this->pieMachineFlyingPastry->set_alpha((int)1);
			HX_STACK_LINE(255)
			if (((this->pieMachineFlyingPastry->y >= (::PiePackIcon_obj::piePackPosY + (int)20)))){
				HX_STACK_LINE(256)
				::flixel::FlxSprite _g = this->pieMachineFlyingPastry;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(256)
				_g->set_y((_g->y - (Float(this->rise) / Float((int)25))));
			}
			HX_STACK_LINE(258)
			if (((this->pieMachineFlyingPastry->x >= (::PiePackIcon_obj::piePackPosX + (int)20)))){
				HX_STACK_LINE(259)
				::flixel::FlxSprite _g = this->pieMachineFlyingPastry;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(259)
				_g->set_x((_g->x - (Float(this->run) / Float((int)25))));
			}
			HX_STACK_LINE(261)
			if (((bool((this->pieMachineFlyingPastry->x <= (::PiePackIcon_obj::piePackPosX + (int)22))) && bool((this->pieMachineFlyingPastry->y <= (::PiePackIcon_obj::piePackPosY + (int)22)))))){
				HX_STACK_LINE(262)
				this->inPiePack = true;
				HX_STACK_LINE(263)
				this->pastryFlying = false;
				HX_STACK_LINE(264)
				this->pieMachineFlyingPastry->set_alpha((int)0);
				HX_STACK_LINE(265)
				this->pieMachineFlyingPastry->set_x((this->myTile->x + (int)90));
				HX_STACK_LINE(266)
				this->pieMachineFlyingPastry->set_y((this->myTile->y + (int)5));
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PieMachine_obj,sendPastryToPack,(void))

Void PieMachine_obj::update( ){
{
		HX_STACK_FRAME("PieMachine","update",0x3cbb495c,"PieMachine.update","PieMachine.hx",271,0xf6f738e3)
		HX_STACK_THIS(this)
		HX_STACK_LINE(272)
		this->machineReliability();
		HX_STACK_LINE(273)
		this->repairSpriteAnimation();
		HX_STACK_LINE(274)
		this->payElectricUtility();
		HX_STACK_LINE(275)
		this->sendPastryToPack();
		HX_STACK_LINE(276)
		this->super::update();
	}
return null();
}


Void PieMachine_obj::destroy( ){
{
		HX_STACK_FRAME("PieMachine","destroy",0x6ddb5247,"PieMachine.destroy","PieMachine.hx",279,0xf6f738e3)
		HX_STACK_THIS(this)
		HX_STACK_LINE(280)
		this->myTile = null();
		HX_STACK_LINE(281)
		this->pieMachineGroup = null();
		HX_STACK_LINE(282)
		this->pieMachineSprite = null();
		HX_STACK_LINE(283)
		this->pieMachineAlertSprite = null();
		HX_STACK_LINE(284)
		this->minusOneSprite = null();
		HX_STACK_LINE(285)
		this->repairAlertSprite = null();
		HX_STACK_LINE(286)
		this->pieMachineFlyingPastry = null();
		HX_STACK_LINE(287)
		this->super::destroy();
	}
return null();
}



PieMachine_obj::PieMachine_obj()
{
}

void PieMachine_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(PieMachine);
	HX_MARK_MEMBER_NAME(pieMachineGroup,"pieMachineGroup");
	HX_MARK_MEMBER_NAME(pieMachineSprite,"pieMachineSprite");
	HX_MARK_MEMBER_NAME(pieMachineAlertSprite,"pieMachineAlertSprite");
	HX_MARK_MEMBER_NAME(minusOneSprite,"minusOneSprite");
	HX_MARK_MEMBER_NAME(repairAlertSprite,"repairAlertSprite");
	HX_MARK_MEMBER_NAME(pieMachineFlyingPastry,"pieMachineFlyingPastry");
	HX_MARK_MEMBER_NAME(pieBakingSound,"pieBakingSound");
	HX_MARK_MEMBER_NAME(pieMachineSound,"pieMachineSound");
	HX_MARK_MEMBER_NAME(myRelativeX,"myRelativeX");
	HX_MARK_MEMBER_NAME(myRelativeY,"myRelativeY");
	HX_MARK_MEMBER_NAME(electricUtilityTimer,"electricUtilityTimer");
	HX_MARK_MEMBER_NAME(machineReliabilityTimer,"machineReliabilityTimer");
	HX_MARK_MEMBER_NAME(rise,"rise");
	HX_MARK_MEMBER_NAME(run,"run");
	HX_MARK_MEMBER_NAME(goldCount,"goldCount");
	HX_MARK_MEMBER_NAME(appleCount,"appleCount");
	HX_MARK_MEMBER_NAME(payElectricBill,"payElectricBill");
	HX_MARK_MEMBER_NAME(machineOn,"machineOn");
	HX_MARK_MEMBER_NAME(machineAlertReset,"machineAlertReset");
	HX_MARK_MEMBER_NAME(machineClicked,"machineClicked");
	HX_MARK_MEMBER_NAME(repairClicked,"repairClicked");
	HX_MARK_MEMBER_NAME(animateRepairSprite,"animateRepairSprite");
	HX_MARK_MEMBER_NAME(pastryFlying,"pastryFlying");
	HX_MARK_MEMBER_NAME(inPiePack,"inPiePack");
	HX_MARK_MEMBER_NAME(myTile,"myTile");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void PieMachine_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(pieMachineGroup,"pieMachineGroup");
	HX_VISIT_MEMBER_NAME(pieMachineSprite,"pieMachineSprite");
	HX_VISIT_MEMBER_NAME(pieMachineAlertSprite,"pieMachineAlertSprite");
	HX_VISIT_MEMBER_NAME(minusOneSprite,"minusOneSprite");
	HX_VISIT_MEMBER_NAME(repairAlertSprite,"repairAlertSprite");
	HX_VISIT_MEMBER_NAME(pieMachineFlyingPastry,"pieMachineFlyingPastry");
	HX_VISIT_MEMBER_NAME(pieBakingSound,"pieBakingSound");
	HX_VISIT_MEMBER_NAME(pieMachineSound,"pieMachineSound");
	HX_VISIT_MEMBER_NAME(myRelativeX,"myRelativeX");
	HX_VISIT_MEMBER_NAME(myRelativeY,"myRelativeY");
	HX_VISIT_MEMBER_NAME(electricUtilityTimer,"electricUtilityTimer");
	HX_VISIT_MEMBER_NAME(machineReliabilityTimer,"machineReliabilityTimer");
	HX_VISIT_MEMBER_NAME(rise,"rise");
	HX_VISIT_MEMBER_NAME(run,"run");
	HX_VISIT_MEMBER_NAME(goldCount,"goldCount");
	HX_VISIT_MEMBER_NAME(appleCount,"appleCount");
	HX_VISIT_MEMBER_NAME(payElectricBill,"payElectricBill");
	HX_VISIT_MEMBER_NAME(machineOn,"machineOn");
	HX_VISIT_MEMBER_NAME(machineAlertReset,"machineAlertReset");
	HX_VISIT_MEMBER_NAME(machineClicked,"machineClicked");
	HX_VISIT_MEMBER_NAME(repairClicked,"repairClicked");
	HX_VISIT_MEMBER_NAME(animateRepairSprite,"animateRepairSprite");
	HX_VISIT_MEMBER_NAME(pastryFlying,"pastryFlying");
	HX_VISIT_MEMBER_NAME(inPiePack,"inPiePack");
	HX_VISIT_MEMBER_NAME(myTile,"myTile");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic PieMachine_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"run") ) { return run; }
		break;
	case 4:
		if (HX_FIELD_EQ(inName,"rise") ) { return rise; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"myTile") ) { return myTile; }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"goldCount") ) { return goldCount; }
		if (HX_FIELD_EQ(inName,"machineOn") ) { return machineOn; }
		if (HX_FIELD_EQ(inName,"inPiePack") ) { return inPiePack; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"appleCount") ) { return appleCount; }
		if (HX_FIELD_EQ(inName,"setRiseRun") ) { return setRiseRun_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"myRelativeX") ) { return myRelativeX; }
		if (HX_FIELD_EQ(inName,"myRelativeY") ) { return myRelativeY; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"pastryFlying") ) { return pastryFlying; }
		if (HX_FIELD_EQ(inName,"getInPiePack") ) { return getInPiePack_dyn(); }
		if (HX_FIELD_EQ(inName,"getMachineOn") ) { return getMachineOn_dyn(); }
		if (HX_FIELD_EQ(inName,"setInPiePack") ) { return setInPiePack_dyn(); }
		if (HX_FIELD_EQ(inName,"setMachineOn") ) { return setMachineOn_dyn(); }
		if (HX_FIELD_EQ(inName,"setGoldCount") ) { return setGoldCount_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"repairClicked") ) { return repairClicked; }
		if (HX_FIELD_EQ(inName,"setAppleCount") ) { return setAppleCount_dyn(); }
		if (HX_FIELD_EQ(inName,"turnMachineOn") ) { return turnMachineOn_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"minusOneSprite") ) { return minusOneSprite; }
		if (HX_FIELD_EQ(inName,"pieBakingSound") ) { return pieBakingSound; }
		if (HX_FIELD_EQ(inName,"machineClicked") ) { return machineClicked; }
		if (HX_FIELD_EQ(inName,"getMyRelativeX") ) { return getMyRelativeX_dyn(); }
		if (HX_FIELD_EQ(inName,"getMyRelativeY") ) { return getMyRelativeY_dyn(); }
		if (HX_FIELD_EQ(inName,"setMyRelativeX") ) { return setMyRelativeX_dyn(); }
		if (HX_FIELD_EQ(inName,"setMyRelativeY") ) { return setMyRelativeY_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"pieMachineGroup") ) { return pieMachineGroup; }
		if (HX_FIELD_EQ(inName,"pieMachineSound") ) { return pieMachineSound; }
		if (HX_FIELD_EQ(inName,"payElectricBill") ) { return payElectricBill; }
		if (HX_FIELD_EQ(inName,"getPastryFlying") ) { return getPastryFlying_dyn(); }
		if (HX_FIELD_EQ(inName,"setPastryFlying") ) { return setPastryFlying_dyn(); }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"pieMachineSprite") ) { return pieMachineSprite; }
		if (HX_FIELD_EQ(inName,"getRepairClicked") ) { return getRepairClicked_dyn(); }
		if (HX_FIELD_EQ(inName,"setRepairClicked") ) { return setRepairClicked_dyn(); }
		if (HX_FIELD_EQ(inName,"sendPastryToPack") ) { return sendPastryToPack_dyn(); }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"repairAlertSprite") ) { return repairAlertSprite; }
		if (HX_FIELD_EQ(inName,"machineAlertReset") ) { return machineAlertReset; }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"getPayElectricBill") ) { return getPayElectricBill_dyn(); }
		if (HX_FIELD_EQ(inName,"setPayElectricBill") ) { return setPayElectricBill_dyn(); }
		if (HX_FIELD_EQ(inName,"repairAlertClicked") ) { return repairAlertClicked_dyn(); }
		if (HX_FIELD_EQ(inName,"payElectricUtility") ) { return payElectricUtility_dyn(); }
		if (HX_FIELD_EQ(inName,"machineReliability") ) { return machineReliability_dyn(); }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"animateRepairSprite") ) { return animateRepairSprite; }
		if (HX_FIELD_EQ(inName,"getpieMachineSprite") ) { return getpieMachineSprite_dyn(); }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"electricUtilityTimer") ) { return electricUtilityTimer; }
		break;
	case 21:
		if (HX_FIELD_EQ(inName,"pieMachineAlertSprite") ) { return pieMachineAlertSprite; }
		if (HX_FIELD_EQ(inName,"registerMachineEvents") ) { return registerMachineEvents_dyn(); }
		if (HX_FIELD_EQ(inName,"resetRepairAlertTimer") ) { return resetRepairAlertTimer_dyn(); }
		if (HX_FIELD_EQ(inName,"repairSpriteAnimation") ) { return repairSpriteAnimation_dyn(); }
		break;
	case 22:
		if (HX_FIELD_EQ(inName,"pieMachineFlyingPastry") ) { return pieMachineFlyingPastry; }
		if (HX_FIELD_EQ(inName,"getAnimateRepairSprite") ) { return getAnimateRepairSprite_dyn(); }
		if (HX_FIELD_EQ(inName,"setAnimateRepairSprite") ) { return setAnimateRepairSprite_dyn(); }
		break;
	case 23:
		if (HX_FIELD_EQ(inName,"machineReliabilityTimer") ) { return machineReliabilityTimer; }
		if (HX_FIELD_EQ(inName,"unregisterMachineEvents") ) { return unregisterMachineEvents_dyn(); }
		break;
	case 25:
		if (HX_FIELD_EQ(inName,"getPieMachineFlyingPastry") ) { return getPieMachineFlyingPastry_dyn(); }
		break;
	case 26:
		if (HX_FIELD_EQ(inName,"registerMachineButtonEvent") ) { return registerMachineButtonEvent_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic PieMachine_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"run") ) { run=inValue.Cast< Float >(); return inValue; }
		break;
	case 4:
		if (HX_FIELD_EQ(inName,"rise") ) { rise=inValue.Cast< Float >(); return inValue; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"myTile") ) { myTile=inValue.Cast< ::Tile >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"goldCount") ) { goldCount=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"machineOn") ) { machineOn=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"inPiePack") ) { inPiePack=inValue.Cast< bool >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"appleCount") ) { appleCount=inValue.Cast< int >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"myRelativeX") ) { myRelativeX=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"myRelativeY") ) { myRelativeY=inValue.Cast< Float >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"pastryFlying") ) { pastryFlying=inValue.Cast< bool >(); return inValue; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"repairClicked") ) { repairClicked=inValue.Cast< bool >(); return inValue; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"minusOneSprite") ) { minusOneSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"pieBakingSound") ) { pieBakingSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		if (HX_FIELD_EQ(inName,"machineClicked") ) { machineClicked=inValue.Cast< bool >(); return inValue; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"pieMachineGroup") ) { pieMachineGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"pieMachineSound") ) { pieMachineSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		if (HX_FIELD_EQ(inName,"payElectricBill") ) { payElectricBill=inValue.Cast< bool >(); return inValue; }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"pieMachineSprite") ) { pieMachineSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"repairAlertSprite") ) { repairAlertSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"machineAlertReset") ) { machineAlertReset=inValue.Cast< bool >(); return inValue; }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"animateRepairSprite") ) { animateRepairSprite=inValue.Cast< bool >(); return inValue; }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"electricUtilityTimer") ) { electricUtilityTimer=inValue.Cast< Float >(); return inValue; }
		break;
	case 21:
		if (HX_FIELD_EQ(inName,"pieMachineAlertSprite") ) { pieMachineAlertSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 22:
		if (HX_FIELD_EQ(inName,"pieMachineFlyingPastry") ) { pieMachineFlyingPastry=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 23:
		if (HX_FIELD_EQ(inName,"machineReliabilityTimer") ) { machineReliabilityTimer=inValue.Cast< Float >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void PieMachine_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("pieMachineGroup"));
	outFields->push(HX_CSTRING("pieMachineSprite"));
	outFields->push(HX_CSTRING("pieMachineAlertSprite"));
	outFields->push(HX_CSTRING("minusOneSprite"));
	outFields->push(HX_CSTRING("repairAlertSprite"));
	outFields->push(HX_CSTRING("pieMachineFlyingPastry"));
	outFields->push(HX_CSTRING("pieBakingSound"));
	outFields->push(HX_CSTRING("pieMachineSound"));
	outFields->push(HX_CSTRING("myRelativeX"));
	outFields->push(HX_CSTRING("myRelativeY"));
	outFields->push(HX_CSTRING("electricUtilityTimer"));
	outFields->push(HX_CSTRING("machineReliabilityTimer"));
	outFields->push(HX_CSTRING("rise"));
	outFields->push(HX_CSTRING("run"));
	outFields->push(HX_CSTRING("goldCount"));
	outFields->push(HX_CSTRING("appleCount"));
	outFields->push(HX_CSTRING("payElectricBill"));
	outFields->push(HX_CSTRING("machineOn"));
	outFields->push(HX_CSTRING("machineAlertReset"));
	outFields->push(HX_CSTRING("machineClicked"));
	outFields->push(HX_CSTRING("repairClicked"));
	outFields->push(HX_CSTRING("animateRepairSprite"));
	outFields->push(HX_CSTRING("pastryFlying"));
	outFields->push(HX_CSTRING("inPiePack"));
	outFields->push(HX_CSTRING("myTile"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(PieMachine_obj,pieMachineGroup),HX_CSTRING("pieMachineGroup")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(PieMachine_obj,pieMachineSprite),HX_CSTRING("pieMachineSprite")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(PieMachine_obj,pieMachineAlertSprite),HX_CSTRING("pieMachineAlertSprite")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(PieMachine_obj,minusOneSprite),HX_CSTRING("minusOneSprite")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(PieMachine_obj,repairAlertSprite),HX_CSTRING("repairAlertSprite")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(PieMachine_obj,pieMachineFlyingPastry),HX_CSTRING("pieMachineFlyingPastry")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(PieMachine_obj,pieBakingSound),HX_CSTRING("pieBakingSound")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(PieMachine_obj,pieMachineSound),HX_CSTRING("pieMachineSound")},
	{hx::fsFloat,(int)offsetof(PieMachine_obj,myRelativeX),HX_CSTRING("myRelativeX")},
	{hx::fsFloat,(int)offsetof(PieMachine_obj,myRelativeY),HX_CSTRING("myRelativeY")},
	{hx::fsFloat,(int)offsetof(PieMachine_obj,electricUtilityTimer),HX_CSTRING("electricUtilityTimer")},
	{hx::fsFloat,(int)offsetof(PieMachine_obj,machineReliabilityTimer),HX_CSTRING("machineReliabilityTimer")},
	{hx::fsFloat,(int)offsetof(PieMachine_obj,rise),HX_CSTRING("rise")},
	{hx::fsFloat,(int)offsetof(PieMachine_obj,run),HX_CSTRING("run")},
	{hx::fsInt,(int)offsetof(PieMachine_obj,goldCount),HX_CSTRING("goldCount")},
	{hx::fsInt,(int)offsetof(PieMachine_obj,appleCount),HX_CSTRING("appleCount")},
	{hx::fsBool,(int)offsetof(PieMachine_obj,payElectricBill),HX_CSTRING("payElectricBill")},
	{hx::fsBool,(int)offsetof(PieMachine_obj,machineOn),HX_CSTRING("machineOn")},
	{hx::fsBool,(int)offsetof(PieMachine_obj,machineAlertReset),HX_CSTRING("machineAlertReset")},
	{hx::fsBool,(int)offsetof(PieMachine_obj,machineClicked),HX_CSTRING("machineClicked")},
	{hx::fsBool,(int)offsetof(PieMachine_obj,repairClicked),HX_CSTRING("repairClicked")},
	{hx::fsBool,(int)offsetof(PieMachine_obj,animateRepairSprite),HX_CSTRING("animateRepairSprite")},
	{hx::fsBool,(int)offsetof(PieMachine_obj,pastryFlying),HX_CSTRING("pastryFlying")},
	{hx::fsBool,(int)offsetof(PieMachine_obj,inPiePack),HX_CSTRING("inPiePack")},
	{hx::fsObject /*::Tile*/ ,(int)offsetof(PieMachine_obj,myTile),HX_CSTRING("myTile")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("pieMachineGroup"),
	HX_CSTRING("pieMachineSprite"),
	HX_CSTRING("pieMachineAlertSprite"),
	HX_CSTRING("minusOneSprite"),
	HX_CSTRING("repairAlertSprite"),
	HX_CSTRING("pieMachineFlyingPastry"),
	HX_CSTRING("pieBakingSound"),
	HX_CSTRING("pieMachineSound"),
	HX_CSTRING("myRelativeX"),
	HX_CSTRING("myRelativeY"),
	HX_CSTRING("electricUtilityTimer"),
	HX_CSTRING("machineReliabilityTimer"),
	HX_CSTRING("rise"),
	HX_CSTRING("run"),
	HX_CSTRING("goldCount"),
	HX_CSTRING("appleCount"),
	HX_CSTRING("payElectricBill"),
	HX_CSTRING("machineOn"),
	HX_CSTRING("machineAlertReset"),
	HX_CSTRING("machineClicked"),
	HX_CSTRING("repairClicked"),
	HX_CSTRING("animateRepairSprite"),
	HX_CSTRING("pastryFlying"),
	HX_CSTRING("inPiePack"),
	HX_CSTRING("myTile"),
	HX_CSTRING("getpieMachineSprite"),
	HX_CSTRING("getInPiePack"),
	HX_CSTRING("getMyRelativeX"),
	HX_CSTRING("getPastryFlying"),
	HX_CSTRING("getPieMachineFlyingPastry"),
	HX_CSTRING("getMyRelativeY"),
	HX_CSTRING("getMachineOn"),
	HX_CSTRING("getPayElectricBill"),
	HX_CSTRING("getRepairClicked"),
	HX_CSTRING("getAnimateRepairSprite"),
	HX_CSTRING("setAnimateRepairSprite"),
	HX_CSTRING("setPastryFlying"),
	HX_CSTRING("setRepairClicked"),
	HX_CSTRING("setInPiePack"),
	HX_CSTRING("setPayElectricBill"),
	HX_CSTRING("setMachineOn"),
	HX_CSTRING("setMyRelativeX"),
	HX_CSTRING("setMyRelativeY"),
	HX_CSTRING("setGoldCount"),
	HX_CSTRING("setAppleCount"),
	HX_CSTRING("registerMachineEvents"),
	HX_CSTRING("unregisterMachineEvents"),
	HX_CSTRING("registerMachineButtonEvent"),
	HX_CSTRING("repairAlertClicked"),
	HX_CSTRING("turnMachineOn"),
	HX_CSTRING("payElectricUtility"),
	HX_CSTRING("machineReliability"),
	HX_CSTRING("resetRepairAlertTimer"),
	HX_CSTRING("repairSpriteAnimation"),
	HX_CSTRING("setRiseRun"),
	HX_CSTRING("sendPastryToPack"),
	HX_CSTRING("update"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(PieMachine_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(PieMachine_obj::__mClass,"__mClass");
};

#endif

Class PieMachine_obj::__mClass;

void PieMachine_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("PieMachine"), hx::TCanCast< PieMachine_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void PieMachine_obj::__boot()
{
}

