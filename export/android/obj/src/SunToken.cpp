#include <hxcpp.h>

#ifndef INCLUDED_SunToken
#include <SunToken.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxPooled
#include <flixel/interfaces/IFlxPooled.h>
#endif
#ifndef INCLUDED_flixel_system_FlxSound
#include <flixel/system/FlxSound.h>
#endif
#ifndef INCLUDED_flixel_system_frontEnds_SoundFrontEnd
#include <flixel/system/frontEnds/SoundFrontEnd.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPoint
#include <flixel/util/FlxPoint.h>
#endif

Void SunToken_obj::__construct()
{
HX_STACK_FRAME("SunToken","new",0x1320855f,"SunToken.new","SunToken.hx",12,0x52bd8af1)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(19)
	this->destroySun = false;
	HX_STACK_LINE(18)
	this->sunDone = false;
	HX_STACK_LINE(15)
	this->sunGroup = ::flixel::group::FlxGroup_obj::__new(null());
	HX_STACK_LINE(24)
	super::__construct(null());
	HX_STACK_LINE(25)
	::flixel::FlxSprite _g = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(25)
	this->sun = _g;
	HX_STACK_LINE(26)
	this->sun->loadGraphic(HX_CSTRING("assets/images/sunTokenAnimation.png"),true,(int)64,(int)64,null(),null());
	HX_STACK_LINE(27)
	this->sun->animation->add(HX_CSTRING("sun"),Array_obj< int >::__new().Add((int)0).Add((int)1).Add((int)2).Add((int)3).Add((int)4).Add((int)5).Add((int)6).Add((int)7),(int)10,true);
	HX_STACK_LINE(28)
	this->sun->animation->play(HX_CSTRING("sun"),null(),null());
	HX_STACK_LINE(29)
	::flixel::system::FlxSound _g1 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/sunshine.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
	HX_STACK_LINE(29)
	this->sunSound = _g1;
	HX_STACK_LINE(30)
	this->sunSound->play(null());
	HX_STACK_LINE(31)
	this->sun->set_alpha(0.0);
	HX_STACK_LINE(32)
	this->sun->scale->set_x(.6);
	HX_STACK_LINE(33)
	this->sun->scale->set_y(.6);
	HX_STACK_LINE(34)
	this->sunGroup->add(this->sun);
	HX_STACK_LINE(35)
	this->add(this->sunGroup);
}
;
	return null();
}

//SunToken_obj::~SunToken_obj() { }

Dynamic SunToken_obj::__CreateEmpty() { return  new SunToken_obj; }
hx::ObjectPtr< SunToken_obj > SunToken_obj::__new()
{  hx::ObjectPtr< SunToken_obj > result = new SunToken_obj();
	result->__construct();
	return result;}

Dynamic SunToken_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< SunToken_obj > result = new SunToken_obj();
	result->__construct();
	return result;}

Float SunToken_obj::getSunAlpha( ){
	HX_STACK_FRAME("SunToken","getSunAlpha",0x240a60e7,"SunToken.getSunAlpha","SunToken.hx",41,0x52bd8af1)
	HX_STACK_THIS(this)
	HX_STACK_LINE(41)
	return this->sun->alpha;
}


HX_DEFINE_DYNAMIC_FUNC0(SunToken_obj,getSunAlpha,return )

Void SunToken_obj::setSunX( Float xVal){
{
		HX_STACK_FRAME("SunToken","setSunX",0x6e45928d,"SunToken.setSunX","SunToken.hx",47,0x52bd8af1)
		HX_STACK_THIS(this)
		HX_STACK_ARG(xVal,"xVal")
		HX_STACK_LINE(47)
		this->sun->set_x(xVal);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SunToken_obj,setSunX,(void))

Void SunToken_obj::setSunY( Float yVal){
{
		HX_STACK_FRAME("SunToken","setSunY",0x6e45928e,"SunToken.setSunY","SunToken.hx",52,0x52bd8af1)
		HX_STACK_THIS(this)
		HX_STACK_ARG(yVal,"yVal")
		HX_STACK_LINE(52)
		this->sun->set_y(yVal);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(SunToken_obj,setSunY,(void))

Void SunToken_obj::fadeSun( ){
{
		HX_STACK_FRAME("SunToken","fadeSun",0x8da3e1ef,"SunToken.fadeSun","SunToken.hx",58,0x52bd8af1)
		HX_STACK_THIS(this)
		HX_STACK_LINE(58)
		if (((bool((this->sun->alpha < (int)1)) && bool(!(this->sunDone))))){
			HX_STACK_LINE(60)
			::flixel::FlxSprite _g = this->sun;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(60)
			_g->set_alpha((_g->alpha + .01));
		}
		else{
			HX_STACK_LINE(65)
			this->sunDone = true;
			HX_STACK_LINE(66)
			{
				HX_STACK_LINE(66)
				::flixel::FlxSprite _g = this->sun;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(66)
				_g->set_alpha((_g->alpha - .005));
			}
			HX_STACK_LINE(68)
			if (((this->sun->alpha <= (int)0))){
				HX_STACK_LINE(69)
				this->destroySun = true;
				HX_STACK_LINE(70)
				this->destroy();
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(SunToken_obj,fadeSun,(void))

Void SunToken_obj::update( ){
{
		HX_STACK_FRAME("SunToken","update",0xdeaeba6a,"SunToken.update","SunToken.hx",77,0x52bd8af1)
		HX_STACK_THIS(this)
		HX_STACK_LINE(79)
		this->fadeSun();
		HX_STACK_LINE(81)
		if ((!(this->destroySun))){
			HX_STACK_LINE(82)
			this->super::update();
		}
	}
return null();
}


Void SunToken_obj::destroy( ){
{
		HX_STACK_FRAME("SunToken","destroy",0x80eacd79,"SunToken.destroy","SunToken.hx",86,0x52bd8af1)
		HX_STACK_THIS(this)
		HX_STACK_LINE(87)
		this->sunGroup = null();
		HX_STACK_LINE(88)
		this->sun = null();
		HX_STACK_LINE(89)
		this->sunSound = null();
		HX_STACK_LINE(90)
		this->destroy_dyn();
		HX_STACK_LINE(91)
		this->super::destroy();
	}
return null();
}



SunToken_obj::SunToken_obj()
{
}

void SunToken_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(SunToken);
	HX_MARK_MEMBER_NAME(sunGroup,"sunGroup");
	HX_MARK_MEMBER_NAME(sun,"sun");
	HX_MARK_MEMBER_NAME(sunSound,"sunSound");
	HX_MARK_MEMBER_NAME(sunDone,"sunDone");
	HX_MARK_MEMBER_NAME(destroySun,"destroySun");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void SunToken_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(sunGroup,"sunGroup");
	HX_VISIT_MEMBER_NAME(sun,"sun");
	HX_VISIT_MEMBER_NAME(sunSound,"sunSound");
	HX_VISIT_MEMBER_NAME(sunDone,"sunDone");
	HX_VISIT_MEMBER_NAME(destroySun,"destroySun");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic SunToken_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"sun") ) { return sun; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"sunDone") ) { return sunDone; }
		if (HX_FIELD_EQ(inName,"setSunX") ) { return setSunX_dyn(); }
		if (HX_FIELD_EQ(inName,"setSunY") ) { return setSunY_dyn(); }
		if (HX_FIELD_EQ(inName,"fadeSun") ) { return fadeSun_dyn(); }
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"sunGroup") ) { return sunGroup; }
		if (HX_FIELD_EQ(inName,"sunSound") ) { return sunSound; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"destroySun") ) { return destroySun; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"getSunAlpha") ) { return getSunAlpha_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic SunToken_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"sun") ) { sun=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"sunDone") ) { sunDone=inValue.Cast< bool >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"sunGroup") ) { sunGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"sunSound") ) { sunSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"destroySun") ) { destroySun=inValue.Cast< bool >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void SunToken_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("sunGroup"));
	outFields->push(HX_CSTRING("sun"));
	outFields->push(HX_CSTRING("sunSound"));
	outFields->push(HX_CSTRING("sunDone"));
	outFields->push(HX_CSTRING("destroySun"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(SunToken_obj,sunGroup),HX_CSTRING("sunGroup")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(SunToken_obj,sun),HX_CSTRING("sun")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(SunToken_obj,sunSound),HX_CSTRING("sunSound")},
	{hx::fsBool,(int)offsetof(SunToken_obj,sunDone),HX_CSTRING("sunDone")},
	{hx::fsBool,(int)offsetof(SunToken_obj,destroySun),HX_CSTRING("destroySun")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("sunGroup"),
	HX_CSTRING("sun"),
	HX_CSTRING("sunSound"),
	HX_CSTRING("sunDone"),
	HX_CSTRING("destroySun"),
	HX_CSTRING("getSunAlpha"),
	HX_CSTRING("setSunX"),
	HX_CSTRING("setSunY"),
	HX_CSTRING("fadeSun"),
	HX_CSTRING("update"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(SunToken_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(SunToken_obj::__mClass,"__mClass");
};

#endif

Class SunToken_obj::__mClass;

void SunToken_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("SunToken"), hx::TCanCast< SunToken_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void SunToken_obj::__boot()
{
}

