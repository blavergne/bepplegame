#include <hxcpp.h>

#ifndef INCLUDED_Tile
#include <Tile.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif

Void Tile_obj::__construct(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y)
{
HX_STACK_FRAME("Tile","new",0x479a5740,"Tile.new","Tile.hx",9,0x12b05030)
HX_STACK_THIS(this)
HX_STACK_ARG(__o_X,"X")
HX_STACK_ARG(__o_Y,"Y")
Float X = __o_X.Default(0);
Float Y = __o_Y.Default(0);
{
	HX_STACK_LINE(13)
	this->tileClicked = false;
	HX_STACK_LINE(12)
	this->tileOwned = false;
	HX_STACK_LINE(11)
	this->hasObject = false;
	HX_STACK_LINE(18)
	super::__construct(X,Y,null());
	HX_STACK_LINE(19)
	this->loadGraphic(HX_CSTRING("assets/images/tileAnimation.png"),false,(int)192,(int)96,null(),null());
	HX_STACK_LINE(20)
	this->animation->add(HX_CSTRING("owned"),Array_obj< int >::__new().Add((int)0),(int)1,false);
	HX_STACK_LINE(21)
	this->animation->add(HX_CSTRING("notOwned"),Array_obj< int >::__new().Add((int)1),(int)1,false);
	HX_STACK_LINE(22)
	this->animation->play(HX_CSTRING("notOwned"),null(),null());
	HX_STACK_LINE(23)
	this->set_x((int)223);
	HX_STACK_LINE(24)
	this->set_y((int)120);
}
;
	return null();
}

//Tile_obj::~Tile_obj() { }

Dynamic Tile_obj::__CreateEmpty() { return  new Tile_obj; }
hx::ObjectPtr< Tile_obj > Tile_obj::__new(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y)
{  hx::ObjectPtr< Tile_obj > result = new Tile_obj();
	result->__construct(__o_X,__o_Y);
	return result;}

Dynamic Tile_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Tile_obj > result = new Tile_obj();
	result->__construct(inArgs[0],inArgs[1]);
	return result;}

bool Tile_obj::getTileOwned( ){
	HX_STACK_FRAME("Tile","getTileOwned",0xaf0a2fc1,"Tile.getTileOwned","Tile.hx",29,0x12b05030)
	HX_STACK_THIS(this)
	HX_STACK_LINE(29)
	return this->tileOwned;
}


HX_DEFINE_DYNAMIC_FUNC0(Tile_obj,getTileOwned,return )

bool Tile_obj::getHasObject( ){
	HX_STACK_FRAME("Tile","getHasObject",0x41606b63,"Tile.getHasObject","Tile.hx",34,0x12b05030)
	HX_STACK_THIS(this)
	HX_STACK_LINE(34)
	return this->hasObject;
}


HX_DEFINE_DYNAMIC_FUNC0(Tile_obj,getHasObject,return )

bool Tile_obj::getTileClicked( ){
	HX_STACK_FRAME("Tile","getTileClicked",0xee8a2d43,"Tile.getTileClicked","Tile.hx",39,0x12b05030)
	HX_STACK_THIS(this)
	HX_STACK_LINE(39)
	return this->tileClicked;
}


HX_DEFINE_DYNAMIC_FUNC0(Tile_obj,getTileClicked,return )

Void Tile_obj::setTileClicked( bool val){
{
		HX_STACK_FRAME("Tile","setTileClicked",0x0eaa15b7,"Tile.setTileClicked","Tile.hx",43,0x12b05030)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(43)
		this->tileClicked = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tile_obj,setTileClicked,(void))

Void Tile_obj::setTileOwned( bool val){
{
		HX_STACK_FRAME("Tile","setTileOwned",0xc4035335,"Tile.setTileOwned","Tile.hx",47,0x12b05030)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(47)
		this->tileOwned = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tile_obj,setTileOwned,(void))

Void Tile_obj::setHasObject( bool val){
{
		HX_STACK_FRAME("Tile","setHasObject",0x56598ed7,"Tile.setHasObject","Tile.hx",52,0x12b05030)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(52)
		this->hasObject = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Tile_obj,setHasObject,(void))

Void Tile_obj::setAsOwned( ){
{
		HX_STACK_FRAME("Tile","setAsOwned",0x1c89bf71,"Tile.setAsOwned","Tile.hx",56,0x12b05030)
		HX_STACK_THIS(this)
		HX_STACK_LINE(57)
		this->animation->play(HX_CSTRING("owned"),null(),null());
		HX_STACK_LINE(58)
		this->tileOwned = true;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tile_obj,setAsOwned,(void))

Void Tile_obj::setAsUnowned( ){
{
		HX_STACK_FRAME("Tile","setAsUnowned",0xa1ac2178,"Tile.setAsUnowned","Tile.hx",61,0x12b05030)
		HX_STACK_THIS(this)
		HX_STACK_LINE(62)
		this->animation->play(HX_CSTRING("notOwned"),null(),null());
		HX_STACK_LINE(63)
		this->tileOwned = false;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Tile_obj,setAsUnowned,(void))

Void Tile_obj::update( ){
{
		HX_STACK_FRAME("Tile","update",0xb6268b29,"Tile.update","Tile.hx",68,0x12b05030)
		HX_STACK_THIS(this)
		HX_STACK_LINE(68)
		this->super::update();
	}
return null();
}



Tile_obj::Tile_obj()
{
}

void Tile_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Tile);
	HX_MARK_MEMBER_NAME(hasObject,"hasObject");
	HX_MARK_MEMBER_NAME(tileOwned,"tileOwned");
	HX_MARK_MEMBER_NAME(tileClicked,"tileClicked");
	HX_MARK_MEMBER_NAME(buyMeSprite,"buyMeSprite");
	::flixel::FlxSprite_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void Tile_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(hasObject,"hasObject");
	HX_VISIT_MEMBER_NAME(tileOwned,"tileOwned");
	HX_VISIT_MEMBER_NAME(tileClicked,"tileClicked");
	HX_VISIT_MEMBER_NAME(buyMeSprite,"buyMeSprite");
	::flixel::FlxSprite_obj::__Visit(HX_VISIT_ARG);
}

Dynamic Tile_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"hasObject") ) { return hasObject; }
		if (HX_FIELD_EQ(inName,"tileOwned") ) { return tileOwned; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"setAsOwned") ) { return setAsOwned_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"tileClicked") ) { return tileClicked; }
		if (HX_FIELD_EQ(inName,"buyMeSprite") ) { return buyMeSprite; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"getTileOwned") ) { return getTileOwned_dyn(); }
		if (HX_FIELD_EQ(inName,"getHasObject") ) { return getHasObject_dyn(); }
		if (HX_FIELD_EQ(inName,"setTileOwned") ) { return setTileOwned_dyn(); }
		if (HX_FIELD_EQ(inName,"setHasObject") ) { return setHasObject_dyn(); }
		if (HX_FIELD_EQ(inName,"setAsUnowned") ) { return setAsUnowned_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"getTileClicked") ) { return getTileClicked_dyn(); }
		if (HX_FIELD_EQ(inName,"setTileClicked") ) { return setTileClicked_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Tile_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 9:
		if (HX_FIELD_EQ(inName,"hasObject") ) { hasObject=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tileOwned") ) { tileOwned=inValue.Cast< bool >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"tileClicked") ) { tileClicked=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"buyMeSprite") ) { buyMeSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Tile_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("hasObject"));
	outFields->push(HX_CSTRING("tileOwned"));
	outFields->push(HX_CSTRING("tileClicked"));
	outFields->push(HX_CSTRING("buyMeSprite"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsBool,(int)offsetof(Tile_obj,hasObject),HX_CSTRING("hasObject")},
	{hx::fsBool,(int)offsetof(Tile_obj,tileOwned),HX_CSTRING("tileOwned")},
	{hx::fsBool,(int)offsetof(Tile_obj,tileClicked),HX_CSTRING("tileClicked")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Tile_obj,buyMeSprite),HX_CSTRING("buyMeSprite")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("hasObject"),
	HX_CSTRING("tileOwned"),
	HX_CSTRING("tileClicked"),
	HX_CSTRING("buyMeSprite"),
	HX_CSTRING("getTileOwned"),
	HX_CSTRING("getHasObject"),
	HX_CSTRING("getTileClicked"),
	HX_CSTRING("setTileClicked"),
	HX_CSTRING("setTileOwned"),
	HX_CSTRING("setHasObject"),
	HX_CSTRING("setAsOwned"),
	HX_CSTRING("setAsUnowned"),
	HX_CSTRING("update"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Tile_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Tile_obj::__mClass,"__mClass");
};

#endif

Class Tile_obj::__mClass;

void Tile_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("Tile"), hx::TCanCast< Tile_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void Tile_obj::__boot()
{
}

