#include <hxcpp.h>

#ifndef INCLUDED_IMap
#include <IMap.h>
#endif
#ifndef INCLUDED_Mouse
#include <Mouse.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_system_FlxSound
#include <flixel/system/FlxSound.h>
#endif
#ifndef INCLUDED_flixel_system_frontEnds_SoundFrontEnd
#include <flixel/system/frontEnds/SoundFrontEnd.h>
#endif
#ifndef INCLUDED_haxe_ds_IntMap
#include <haxe/ds/IntMap.h>
#endif

Void Mouse_obj::__construct()
{
HX_STACK_FRAME("Mouse","new",0x820a0d77,"Mouse.new","Mouse.hx",12,0x7db68dd9)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(39)
	this->mouseMovementSpeed = (int)1;
	HX_STACK_LINE(38)
	this->homeAnimationPause = (int)3;
	HX_STACK_LINE(37)
	this->mouseJumpPause = (int)1;
	HX_STACK_LINE(36)
	this->mouseWalkPause = (int)0;
	HX_STACK_LINE(35)
	this->lastCheck = false;
	HX_STACK_LINE(34)
	this->mouseAllDone = false;
	HX_STACK_LINE(33)
	this->thirdFinishRetired = false;
	HX_STACK_LINE(32)
	this->secondFinishRetired = false;
	HX_STACK_LINE(31)
	this->firstFinishRetired = false;
	HX_STACK_LINE(30)
	this->timeToCloseHole = false;
	HX_STACK_LINE(29)
	this->mouseCollision = false;
	HX_STACK_LINE(28)
	this->thirdPhase = false;
	HX_STACK_LINE(27)
	this->secondPhase = false;
	HX_STACK_LINE(26)
	this->firstPhase = true;
	HX_STACK_LINE(25)
	this->removeMe = false;
	HX_STACK_LINE(24)
	this->holeClosed = false;
	HX_STACK_LINE(23)
	this->timeToDie = false;
	HX_STACK_LINE(22)
	this->goHome = false;
	HX_STACK_LINE(21)
	this->turnAround = false;
	HX_STACK_LINE(20)
	this->mouseWalking = false;
	HX_STACK_LINE(43)
	super::__construct(null());
	HX_STACK_LINE(44)
	this->initMouse();
}
;
	return null();
}

//Mouse_obj::~Mouse_obj() { }

Dynamic Mouse_obj::__CreateEmpty() { return  new Mouse_obj; }
hx::ObjectPtr< Mouse_obj > Mouse_obj::__new()
{  hx::ObjectPtr< Mouse_obj > result = new Mouse_obj();
	result->__construct();
	return result;}

Dynamic Mouse_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Mouse_obj > result = new Mouse_obj();
	result->__construct();
	return result;}

Void Mouse_obj::initMouse( ){
{
		HX_STACK_FRAME("Mouse","initMouse",0x2fe6d94c,"Mouse.initMouse","Mouse.hx",48,0x7db68dd9)
		HX_STACK_THIS(this)
		HX_STACK_LINE(49)
		::flixel::group::FlxGroup _g = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(49)
		this->mouseGroup = _g;
		HX_STACK_LINE(50)
		::flixel::FlxSprite _g1 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(50)
		this->mouse = _g1;
		HX_STACK_LINE(51)
		::flixel::FlxSprite _g2 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(51)
		this->mouseHole = _g2;
		HX_STACK_LINE(52)
		this->mouseHole->loadGraphic(HX_CSTRING("assets/images/holeAnimation.png"),true,(int)16,(int)16,null(),null());
		HX_STACK_LINE(53)
		this->mouseHole->animation->add(HX_CSTRING("makeHole"),Array_obj< int >::__new().Add((int)0).Add((int)1).Add((int)2).Add((int)3),(int)6,false);
		HX_STACK_LINE(54)
		this->mouseHole->animation->add(HX_CSTRING("closeHole"),Array_obj< int >::__new().Add((int)3).Add((int)2).Add((int)1).Add((int)0),(int)2,false);
		HX_STACK_LINE(55)
		this->mouse->loadGraphic(HX_CSTRING("assets/images/mouseAnimation.png"),true,(int)16,(int)16,null(),null());
		HX_STACK_LINE(56)
		this->mouse->animation->add(HX_CSTRING("mouseJump"),Array_obj< int >::__new().Add((int)0).Add((int)1).Add((int)2).Add((int)3).Add((int)4).Add((int)5).Add((int)6).Add((int)7).Add((int)8).Add((int)9).Add((int)10).Add((int)11).Add((int)12).Add((int)13).Add((int)14).Add((int)15).Add((int)16).Add((int)17),(int)10,false);
		HX_STACK_LINE(57)
		this->mouse->animation->add(HX_CSTRING("mouseHome"),Array_obj< int >::__new().Add((int)17).Add((int)16).Add((int)15).Add((int)14).Add((int)13).Add((int)12).Add((int)11),(int)10,false);
		HX_STACK_LINE(58)
		this->mouse->animation->add(HX_CSTRING("mouseWalk"),Array_obj< int >::__new().Add((int)18).Add((int)19).Add((int)20).Add((int)21),(int)10,true);
		HX_STACK_LINE(59)
		::flixel::system::FlxSound _g3 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/mouseHoleSound.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(59)
		this->mouseHoleSound = _g3;
		HX_STACK_LINE(60)
		::flixel::system::FlxSound _g4 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/mouseWalkingSound.wav"),(int)1,true,null(),null(),null(),null());		HX_STACK_VAR(_g4,"_g4");
		HX_STACK_LINE(60)
		this->mouseWalkSound = _g4;
		struct _Function_1_1{
			inline static Dynamic Block( ){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","Mouse.hx",61,0x7db68dd9)
				{
					hx::Anon __result = hx::Anon_obj::Create();
					__result->Add(HX_CSTRING("x") , false,false);
					__result->Add(HX_CSTRING("y") , false,false);
					return __result;
				}
				return null();
			}
		};
		HX_STACK_LINE(61)
		this->mouse->_facingFlip->set((int)1,_Function_1_1::Block());
		struct _Function_1_2{
			inline static Dynamic Block( ){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","Mouse.hx",62,0x7db68dd9)
				{
					hx::Anon __result = hx::Anon_obj::Create();
					__result->Add(HX_CSTRING("x") , true,false);
					__result->Add(HX_CSTRING("y") , false,false);
					return __result;
				}
				return null();
			}
		};
		HX_STACK_LINE(62)
		this->mouse->_facingFlip->set((int)16,_Function_1_2::Block());
		HX_STACK_LINE(63)
		this->mouseGroup->add(this->mouse);
		HX_STACK_LINE(64)
		this->mouseGroup->add(this->mouseHole);
		HX_STACK_LINE(65)
		this->add(this->mouseGroup);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Mouse_obj,initMouse,(void))

Void Mouse_obj::mouseWork( ){
{
		HX_STACK_FRAME("Mouse","mouseWork",0x034162ed,"Mouse.mouseWork","Mouse.hx",69,0x7db68dd9)
		HX_STACK_THIS(this)
		HX_STACK_LINE(70)
		if ((this->firstPhase)){
			HX_STACK_LINE(71)
			this->mouseHoleSound->play(null());
			HX_STACK_LINE(72)
			this->mouseHole->animation->play(HX_CSTRING("makeHole"),null(),null());
			HX_STACK_LINE(73)
			this->mouse->animation->play(HX_CSTRING("mouseJump"),null(),null());
			HX_STACK_LINE(74)
			this->firstPhase = false;
		}
		HX_STACK_LINE(76)
		if (((  ((this->mouse->animation->get_finished())) ? bool(!(this->firstFinishRetired)) : bool(false) ))){
			HX_STACK_LINE(77)
			this->secondPhase = true;
			HX_STACK_LINE(78)
			this->firstFinishRetired = true;
		}
		HX_STACK_LINE(80)
		if ((this->secondPhase)){
			HX_STACK_LINE(81)
			hx::SubEq(this->mouseWalkPause,::flixel::FlxG_obj::elapsed);
			HX_STACK_LINE(82)
			if (((this->mouseWalkPause < (int)0))){
				HX_STACK_LINE(83)
				this->mouseWalkSound->play(null());
				HX_STACK_LINE(84)
				this->mouse->animation->play(HX_CSTRING("mouseWalk"),null(),null());
				HX_STACK_LINE(85)
				this->mouseWalkPause = .1;
				HX_STACK_LINE(86)
				if ((!(this->mouseCollision))){
					HX_STACK_LINE(87)
					::flixel::FlxSprite _g = this->mouse;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(87)
					_g->set_x((_g->x - (int)2));
				}
				else{
					HX_STACK_LINE(90)
					this->turnAround = true;
					HX_STACK_LINE(91)
					this->mouse->set_facing((int)16);
					HX_STACK_LINE(92)
					{
						HX_STACK_LINE(92)
						::flixel::FlxSprite _g = this->mouse;		HX_STACK_VAR(_g,"_g");
						HX_STACK_LINE(92)
						_g->set_x((_g->x + (int)2));
					}
				}
			}
		}
		HX_STACK_LINE(97)
		if (((bool(this->turnAround) && bool((this->mouse->x == this->mouseHole->x))))){
			HX_STACK_LINE(98)
			{
				HX_STACK_LINE(98)
				::flixel::system::FlxSound _this = this->mouseWalkSound;		HX_STACK_VAR(_this,"_this");
				HX_STACK_LINE(98)
				_this->cleanup(_this->autoDestroy,true,true);
				HX_STACK_LINE(98)
				_this;
			}
			HX_STACK_LINE(99)
			this->secondPhase = false;
			HX_STACK_LINE(100)
			this->mouse->animation->play(HX_CSTRING("mouseHome"),null(),null());
			HX_STACK_LINE(101)
			this->turnAround = false;
		}
		HX_STACK_LINE(103)
		if (((  ((this->mouse->animation->get_finished())) ? bool(!(this->secondFinishRetired)) : bool(false) ))){
			HX_STACK_LINE(104)
			this->thirdPhase = true;
			HX_STACK_LINE(105)
			this->secondFinishRetired = true;
		}
		HX_STACK_LINE(107)
		if ((this->thirdPhase)){
			HX_STACK_LINE(108)
			this->mouseHoleSound->play(null());
			HX_STACK_LINE(109)
			this->mouseHole->animation->play(HX_CSTRING("closeHole"),null(),null());
			HX_STACK_LINE(110)
			this->thirdPhase = false;
			HX_STACK_LINE(111)
			this->lastCheck = true;
		}
		HX_STACK_LINE(113)
		if (((  (((  ((this->mouseHole->animation->get_finished())) ? bool(!(this->thirdFinishRetired)) : bool(false) ))) ? bool(this->lastCheck) : bool(false) ))){
			HX_STACK_LINE(114)
			this->mouseAllDone = true;
			HX_STACK_LINE(115)
			this->thirdFinishRetired = true;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Mouse_obj,mouseWork,(void))

::flixel::FlxSprite Mouse_obj::getMouseSprite( ){
	HX_STACK_FRAME("Mouse","getMouseSprite",0xd4181f5d,"Mouse.getMouseSprite","Mouse.hx",121,0x7db68dd9)
	HX_STACK_THIS(this)
	HX_STACK_LINE(121)
	return this->mouse;
}


HX_DEFINE_DYNAMIC_FUNC0(Mouse_obj,getMouseSprite,return )

bool Mouse_obj::getMouseAllDone( ){
	HX_STACK_FRAME("Mouse","getMouseAllDone",0xae3363eb,"Mouse.getMouseAllDone","Mouse.hx",125,0x7db68dd9)
	HX_STACK_THIS(this)
	HX_STACK_LINE(125)
	return this->mouseAllDone;
}


HX_DEFINE_DYNAMIC_FUNC0(Mouse_obj,getMouseAllDone,return )

::flixel::FlxSprite Mouse_obj::getHoleSprite( ){
	HX_STACK_FRAME("Mouse","getHoleSprite",0x078a0572,"Mouse.getHoleSprite","Mouse.hx",129,0x7db68dd9)
	HX_STACK_THIS(this)
	HX_STACK_LINE(129)
	return this->mouseHole;
}


HX_DEFINE_DYNAMIC_FUNC0(Mouse_obj,getHoleSprite,return )

Void Mouse_obj::setMouseCollision( bool val){
{
		HX_STACK_FRAME("Mouse","setMouseCollision",0x2edce426,"Mouse.setMouseCollision","Mouse.hx",133,0x7db68dd9)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(133)
		this->mouseCollision = true;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Mouse_obj,setMouseCollision,(void))

Void Mouse_obj::destroy( ){
{
		HX_STACK_FRAME("Mouse","destroy",0x68d64191,"Mouse.destroy","Mouse.hx",136,0x7db68dd9)
		HX_STACK_THIS(this)
		HX_STACK_LINE(137)
		this->mouseGroup = null();
		HX_STACK_LINE(138)
		this->mouse = null();
		HX_STACK_LINE(139)
		this->mouseHole = null();
		HX_STACK_LINE(140)
		this->mouseHoleSound = null();
		HX_STACK_LINE(141)
		this->mouseWalkSound = null();
		HX_STACK_LINE(142)
		this->super::destroy();
	}
return null();
}


Void Mouse_obj::faceRight( ){
{
		HX_STACK_FRAME("Mouse","faceRight",0xb3a69656,"Mouse.faceRight","Mouse.hx",146,0x7db68dd9)
		HX_STACK_THIS(this)
		HX_STACK_LINE(146)
		this->mouse->set_facing((int)16);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Mouse_obj,faceRight,(void))

Void Mouse_obj::update( ){
{
		HX_STACK_FRAME("Mouse","update",0xdc475152,"Mouse.update","Mouse.hx",149,0x7db68dd9)
		HX_STACK_THIS(this)
		HX_STACK_LINE(151)
		this->mouseWork();
		HX_STACK_LINE(152)
		this->super::update();
	}
return null();
}



Mouse_obj::Mouse_obj()
{
}

void Mouse_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Mouse);
	HX_MARK_MEMBER_NAME(mouseGroup,"mouseGroup");
	HX_MARK_MEMBER_NAME(mouse,"mouse");
	HX_MARK_MEMBER_NAME(mouseHole,"mouseHole");
	HX_MARK_MEMBER_NAME(mouseHoleSound,"mouseHoleSound");
	HX_MARK_MEMBER_NAME(mouseWalkSound,"mouseWalkSound");
	HX_MARK_MEMBER_NAME(mouseWalking,"mouseWalking");
	HX_MARK_MEMBER_NAME(turnAround,"turnAround");
	HX_MARK_MEMBER_NAME(goHome,"goHome");
	HX_MARK_MEMBER_NAME(timeToDie,"timeToDie");
	HX_MARK_MEMBER_NAME(holeClosed,"holeClosed");
	HX_MARK_MEMBER_NAME(removeMe,"removeMe");
	HX_MARK_MEMBER_NAME(firstPhase,"firstPhase");
	HX_MARK_MEMBER_NAME(secondPhase,"secondPhase");
	HX_MARK_MEMBER_NAME(thirdPhase,"thirdPhase");
	HX_MARK_MEMBER_NAME(mouseCollision,"mouseCollision");
	HX_MARK_MEMBER_NAME(timeToCloseHole,"timeToCloseHole");
	HX_MARK_MEMBER_NAME(firstFinishRetired,"firstFinishRetired");
	HX_MARK_MEMBER_NAME(secondFinishRetired,"secondFinishRetired");
	HX_MARK_MEMBER_NAME(thirdFinishRetired,"thirdFinishRetired");
	HX_MARK_MEMBER_NAME(mouseAllDone,"mouseAllDone");
	HX_MARK_MEMBER_NAME(lastCheck,"lastCheck");
	HX_MARK_MEMBER_NAME(mouseWalkPause,"mouseWalkPause");
	HX_MARK_MEMBER_NAME(mouseJumpPause,"mouseJumpPause");
	HX_MARK_MEMBER_NAME(homeAnimationPause,"homeAnimationPause");
	HX_MARK_MEMBER_NAME(mouseMovementSpeed,"mouseMovementSpeed");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void Mouse_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(mouseGroup,"mouseGroup");
	HX_VISIT_MEMBER_NAME(mouse,"mouse");
	HX_VISIT_MEMBER_NAME(mouseHole,"mouseHole");
	HX_VISIT_MEMBER_NAME(mouseHoleSound,"mouseHoleSound");
	HX_VISIT_MEMBER_NAME(mouseWalkSound,"mouseWalkSound");
	HX_VISIT_MEMBER_NAME(mouseWalking,"mouseWalking");
	HX_VISIT_MEMBER_NAME(turnAround,"turnAround");
	HX_VISIT_MEMBER_NAME(goHome,"goHome");
	HX_VISIT_MEMBER_NAME(timeToDie,"timeToDie");
	HX_VISIT_MEMBER_NAME(holeClosed,"holeClosed");
	HX_VISIT_MEMBER_NAME(removeMe,"removeMe");
	HX_VISIT_MEMBER_NAME(firstPhase,"firstPhase");
	HX_VISIT_MEMBER_NAME(secondPhase,"secondPhase");
	HX_VISIT_MEMBER_NAME(thirdPhase,"thirdPhase");
	HX_VISIT_MEMBER_NAME(mouseCollision,"mouseCollision");
	HX_VISIT_MEMBER_NAME(timeToCloseHole,"timeToCloseHole");
	HX_VISIT_MEMBER_NAME(firstFinishRetired,"firstFinishRetired");
	HX_VISIT_MEMBER_NAME(secondFinishRetired,"secondFinishRetired");
	HX_VISIT_MEMBER_NAME(thirdFinishRetired,"thirdFinishRetired");
	HX_VISIT_MEMBER_NAME(mouseAllDone,"mouseAllDone");
	HX_VISIT_MEMBER_NAME(lastCheck,"lastCheck");
	HX_VISIT_MEMBER_NAME(mouseWalkPause,"mouseWalkPause");
	HX_VISIT_MEMBER_NAME(mouseJumpPause,"mouseJumpPause");
	HX_VISIT_MEMBER_NAME(homeAnimationPause,"homeAnimationPause");
	HX_VISIT_MEMBER_NAME(mouseMovementSpeed,"mouseMovementSpeed");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic Mouse_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"mouse") ) { return mouse; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"goHome") ) { return goHome; }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"removeMe") ) { return removeMe; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"mouseHole") ) { return mouseHole; }
		if (HX_FIELD_EQ(inName,"timeToDie") ) { return timeToDie; }
		if (HX_FIELD_EQ(inName,"lastCheck") ) { return lastCheck; }
		if (HX_FIELD_EQ(inName,"initMouse") ) { return initMouse_dyn(); }
		if (HX_FIELD_EQ(inName,"mouseWork") ) { return mouseWork_dyn(); }
		if (HX_FIELD_EQ(inName,"faceRight") ) { return faceRight_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"mouseGroup") ) { return mouseGroup; }
		if (HX_FIELD_EQ(inName,"turnAround") ) { return turnAround; }
		if (HX_FIELD_EQ(inName,"holeClosed") ) { return holeClosed; }
		if (HX_FIELD_EQ(inName,"firstPhase") ) { return firstPhase; }
		if (HX_FIELD_EQ(inName,"thirdPhase") ) { return thirdPhase; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"secondPhase") ) { return secondPhase; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"mouseWalking") ) { return mouseWalking; }
		if (HX_FIELD_EQ(inName,"mouseAllDone") ) { return mouseAllDone; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"getHoleSprite") ) { return getHoleSprite_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"mouseHoleSound") ) { return mouseHoleSound; }
		if (HX_FIELD_EQ(inName,"mouseWalkSound") ) { return mouseWalkSound; }
		if (HX_FIELD_EQ(inName,"mouseCollision") ) { return mouseCollision; }
		if (HX_FIELD_EQ(inName,"mouseWalkPause") ) { return mouseWalkPause; }
		if (HX_FIELD_EQ(inName,"mouseJumpPause") ) { return mouseJumpPause; }
		if (HX_FIELD_EQ(inName,"getMouseSprite") ) { return getMouseSprite_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"timeToCloseHole") ) { return timeToCloseHole; }
		if (HX_FIELD_EQ(inName,"getMouseAllDone") ) { return getMouseAllDone_dyn(); }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"setMouseCollision") ) { return setMouseCollision_dyn(); }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"firstFinishRetired") ) { return firstFinishRetired; }
		if (HX_FIELD_EQ(inName,"thirdFinishRetired") ) { return thirdFinishRetired; }
		if (HX_FIELD_EQ(inName,"homeAnimationPause") ) { return homeAnimationPause; }
		if (HX_FIELD_EQ(inName,"mouseMovementSpeed") ) { return mouseMovementSpeed; }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"secondFinishRetired") ) { return secondFinishRetired; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Mouse_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"mouse") ) { mouse=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"goHome") ) { goHome=inValue.Cast< bool >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"removeMe") ) { removeMe=inValue.Cast< bool >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"mouseHole") ) { mouseHole=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"timeToDie") ) { timeToDie=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"lastCheck") ) { lastCheck=inValue.Cast< bool >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"mouseGroup") ) { mouseGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"turnAround") ) { turnAround=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"holeClosed") ) { holeClosed=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"firstPhase") ) { firstPhase=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"thirdPhase") ) { thirdPhase=inValue.Cast< bool >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"secondPhase") ) { secondPhase=inValue.Cast< bool >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"mouseWalking") ) { mouseWalking=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"mouseAllDone") ) { mouseAllDone=inValue.Cast< bool >(); return inValue; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"mouseHoleSound") ) { mouseHoleSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		if (HX_FIELD_EQ(inName,"mouseWalkSound") ) { mouseWalkSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		if (HX_FIELD_EQ(inName,"mouseCollision") ) { mouseCollision=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"mouseWalkPause") ) { mouseWalkPause=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"mouseJumpPause") ) { mouseJumpPause=inValue.Cast< Float >(); return inValue; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"timeToCloseHole") ) { timeToCloseHole=inValue.Cast< bool >(); return inValue; }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"firstFinishRetired") ) { firstFinishRetired=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"thirdFinishRetired") ) { thirdFinishRetired=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"homeAnimationPause") ) { homeAnimationPause=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"mouseMovementSpeed") ) { mouseMovementSpeed=inValue.Cast< int >(); return inValue; }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"secondFinishRetired") ) { secondFinishRetired=inValue.Cast< bool >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Mouse_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("mouseGroup"));
	outFields->push(HX_CSTRING("mouse"));
	outFields->push(HX_CSTRING("mouseHole"));
	outFields->push(HX_CSTRING("mouseHoleSound"));
	outFields->push(HX_CSTRING("mouseWalkSound"));
	outFields->push(HX_CSTRING("mouseWalking"));
	outFields->push(HX_CSTRING("turnAround"));
	outFields->push(HX_CSTRING("goHome"));
	outFields->push(HX_CSTRING("timeToDie"));
	outFields->push(HX_CSTRING("holeClosed"));
	outFields->push(HX_CSTRING("removeMe"));
	outFields->push(HX_CSTRING("firstPhase"));
	outFields->push(HX_CSTRING("secondPhase"));
	outFields->push(HX_CSTRING("thirdPhase"));
	outFields->push(HX_CSTRING("mouseCollision"));
	outFields->push(HX_CSTRING("timeToCloseHole"));
	outFields->push(HX_CSTRING("firstFinishRetired"));
	outFields->push(HX_CSTRING("secondFinishRetired"));
	outFields->push(HX_CSTRING("thirdFinishRetired"));
	outFields->push(HX_CSTRING("mouseAllDone"));
	outFields->push(HX_CSTRING("lastCheck"));
	outFields->push(HX_CSTRING("mouseWalkPause"));
	outFields->push(HX_CSTRING("mouseJumpPause"));
	outFields->push(HX_CSTRING("homeAnimationPause"));
	outFields->push(HX_CSTRING("mouseMovementSpeed"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(Mouse_obj,mouseGroup),HX_CSTRING("mouseGroup")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Mouse_obj,mouse),HX_CSTRING("mouse")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(Mouse_obj,mouseHole),HX_CSTRING("mouseHole")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(Mouse_obj,mouseHoleSound),HX_CSTRING("mouseHoleSound")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(Mouse_obj,mouseWalkSound),HX_CSTRING("mouseWalkSound")},
	{hx::fsBool,(int)offsetof(Mouse_obj,mouseWalking),HX_CSTRING("mouseWalking")},
	{hx::fsBool,(int)offsetof(Mouse_obj,turnAround),HX_CSTRING("turnAround")},
	{hx::fsBool,(int)offsetof(Mouse_obj,goHome),HX_CSTRING("goHome")},
	{hx::fsBool,(int)offsetof(Mouse_obj,timeToDie),HX_CSTRING("timeToDie")},
	{hx::fsBool,(int)offsetof(Mouse_obj,holeClosed),HX_CSTRING("holeClosed")},
	{hx::fsBool,(int)offsetof(Mouse_obj,removeMe),HX_CSTRING("removeMe")},
	{hx::fsBool,(int)offsetof(Mouse_obj,firstPhase),HX_CSTRING("firstPhase")},
	{hx::fsBool,(int)offsetof(Mouse_obj,secondPhase),HX_CSTRING("secondPhase")},
	{hx::fsBool,(int)offsetof(Mouse_obj,thirdPhase),HX_CSTRING("thirdPhase")},
	{hx::fsBool,(int)offsetof(Mouse_obj,mouseCollision),HX_CSTRING("mouseCollision")},
	{hx::fsBool,(int)offsetof(Mouse_obj,timeToCloseHole),HX_CSTRING("timeToCloseHole")},
	{hx::fsBool,(int)offsetof(Mouse_obj,firstFinishRetired),HX_CSTRING("firstFinishRetired")},
	{hx::fsBool,(int)offsetof(Mouse_obj,secondFinishRetired),HX_CSTRING("secondFinishRetired")},
	{hx::fsBool,(int)offsetof(Mouse_obj,thirdFinishRetired),HX_CSTRING("thirdFinishRetired")},
	{hx::fsBool,(int)offsetof(Mouse_obj,mouseAllDone),HX_CSTRING("mouseAllDone")},
	{hx::fsBool,(int)offsetof(Mouse_obj,lastCheck),HX_CSTRING("lastCheck")},
	{hx::fsFloat,(int)offsetof(Mouse_obj,mouseWalkPause),HX_CSTRING("mouseWalkPause")},
	{hx::fsFloat,(int)offsetof(Mouse_obj,mouseJumpPause),HX_CSTRING("mouseJumpPause")},
	{hx::fsFloat,(int)offsetof(Mouse_obj,homeAnimationPause),HX_CSTRING("homeAnimationPause")},
	{hx::fsInt,(int)offsetof(Mouse_obj,mouseMovementSpeed),HX_CSTRING("mouseMovementSpeed")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("mouseGroup"),
	HX_CSTRING("mouse"),
	HX_CSTRING("mouseHole"),
	HX_CSTRING("mouseHoleSound"),
	HX_CSTRING("mouseWalkSound"),
	HX_CSTRING("mouseWalking"),
	HX_CSTRING("turnAround"),
	HX_CSTRING("goHome"),
	HX_CSTRING("timeToDie"),
	HX_CSTRING("holeClosed"),
	HX_CSTRING("removeMe"),
	HX_CSTRING("firstPhase"),
	HX_CSTRING("secondPhase"),
	HX_CSTRING("thirdPhase"),
	HX_CSTRING("mouseCollision"),
	HX_CSTRING("timeToCloseHole"),
	HX_CSTRING("firstFinishRetired"),
	HX_CSTRING("secondFinishRetired"),
	HX_CSTRING("thirdFinishRetired"),
	HX_CSTRING("mouseAllDone"),
	HX_CSTRING("lastCheck"),
	HX_CSTRING("mouseWalkPause"),
	HX_CSTRING("mouseJumpPause"),
	HX_CSTRING("homeAnimationPause"),
	HX_CSTRING("mouseMovementSpeed"),
	HX_CSTRING("initMouse"),
	HX_CSTRING("mouseWork"),
	HX_CSTRING("getMouseSprite"),
	HX_CSTRING("getMouseAllDone"),
	HX_CSTRING("getHoleSprite"),
	HX_CSTRING("setMouseCollision"),
	HX_CSTRING("destroy"),
	HX_CSTRING("faceRight"),
	HX_CSTRING("update"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Mouse_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Mouse_obj::__mClass,"__mClass");
};

#endif

Class Mouse_obj::__mClass;

void Mouse_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("Mouse"), hx::TCanCast< Mouse_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void Mouse_obj::__boot()
{
}

