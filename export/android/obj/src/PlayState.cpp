#include <hxcpp.h>

#ifndef INCLUDED_AppleBasket
#include <AppleBasket.h>
#endif
#ifndef INCLUDED_AppleShed
#include <AppleShed.h>
#endif
#ifndef INCLUDED_AppleTruck
#include <AppleTruck.h>
#endif
#ifndef INCLUDED_AsteroidBelt
#include <AsteroidBelt.h>
#endif
#ifndef INCLUDED_Cloud
#include <Cloud.h>
#endif
#ifndef INCLUDED_FishIcon
#include <FishIcon.h>
#endif
#ifndef INCLUDED_GreenHouseIcon
#include <GreenHouseIcon.h>
#endif
#ifndef INCLUDED_Greenhouse
#include <Greenhouse.h>
#endif
#ifndef INCLUDED_Island
#include <Island.h>
#endif
#ifndef INCLUDED_JuiceIcon
#include <JuiceIcon.h>
#endif
#ifndef INCLUDED_JuiceMachine
#include <JuiceMachine.h>
#endif
#ifndef INCLUDED_JuicePackIcon
#include <JuicePackIcon.h>
#endif
#ifndef INCLUDED_KoiPond
#include <KoiPond.h>
#endif
#ifndef INCLUDED_Market
#include <Market.h>
#endif
#ifndef INCLUDED_MenuState
#include <MenuState.h>
#endif
#ifndef INCLUDED_NumberPad
#include <NumberPad.h>
#endif
#ifndef INCLUDED_OptionsScreen
#include <OptionsScreen.h>
#endif
#ifndef INCLUDED_PieIcon
#include <PieIcon.h>
#endif
#ifndef INCLUDED_PieMachine
#include <PieMachine.h>
#endif
#ifndef INCLUDED_PiePackIcon
#include <PiePackIcon.h>
#endif
#ifndef INCLUDED_PlayState
#include <PlayState.h>
#endif
#ifndef INCLUDED_ProblemBoard
#include <ProblemBoard.h>
#endif
#ifndef INCLUDED_Seed
#include <Seed.h>
#endif
#ifndef INCLUDED_Std
#include <Std.h>
#endif
#ifndef INCLUDED_SunToken
#include <SunToken.h>
#endif
#ifndef INCLUDED_SystemStar
#include <SystemStar.h>
#endif
#ifndef INCLUDED_Tile
#include <Tile.h>
#endif
#ifndef INCLUDED_TileBoard
#include <TileBoard.h>
#endif
#ifndef INCLUDED_TileBoarder
#include <TileBoarder.h>
#endif
#ifndef INCLUDED_Tree
#include <Tree.h>
#endif
#ifndef INCLUDED_TruckButton
#include <TruckButton.h>
#endif
#ifndef INCLUDED_Tutorial
#include <Tutorial.h>
#endif
#ifndef INCLUDED_Type
#include <Type.h>
#endif
#ifndef INCLUDED_Weed
#include <Weed.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxCamera
#include <flixel/FlxCamera.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxGame
#include <flixel/FlxGame.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_FlxState
#include <flixel/FlxState.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_input_mouse_FlxMouse
#include <flixel/input/mouse/FlxMouse.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxInput
#include <flixel/interfaces/IFlxInput.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxPooled
#include <flixel/interfaces/IFlxPooled.h>
#endif
#ifndef INCLUDED_flixel_plugin_FlxPlugin
#include <flixel/plugin/FlxPlugin.h>
#endif
#ifndef INCLUDED_flixel_plugin_MouseEventManager
#include <flixel/plugin/MouseEventManager.h>
#endif
#ifndef INCLUDED_flixel_system_FlxSound
#include <flixel/system/FlxSound.h>
#endif
#ifndef INCLUDED_flixel_system_frontEnds_SoundFrontEnd
#include <flixel/system/frontEnds/SoundFrontEnd.h>
#endif
#ifndef INCLUDED_flixel_text_FlxText
#include <flixel/text/FlxText.h>
#endif
#ifndef INCLUDED_flixel_ui_FlxButton
#include <flixel/ui/FlxButton.h>
#endif
#ifndef INCLUDED_flixel_ui_FlxTypedButton
#include <flixel/ui/FlxTypedButton.h>
#endif
#ifndef INCLUDED_flixel_util_FlxCollision
#include <flixel/util/FlxCollision.h>
#endif
#ifndef INCLUDED_flixel_util_FlxDestroyUtil
#include <flixel/util/FlxDestroyUtil.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPoint
#include <flixel/util/FlxPoint.h>
#endif
#ifndef INCLUDED_flixel_util_FlxRandom
#include <flixel/util/FlxRandom.h>
#endif
#ifndef INCLUDED_flixel_util_FlxTimer
#include <flixel/util/FlxTimer.h>
#endif
#ifndef INCLUDED_openfl__v2_display_DisplayObject
#include <openfl/_v2/display/DisplayObject.h>
#endif
#ifndef INCLUDED_openfl__v2_display_DisplayObjectContainer
#include <openfl/_v2/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_openfl__v2_display_IBitmapDrawable
#include <openfl/_v2/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_openfl__v2_display_InteractiveObject
#include <openfl/_v2/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_openfl__v2_display_Sprite
#include <openfl/_v2/display/Sprite.h>
#endif
#ifndef INCLUDED_openfl__v2_events_EventDispatcher
#include <openfl/_v2/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__v2_events_IEventDispatcher
#include <openfl/_v2/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__v2_media_SoundChannel
#include <openfl/_v2/media/SoundChannel.h>
#endif
#ifndef INCLUDED_openfl__v2_system_System
#include <openfl/_v2/system/System.h>
#endif

Void PlayState_obj::__construct(Dynamic MaxSize)
{
HX_STACK_FRAME("PlayState","new",0xf8bf96cf,"PlayState.new","PlayState.hx",30,0xb30d7781)
HX_STACK_THIS(this)
HX_STACK_ARG(MaxSize,"MaxSize")
{
	HX_STACK_LINE(138)
	this->mathless = ::MenuState_obj::mathless;
	HX_STACK_LINE(127)
	this->tutBuildBarBack = true;
	HX_STACK_LINE(126)
	this->tutUnpauseAlertsDone = false;
	HX_STACK_LINE(125)
	this->closeSecondTreeGrowthCheck = false;
	HX_STACK_LINE(124)
	this->tutorialWrongAnswersSkip = true;
	HX_STACK_LINE(123)
	this->callBackStopTwo = false;
	HX_STACK_LINE(122)
	this->callBackStopOne = true;
	HX_STACK_LINE(121)
	this->truckTutSection = true;
	HX_STACK_LINE(120)
	this->buildBarIsOpen = false;
	HX_STACK_LINE(118)
	this->marketOpenCheck = true;
	HX_STACK_LINE(117)
	this->closeAppleCountCheck = false;
	HX_STACK_LINE(116)
	this->closeTreeGrowthCheck = false;
	HX_STACK_LINE(115)
	this->ownedTile = false;
	HX_STACK_LINE(114)
	this->dropletClicked = false;
	HX_STACK_LINE(113)
	this->sunClicked = false;
	HX_STACK_LINE(112)
	this->overlaysInUse = false;
	HX_STACK_LINE(77)
	this->appleDisplayLock = true;
	HX_STACK_LINE(76)
	this->boardIsDown = false;
	HX_STACK_LINE(75)
	this->answerString = HX_CSTRING("");
	HX_STACK_LINE(74)
	this->appleCountString = HX_CSTRING("");
	HX_STACK_LINE(73)
	this->islandCreationTimer = (int)1;
	HX_STACK_LINE(72)
	this->buildBarOpenSpeed = (int)3;
	HX_STACK_LINE(71)
	this->appleAndJuiceForPieTimer = (int)1;
	HX_STACK_LINE(70)
	this->appleForJuiceTimer = (int)1;
	HX_STACK_LINE(69)
	this->leaveTruckTimer = (int)2;
	HX_STACK_LINE(68)
	this->loadAppleTimer = (int)1;
	HX_STACK_LINE(67)
	this->buildBarOpenTimer = (int)15;
	HX_STACK_LINE(65)
	this->currentTruckLvl = (int)1;
	HX_STACK_LINE(64)
	this->piePerTenCost = (int)25;
	HX_STACK_LINE(63)
	this->juicePerTenCost = (int)15;
	HX_STACK_LINE(62)
	this->applePerTenCost = (int)5;
	HX_STACK_LINE(61)
	this->buySignCost = (int)2;
	HX_STACK_LINE(60)
	this->appleCount = (int)0;
	HX_STACK_LINE(59)
	this->menuVisible = (int)0;
	HX_STACK_LINE(48)
	this->lastTileClicked = null();
	HX_STACK_LINE(30)
	super::__construct(MaxSize);
}
;
	return null();
}

//PlayState_obj::~PlayState_obj() { }

Dynamic PlayState_obj::__CreateEmpty() { return  new PlayState_obj; }
hx::ObjectPtr< PlayState_obj > PlayState_obj::__new(Dynamic MaxSize)
{  hx::ObjectPtr< PlayState_obj > result = new PlayState_obj();
	result->__construct(MaxSize);
	return result;}

Dynamic PlayState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< PlayState_obj > result = new PlayState_obj();
	result->__construct(inArgs[0]);
	return result;}

Void PlayState_obj::create( ){
{
		HX_STACK_FRAME("PlayState","create",0x82220fed,"PlayState.create","PlayState.hx",145,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(146)
		this->initPlaystate();
		HX_STACK_LINE(149)
		this->super::create();
	}
return null();
}


Void PlayState_obj::initPlaystate( ){
{
		HX_STACK_FRAME("PlayState","initPlaystate",0x4051bd9c,"PlayState.initPlaystate","PlayState.hx",153,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(157)
		::flixel::ui::FlxButton _g = ::flixel::ui::FlxButton_obj::__new((int)550,(int)450,HX_CSTRING("Menu"),this->openMenu_dyn());		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(157)
		this->menuButton = _g;
		HX_STACK_LINE(158)
		this->tutorialOn = ::MenuState_obj::tutorialOn;
		HX_STACK_LINE(159)
		this->set_bgColor((int)-10027009);
		HX_STACK_LINE(160)
		if ((this->tutorialOn)){
			HX_STACK_LINE(161)
			::Tutorial _g1 = ::Tutorial_obj::__new((int)0);		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(161)
			this->tutorial = _g1;
		}
		else{
			HX_STACK_LINE(164)
			this->tutorial = null();
		}
		HX_STACK_LINE(166)
		::TileBoard _g2 = ::TileBoard_obj::__new(null(),null());		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(166)
		this->tileBoard = _g2;
		HX_STACK_LINE(167)
		::TileBoarder _g3 = ::TileBoarder_obj::__new(null(),null());		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(167)
		this->currentTileBorder = _g3;
		HX_STACK_LINE(168)
		::Market _g4 = ::Market_obj::__new();		HX_STACK_VAR(_g4,"_g4");
		HX_STACK_LINE(168)
		this->market = _g4;
		HX_STACK_LINE(169)
		::AppleShed _g5 = ::AppleShed_obj::__new();		HX_STACK_VAR(_g5,"_g5");
		HX_STACK_LINE(169)
		this->appleShed = _g5;
		HX_STACK_LINE(170)
		::TruckButton _g6 = ::TruckButton_obj::__new();		HX_STACK_VAR(_g6,"_g6");
		HX_STACK_LINE(170)
		this->truckButton = _g6;
		HX_STACK_LINE(171)
		::AppleTruck _g7 = ::AppleTruck_obj::__new();		HX_STACK_VAR(_g7,"_g7");
		HX_STACK_LINE(171)
		this->appleTruck = _g7;
		HX_STACK_LINE(172)
		::AppleBasket _g8 = ::AppleBasket_obj::__new();		HX_STACK_VAR(_g8,"_g8");
		HX_STACK_LINE(172)
		this->basket = _g8;
		HX_STACK_LINE(173)
		::Seed _g9 = ::Seed_obj::__new((int)0,(int)0,false);		HX_STACK_VAR(_g9,"_g9");
		HX_STACK_LINE(173)
		this->seed = _g9;
		HX_STACK_LINE(174)
		::FishIcon _g10 = ::FishIcon_obj::__new((int)0,(int)0,false);		HX_STACK_VAR(_g10,"_g10");
		HX_STACK_LINE(174)
		this->fishIcon = _g10;
		HX_STACK_LINE(175)
		::GreenHouseIcon _g11 = ::GreenHouseIcon_obj::__new((int)0,(int)0,false);		HX_STACK_VAR(_g11,"_g11");
		HX_STACK_LINE(175)
		this->greenHouseBarIcon = _g11;
		HX_STACK_LINE(176)
		::SystemStar _g12 = ::SystemStar_obj::__new();		HX_STACK_VAR(_g12,"_g12");
		HX_STACK_LINE(176)
		this->systemStar = _g12;
		HX_STACK_LINE(177)
		::flixel::FlxSprite _g13 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g13,"_g13");
		HX_STACK_LINE(177)
		this->buildBar = _g13;
		HX_STACK_LINE(178)
		::flixel::FlxSprite _g14 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g14,"_g14");
		HX_STACK_LINE(178)
		this->buildBarTab = _g14;
		HX_STACK_LINE(179)
		::flixel::FlxSprite _g15 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g15,"_g15");
		HX_STACK_LINE(179)
		this->buildBarShaft = _g15;
		HX_STACK_LINE(180)
		::JuiceIcon _g16 = ::JuiceIcon_obj::__new((int)118,(int)70,false);		HX_STACK_VAR(_g16,"_g16");
		HX_STACK_LINE(180)
		this->juiceMachineIcon = _g16;
		HX_STACK_LINE(181)
		::JuicePackIcon _g17 = ::JuicePackIcon_obj::__new();		HX_STACK_VAR(_g17,"_g17");
		HX_STACK_LINE(181)
		this->juicePackIcon = _g17;
		HX_STACK_LINE(182)
		::PiePackIcon _g18 = ::PiePackIcon_obj::__new();		HX_STACK_VAR(_g18,"_g18");
		HX_STACK_LINE(182)
		this->piePackIcon = _g18;
		HX_STACK_LINE(183)
		::PieIcon _g19 = ::PieIcon_obj::__new((int)0,(int)0,false);		HX_STACK_VAR(_g19,"_g19");
		HX_STACK_LINE(183)
		this->pieMachineIcon = _g19;
		HX_STACK_LINE(184)
		::flixel::plugin::MouseEventManager_obj::remove(this->seed);
		HX_STACK_LINE(185)
		::flixel::plugin::MouseEventManager_obj::remove(this->juiceMachineIcon);
		HX_STACK_LINE(186)
		::flixel::plugin::MouseEventManager_obj::remove(this->pieMachineIcon);
		HX_STACK_LINE(187)
		::flixel::plugin::MouseEventManager_obj::remove(this->fishIcon);
		HX_STACK_LINE(188)
		::flixel::plugin::MouseEventManager_obj::remove(this->greenHouseBarIcon);
		HX_STACK_LINE(189)
		::flixel::FlxSprite _g20 = this->truckButton->getTruckButtonSprite();		HX_STACK_VAR(_g20,"_g20");
		HX_STACK_LINE(189)
		::flixel::plugin::MouseEventManager_obj::add(_g20,this->callTruck_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(190)
		::flixel::plugin::MouseEventManager_obj::add(this->buildBarTab,this->buildBarOpen_dyn(),null(),null(),null(),null(),null(),null());
		HX_STACK_LINE(191)
		::Seed _g21 = ::Seed_obj::__new((int)0,(int)0,true);		HX_STACK_VAR(_g21,"_g21");
		HX_STACK_LINE(191)
		this->seedDrag = _g21;
		HX_STACK_LINE(192)
		::JuiceIcon _g22 = ::JuiceIcon_obj::__new((int)0,(int)0,true);		HX_STACK_VAR(_g22,"_g22");
		HX_STACK_LINE(192)
		this->juiceMachineIconDrag = _g22;
		HX_STACK_LINE(193)
		::PieIcon _g23 = ::PieIcon_obj::__new((int)0,(int)0,true);		HX_STACK_VAR(_g23,"_g23");
		HX_STACK_LINE(193)
		this->pieMachineIconDrag = _g23;
		HX_STACK_LINE(194)
		::FishIcon _g24 = ::FishIcon_obj::__new((int)0,(int)0,true);		HX_STACK_VAR(_g24,"_g24");
		HX_STACK_LINE(194)
		this->fishIconDrag = _g24;
		HX_STACK_LINE(195)
		::GreenHouseIcon _g25 = ::GreenHouseIcon_obj::__new((int)0,(int)0,true);		HX_STACK_VAR(_g25,"_g25");
		HX_STACK_LINE(195)
		this->greenHouseBarIconDrag = _g25;
		HX_STACK_LINE(196)
		::ProblemBoard _g26 = ::ProblemBoard_obj::__new();		HX_STACK_VAR(_g26,"_g26");
		HX_STACK_LINE(196)
		this->bb = _g26;
		HX_STACK_LINE(197)
		::flixel::FlxSprite _g27 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g27,"_g27");
		HX_STACK_LINE(197)
		this->buySign = _g27;
		HX_STACK_LINE(198)
		::flixel::group::FlxGroup _g28 = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g28,"_g28");
		HX_STACK_LINE(198)
		this->aboveGrassLayer = _g28;
		HX_STACK_LINE(199)
		::flixel::group::FlxGroup _g29 = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g29,"_g29");
		HX_STACK_LINE(199)
		this->belowBottomSprites = _g29;
		HX_STACK_LINE(200)
		::flixel::group::FlxGroup _g30 = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g30,"_g30");
		HX_STACK_LINE(200)
		this->bottomeSprites = _g30;
		HX_STACK_LINE(201)
		::flixel::group::FlxGroup _g31 = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g31,"_g31");
		HX_STACK_LINE(201)
		this->plantLayer = _g31;
		HX_STACK_LINE(202)
		::flixel::group::FlxGroup _g32 = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g32,"_g32");
		HX_STACK_LINE(202)
		this->machineLayer = _g32;
		HX_STACK_LINE(203)
		::flixel::FlxSprite _g33 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g33,"_g33");
		HX_STACK_LINE(203)
		this->backgroundSprite = _g33;
		HX_STACK_LINE(204)
		::AsteroidBelt _g34 = ::AsteroidBelt_obj::__new((int)-60);		HX_STACK_VAR(_g34,"_g34");
		HX_STACK_LINE(204)
		this->asteroidBelt = _g34;
		HX_STACK_LINE(205)
		::flixel::FlxSprite _g35 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g35,"_g35");
		HX_STACK_LINE(205)
		this->foreGroundRocks = _g35;
		HX_STACK_LINE(206)
		::flixel::FlxSprite _g36 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g36,"_g36");
		HX_STACK_LINE(206)
		this->bridge = _g36;
		HX_STACK_LINE(207)
		this->buildBar->loadGraphic(HX_CSTRING("assets/images/buildBar.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(208)
		this->buildBarTab->loadGraphic(HX_CSTRING("assets/images/buildBarTab.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(209)
		this->buildBarShaft->loadGraphic(HX_CSTRING("assets/images/buildBarShaft.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(210)
		this->backgroundSprite->loadGraphic(HX_CSTRING("assets/images/spaceBackground.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(211)
		this->buySign->loadGraphic(HX_CSTRING("assets/images/buyLandButton.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(212)
		this->foreGroundRocks->loadGraphic(HX_CSTRING("assets/images/rockyForeground.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(213)
		this->bridge->loadGraphic(HX_CSTRING("assets/images/bridgeImage.png"),false,null(),null(),null(),null());
		HX_STACK_LINE(214)
		::flixel::text::FlxText _g37 = ::flixel::text::FlxText_obj::__new((int)123,(int)43,(int)32,this->appleCountString,(int)12,false);		HX_STACK_VAR(_g37,"_g37");
		HX_STACK_LINE(214)
		this->appleCountFieldText = _g37;
		HX_STACK_LINE(215)
		this->belowBottomSprites->add(this->backgroundSprite);
		HX_STACK_LINE(216)
		this->belowBottomSprites->add(this->asteroidBelt);
		HX_STACK_LINE(217)
		this->belowBottomSprites->add(this->systemStar);
		HX_STACK_LINE(218)
		this->aboveGrassLayer->add(this->truckButton);
		HX_STACK_LINE(219)
		this->appleCountFieldText->set_color((int)268435455);
		HX_STACK_LINE(220)
		this->seedDrag->set_alpha(.5);
		HX_STACK_LINE(221)
		this->juiceMachineIconDrag->set_alpha(.5);
		HX_STACK_LINE(222)
		this->pieMachineIconDrag->set_alpha(.5);
		HX_STACK_LINE(223)
		this->fishIconDrag->set_alpha(.5);
		HX_STACK_LINE(224)
		this->greenHouseBarIconDrag->set_alpha(.5);
		HX_STACK_LINE(225)
		this->foreGroundRocks->set_x((int)30);
		HX_STACK_LINE(226)
		this->foreGroundRocks->set_y((int)265);
		HX_STACK_LINE(227)
		this->bridge->set_y((int)260);
		HX_STACK_LINE(228)
		this->bridge->set_x((int)-55);
		HX_STACK_LINE(229)
		this->buildBarShaft->set_y((int)65);
		HX_STACK_LINE(230)
		this->buildBarShaft->set_x((int)-185);
		HX_STACK_LINE(231)
		this->buildBarTab->set_y((int)65);
		HX_STACK_LINE(232)
		this->buildBarTab->set_x((int)-4);
		HX_STACK_LINE(234)
		this->add(this->belowBottomSprites);
		HX_STACK_LINE(235)
		this->add(this->bottomeSprites);
		HX_STACK_LINE(236)
		this->add(this->floatingIsland);
		HX_STACK_LINE(237)
		this->add(this->buildBarShaft);
		HX_STACK_LINE(238)
		this->add(this->buildBarTab);
		HX_STACK_LINE(239)
		this->add(this->basket);
		HX_STACK_LINE(240)
		this->add(this->juicePackIcon);
		HX_STACK_LINE(241)
		this->add(this->piePackIcon);
		HX_STACK_LINE(242)
		this->drawBoard();
		HX_STACK_LINE(243)
		this->add(this->market);
		HX_STACK_LINE(244)
		this->add(this->appleShed);
		HX_STACK_LINE(246)
		this->add(this->machineLayer);
		HX_STACK_LINE(247)
		this->add(this->plantLayer);
		HX_STACK_LINE(248)
		this->add(this->bridge);
		HX_STACK_LINE(249)
		this->add(this->appleTruck);
		HX_STACK_LINE(250)
		this->add(this->seed);
		HX_STACK_LINE(251)
		this->add(this->seedDrag);
		HX_STACK_LINE(252)
		this->add(this->juiceMachineIcon);
		HX_STACK_LINE(253)
		this->add(this->juiceMachineIconDrag);
		HX_STACK_LINE(254)
		this->add(this->pieMachineIcon);
		HX_STACK_LINE(255)
		this->add(this->pieMachineIconDrag);
		HX_STACK_LINE(256)
		this->add(this->fishIcon);
		HX_STACK_LINE(257)
		this->add(this->fishIconDrag);
		HX_STACK_LINE(258)
		this->add(this->greenHouseBarIcon);
		HX_STACK_LINE(259)
		this->add(this->greenHouseBarIconDrag);
		HX_STACK_LINE(260)
		if ((this->tutorialOn)){
			HX_STACK_LINE(261)
			this->add(this->tutorial);
			HX_STACK_LINE(262)
			this->buildBarIsOpen = true;
			HX_STACK_LINE(263)
			this->buildBarOpenTimer = (int)-1;
		}
		HX_STACK_LINE(265)
		this->add(this->bb);
		HX_STACK_LINE(266)
		::NumberPad _g38 = ::NumberPad_obj::__new();		HX_STACK_VAR(_g38,"_g38");
		HX_STACK_LINE(266)
		this->numPad = _g38;
		HX_STACK_LINE(267)
		::String _g39 = this->numPad->getPlayerAnswerString();		HX_STACK_VAR(_g39,"_g39");
		HX_STACK_LINE(267)
		::flixel::text::FlxText _g40 = ::flixel::text::FlxText_obj::__new((int)235,(int)132,(int)105,_g39,(int)24,false);		HX_STACK_VAR(_g40,"_g40");
		HX_STACK_LINE(267)
		this->answerFieldText = _g40;
		HX_STACK_LINE(268)
		this->answerFieldText->set_color((int)0);
		HX_STACK_LINE(269)
		this->add(this->aboveGrassLayer);
		HX_STACK_LINE(270)
		this->add(this->numPad);
		HX_STACK_LINE(271)
		this->add(this->menuButton);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,initPlaystate,(void))

Void PlayState_obj::openMenu( ){
{
		HX_STACK_FRAME("PlayState","openMenu",0x9858a9ba,"PlayState.openMenu","PlayState.hx",275,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(275)
		if (((this->menuVisible == (int)0))){
			HX_STACK_LINE(276)
			::OptionsScreen _g = ::OptionsScreen_obj::__new();		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(276)
			this->menuScreen = _g;
			HX_STACK_LINE(277)
			this->menuScreen->menuVisible((int)1);
			HX_STACK_LINE(278)
			this->add(this->menuScreen);
			HX_STACK_LINE(279)
			this->releaseAllClickEvents((int)1);
			HX_STACK_LINE(280)
			this->menuVisible = (int)1;
		}
		else{
			HX_STACK_LINE(283)
			this->menuScreen->destroy();
			HX_STACK_LINE(284)
			this->releaseAllClickEvents((int)0);
			HX_STACK_LINE(285)
			this->menuVisible = (int)0;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,openMenu,(void))

Void PlayState_obj::releaseAllClickEvents( int val){
{
		HX_STACK_FRAME("PlayState","releaseAllClickEvents",0xc65e1c36,"PlayState.releaseAllClickEvents","PlayState.hx",291,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(291)
		if (((val == (int)1))){
			HX_STACK_LINE(292)
			this->numPad->unregisterNumPadEvents();
			HX_STACK_LINE(293)
			this->seedDrag->unregisterSeedEvent();
			HX_STACK_LINE(294)
			this->pieMachineIconDrag->unregisterPieEvent();
			HX_STACK_LINE(295)
			this->juiceMachineIconDrag->unregisterJuiceEvent();
			HX_STACK_LINE(296)
			this->fishIconDrag->unregisterFishEvent();
			HX_STACK_LINE(297)
			this->greenHouseBarIconDrag->unregisterGreenHouseImageEvent();
			HX_STACK_LINE(298)
			::flixel::FlxSprite _g = this->truckButton->getTruckButtonSprite();		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(298)
			::flixel::plugin::MouseEventManager_obj::remove(_g);
			HX_STACK_LINE(299)
			this->market->unregisterEvents();
			HX_STACK_LINE(300)
			{
				HX_STACK_LINE(300)
				int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(300)
				int _g2 = this->treeArray->length;		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(300)
				while((true)){
					HX_STACK_LINE(300)
					if ((!(((_g1 < _g2))))){
						HX_STACK_LINE(300)
						break;
					}
					HX_STACK_LINE(300)
					int k = (_g1)++;		HX_STACK_VAR(k,"k");
					HX_STACK_LINE(301)
					if (((this->treeArray->__get(k).StaticCast< ::Tree >() != null()))){
						HX_STACK_LINE(302)
						this->treeArray->__get(k).StaticCast< ::Tree >()->unregisterAlertEvents();
						HX_STACK_LINE(303)
						this->treeArray->__get(k).StaticCast< ::Tree >()->unregisterTreeClickedMouseEvent();
						HX_STACK_LINE(304)
						this->treeArray->__get(k).StaticCast< ::Tree >()->unregisterAppleEvent();
					}
				}
			}
			HX_STACK_LINE(307)
			{
				HX_STACK_LINE(307)
				int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(307)
				int _g2 = this->board->length;		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(307)
				while((true)){
					HX_STACK_LINE(307)
					if ((!(((_g1 < _g2))))){
						HX_STACK_LINE(307)
						break;
					}
					HX_STACK_LINE(307)
					int i = (_g1)++;		HX_STACK_VAR(i,"i");
					HX_STACK_LINE(308)
					{
						HX_STACK_LINE(308)
						int _g3 = (int)0;		HX_STACK_VAR(_g3,"_g3");
						HX_STACK_LINE(308)
						int _g21 = this->board->length;		HX_STACK_VAR(_g21,"_g21");
						HX_STACK_LINE(308)
						while((true)){
							HX_STACK_LINE(308)
							if ((!(((_g3 < _g21))))){
								HX_STACK_LINE(308)
								break;
							}
							HX_STACK_LINE(308)
							int j = (_g3)++;		HX_STACK_VAR(j,"j");
							HX_STACK_LINE(309)
							::flixel::plugin::MouseEventManager_obj::remove(this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >());
						}
					}
				}
			}
		}
		else{
			HX_STACK_LINE(314)
			this->seedDrag->registerSeedEvent();
			HX_STACK_LINE(315)
			this->pieMachineIconDrag->registerPieEvent();
			HX_STACK_LINE(316)
			this->juiceMachineIconDrag->registerJuiceEvent();
			HX_STACK_LINE(317)
			this->fishIconDrag->registerFishEvent();
			HX_STACK_LINE(318)
			this->greenHouseBarIconDrag->registerGreenHouseImageEvent();
			HX_STACK_LINE(319)
			::flixel::FlxSprite _g1 = this->truckButton->getTruckButtonSprite();		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(319)
			::flixel::plugin::MouseEventManager_obj::add(_g1,this->callTruck_dyn(),null(),null(),null(),null(),null(),null());
			HX_STACK_LINE(320)
			this->market->registerEvents();
			HX_STACK_LINE(321)
			{
				HX_STACK_LINE(321)
				int _g11 = (int)0;		HX_STACK_VAR(_g11,"_g11");
				HX_STACK_LINE(321)
				int _g = this->board->length;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(321)
				while((true)){
					HX_STACK_LINE(321)
					if ((!(((_g11 < _g))))){
						HX_STACK_LINE(321)
						break;
					}
					HX_STACK_LINE(321)
					int i = (_g11)++;		HX_STACK_VAR(i,"i");
					HX_STACK_LINE(322)
					{
						HX_STACK_LINE(322)
						int _g3 = (int)0;		HX_STACK_VAR(_g3,"_g3");
						HX_STACK_LINE(322)
						int _g2 = this->board->length;		HX_STACK_VAR(_g2,"_g2");
						HX_STACK_LINE(322)
						while((true)){
							HX_STACK_LINE(322)
							if ((!(((_g3 < _g2))))){
								HX_STACK_LINE(322)
								break;
							}
							HX_STACK_LINE(322)
							int j = (_g3)++;		HX_STACK_VAR(j,"j");
							HX_STACK_LINE(323)
							if ((!(this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >()->getHasObject()))){
								HX_STACK_LINE(324)
								::flixel::plugin::MouseEventManager_obj::add(this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >(),this->buyButtonPopUp_dyn(),this->placeObject_dyn(),null(),null(),null(),null(),null());
							}
						}
					}
				}
			}
			HX_STACK_LINE(328)
			{
				HX_STACK_LINE(328)
				int _g11 = (int)0;		HX_STACK_VAR(_g11,"_g11");
				HX_STACK_LINE(328)
				int _g = this->treeArray->length;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(328)
				while((true)){
					HX_STACK_LINE(328)
					if ((!(((_g11 < _g))))){
						HX_STACK_LINE(328)
						break;
					}
					HX_STACK_LINE(328)
					int k = (_g11)++;		HX_STACK_VAR(k,"k");
					HX_STACK_LINE(329)
					if (((this->treeArray->__get(k).StaticCast< ::Tree >() != null()))){
						HX_STACK_LINE(330)
						::flixel::FlxSprite _g2 = this->treeArray->__get(k).StaticCast< ::Tree >()->getTreeSprite();		HX_STACK_VAR(_g2,"_g2");
						HX_STACK_LINE(330)
						::flixel::plugin::MouseEventManager_obj::add(_g2,this->treeClicked_dyn(),null(),null(),null(),null(),null(),null());
						HX_STACK_LINE(331)
						this->treeArray->__get(k).StaticCast< ::Tree >()->registerAlertEvents();
						HX_STACK_LINE(332)
						this->treeArray->__get(k).StaticCast< ::Tree >()->registerTreeClickedMouseEvent();
						HX_STACK_LINE(333)
						this->treeArray->__get(k).StaticCast< ::Tree >()->registerAppleEvent();
					}
				}
			}
			HX_STACK_LINE(336)
			this->numPad->registerEvent();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,releaseAllClickEvents,(void))

Void PlayState_obj::initTreeArray( ){
{
		HX_STACK_FRAME("PlayState","initTreeArray",0xbf2a195a,"PlayState.initTreeArray","PlayState.hx",342,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(342)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(342)
		while((true)){
			HX_STACK_LINE(342)
			if ((!(((_g < (int)9))))){
				HX_STACK_LINE(342)
				break;
			}
			HX_STACK_LINE(342)
			int i = (_g)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(343)
			this->treeArray[i] = null();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,initTreeArray,(void))

Void PlayState_obj::plantWeeds( ){
{
		HX_STACK_FRAME("PlayState","plantWeeds",0xf12d52ec,"PlayState.plantWeeds","PlayState.hx",348,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(349)
		::Weed _g = ::Weed_obj::__new((int)1);		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(349)
		this->weed = _g;
		HX_STACK_LINE(350)
		this->weed->getChosenWeed()->set_x((int)150);
		HX_STACK_LINE(351)
		this->weed->getChosenWeed()->set_y((int)260);
		HX_STACK_LINE(352)
		this->plantLayer->add(this->weed);
		HX_STACK_LINE(353)
		::Weed _g1 = ::Weed_obj::__new((int)1);		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(353)
		this->weed = _g1;
		HX_STACK_LINE(354)
		this->weed->getChosenWeed()->set_x((int)450);
		HX_STACK_LINE(355)
		this->weed->getChosenWeed()->set_y((int)260);
		HX_STACK_LINE(356)
		this->plantLayer->add(this->weed);
		HX_STACK_LINE(357)
		::Weed _g2 = ::Weed_obj::__new((int)1);		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(357)
		this->weed = _g2;
		HX_STACK_LINE(358)
		this->weed->getChosenWeed()->set_x((int)330);
		HX_STACK_LINE(359)
		this->weed->getChosenWeed()->set_y((int)375);
		HX_STACK_LINE(360)
		this->plantLayer->add(this->weed);
		HX_STACK_LINE(361)
		::Weed _g3 = ::Weed_obj::__new((int)1);		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(361)
		this->weed = _g3;
		HX_STACK_LINE(362)
		this->weed->getChosenWeed()->set_x((int)305);
		HX_STACK_LINE(363)
		this->weed->getChosenWeed()->set_y((int)190);
		HX_STACK_LINE(364)
		this->plantLayer->add(this->weed);
		HX_STACK_LINE(365)
		::Weed _g4 = ::Weed_obj::__new((int)1);		HX_STACK_VAR(_g4,"_g4");
		HX_STACK_LINE(365)
		this->weed = _g4;
		HX_STACK_LINE(366)
		this->weed->getChosenWeed()->set_x((int)70);
		HX_STACK_LINE(367)
		this->weed->getChosenWeed()->set_y((int)260);
		HX_STACK_LINE(368)
		this->plantLayer->add(this->weed);
		HX_STACK_LINE(369)
		::Weed _g5 = ::Weed_obj::__new((int)1);		HX_STACK_VAR(_g5,"_g5");
		HX_STACK_LINE(369)
		this->weed = _g5;
		HX_STACK_LINE(370)
		this->weed->getChosenWeed()->set_x((int)150);
		HX_STACK_LINE(371)
		this->weed->getChosenWeed()->set_y((int)190);
		HX_STACK_LINE(372)
		this->plantLayer->add(this->weed);
		HX_STACK_LINE(373)
		::Weed _g6 = ::Weed_obj::__new((int)0);		HX_STACK_VAR(_g6,"_g6");
		HX_STACK_LINE(373)
		this->weed = _g6;
		HX_STACK_LINE(374)
		this->weed->getChosenWeed()->set_x((int)365);
		HX_STACK_LINE(375)
		this->weed->getChosenWeed()->set_y((int)225);
		HX_STACK_LINE(376)
		this->plantLayer->add(this->weed);
		HX_STACK_LINE(377)
		::Weed _g7 = ::Weed_obj::__new((int)0);		HX_STACK_VAR(_g7,"_g7");
		HX_STACK_LINE(377)
		this->weed = _g7;
		HX_STACK_LINE(378)
		this->weed->getChosenWeed()->set_x((int)170);
		HX_STACK_LINE(379)
		this->weed->getChosenWeed()->set_y((int)240);
		HX_STACK_LINE(380)
		this->plantLayer->add(this->weed);
		HX_STACK_LINE(381)
		::Weed _g8 = ::Weed_obj::__new((int)0);		HX_STACK_VAR(_g8,"_g8");
		HX_STACK_LINE(381)
		this->weed = _g8;
		HX_STACK_LINE(382)
		this->weed->getChosenWeed()->set_x((int)190);
		HX_STACK_LINE(383)
		this->weed->getChosenWeed()->set_y((int)320);
		HX_STACK_LINE(384)
		this->plantLayer->add(this->weed);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,plantWeeds,(void))

Void PlayState_obj::createFloatingIsland( ){
{
		HX_STACK_FRAME("PlayState","createFloatingIsland",0x88336508,"PlayState.createFloatingIsland","PlayState.hx",388,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(389)
		if (((this->islandCreationTimer <= (int)0))){
			HX_STACK_LINE(390)
			::Island _g = ::Island_obj::__new();		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(390)
			this->floatingIsland = _g;
			HX_STACK_LINE(391)
			this->bottomeSprites->add(this->floatingIsland);
			HX_STACK_LINE(392)
			int _g1 = ::flixel::util::FlxRandom_obj::intRanged((int)2,(int)10,null());		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(392)
			this->islandCreationTimer = _g1;
		}
		HX_STACK_LINE(394)
		hx::SubEq(this->islandCreationTimer,::flixel::FlxG_obj::elapsed);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,createFloatingIsland,(void))

Void PlayState_obj::initFlyingJuiceBottleArray( ){
{
		HX_STACK_FRAME("PlayState","initFlyingJuiceBottleArray",0xb8803703,"PlayState.initFlyingJuiceBottleArray","PlayState.hx",399,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(399)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(399)
		while((true)){
			HX_STACK_LINE(399)
			if ((!(((_g < (int)9))))){
				HX_STACK_LINE(399)
				break;
			}
			HX_STACK_LINE(399)
			int i = (_g)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(400)
			this->flyingJuiceBottleArray[i] = null();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,initFlyingJuiceBottleArray,(void))

Void PlayState_obj::initJuiceMachineArray( ){
{
		HX_STACK_FRAME("PlayState","initJuiceMachineArray",0x68c8ed11,"PlayState.initJuiceMachineArray","PlayState.hx",406,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(406)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(406)
		while((true)){
			HX_STACK_LINE(406)
			if ((!(((_g < (int)9))))){
				HX_STACK_LINE(406)
				break;
			}
			HX_STACK_LINE(406)
			int i = (_g)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(407)
			this->juiceMachineArray[i] = null();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,initJuiceMachineArray,(void))

Void PlayState_obj::initPieMachineArray( ){
{
		HX_STACK_FRAME("PlayState","initPieMachineArray",0x10c7f23d,"PlayState.initPieMachineArray","PlayState.hx",413,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(413)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(413)
		while((true)){
			HX_STACK_LINE(413)
			if ((!(((_g < (int)9))))){
				HX_STACK_LINE(413)
				break;
			}
			HX_STACK_LINE(413)
			int i = (_g)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(414)
			this->pieMachineArray[i] = null();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,initPieMachineArray,(void))

Void PlayState_obj::addFlyingBottleArray( ::JuiceIcon flyingBottle){
{
		HX_STACK_FRAME("PlayState","addFlyingBottleArray",0x5f067ab4,"PlayState.addFlyingBottleArray","PlayState.hx",420,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(flyingBottle,"flyingBottle")
		HX_STACK_LINE(420)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(420)
		int _g = this->flyingJuiceBottleArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(420)
		while((true)){
			HX_STACK_LINE(420)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(420)
				break;
			}
			HX_STACK_LINE(420)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(421)
			if (((this->flyingJuiceBottleArray->__get(i).StaticCast< ::JuiceIcon >() == null()))){
				HX_STACK_LINE(422)
				this->flyingJuiceBottleArray[i] = flyingBottle;
				HX_STACK_LINE(423)
				return null();
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,addFlyingBottleArray,(void))

Void PlayState_obj::addTreeArray( ::Tree treeObj){
{
		HX_STACK_FRAME("PlayState","addTreeArray",0xab2424ab,"PlayState.addTreeArray","PlayState.hx",430,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(treeObj,"treeObj")
		HX_STACK_LINE(430)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(430)
		int _g = this->treeArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(430)
		while((true)){
			HX_STACK_LINE(430)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(430)
				break;
			}
			HX_STACK_LINE(430)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(431)
			if (((this->treeArray->__get(i).StaticCast< ::Tree >() == null()))){
				HX_STACK_LINE(432)
				this->treeArray[i] = treeObj;
				HX_STACK_LINE(433)
				return null();
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,addTreeArray,(void))

Void PlayState_obj::addJuiceMachineArray( ::JuiceMachine machineObj){
{
		HX_STACK_FRAME("PlayState","addJuiceMachineArray",0xfca83162,"PlayState.addJuiceMachineArray","PlayState.hx",440,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(machineObj,"machineObj")
		HX_STACK_LINE(440)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(440)
		int _g = this->juiceMachineArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(440)
		while((true)){
			HX_STACK_LINE(440)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(440)
				break;
			}
			HX_STACK_LINE(440)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(441)
			if (((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >() == null()))){
				HX_STACK_LINE(442)
				this->juiceMachineArray[i] = machineObj;
				HX_STACK_LINE(443)
				return null();
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,addJuiceMachineArray,(void))

Void PlayState_obj::addPieMachineArray( ::PieMachine machineObj){
{
		HX_STACK_FRAME("PlayState","addPieMachineArray",0xc8a6d04e,"PlayState.addPieMachineArray","PlayState.hx",450,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(machineObj,"machineObj")
		HX_STACK_LINE(450)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(450)
		int _g = this->pieMachineArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(450)
		while((true)){
			HX_STACK_LINE(450)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(450)
				break;
			}
			HX_STACK_LINE(450)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(451)
			if (((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >() == null()))){
				HX_STACK_LINE(452)
				this->pieMachineArray[i] = machineObj;
				HX_STACK_LINE(453)
				return null();
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,addPieMachineArray,(void))

Void PlayState_obj::tileBoarderFlip( ){
{
		HX_STACK_FRAME("PlayState","tileBoarderFlip",0xb78e6941,"PlayState.tileBoarderFlip","PlayState.hx",461,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(461)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(461)
		while((true)){
			HX_STACK_LINE(461)
			if ((!(((_g < (int)3))))){
				HX_STACK_LINE(461)
				break;
			}
			HX_STACK_LINE(461)
			int i = (_g)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(462)
			{
				HX_STACK_LINE(462)
				int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(462)
				while((true)){
					HX_STACK_LINE(462)
					if ((!(((_g1 < (int)3))))){
						HX_STACK_LINE(462)
						break;
					}
					HX_STACK_LINE(462)
					int j = (_g1)++;		HX_STACK_VAR(j,"j");
					struct _Function_4_1{
						inline static bool Block( int &i,hx::ObjectPtr< ::PlayState_obj > __this,int &j){
							HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",465,0xb30d7781)
							{
								HX_STACK_LINE(465)
								return (  ((::flixel::util::FlxCollision_obj::pixelPerfectPointCheck(::flixel::FlxG_obj::mouse->screenX,::flixel::FlxG_obj::mouse->screenY,__this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >(),null()))) ? bool(__this->juiceMachineIconDrag->getDragging()) : bool(false) );
							}
							return null();
						}
					};
					struct _Function_4_2{
						inline static bool Block( int &i,hx::ObjectPtr< ::PlayState_obj > __this,int &j){
							HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",466,0xb30d7781)
							{
								HX_STACK_LINE(466)
								return (  ((::flixel::util::FlxCollision_obj::pixelPerfectPointCheck(::flixel::FlxG_obj::mouse->screenX,::flixel::FlxG_obj::mouse->screenY,__this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >(),null()))) ? bool(__this->pieMachineIconDrag->getDragging()) : bool(false) );
							}
							return null();
						}
					};
					struct _Function_4_3{
						inline static bool Block( int &i,hx::ObjectPtr< ::PlayState_obj > __this,int &j){
							HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",467,0xb30d7781)
							{
								HX_STACK_LINE(467)
								return (  ((::flixel::util::FlxCollision_obj::pixelPerfectPointCheck(::flixel::FlxG_obj::mouse->screenX,::flixel::FlxG_obj::mouse->screenY,__this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >(),null()))) ? bool(__this->fishIconDrag->getDragging()) : bool(false) );
							}
							return null();
						}
					};
					struct _Function_4_4{
						inline static bool Block( int &i,hx::ObjectPtr< ::PlayState_obj > __this,int &j){
							HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",468,0xb30d7781)
							{
								HX_STACK_LINE(468)
								return (  ((::flixel::util::FlxCollision_obj::pixelPerfectPointCheck(::flixel::FlxG_obj::mouse->screenX,::flixel::FlxG_obj::mouse->screenY,__this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >(),null()))) ? bool(__this->greenHouseBarIconDrag->getDragging()) : bool(false) );
							}
							return null();
						}
					};
					HX_STACK_LINE(464)
					if (((  ((!(((  ((!(((  ((!(((  ((!(((  ((::flixel::util::FlxCollision_obj::pixelPerfectPointCheck(::flixel::FlxG_obj::mouse->screenX,::flixel::FlxG_obj::mouse->screenY,this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >(),null()))) ? bool(this->seedDrag->getDragging()) : bool(false) ))))) ? bool(_Function_4_1::Block(i,this,j)) : bool(true) ))))) ? bool(_Function_4_2::Block(i,this,j)) : bool(true) ))))) ? bool(_Function_4_3::Block(i,this,j)) : bool(true) ))))) ? bool(_Function_4_4::Block(i,this,j)) : bool(true) ))){
						HX_STACK_LINE(470)
						this->boarder->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::TileBoarder >()->boarderSwitch(HX_CSTRING("on"));
					}
					else{
						HX_STACK_LINE(475)
						this->boarder->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::TileBoarder >()->boarderSwitch(HX_CSTRING("off"));
					}
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,tileBoarderFlip,(void))

Void PlayState_obj::buyButtonPopUp( ::Tile sprite){
{
		HX_STACK_FRAME("PlayState","buyButtonPopUp",0xdd7edf45,"PlayState.buyButtonPopUp","PlayState.hx",485,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(485)
		if (((  ((!(this->numPad->getPadScrollingIn()))) ? bool(!(sprite->getTileOwned())) : bool(false) ))){
			HX_STACK_LINE(486)
			this->buySign->set_x((sprite->x + (int)70));
			HX_STACK_LINE(487)
			this->buySign->set_y((sprite->y + (int)38));
			HX_STACK_LINE(488)
			this->aboveGrassLayer->remove(this->buySignCostFieldText,null());
			HX_STACK_LINE(489)
			::flixel::plugin::MouseEventManager_obj::add(this->buySign,this->buyLand_dyn(),null(),null(),null(),null(),null(),null());
			HX_STACK_LINE(490)
			if ((!(sprite->getTileClicked()))){
				HX_STACK_LINE(491)
				::flixel::system::FlxSound _g = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/buySignSound.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(491)
				this->buySignSound = _g;
				HX_STACK_LINE(492)
				this->buySignSound->play(null());
				HX_STACK_LINE(493)
				this->buySign->revive();
				HX_STACK_LINE(494)
				this->aboveGrassLayer->add(this->buySign);
				HX_STACK_LINE(495)
				::String _g1 = ::Std_obj::string(this->buySignCost);		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(495)
				::flixel::text::FlxText _g2 = ::flixel::text::FlxText_obj::__new((this->buySign->x + (int)33),(this->buySign->y + (int)1),(int)16,_g1,(int)12,null());		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(495)
				this->buySignCostFieldText = _g2;
				HX_STACK_LINE(496)
				this->buySignCostFieldText->set_color((int)15066459);
				HX_STACK_LINE(497)
				this->aboveGrassLayer->add(this->buySignCostFieldText);
				HX_STACK_LINE(498)
				sprite->setTileClicked(true);
				HX_STACK_LINE(499)
				if (((bool((this->lastTileClicked != sprite)) && bool((this->lastTileClicked != null()))))){
					HX_STACK_LINE(500)
					this->lastTileClicked->setTileClicked(false);
				}
			}
			else{
				HX_STACK_LINE(504)
				this->buySign->kill();
				HX_STACK_LINE(505)
				this->aboveGrassLayer->remove(this->buySignCostFieldText,null());
				HX_STACK_LINE(506)
				sprite->setTileClicked(false);
			}
			HX_STACK_LINE(508)
			this->lastTileClicked = sprite;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,buyButtonPopUp,(void))

Void PlayState_obj::displayAppleCount( ){
{
		HX_STACK_FRAME("PlayState","displayAppleCount",0x145168a6,"PlayState.displayAppleCount","PlayState.hx",513,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(515)
		{
			HX_STACK_LINE(515)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(515)
			int _g = this->treeArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(515)
			while((true)){
				HX_STACK_LINE(515)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(515)
					break;
				}
				HX_STACK_LINE(515)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(516)
				if (((this->treeArray->__get(i).StaticCast< ::Tree >() != null()))){
					HX_STACK_LINE(517)
					int _g2 = this->market->getCoinCount();		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(517)
					this->treeArray->__get(i).StaticCast< ::Tree >()->currentGoldCount(_g2);
					struct _Function_4_1{
						inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
							HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",518,0xb30d7781)
							{
								HX_STACK_LINE(518)
								int _g11 = __this->basket->getBasketFullVal();		HX_STACK_VAR(_g11,"_g11");
								HX_STACK_LINE(518)
								return (__this->appleCount < _g11);
							}
							return null();
						}
					};
					HX_STACK_LINE(518)
					if (((  ((this->treeArray->__get(i).StaticCast< ::Tree >()->getAppleStored())) ? bool(_Function_4_1::Block(this)) : bool(false) ))){
						HX_STACK_LINE(519)
						(this->appleCount)++;
						HX_STACK_LINE(521)
						int _g21 = this->basket->getBasketFullVal();		HX_STACK_VAR(_g21,"_g21");
						HX_STACK_LINE(521)
						this->appleShed->animateShed(this->appleCount,_g21);
						HX_STACK_LINE(522)
						this->treeArray->__get(i).StaticCast< ::Tree >()->setAppleStored(false);
					}
					HX_STACK_LINE(524)
					this->treeArray->__get(i).StaticCast< ::Tree >()->setAppleClicked(false);
				}
			}
		}
		struct _Function_1_1{
			inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",527,0xb30d7781)
				{
					HX_STACK_LINE(527)
					int _g3 = __this->market->getJuicePackCount();		HX_STACK_VAR(_g3,"_g3");
					HX_STACK_LINE(527)
					return (_g3 >= (int)10);
				}
				return null();
			}
		};
		struct _Function_1_2{
			inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",527,0xb30d7781)
				{
					HX_STACK_LINE(527)
					int _g4 = __this->market->getPiePackCount();		HX_STACK_VAR(_g4,"_g4");
					HX_STACK_LINE(527)
					return (_g4 >= (int)10);
				}
				return null();
			}
		};
		HX_STACK_LINE(527)
		if (((  (((  (((  ((!(((  ((!(((this->appleCount >= (int)10))))) ? bool(_Function_1_1::Block(this)) : bool(true) ))))) ? bool(_Function_1_2::Block(this)) : bool(true) ))) ? bool(!(this->truckButton->getButtonClickable())) : bool(false) ))) ? bool(this->appleTruck->getTruckIsReset()) : bool(false) ))){
			HX_STACK_LINE(528)
			this->truckButton->setButtonLevel((int)1);
			HX_STACK_LINE(529)
			::flixel::FlxSprite _g5 = this->truckButton->getTruckButtonSprite();		HX_STACK_VAR(_g5,"_g5");
			HX_STACK_LINE(529)
			::flixel::plugin::MouseEventManager_obj::add(_g5,this->callTruck_dyn(),null(),null(),null(),null(),null(),null());
			HX_STACK_LINE(530)
			this->truckButton->buttonSetting();
			HX_STACK_LINE(531)
			this->truckButton->setButtonClickable(true);
		}
		struct _Function_1_3{
			inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",533,0xb30d7781)
				{
					HX_STACK_LINE(533)
					int _g6 = __this->market->getJuicePackCount();		HX_STACK_VAR(_g6,"_g6");
					HX_STACK_LINE(533)
					return (_g6 < (int)10);
				}
				return null();
			}
		};
		struct _Function_1_4{
			inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",533,0xb30d7781)
				{
					HX_STACK_LINE(533)
					int _g7 = __this->market->getPiePackCount();		HX_STACK_VAR(_g7,"_g7");
					HX_STACK_LINE(533)
					return (_g7 < (int)10);
				}
				return null();
			}
		};
		HX_STACK_LINE(533)
		if (((  (((  (((this->appleCount < (int)10))) ? bool(_Function_1_3::Block(this)) : bool(false) ))) ? bool(_Function_1_4::Block(this)) : bool(false) ))){
			HX_STACK_LINE(534)
			this->truckButton->setButtonLevel((int)0);
			HX_STACK_LINE(535)
			this->truckButton->buttonSetting();
			HX_STACK_LINE(536)
			this->truckButton->setButtonClickable(false);
		}
		HX_STACK_LINE(538)
		::String _g8 = ::Std_obj::string(this->appleCount);		HX_STACK_VAR(_g8,"_g8");
		HX_STACK_LINE(538)
		this->appleCountFieldText->set_text(_g8);
		HX_STACK_LINE(539)
		this->add(this->appleCountFieldText);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,displayAppleCount,(void))

Void PlayState_obj::displayPadNumbers( ){
{
		HX_STACK_FRAME("PlayState","displayPadNumbers",0xfa8e4008,"PlayState.displayPadNumbers","PlayState.hx",543,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(545)
		::String _g = this->numPad->getPlayerAnswerString();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(545)
		this->answerFieldText->set_text(_g);
		HX_STACK_LINE(546)
		Float _g1 = this->numPad->getPadY();		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(546)
		Float _g2 = (_g1 + (int)12);		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(546)
		this->answerFieldText->set_y(_g2);
		HX_STACK_LINE(547)
		this->add(this->answerFieldText);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,displayPadNumbers,(void))

Void PlayState_obj::placeObject( ::Tile sprite){
{
		HX_STACK_FRAME("PlayState","placeObject",0x47ce8455,"PlayState.placeObject","PlayState.hx",551,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(552)
		if (((  (((  (((  ((::flixel::util::FlxCollision_obj::pixelPerfectCheck(this->seedDrag,sprite,null(),null()))) ? bool(::flixel::util::FlxCollision_obj::pixelPerfectPointCheck(::flixel::FlxG_obj::mouse->screenX,::flixel::FlxG_obj::mouse->screenY,sprite,null())) : bool(false) ))) ? bool(!(sprite->getHasObject())) : bool(false) ))) ? bool(sprite->getTileOwned()) : bool(false) ))){
			HX_STACK_LINE(553)
			::flixel::system::FlxSound _g = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/plantingSeedSound.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(553)
			this->plantingSeedSound = _g;
			HX_STACK_LINE(554)
			this->plantingSeedSound->play(true);
			HX_STACK_LINE(555)
			this->market->decSeedCount();
			HX_STACK_LINE(556)
			::flixel::FlxSprite _g1 = this->basket->getBasketSprite();		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(556)
			::Tree _g2 = ::Tree_obj::__new(_g1);		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(556)
			this->tree = _g2;
			HX_STACK_LINE(558)
			this->tree->setTreeGrowthPhase((int)0);
			HX_STACK_LINE(559)
			this->tree->setMyTile(sprite);
			HX_STACK_LINE(560)
			::flixel::FlxSprite _g3 = this->tree->getTreeSprite();		HX_STACK_VAR(_g3,"_g3");
			HX_STACK_LINE(560)
			::flixel::plugin::MouseEventManager_obj::add(_g3,this->treeClicked_dyn(),null(),null(),null(),null(),null(),null());
			HX_STACK_LINE(561)
			bool _g4 = sprite->getTileOwned();		HX_STACK_VAR(_g4,"_g4");
			HX_STACK_LINE(561)
			this->tree->setTreeGroundColor(_g4);
			HX_STACK_LINE(562)
			this->tree->setTreeX((sprite->x + (int)67));
			HX_STACK_LINE(563)
			this->tree->setTreeY((sprite->y - (int)25));
			HX_STACK_LINE(564)
			this->tree->positionAlerts();
			HX_STACK_LINE(565)
			sprite->setHasObject(true);
			HX_STACK_LINE(566)
			this->addTreeArray(this->tree);
			HX_STACK_LINE(567)
			this->secondTreeTutMessage();
			HX_STACK_LINE(568)
			this->tree->growTree();
			HX_STACK_LINE(569)
			this->aboveGrassLayer->add(this->tree);
			HX_STACK_LINE(570)
			if ((this->tutorialOn)){
				HX_STACK_LINE(571)
				this->treeArray->__get((int)0).StaticCast< ::Tree >()->setTreeThirstTut((int)9999);
				HX_STACK_LINE(572)
				this->treeArray->__get((int)0).StaticCast< ::Tree >()->setTreeSunlightTut((int)9999);
				HX_STACK_LINE(573)
				this->tutorial->destroy();
				HX_STACK_LINE(574)
				::Tutorial _g5 = ::Tutorial_obj::__new((int)1);		HX_STACK_VAR(_g5,"_g5");
				HX_STACK_LINE(574)
				this->tutorial = _g5;
				HX_STACK_LINE(575)
				this->add(this->tutorial);
			}
		}
		HX_STACK_LINE(578)
		if (((  (((  (((  ((::flixel::util::FlxCollision_obj::pixelPerfectCheck(this->juiceMachineIconDrag,sprite,null(),null()))) ? bool(::flixel::util::FlxCollision_obj::pixelPerfectPointCheck(::flixel::FlxG_obj::mouse->screenX,::flixel::FlxG_obj::mouse->screenY,sprite,null())) : bool(false) ))) ? bool(!(sprite->getHasObject())) : bool(false) ))) ? bool(sprite->getTileOwned()) : bool(false) ))){
			HX_STACK_LINE(579)
			::flixel::system::FlxSound _g6 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/placedMachineSound.wav"),.6,null(),null(),null(),null(),null());		HX_STACK_VAR(_g6,"_g6");
			HX_STACK_LINE(579)
			this->placedMachineSound = _g6;
			HX_STACK_LINE(580)
			this->placedMachineSound->play(null());
			HX_STACK_LINE(581)
			this->market->decjuiceCount((int)1);
			HX_STACK_LINE(582)
			::JuiceMachine _g7 = ::JuiceMachine_obj::__new(sprite);		HX_STACK_VAR(_g7,"_g7");
			HX_STACK_LINE(582)
			this->juiceMachine = _g7;
			HX_STACK_LINE(583)
			this->juiceMachine->setRiseRun();
			HX_STACK_LINE(584)
			sprite->setHasObject(true);
			HX_STACK_LINE(585)
			this->addJuiceMachineArray(this->juiceMachine);
			HX_STACK_LINE(586)
			this->machineLayer->add(this->juiceMachine);
		}
		HX_STACK_LINE(588)
		if (((  (((  (((  ((::flixel::util::FlxCollision_obj::pixelPerfectCheck(this->pieMachineIconDrag,sprite,null(),null()))) ? bool(::flixel::util::FlxCollision_obj::pixelPerfectPointCheck(::flixel::FlxG_obj::mouse->screenX,::flixel::FlxG_obj::mouse->screenY,sprite,null())) : bool(false) ))) ? bool(!(sprite->getHasObject())) : bool(false) ))) ? bool(sprite->getTileOwned()) : bool(false) ))){
			HX_STACK_LINE(589)
			::flixel::system::FlxSound _g8 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/placedMachineSound.wav"),.6,null(),null(),null(),null(),null());		HX_STACK_VAR(_g8,"_g8");
			HX_STACK_LINE(589)
			this->placedMachineSound = _g8;
			HX_STACK_LINE(590)
			this->placedMachineSound->play(null());
			HX_STACK_LINE(591)
			this->market->decPieCount();
			HX_STACK_LINE(592)
			::PieMachine _g9 = ::PieMachine_obj::__new(sprite);		HX_STACK_VAR(_g9,"_g9");
			HX_STACK_LINE(592)
			this->pieMachine = _g9;
			HX_STACK_LINE(593)
			this->pieMachine->setRiseRun();
			HX_STACK_LINE(594)
			sprite->setHasObject(true);
			HX_STACK_LINE(595)
			this->addPieMachineArray(this->pieMachine);
			HX_STACK_LINE(596)
			this->machineLayer->add(this->pieMachine);
		}
		HX_STACK_LINE(598)
		if (((  (((  (((  ((::flixel::util::FlxCollision_obj::pixelPerfectCheck(this->fishIconDrag,sprite,null(),null()))) ? bool(::flixel::util::FlxCollision_obj::pixelPerfectPointCheck(::flixel::FlxG_obj::mouse->screenX,::flixel::FlxG_obj::mouse->screenY,sprite,null())) : bool(false) ))) ? bool(!(sprite->getHasObject())) : bool(false) ))) ? bool(sprite->getTileOwned()) : bool(false) ))){
			HX_STACK_LINE(599)
			::flixel::system::FlxSound _g10 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/koiPondPlacedSound.wav"),(int)1,null(),null(),null(),null(),null());		HX_STACK_VAR(_g10,"_g10");
			HX_STACK_LINE(599)
			this->placedMachineSound = _g10;
			HX_STACK_LINE(600)
			this->placedMachineSound->play(null());
			HX_STACK_LINE(601)
			this->market->decFishCount();
			HX_STACK_LINE(602)
			::KoiPond _g11 = ::KoiPond_obj::__new(sprite);		HX_STACK_VAR(_g11,"_g11");
			HX_STACK_LINE(602)
			this->koiPond = _g11;
			HX_STACK_LINE(603)
			sprite->setHasObject(true);
			HX_STACK_LINE(604)
			this->machineLayer->add(this->koiPond);
		}
		HX_STACK_LINE(606)
		if (((  (((  (((  ((::flixel::util::FlxCollision_obj::pixelPerfectCheck(this->greenHouseBarIconDrag,sprite,null(),null()))) ? bool(::flixel::util::FlxCollision_obj::pixelPerfectPointCheck(::flixel::FlxG_obj::mouse->screenX,::flixel::FlxG_obj::mouse->screenY,sprite,null())) : bool(false) ))) ? bool(!(sprite->getHasObject())) : bool(false) ))) ? bool(sprite->getTileOwned()) : bool(false) ))){
			HX_STACK_LINE(607)
			::flixel::system::FlxSound _g12 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/koiPondPlacedSound.wav"),(int)1,null(),null(),null(),null(),null());		HX_STACK_VAR(_g12,"_g12");
			HX_STACK_LINE(607)
			this->placedMachineSound = _g12;
			HX_STACK_LINE(608)
			this->placedMachineSound->play(null());
			HX_STACK_LINE(609)
			this->market->decGreenHouseCount();
			HX_STACK_LINE(610)
			::Greenhouse _g13 = ::Greenhouse_obj::__new(sprite);		HX_STACK_VAR(_g13,"_g13");
			HX_STACK_LINE(610)
			this->greenHouse = _g13;
			HX_STACK_LINE(611)
			sprite->setHasObject(true);
			HX_STACK_LINE(612)
			this->aboveGrassLayer->add(this->greenHouse);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,placeObject,(void))

Void PlayState_obj::treeClicked( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("PlayState","treeClicked",0xa0e35a58,"PlayState.treeClicked","PlayState.hx",616,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,treeClicked,(void))

Void PlayState_obj::secondTreeTutMessage( ){
{
		HX_STACK_FRAME("PlayState","secondTreeTutMessage",0x59e2b477,"PlayState.secondTreeTutMessage","PlayState.hx",622,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(622)
		if (((this->treeArray->__get((int)1).StaticCast< ::Tree >() != null()))){
			HX_STACK_LINE(623)
			::Tutorial _g = ::Tutorial_obj::__new((int)5);		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(623)
			this->tutorial = _g;
			HX_STACK_LINE(624)
			if ((this->tutorialOn)){
				HX_STACK_LINE(625)
				this->add(this->tutorial);
			}
			HX_STACK_LINE(627)
			this->tutorialOn = false;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,secondTreeTutMessage,(void))

Void PlayState_obj::buyLand( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("PlayState","buyLand",0xdb3c3a40,"PlayState.buyLand","PlayState.hx",633,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(633)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(633)
		while((true)){
			HX_STACK_LINE(633)
			if ((!(((_g < (int)3))))){
				HX_STACK_LINE(633)
				break;
			}
			HX_STACK_LINE(633)
			int i = (_g)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(634)
			{
				HX_STACK_LINE(634)
				int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(634)
				while((true)){
					HX_STACK_LINE(634)
					if ((!(((_g1 < (int)3))))){
						HX_STACK_LINE(634)
						break;
					}
					HX_STACK_LINE(634)
					int j = (_g1)++;		HX_STACK_VAR(j,"j");
					struct _Function_4_1{
						inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
							HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",635,0xb30d7781)
							{
								HX_STACK_LINE(635)
								int _g2 = __this->market->getCoinCount();		HX_STACK_VAR(_g2,"_g2");
								HX_STACK_LINE(635)
								return (_g2 >= __this->buySignCost);
							}
							return null();
						}
					};
					HX_STACK_LINE(635)
					if (((  ((::flixel::util::FlxCollision_obj::pixelPerfectPointCheck(::flixel::FlxG_obj::mouse->screenX,::flixel::FlxG_obj::mouse->screenY,this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >(),null()))) ? bool(_Function_4_1::Block(this)) : bool(false) ))){
						HX_STACK_LINE(636)
						::flixel::system::FlxSound _g11 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/cashRegisterSound.wav"),.3,null(),null(),null(),null(),null());		HX_STACK_VAR(_g11,"_g11");
						HX_STACK_LINE(636)
						this->buyLandSound = _g11;
						HX_STACK_LINE(637)
						this->buyLandSound->play(null());
						HX_STACK_LINE(638)
						this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >()->setAsOwned();
						HX_STACK_LINE(639)
						this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >()->setHasObject(false);
						HX_STACK_LINE(640)
						this->buySign->kill();
						HX_STACK_LINE(641)
						this->market->addSubtractCoins(HX_CSTRING("-"),this->buySignCost);
						HX_STACK_LINE(642)
						this->aboveGrassLayer->remove(this->buySignCostFieldText,null());
						HX_STACK_LINE(643)
						(this->buySignCost)++;
					}
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,buyLand,(void))

Void PlayState_obj::callTruck( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("PlayState","callTruck",0xdec0d9d0,"PlayState.callTruck","PlayState.hx",650,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		struct _Function_1_1{
			inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",650,0xb30d7781)
				{
					HX_STACK_LINE(650)
					int _g = __this->market->getJuicePackCount();		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(650)
					return (_g >= (int)10);
				}
				return null();
			}
		};
		struct _Function_1_2{
			inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",650,0xb30d7781)
				{
					HX_STACK_LINE(650)
					int _g1 = __this->market->getPiePackCount();		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(650)
					return (_g1 >= (int)10);
				}
				return null();
			}
		};
		HX_STACK_LINE(650)
		if (((  (((  ((!(((  ((!(((this->appleCount >= (int)10))))) ? bool(_Function_1_1::Block(this)) : bool(true) ))))) ? bool(_Function_1_2::Block(this)) : bool(true) ))) ? bool(this->appleTruck->getTruckIsReset()) : bool(false) ))){
			HX_STACK_LINE(651)
			if (((this->tutorial != null()))){
				HX_STACK_LINE(652)
				Float _g2 = this->tutorial->getCurrentTutSectionNumber();		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(652)
				if (((  (((_g2 == (int)1))) ? bool(this->tutorialOn) : bool(false) ))){
					HX_STACK_LINE(653)
					this->tutorial->destroy();
				}
			}
			HX_STACK_LINE(656)
			if ((this->tutorialOn)){
				HX_STACK_LINE(657)
				this->tutorial->destroy();
			}
			HX_STACK_LINE(659)
			::flixel::system::FlxSound _g3 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/truckButtonSound.wav"),.5,null(),null(),null(),null(),null());		HX_STACK_VAR(_g3,"_g3");
			HX_STACK_LINE(659)
			this->truckButtonSound = _g3;
			HX_STACK_LINE(660)
			this->truckButtonSound->play(null());
			HX_STACK_LINE(661)
			this->appleTruck->setPickUpReady(true);
			HX_STACK_LINE(662)
			this->truckButton->setButtonLevel((int)2);
			HX_STACK_LINE(663)
			this->truckButton->buttonSetting();
			HX_STACK_LINE(664)
			::flixel::FlxSprite _g4 = this->truckButton->getTruckButtonSprite();		HX_STACK_VAR(_g4,"_g4");
			HX_STACK_LINE(664)
			::flixel::plugin::MouseEventManager_obj::remove(_g4);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,callTruck,(void))

Void PlayState_obj::truckLoadingGoods( ){
{
		HX_STACK_FRAME("PlayState","truckLoadingGoods",0xdb59a568,"PlayState.truckLoadingGoods","PlayState.hx",669,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(669)
		if ((this->appleTruck->getLoadingApples())){
			HX_STACK_LINE(670)
			if ((this->truckTutSection)){
				HX_STACK_LINE(672)
				if ((this->tutorialOn)){
					HX_STACK_LINE(673)
					this->tutorial->destroy();
					HX_STACK_LINE(674)
					::Tutorial _g = ::Tutorial_obj::__new((int)4);		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(674)
					this->tutorial = _g;
					HX_STACK_LINE(675)
					this->add(this->tutorial);
				}
				HX_STACK_LINE(677)
				this->truckTutSection = false;
			}
			HX_STACK_LINE(679)
			hx::SubEq(this->loadAppleTimer,::flixel::FlxG_obj::elapsed);
			HX_STACK_LINE(680)
			if (((this->loadAppleTimer < (int)0))){
				HX_STACK_LINE(681)
				int _g1 = this->appleTruck->getAppleTruckCapacity();		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(681)
				if (((  (((_g1 > (int)0))) ? bool((this->appleCount >= (int)10)) : bool(false) ))){
					HX_STACK_LINE(682)
					hx::SubEq(this->appleCount,(int)10);
					HX_STACK_LINE(683)
					this->appleTruck->decTruckCapacity();
					HX_STACK_LINE(685)
					int _g2 = this->basket->getBasketFullVal();		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(685)
					this->appleShed->animateShed(this->appleCount,_g2);
					HX_STACK_LINE(686)
					this->market->addSubtractCoins(HX_CSTRING("+"),this->applePerTenCost);
					HX_STACK_LINE(687)
					::flixel::system::FlxSound _g3 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/coinsSound.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g3,"_g3");
					HX_STACK_LINE(687)
					this->coinsSound = _g3;
					HX_STACK_LINE(688)
					this->coinsSound->play(null());
					HX_STACK_LINE(689)
					this->loadAppleTimer = (int)1;
				}
				HX_STACK_LINE(691)
				int _g4 = this->appleTruck->getAppleTruckCapacity();		HX_STACK_VAR(_g4,"_g4");
				struct _Function_3_1{
					inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",691,0xb30d7781)
						{
							HX_STACK_LINE(691)
							int _g5 = __this->market->getJuicePackCount();		HX_STACK_VAR(_g5,"_g5");
							HX_STACK_LINE(691)
							return (_g5 >= (int)10);
						}
						return null();
					}
				};
				HX_STACK_LINE(691)
				if (((  (((_g4 > (int)0))) ? bool(_Function_3_1::Block(this)) : bool(false) ))){
					HX_STACK_LINE(692)
					this->market->loadJuicePacksDec();
					HX_STACK_LINE(693)
					this->appleTruck->decTruckCapacity();
					HX_STACK_LINE(694)
					this->market->addSubtractCoins(HX_CSTRING("+"),this->juicePerTenCost);
					HX_STACK_LINE(695)
					::flixel::system::FlxSound _g6 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/coinsSound.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g6,"_g6");
					HX_STACK_LINE(695)
					this->coinsSound = _g6;
					HX_STACK_LINE(696)
					this->coinsSound->play(null());
					HX_STACK_LINE(697)
					this->loadAppleTimer = (int)1;
				}
				HX_STACK_LINE(699)
				int _g7 = this->appleTruck->getAppleTruckCapacity();		HX_STACK_VAR(_g7,"_g7");
				struct _Function_3_2{
					inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",699,0xb30d7781)
						{
							HX_STACK_LINE(699)
							int _g8 = __this->market->getPiePackCount();		HX_STACK_VAR(_g8,"_g8");
							HX_STACK_LINE(699)
							return (_g8 >= (int)10);
						}
						return null();
					}
				};
				HX_STACK_LINE(699)
				if (((  (((_g7 > (int)0))) ? bool(_Function_3_2::Block(this)) : bool(false) ))){
					HX_STACK_LINE(700)
					this->market->loadPiePacksDec();
					HX_STACK_LINE(701)
					this->appleTruck->decTruckCapacity();
					HX_STACK_LINE(702)
					this->market->addSubtractCoins(HX_CSTRING("+"),this->piePerTenCost);
					HX_STACK_LINE(703)
					::flixel::system::FlxSound _g9 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/coinsSound.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g9,"_g9");
					HX_STACK_LINE(703)
					this->coinsSound = _g9;
					HX_STACK_LINE(704)
					this->coinsSound->play(null());
					HX_STACK_LINE(705)
					this->loadAppleTimer = (int)1;
				}
			}
			HX_STACK_LINE(708)
			int _g10 = this->appleTruck->getAppleTruckCapacity();		HX_STACK_VAR(_g10,"_g10");
			struct _Function_2_1{
				inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
					HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",708,0xb30d7781)
					{
						struct _Function_3_1{
							inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
								HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",708,0xb30d7781)
								{
									HX_STACK_LINE(708)
									int _g11 = __this->market->getJuicePackCount();		HX_STACK_VAR(_g11,"_g11");
									HX_STACK_LINE(708)
									return (_g11 < (int)10);
								}
								return null();
							}
						};
						struct _Function_3_2{
							inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
								HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",708,0xb30d7781)
								{
									HX_STACK_LINE(708)
									int _g12 = __this->market->getPiePackCount();		HX_STACK_VAR(_g12,"_g12");
									HX_STACK_LINE(708)
									return (_g12 < (int)10);
								}
								return null();
							}
						};
						HX_STACK_LINE(708)
						return (  (((  (((__this->appleCount < (int)10))) ? bool(_Function_3_1::Block(__this)) : bool(false) ))) ? bool(_Function_3_2::Block(__this)) : bool(false) );
					}
					return null();
				}
			};
			HX_STACK_LINE(708)
			if (((  ((!(((_g10 == (int)0))))) ? bool(_Function_2_1::Block(this)) : bool(true) ))){
				HX_STACK_LINE(709)
				hx::SubEq(this->leaveTruckTimer,::flixel::FlxG_obj::elapsed);
				HX_STACK_LINE(710)
				if (((this->leaveTruckTimer < (int)0))){
					HX_STACK_LINE(711)
					this->truckButton->setButtonLevel((int)0);
					HX_STACK_LINE(712)
					this->truckButton->buttonSetting();
					HX_STACK_LINE(713)
					this->loadAppleTimer = (int)1;
					HX_STACK_LINE(714)
					this->appleTruck->setTruckSize(this->currentTruckLvl);
					HX_STACK_LINE(715)
					this->appleTruck->setLoadingApples(false);
					HX_STACK_LINE(716)
					this->appleTruck->setPickUpReady(false);
					HX_STACK_LINE(717)
					this->truckButton->setButtonClickable(false);
					HX_STACK_LINE(718)
					this->leaveTruckTimer = (int)2;
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,truckLoadingGoods,(void))

Void PlayState_obj::stopSeedDragIfBoard( ){
{
		HX_STACK_FRAME("PlayState","stopSeedDragIfBoard",0xac547091,"PlayState.stopSeedDragIfBoard","PlayState.hx",726,0xb30d7781)
		HX_STACK_THIS(this)
		struct _Function_1_1{
			inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",726,0xb30d7781)
				{
					HX_STACK_LINE(726)
					int _g = __this->market->getSeedCount();		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(726)
					return (_g < (int)1);
				}
				return null();
			}
		};
		HX_STACK_LINE(726)
		if (((  ((!(this->overlaysInUse))) ? bool(_Function_1_1::Block(this)) : bool(true) ))){
			HX_STACK_LINE(728)
			this->seedDrag->setDragging(false);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,stopSeedDragIfBoard,(void))

Void PlayState_obj::stopJuiceDragIfBoard( ){
{
		HX_STACK_FRAME("PlayState","stopJuiceDragIfBoard",0xd98ab548,"PlayState.stopJuiceDragIfBoard","PlayState.hx",736,0xb30d7781)
		HX_STACK_THIS(this)
		struct _Function_1_1{
			inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",736,0xb30d7781)
				{
					HX_STACK_LINE(736)
					int _g = __this->market->getJuiceCount();		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(736)
					return (_g < (int)1);
				}
				return null();
			}
		};
		HX_STACK_LINE(736)
		if (((  ((!(this->overlaysInUse))) ? bool(_Function_1_1::Block(this)) : bool(true) ))){
			HX_STACK_LINE(738)
			this->juiceMachineIconDrag->setDragging(false);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,stopJuiceDragIfBoard,(void))

Void PlayState_obj::stopPieDragIfBoard( ){
{
		HX_STACK_FRAME("PlayState","stopPieDragIfBoard",0x18c2f91c,"PlayState.stopPieDragIfBoard","PlayState.hx",747,0xb30d7781)
		HX_STACK_THIS(this)
		struct _Function_1_1{
			inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",747,0xb30d7781)
				{
					HX_STACK_LINE(747)
					int _g = __this->market->getPieCount();		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(747)
					return (_g < (int)1);
				}
				return null();
			}
		};
		HX_STACK_LINE(747)
		if (((  ((!(this->overlaysInUse))) ? bool(_Function_1_1::Block(this)) : bool(true) ))){
			HX_STACK_LINE(749)
			this->pieMachineIconDrag->setDragging(false);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,stopPieDragIfBoard,(void))

Void PlayState_obj::stopFishDragIfBoard( ){
{
		HX_STACK_FRAME("PlayState","stopFishDragIfBoard",0xfdc734ca,"PlayState.stopFishDragIfBoard","PlayState.hx",757,0xb30d7781)
		HX_STACK_THIS(this)
		struct _Function_1_1{
			inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",757,0xb30d7781)
				{
					HX_STACK_LINE(757)
					int _g = __this->market->getFishCount();		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(757)
					return (_g < (int)1);
				}
				return null();
			}
		};
		HX_STACK_LINE(757)
		if (((  ((!(this->overlaysInUse))) ? bool(_Function_1_1::Block(this)) : bool(true) ))){
			HX_STACK_LINE(759)
			this->fishIconDrag->setDragging(false);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,stopFishDragIfBoard,(void))

Void PlayState_obj::stopGreenHouseDragIfBoard( ){
{
		HX_STACK_FRAME("PlayState","stopGreenHouseDragIfBoard",0xccc3ef65,"PlayState.stopGreenHouseDragIfBoard","PlayState.hx",767,0xb30d7781)
		HX_STACK_THIS(this)
		struct _Function_1_1{
			inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",767,0xb30d7781)
				{
					HX_STACK_LINE(767)
					int _g = __this->market->getGreenHouseCount();		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(767)
					return (_g < (int)1);
				}
				return null();
			}
		};
		HX_STACK_LINE(767)
		if (((  ((!(this->overlaysInUse))) ? bool(_Function_1_1::Block(this)) : bool(true) ))){
			HX_STACK_LINE(769)
			this->greenHouseBarIconDrag->setDragging(false);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,stopGreenHouseDragIfBoard,(void))

Void PlayState_obj::drawBoard( ){
{
		HX_STACK_FRAME("PlayState","drawBoard",0xbb4b7fb1,"PlayState.drawBoard","PlayState.hx",775,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(776)
		this->add(this->foreGroundRocks);
		HX_STACK_LINE(777)
		Array< ::Dynamic > _g = this->tileBoard->getBoardArray();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(777)
		this->board = _g;
		HX_STACK_LINE(778)
		Array< ::Dynamic > _g1 = this->tileBoard->getBoardBoarderArray();		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(778)
		this->boarder = _g1;
		HX_STACK_LINE(779)
		Array< ::Dynamic > _g2 = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(779)
		this->treeArray = _g2;
		HX_STACK_LINE(780)
		Array< ::Dynamic > _g3 = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(780)
		this->juiceMachineArray = _g3;
		HX_STACK_LINE(781)
		Array< ::Dynamic > _g4 = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(_g4,"_g4");
		HX_STACK_LINE(781)
		this->pieMachineArray = _g4;
		HX_STACK_LINE(782)
		Array< ::Dynamic > _g5 = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(_g5,"_g5");
		HX_STACK_LINE(782)
		this->flyingJuiceBottleArray = _g5;
		HX_STACK_LINE(783)
		this->initTreeArray();
		HX_STACK_LINE(784)
		this->initJuiceMachineArray();
		HX_STACK_LINE(785)
		this->initPieMachineArray();
		HX_STACK_LINE(786)
		this->initFlyingJuiceBottleArray();
		HX_STACK_LINE(787)
		int shiftX = (int)0;		HX_STACK_VAR(shiftX,"shiftX");
		HX_STACK_LINE(788)
		int shiftY = (int)0;		HX_STACK_VAR(shiftY,"shiftY");
		HX_STACK_LINE(789)
		int newPosX = (int)0;		HX_STACK_VAR(newPosX,"newPosX");
		HX_STACK_LINE(790)
		int newPosY = (int)0;		HX_STACK_VAR(newPosY,"newPosY");
		HX_STACK_LINE(792)
		{
			HX_STACK_LINE(792)
			int _g11 = (int)0;		HX_STACK_VAR(_g11,"_g11");
			HX_STACK_LINE(792)
			int _g6 = this->board->length;		HX_STACK_VAR(_g6,"_g6");
			HX_STACK_LINE(792)
			while((true)){
				HX_STACK_LINE(792)
				if ((!(((_g11 < _g6))))){
					HX_STACK_LINE(792)
					break;
				}
				HX_STACK_LINE(792)
				int i = (_g11)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(794)
				shiftX = (int)0;
				HX_STACK_LINE(795)
				shiftY = (int)0;
				HX_STACK_LINE(797)
				{
					HX_STACK_LINE(797)
					int _g31 = (int)0;		HX_STACK_VAR(_g31,"_g31");
					HX_STACK_LINE(797)
					int _g21 = this->board->length;		HX_STACK_VAR(_g21,"_g21");
					HX_STACK_LINE(797)
					while((true)){
						HX_STACK_LINE(797)
						if ((!(((_g31 < _g21))))){
							HX_STACK_LINE(797)
							break;
						}
						HX_STACK_LINE(797)
						int j = (_g31)++;		HX_STACK_VAR(j,"j");
						HX_STACK_LINE(799)
						{
							HX_STACK_LINE(799)
							::Tile _g41 = this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >();		HX_STACK_VAR(_g41,"_g41");
							HX_STACK_LINE(799)
							_g41->set_x((_g41->x + ((newPosX + shiftX))));
						}
						HX_STACK_LINE(800)
						{
							HX_STACK_LINE(800)
							::Tile _g41 = this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >();		HX_STACK_VAR(_g41,"_g41");
							HX_STACK_LINE(800)
							_g41->set_y((_g41->y + ((newPosY + shiftY))));
						}
						HX_STACK_LINE(801)
						{
							HX_STACK_LINE(801)
							::TileBoarder _g41 = this->boarder->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::TileBoarder >();		HX_STACK_VAR(_g41,"_g41");
							HX_STACK_LINE(801)
							_g41->set_x((_g41->x + ((newPosX + shiftX))));
						}
						HX_STACK_LINE(802)
						{
							HX_STACK_LINE(802)
							::TileBoarder _g41 = this->boarder->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::TileBoarder >();		HX_STACK_VAR(_g41,"_g41");
							HX_STACK_LINE(802)
							_g41->set_y((_g41->y + ((newPosY + shiftY))));
						}
						HX_STACK_LINE(803)
						::flixel::plugin::MouseEventManager_obj::add(this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >(),this->buyButtonPopUp_dyn(),this->placeObject_dyn(),null(),null(),null(),null(),null());
						HX_STACK_LINE(804)
						this->add(this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >());
						HX_STACK_LINE(805)
						this->add(this->boarder->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::TileBoarder >());
						HX_STACK_LINE(806)
						hx::AddEq(shiftX,(int)96);
						HX_STACK_LINE(807)
						hx::AddEq(shiftY,(int)48);
					}
				}
				HX_STACK_LINE(811)
				hx::SubEq(newPosX,(int)96);
				HX_STACK_LINE(812)
				hx::AddEq(newPosY,(int)48);
			}
		}
		HX_STACK_LINE(815)
		this->board->__get((int)2).StaticCast< Array< ::Dynamic > >()->__get((int)0).StaticCast< ::Tile >()->setHasObject(true);
		HX_STACK_LINE(816)
		this->board->__get((int)2).StaticCast< Array< ::Dynamic > >()->__get((int)0).StaticCast< ::Tile >()->setAsOwned();
		HX_STACK_LINE(817)
		this->board->__get((int)1).StaticCast< Array< ::Dynamic > >()->__get((int)0).StaticCast< ::Tile >()->setAsOwned();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,drawBoard,(void))

Void PlayState_obj::letItRain( ::Tree treeObj){
{
		HX_STACK_FRAME("PlayState","letItRain",0xd3d1fcc9,"PlayState.letItRain","PlayState.hx",821,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(treeObj,"treeObj")
		HX_STACK_LINE(822)
		::Cloud _g = ::Cloud_obj::__new();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(822)
		this->cloud = _g;
		HX_STACK_LINE(823)
		Float _g1 = treeObj->getTreeX();		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(823)
		this->cloud->setCloudX(_g1);
		HX_STACK_LINE(824)
		Float _g2 = treeObj->getTreeY();		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(824)
		Float _g3 = (_g2 - (int)50);		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(824)
		this->cloud->setCloudY(_g3);
		HX_STACK_LINE(825)
		this->aboveGrassLayer->add(this->cloud);
		HX_STACK_LINE(826)
		this->cloud = null();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,letItRain,(void))

Void PlayState_obj::letItShine( ::Tree treeObj){
{
		HX_STACK_FRAME("PlayState","letItShine",0x1bf24656,"PlayState.letItShine","PlayState.hx",829,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(treeObj,"treeObj")
		HX_STACK_LINE(831)
		::SunToken _g = ::SunToken_obj::__new();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(831)
		this->sun = _g;
		HX_STACK_LINE(832)
		Float _g1 = treeObj->getTreeX();		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(832)
		Float _g2 = (_g1 - (int)2);		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(832)
		this->sun->setSunX(_g2);
		HX_STACK_LINE(833)
		Float _g3 = treeObj->getTreeY();		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(833)
		Float _g4 = (_g3 - (int)50);		HX_STACK_VAR(_g4,"_g4");
		HX_STACK_LINE(833)
		this->sun->setSunY(_g4);
		HX_STACK_LINE(834)
		this->aboveGrassLayer->add(this->sun);
		HX_STACK_LINE(835)
		this->sun = null();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,letItShine,(void))

Void PlayState_obj::unregisterAllAlertEvents( ){
{
		HX_STACK_FRAME("PlayState","unregisterAllAlertEvents",0xaae15401,"PlayState.unregisterAllAlertEvents","PlayState.hx",840,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(840)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(840)
		int _g = this->treeArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(840)
		while((true)){
			HX_STACK_LINE(840)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(840)
				break;
			}
			HX_STACK_LINE(840)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(841)
			if (((this->treeArray->__get(i).StaticCast< ::Tree >() != null()))){
				HX_STACK_LINE(842)
				this->treeArray->__get(i).StaticCast< ::Tree >()->unregisterAlertEvents();
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,unregisterAllAlertEvents,(void))

Void PlayState_obj::registerAllAlertEvents( ){
{
		HX_STACK_FRAME("PlayState","registerAllAlertEvents",0xa77b44a8,"PlayState.registerAllAlertEvents","PlayState.hx",849,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(849)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(849)
		int _g = this->treeArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(849)
		while((true)){
			HX_STACK_LINE(849)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(849)
				break;
			}
			HX_STACK_LINE(849)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(850)
			if (((this->treeArray->__get(i).StaticCast< ::Tree >() != null()))){
				HX_STACK_LINE(851)
				this->treeArray->__get(i).StaticCast< ::Tree >()->registerAlertEvents();
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,registerAllAlertEvents,(void))

Void PlayState_obj::checkForAlertClick( ){
{
		HX_STACK_FRAME("PlayState","checkForAlertClick",0xd1ef603e,"PlayState.checkForAlertClick","PlayState.hx",857,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(859)
		{
			HX_STACK_LINE(859)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(859)
			int _g = this->treeArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(859)
			while((true)){
				HX_STACK_LINE(859)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(859)
					break;
				}
				HX_STACK_LINE(859)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(860)
				if (((this->treeArray->__get(i).StaticCast< ::Tree >() != null()))){
					HX_STACK_LINE(861)
					if (((  ((!(this->treeArray->__get(i).StaticCast< ::Tree >()->getDropletClicked()))) ? bool(this->treeArray->__get(i).StaticCast< ::Tree >()->getSunClicked()) : bool(true) ))){
						HX_STACK_LINE(862)
						::flixel::system::FlxSound _g2 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/alertButtonSound.wav"),.3,null(),null(),null(),null(),null());		HX_STACK_VAR(_g2,"_g2");
						HX_STACK_LINE(862)
						this->alertButtonSound = _g2;
						HX_STACK_LINE(863)
						this->alertButtonSound->play(null());
						HX_STACK_LINE(864)
						if (((this->tutorial != null()))){
							HX_STACK_LINE(865)
							Float _g11 = this->tutorial->getCurrentTutSectionNumber();		HX_STACK_VAR(_g11,"_g11");
							HX_STACK_LINE(865)
							if (((  (((  (((_g11 == (int)3))) ? bool(!(this->tutUnpauseAlertsDone)) : bool(false) ))) ? bool(this->tutorialOn) : bool(false) ))){
								HX_STACK_LINE(866)
								this->tutorial->destroy();
								HX_STACK_LINE(867)
								this->treeArray->__get((int)0).StaticCast< ::Tree >()->unpauseAlertTimers();
								HX_STACK_LINE(868)
								this->tutUnpauseAlertsDone = true;
							}
							HX_STACK_LINE(870)
							Float _g21 = this->tutorial->getCurrentTutSectionNumber();		HX_STACK_VAR(_g21,"_g21");
							struct _Function_6_1{
								inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
									HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",870,0xb30d7781)
									{
										HX_STACK_LINE(870)
										Float _g3 = __this->tutorial->getCurrentTutSectionNumber();		HX_STACK_VAR(_g3,"_g3");
										HX_STACK_LINE(870)
										return (_g3 == (int)4);
									}
									return null();
								}
							};
							HX_STACK_LINE(870)
							if (((  (((  ((!(((_g21 == (int)1))))) ? bool(_Function_6_1::Block(this)) : bool(true) ))) ? bool(this->tutorialOn) : bool(false) ))){
								HX_STACK_LINE(871)
								this->tutorial->destroy();
							}
						}
						HX_STACK_LINE(874)
						if ((this->treeArray->__get(i).StaticCast< ::Tree >()->getSunClicked())){
							HX_STACK_LINE(875)
							this->sunClicked = true;
							HX_STACK_LINE(876)
							this->dropletClicked = false;
						}
						HX_STACK_LINE(878)
						if ((this->treeArray->__get(i).StaticCast< ::Tree >()->getDropletClicked())){
							HX_STACK_LINE(879)
							this->dropletClicked = true;
							HX_STACK_LINE(880)
							this->sunClicked = false;
						}
						HX_STACK_LINE(884)
						this->currentTargetTree = this->treeArray->__get(i).StaticCast< ::Tree >();
						HX_STACK_LINE(885)
						if ((!(this->mathless))){
							HX_STACK_LINE(886)
							this->bb->destroy();
							HX_STACK_LINE(887)
							this->bb = null();
							HX_STACK_LINE(888)
							::ProblemBoard _g4 = ::ProblemBoard_obj::__new();		HX_STACK_VAR(_g4,"_g4");
							HX_STACK_LINE(888)
							this->bb = _g4;
							HX_STACK_LINE(889)
							this->unregisterAllAlertEvents();
							HX_STACK_LINE(890)
							this->add(this->numPad);
							HX_STACK_LINE(891)
							this->add(this->bb);
							HX_STACK_LINE(892)
							this->add(this->answerFieldText);
							HX_STACK_LINE(893)
							this->numPad->setPadScrollingIn(true);
							HX_STACK_LINE(894)
							this->numPad->registerEvent();
							HX_STACK_LINE(895)
							this->bb->pickOperation(HX_CSTRING("tree"));
							HX_STACK_LINE(896)
							this->bb->setBoardInUse(true);
							HX_STACK_LINE(897)
							this->overlaysInUse = true;
						}
						HX_STACK_LINE(899)
						this->treeArray->__get(i).StaticCast< ::Tree >()->setDropletClicked(false);
						HX_STACK_LINE(900)
						this->treeArray->__get(i).StaticCast< ::Tree >()->setSunClicked(false);
					}
				}
			}
		}
		HX_STACK_LINE(904)
		{
			HX_STACK_LINE(904)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(904)
			int _g = this->juiceMachineArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(904)
			while((true)){
				HX_STACK_LINE(904)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(904)
					break;
				}
				HX_STACK_LINE(904)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(905)
				if (((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >() != null()))){
					HX_STACK_LINE(906)
					if ((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->getRepairClicked())){
						HX_STACK_LINE(907)
						::flixel::system::FlxSound _g5 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/alertButtonSound.wav"),.6,null(),null(),null(),null(),null());		HX_STACK_VAR(_g5,"_g5");
						HX_STACK_LINE(907)
						this->alertButtonSound = _g5;
						HX_STACK_LINE(908)
						this->alertButtonSound->play(null());
						HX_STACK_LINE(909)
						this->currentTargetJuiceMachine = this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >();
						HX_STACK_LINE(910)
						this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->setRepairClicked(false);
						HX_STACK_LINE(911)
						if ((!(this->mathless))){
							HX_STACK_LINE(912)
							this->members->__Field(HX_CSTRING("remove"),true)(this->numPad);
							HX_STACK_LINE(913)
							this->members->__Field(HX_CSTRING("remove"),true)(this->bb);
							HX_STACK_LINE(914)
							this->members->__Field(HX_CSTRING("remove"),true)(this->answerFieldText);
							HX_STACK_LINE(915)
							this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->unregisterMachineEvents();
							HX_STACK_LINE(916)
							this->add(this->numPad);
							HX_STACK_LINE(917)
							this->add(this->bb);
							HX_STACK_LINE(918)
							this->add(this->answerFieldText);
							HX_STACK_LINE(919)
							this->numPad->setPadScrollingIn(true);
							HX_STACK_LINE(920)
							this->numPad->registerEvent();
							HX_STACK_LINE(921)
							this->bb->pickOperation(HX_CSTRING("machine"));
							HX_STACK_LINE(922)
							this->bb->setBoardInUse(true);
							HX_STACK_LINE(923)
							this->overlaysInUse = true;
						}
						else{
							HX_STACK_LINE(926)
							this->currentTargetJuiceMachine->registerMachineButtonEvent();
						}
					}
				}
			}
		}
		HX_STACK_LINE(931)
		{
			HX_STACK_LINE(931)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(931)
			int _g = this->pieMachineArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(931)
			while((true)){
				HX_STACK_LINE(931)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(931)
					break;
				}
				HX_STACK_LINE(931)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(932)
				if (((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >() != null()))){
					HX_STACK_LINE(933)
					if ((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->getRepairClicked())){
						HX_STACK_LINE(934)
						::flixel::system::FlxSound _g6 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/alertButtonSound.wav"),.6,null(),null(),null(),null(),null());		HX_STACK_VAR(_g6,"_g6");
						HX_STACK_LINE(934)
						this->alertButtonSound = _g6;
						HX_STACK_LINE(935)
						this->alertButtonSound->play(null());
						HX_STACK_LINE(936)
						this->currentTargetPieMachine = this->pieMachineArray->__get(i).StaticCast< ::PieMachine >();
						HX_STACK_LINE(937)
						this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->setRepairClicked(false);
						HX_STACK_LINE(938)
						if ((!(this->mathless))){
							HX_STACK_LINE(939)
							this->members->__Field(HX_CSTRING("remove"),true)(this->numPad);
							HX_STACK_LINE(940)
							this->members->__Field(HX_CSTRING("remove"),true)(this->bb);
							HX_STACK_LINE(941)
							this->members->__Field(HX_CSTRING("remove"),true)(this->answerFieldText);
							HX_STACK_LINE(942)
							this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->unregisterMachineEvents();
							HX_STACK_LINE(943)
							this->add(this->numPad);
							HX_STACK_LINE(944)
							this->add(this->bb);
							HX_STACK_LINE(945)
							this->add(this->answerFieldText);
							HX_STACK_LINE(946)
							this->numPad->setPadScrollingIn(true);
							HX_STACK_LINE(947)
							this->numPad->registerEvent();
							HX_STACK_LINE(948)
							this->bb->pickOperation(HX_CSTRING("machine"));
							HX_STACK_LINE(949)
							this->bb->setBoardInUse(true);
							HX_STACK_LINE(950)
							this->overlaysInUse = true;
						}
						else{
							HX_STACK_LINE(953)
							this->currentTargetPieMachine->registerMachineButtonEvent();
						}
					}
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,checkForAlertClick,(void))

Void PlayState_obj::submitAnswer( ){
{
		HX_STACK_FRAME("PlayState","submitAnswer",0xe4355f67,"PlayState.submitAnswer","PlayState.hx",962,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(962)
		if (((  ((!(this->numPad->getAnswerButtonPressed()))) ? bool(this->mathless) : bool(true) ))){
			HX_STACK_LINE(963)
			if ((!(this->mathless))){
				HX_STACK_LINE(964)
				this->numPad->unregisterNumPadEvents();
			}
			HX_STACK_LINE(966)
			::String _g = this->numPad->getPlayerAnswerString();		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(966)
			::String _g1 = this->bb->getProblemAnswerString();		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(966)
			if (((  ((!(((_g == _g1))))) ? bool(this->mathless) : bool(true) ))){
				HX_STACK_LINE(967)
				if ((!(this->mathless))){
					HX_STACK_LINE(968)
					::flixel::system::FlxSound _g2 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/correctAnswerSoundA.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(968)
					this->correctAnswerSound = _g2;
					HX_STACK_LINE(969)
					this->correctAnswerSound->play(null());
					HX_STACK_LINE(970)
					this->bb->setBoarderColor(HX_CSTRING("correctAnswer"));
				}
				HX_STACK_LINE(972)
				if ((this->dropletClicked)){
					struct _Function_4_1{
						inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
							HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",973,0xb30d7781)
							{
								HX_STACK_LINE(973)
								int _g3 = __this->treeArray->__get((int)0).StaticCast< ::Tree >()->getTreeGrowthPhase();		HX_STACK_VAR(_g3,"_g3");
								HX_STACK_LINE(973)
								return (_g3 < (int)6);
							}
							return null();
						}
					};
					HX_STACK_LINE(973)
					if (((  ((this->tutorialOn)) ? bool(_Function_4_1::Block(this)) : bool(false) ))){
						HX_STACK_LINE(974)
						this->treeArray->__get((int)0).StaticCast< ::Tree >()->setThirstValue((int)0);
						HX_STACK_LINE(975)
						this->treeArray->__get((int)0).StaticCast< ::Tree >()->incGrowTree();
						HX_STACK_LINE(976)
						this->treeArray->__get((int)0).StaticCast< ::Tree >()->setSunlightValue((int)1);
					}
					HX_STACK_LINE(978)
					this->currentTargetTree->setTreeWatered();
					HX_STACK_LINE(979)
					this->currentTargetTree->incGrowTree();
					HX_STACK_LINE(980)
					this->letItRain(this->currentTargetTree);
					HX_STACK_LINE(981)
					this->currentTargetTree->resetDropTimer();
					HX_STACK_LINE(982)
					this->dropletClicked = false;
				}
				else{
					HX_STACK_LINE(984)
					if ((this->sunClicked)){
						struct _Function_5_1{
							inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
								HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",985,0xb30d7781)
								{
									HX_STACK_LINE(985)
									int _g4 = __this->treeArray->__get((int)0).StaticCast< ::Tree >()->getTreeGrowthPhase();		HX_STACK_VAR(_g4,"_g4");
									HX_STACK_LINE(985)
									return (_g4 < (int)6);
								}
								return null();
							}
						};
						HX_STACK_LINE(985)
						if (((  ((this->tutorialOn)) ? bool(_Function_5_1::Block(this)) : bool(false) ))){
							HX_STACK_LINE(986)
							this->treeArray->__get((int)0).StaticCast< ::Tree >()->setSunlightValue((int)0);
							HX_STACK_LINE(987)
							this->treeArray->__get((int)0).StaticCast< ::Tree >()->incGrowTree();
							HX_STACK_LINE(988)
							this->treeArray->__get((int)0).StaticCast< ::Tree >()->setThirstValue((int)1);
						}
						HX_STACK_LINE(990)
						this->currentTargetTree->setTreeSunLight();
						HX_STACK_LINE(991)
						this->currentTargetTree->incGrowTree();
						HX_STACK_LINE(992)
						this->letItShine(this->currentTargetTree);
						HX_STACK_LINE(993)
						this->currentTargetTree->resetSunTimer();
						HX_STACK_LINE(994)
						this->sunClicked = false;
					}
					else{
						HX_STACK_LINE(996)
						if (((this->currentTargetJuiceMachine != null()))){
							HX_STACK_LINE(997)
							this->currentTargetJuiceMachine->setMachineOn(false);
							HX_STACK_LINE(998)
							this->currentTargetJuiceMachine->resetRepairAlertTimer();
							HX_STACK_LINE(999)
							this->currentTargetJuiceMachine->setAnimateRepairSprite(false);
							HX_STACK_LINE(1000)
							this->currentTargetJuiceMachine = null();
						}
						else{
							HX_STACK_LINE(1002)
							if (((this->currentTargetPieMachine != null()))){
								HX_STACK_LINE(1003)
								this->currentTargetPieMachine->setMachineOn(false);
								HX_STACK_LINE(1004)
								this->currentTargetPieMachine->resetRepairAlertTimer();
								HX_STACK_LINE(1005)
								this->currentTargetPieMachine->setAnimateRepairSprite(false);
								HX_STACK_LINE(1006)
								this->currentTargetPieMachine = null();
							}
						}
					}
				}
			}
			else{
				HX_STACK_LINE(1010)
				if ((this->tutorialOn)){
					HX_STACK_LINE(1011)
					int _g5 = this->treeArray->__get((int)0).StaticCast< ::Tree >()->getTreeGrowthPhase();		HX_STACK_VAR(_g5,"_g5");
					HX_STACK_LINE(1011)
					if (((_g5 > (int)5))){
						HX_STACK_LINE(1012)
						this->bb->setBoarderColor(HX_CSTRING("incorrectAnswer"));
						HX_STACK_LINE(1013)
						::flixel::system::FlxSound _g6 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/incorrectAnswerSound.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g6,"_g6");
						HX_STACK_LINE(1013)
						this->incorrectAnswerSound = _g6;
						HX_STACK_LINE(1014)
						this->incorrectAnswerSound->play(null());
						HX_STACK_LINE(1015)
						if ((this->dropletClicked)){
							HX_STACK_LINE(1016)
							this->currentTargetTree->setTreeThirsty(null());
						}
						else{
							HX_STACK_LINE(1018)
							if ((this->sunClicked)){
								HX_STACK_LINE(1019)
								this->currentTargetTree->setTreeNoLight(null());
							}
						}
					}
				}
				else{
					HX_STACK_LINE(1024)
					if ((!(this->mathless))){
						HX_STACK_LINE(1025)
						this->bb->setBoarderColor(HX_CSTRING("incorrectAnswer"));
						HX_STACK_LINE(1026)
						::flixel::system::FlxSound _g7 = ::flixel::FlxG_obj::sound->load(HX_CSTRING("assets/sounds/incorrectAnswerSound.wav"),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(_g7,"_g7");
						HX_STACK_LINE(1026)
						this->incorrectAnswerSound = _g7;
						HX_STACK_LINE(1027)
						this->incorrectAnswerSound->play(null());
						HX_STACK_LINE(1028)
						if ((this->dropletClicked)){
							HX_STACK_LINE(1029)
							this->currentTargetTree->setTreeThirsty(null());
						}
						else{
							HX_STACK_LINE(1031)
							if ((this->sunClicked)){
								HX_STACK_LINE(1032)
								this->currentTargetTree->setTreeNoLight(null());
							}
						}
					}
				}
			}
			HX_STACK_LINE(1037)
			if ((!(this->mathless))){
				HX_STACK_LINE(1038)
				this->bb->setBoardInUse(false);
				HX_STACK_LINE(1039)
				this->numPad->setPadScrollingIn(false);
				HX_STACK_LINE(1040)
				this->overlaysInUse = false;
				HX_STACK_LINE(1041)
				this->numPad->resetAnswerDigits();
				HX_STACK_LINE(1042)
				this->bb->resetProblemString();
				HX_STACK_LINE(1043)
				this->registerAllAlertEvents();
				HX_STACK_LINE(1044)
				this->numPad->setAnswerButtonPressed(false);
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,submitAnswer,(void))

Void PlayState_obj::destroyObjects( ){
{
		HX_STACK_FRAME("PlayState","destroyObjects",0x58f9d8eb,"PlayState.destroyObjects","PlayState.hx",1051,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1051)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(1051)
		int _g = this->treeArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(1051)
		while((true)){
			HX_STACK_LINE(1051)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(1051)
				break;
			}
			HX_STACK_LINE(1051)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(1052)
			if (((this->treeArray->__get(i).StaticCast< ::Tree >() != null()))){
				HX_STACK_LINE(1053)
				if ((this->treeArray->__get(i).StaticCast< ::Tree >()->getAmIDeadYet())){
					HX_STACK_LINE(1054)
					::flixel::FlxSprite _g2 = this->treeArray->__get(i).StaticCast< ::Tree >()->getTreeSprite();		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(1054)
					::flixel::plugin::MouseEventManager_obj::remove(_g2);
					HX_STACK_LINE(1055)
					this->treeArray->__get(i).StaticCast< ::Tree >()->getMyTile()->setHasObject(false);
					HX_STACK_LINE(1056)
					if (((this->currentTargetTree == this->treeArray->__get(i).StaticCast< ::Tree >()))){
						HX_STACK_LINE(1057)
						this->currentTargetTree = null();
					}
					HX_STACK_LINE(1060)
					this->treeArray->__get(i).StaticCast< ::Tree >()->destroy();
					HX_STACK_LINE(1061)
					this->treeArray[i] = null();
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,destroyObjects,(void))

Void PlayState_obj::waterBill( ){
{
		HX_STACK_FRAME("PlayState","waterBill",0x000b536d,"PlayState.waterBill","PlayState.hx",1067,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1068)
		int clientCount = (int)0;		HX_STACK_VAR(clientCount,"clientCount");
		HX_STACK_LINE(1069)
		{
			HX_STACK_LINE(1069)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(1069)
			int _g = this->treeArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(1069)
			while((true)){
				HX_STACK_LINE(1069)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(1069)
					break;
				}
				HX_STACK_LINE(1069)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(1070)
				if (((this->treeArray->__get(i).StaticCast< ::Tree >() != null()))){
					HX_STACK_LINE(1071)
					if ((this->treeArray->__get(i).StaticCast< ::Tree >()->getSprinklerSystemOn())){
						HX_STACK_LINE(1072)
						(clientCount)++;
					}
				}
			}
		}
		HX_STACK_LINE(1076)
		this->market->setWaterClientCount(clientCount);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,waterBill,(void))

Void PlayState_obj::electricBill( ){
{
		HX_STACK_FRAME("PlayState","electricBill",0x71cce235,"PlayState.electricBill","PlayState.hx",1079,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1080)
		int clientCount = (int)0;		HX_STACK_VAR(clientCount,"clientCount");
		HX_STACK_LINE(1081)
		{
			HX_STACK_LINE(1081)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(1081)
			int _g = this->juiceMachineArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(1081)
			while((true)){
				HX_STACK_LINE(1081)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(1081)
					break;
				}
				HX_STACK_LINE(1081)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(1082)
				if (((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >() != null()))){
					HX_STACK_LINE(1083)
					if ((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->getMachineOn())){
						HX_STACK_LINE(1084)
						(clientCount)++;
					}
				}
			}
		}
		HX_STACK_LINE(1088)
		{
			HX_STACK_LINE(1088)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(1088)
			int _g = this->pieMachineArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(1088)
			while((true)){
				HX_STACK_LINE(1088)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(1088)
					break;
				}
				HX_STACK_LINE(1088)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(1089)
				if (((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >() != null()))){
					HX_STACK_LINE(1090)
					if ((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->getMachineOn())){
						HX_STACK_LINE(1091)
						(clientCount)++;
					}
				}
			}
		}
		HX_STACK_LINE(1095)
		this->market->setElectricClientCount(clientCount);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,electricBill,(void))

Void PlayState_obj::appleBill( int machinesInUse){
{
		HX_STACK_FRAME("PlayState","appleBill",0xe3351e30,"PlayState.appleBill","PlayState.hx",1098,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(machinesInUse,"machinesInUse")
		HX_STACK_LINE(1099)
		hx::SubEq(this->appleForJuiceTimer,::flixel::FlxG_obj::elapsed);
		HX_STACK_LINE(1100)
		if (((this->appleCount > (int)1))){
			HX_STACK_LINE(1101)
			if (((this->appleForJuiceTimer < (int)0))){
				HX_STACK_LINE(1102)
				hx::SubEq(this->appleCount,(machinesInUse * (int)2));
				HX_STACK_LINE(1103)
				this->market->stockJuicePacks(machinesInUse);
				HX_STACK_LINE(1105)
				this->appleForJuiceTimer = (int)1;
			}
		}
		else{
			HX_STACK_LINE(1109)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(1109)
			int _g = this->juiceMachineArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(1109)
			while((true)){
				HX_STACK_LINE(1109)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(1109)
					break;
				}
				HX_STACK_LINE(1109)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(1110)
				if (((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >() != null()))){
					HX_STACK_LINE(1111)
					this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->setFillingBottle(false);
					HX_STACK_LINE(1112)
					this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->setMachineOn(true);
					HX_STACK_LINE(1113)
					this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->turnMachineOn(null());
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,appleBill,(void))

Void PlayState_obj::appleAndJuiceBill( int machinesInUse){
{
		HX_STACK_FRAME("PlayState","appleAndJuiceBill",0xcf1514d9,"PlayState.appleAndJuiceBill","PlayState.hx",1119,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(machinesInUse,"machinesInUse")
		HX_STACK_LINE(1120)
		hx::SubEq(this->appleAndJuiceForPieTimer,::flixel::FlxG_obj::elapsed);
		struct _Function_1_1{
			inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",1121,0xb30d7781)
				{
					HX_STACK_LINE(1121)
					int _g = __this->market->getJuicePackCount();		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(1121)
					return (_g > (int)0);
				}
				return null();
			}
		};
		HX_STACK_LINE(1121)
		if (((  (((this->appleCount > (int)1))) ? bool(_Function_1_1::Block(this)) : bool(false) ))){
			HX_STACK_LINE(1122)
			if (((this->appleAndJuiceForPieTimer < (int)0))){
				HX_STACK_LINE(1123)
				hx::SubEq(this->appleCount,(machinesInUse * (int)2));
				HX_STACK_LINE(1124)
				this->market->decjuicePackCount(machinesInUse);
				HX_STACK_LINE(1125)
				this->market->stockPiePacks(machinesInUse);
				HX_STACK_LINE(1127)
				this->appleAndJuiceForPieTimer = (int)1;
			}
		}
		else{
			HX_STACK_LINE(1131)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(1131)
			int _g = this->juiceMachineArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(1131)
			while((true)){
				HX_STACK_LINE(1131)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(1131)
					break;
				}
				HX_STACK_LINE(1131)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(1132)
				if (((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >() != null()))){
					HX_STACK_LINE(1133)
					this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->setMachineOn(true);
					HX_STACK_LINE(1134)
					this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->turnMachineOn(null());
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,appleAndJuiceBill,(void))

Void PlayState_obj::turnWaterOff( ){
{
		HX_STACK_FRAME("PlayState","turnWaterOff",0x16ba7be6,"PlayState.turnWaterOff","PlayState.hx",1140,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1141)
		int _g = this->market->getCoinCount();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(1141)
		if (((_g <= (int)0))){
			HX_STACK_LINE(1142)
			this->market->setCoinCount((int)0);
			HX_STACK_LINE(1143)
			{
				HX_STACK_LINE(1143)
				int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(1143)
				int _g2 = this->treeArray->length;		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(1143)
				while((true)){
					HX_STACK_LINE(1143)
					if ((!(((_g1 < _g2))))){
						HX_STACK_LINE(1143)
						break;
					}
					HX_STACK_LINE(1143)
					int i = (_g1)++;		HX_STACK_VAR(i,"i");
					HX_STACK_LINE(1144)
					if (((this->treeArray->__get(i).StaticCast< ::Tree >() != null()))){
						HX_STACK_LINE(1145)
						this->treeArray->__get(i).StaticCast< ::Tree >()->setSprinklerSystemOn(false);
					}
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,turnWaterOff,(void))

Void PlayState_obj::applesAndJuiceForPie( ){
{
		HX_STACK_FRAME("PlayState","applesAndJuiceForPie",0x68212b16,"PlayState.applesAndJuiceForPie","PlayState.hx",1152,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1152)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(1152)
		int _g = this->pieMachineArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(1152)
		while((true)){
			HX_STACK_LINE(1152)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(1152)
				break;
			}
			HX_STACK_LINE(1152)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(1153)
			if (((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >() != null()))){
				struct _Function_3_1{
					inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",1154,0xb30d7781)
						{
							HX_STACK_LINE(1154)
							int _g2 = __this->market->getJuicePackCount();		HX_STACK_VAR(_g2,"_g2");
							HX_STACK_LINE(1154)
							return (_g2 > (int)0);
						}
						return null();
					}
				};
				HX_STACK_LINE(1154)
				if (((  (((  (((this->appleCount > (int)1))) ? bool(_Function_3_1::Block(this)) : bool(false) ))) ? bool(this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->getMachineOn()) : bool(false) ))){
					HX_STACK_LINE(1155)
					hx::SubEq(this->appleAndJuiceForPieTimer,::flixel::FlxG_obj::elapsed);
					HX_STACK_LINE(1156)
					if (((this->appleAndJuiceForPieTimer < (int)0))){
						HX_STACK_LINE(1157)
						hx::SubEq(this->appleCount,(int)2);
						HX_STACK_LINE(1158)
						this->market->decjuicePackCount((int)1);
						HX_STACK_LINE(1159)
						this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->setPastryFlying(true);
						HX_STACK_LINE(1160)
						::flixel::FlxSprite _g11 = this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->getPieMachineFlyingPastry();		HX_STACK_VAR(_g11,"_g11");
						HX_STACK_LINE(1160)
						this->add(_g11);
						HX_STACK_LINE(1161)
						this->appleAndJuiceForPieTimer = (int)1;
					}
				}
				else{
					HX_STACK_LINE(1165)
					if (((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >() != null()))){
						HX_STACK_LINE(1166)
						this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->setMachineOn(true);
						HX_STACK_LINE(1167)
						this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->turnMachineOn(null());
					}
				}
				HX_STACK_LINE(1170)
				if ((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->getInPiePack())){
					HX_STACK_LINE(1171)
					this->market->stockPiePacks((int)1);
					HX_STACK_LINE(1172)
					this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->setInPiePack(false);
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,applesAndJuiceForPie,(void))

Void PlayState_obj::applesForJuice( ){
{
		HX_STACK_FRAME("PlayState","applesForJuice",0x9d206881,"PlayState.applesForJuice","PlayState.hx",1179,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1179)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(1179)
		int _g = this->juiceMachineArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(1179)
		while((true)){
			HX_STACK_LINE(1179)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(1179)
				break;
			}
			HX_STACK_LINE(1179)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(1180)
			if (((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >() != null()))){
				HX_STACK_LINE(1181)
				if (((  (((this->appleCount > (int)1))) ? bool(this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->getMachineOn()) : bool(false) ))){
					HX_STACK_LINE(1182)
					hx::SubEq(this->appleForJuiceTimer,::flixel::FlxG_obj::elapsed);
					HX_STACK_LINE(1183)
					if (((this->appleForJuiceTimer < (int)0))){
						HX_STACK_LINE(1184)
						hx::SubEq(this->appleCount,(int)2);
						HX_STACK_LINE(1185)
						this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->setBottleFlying(true);
						HX_STACK_LINE(1186)
						::flixel::FlxSprite _g2 = this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->getJuiceMachineFlyingBotte();		HX_STACK_VAR(_g2,"_g2");
						HX_STACK_LINE(1186)
						this->add(_g2);
						HX_STACK_LINE(1187)
						this->appleForJuiceTimer = (int)1;
					}
				}
				else{
					HX_STACK_LINE(1191)
					if (((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >() != null()))){
						HX_STACK_LINE(1192)
						this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->setFillingBottle(false);
						HX_STACK_LINE(1193)
						this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->setMachineOn(true);
						HX_STACK_LINE(1194)
						this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->turnMachineOn(null());
					}
				}
				HX_STACK_LINE(1197)
				if ((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->getInJuicePack())){
					HX_STACK_LINE(1198)
					this->market->stockJuicePacks((int)1);
					HX_STACK_LINE(1199)
					this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->setInJuicePack(false);
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,applesForJuice,(void))

Void PlayState_obj::waterSprinklerBill( ){
{
		HX_STACK_FRAME("PlayState","waterSprinklerBill",0xa60b65a9,"PlayState.waterSprinklerBill","PlayState.hx",1206,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1206)
		int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(1206)
		int _g = this->treeArray->length;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(1206)
		while((true)){
			HX_STACK_LINE(1206)
			if ((!(((_g1 < _g))))){
				HX_STACK_LINE(1206)
				break;
			}
			HX_STACK_LINE(1206)
			int i = (_g1)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(1207)
			if (((this->treeArray->__get(i).StaticCast< ::Tree >() != null()))){
				HX_STACK_LINE(1208)
				if ((this->treeArray->__get(i).StaticCast< ::Tree >()->getPayWaterBill())){
					HX_STACK_LINE(1209)
					this->market->coinPaymentUtil((int)1);
					HX_STACK_LINE(1210)
					this->treeArray->__get(i).StaticCast< ::Tree >()->setPayWaterBill(false);
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,waterSprinklerBill,(void))

Void PlayState_obj::factoryElectricBill( ){
{
		HX_STACK_FRAME("PlayState","factoryElectricBill",0x5caeb2dd,"PlayState.factoryElectricBill","PlayState.hx",1216,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1217)
		{
			HX_STACK_LINE(1217)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(1217)
			int _g = this->juiceMachineArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(1217)
			while((true)){
				HX_STACK_LINE(1217)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(1217)
					break;
				}
				HX_STACK_LINE(1217)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(1218)
				if (((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >() != null()))){
					HX_STACK_LINE(1219)
					if ((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->getPayElectricBill())){
						HX_STACK_LINE(1220)
						this->market->coinPaymentUtil((int)1);
						HX_STACK_LINE(1221)
						this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->setPayElectricBill(false);
					}
				}
			}
		}
		HX_STACK_LINE(1225)
		{
			HX_STACK_LINE(1225)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(1225)
			int _g = this->pieMachineArray->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(1225)
			while((true)){
				HX_STACK_LINE(1225)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(1225)
					break;
				}
				HX_STACK_LINE(1225)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(1226)
				if (((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >() != null()))){
					HX_STACK_LINE(1227)
					if ((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->getPayElectricBill())){
						HX_STACK_LINE(1228)
						this->market->coinPaymentUtil((int)1);
						HX_STACK_LINE(1229)
						this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->setPayElectricBill(false);
					}
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,factoryElectricBill,(void))

Void PlayState_obj::removeCallBackOnPad( ){
{
		HX_STACK_FRAME("PlayState","removeCallBackOnPad",0x8efa3eda,"PlayState.removeCallBackOnPad","PlayState.hx",1235,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1236)
		if (((  ((this->numPad->getPadScrollingIn())) ? bool(this->callBackStopOne) : bool(false) ))){
			HX_STACK_LINE(1237)
			{
				HX_STACK_LINE(1237)
				int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(1237)
				int _g = this->treeArray->length;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(1237)
				while((true)){
					HX_STACK_LINE(1237)
					if ((!(((_g1 < _g))))){
						HX_STACK_LINE(1237)
						break;
					}
					HX_STACK_LINE(1237)
					int i = (_g1)++;		HX_STACK_VAR(i,"i");
					HX_STACK_LINE(1238)
					if (((this->treeArray->__get(i).StaticCast< ::Tree >() != null()))){
						HX_STACK_LINE(1239)
						this->treeArray->__get(i).StaticCast< ::Tree >()->unregisterTreeClickedMouseEvent();
					}
				}
			}
			HX_STACK_LINE(1242)
			{
				HX_STACK_LINE(1242)
				int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(1242)
				int _g = this->juiceMachineArray->length;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(1242)
				while((true)){
					HX_STACK_LINE(1242)
					if ((!(((_g1 < _g))))){
						HX_STACK_LINE(1242)
						break;
					}
					HX_STACK_LINE(1242)
					int i = (_g1)++;		HX_STACK_VAR(i,"i");
					HX_STACK_LINE(1243)
					if (((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >() != null()))){
						HX_STACK_LINE(1244)
						this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->unregisterMachineEvents();
					}
				}
			}
			HX_STACK_LINE(1247)
			{
				HX_STACK_LINE(1247)
				int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(1247)
				int _g = this->pieMachineArray->length;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(1247)
				while((true)){
					HX_STACK_LINE(1247)
					if ((!(((_g1 < _g))))){
						HX_STACK_LINE(1247)
						break;
					}
					HX_STACK_LINE(1247)
					int i = (_g1)++;		HX_STACK_VAR(i,"i");
					HX_STACK_LINE(1248)
					if (((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >() != null()))){
						HX_STACK_LINE(1249)
						this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->unregisterMachineEvents();
					}
				}
			}
			HX_STACK_LINE(1252)
			this->callBackStopOne = false;
			HX_STACK_LINE(1253)
			this->callBackStopTwo = true;
		}
		HX_STACK_LINE(1255)
		if (((  ((!(this->numPad->getPadScrollingIn()))) ? bool(this->callBackStopTwo) : bool(false) ))){
			HX_STACK_LINE(1256)
			{
				HX_STACK_LINE(1256)
				int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(1256)
				int _g = this->treeArray->length;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(1256)
				while((true)){
					HX_STACK_LINE(1256)
					if ((!(((_g1 < _g))))){
						HX_STACK_LINE(1256)
						break;
					}
					HX_STACK_LINE(1256)
					int i = (_g1)++;		HX_STACK_VAR(i,"i");
					HX_STACK_LINE(1257)
					if (((this->treeArray->__get(i).StaticCast< ::Tree >() != null()))){
						HX_STACK_LINE(1258)
						this->treeArray->__get(i).StaticCast< ::Tree >()->registerTreeClickedMouseEvent();
					}
				}
			}
			HX_STACK_LINE(1261)
			{
				HX_STACK_LINE(1261)
				int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(1261)
				int _g = this->juiceMachineArray->length;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(1261)
				while((true)){
					HX_STACK_LINE(1261)
					if ((!(((_g1 < _g))))){
						HX_STACK_LINE(1261)
						break;
					}
					HX_STACK_LINE(1261)
					int i = (_g1)++;		HX_STACK_VAR(i,"i");
					HX_STACK_LINE(1262)
					if (((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >() != null()))){
						HX_STACK_LINE(1263)
						this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->registerMachineEvents();
					}
				}
			}
			HX_STACK_LINE(1266)
			{
				HX_STACK_LINE(1266)
				int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(1266)
				int _g = this->pieMachineArray->length;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(1266)
				while((true)){
					HX_STACK_LINE(1266)
					if ((!(((_g1 < _g))))){
						HX_STACK_LINE(1266)
						break;
					}
					HX_STACK_LINE(1266)
					int i = (_g1)++;		HX_STACK_VAR(i,"i");
					HX_STACK_LINE(1267)
					if (((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >() != null()))){
						HX_STACK_LINE(1268)
						this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->registerMachineEvents();
					}
				}
			}
			HX_STACK_LINE(1271)
			this->callBackStopOne = true;
			HX_STACK_LINE(1272)
			this->callBackStopTwo = false;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,removeCallBackOnPad,(void))

Void PlayState_obj::createAsteroidBelt( ){
{
		HX_STACK_FRAME("PlayState","createAsteroidBelt",0x33efcd73,"PlayState.createAsteroidBelt","PlayState.hx",1277,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1277)
		if (((this->asteroidBelt->getAsteroidBeltSprite()->x == (int)0))){
			HX_STACK_LINE(1278)
			::AsteroidBelt _g = ::AsteroidBelt_obj::__new((int)-700);		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(1278)
			this->asteroidBelt = _g;
			HX_STACK_LINE(1279)
			this->belowBottomSprites->add(this->asteroidBelt);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,createAsteroidBelt,(void))

Void PlayState_obj::currentTreeGrowthValue( ){
{
		HX_STACK_FRAME("PlayState","currentTreeGrowthValue",0x789dafa4,"PlayState.currentTreeGrowthValue","PlayState.hx",1285,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1285)
		if (((this->tutorial != null()))){
			struct _Function_2_1{
				inline static bool Block( hx::ObjectPtr< ::PlayState_obj > __this){
					HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",1286,0xb30d7781)
					{
						HX_STACK_LINE(1286)
						Float _g = __this->tutorial->getCurrentTutSectionNumber();		HX_STACK_VAR(_g,"_g");
						HX_STACK_LINE(1286)
						return (_g < (int)2);
					}
					return null();
				}
			};
			HX_STACK_LINE(1286)
			if (((  (((  (((this->treeArray->__get((int)0).StaticCast< ::Tree >() != null()))) ? bool(_Function_2_1::Block(this)) : bool(false) ))) ? bool(this->tutorialOn) : bool(false) ))){
				HX_STACK_LINE(1287)
				int _g1 = this->treeArray->__get((int)0).StaticCast< ::Tree >()->getTreeGrowthPhase();		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(1287)
				this->tutorial->getTutTreeGrowthVal(_g1);
				HX_STACK_LINE(1288)
				int _g2 = this->treeArray->__get((int)0).StaticCast< ::Tree >()->getTreeGrowthPhase();		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(1288)
				if (((  (((_g2 == (int)6))) ? bool(!(this->closeTreeGrowthCheck)) : bool(false) ))){
					HX_STACK_LINE(1289)
					this->treeArray->__get((int)0).StaticCast< ::Tree >()->setTreeSunlightTut((int)10);
					HX_STACK_LINE(1290)
					this->treeArray->__get((int)0).StaticCast< ::Tree >()->setTreeThirstTut((int)15);
					HX_STACK_LINE(1291)
					this->tutorial->destroy();
					HX_STACK_LINE(1292)
					::Tutorial _g3 = ::Tutorial_obj::__new((int)2);		HX_STACK_VAR(_g3,"_g3");
					HX_STACK_LINE(1292)
					this->tutorial = _g3;
					HX_STACK_LINE(1293)
					this->add(this->tutorial);
					HX_STACK_LINE(1294)
					this->closeTreeGrowthCheck = true;
				}
			}
			HX_STACK_LINE(1297)
			if (((bool((bool((this->appleCount == (int)10)) && bool(!(this->closeAppleCountCheck)))) && bool(this->tutorialOn)))){
				HX_STACK_LINE(1298)
				this->tutorial->destroy();
				HX_STACK_LINE(1299)
				::Tutorial _g4 = ::Tutorial_obj::__new((int)3);		HX_STACK_VAR(_g4,"_g4");
				HX_STACK_LINE(1299)
				this->tutorial = _g4;
				HX_STACK_LINE(1300)
				this->add(this->tutorial);
				HX_STACK_LINE(1301)
				this->treeArray->__get((int)0).StaticCast< ::Tree >()->setSunlightValue((int)1);
				HX_STACK_LINE(1302)
				this->treeArray->__get((int)0).StaticCast< ::Tree >()->pauseAlertTimers();
				HX_STACK_LINE(1303)
				this->closeAppleCountCheck = true;
			}
			HX_STACK_LINE(1305)
			if (((this->treeArray->__get((int)0).StaticCast< ::Tree >() != null()))){
				HX_STACK_LINE(1306)
				int _g5 = this->treeArray->__get((int)0).StaticCast< ::Tree >()->getTreeGrowthPhase();		HX_STACK_VAR(_g5,"_g5");
				HX_STACK_LINE(1306)
				if (((  (((  (((_g5 == (int)1))) ? bool(this->tutorialOn) : bool(false) ))) ? bool(!(this->closeSecondTreeGrowthCheck)) : bool(false) ))){
					HX_STACK_LINE(1307)
					this->tutorial->destroy();
					HX_STACK_LINE(1308)
					::Tutorial _g6 = ::Tutorial_obj::__new(1.5);		HX_STACK_VAR(_g6,"_g6");
					HX_STACK_LINE(1308)
					this->tutorial = _g6;
					HX_STACK_LINE(1309)
					this->add(this->tutorial);
					HX_STACK_LINE(1310)
					this->closeSecondTreeGrowthCheck = true;
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,currentTreeGrowthValue,(void))

Void PlayState_obj::marketTutOpened( ){
{
		HX_STACK_FRAME("PlayState","marketTutOpened",0xcca3666f,"PlayState.marketTutOpened","PlayState.hx",1319,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1319)
		if (((bool(this->marketOpenCheck) && bool(this->tutorialOn)))){
			HX_STACK_LINE(1320)
			Float _g = this->tutorial->getCurrentTutSectionNumber();		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(1320)
			if (((_g == (int)4))){
				HX_STACK_LINE(1321)
				this->treeArray->__get((int)0).StaticCast< ::Tree >()->setThirstValue((int)0);
				HX_STACK_LINE(1322)
				this->treeArray->__get((int)0).StaticCast< ::Tree >()->setSunlightValue((int)1);
				HX_STACK_LINE(1323)
				if ((this->market->getMarketOpen())){
					HX_STACK_LINE(1324)
					this->tutorial->destroy();
					HX_STACK_LINE(1325)
					this->marketOpenCheck = false;
					HX_STACK_LINE(1326)
					this->tutorialOn = false;
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,marketTutOpened,(void))

Void PlayState_obj::restartGameOver( ){
{
		HX_STACK_FRAME("PlayState","restartGameOver",0x6d7c2e44,"PlayState.restartGameOver","PlayState.hx",1333,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1333)
		if (((this->menuScreen != null()))){
			HX_STACK_LINE(1334)
			bool _g = this->menuScreen->getGameRestart();		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(1334)
			if (((_g == true))){
				HX_STACK_LINE(1395)
				::flixel::plugin::MouseEventManager_obj::remove(this->seed);
				HX_STACK_LINE(1396)
				::flixel::plugin::MouseEventManager_obj::remove(this->juiceMachineIcon);
				HX_STACK_LINE(1397)
				::flixel::plugin::MouseEventManager_obj::remove(this->pieMachineIcon);
				HX_STACK_LINE(1398)
				::flixel::plugin::MouseEventManager_obj::remove(this->fishIcon);
				HX_STACK_LINE(1399)
				::flixel::plugin::MouseEventManager_obj::remove(this->greenHouseBarIcon);
				HX_STACK_LINE(1400)
				::flixel::FlxSprite _g1 = this->truckButton->getTruckButtonSprite();		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(1400)
				::flixel::plugin::MouseEventManager_obj::remove(_g1);
				HX_STACK_LINE(1401)
				::flixel::plugin::MouseEventManager_obj::remove(this->buildBarTab);
				HX_STACK_LINE(1402)
				::flixel::plugin::MouseEventManager_obj::remove(this->buildBarShaft);
				HX_STACK_LINE(1403)
				::flixel::plugin::MouseEventManager_obj::remove(this->buySign);
				HX_STACK_LINE(1404)
				{
					HX_STACK_LINE(1404)
					int _g11 = (int)0;		HX_STACK_VAR(_g11,"_g11");
					HX_STACK_LINE(1404)
					int _g2 = this->board->length;		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(1404)
					while((true)){
						HX_STACK_LINE(1404)
						if ((!(((_g11 < _g2))))){
							HX_STACK_LINE(1404)
							break;
						}
						HX_STACK_LINE(1404)
						int i = (_g11)++;		HX_STACK_VAR(i,"i");
						HX_STACK_LINE(1405)
						{
							HX_STACK_LINE(1405)
							int _g3 = (int)0;		HX_STACK_VAR(_g3,"_g3");
							HX_STACK_LINE(1405)
							int _g21 = this->board->length;		HX_STACK_VAR(_g21,"_g21");
							HX_STACK_LINE(1405)
							while((true)){
								HX_STACK_LINE(1405)
								if ((!(((_g3 < _g21))))){
									HX_STACK_LINE(1405)
									break;
								}
								HX_STACK_LINE(1405)
								int j = (_g3)++;		HX_STACK_VAR(j,"j");
								HX_STACK_LINE(1406)
								::flixel::plugin::MouseEventManager_obj::remove(this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >());
							}
						}
					}
				}
				HX_STACK_LINE(1409)
				{
					HX_STACK_LINE(1409)
					int _g11 = (int)0;		HX_STACK_VAR(_g11,"_g11");
					HX_STACK_LINE(1409)
					int _g2 = this->treeArray->length;		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(1409)
					while((true)){
						HX_STACK_LINE(1409)
						if ((!(((_g11 < _g2))))){
							HX_STACK_LINE(1409)
							break;
						}
						HX_STACK_LINE(1409)
						int k = (_g11)++;		HX_STACK_VAR(k,"k");
						HX_STACK_LINE(1410)
						if (((this->treeArray->__get(k).StaticCast< ::Tree >() != null()))){
							HX_STACK_LINE(1411)
							::flixel::FlxSprite _g21 = this->treeArray->__get(k).StaticCast< ::Tree >()->getTreeSprite();		HX_STACK_VAR(_g21,"_g21");
							HX_STACK_LINE(1411)
							::flixel::plugin::MouseEventManager_obj::remove(_g21);
							HX_STACK_LINE(1412)
							this->treeArray->__get(k).StaticCast< ::Tree >()->unregisterAlertEvents();
							HX_STACK_LINE(1413)
							this->treeArray->__get(k).StaticCast< ::Tree >()->unregisterTreeClickedMouseEvent();
							HX_STACK_LINE(1414)
							this->treeArray->__get(k).StaticCast< ::Tree >()->unregisterAppleEvent();
						}
					}
				}
				HX_STACK_LINE(1417)
				this->bb->setBoardInUse(false);
				HX_STACK_LINE(1418)
				this->floatingIsland = null();
				HX_STACK_LINE(1419)
				::Greenhouse_obj::greenHouseInPlay = false;
				HX_STACK_LINE(1420)
				{
					HX_STACK_LINE(1420)
					::Class _g3 = ::Type_obj::getClass(::flixel::FlxG_obj::game->_state);		HX_STACK_VAR(_g3,"_g3");
					HX_STACK_LINE(1420)
					::flixel::FlxState State = ::Type_obj::createInstance(_g3,Dynamic( Array_obj<Dynamic>::__new()));		HX_STACK_VAR(State,"State");
					HX_STACK_LINE(1420)
					::flixel::FlxG_obj::game->_requestedState = State;
				}
			}
			HX_STACK_LINE(1422)
			if ((this->menuScreen->getGameEnd())){
				HX_STACK_LINE(1423)
				if (((this->greenHouse != null()))){
					HX_STACK_LINE(1424)
					this->greenHouse->destroy();
					HX_STACK_LINE(1425)
					this->greenHouse = null();
				}
				HX_STACK_LINE(1427)
				if (((this->koiPond != null()))){
					HX_STACK_LINE(1428)
					this->koiPond->destroy();
					HX_STACK_LINE(1429)
					this->koiPond = null();
				}
				HX_STACK_LINE(1431)
				{
					HX_STACK_LINE(1431)
					int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(1431)
					int _g2 = this->treeArray->length;		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(1431)
					while((true)){
						HX_STACK_LINE(1431)
						if ((!(((_g1 < _g2))))){
							HX_STACK_LINE(1431)
							break;
						}
						HX_STACK_LINE(1431)
						int i = (_g1)++;		HX_STACK_VAR(i,"i");
						HX_STACK_LINE(1432)
						if (((this->treeArray->__get(i).StaticCast< ::Tree >() != null()))){
							HX_STACK_LINE(1433)
							this->treeArray->__get(i).StaticCast< ::Tree >()->destroy();
							HX_STACK_LINE(1434)
							this->treeArray[i] = null();
						}
					}
				}
				HX_STACK_LINE(1437)
				{
					HX_STACK_LINE(1437)
					int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(1437)
					int _g2 = this->board->length;		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(1437)
					while((true)){
						HX_STACK_LINE(1437)
						if ((!(((_g1 < _g2))))){
							HX_STACK_LINE(1437)
							break;
						}
						HX_STACK_LINE(1437)
						int i = (_g1)++;		HX_STACK_VAR(i,"i");
						HX_STACK_LINE(1438)
						{
							HX_STACK_LINE(1438)
							int _g3 = (int)0;		HX_STACK_VAR(_g3,"_g3");
							HX_STACK_LINE(1438)
							int _g21 = this->board->length;		HX_STACK_VAR(_g21,"_g21");
							HX_STACK_LINE(1438)
							while((true)){
								HX_STACK_LINE(1438)
								if ((!(((_g3 < _g21))))){
									HX_STACK_LINE(1438)
									break;
								}
								HX_STACK_LINE(1438)
								int j = (_g3)++;		HX_STACK_VAR(j,"j");
								HX_STACK_LINE(1439)
								this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >()->setAsUnowned();
								HX_STACK_LINE(1440)
								this->board->__get(i).StaticCast< Array< ::Dynamic > >()->__get(j).StaticCast< ::Tile >()->setHasObject(false);
							}
						}
					}
				}
				HX_STACK_LINE(1443)
				{
					HX_STACK_LINE(1443)
					int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(1443)
					int _g2 = this->juiceMachineArray->length;		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(1443)
					while((true)){
						HX_STACK_LINE(1443)
						if ((!(((_g1 < _g2))))){
							HX_STACK_LINE(1443)
							break;
						}
						HX_STACK_LINE(1443)
						int i = (_g1)++;		HX_STACK_VAR(i,"i");
						HX_STACK_LINE(1444)
						if (((this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >() != null()))){
							HX_STACK_LINE(1445)
							this->juiceMachineArray->__get(i).StaticCast< ::JuiceMachine >()->destroy();
							HX_STACK_LINE(1446)
							this->juiceMachineArray[i] = null();
						}
					}
				}
				HX_STACK_LINE(1449)
				{
					HX_STACK_LINE(1449)
					int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(1449)
					int _g2 = this->pieMachineArray->length;		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(1449)
					while((true)){
						HX_STACK_LINE(1449)
						if ((!(((_g1 < _g2))))){
							HX_STACK_LINE(1449)
							break;
						}
						HX_STACK_LINE(1449)
						int i = (_g1)++;		HX_STACK_VAR(i,"i");
						HX_STACK_LINE(1450)
						if (((this->pieMachineArray->__get(i).StaticCast< ::PieMachine >() != null()))){
							HX_STACK_LINE(1451)
							this->pieMachineArray->__get(i).StaticCast< ::PieMachine >()->destroy();
							HX_STACK_LINE(1452)
							this->pieMachineArray[i] = null();
						}
					}
				}
				HX_STACK_LINE(1456)
				this->tileBoard->destroyAllTiles();
				HX_STACK_LINE(1457)
				::openfl::_v2::system::System_obj::exit((int)0);
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,restartGameOver,(void))

Void PlayState_obj::destroySound( ){
{
		HX_STACK_FRAME("PlayState","destroySound",0x8e01f926,"PlayState.destroySound","PlayState.hx",1462,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1463)
		if (((bool((this->coinsSound != null())) && bool(!(((this->coinsSound->_channel != null()))))))){
			HX_STACK_LINE(1464)
			::flixel::system::FlxSound _g = ::flixel::util::FlxDestroyUtil_obj::destroy(this->coinsSound);		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(1464)
			this->coinsSound = _g;
			HX_STACK_LINE(1465)
			this->coinsSound = null();
		}
		HX_STACK_LINE(1468)
		if (((bool((this->alertButtonSound != null())) && bool(!(((this->alertButtonSound->_channel != null()))))))){
			HX_STACK_LINE(1469)
			::flixel::system::FlxSound _g1 = ::flixel::util::FlxDestroyUtil_obj::destroy(this->alertButtonSound);		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(1469)
			this->alertButtonSound = _g1;
			HX_STACK_LINE(1470)
			this->alertButtonSound = null();
		}
		HX_STACK_LINE(1473)
		if (((bool((this->correctAnswerSound != null())) && bool(!(((this->correctAnswerSound->_channel != null()))))))){
			HX_STACK_LINE(1474)
			::flixel::system::FlxSound _g2 = ::flixel::util::FlxDestroyUtil_obj::destroy(this->correctAnswerSound);		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(1474)
			this->correctAnswerSound = _g2;
			HX_STACK_LINE(1475)
			this->correctAnswerSound = null();
		}
		HX_STACK_LINE(1478)
		if (((bool((this->incorrectAnswerSound != null())) && bool(!(((this->incorrectAnswerSound->_channel != null()))))))){
			HX_STACK_LINE(1479)
			::flixel::system::FlxSound _g3 = ::flixel::util::FlxDestroyUtil_obj::destroy(this->incorrectAnswerSound);		HX_STACK_VAR(_g3,"_g3");
			HX_STACK_LINE(1479)
			this->incorrectAnswerSound = _g3;
			HX_STACK_LINE(1480)
			this->incorrectAnswerSound = null();
		}
		HX_STACK_LINE(1483)
		if (((bool((this->plantingSeedSound != null())) && bool(!(((this->plantingSeedSound->_channel != null()))))))){
			HX_STACK_LINE(1484)
			::flixel::system::FlxSound _g4 = ::flixel::util::FlxDestroyUtil_obj::destroy(this->plantingSeedSound);		HX_STACK_VAR(_g4,"_g4");
			HX_STACK_LINE(1484)
			this->plantingSeedSound = _g4;
			HX_STACK_LINE(1485)
			this->plantingSeedSound = null();
		}
		HX_STACK_LINE(1488)
		if (((bool((this->placedMachineSound != null())) && bool(!(((this->placedMachineSound->_channel != null()))))))){
			HX_STACK_LINE(1489)
			::flixel::system::FlxSound _g5 = ::flixel::util::FlxDestroyUtil_obj::destroy(this->placedMachineSound);		HX_STACK_VAR(_g5,"_g5");
			HX_STACK_LINE(1489)
			this->placedMachineSound = _g5;
			HX_STACK_LINE(1490)
			this->placedMachineSound = null();
		}
		HX_STACK_LINE(1493)
		if (((bool((this->buyLandSound != null())) && bool(!(((this->buyLandSound->_channel != null()))))))){
			HX_STACK_LINE(1494)
			::flixel::system::FlxSound _g6 = ::flixel::util::FlxDestroyUtil_obj::destroy(this->buyLandSound);		HX_STACK_VAR(_g6,"_g6");
			HX_STACK_LINE(1494)
			this->buyLandSound = _g6;
			HX_STACK_LINE(1495)
			this->buyLandSound = null();
		}
		HX_STACK_LINE(1498)
		if (((bool((this->buySignSound != null())) && bool(!(((this->buySignSound->_channel != null()))))))){
			HX_STACK_LINE(1499)
			::flixel::system::FlxSound _g7 = ::flixel::util::FlxDestroyUtil_obj::destroy(this->buySignSound);		HX_STACK_VAR(_g7,"_g7");
			HX_STACK_LINE(1499)
			this->buySignSound = _g7;
			HX_STACK_LINE(1500)
			this->buySignSound = null();
		}
		HX_STACK_LINE(1503)
		if (((bool((this->truckButtonSound != null())) && bool(!(((this->truckButtonSound->_channel != null()))))))){
			HX_STACK_LINE(1504)
			::flixel::system::FlxSound _g8 = ::flixel::util::FlxDestroyUtil_obj::destroy(this->truckButtonSound);		HX_STACK_VAR(_g8,"_g8");
			HX_STACK_LINE(1504)
			this->truckButtonSound = _g8;
			HX_STACK_LINE(1505)
			this->truckButtonSound = null();
		}
		HX_STACK_LINE(1508)
		if (((bool((this->koiPondPlacedSound != null())) && bool(!(((this->koiPondPlacedSound->_channel != null()))))))){
			HX_STACK_LINE(1509)
			::flixel::system::FlxSound _g9 = ::flixel::util::FlxDestroyUtil_obj::destroy(this->koiPondPlacedSound);		HX_STACK_VAR(_g9,"_g9");
			HX_STACK_LINE(1509)
			this->koiPondPlacedSound = _g9;
			HX_STACK_LINE(1510)
			this->koiPondPlacedSound = null();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,destroySound,(void))

Void PlayState_obj::buildBarOpen( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("PlayState","buildBarOpen",0xb99e6960,"PlayState.buildBarOpen","PlayState.hx",1516,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(1516)
		if ((!(this->buildBarIsOpen))){
			HX_STACK_LINE(1517)
			this->buildBarIsOpen = true;
		}
		else{
			HX_STACK_LINE(1520)
			this->buildBarIsOpen = false;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(PlayState_obj,buildBarOpen,(void))

Void PlayState_obj::buildBarActions( ){
{
		HX_STACK_FRAME("PlayState","buildBarActions",0x6b36c027,"PlayState.buildBarActions","PlayState.hx",1524,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1525)
		if ((this->buildBarIsOpen)){
			HX_STACK_LINE(1526)
			hx::SubEq(this->buildBarOpenTimer,::flixel::FlxG_obj::elapsed);
			HX_STACK_LINE(1527)
			if (((this->buildBarShaft->x < (int)-21))){
				HX_STACK_LINE(1528)
				{
					HX_STACK_LINE(1528)
					::flixel::FlxSprite _g = this->buildBarShaft;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(1528)
					_g->set_x((_g->x + this->buildBarOpenSpeed));
				}
				HX_STACK_LINE(1529)
				{
					HX_STACK_LINE(1529)
					::flixel::FlxSprite _g = this->buildBarTab;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(1529)
					_g->set_x((_g->x + this->buildBarOpenSpeed));
				}
				HX_STACK_LINE(1530)
				{
					HX_STACK_LINE(1530)
					::Seed _g = this->seed;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(1530)
					_g->set_x((_g->x + this->buildBarOpenSpeed));
				}
				HX_STACK_LINE(1531)
				{
					HX_STACK_LINE(1531)
					::Seed _g = this->seedDrag;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(1531)
					_g->set_x((_g->x + this->buildBarOpenSpeed));
				}
				HX_STACK_LINE(1532)
				{
					HX_STACK_LINE(1532)
					::JuiceIcon _g = this->juiceMachineIcon;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(1532)
					_g->set_x((_g->x + this->buildBarOpenSpeed));
				}
				HX_STACK_LINE(1533)
				{
					HX_STACK_LINE(1533)
					::JuiceIcon _g = this->juiceMachineIconDrag;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(1533)
					_g->set_x((_g->x + this->buildBarOpenSpeed));
				}
				HX_STACK_LINE(1534)
				{
					HX_STACK_LINE(1534)
					::PieIcon _g = this->pieMachineIcon;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(1534)
					_g->set_x((_g->x + this->buildBarOpenSpeed));
				}
				HX_STACK_LINE(1535)
				{
					HX_STACK_LINE(1535)
					::PieIcon _g = this->pieMachineIconDrag;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(1535)
					_g->set_x((_g->x + this->buildBarOpenSpeed));
				}
				HX_STACK_LINE(1536)
				{
					HX_STACK_LINE(1536)
					::FishIcon _g = this->fishIcon;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(1536)
					_g->set_x((_g->x + this->buildBarOpenSpeed));
				}
				HX_STACK_LINE(1537)
				{
					HX_STACK_LINE(1537)
					::FishIcon _g = this->fishIconDrag;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(1537)
					_g->set_x((_g->x + this->buildBarOpenSpeed));
				}
				HX_STACK_LINE(1538)
				{
					HX_STACK_LINE(1538)
					::GreenHouseIcon _g = this->greenHouseBarIcon;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(1538)
					_g->set_x((_g->x + this->buildBarOpenSpeed));
				}
				HX_STACK_LINE(1539)
				{
					HX_STACK_LINE(1539)
					::GreenHouseIcon _g = this->greenHouseBarIconDrag;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(1539)
					_g->set_x((_g->x + this->buildBarOpenSpeed));
				}
				HX_STACK_LINE(1540)
				this->market->setCountTextXOpen(this->buildBarOpenSpeed);
			}
			HX_STACK_LINE(1542)
			if (((bool((this->buildBarOpenTimer <= (int)0)) && bool((this->buildBarOpenTimer > (int)-1))))){
				HX_STACK_LINE(1543)
				this->buildBarIsOpen = false;
			}
		}
		else{
			HX_STACK_LINE(1547)
			this->buildBarOpenTimer = (int)15;
			HX_STACK_LINE(1548)
			if (((this->buildBarShaft->x > (int)-185))){
				HX_STACK_LINE(1549)
				{
					HX_STACK_LINE(1549)
					::flixel::FlxSprite _g = this->buildBarShaft;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(1549)
					_g->set_x((_g->x - this->buildBarOpenSpeed));
				}
				HX_STACK_LINE(1550)
				{
					HX_STACK_LINE(1550)
					::flixel::FlxSprite _g = this->buildBarTab;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(1550)
					_g->set_x((_g->x - this->buildBarOpenSpeed));
				}
				HX_STACK_LINE(1551)
				{
					HX_STACK_LINE(1551)
					::Seed _g = this->seed;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(1551)
					_g->set_x((_g->x - this->buildBarOpenSpeed));
				}
				HX_STACK_LINE(1552)
				{
					HX_STACK_LINE(1552)
					::Seed _g = this->seedDrag;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(1552)
					_g->set_x((_g->x - this->buildBarOpenSpeed));
				}
				HX_STACK_LINE(1553)
				{
					HX_STACK_LINE(1553)
					::JuiceIcon _g = this->juiceMachineIcon;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(1553)
					_g->set_x((_g->x - this->buildBarOpenSpeed));
				}
				HX_STACK_LINE(1554)
				{
					HX_STACK_LINE(1554)
					::JuiceIcon _g = this->juiceMachineIconDrag;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(1554)
					_g->set_x((_g->x - this->buildBarOpenSpeed));
				}
				HX_STACK_LINE(1555)
				{
					HX_STACK_LINE(1555)
					::PieIcon _g = this->pieMachineIcon;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(1555)
					_g->set_x((_g->x - this->buildBarOpenSpeed));
				}
				HX_STACK_LINE(1556)
				{
					HX_STACK_LINE(1556)
					::PieIcon _g = this->pieMachineIconDrag;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(1556)
					_g->set_x((_g->x - this->buildBarOpenSpeed));
				}
				HX_STACK_LINE(1557)
				{
					HX_STACK_LINE(1557)
					::FishIcon _g = this->fishIcon;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(1557)
					_g->set_x((_g->x - this->buildBarOpenSpeed));
				}
				HX_STACK_LINE(1558)
				{
					HX_STACK_LINE(1558)
					::FishIcon _g = this->fishIconDrag;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(1558)
					_g->set_x((_g->x - this->buildBarOpenSpeed));
				}
				HX_STACK_LINE(1559)
				{
					HX_STACK_LINE(1559)
					::GreenHouseIcon _g = this->greenHouseBarIcon;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(1559)
					_g->set_x((_g->x - this->buildBarOpenSpeed));
				}
				HX_STACK_LINE(1560)
				{
					HX_STACK_LINE(1560)
					::GreenHouseIcon _g = this->greenHouseBarIconDrag;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(1560)
					_g->set_x((_g->x - this->buildBarOpenSpeed));
				}
				HX_STACK_LINE(1561)
				this->market->setCountTextXClose(this->buildBarOpenSpeed);
			}
		}
		HX_STACK_LINE(1564)
		if (((this->buildBarShaft->x >= (int)-21))){
			HX_STACK_LINE(1565)
			this->seedDrag->revive();
			HX_STACK_LINE(1566)
			this->juiceMachineIconDrag->revive();
			HX_STACK_LINE(1567)
			this->pieMachineIconDrag->revive();
			HX_STACK_LINE(1568)
			this->fishIconDrag->revive();
			HX_STACK_LINE(1569)
			this->greenHouseBarIconDrag->revive();
		}
		else{
			HX_STACK_LINE(1572)
			this->seedDrag->kill();
			HX_STACK_LINE(1573)
			this->juiceMachineIconDrag->kill();
			HX_STACK_LINE(1574)
			this->pieMachineIconDrag->kill();
			HX_STACK_LINE(1575)
			this->fishIconDrag->kill();
			HX_STACK_LINE(1576)
			this->greenHouseBarIconDrag->kill();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,buildBarActions,(void))

Void PlayState_obj::update( ){
{
		HX_STACK_FRAME("PlayState","update",0x8d182efa,"PlayState.update","PlayState.hx",1585,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1586)
		this->removeCallBackOnPad();
		HX_STACK_LINE(1587)
		this->factoryElectricBill();
		HX_STACK_LINE(1588)
		this->waterSprinklerBill();
		HX_STACK_LINE(1589)
		this->applesForJuice();
		HX_STACK_LINE(1590)
		this->applesAndJuiceForPie();
		HX_STACK_LINE(1591)
		this->turnWaterOff();
		HX_STACK_LINE(1592)
		this->destroyObjects();
		HX_STACK_LINE(1593)
		this->checkForAlertClick();
		HX_STACK_LINE(1594)
		this->submitAnswer();
		HX_STACK_LINE(1595)
		this->stopSeedDragIfBoard();
		HX_STACK_LINE(1596)
		this->stopJuiceDragIfBoard();
		HX_STACK_LINE(1597)
		this->stopPieDragIfBoard();
		HX_STACK_LINE(1598)
		this->stopFishDragIfBoard();
		HX_STACK_LINE(1599)
		this->stopGreenHouseDragIfBoard();
		HX_STACK_LINE(1600)
		this->tileBoarderFlip();
		HX_STACK_LINE(1601)
		this->displayAppleCount();
		HX_STACK_LINE(1602)
		this->displayPadNumbers();
		HX_STACK_LINE(1603)
		this->truckLoadingGoods();
		HX_STACK_LINE(1604)
		this->createFloatingIsland();
		HX_STACK_LINE(1605)
		this->createAsteroidBelt();
		HX_STACK_LINE(1606)
		this->currentTreeGrowthValue();
		HX_STACK_LINE(1607)
		this->marketTutOpened();
		HX_STACK_LINE(1608)
		this->restartGameOver();
		HX_STACK_LINE(1609)
		this->destroySound();
		HX_STACK_LINE(1610)
		this->buildBarActions();
		HX_STACK_LINE(1611)
		this->super::update();
	}
return null();
}


Void PlayState_obj::destroy( ){
{
		HX_STACK_FRAME("PlayState","destroy",0x6ec756e9,"PlayState.destroy","PlayState.hx",1620,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(1620)
		this->super::destroy();
	}
return null();
}



PlayState_obj::PlayState_obj()
{
}

void PlayState_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(PlayState);
	HX_MARK_MEMBER_NAME(menuButton,"menuButton");
	HX_MARK_MEMBER_NAME(menuScreen,"menuScreen");
	HX_MARK_MEMBER_NAME(tutorial,"tutorial");
	HX_MARK_MEMBER_NAME(weed,"weed");
	HX_MARK_MEMBER_NAME(systemStar,"systemStar");
	HX_MARK_MEMBER_NAME(backgroundSprite,"backgroundSprite");
	HX_MARK_MEMBER_NAME(buildBar,"buildBar");
	HX_MARK_MEMBER_NAME(buildBarTab,"buildBarTab");
	HX_MARK_MEMBER_NAME(buildBarShaft,"buildBarShaft");
	HX_MARK_MEMBER_NAME(aboveGrassLayer,"aboveGrassLayer");
	HX_MARK_MEMBER_NAME(plantLayer,"plantLayer");
	HX_MARK_MEMBER_NAME(machineLayer,"machineLayer");
	HX_MARK_MEMBER_NAME(belowBottomSprites,"belowBottomSprites");
	HX_MARK_MEMBER_NAME(bottomeSprites,"bottomeSprites");
	HX_MARK_MEMBER_NAME(floatingIsland,"floatingIsland");
	HX_MARK_MEMBER_NAME(tileBoard,"tileBoard");
	HX_MARK_MEMBER_NAME(lastTileClicked,"lastTileClicked");
	HX_MARK_MEMBER_NAME(tileBoardBorder,"tileBoardBorder");
	HX_MARK_MEMBER_NAME(board,"board");
	HX_MARK_MEMBER_NAME(boarder,"boarder");
	HX_MARK_MEMBER_NAME(treeArray,"treeArray");
	HX_MARK_MEMBER_NAME(juiceMachineArray,"juiceMachineArray");
	HX_MARK_MEMBER_NAME(pieMachineArray,"pieMachineArray");
	HX_MARK_MEMBER_NAME(flyingJuiceBottleArray,"flyingJuiceBottleArray");
	HX_MARK_MEMBER_NAME(numPad,"numPad");
	HX_MARK_MEMBER_NAME(bb,"bb");
	HX_MARK_MEMBER_NAME(menuVisible,"menuVisible");
	HX_MARK_MEMBER_NAME(appleCount,"appleCount");
	HX_MARK_MEMBER_NAME(buySignCost,"buySignCost");
	HX_MARK_MEMBER_NAME(applePerTenCost,"applePerTenCost");
	HX_MARK_MEMBER_NAME(juicePerTenCost,"juicePerTenCost");
	HX_MARK_MEMBER_NAME(piePerTenCost,"piePerTenCost");
	HX_MARK_MEMBER_NAME(currentTruckLvl,"currentTruckLvl");
	HX_MARK_MEMBER_NAME(randomIslandCreationTimer,"randomIslandCreationTimer");
	HX_MARK_MEMBER_NAME(buildBarOpenTimer,"buildBarOpenTimer");
	HX_MARK_MEMBER_NAME(loadAppleTimer,"loadAppleTimer");
	HX_MARK_MEMBER_NAME(leaveTruckTimer,"leaveTruckTimer");
	HX_MARK_MEMBER_NAME(appleForJuiceTimer,"appleForJuiceTimer");
	HX_MARK_MEMBER_NAME(appleAndJuiceForPieTimer,"appleAndJuiceForPieTimer");
	HX_MARK_MEMBER_NAME(buildBarOpenSpeed,"buildBarOpenSpeed");
	HX_MARK_MEMBER_NAME(islandCreationTimer,"islandCreationTimer");
	HX_MARK_MEMBER_NAME(appleCountString,"appleCountString");
	HX_MARK_MEMBER_NAME(answerString,"answerString");
	HX_MARK_MEMBER_NAME(boardIsDown,"boardIsDown");
	HX_MARK_MEMBER_NAME(appleDisplayLock,"appleDisplayLock");
	HX_MARK_MEMBER_NAME(asteroidBelt,"asteroidBelt");
	HX_MARK_MEMBER_NAME(greenHouse,"greenHouse");
	HX_MARK_MEMBER_NAME(bridge,"bridge");
	HX_MARK_MEMBER_NAME(buySign,"buySign");
	HX_MARK_MEMBER_NAME(foreGroundRocks,"foreGroundRocks");
	HX_MARK_MEMBER_NAME(answerFieldText,"answerFieldText");
	HX_MARK_MEMBER_NAME(appleCountFieldText,"appleCountFieldText");
	HX_MARK_MEMBER_NAME(buySignCostFieldText,"buySignCostFieldText");
	HX_MARK_MEMBER_NAME(market,"market");
	HX_MARK_MEMBER_NAME(truckButton,"truckButton");
	HX_MARK_MEMBER_NAME(juiceMachine,"juiceMachine");
	HX_MARK_MEMBER_NAME(pieMachine,"pieMachine");
	HX_MARK_MEMBER_NAME(appleShed,"appleShed");
	HX_MARK_MEMBER_NAME(appleTruck,"appleTruck");
	HX_MARK_MEMBER_NAME(tree,"tree");
	HX_MARK_MEMBER_NAME(basket,"basket");
	HX_MARK_MEMBER_NAME(currentTargetTree,"currentTargetTree");
	HX_MARK_MEMBER_NAME(currentTargetJuiceMachine,"currentTargetJuiceMachine");
	HX_MARK_MEMBER_NAME(currentTargetPieMachine,"currentTargetPieMachine");
	HX_MARK_MEMBER_NAME(juiceMachineIcon,"juiceMachineIcon");
	HX_MARK_MEMBER_NAME(pieMachineIcon,"pieMachineIcon");
	HX_MARK_MEMBER_NAME(juiceMachineIconDrag,"juiceMachineIconDrag");
	HX_MARK_MEMBER_NAME(pieMachineIconDrag,"pieMachineIconDrag");
	HX_MARK_MEMBER_NAME(fishIcon,"fishIcon");
	HX_MARK_MEMBER_NAME(fishIconDrag,"fishIconDrag");
	HX_MARK_MEMBER_NAME(juicePackIcon,"juicePackIcon");
	HX_MARK_MEMBER_NAME(piePackIcon,"piePackIcon");
	HX_MARK_MEMBER_NAME(seed,"seed");
	HX_MARK_MEMBER_NAME(seedDrag,"seedDrag");
	HX_MARK_MEMBER_NAME(greenHouseBarIcon,"greenHouseBarIcon");
	HX_MARK_MEMBER_NAME(greenHouseBarIconDrag,"greenHouseBarIconDrag");
	HX_MARK_MEMBER_NAME(cloud,"cloud");
	HX_MARK_MEMBER_NAME(sun,"sun");
	HX_MARK_MEMBER_NAME(currentTileBorder,"currentTileBorder");
	HX_MARK_MEMBER_NAME(overlaysInUse,"overlaysInUse");
	HX_MARK_MEMBER_NAME(sunClicked,"sunClicked");
	HX_MARK_MEMBER_NAME(dropletClicked,"dropletClicked");
	HX_MARK_MEMBER_NAME(ownedTile,"ownedTile");
	HX_MARK_MEMBER_NAME(closeTreeGrowthCheck,"closeTreeGrowthCheck");
	HX_MARK_MEMBER_NAME(closeAppleCountCheck,"closeAppleCountCheck");
	HX_MARK_MEMBER_NAME(marketOpenCheck,"marketOpenCheck");
	HX_MARK_MEMBER_NAME(tutorialOn,"tutorialOn");
	HX_MARK_MEMBER_NAME(buildBarIsOpen,"buildBarIsOpen");
	HX_MARK_MEMBER_NAME(truckTutSection,"truckTutSection");
	HX_MARK_MEMBER_NAME(callBackStopOne,"callBackStopOne");
	HX_MARK_MEMBER_NAME(callBackStopTwo,"callBackStopTwo");
	HX_MARK_MEMBER_NAME(tutorialWrongAnswersSkip,"tutorialWrongAnswersSkip");
	HX_MARK_MEMBER_NAME(closeSecondTreeGrowthCheck,"closeSecondTreeGrowthCheck");
	HX_MARK_MEMBER_NAME(tutUnpauseAlertsDone,"tutUnpauseAlertsDone");
	HX_MARK_MEMBER_NAME(tutBuildBarBack,"tutBuildBarBack");
	HX_MARK_MEMBER_NAME(coinsSound,"coinsSound");
	HX_MARK_MEMBER_NAME(alertButtonSound,"alertButtonSound");
	HX_MARK_MEMBER_NAME(correctAnswerSound,"correctAnswerSound");
	HX_MARK_MEMBER_NAME(incorrectAnswerSound,"incorrectAnswerSound");
	HX_MARK_MEMBER_NAME(plantingSeedSound,"plantingSeedSound");
	HX_MARK_MEMBER_NAME(buySignSound,"buySignSound");
	HX_MARK_MEMBER_NAME(buyLandSound,"buyLandSound");
	HX_MARK_MEMBER_NAME(placedMachineSound,"placedMachineSound");
	HX_MARK_MEMBER_NAME(truckButtonSound,"truckButtonSound");
	HX_MARK_MEMBER_NAME(koiPondPlacedSound,"koiPondPlacedSound");
	HX_MARK_MEMBER_NAME(mathless,"mathless");
	HX_MARK_MEMBER_NAME(koiPond,"koiPond");
	::flixel::FlxState_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void PlayState_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(menuButton,"menuButton");
	HX_VISIT_MEMBER_NAME(menuScreen,"menuScreen");
	HX_VISIT_MEMBER_NAME(tutorial,"tutorial");
	HX_VISIT_MEMBER_NAME(weed,"weed");
	HX_VISIT_MEMBER_NAME(systemStar,"systemStar");
	HX_VISIT_MEMBER_NAME(backgroundSprite,"backgroundSprite");
	HX_VISIT_MEMBER_NAME(buildBar,"buildBar");
	HX_VISIT_MEMBER_NAME(buildBarTab,"buildBarTab");
	HX_VISIT_MEMBER_NAME(buildBarShaft,"buildBarShaft");
	HX_VISIT_MEMBER_NAME(aboveGrassLayer,"aboveGrassLayer");
	HX_VISIT_MEMBER_NAME(plantLayer,"plantLayer");
	HX_VISIT_MEMBER_NAME(machineLayer,"machineLayer");
	HX_VISIT_MEMBER_NAME(belowBottomSprites,"belowBottomSprites");
	HX_VISIT_MEMBER_NAME(bottomeSprites,"bottomeSprites");
	HX_VISIT_MEMBER_NAME(floatingIsland,"floatingIsland");
	HX_VISIT_MEMBER_NAME(tileBoard,"tileBoard");
	HX_VISIT_MEMBER_NAME(lastTileClicked,"lastTileClicked");
	HX_VISIT_MEMBER_NAME(tileBoardBorder,"tileBoardBorder");
	HX_VISIT_MEMBER_NAME(board,"board");
	HX_VISIT_MEMBER_NAME(boarder,"boarder");
	HX_VISIT_MEMBER_NAME(treeArray,"treeArray");
	HX_VISIT_MEMBER_NAME(juiceMachineArray,"juiceMachineArray");
	HX_VISIT_MEMBER_NAME(pieMachineArray,"pieMachineArray");
	HX_VISIT_MEMBER_NAME(flyingJuiceBottleArray,"flyingJuiceBottleArray");
	HX_VISIT_MEMBER_NAME(numPad,"numPad");
	HX_VISIT_MEMBER_NAME(bb,"bb");
	HX_VISIT_MEMBER_NAME(menuVisible,"menuVisible");
	HX_VISIT_MEMBER_NAME(appleCount,"appleCount");
	HX_VISIT_MEMBER_NAME(buySignCost,"buySignCost");
	HX_VISIT_MEMBER_NAME(applePerTenCost,"applePerTenCost");
	HX_VISIT_MEMBER_NAME(juicePerTenCost,"juicePerTenCost");
	HX_VISIT_MEMBER_NAME(piePerTenCost,"piePerTenCost");
	HX_VISIT_MEMBER_NAME(currentTruckLvl,"currentTruckLvl");
	HX_VISIT_MEMBER_NAME(randomIslandCreationTimer,"randomIslandCreationTimer");
	HX_VISIT_MEMBER_NAME(buildBarOpenTimer,"buildBarOpenTimer");
	HX_VISIT_MEMBER_NAME(loadAppleTimer,"loadAppleTimer");
	HX_VISIT_MEMBER_NAME(leaveTruckTimer,"leaveTruckTimer");
	HX_VISIT_MEMBER_NAME(appleForJuiceTimer,"appleForJuiceTimer");
	HX_VISIT_MEMBER_NAME(appleAndJuiceForPieTimer,"appleAndJuiceForPieTimer");
	HX_VISIT_MEMBER_NAME(buildBarOpenSpeed,"buildBarOpenSpeed");
	HX_VISIT_MEMBER_NAME(islandCreationTimer,"islandCreationTimer");
	HX_VISIT_MEMBER_NAME(appleCountString,"appleCountString");
	HX_VISIT_MEMBER_NAME(answerString,"answerString");
	HX_VISIT_MEMBER_NAME(boardIsDown,"boardIsDown");
	HX_VISIT_MEMBER_NAME(appleDisplayLock,"appleDisplayLock");
	HX_VISIT_MEMBER_NAME(asteroidBelt,"asteroidBelt");
	HX_VISIT_MEMBER_NAME(greenHouse,"greenHouse");
	HX_VISIT_MEMBER_NAME(bridge,"bridge");
	HX_VISIT_MEMBER_NAME(buySign,"buySign");
	HX_VISIT_MEMBER_NAME(foreGroundRocks,"foreGroundRocks");
	HX_VISIT_MEMBER_NAME(answerFieldText,"answerFieldText");
	HX_VISIT_MEMBER_NAME(appleCountFieldText,"appleCountFieldText");
	HX_VISIT_MEMBER_NAME(buySignCostFieldText,"buySignCostFieldText");
	HX_VISIT_MEMBER_NAME(market,"market");
	HX_VISIT_MEMBER_NAME(truckButton,"truckButton");
	HX_VISIT_MEMBER_NAME(juiceMachine,"juiceMachine");
	HX_VISIT_MEMBER_NAME(pieMachine,"pieMachine");
	HX_VISIT_MEMBER_NAME(appleShed,"appleShed");
	HX_VISIT_MEMBER_NAME(appleTruck,"appleTruck");
	HX_VISIT_MEMBER_NAME(tree,"tree");
	HX_VISIT_MEMBER_NAME(basket,"basket");
	HX_VISIT_MEMBER_NAME(currentTargetTree,"currentTargetTree");
	HX_VISIT_MEMBER_NAME(currentTargetJuiceMachine,"currentTargetJuiceMachine");
	HX_VISIT_MEMBER_NAME(currentTargetPieMachine,"currentTargetPieMachine");
	HX_VISIT_MEMBER_NAME(juiceMachineIcon,"juiceMachineIcon");
	HX_VISIT_MEMBER_NAME(pieMachineIcon,"pieMachineIcon");
	HX_VISIT_MEMBER_NAME(juiceMachineIconDrag,"juiceMachineIconDrag");
	HX_VISIT_MEMBER_NAME(pieMachineIconDrag,"pieMachineIconDrag");
	HX_VISIT_MEMBER_NAME(fishIcon,"fishIcon");
	HX_VISIT_MEMBER_NAME(fishIconDrag,"fishIconDrag");
	HX_VISIT_MEMBER_NAME(juicePackIcon,"juicePackIcon");
	HX_VISIT_MEMBER_NAME(piePackIcon,"piePackIcon");
	HX_VISIT_MEMBER_NAME(seed,"seed");
	HX_VISIT_MEMBER_NAME(seedDrag,"seedDrag");
	HX_VISIT_MEMBER_NAME(greenHouseBarIcon,"greenHouseBarIcon");
	HX_VISIT_MEMBER_NAME(greenHouseBarIconDrag,"greenHouseBarIconDrag");
	HX_VISIT_MEMBER_NAME(cloud,"cloud");
	HX_VISIT_MEMBER_NAME(sun,"sun");
	HX_VISIT_MEMBER_NAME(currentTileBorder,"currentTileBorder");
	HX_VISIT_MEMBER_NAME(overlaysInUse,"overlaysInUse");
	HX_VISIT_MEMBER_NAME(sunClicked,"sunClicked");
	HX_VISIT_MEMBER_NAME(dropletClicked,"dropletClicked");
	HX_VISIT_MEMBER_NAME(ownedTile,"ownedTile");
	HX_VISIT_MEMBER_NAME(closeTreeGrowthCheck,"closeTreeGrowthCheck");
	HX_VISIT_MEMBER_NAME(closeAppleCountCheck,"closeAppleCountCheck");
	HX_VISIT_MEMBER_NAME(marketOpenCheck,"marketOpenCheck");
	HX_VISIT_MEMBER_NAME(tutorialOn,"tutorialOn");
	HX_VISIT_MEMBER_NAME(buildBarIsOpen,"buildBarIsOpen");
	HX_VISIT_MEMBER_NAME(truckTutSection,"truckTutSection");
	HX_VISIT_MEMBER_NAME(callBackStopOne,"callBackStopOne");
	HX_VISIT_MEMBER_NAME(callBackStopTwo,"callBackStopTwo");
	HX_VISIT_MEMBER_NAME(tutorialWrongAnswersSkip,"tutorialWrongAnswersSkip");
	HX_VISIT_MEMBER_NAME(closeSecondTreeGrowthCheck,"closeSecondTreeGrowthCheck");
	HX_VISIT_MEMBER_NAME(tutUnpauseAlertsDone,"tutUnpauseAlertsDone");
	HX_VISIT_MEMBER_NAME(tutBuildBarBack,"tutBuildBarBack");
	HX_VISIT_MEMBER_NAME(coinsSound,"coinsSound");
	HX_VISIT_MEMBER_NAME(alertButtonSound,"alertButtonSound");
	HX_VISIT_MEMBER_NAME(correctAnswerSound,"correctAnswerSound");
	HX_VISIT_MEMBER_NAME(incorrectAnswerSound,"incorrectAnswerSound");
	HX_VISIT_MEMBER_NAME(plantingSeedSound,"plantingSeedSound");
	HX_VISIT_MEMBER_NAME(buySignSound,"buySignSound");
	HX_VISIT_MEMBER_NAME(buyLandSound,"buyLandSound");
	HX_VISIT_MEMBER_NAME(placedMachineSound,"placedMachineSound");
	HX_VISIT_MEMBER_NAME(truckButtonSound,"truckButtonSound");
	HX_VISIT_MEMBER_NAME(koiPondPlacedSound,"koiPondPlacedSound");
	HX_VISIT_MEMBER_NAME(mathless,"mathless");
	HX_VISIT_MEMBER_NAME(koiPond,"koiPond");
	::flixel::FlxState_obj::__Visit(HX_VISIT_ARG);
}

Dynamic PlayState_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 2:
		if (HX_FIELD_EQ(inName,"bb") ) { return bb; }
		break;
	case 3:
		if (HX_FIELD_EQ(inName,"sun") ) { return sun; }
		break;
	case 4:
		if (HX_FIELD_EQ(inName,"weed") ) { return weed; }
		if (HX_FIELD_EQ(inName,"tree") ) { return tree; }
		if (HX_FIELD_EQ(inName,"seed") ) { return seed; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"board") ) { return board; }
		if (HX_FIELD_EQ(inName,"cloud") ) { return cloud; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"numPad") ) { return numPad; }
		if (HX_FIELD_EQ(inName,"bridge") ) { return bridge; }
		if (HX_FIELD_EQ(inName,"market") ) { return market; }
		if (HX_FIELD_EQ(inName,"basket") ) { return basket; }
		if (HX_FIELD_EQ(inName,"create") ) { return create_dyn(); }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"boarder") ) { return boarder; }
		if (HX_FIELD_EQ(inName,"buySign") ) { return buySign; }
		if (HX_FIELD_EQ(inName,"koiPond") ) { return koiPond; }
		if (HX_FIELD_EQ(inName,"buyLand") ) { return buyLand_dyn(); }
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"tutorial") ) { return tutorial; }
		if (HX_FIELD_EQ(inName,"buildBar") ) { return buildBar; }
		if (HX_FIELD_EQ(inName,"fishIcon") ) { return fishIcon; }
		if (HX_FIELD_EQ(inName,"seedDrag") ) { return seedDrag; }
		if (HX_FIELD_EQ(inName,"mathless") ) { return mathless; }
		if (HX_FIELD_EQ(inName,"openMenu") ) { return openMenu_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"tileBoard") ) { return tileBoard; }
		if (HX_FIELD_EQ(inName,"treeArray") ) { return treeArray; }
		if (HX_FIELD_EQ(inName,"appleShed") ) { return appleShed; }
		if (HX_FIELD_EQ(inName,"ownedTile") ) { return ownedTile; }
		if (HX_FIELD_EQ(inName,"callTruck") ) { return callTruck_dyn(); }
		if (HX_FIELD_EQ(inName,"drawBoard") ) { return drawBoard_dyn(); }
		if (HX_FIELD_EQ(inName,"letItRain") ) { return letItRain_dyn(); }
		if (HX_FIELD_EQ(inName,"waterBill") ) { return waterBill_dyn(); }
		if (HX_FIELD_EQ(inName,"appleBill") ) { return appleBill_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"menuButton") ) { return menuButton; }
		if (HX_FIELD_EQ(inName,"menuScreen") ) { return menuScreen; }
		if (HX_FIELD_EQ(inName,"systemStar") ) { return systemStar; }
		if (HX_FIELD_EQ(inName,"plantLayer") ) { return plantLayer; }
		if (HX_FIELD_EQ(inName,"appleCount") ) { return appleCount; }
		if (HX_FIELD_EQ(inName,"greenHouse") ) { return greenHouse; }
		if (HX_FIELD_EQ(inName,"pieMachine") ) { return pieMachine; }
		if (HX_FIELD_EQ(inName,"appleTruck") ) { return appleTruck; }
		if (HX_FIELD_EQ(inName,"sunClicked") ) { return sunClicked; }
		if (HX_FIELD_EQ(inName,"tutorialOn") ) { return tutorialOn; }
		if (HX_FIELD_EQ(inName,"coinsSound") ) { return coinsSound; }
		if (HX_FIELD_EQ(inName,"plantWeeds") ) { return plantWeeds_dyn(); }
		if (HX_FIELD_EQ(inName,"letItShine") ) { return letItShine_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"buildBarTab") ) { return buildBarTab; }
		if (HX_FIELD_EQ(inName,"menuVisible") ) { return menuVisible; }
		if (HX_FIELD_EQ(inName,"buySignCost") ) { return buySignCost; }
		if (HX_FIELD_EQ(inName,"boardIsDown") ) { return boardIsDown; }
		if (HX_FIELD_EQ(inName,"truckButton") ) { return truckButton; }
		if (HX_FIELD_EQ(inName,"piePackIcon") ) { return piePackIcon; }
		if (HX_FIELD_EQ(inName,"placeObject") ) { return placeObject_dyn(); }
		if (HX_FIELD_EQ(inName,"treeClicked") ) { return treeClicked_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"machineLayer") ) { return machineLayer; }
		if (HX_FIELD_EQ(inName,"answerString") ) { return answerString; }
		if (HX_FIELD_EQ(inName,"asteroidBelt") ) { return asteroidBelt; }
		if (HX_FIELD_EQ(inName,"juiceMachine") ) { return juiceMachine; }
		if (HX_FIELD_EQ(inName,"fishIconDrag") ) { return fishIconDrag; }
		if (HX_FIELD_EQ(inName,"buySignSound") ) { return buySignSound; }
		if (HX_FIELD_EQ(inName,"buyLandSound") ) { return buyLandSound; }
		if (HX_FIELD_EQ(inName,"addTreeArray") ) { return addTreeArray_dyn(); }
		if (HX_FIELD_EQ(inName,"submitAnswer") ) { return submitAnswer_dyn(); }
		if (HX_FIELD_EQ(inName,"electricBill") ) { return electricBill_dyn(); }
		if (HX_FIELD_EQ(inName,"turnWaterOff") ) { return turnWaterOff_dyn(); }
		if (HX_FIELD_EQ(inName,"destroySound") ) { return destroySound_dyn(); }
		if (HX_FIELD_EQ(inName,"buildBarOpen") ) { return buildBarOpen_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"buildBarShaft") ) { return buildBarShaft; }
		if (HX_FIELD_EQ(inName,"piePerTenCost") ) { return piePerTenCost; }
		if (HX_FIELD_EQ(inName,"juicePackIcon") ) { return juicePackIcon; }
		if (HX_FIELD_EQ(inName,"overlaysInUse") ) { return overlaysInUse; }
		if (HX_FIELD_EQ(inName,"initPlaystate") ) { return initPlaystate_dyn(); }
		if (HX_FIELD_EQ(inName,"initTreeArray") ) { return initTreeArray_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"bottomeSprites") ) { return bottomeSprites; }
		if (HX_FIELD_EQ(inName,"floatingIsland") ) { return floatingIsland; }
		if (HX_FIELD_EQ(inName,"loadAppleTimer") ) { return loadAppleTimer; }
		if (HX_FIELD_EQ(inName,"pieMachineIcon") ) { return pieMachineIcon; }
		if (HX_FIELD_EQ(inName,"dropletClicked") ) { return dropletClicked; }
		if (HX_FIELD_EQ(inName,"buildBarIsOpen") ) { return buildBarIsOpen; }
		if (HX_FIELD_EQ(inName,"buyButtonPopUp") ) { return buyButtonPopUp_dyn(); }
		if (HX_FIELD_EQ(inName,"destroyObjects") ) { return destroyObjects_dyn(); }
		if (HX_FIELD_EQ(inName,"applesForJuice") ) { return applesForJuice_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"aboveGrassLayer") ) { return aboveGrassLayer; }
		if (HX_FIELD_EQ(inName,"lastTileClicked") ) { return lastTileClicked; }
		if (HX_FIELD_EQ(inName,"tileBoardBorder") ) { return tileBoardBorder; }
		if (HX_FIELD_EQ(inName,"pieMachineArray") ) { return pieMachineArray; }
		if (HX_FIELD_EQ(inName,"applePerTenCost") ) { return applePerTenCost; }
		if (HX_FIELD_EQ(inName,"juicePerTenCost") ) { return juicePerTenCost; }
		if (HX_FIELD_EQ(inName,"currentTruckLvl") ) { return currentTruckLvl; }
		if (HX_FIELD_EQ(inName,"leaveTruckTimer") ) { return leaveTruckTimer; }
		if (HX_FIELD_EQ(inName,"foreGroundRocks") ) { return foreGroundRocks; }
		if (HX_FIELD_EQ(inName,"answerFieldText") ) { return answerFieldText; }
		if (HX_FIELD_EQ(inName,"marketOpenCheck") ) { return marketOpenCheck; }
		if (HX_FIELD_EQ(inName,"truckTutSection") ) { return truckTutSection; }
		if (HX_FIELD_EQ(inName,"callBackStopOne") ) { return callBackStopOne; }
		if (HX_FIELD_EQ(inName,"callBackStopTwo") ) { return callBackStopTwo; }
		if (HX_FIELD_EQ(inName,"tutBuildBarBack") ) { return tutBuildBarBack; }
		if (HX_FIELD_EQ(inName,"tileBoarderFlip") ) { return tileBoarderFlip_dyn(); }
		if (HX_FIELD_EQ(inName,"marketTutOpened") ) { return marketTutOpened_dyn(); }
		if (HX_FIELD_EQ(inName,"restartGameOver") ) { return restartGameOver_dyn(); }
		if (HX_FIELD_EQ(inName,"buildBarActions") ) { return buildBarActions_dyn(); }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"backgroundSprite") ) { return backgroundSprite; }
		if (HX_FIELD_EQ(inName,"appleCountString") ) { return appleCountString; }
		if (HX_FIELD_EQ(inName,"appleDisplayLock") ) { return appleDisplayLock; }
		if (HX_FIELD_EQ(inName,"juiceMachineIcon") ) { return juiceMachineIcon; }
		if (HX_FIELD_EQ(inName,"alertButtonSound") ) { return alertButtonSound; }
		if (HX_FIELD_EQ(inName,"truckButtonSound") ) { return truckButtonSound; }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"juiceMachineArray") ) { return juiceMachineArray; }
		if (HX_FIELD_EQ(inName,"buildBarOpenTimer") ) { return buildBarOpenTimer; }
		if (HX_FIELD_EQ(inName,"buildBarOpenSpeed") ) { return buildBarOpenSpeed; }
		if (HX_FIELD_EQ(inName,"currentTargetTree") ) { return currentTargetTree; }
		if (HX_FIELD_EQ(inName,"greenHouseBarIcon") ) { return greenHouseBarIcon; }
		if (HX_FIELD_EQ(inName,"currentTileBorder") ) { return currentTileBorder; }
		if (HX_FIELD_EQ(inName,"plantingSeedSound") ) { return plantingSeedSound; }
		if (HX_FIELD_EQ(inName,"displayAppleCount") ) { return displayAppleCount_dyn(); }
		if (HX_FIELD_EQ(inName,"displayPadNumbers") ) { return displayPadNumbers_dyn(); }
		if (HX_FIELD_EQ(inName,"truckLoadingGoods") ) { return truckLoadingGoods_dyn(); }
		if (HX_FIELD_EQ(inName,"appleAndJuiceBill") ) { return appleAndJuiceBill_dyn(); }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"belowBottomSprites") ) { return belowBottomSprites; }
		if (HX_FIELD_EQ(inName,"appleForJuiceTimer") ) { return appleForJuiceTimer; }
		if (HX_FIELD_EQ(inName,"pieMachineIconDrag") ) { return pieMachineIconDrag; }
		if (HX_FIELD_EQ(inName,"correctAnswerSound") ) { return correctAnswerSound; }
		if (HX_FIELD_EQ(inName,"placedMachineSound") ) { return placedMachineSound; }
		if (HX_FIELD_EQ(inName,"koiPondPlacedSound") ) { return koiPondPlacedSound; }
		if (HX_FIELD_EQ(inName,"addPieMachineArray") ) { return addPieMachineArray_dyn(); }
		if (HX_FIELD_EQ(inName,"stopPieDragIfBoard") ) { return stopPieDragIfBoard_dyn(); }
		if (HX_FIELD_EQ(inName,"checkForAlertClick") ) { return checkForAlertClick_dyn(); }
		if (HX_FIELD_EQ(inName,"waterSprinklerBill") ) { return waterSprinklerBill_dyn(); }
		if (HX_FIELD_EQ(inName,"createAsteroidBelt") ) { return createAsteroidBelt_dyn(); }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"islandCreationTimer") ) { return islandCreationTimer; }
		if (HX_FIELD_EQ(inName,"appleCountFieldText") ) { return appleCountFieldText; }
		if (HX_FIELD_EQ(inName,"initPieMachineArray") ) { return initPieMachineArray_dyn(); }
		if (HX_FIELD_EQ(inName,"stopSeedDragIfBoard") ) { return stopSeedDragIfBoard_dyn(); }
		if (HX_FIELD_EQ(inName,"stopFishDragIfBoard") ) { return stopFishDragIfBoard_dyn(); }
		if (HX_FIELD_EQ(inName,"factoryElectricBill") ) { return factoryElectricBill_dyn(); }
		if (HX_FIELD_EQ(inName,"removeCallBackOnPad") ) { return removeCallBackOnPad_dyn(); }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"buySignCostFieldText") ) { return buySignCostFieldText; }
		if (HX_FIELD_EQ(inName,"juiceMachineIconDrag") ) { return juiceMachineIconDrag; }
		if (HX_FIELD_EQ(inName,"closeTreeGrowthCheck") ) { return closeTreeGrowthCheck; }
		if (HX_FIELD_EQ(inName,"closeAppleCountCheck") ) { return closeAppleCountCheck; }
		if (HX_FIELD_EQ(inName,"tutUnpauseAlertsDone") ) { return tutUnpauseAlertsDone; }
		if (HX_FIELD_EQ(inName,"incorrectAnswerSound") ) { return incorrectAnswerSound; }
		if (HX_FIELD_EQ(inName,"createFloatingIsland") ) { return createFloatingIsland_dyn(); }
		if (HX_FIELD_EQ(inName,"addFlyingBottleArray") ) { return addFlyingBottleArray_dyn(); }
		if (HX_FIELD_EQ(inName,"addJuiceMachineArray") ) { return addJuiceMachineArray_dyn(); }
		if (HX_FIELD_EQ(inName,"secondTreeTutMessage") ) { return secondTreeTutMessage_dyn(); }
		if (HX_FIELD_EQ(inName,"stopJuiceDragIfBoard") ) { return stopJuiceDragIfBoard_dyn(); }
		if (HX_FIELD_EQ(inName,"applesAndJuiceForPie") ) { return applesAndJuiceForPie_dyn(); }
		break;
	case 21:
		if (HX_FIELD_EQ(inName,"greenHouseBarIconDrag") ) { return greenHouseBarIconDrag; }
		if (HX_FIELD_EQ(inName,"releaseAllClickEvents") ) { return releaseAllClickEvents_dyn(); }
		if (HX_FIELD_EQ(inName,"initJuiceMachineArray") ) { return initJuiceMachineArray_dyn(); }
		break;
	case 22:
		if (HX_FIELD_EQ(inName,"flyingJuiceBottleArray") ) { return flyingJuiceBottleArray; }
		if (HX_FIELD_EQ(inName,"registerAllAlertEvents") ) { return registerAllAlertEvents_dyn(); }
		if (HX_FIELD_EQ(inName,"currentTreeGrowthValue") ) { return currentTreeGrowthValue_dyn(); }
		break;
	case 23:
		if (HX_FIELD_EQ(inName,"currentTargetPieMachine") ) { return currentTargetPieMachine; }
		break;
	case 24:
		if (HX_FIELD_EQ(inName,"appleAndJuiceForPieTimer") ) { return appleAndJuiceForPieTimer; }
		if (HX_FIELD_EQ(inName,"tutorialWrongAnswersSkip") ) { return tutorialWrongAnswersSkip; }
		if (HX_FIELD_EQ(inName,"unregisterAllAlertEvents") ) { return unregisterAllAlertEvents_dyn(); }
		break;
	case 25:
		if (HX_FIELD_EQ(inName,"randomIslandCreationTimer") ) { return randomIslandCreationTimer; }
		if (HX_FIELD_EQ(inName,"currentTargetJuiceMachine") ) { return currentTargetJuiceMachine; }
		if (HX_FIELD_EQ(inName,"stopGreenHouseDragIfBoard") ) { return stopGreenHouseDragIfBoard_dyn(); }
		break;
	case 26:
		if (HX_FIELD_EQ(inName,"closeSecondTreeGrowthCheck") ) { return closeSecondTreeGrowthCheck; }
		if (HX_FIELD_EQ(inName,"initFlyingJuiceBottleArray") ) { return initFlyingJuiceBottleArray_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic PlayState_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 2:
		if (HX_FIELD_EQ(inName,"bb") ) { bb=inValue.Cast< ::ProblemBoard >(); return inValue; }
		break;
	case 3:
		if (HX_FIELD_EQ(inName,"sun") ) { sun=inValue.Cast< ::SunToken >(); return inValue; }
		break;
	case 4:
		if (HX_FIELD_EQ(inName,"weed") ) { weed=inValue.Cast< ::Weed >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tree") ) { tree=inValue.Cast< ::Tree >(); return inValue; }
		if (HX_FIELD_EQ(inName,"seed") ) { seed=inValue.Cast< ::Seed >(); return inValue; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"board") ) { board=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		if (HX_FIELD_EQ(inName,"cloud") ) { cloud=inValue.Cast< ::Cloud >(); return inValue; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"numPad") ) { numPad=inValue.Cast< ::NumberPad >(); return inValue; }
		if (HX_FIELD_EQ(inName,"bridge") ) { bridge=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"market") ) { market=inValue.Cast< ::Market >(); return inValue; }
		if (HX_FIELD_EQ(inName,"basket") ) { basket=inValue.Cast< ::AppleBasket >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"boarder") ) { boarder=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		if (HX_FIELD_EQ(inName,"buySign") ) { buySign=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"koiPond") ) { koiPond=inValue.Cast< ::KoiPond >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"tutorial") ) { tutorial=inValue.Cast< ::Tutorial >(); return inValue; }
		if (HX_FIELD_EQ(inName,"buildBar") ) { buildBar=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"fishIcon") ) { fishIcon=inValue.Cast< ::FishIcon >(); return inValue; }
		if (HX_FIELD_EQ(inName,"seedDrag") ) { seedDrag=inValue.Cast< ::Seed >(); return inValue; }
		if (HX_FIELD_EQ(inName,"mathless") ) { mathless=inValue.Cast< bool >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"tileBoard") ) { tileBoard=inValue.Cast< ::TileBoard >(); return inValue; }
		if (HX_FIELD_EQ(inName,"treeArray") ) { treeArray=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		if (HX_FIELD_EQ(inName,"appleShed") ) { appleShed=inValue.Cast< ::AppleShed >(); return inValue; }
		if (HX_FIELD_EQ(inName,"ownedTile") ) { ownedTile=inValue.Cast< bool >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"menuButton") ) { menuButton=inValue.Cast< ::flixel::ui::FlxButton >(); return inValue; }
		if (HX_FIELD_EQ(inName,"menuScreen") ) { menuScreen=inValue.Cast< ::OptionsScreen >(); return inValue; }
		if (HX_FIELD_EQ(inName,"systemStar") ) { systemStar=inValue.Cast< ::SystemStar >(); return inValue; }
		if (HX_FIELD_EQ(inName,"plantLayer") ) { plantLayer=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"appleCount") ) { appleCount=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"greenHouse") ) { greenHouse=inValue.Cast< ::Greenhouse >(); return inValue; }
		if (HX_FIELD_EQ(inName,"pieMachine") ) { pieMachine=inValue.Cast< ::PieMachine >(); return inValue; }
		if (HX_FIELD_EQ(inName,"appleTruck") ) { appleTruck=inValue.Cast< ::AppleTruck >(); return inValue; }
		if (HX_FIELD_EQ(inName,"sunClicked") ) { sunClicked=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tutorialOn") ) { tutorialOn=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"coinsSound") ) { coinsSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"buildBarTab") ) { buildBarTab=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"menuVisible") ) { menuVisible=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"buySignCost") ) { buySignCost=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"boardIsDown") ) { boardIsDown=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"truckButton") ) { truckButton=inValue.Cast< ::TruckButton >(); return inValue; }
		if (HX_FIELD_EQ(inName,"piePackIcon") ) { piePackIcon=inValue.Cast< ::PiePackIcon >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"machineLayer") ) { machineLayer=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"answerString") ) { answerString=inValue.Cast< ::String >(); return inValue; }
		if (HX_FIELD_EQ(inName,"asteroidBelt") ) { asteroidBelt=inValue.Cast< ::AsteroidBelt >(); return inValue; }
		if (HX_FIELD_EQ(inName,"juiceMachine") ) { juiceMachine=inValue.Cast< ::JuiceMachine >(); return inValue; }
		if (HX_FIELD_EQ(inName,"fishIconDrag") ) { fishIconDrag=inValue.Cast< ::FishIcon >(); return inValue; }
		if (HX_FIELD_EQ(inName,"buySignSound") ) { buySignSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		if (HX_FIELD_EQ(inName,"buyLandSound") ) { buyLandSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"buildBarShaft") ) { buildBarShaft=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"piePerTenCost") ) { piePerTenCost=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"juicePackIcon") ) { juicePackIcon=inValue.Cast< ::JuicePackIcon >(); return inValue; }
		if (HX_FIELD_EQ(inName,"overlaysInUse") ) { overlaysInUse=inValue.Cast< bool >(); return inValue; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"bottomeSprites") ) { bottomeSprites=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"floatingIsland") ) { floatingIsland=inValue.Cast< ::Island >(); return inValue; }
		if (HX_FIELD_EQ(inName,"loadAppleTimer") ) { loadAppleTimer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"pieMachineIcon") ) { pieMachineIcon=inValue.Cast< ::PieIcon >(); return inValue; }
		if (HX_FIELD_EQ(inName,"dropletClicked") ) { dropletClicked=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"buildBarIsOpen") ) { buildBarIsOpen=inValue.Cast< bool >(); return inValue; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"aboveGrassLayer") ) { aboveGrassLayer=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"lastTileClicked") ) { lastTileClicked=inValue.Cast< ::Tile >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tileBoardBorder") ) { tileBoardBorder=inValue.Cast< ::TileBoarder >(); return inValue; }
		if (HX_FIELD_EQ(inName,"pieMachineArray") ) { pieMachineArray=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		if (HX_FIELD_EQ(inName,"applePerTenCost") ) { applePerTenCost=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"juicePerTenCost") ) { juicePerTenCost=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"currentTruckLvl") ) { currentTruckLvl=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"leaveTruckTimer") ) { leaveTruckTimer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"foreGroundRocks") ) { foreGroundRocks=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"answerFieldText") ) { answerFieldText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		if (HX_FIELD_EQ(inName,"marketOpenCheck") ) { marketOpenCheck=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"truckTutSection") ) { truckTutSection=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"callBackStopOne") ) { callBackStopOne=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"callBackStopTwo") ) { callBackStopTwo=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tutBuildBarBack") ) { tutBuildBarBack=inValue.Cast< bool >(); return inValue; }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"backgroundSprite") ) { backgroundSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"appleCountString") ) { appleCountString=inValue.Cast< ::String >(); return inValue; }
		if (HX_FIELD_EQ(inName,"appleDisplayLock") ) { appleDisplayLock=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"juiceMachineIcon") ) { juiceMachineIcon=inValue.Cast< ::JuiceIcon >(); return inValue; }
		if (HX_FIELD_EQ(inName,"alertButtonSound") ) { alertButtonSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		if (HX_FIELD_EQ(inName,"truckButtonSound") ) { truckButtonSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"juiceMachineArray") ) { juiceMachineArray=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		if (HX_FIELD_EQ(inName,"buildBarOpenTimer") ) { buildBarOpenTimer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"buildBarOpenSpeed") ) { buildBarOpenSpeed=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"currentTargetTree") ) { currentTargetTree=inValue.Cast< ::Tree >(); return inValue; }
		if (HX_FIELD_EQ(inName,"greenHouseBarIcon") ) { greenHouseBarIcon=inValue.Cast< ::GreenHouseIcon >(); return inValue; }
		if (HX_FIELD_EQ(inName,"currentTileBorder") ) { currentTileBorder=inValue.Cast< ::TileBoarder >(); return inValue; }
		if (HX_FIELD_EQ(inName,"plantingSeedSound") ) { plantingSeedSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"belowBottomSprites") ) { belowBottomSprites=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"appleForJuiceTimer") ) { appleForJuiceTimer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"pieMachineIconDrag") ) { pieMachineIconDrag=inValue.Cast< ::PieIcon >(); return inValue; }
		if (HX_FIELD_EQ(inName,"correctAnswerSound") ) { correctAnswerSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		if (HX_FIELD_EQ(inName,"placedMachineSound") ) { placedMachineSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		if (HX_FIELD_EQ(inName,"koiPondPlacedSound") ) { koiPondPlacedSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"islandCreationTimer") ) { islandCreationTimer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"appleCountFieldText") ) { appleCountFieldText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"buySignCostFieldText") ) { buySignCostFieldText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		if (HX_FIELD_EQ(inName,"juiceMachineIconDrag") ) { juiceMachineIconDrag=inValue.Cast< ::JuiceIcon >(); return inValue; }
		if (HX_FIELD_EQ(inName,"closeTreeGrowthCheck") ) { closeTreeGrowthCheck=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"closeAppleCountCheck") ) { closeAppleCountCheck=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tutUnpauseAlertsDone") ) { tutUnpauseAlertsDone=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"incorrectAnswerSound") ) { incorrectAnswerSound=inValue.Cast< ::flixel::system::FlxSound >(); return inValue; }
		break;
	case 21:
		if (HX_FIELD_EQ(inName,"greenHouseBarIconDrag") ) { greenHouseBarIconDrag=inValue.Cast< ::GreenHouseIcon >(); return inValue; }
		break;
	case 22:
		if (HX_FIELD_EQ(inName,"flyingJuiceBottleArray") ) { flyingJuiceBottleArray=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		break;
	case 23:
		if (HX_FIELD_EQ(inName,"currentTargetPieMachine") ) { currentTargetPieMachine=inValue.Cast< ::PieMachine >(); return inValue; }
		break;
	case 24:
		if (HX_FIELD_EQ(inName,"appleAndJuiceForPieTimer") ) { appleAndJuiceForPieTimer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tutorialWrongAnswersSkip") ) { tutorialWrongAnswersSkip=inValue.Cast< bool >(); return inValue; }
		break;
	case 25:
		if (HX_FIELD_EQ(inName,"randomIslandCreationTimer") ) { randomIslandCreationTimer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"currentTargetJuiceMachine") ) { currentTargetJuiceMachine=inValue.Cast< ::JuiceMachine >(); return inValue; }
		break;
	case 26:
		if (HX_FIELD_EQ(inName,"closeSecondTreeGrowthCheck") ) { closeSecondTreeGrowthCheck=inValue.Cast< bool >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void PlayState_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("menuButton"));
	outFields->push(HX_CSTRING("menuScreen"));
	outFields->push(HX_CSTRING("tutorial"));
	outFields->push(HX_CSTRING("weed"));
	outFields->push(HX_CSTRING("systemStar"));
	outFields->push(HX_CSTRING("backgroundSprite"));
	outFields->push(HX_CSTRING("buildBar"));
	outFields->push(HX_CSTRING("buildBarTab"));
	outFields->push(HX_CSTRING("buildBarShaft"));
	outFields->push(HX_CSTRING("aboveGrassLayer"));
	outFields->push(HX_CSTRING("plantLayer"));
	outFields->push(HX_CSTRING("machineLayer"));
	outFields->push(HX_CSTRING("belowBottomSprites"));
	outFields->push(HX_CSTRING("bottomeSprites"));
	outFields->push(HX_CSTRING("floatingIsland"));
	outFields->push(HX_CSTRING("tileBoard"));
	outFields->push(HX_CSTRING("lastTileClicked"));
	outFields->push(HX_CSTRING("tileBoardBorder"));
	outFields->push(HX_CSTRING("board"));
	outFields->push(HX_CSTRING("boarder"));
	outFields->push(HX_CSTRING("treeArray"));
	outFields->push(HX_CSTRING("juiceMachineArray"));
	outFields->push(HX_CSTRING("pieMachineArray"));
	outFields->push(HX_CSTRING("flyingJuiceBottleArray"));
	outFields->push(HX_CSTRING("numPad"));
	outFields->push(HX_CSTRING("bb"));
	outFields->push(HX_CSTRING("menuVisible"));
	outFields->push(HX_CSTRING("appleCount"));
	outFields->push(HX_CSTRING("buySignCost"));
	outFields->push(HX_CSTRING("applePerTenCost"));
	outFields->push(HX_CSTRING("juicePerTenCost"));
	outFields->push(HX_CSTRING("piePerTenCost"));
	outFields->push(HX_CSTRING("currentTruckLvl"));
	outFields->push(HX_CSTRING("randomIslandCreationTimer"));
	outFields->push(HX_CSTRING("buildBarOpenTimer"));
	outFields->push(HX_CSTRING("loadAppleTimer"));
	outFields->push(HX_CSTRING("leaveTruckTimer"));
	outFields->push(HX_CSTRING("appleForJuiceTimer"));
	outFields->push(HX_CSTRING("appleAndJuiceForPieTimer"));
	outFields->push(HX_CSTRING("buildBarOpenSpeed"));
	outFields->push(HX_CSTRING("islandCreationTimer"));
	outFields->push(HX_CSTRING("appleCountString"));
	outFields->push(HX_CSTRING("answerString"));
	outFields->push(HX_CSTRING("boardIsDown"));
	outFields->push(HX_CSTRING("appleDisplayLock"));
	outFields->push(HX_CSTRING("asteroidBelt"));
	outFields->push(HX_CSTRING("greenHouse"));
	outFields->push(HX_CSTRING("bridge"));
	outFields->push(HX_CSTRING("buySign"));
	outFields->push(HX_CSTRING("foreGroundRocks"));
	outFields->push(HX_CSTRING("answerFieldText"));
	outFields->push(HX_CSTRING("appleCountFieldText"));
	outFields->push(HX_CSTRING("buySignCostFieldText"));
	outFields->push(HX_CSTRING("market"));
	outFields->push(HX_CSTRING("truckButton"));
	outFields->push(HX_CSTRING("juiceMachine"));
	outFields->push(HX_CSTRING("pieMachine"));
	outFields->push(HX_CSTRING("appleShed"));
	outFields->push(HX_CSTRING("appleTruck"));
	outFields->push(HX_CSTRING("tree"));
	outFields->push(HX_CSTRING("basket"));
	outFields->push(HX_CSTRING("currentTargetTree"));
	outFields->push(HX_CSTRING("currentTargetJuiceMachine"));
	outFields->push(HX_CSTRING("currentTargetPieMachine"));
	outFields->push(HX_CSTRING("juiceMachineIcon"));
	outFields->push(HX_CSTRING("pieMachineIcon"));
	outFields->push(HX_CSTRING("juiceMachineIconDrag"));
	outFields->push(HX_CSTRING("pieMachineIconDrag"));
	outFields->push(HX_CSTRING("fishIcon"));
	outFields->push(HX_CSTRING("fishIconDrag"));
	outFields->push(HX_CSTRING("juicePackIcon"));
	outFields->push(HX_CSTRING("piePackIcon"));
	outFields->push(HX_CSTRING("seed"));
	outFields->push(HX_CSTRING("seedDrag"));
	outFields->push(HX_CSTRING("greenHouseBarIcon"));
	outFields->push(HX_CSTRING("greenHouseBarIconDrag"));
	outFields->push(HX_CSTRING("cloud"));
	outFields->push(HX_CSTRING("sun"));
	outFields->push(HX_CSTRING("currentTileBorder"));
	outFields->push(HX_CSTRING("overlaysInUse"));
	outFields->push(HX_CSTRING("sunClicked"));
	outFields->push(HX_CSTRING("dropletClicked"));
	outFields->push(HX_CSTRING("ownedTile"));
	outFields->push(HX_CSTRING("closeTreeGrowthCheck"));
	outFields->push(HX_CSTRING("closeAppleCountCheck"));
	outFields->push(HX_CSTRING("marketOpenCheck"));
	outFields->push(HX_CSTRING("tutorialOn"));
	outFields->push(HX_CSTRING("buildBarIsOpen"));
	outFields->push(HX_CSTRING("truckTutSection"));
	outFields->push(HX_CSTRING("callBackStopOne"));
	outFields->push(HX_CSTRING("callBackStopTwo"));
	outFields->push(HX_CSTRING("tutorialWrongAnswersSkip"));
	outFields->push(HX_CSTRING("closeSecondTreeGrowthCheck"));
	outFields->push(HX_CSTRING("tutUnpauseAlertsDone"));
	outFields->push(HX_CSTRING("tutBuildBarBack"));
	outFields->push(HX_CSTRING("coinsSound"));
	outFields->push(HX_CSTRING("alertButtonSound"));
	outFields->push(HX_CSTRING("correctAnswerSound"));
	outFields->push(HX_CSTRING("incorrectAnswerSound"));
	outFields->push(HX_CSTRING("plantingSeedSound"));
	outFields->push(HX_CSTRING("buySignSound"));
	outFields->push(HX_CSTRING("buyLandSound"));
	outFields->push(HX_CSTRING("placedMachineSound"));
	outFields->push(HX_CSTRING("truckButtonSound"));
	outFields->push(HX_CSTRING("koiPondPlacedSound"));
	outFields->push(HX_CSTRING("mathless"));
	outFields->push(HX_CSTRING("koiPond"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::ui::FlxButton*/ ,(int)offsetof(PlayState_obj,menuButton),HX_CSTRING("menuButton")},
	{hx::fsObject /*::OptionsScreen*/ ,(int)offsetof(PlayState_obj,menuScreen),HX_CSTRING("menuScreen")},
	{hx::fsObject /*::Tutorial*/ ,(int)offsetof(PlayState_obj,tutorial),HX_CSTRING("tutorial")},
	{hx::fsObject /*::Weed*/ ,(int)offsetof(PlayState_obj,weed),HX_CSTRING("weed")},
	{hx::fsObject /*::SystemStar*/ ,(int)offsetof(PlayState_obj,systemStar),HX_CSTRING("systemStar")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(PlayState_obj,backgroundSprite),HX_CSTRING("backgroundSprite")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(PlayState_obj,buildBar),HX_CSTRING("buildBar")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(PlayState_obj,buildBarTab),HX_CSTRING("buildBarTab")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(PlayState_obj,buildBarShaft),HX_CSTRING("buildBarShaft")},
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(PlayState_obj,aboveGrassLayer),HX_CSTRING("aboveGrassLayer")},
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(PlayState_obj,plantLayer),HX_CSTRING("plantLayer")},
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(PlayState_obj,machineLayer),HX_CSTRING("machineLayer")},
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(PlayState_obj,belowBottomSprites),HX_CSTRING("belowBottomSprites")},
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(PlayState_obj,bottomeSprites),HX_CSTRING("bottomeSprites")},
	{hx::fsObject /*::Island*/ ,(int)offsetof(PlayState_obj,floatingIsland),HX_CSTRING("floatingIsland")},
	{hx::fsObject /*::TileBoard*/ ,(int)offsetof(PlayState_obj,tileBoard),HX_CSTRING("tileBoard")},
	{hx::fsObject /*::Tile*/ ,(int)offsetof(PlayState_obj,lastTileClicked),HX_CSTRING("lastTileClicked")},
	{hx::fsObject /*::TileBoarder*/ ,(int)offsetof(PlayState_obj,tileBoardBorder),HX_CSTRING("tileBoardBorder")},
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(PlayState_obj,board),HX_CSTRING("board")},
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(PlayState_obj,boarder),HX_CSTRING("boarder")},
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(PlayState_obj,treeArray),HX_CSTRING("treeArray")},
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(PlayState_obj,juiceMachineArray),HX_CSTRING("juiceMachineArray")},
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(PlayState_obj,pieMachineArray),HX_CSTRING("pieMachineArray")},
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(PlayState_obj,flyingJuiceBottleArray),HX_CSTRING("flyingJuiceBottleArray")},
	{hx::fsObject /*::NumberPad*/ ,(int)offsetof(PlayState_obj,numPad),HX_CSTRING("numPad")},
	{hx::fsObject /*::ProblemBoard*/ ,(int)offsetof(PlayState_obj,bb),HX_CSTRING("bb")},
	{hx::fsInt,(int)offsetof(PlayState_obj,menuVisible),HX_CSTRING("menuVisible")},
	{hx::fsInt,(int)offsetof(PlayState_obj,appleCount),HX_CSTRING("appleCount")},
	{hx::fsInt,(int)offsetof(PlayState_obj,buySignCost),HX_CSTRING("buySignCost")},
	{hx::fsInt,(int)offsetof(PlayState_obj,applePerTenCost),HX_CSTRING("applePerTenCost")},
	{hx::fsInt,(int)offsetof(PlayState_obj,juicePerTenCost),HX_CSTRING("juicePerTenCost")},
	{hx::fsInt,(int)offsetof(PlayState_obj,piePerTenCost),HX_CSTRING("piePerTenCost")},
	{hx::fsInt,(int)offsetof(PlayState_obj,currentTruckLvl),HX_CSTRING("currentTruckLvl")},
	{hx::fsFloat,(int)offsetof(PlayState_obj,randomIslandCreationTimer),HX_CSTRING("randomIslandCreationTimer")},
	{hx::fsFloat,(int)offsetof(PlayState_obj,buildBarOpenTimer),HX_CSTRING("buildBarOpenTimer")},
	{hx::fsFloat,(int)offsetof(PlayState_obj,loadAppleTimer),HX_CSTRING("loadAppleTimer")},
	{hx::fsFloat,(int)offsetof(PlayState_obj,leaveTruckTimer),HX_CSTRING("leaveTruckTimer")},
	{hx::fsFloat,(int)offsetof(PlayState_obj,appleForJuiceTimer),HX_CSTRING("appleForJuiceTimer")},
	{hx::fsFloat,(int)offsetof(PlayState_obj,appleAndJuiceForPieTimer),HX_CSTRING("appleAndJuiceForPieTimer")},
	{hx::fsInt,(int)offsetof(PlayState_obj,buildBarOpenSpeed),HX_CSTRING("buildBarOpenSpeed")},
	{hx::fsFloat,(int)offsetof(PlayState_obj,islandCreationTimer),HX_CSTRING("islandCreationTimer")},
	{hx::fsString,(int)offsetof(PlayState_obj,appleCountString),HX_CSTRING("appleCountString")},
	{hx::fsString,(int)offsetof(PlayState_obj,answerString),HX_CSTRING("answerString")},
	{hx::fsBool,(int)offsetof(PlayState_obj,boardIsDown),HX_CSTRING("boardIsDown")},
	{hx::fsBool,(int)offsetof(PlayState_obj,appleDisplayLock),HX_CSTRING("appleDisplayLock")},
	{hx::fsObject /*::AsteroidBelt*/ ,(int)offsetof(PlayState_obj,asteroidBelt),HX_CSTRING("asteroidBelt")},
	{hx::fsObject /*::Greenhouse*/ ,(int)offsetof(PlayState_obj,greenHouse),HX_CSTRING("greenHouse")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(PlayState_obj,bridge),HX_CSTRING("bridge")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(PlayState_obj,buySign),HX_CSTRING("buySign")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(PlayState_obj,foreGroundRocks),HX_CSTRING("foreGroundRocks")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(PlayState_obj,answerFieldText),HX_CSTRING("answerFieldText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(PlayState_obj,appleCountFieldText),HX_CSTRING("appleCountFieldText")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(PlayState_obj,buySignCostFieldText),HX_CSTRING("buySignCostFieldText")},
	{hx::fsObject /*::Market*/ ,(int)offsetof(PlayState_obj,market),HX_CSTRING("market")},
	{hx::fsObject /*::TruckButton*/ ,(int)offsetof(PlayState_obj,truckButton),HX_CSTRING("truckButton")},
	{hx::fsObject /*::JuiceMachine*/ ,(int)offsetof(PlayState_obj,juiceMachine),HX_CSTRING("juiceMachine")},
	{hx::fsObject /*::PieMachine*/ ,(int)offsetof(PlayState_obj,pieMachine),HX_CSTRING("pieMachine")},
	{hx::fsObject /*::AppleShed*/ ,(int)offsetof(PlayState_obj,appleShed),HX_CSTRING("appleShed")},
	{hx::fsObject /*::AppleTruck*/ ,(int)offsetof(PlayState_obj,appleTruck),HX_CSTRING("appleTruck")},
	{hx::fsObject /*::Tree*/ ,(int)offsetof(PlayState_obj,tree),HX_CSTRING("tree")},
	{hx::fsObject /*::AppleBasket*/ ,(int)offsetof(PlayState_obj,basket),HX_CSTRING("basket")},
	{hx::fsObject /*::Tree*/ ,(int)offsetof(PlayState_obj,currentTargetTree),HX_CSTRING("currentTargetTree")},
	{hx::fsObject /*::JuiceMachine*/ ,(int)offsetof(PlayState_obj,currentTargetJuiceMachine),HX_CSTRING("currentTargetJuiceMachine")},
	{hx::fsObject /*::PieMachine*/ ,(int)offsetof(PlayState_obj,currentTargetPieMachine),HX_CSTRING("currentTargetPieMachine")},
	{hx::fsObject /*::JuiceIcon*/ ,(int)offsetof(PlayState_obj,juiceMachineIcon),HX_CSTRING("juiceMachineIcon")},
	{hx::fsObject /*::PieIcon*/ ,(int)offsetof(PlayState_obj,pieMachineIcon),HX_CSTRING("pieMachineIcon")},
	{hx::fsObject /*::JuiceIcon*/ ,(int)offsetof(PlayState_obj,juiceMachineIconDrag),HX_CSTRING("juiceMachineIconDrag")},
	{hx::fsObject /*::PieIcon*/ ,(int)offsetof(PlayState_obj,pieMachineIconDrag),HX_CSTRING("pieMachineIconDrag")},
	{hx::fsObject /*::FishIcon*/ ,(int)offsetof(PlayState_obj,fishIcon),HX_CSTRING("fishIcon")},
	{hx::fsObject /*::FishIcon*/ ,(int)offsetof(PlayState_obj,fishIconDrag),HX_CSTRING("fishIconDrag")},
	{hx::fsObject /*::JuicePackIcon*/ ,(int)offsetof(PlayState_obj,juicePackIcon),HX_CSTRING("juicePackIcon")},
	{hx::fsObject /*::PiePackIcon*/ ,(int)offsetof(PlayState_obj,piePackIcon),HX_CSTRING("piePackIcon")},
	{hx::fsObject /*::Seed*/ ,(int)offsetof(PlayState_obj,seed),HX_CSTRING("seed")},
	{hx::fsObject /*::Seed*/ ,(int)offsetof(PlayState_obj,seedDrag),HX_CSTRING("seedDrag")},
	{hx::fsObject /*::GreenHouseIcon*/ ,(int)offsetof(PlayState_obj,greenHouseBarIcon),HX_CSTRING("greenHouseBarIcon")},
	{hx::fsObject /*::GreenHouseIcon*/ ,(int)offsetof(PlayState_obj,greenHouseBarIconDrag),HX_CSTRING("greenHouseBarIconDrag")},
	{hx::fsObject /*::Cloud*/ ,(int)offsetof(PlayState_obj,cloud),HX_CSTRING("cloud")},
	{hx::fsObject /*::SunToken*/ ,(int)offsetof(PlayState_obj,sun),HX_CSTRING("sun")},
	{hx::fsObject /*::TileBoarder*/ ,(int)offsetof(PlayState_obj,currentTileBorder),HX_CSTRING("currentTileBorder")},
	{hx::fsBool,(int)offsetof(PlayState_obj,overlaysInUse),HX_CSTRING("overlaysInUse")},
	{hx::fsBool,(int)offsetof(PlayState_obj,sunClicked),HX_CSTRING("sunClicked")},
	{hx::fsBool,(int)offsetof(PlayState_obj,dropletClicked),HX_CSTRING("dropletClicked")},
	{hx::fsBool,(int)offsetof(PlayState_obj,ownedTile),HX_CSTRING("ownedTile")},
	{hx::fsBool,(int)offsetof(PlayState_obj,closeTreeGrowthCheck),HX_CSTRING("closeTreeGrowthCheck")},
	{hx::fsBool,(int)offsetof(PlayState_obj,closeAppleCountCheck),HX_CSTRING("closeAppleCountCheck")},
	{hx::fsBool,(int)offsetof(PlayState_obj,marketOpenCheck),HX_CSTRING("marketOpenCheck")},
	{hx::fsBool,(int)offsetof(PlayState_obj,tutorialOn),HX_CSTRING("tutorialOn")},
	{hx::fsBool,(int)offsetof(PlayState_obj,buildBarIsOpen),HX_CSTRING("buildBarIsOpen")},
	{hx::fsBool,(int)offsetof(PlayState_obj,truckTutSection),HX_CSTRING("truckTutSection")},
	{hx::fsBool,(int)offsetof(PlayState_obj,callBackStopOne),HX_CSTRING("callBackStopOne")},
	{hx::fsBool,(int)offsetof(PlayState_obj,callBackStopTwo),HX_CSTRING("callBackStopTwo")},
	{hx::fsBool,(int)offsetof(PlayState_obj,tutorialWrongAnswersSkip),HX_CSTRING("tutorialWrongAnswersSkip")},
	{hx::fsBool,(int)offsetof(PlayState_obj,closeSecondTreeGrowthCheck),HX_CSTRING("closeSecondTreeGrowthCheck")},
	{hx::fsBool,(int)offsetof(PlayState_obj,tutUnpauseAlertsDone),HX_CSTRING("tutUnpauseAlertsDone")},
	{hx::fsBool,(int)offsetof(PlayState_obj,tutBuildBarBack),HX_CSTRING("tutBuildBarBack")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(PlayState_obj,coinsSound),HX_CSTRING("coinsSound")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(PlayState_obj,alertButtonSound),HX_CSTRING("alertButtonSound")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(PlayState_obj,correctAnswerSound),HX_CSTRING("correctAnswerSound")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(PlayState_obj,incorrectAnswerSound),HX_CSTRING("incorrectAnswerSound")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(PlayState_obj,plantingSeedSound),HX_CSTRING("plantingSeedSound")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(PlayState_obj,buySignSound),HX_CSTRING("buySignSound")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(PlayState_obj,buyLandSound),HX_CSTRING("buyLandSound")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(PlayState_obj,placedMachineSound),HX_CSTRING("placedMachineSound")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(PlayState_obj,truckButtonSound),HX_CSTRING("truckButtonSound")},
	{hx::fsObject /*::flixel::system::FlxSound*/ ,(int)offsetof(PlayState_obj,koiPondPlacedSound),HX_CSTRING("koiPondPlacedSound")},
	{hx::fsBool,(int)offsetof(PlayState_obj,mathless),HX_CSTRING("mathless")},
	{hx::fsObject /*::KoiPond*/ ,(int)offsetof(PlayState_obj,koiPond),HX_CSTRING("koiPond")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("menuButton"),
	HX_CSTRING("menuScreen"),
	HX_CSTRING("tutorial"),
	HX_CSTRING("weed"),
	HX_CSTRING("systemStar"),
	HX_CSTRING("backgroundSprite"),
	HX_CSTRING("buildBar"),
	HX_CSTRING("buildBarTab"),
	HX_CSTRING("buildBarShaft"),
	HX_CSTRING("aboveGrassLayer"),
	HX_CSTRING("plantLayer"),
	HX_CSTRING("machineLayer"),
	HX_CSTRING("belowBottomSprites"),
	HX_CSTRING("bottomeSprites"),
	HX_CSTRING("floatingIsland"),
	HX_CSTRING("tileBoard"),
	HX_CSTRING("lastTileClicked"),
	HX_CSTRING("tileBoardBorder"),
	HX_CSTRING("board"),
	HX_CSTRING("boarder"),
	HX_CSTRING("treeArray"),
	HX_CSTRING("juiceMachineArray"),
	HX_CSTRING("pieMachineArray"),
	HX_CSTRING("flyingJuiceBottleArray"),
	HX_CSTRING("numPad"),
	HX_CSTRING("bb"),
	HX_CSTRING("menuVisible"),
	HX_CSTRING("appleCount"),
	HX_CSTRING("buySignCost"),
	HX_CSTRING("applePerTenCost"),
	HX_CSTRING("juicePerTenCost"),
	HX_CSTRING("piePerTenCost"),
	HX_CSTRING("currentTruckLvl"),
	HX_CSTRING("randomIslandCreationTimer"),
	HX_CSTRING("buildBarOpenTimer"),
	HX_CSTRING("loadAppleTimer"),
	HX_CSTRING("leaveTruckTimer"),
	HX_CSTRING("appleForJuiceTimer"),
	HX_CSTRING("appleAndJuiceForPieTimer"),
	HX_CSTRING("buildBarOpenSpeed"),
	HX_CSTRING("islandCreationTimer"),
	HX_CSTRING("appleCountString"),
	HX_CSTRING("answerString"),
	HX_CSTRING("boardIsDown"),
	HX_CSTRING("appleDisplayLock"),
	HX_CSTRING("asteroidBelt"),
	HX_CSTRING("greenHouse"),
	HX_CSTRING("bridge"),
	HX_CSTRING("buySign"),
	HX_CSTRING("foreGroundRocks"),
	HX_CSTRING("answerFieldText"),
	HX_CSTRING("appleCountFieldText"),
	HX_CSTRING("buySignCostFieldText"),
	HX_CSTRING("market"),
	HX_CSTRING("truckButton"),
	HX_CSTRING("juiceMachine"),
	HX_CSTRING("pieMachine"),
	HX_CSTRING("appleShed"),
	HX_CSTRING("appleTruck"),
	HX_CSTRING("tree"),
	HX_CSTRING("basket"),
	HX_CSTRING("currentTargetTree"),
	HX_CSTRING("currentTargetJuiceMachine"),
	HX_CSTRING("currentTargetPieMachine"),
	HX_CSTRING("juiceMachineIcon"),
	HX_CSTRING("pieMachineIcon"),
	HX_CSTRING("juiceMachineIconDrag"),
	HX_CSTRING("pieMachineIconDrag"),
	HX_CSTRING("fishIcon"),
	HX_CSTRING("fishIconDrag"),
	HX_CSTRING("juicePackIcon"),
	HX_CSTRING("piePackIcon"),
	HX_CSTRING("seed"),
	HX_CSTRING("seedDrag"),
	HX_CSTRING("greenHouseBarIcon"),
	HX_CSTRING("greenHouseBarIconDrag"),
	HX_CSTRING("cloud"),
	HX_CSTRING("sun"),
	HX_CSTRING("currentTileBorder"),
	HX_CSTRING("overlaysInUse"),
	HX_CSTRING("sunClicked"),
	HX_CSTRING("dropletClicked"),
	HX_CSTRING("ownedTile"),
	HX_CSTRING("closeTreeGrowthCheck"),
	HX_CSTRING("closeAppleCountCheck"),
	HX_CSTRING("marketOpenCheck"),
	HX_CSTRING("tutorialOn"),
	HX_CSTRING("buildBarIsOpen"),
	HX_CSTRING("truckTutSection"),
	HX_CSTRING("callBackStopOne"),
	HX_CSTRING("callBackStopTwo"),
	HX_CSTRING("tutorialWrongAnswersSkip"),
	HX_CSTRING("closeSecondTreeGrowthCheck"),
	HX_CSTRING("tutUnpauseAlertsDone"),
	HX_CSTRING("tutBuildBarBack"),
	HX_CSTRING("coinsSound"),
	HX_CSTRING("alertButtonSound"),
	HX_CSTRING("correctAnswerSound"),
	HX_CSTRING("incorrectAnswerSound"),
	HX_CSTRING("plantingSeedSound"),
	HX_CSTRING("buySignSound"),
	HX_CSTRING("buyLandSound"),
	HX_CSTRING("placedMachineSound"),
	HX_CSTRING("truckButtonSound"),
	HX_CSTRING("koiPondPlacedSound"),
	HX_CSTRING("mathless"),
	HX_CSTRING("koiPond"),
	HX_CSTRING("create"),
	HX_CSTRING("initPlaystate"),
	HX_CSTRING("openMenu"),
	HX_CSTRING("releaseAllClickEvents"),
	HX_CSTRING("initTreeArray"),
	HX_CSTRING("plantWeeds"),
	HX_CSTRING("createFloatingIsland"),
	HX_CSTRING("initFlyingJuiceBottleArray"),
	HX_CSTRING("initJuiceMachineArray"),
	HX_CSTRING("initPieMachineArray"),
	HX_CSTRING("addFlyingBottleArray"),
	HX_CSTRING("addTreeArray"),
	HX_CSTRING("addJuiceMachineArray"),
	HX_CSTRING("addPieMachineArray"),
	HX_CSTRING("tileBoarderFlip"),
	HX_CSTRING("buyButtonPopUp"),
	HX_CSTRING("displayAppleCount"),
	HX_CSTRING("displayPadNumbers"),
	HX_CSTRING("placeObject"),
	HX_CSTRING("treeClicked"),
	HX_CSTRING("secondTreeTutMessage"),
	HX_CSTRING("buyLand"),
	HX_CSTRING("callTruck"),
	HX_CSTRING("truckLoadingGoods"),
	HX_CSTRING("stopSeedDragIfBoard"),
	HX_CSTRING("stopJuiceDragIfBoard"),
	HX_CSTRING("stopPieDragIfBoard"),
	HX_CSTRING("stopFishDragIfBoard"),
	HX_CSTRING("stopGreenHouseDragIfBoard"),
	HX_CSTRING("drawBoard"),
	HX_CSTRING("letItRain"),
	HX_CSTRING("letItShine"),
	HX_CSTRING("unregisterAllAlertEvents"),
	HX_CSTRING("registerAllAlertEvents"),
	HX_CSTRING("checkForAlertClick"),
	HX_CSTRING("submitAnswer"),
	HX_CSTRING("destroyObjects"),
	HX_CSTRING("waterBill"),
	HX_CSTRING("electricBill"),
	HX_CSTRING("appleBill"),
	HX_CSTRING("appleAndJuiceBill"),
	HX_CSTRING("turnWaterOff"),
	HX_CSTRING("applesAndJuiceForPie"),
	HX_CSTRING("applesForJuice"),
	HX_CSTRING("waterSprinklerBill"),
	HX_CSTRING("factoryElectricBill"),
	HX_CSTRING("removeCallBackOnPad"),
	HX_CSTRING("createAsteroidBelt"),
	HX_CSTRING("currentTreeGrowthValue"),
	HX_CSTRING("marketTutOpened"),
	HX_CSTRING("restartGameOver"),
	HX_CSTRING("destroySound"),
	HX_CSTRING("buildBarOpen"),
	HX_CSTRING("buildBarActions"),
	HX_CSTRING("update"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(PlayState_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(PlayState_obj::__mClass,"__mClass");
};

#endif

Class PlayState_obj::__mClass;

void PlayState_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("PlayState"), hx::TCanCast< PlayState_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void PlayState_obj::__boot()
{
}

