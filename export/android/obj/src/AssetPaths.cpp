#include <hxcpp.h>

#ifndef INCLUDED_AssetPaths
#include <AssetPaths.h>
#endif

Void AssetPaths_obj::__construct()
{
	return null();
}

//AssetPaths_obj::~AssetPaths_obj() { }

Dynamic AssetPaths_obj::__CreateEmpty() { return  new AssetPaths_obj; }
hx::ObjectPtr< AssetPaths_obj > AssetPaths_obj::__new()
{  hx::ObjectPtr< AssetPaths_obj > result = new AssetPaths_obj();
	result->__construct();
	return result;}

Dynamic AssetPaths_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< AssetPaths_obj > result = new AssetPaths_obj();
	result->__construct();
	return result;}

::String AssetPaths_obj::data_goes_here__txt;

::String AssetPaths_obj::animatedAppleA__png;

::String AssetPaths_obj::animatedAppleB__png;

::String AssetPaths_obj::answerButton__png;

::String AssetPaths_obj::appleJuiceItemImage__png;

::String AssetPaths_obj::appleRotAnimation__png;

::String AssetPaths_obj::appleShedAnimation__png;

::String AssetPaths_obj::appleTruck__png;

::String AssetPaths_obj::appleTruckButtonOnAnimation__png;

::String AssetPaths_obj::backGroundMassA__png;

::String AssetPaths_obj::backGroundMassAmaster__png;

::String AssetPaths_obj::backGroundMassB__png;

::String AssetPaths_obj::backGroundMassBmaster__png;

::String AssetPaths_obj::backGroundMassC__png;

::String AssetPaths_obj::backGroundMassCmaster__png;

::String AssetPaths_obj::backGroundMassD__png;

::String AssetPaths_obj::backGroundMassDmaster__png;

::String AssetPaths_obj::backGroundMassE__png;

::String AssetPaths_obj::backGroundMassEmaster__png;

::String AssetPaths_obj::backGroundMassF__png;

::String AssetPaths_obj::backGroundMassFmaster__png;

::String AssetPaths_obj::backGroundMassFMouse__png;

::String AssetPaths_obj::backGroundMassG__png;

::String AssetPaths_obj::backGroundMassGmaster__png;

::String AssetPaths_obj::backGroundMassH__png;

::String AssetPaths_obj::backGroundMassHmaster__png;

::String AssetPaths_obj::basketAnimation__png;

::String AssetPaths_obj::blackBoard__png;

::String AssetPaths_obj::blackBoardCorrectBorder__png;

::String AssetPaths_obj::blackBoardWrongBorder__png;

::String AssetPaths_obj::boilerImage__png;

::String AssetPaths_obj::bridgeImage__png;

::String AssetPaths_obj::buildBar__png;

::String AssetPaths_obj::buildBarShaft__png;

::String AssetPaths_obj::buildBarTab__png;

::String AssetPaths_obj::buttonClear__png;

::String AssetPaths_obj::buttonEight__png;

::String AssetPaths_obj::buttonFive__png;

::String AssetPaths_obj::buttonFour__png;

::String AssetPaths_obj::buttonNine__png;

::String AssetPaths_obj::buttonOne__png;

::String AssetPaths_obj::buttonSeven__png;

::String AssetPaths_obj::buttonSix__png;

::String AssetPaths_obj::buttonThree__png;

::String AssetPaths_obj::buttonTwo__png;

::String AssetPaths_obj::buttonZero__png;

::String AssetPaths_obj::buttonZeroShadow__png;

::String AssetPaths_obj::buyLandButton__png;

::String AssetPaths_obj::calcImage__png;

::String AssetPaths_obj::chalkEquals__png;

::String AssetPaths_obj::chalkNumbers_0__png;

::String AssetPaths_obj::chalkNumbers_1__png;

::String AssetPaths_obj::chalkNumbers_2__png;

::String AssetPaths_obj::chalkNumbers_3__png;

::String AssetPaths_obj::chalkNumbers_4__png;

::String AssetPaths_obj::chalkNumbers_5__png;

::String AssetPaths_obj::chalkNumbers_6__png;

::String AssetPaths_obj::chalkNumbers_7__png;

::String AssetPaths_obj::chalkNumbers_8__png;

::String AssetPaths_obj::chalkNumbers_9__png;

::String AssetPaths_obj::cloud_blue__png;

::String AssetPaths_obj::coyPondImage__png;

::String AssetPaths_obj::dropletsAnimation__png;

::String AssetPaths_obj::dropletsAnimationS__png;

::String AssetPaths_obj::dropletsAnimationT__png;

::String AssetPaths_obj::dropletsAnimationU__png;

::String AssetPaths_obj::fillingBottleAnimation__png;

::String AssetPaths_obj::fishIcon__png;

::String AssetPaths_obj::fishItemAnimation__png;

::String AssetPaths_obj::gameTile__png;

::String AssetPaths_obj::gameTileBoarder__png;

::String AssetPaths_obj::gameTileOwned__png;

::String AssetPaths_obj::goldCoin__png;

::String AssetPaths_obj::greenHouseAnimation__png;

::String AssetPaths_obj::greenHouseIconImage__png;

::String AssetPaths_obj::GreenHouseItemAnimation__png;

::String AssetPaths_obj::greenHouseShrub__png;

::String AssetPaths_obj::greenHouseTree__png;

::String AssetPaths_obj::greenHouseTreeAnimation__png;

::String AssetPaths_obj::holeAnimation__png;

::String AssetPaths_obj::images_go_here__txt;

::String AssetPaths_obj::islandMouseAnimation__png;

::String AssetPaths_obj::juiceMachineAnimation__png;

::String AssetPaths_obj::juiceMachineIcon__png;

::String AssetPaths_obj::juiceMachineSwitchAnimation__png;

::String AssetPaths_obj::koiFishAnimation__png;

::String AssetPaths_obj::koiFishItemDrag__png;

::String AssetPaths_obj::koiPad__png;

::String AssetPaths_obj::koiPondAnimation__png;

::String AssetPaths_obj::koiPondImage__png;

::String AssetPaths_obj::koiRock__png;

::String AssetPaths_obj::koiSand__png;

::String AssetPaths_obj::marketSideBar__png;

::String AssetPaths_obj::marketSideBarShaft__png;

::String AssetPaths_obj::marketSideBarTab__png;

::String AssetPaths_obj::market_seed__png;

::String AssetPaths_obj::meteorField__png;

::String AssetPaths_obj::meteorFieldMaster__png;

::String AssetPaths_obj::minusOneImage__png;

::String AssetPaths_obj::minusOperation__png;

::String AssetPaths_obj::mouseAnimation__png;

::String AssetPaths_obj::multiOperation__png;

::String AssetPaths_obj::optionsScreen__png;

::String AssetPaths_obj::packOfJuiceIcon__png;

::String AssetPaths_obj::packOfPiesIcon__png;

::String AssetPaths_obj::pickedAppleLeaves__png;

::String AssetPaths_obj::pieIcon__png;

::String AssetPaths_obj::pieMachineAnimation__png;

::String AssetPaths_obj::pieMachineItem__png;

::String AssetPaths_obj::plusOperation__png;

::String AssetPaths_obj::rainCloudAnimation__png;

::String AssetPaths_obj::rainCloudAnimationA__png;

::String AssetPaths_obj::repairAnimationS__png;

::String AssetPaths_obj::rockyForeground__png;

::String AssetPaths_obj::rollingMoon__png;

::String AssetPaths_obj::seedItemAnimation__png;

::String AssetPaths_obj::sidewaysIsland__png;

::String AssetPaths_obj::spaceBackground__png;

::String AssetPaths_obj::sprinklerAlertAnimation__png;

::String AssetPaths_obj::sprinklerAlertAnimationS__png;

::String AssetPaths_obj::sprinklerAnimation__png;

::String AssetPaths_obj::sprinklerSwtichAnimation__png;

::String AssetPaths_obj::sunAnimationA__png;

::String AssetPaths_obj::sunAnimationB__png;

::String AssetPaths_obj::sunAnimationC__png;

::String AssetPaths_obj::sunTokenAnimation__png;

::String AssetPaths_obj::systemStar__png;

::String AssetPaths_obj::systemStarAnimation__png;

::String AssetPaths_obj::tileAnimation__png;

::String AssetPaths_obj::tileAnimationTEST__png;

::String AssetPaths_obj::tileSprite__png;

::String AssetPaths_obj::titleBackground__png;

::String AssetPaths_obj::titleEasy__png;

::String AssetPaths_obj::titleImage__png;

::String AssetPaths_obj::titleImageProcessed__png;

::String AssetPaths_obj::titleScreenGrass__png;

::String AssetPaths_obj::treeAnim0__png;

::String AssetPaths_obj::treeAnim1__png;

::String AssetPaths_obj::treeAnim2__png;

::String AssetPaths_obj::treeAnim3__png;

::String AssetPaths_obj::treeAnim4__png;

::String AssetPaths_obj::treeAnim5__png;

::String AssetPaths_obj::treeAnim6__png;

::String AssetPaths_obj::treeAnim7__png;

::String AssetPaths_obj::treeAnimation__png;

::String AssetPaths_obj::treeAnimationA__png;

::String AssetPaths_obj::treeAnimationB__png;

::String AssetPaths_obj::treeDeathAnimation__png;

::String AssetPaths_obj::treeDeathAnimation97__png;

::String AssetPaths_obj::treeDeathAnimation98__png;

::String AssetPaths_obj::treeDeathAnimation99__png;

::String AssetPaths_obj::treeSpriteA__png;

::String AssetPaths_obj::treeSpriteA99__png;

::String AssetPaths_obj::tree_seed__png;

::String AssetPaths_obj::truckBellAnimation__png;

::String AssetPaths_obj::truckBellImage__png;

::String AssetPaths_obj::tutCheckBoxAnimate__png;

::String AssetPaths_obj::tutHand__png;

::String AssetPaths_obj::tutHandAnimation__png;

::String AssetPaths_obj::tutMessageEight__png;

::String AssetPaths_obj::tutMessageFive__png;

::String AssetPaths_obj::tutMessageFour__png;

::String AssetPaths_obj::tutMessageNine__png;

::String AssetPaths_obj::tutMessageOne__png;

::String AssetPaths_obj::tutMessageSeven__png;

::String AssetPaths_obj::tutMessageSix__png;

::String AssetPaths_obj::tutMessageTen__png;

::String AssetPaths_obj::tutMessageThree__png;

::String AssetPaths_obj::tutMessageTwo__png;

::String AssetPaths_obj::upsideDownIsland__png;

::String AssetPaths_obj::waterfallAnimation__png;

::String AssetPaths_obj::waterfallImage__png;

::String AssetPaths_obj::waterFallTile__png;

::String AssetPaths_obj::weedA__png;

::String AssetPaths_obj::weedB__png;

::String AssetPaths_obj::BeppleSongONe__wav;

::String AssetPaths_obj::BeppleSongOneOGG__ogg;

::String AssetPaths_obj::music_goes_here__txt;

::String AssetPaths_obj::alertButtonSound__wav;

::String AssetPaths_obj::buySignSound__wav;

::String AssetPaths_obj::calcButtonSound__wav;

::String AssetPaths_obj::cashRegisterSound__wav;

::String AssetPaths_obj::coinsSound__wav;

::String AssetPaths_obj::correctAnswerSoundA__wav;

::String AssetPaths_obj::fallenAppleSound__wav;

::String AssetPaths_obj::fillingJuiceBottleSound__wav;

::String AssetPaths_obj::greenHouseWindChimeSound__wav;

::String AssetPaths_obj::growingTree__wav;

::String AssetPaths_obj::growingTreeA__wav;

::String AssetPaths_obj::growingTreeB__wav;

::String AssetPaths_obj::growingTreeC__wav;

::String AssetPaths_obj::growingTreeD__wav;

::String AssetPaths_obj::incorrectAnswerSound__wav;

::String AssetPaths_obj::juiceMachineSound__wav;

::String AssetPaths_obj::koiClicked0__wav;

::String AssetPaths_obj::koiClicked1__wav;

::String AssetPaths_obj::koiClicked2__wav;

::String AssetPaths_obj::koiClicked4__wav;

::String AssetPaths_obj::koiPondPlacedSound__wav;

::String AssetPaths_obj::koiPondSound__wav;

::String AssetPaths_obj::marketSlideClosedSound__wav;

::String AssetPaths_obj::marketSlideOpenSound__wav;

::String AssetPaths_obj::mouseHoleSound__wav;

::String AssetPaths_obj::mouseWalkingSound__wav;

::String AssetPaths_obj::pickedApplesGround__wav;

::String AssetPaths_obj::pickedLeavesSound__wav;

::String AssetPaths_obj::pieBakedSound__wav;

::String AssetPaths_obj::placedMachineSound__wav;

::String AssetPaths_obj::plantingSeedSound__wav;

::String AssetPaths_obj::pouringJuiceSound__wav;

::String AssetPaths_obj::raining__wav;

::String AssetPaths_obj::sounds_go_here__txt;

::String AssetPaths_obj::sprinklerButtonSound__wav;

::String AssetPaths_obj::sprinklerSound__wav;

::String AssetPaths_obj::sunshine__wav;

::String AssetPaths_obj::truckButtonSound__wav;

::String AssetPaths_obj::truckSound__wav;


AssetPaths_obj::AssetPaths_obj()
{
}

Dynamic AssetPaths_obj::__Field(const ::String &inName,bool inCallProp)
{
	return super::__Field(inName,inCallProp);
}

Dynamic AssetPaths_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	return super::__SetField(inName,inValue,inCallProp);
}

void AssetPaths_obj::__GetFields(Array< ::String> &outFields)
{
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("data_goes_here__txt"),
	HX_CSTRING("animatedAppleA__png"),
	HX_CSTRING("animatedAppleB__png"),
	HX_CSTRING("answerButton__png"),
	HX_CSTRING("appleJuiceItemImage__png"),
	HX_CSTRING("appleRotAnimation__png"),
	HX_CSTRING("appleShedAnimation__png"),
	HX_CSTRING("appleTruck__png"),
	HX_CSTRING("appleTruckButtonOnAnimation__png"),
	HX_CSTRING("backGroundMassA__png"),
	HX_CSTRING("backGroundMassAmaster__png"),
	HX_CSTRING("backGroundMassB__png"),
	HX_CSTRING("backGroundMassBmaster__png"),
	HX_CSTRING("backGroundMassC__png"),
	HX_CSTRING("backGroundMassCmaster__png"),
	HX_CSTRING("backGroundMassD__png"),
	HX_CSTRING("backGroundMassDmaster__png"),
	HX_CSTRING("backGroundMassE__png"),
	HX_CSTRING("backGroundMassEmaster__png"),
	HX_CSTRING("backGroundMassF__png"),
	HX_CSTRING("backGroundMassFmaster__png"),
	HX_CSTRING("backGroundMassFMouse__png"),
	HX_CSTRING("backGroundMassG__png"),
	HX_CSTRING("backGroundMassGmaster__png"),
	HX_CSTRING("backGroundMassH__png"),
	HX_CSTRING("backGroundMassHmaster__png"),
	HX_CSTRING("basketAnimation__png"),
	HX_CSTRING("blackBoard__png"),
	HX_CSTRING("blackBoardCorrectBorder__png"),
	HX_CSTRING("blackBoardWrongBorder__png"),
	HX_CSTRING("boilerImage__png"),
	HX_CSTRING("bridgeImage__png"),
	HX_CSTRING("buildBar__png"),
	HX_CSTRING("buildBarShaft__png"),
	HX_CSTRING("buildBarTab__png"),
	HX_CSTRING("buttonClear__png"),
	HX_CSTRING("buttonEight__png"),
	HX_CSTRING("buttonFive__png"),
	HX_CSTRING("buttonFour__png"),
	HX_CSTRING("buttonNine__png"),
	HX_CSTRING("buttonOne__png"),
	HX_CSTRING("buttonSeven__png"),
	HX_CSTRING("buttonSix__png"),
	HX_CSTRING("buttonThree__png"),
	HX_CSTRING("buttonTwo__png"),
	HX_CSTRING("buttonZero__png"),
	HX_CSTRING("buttonZeroShadow__png"),
	HX_CSTRING("buyLandButton__png"),
	HX_CSTRING("calcImage__png"),
	HX_CSTRING("chalkEquals__png"),
	HX_CSTRING("chalkNumbers_0__png"),
	HX_CSTRING("chalkNumbers_1__png"),
	HX_CSTRING("chalkNumbers_2__png"),
	HX_CSTRING("chalkNumbers_3__png"),
	HX_CSTRING("chalkNumbers_4__png"),
	HX_CSTRING("chalkNumbers_5__png"),
	HX_CSTRING("chalkNumbers_6__png"),
	HX_CSTRING("chalkNumbers_7__png"),
	HX_CSTRING("chalkNumbers_8__png"),
	HX_CSTRING("chalkNumbers_9__png"),
	HX_CSTRING("cloud_blue__png"),
	HX_CSTRING("coyPondImage__png"),
	HX_CSTRING("dropletsAnimation__png"),
	HX_CSTRING("dropletsAnimationS__png"),
	HX_CSTRING("dropletsAnimationT__png"),
	HX_CSTRING("dropletsAnimationU__png"),
	HX_CSTRING("fillingBottleAnimation__png"),
	HX_CSTRING("fishIcon__png"),
	HX_CSTRING("fishItemAnimation__png"),
	HX_CSTRING("gameTile__png"),
	HX_CSTRING("gameTileBoarder__png"),
	HX_CSTRING("gameTileOwned__png"),
	HX_CSTRING("goldCoin__png"),
	HX_CSTRING("greenHouseAnimation__png"),
	HX_CSTRING("greenHouseIconImage__png"),
	HX_CSTRING("GreenHouseItemAnimation__png"),
	HX_CSTRING("greenHouseShrub__png"),
	HX_CSTRING("greenHouseTree__png"),
	HX_CSTRING("greenHouseTreeAnimation__png"),
	HX_CSTRING("holeAnimation__png"),
	HX_CSTRING("images_go_here__txt"),
	HX_CSTRING("islandMouseAnimation__png"),
	HX_CSTRING("juiceMachineAnimation__png"),
	HX_CSTRING("juiceMachineIcon__png"),
	HX_CSTRING("juiceMachineSwitchAnimation__png"),
	HX_CSTRING("koiFishAnimation__png"),
	HX_CSTRING("koiFishItemDrag__png"),
	HX_CSTRING("koiPad__png"),
	HX_CSTRING("koiPondAnimation__png"),
	HX_CSTRING("koiPondImage__png"),
	HX_CSTRING("koiRock__png"),
	HX_CSTRING("koiSand__png"),
	HX_CSTRING("marketSideBar__png"),
	HX_CSTRING("marketSideBarShaft__png"),
	HX_CSTRING("marketSideBarTab__png"),
	HX_CSTRING("market_seed__png"),
	HX_CSTRING("meteorField__png"),
	HX_CSTRING("meteorFieldMaster__png"),
	HX_CSTRING("minusOneImage__png"),
	HX_CSTRING("minusOperation__png"),
	HX_CSTRING("mouseAnimation__png"),
	HX_CSTRING("multiOperation__png"),
	HX_CSTRING("optionsScreen__png"),
	HX_CSTRING("packOfJuiceIcon__png"),
	HX_CSTRING("packOfPiesIcon__png"),
	HX_CSTRING("pickedAppleLeaves__png"),
	HX_CSTRING("pieIcon__png"),
	HX_CSTRING("pieMachineAnimation__png"),
	HX_CSTRING("pieMachineItem__png"),
	HX_CSTRING("plusOperation__png"),
	HX_CSTRING("rainCloudAnimation__png"),
	HX_CSTRING("rainCloudAnimationA__png"),
	HX_CSTRING("repairAnimationS__png"),
	HX_CSTRING("rockyForeground__png"),
	HX_CSTRING("rollingMoon__png"),
	HX_CSTRING("seedItemAnimation__png"),
	HX_CSTRING("sidewaysIsland__png"),
	HX_CSTRING("spaceBackground__png"),
	HX_CSTRING("sprinklerAlertAnimation__png"),
	HX_CSTRING("sprinklerAlertAnimationS__png"),
	HX_CSTRING("sprinklerAnimation__png"),
	HX_CSTRING("sprinklerSwtichAnimation__png"),
	HX_CSTRING("sunAnimationA__png"),
	HX_CSTRING("sunAnimationB__png"),
	HX_CSTRING("sunAnimationC__png"),
	HX_CSTRING("sunTokenAnimation__png"),
	HX_CSTRING("systemStar__png"),
	HX_CSTRING("systemStarAnimation__png"),
	HX_CSTRING("tileAnimation__png"),
	HX_CSTRING("tileAnimationTEST__png"),
	HX_CSTRING("tileSprite__png"),
	HX_CSTRING("titleBackground__png"),
	HX_CSTRING("titleEasy__png"),
	HX_CSTRING("titleImage__png"),
	HX_CSTRING("titleImageProcessed__png"),
	HX_CSTRING("titleScreenGrass__png"),
	HX_CSTRING("treeAnim0__png"),
	HX_CSTRING("treeAnim1__png"),
	HX_CSTRING("treeAnim2__png"),
	HX_CSTRING("treeAnim3__png"),
	HX_CSTRING("treeAnim4__png"),
	HX_CSTRING("treeAnim5__png"),
	HX_CSTRING("treeAnim6__png"),
	HX_CSTRING("treeAnim7__png"),
	HX_CSTRING("treeAnimation__png"),
	HX_CSTRING("treeAnimationA__png"),
	HX_CSTRING("treeAnimationB__png"),
	HX_CSTRING("treeDeathAnimation__png"),
	HX_CSTRING("treeDeathAnimation97__png"),
	HX_CSTRING("treeDeathAnimation98__png"),
	HX_CSTRING("treeDeathAnimation99__png"),
	HX_CSTRING("treeSpriteA__png"),
	HX_CSTRING("treeSpriteA99__png"),
	HX_CSTRING("tree_seed__png"),
	HX_CSTRING("truckBellAnimation__png"),
	HX_CSTRING("truckBellImage__png"),
	HX_CSTRING("tutCheckBoxAnimate__png"),
	HX_CSTRING("tutHand__png"),
	HX_CSTRING("tutHandAnimation__png"),
	HX_CSTRING("tutMessageEight__png"),
	HX_CSTRING("tutMessageFive__png"),
	HX_CSTRING("tutMessageFour__png"),
	HX_CSTRING("tutMessageNine__png"),
	HX_CSTRING("tutMessageOne__png"),
	HX_CSTRING("tutMessageSeven__png"),
	HX_CSTRING("tutMessageSix__png"),
	HX_CSTRING("tutMessageTen__png"),
	HX_CSTRING("tutMessageThree__png"),
	HX_CSTRING("tutMessageTwo__png"),
	HX_CSTRING("upsideDownIsland__png"),
	HX_CSTRING("waterfallAnimation__png"),
	HX_CSTRING("waterfallImage__png"),
	HX_CSTRING("waterFallTile__png"),
	HX_CSTRING("weedA__png"),
	HX_CSTRING("weedB__png"),
	HX_CSTRING("BeppleSongONe__wav"),
	HX_CSTRING("BeppleSongOneOGG__ogg"),
	HX_CSTRING("music_goes_here__txt"),
	HX_CSTRING("alertButtonSound__wav"),
	HX_CSTRING("buySignSound__wav"),
	HX_CSTRING("calcButtonSound__wav"),
	HX_CSTRING("cashRegisterSound__wav"),
	HX_CSTRING("coinsSound__wav"),
	HX_CSTRING("correctAnswerSoundA__wav"),
	HX_CSTRING("fallenAppleSound__wav"),
	HX_CSTRING("fillingJuiceBottleSound__wav"),
	HX_CSTRING("greenHouseWindChimeSound__wav"),
	HX_CSTRING("growingTree__wav"),
	HX_CSTRING("growingTreeA__wav"),
	HX_CSTRING("growingTreeB__wav"),
	HX_CSTRING("growingTreeC__wav"),
	HX_CSTRING("growingTreeD__wav"),
	HX_CSTRING("incorrectAnswerSound__wav"),
	HX_CSTRING("juiceMachineSound__wav"),
	HX_CSTRING("koiClicked0__wav"),
	HX_CSTRING("koiClicked1__wav"),
	HX_CSTRING("koiClicked2__wav"),
	HX_CSTRING("koiClicked4__wav"),
	HX_CSTRING("koiPondPlacedSound__wav"),
	HX_CSTRING("koiPondSound__wav"),
	HX_CSTRING("marketSlideClosedSound__wav"),
	HX_CSTRING("marketSlideOpenSound__wav"),
	HX_CSTRING("mouseHoleSound__wav"),
	HX_CSTRING("mouseWalkingSound__wav"),
	HX_CSTRING("pickedApplesGround__wav"),
	HX_CSTRING("pickedLeavesSound__wav"),
	HX_CSTRING("pieBakedSound__wav"),
	HX_CSTRING("placedMachineSound__wav"),
	HX_CSTRING("plantingSeedSound__wav"),
	HX_CSTRING("pouringJuiceSound__wav"),
	HX_CSTRING("raining__wav"),
	HX_CSTRING("sounds_go_here__txt"),
	HX_CSTRING("sprinklerButtonSound__wav"),
	HX_CSTRING("sprinklerSound__wav"),
	HX_CSTRING("sunshine__wav"),
	HX_CSTRING("truckButtonSound__wav"),
	HX_CSTRING("truckSound__wav"),
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo *sMemberStorageInfo = 0;
#endif

static ::String sMemberFields[] = {
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(AssetPaths_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::data_goes_here__txt,"data_goes_here__txt");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::animatedAppleA__png,"animatedAppleA__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::animatedAppleB__png,"animatedAppleB__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::answerButton__png,"answerButton__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::appleJuiceItemImage__png,"appleJuiceItemImage__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::appleRotAnimation__png,"appleRotAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::appleShedAnimation__png,"appleShedAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::appleTruck__png,"appleTruck__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::appleTruckButtonOnAnimation__png,"appleTruckButtonOnAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::backGroundMassA__png,"backGroundMassA__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::backGroundMassAmaster__png,"backGroundMassAmaster__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::backGroundMassB__png,"backGroundMassB__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::backGroundMassBmaster__png,"backGroundMassBmaster__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::backGroundMassC__png,"backGroundMassC__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::backGroundMassCmaster__png,"backGroundMassCmaster__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::backGroundMassD__png,"backGroundMassD__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::backGroundMassDmaster__png,"backGroundMassDmaster__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::backGroundMassE__png,"backGroundMassE__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::backGroundMassEmaster__png,"backGroundMassEmaster__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::backGroundMassF__png,"backGroundMassF__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::backGroundMassFmaster__png,"backGroundMassFmaster__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::backGroundMassFMouse__png,"backGroundMassFMouse__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::backGroundMassG__png,"backGroundMassG__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::backGroundMassGmaster__png,"backGroundMassGmaster__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::backGroundMassH__png,"backGroundMassH__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::backGroundMassHmaster__png,"backGroundMassHmaster__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::basketAnimation__png,"basketAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::blackBoard__png,"blackBoard__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::blackBoardCorrectBorder__png,"blackBoardCorrectBorder__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::blackBoardWrongBorder__png,"blackBoardWrongBorder__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::boilerImage__png,"boilerImage__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::bridgeImage__png,"bridgeImage__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::buildBar__png,"buildBar__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::buildBarShaft__png,"buildBarShaft__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::buildBarTab__png,"buildBarTab__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::buttonClear__png,"buttonClear__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::buttonEight__png,"buttonEight__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::buttonFive__png,"buttonFive__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::buttonFour__png,"buttonFour__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::buttonNine__png,"buttonNine__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::buttonOne__png,"buttonOne__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::buttonSeven__png,"buttonSeven__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::buttonSix__png,"buttonSix__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::buttonThree__png,"buttonThree__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::buttonTwo__png,"buttonTwo__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::buttonZero__png,"buttonZero__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::buttonZeroShadow__png,"buttonZeroShadow__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::buyLandButton__png,"buyLandButton__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::calcImage__png,"calcImage__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::chalkEquals__png,"chalkEquals__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::chalkNumbers_0__png,"chalkNumbers_0__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::chalkNumbers_1__png,"chalkNumbers_1__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::chalkNumbers_2__png,"chalkNumbers_2__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::chalkNumbers_3__png,"chalkNumbers_3__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::chalkNumbers_4__png,"chalkNumbers_4__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::chalkNumbers_5__png,"chalkNumbers_5__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::chalkNumbers_6__png,"chalkNumbers_6__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::chalkNumbers_7__png,"chalkNumbers_7__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::chalkNumbers_8__png,"chalkNumbers_8__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::chalkNumbers_9__png,"chalkNumbers_9__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::cloud_blue__png,"cloud_blue__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::coyPondImage__png,"coyPondImage__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::dropletsAnimation__png,"dropletsAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::dropletsAnimationS__png,"dropletsAnimationS__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::dropletsAnimationT__png,"dropletsAnimationT__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::dropletsAnimationU__png,"dropletsAnimationU__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::fillingBottleAnimation__png,"fillingBottleAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::fishIcon__png,"fishIcon__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::fishItemAnimation__png,"fishItemAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::gameTile__png,"gameTile__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::gameTileBoarder__png,"gameTileBoarder__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::gameTileOwned__png,"gameTileOwned__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::goldCoin__png,"goldCoin__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::greenHouseAnimation__png,"greenHouseAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::greenHouseIconImage__png,"greenHouseIconImage__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::GreenHouseItemAnimation__png,"GreenHouseItemAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::greenHouseShrub__png,"greenHouseShrub__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::greenHouseTree__png,"greenHouseTree__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::greenHouseTreeAnimation__png,"greenHouseTreeAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::holeAnimation__png,"holeAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::images_go_here__txt,"images_go_here__txt");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::islandMouseAnimation__png,"islandMouseAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::juiceMachineAnimation__png,"juiceMachineAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::juiceMachineIcon__png,"juiceMachineIcon__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::juiceMachineSwitchAnimation__png,"juiceMachineSwitchAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::koiFishAnimation__png,"koiFishAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::koiFishItemDrag__png,"koiFishItemDrag__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::koiPad__png,"koiPad__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::koiPondAnimation__png,"koiPondAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::koiPondImage__png,"koiPondImage__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::koiRock__png,"koiRock__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::koiSand__png,"koiSand__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::marketSideBar__png,"marketSideBar__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::marketSideBarShaft__png,"marketSideBarShaft__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::marketSideBarTab__png,"marketSideBarTab__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::market_seed__png,"market_seed__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::meteorField__png,"meteorField__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::meteorFieldMaster__png,"meteorFieldMaster__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::minusOneImage__png,"minusOneImage__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::minusOperation__png,"minusOperation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::mouseAnimation__png,"mouseAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::multiOperation__png,"multiOperation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::optionsScreen__png,"optionsScreen__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::packOfJuiceIcon__png,"packOfJuiceIcon__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::packOfPiesIcon__png,"packOfPiesIcon__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::pickedAppleLeaves__png,"pickedAppleLeaves__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::pieIcon__png,"pieIcon__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::pieMachineAnimation__png,"pieMachineAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::pieMachineItem__png,"pieMachineItem__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::plusOperation__png,"plusOperation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::rainCloudAnimation__png,"rainCloudAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::rainCloudAnimationA__png,"rainCloudAnimationA__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::repairAnimationS__png,"repairAnimationS__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::rockyForeground__png,"rockyForeground__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::rollingMoon__png,"rollingMoon__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::seedItemAnimation__png,"seedItemAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::sidewaysIsland__png,"sidewaysIsland__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::spaceBackground__png,"spaceBackground__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::sprinklerAlertAnimation__png,"sprinklerAlertAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::sprinklerAlertAnimationS__png,"sprinklerAlertAnimationS__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::sprinklerAnimation__png,"sprinklerAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::sprinklerSwtichAnimation__png,"sprinklerSwtichAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::sunAnimationA__png,"sunAnimationA__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::sunAnimationB__png,"sunAnimationB__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::sunAnimationC__png,"sunAnimationC__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::sunTokenAnimation__png,"sunTokenAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::systemStar__png,"systemStar__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::systemStarAnimation__png,"systemStarAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::tileAnimation__png,"tileAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::tileAnimationTEST__png,"tileAnimationTEST__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::tileSprite__png,"tileSprite__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::titleBackground__png,"titleBackground__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::titleEasy__png,"titleEasy__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::titleImage__png,"titleImage__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::titleImageProcessed__png,"titleImageProcessed__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::titleScreenGrass__png,"titleScreenGrass__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::treeAnim0__png,"treeAnim0__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::treeAnim1__png,"treeAnim1__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::treeAnim2__png,"treeAnim2__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::treeAnim3__png,"treeAnim3__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::treeAnim4__png,"treeAnim4__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::treeAnim5__png,"treeAnim5__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::treeAnim6__png,"treeAnim6__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::treeAnim7__png,"treeAnim7__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::treeAnimation__png,"treeAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::treeAnimationA__png,"treeAnimationA__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::treeAnimationB__png,"treeAnimationB__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::treeDeathAnimation__png,"treeDeathAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::treeDeathAnimation97__png,"treeDeathAnimation97__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::treeDeathAnimation98__png,"treeDeathAnimation98__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::treeDeathAnimation99__png,"treeDeathAnimation99__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::treeSpriteA__png,"treeSpriteA__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::treeSpriteA99__png,"treeSpriteA99__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::tree_seed__png,"tree_seed__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::truckBellAnimation__png,"truckBellAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::truckBellImage__png,"truckBellImage__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::tutCheckBoxAnimate__png,"tutCheckBoxAnimate__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::tutHand__png,"tutHand__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::tutHandAnimation__png,"tutHandAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::tutMessageEight__png,"tutMessageEight__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::tutMessageFive__png,"tutMessageFive__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::tutMessageFour__png,"tutMessageFour__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::tutMessageNine__png,"tutMessageNine__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::tutMessageOne__png,"tutMessageOne__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::tutMessageSeven__png,"tutMessageSeven__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::tutMessageSix__png,"tutMessageSix__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::tutMessageTen__png,"tutMessageTen__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::tutMessageThree__png,"tutMessageThree__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::tutMessageTwo__png,"tutMessageTwo__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::upsideDownIsland__png,"upsideDownIsland__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::waterfallAnimation__png,"waterfallAnimation__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::waterfallImage__png,"waterfallImage__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::waterFallTile__png,"waterFallTile__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::weedA__png,"weedA__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::weedB__png,"weedB__png");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::BeppleSongONe__wav,"BeppleSongONe__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::BeppleSongOneOGG__ogg,"BeppleSongOneOGG__ogg");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::music_goes_here__txt,"music_goes_here__txt");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::alertButtonSound__wav,"alertButtonSound__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::buySignSound__wav,"buySignSound__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::calcButtonSound__wav,"calcButtonSound__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::cashRegisterSound__wav,"cashRegisterSound__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::coinsSound__wav,"coinsSound__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::correctAnswerSoundA__wav,"correctAnswerSoundA__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::fallenAppleSound__wav,"fallenAppleSound__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::fillingJuiceBottleSound__wav,"fillingJuiceBottleSound__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::greenHouseWindChimeSound__wav,"greenHouseWindChimeSound__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::growingTree__wav,"growingTree__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::growingTreeA__wav,"growingTreeA__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::growingTreeB__wav,"growingTreeB__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::growingTreeC__wav,"growingTreeC__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::growingTreeD__wav,"growingTreeD__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::incorrectAnswerSound__wav,"incorrectAnswerSound__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::juiceMachineSound__wav,"juiceMachineSound__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::koiClicked0__wav,"koiClicked0__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::koiClicked1__wav,"koiClicked1__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::koiClicked2__wav,"koiClicked2__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::koiClicked4__wav,"koiClicked4__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::koiPondPlacedSound__wav,"koiPondPlacedSound__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::koiPondSound__wav,"koiPondSound__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::marketSlideClosedSound__wav,"marketSlideClosedSound__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::marketSlideOpenSound__wav,"marketSlideOpenSound__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::mouseHoleSound__wav,"mouseHoleSound__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::mouseWalkingSound__wav,"mouseWalkingSound__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::pickedApplesGround__wav,"pickedApplesGround__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::pickedLeavesSound__wav,"pickedLeavesSound__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::pieBakedSound__wav,"pieBakedSound__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::placedMachineSound__wav,"placedMachineSound__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::plantingSeedSound__wav,"plantingSeedSound__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::pouringJuiceSound__wav,"pouringJuiceSound__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::raining__wav,"raining__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::sounds_go_here__txt,"sounds_go_here__txt");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::sprinklerButtonSound__wav,"sprinklerButtonSound__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::sprinklerSound__wav,"sprinklerSound__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::sunshine__wav,"sunshine__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::truckButtonSound__wav,"truckButtonSound__wav");
	HX_MARK_MEMBER_NAME(AssetPaths_obj::truckSound__wav,"truckSound__wav");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::data_goes_here__txt,"data_goes_here__txt");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::animatedAppleA__png,"animatedAppleA__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::animatedAppleB__png,"animatedAppleB__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::answerButton__png,"answerButton__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::appleJuiceItemImage__png,"appleJuiceItemImage__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::appleRotAnimation__png,"appleRotAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::appleShedAnimation__png,"appleShedAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::appleTruck__png,"appleTruck__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::appleTruckButtonOnAnimation__png,"appleTruckButtonOnAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::backGroundMassA__png,"backGroundMassA__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::backGroundMassAmaster__png,"backGroundMassAmaster__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::backGroundMassB__png,"backGroundMassB__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::backGroundMassBmaster__png,"backGroundMassBmaster__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::backGroundMassC__png,"backGroundMassC__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::backGroundMassCmaster__png,"backGroundMassCmaster__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::backGroundMassD__png,"backGroundMassD__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::backGroundMassDmaster__png,"backGroundMassDmaster__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::backGroundMassE__png,"backGroundMassE__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::backGroundMassEmaster__png,"backGroundMassEmaster__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::backGroundMassF__png,"backGroundMassF__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::backGroundMassFmaster__png,"backGroundMassFmaster__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::backGroundMassFMouse__png,"backGroundMassFMouse__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::backGroundMassG__png,"backGroundMassG__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::backGroundMassGmaster__png,"backGroundMassGmaster__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::backGroundMassH__png,"backGroundMassH__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::backGroundMassHmaster__png,"backGroundMassHmaster__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::basketAnimation__png,"basketAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::blackBoard__png,"blackBoard__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::blackBoardCorrectBorder__png,"blackBoardCorrectBorder__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::blackBoardWrongBorder__png,"blackBoardWrongBorder__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::boilerImage__png,"boilerImage__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::bridgeImage__png,"bridgeImage__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::buildBar__png,"buildBar__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::buildBarShaft__png,"buildBarShaft__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::buildBarTab__png,"buildBarTab__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::buttonClear__png,"buttonClear__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::buttonEight__png,"buttonEight__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::buttonFive__png,"buttonFive__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::buttonFour__png,"buttonFour__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::buttonNine__png,"buttonNine__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::buttonOne__png,"buttonOne__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::buttonSeven__png,"buttonSeven__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::buttonSix__png,"buttonSix__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::buttonThree__png,"buttonThree__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::buttonTwo__png,"buttonTwo__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::buttonZero__png,"buttonZero__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::buttonZeroShadow__png,"buttonZeroShadow__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::buyLandButton__png,"buyLandButton__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::calcImage__png,"calcImage__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::chalkEquals__png,"chalkEquals__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::chalkNumbers_0__png,"chalkNumbers_0__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::chalkNumbers_1__png,"chalkNumbers_1__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::chalkNumbers_2__png,"chalkNumbers_2__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::chalkNumbers_3__png,"chalkNumbers_3__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::chalkNumbers_4__png,"chalkNumbers_4__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::chalkNumbers_5__png,"chalkNumbers_5__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::chalkNumbers_6__png,"chalkNumbers_6__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::chalkNumbers_7__png,"chalkNumbers_7__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::chalkNumbers_8__png,"chalkNumbers_8__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::chalkNumbers_9__png,"chalkNumbers_9__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::cloud_blue__png,"cloud_blue__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::coyPondImage__png,"coyPondImage__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::dropletsAnimation__png,"dropletsAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::dropletsAnimationS__png,"dropletsAnimationS__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::dropletsAnimationT__png,"dropletsAnimationT__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::dropletsAnimationU__png,"dropletsAnimationU__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::fillingBottleAnimation__png,"fillingBottleAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::fishIcon__png,"fishIcon__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::fishItemAnimation__png,"fishItemAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::gameTile__png,"gameTile__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::gameTileBoarder__png,"gameTileBoarder__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::gameTileOwned__png,"gameTileOwned__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::goldCoin__png,"goldCoin__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::greenHouseAnimation__png,"greenHouseAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::greenHouseIconImage__png,"greenHouseIconImage__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::GreenHouseItemAnimation__png,"GreenHouseItemAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::greenHouseShrub__png,"greenHouseShrub__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::greenHouseTree__png,"greenHouseTree__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::greenHouseTreeAnimation__png,"greenHouseTreeAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::holeAnimation__png,"holeAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::images_go_here__txt,"images_go_here__txt");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::islandMouseAnimation__png,"islandMouseAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::juiceMachineAnimation__png,"juiceMachineAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::juiceMachineIcon__png,"juiceMachineIcon__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::juiceMachineSwitchAnimation__png,"juiceMachineSwitchAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::koiFishAnimation__png,"koiFishAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::koiFishItemDrag__png,"koiFishItemDrag__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::koiPad__png,"koiPad__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::koiPondAnimation__png,"koiPondAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::koiPondImage__png,"koiPondImage__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::koiRock__png,"koiRock__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::koiSand__png,"koiSand__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::marketSideBar__png,"marketSideBar__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::marketSideBarShaft__png,"marketSideBarShaft__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::marketSideBarTab__png,"marketSideBarTab__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::market_seed__png,"market_seed__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::meteorField__png,"meteorField__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::meteorFieldMaster__png,"meteorFieldMaster__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::minusOneImage__png,"minusOneImage__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::minusOperation__png,"minusOperation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::mouseAnimation__png,"mouseAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::multiOperation__png,"multiOperation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::optionsScreen__png,"optionsScreen__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::packOfJuiceIcon__png,"packOfJuiceIcon__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::packOfPiesIcon__png,"packOfPiesIcon__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::pickedAppleLeaves__png,"pickedAppleLeaves__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::pieIcon__png,"pieIcon__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::pieMachineAnimation__png,"pieMachineAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::pieMachineItem__png,"pieMachineItem__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::plusOperation__png,"plusOperation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::rainCloudAnimation__png,"rainCloudAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::rainCloudAnimationA__png,"rainCloudAnimationA__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::repairAnimationS__png,"repairAnimationS__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::rockyForeground__png,"rockyForeground__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::rollingMoon__png,"rollingMoon__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::seedItemAnimation__png,"seedItemAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::sidewaysIsland__png,"sidewaysIsland__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::spaceBackground__png,"spaceBackground__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::sprinklerAlertAnimation__png,"sprinklerAlertAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::sprinklerAlertAnimationS__png,"sprinklerAlertAnimationS__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::sprinklerAnimation__png,"sprinklerAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::sprinklerSwtichAnimation__png,"sprinklerSwtichAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::sunAnimationA__png,"sunAnimationA__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::sunAnimationB__png,"sunAnimationB__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::sunAnimationC__png,"sunAnimationC__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::sunTokenAnimation__png,"sunTokenAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::systemStar__png,"systemStar__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::systemStarAnimation__png,"systemStarAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::tileAnimation__png,"tileAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::tileAnimationTEST__png,"tileAnimationTEST__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::tileSprite__png,"tileSprite__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::titleBackground__png,"titleBackground__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::titleEasy__png,"titleEasy__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::titleImage__png,"titleImage__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::titleImageProcessed__png,"titleImageProcessed__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::titleScreenGrass__png,"titleScreenGrass__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::treeAnim0__png,"treeAnim0__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::treeAnim1__png,"treeAnim1__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::treeAnim2__png,"treeAnim2__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::treeAnim3__png,"treeAnim3__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::treeAnim4__png,"treeAnim4__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::treeAnim5__png,"treeAnim5__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::treeAnim6__png,"treeAnim6__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::treeAnim7__png,"treeAnim7__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::treeAnimation__png,"treeAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::treeAnimationA__png,"treeAnimationA__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::treeAnimationB__png,"treeAnimationB__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::treeDeathAnimation__png,"treeDeathAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::treeDeathAnimation97__png,"treeDeathAnimation97__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::treeDeathAnimation98__png,"treeDeathAnimation98__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::treeDeathAnimation99__png,"treeDeathAnimation99__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::treeSpriteA__png,"treeSpriteA__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::treeSpriteA99__png,"treeSpriteA99__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::tree_seed__png,"tree_seed__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::truckBellAnimation__png,"truckBellAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::truckBellImage__png,"truckBellImage__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::tutCheckBoxAnimate__png,"tutCheckBoxAnimate__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::tutHand__png,"tutHand__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::tutHandAnimation__png,"tutHandAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::tutMessageEight__png,"tutMessageEight__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::tutMessageFive__png,"tutMessageFive__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::tutMessageFour__png,"tutMessageFour__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::tutMessageNine__png,"tutMessageNine__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::tutMessageOne__png,"tutMessageOne__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::tutMessageSeven__png,"tutMessageSeven__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::tutMessageSix__png,"tutMessageSix__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::tutMessageTen__png,"tutMessageTen__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::tutMessageThree__png,"tutMessageThree__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::tutMessageTwo__png,"tutMessageTwo__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::upsideDownIsland__png,"upsideDownIsland__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::waterfallAnimation__png,"waterfallAnimation__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::waterfallImage__png,"waterfallImage__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::waterFallTile__png,"waterFallTile__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::weedA__png,"weedA__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::weedB__png,"weedB__png");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::BeppleSongONe__wav,"BeppleSongONe__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::BeppleSongOneOGG__ogg,"BeppleSongOneOGG__ogg");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::music_goes_here__txt,"music_goes_here__txt");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::alertButtonSound__wav,"alertButtonSound__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::buySignSound__wav,"buySignSound__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::calcButtonSound__wav,"calcButtonSound__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::cashRegisterSound__wav,"cashRegisterSound__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::coinsSound__wav,"coinsSound__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::correctAnswerSoundA__wav,"correctAnswerSoundA__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::fallenAppleSound__wav,"fallenAppleSound__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::fillingJuiceBottleSound__wav,"fillingJuiceBottleSound__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::greenHouseWindChimeSound__wav,"greenHouseWindChimeSound__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::growingTree__wav,"growingTree__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::growingTreeA__wav,"growingTreeA__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::growingTreeB__wav,"growingTreeB__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::growingTreeC__wav,"growingTreeC__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::growingTreeD__wav,"growingTreeD__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::incorrectAnswerSound__wav,"incorrectAnswerSound__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::juiceMachineSound__wav,"juiceMachineSound__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::koiClicked0__wav,"koiClicked0__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::koiClicked1__wav,"koiClicked1__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::koiClicked2__wav,"koiClicked2__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::koiClicked4__wav,"koiClicked4__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::koiPondPlacedSound__wav,"koiPondPlacedSound__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::koiPondSound__wav,"koiPondSound__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::marketSlideClosedSound__wav,"marketSlideClosedSound__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::marketSlideOpenSound__wav,"marketSlideOpenSound__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::mouseHoleSound__wav,"mouseHoleSound__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::mouseWalkingSound__wav,"mouseWalkingSound__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::pickedApplesGround__wav,"pickedApplesGround__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::pickedLeavesSound__wav,"pickedLeavesSound__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::pieBakedSound__wav,"pieBakedSound__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::placedMachineSound__wav,"placedMachineSound__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::plantingSeedSound__wav,"plantingSeedSound__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::pouringJuiceSound__wav,"pouringJuiceSound__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::raining__wav,"raining__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::sounds_go_here__txt,"sounds_go_here__txt");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::sprinklerButtonSound__wav,"sprinklerButtonSound__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::sprinklerSound__wav,"sprinklerSound__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::sunshine__wav,"sunshine__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::truckButtonSound__wav,"truckButtonSound__wav");
	HX_VISIT_MEMBER_NAME(AssetPaths_obj::truckSound__wav,"truckSound__wav");
};

#endif

Class AssetPaths_obj::__mClass;

void AssetPaths_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("AssetPaths"), hx::TCanCast< AssetPaths_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void AssetPaths_obj::__boot()
{
	data_goes_here__txt= HX_CSTRING("assets/data/data-goes-here.txt");
	animatedAppleA__png= HX_CSTRING("assets/images/animatedAppleA.png");
	animatedAppleB__png= HX_CSTRING("assets/images/animatedAppleB.png");
	answerButton__png= HX_CSTRING("assets/images/answerButton.png");
	appleJuiceItemImage__png= HX_CSTRING("assets/images/appleJuiceItemImage.png");
	appleRotAnimation__png= HX_CSTRING("assets/images/appleRotAnimation.png");
	appleShedAnimation__png= HX_CSTRING("assets/images/appleShedAnimation.png");
	appleTruck__png= HX_CSTRING("assets/images/appleTruck.png");
	appleTruckButtonOnAnimation__png= HX_CSTRING("assets/images/appleTruckButtonOnAnimation.png");
	backGroundMassA__png= HX_CSTRING("assets/images/backGroundMassA.png");
	backGroundMassAmaster__png= HX_CSTRING("assets/images/backGroundMassAmaster.png");
	backGroundMassB__png= HX_CSTRING("assets/images/backGroundMassB.png");
	backGroundMassBmaster__png= HX_CSTRING("assets/images/backGroundMassBmaster.png");
	backGroundMassC__png= HX_CSTRING("assets/images/backGroundMassC.png");
	backGroundMassCmaster__png= HX_CSTRING("assets/images/backGroundMassCmaster.png");
	backGroundMassD__png= HX_CSTRING("assets/images/backGroundMassD.png");
	backGroundMassDmaster__png= HX_CSTRING("assets/images/backGroundMassDmaster.png");
	backGroundMassE__png= HX_CSTRING("assets/images/backGroundMassE.png");
	backGroundMassEmaster__png= HX_CSTRING("assets/images/backGroundMassEmaster.png");
	backGroundMassF__png= HX_CSTRING("assets/images/backGroundMassF.png");
	backGroundMassFmaster__png= HX_CSTRING("assets/images/backGroundMassFmaster.png");
	backGroundMassFMouse__png= HX_CSTRING("assets/images/backGroundMassFMouse.png");
	backGroundMassG__png= HX_CSTRING("assets/images/backGroundMassG.png");
	backGroundMassGmaster__png= HX_CSTRING("assets/images/backGroundMassGmaster.png");
	backGroundMassH__png= HX_CSTRING("assets/images/backGroundMassH.png");
	backGroundMassHmaster__png= HX_CSTRING("assets/images/backGroundMassHmaster.png");
	basketAnimation__png= HX_CSTRING("assets/images/basketAnimation.png");
	blackBoard__png= HX_CSTRING("assets/images/blackBoard.png");
	blackBoardCorrectBorder__png= HX_CSTRING("assets/images/blackBoardCorrectBorder.png");
	blackBoardWrongBorder__png= HX_CSTRING("assets/images/blackBoardWrongBorder.png");
	boilerImage__png= HX_CSTRING("assets/images/boilerImage.png");
	bridgeImage__png= HX_CSTRING("assets/images/bridgeImage.png");
	buildBar__png= HX_CSTRING("assets/images/buildBar.png");
	buildBarShaft__png= HX_CSTRING("assets/images/buildBarShaft.png");
	buildBarTab__png= HX_CSTRING("assets/images/buildBarTab.png");
	buttonClear__png= HX_CSTRING("assets/images/buttonClear.png");
	buttonEight__png= HX_CSTRING("assets/images/buttonEight.png");
	buttonFive__png= HX_CSTRING("assets/images/buttonFive.png");
	buttonFour__png= HX_CSTRING("assets/images/buttonFour.png");
	buttonNine__png= HX_CSTRING("assets/images/buttonNine.png");
	buttonOne__png= HX_CSTRING("assets/images/buttonOne.png");
	buttonSeven__png= HX_CSTRING("assets/images/buttonSeven.png");
	buttonSix__png= HX_CSTRING("assets/images/buttonSix.png");
	buttonThree__png= HX_CSTRING("assets/images/buttonThree.png");
	buttonTwo__png= HX_CSTRING("assets/images/buttonTwo.png");
	buttonZero__png= HX_CSTRING("assets/images/buttonZero.png");
	buttonZeroShadow__png= HX_CSTRING("assets/images/buttonZeroShadow.png");
	buyLandButton__png= HX_CSTRING("assets/images/buyLandButton.png");
	calcImage__png= HX_CSTRING("assets/images/calcImage.png");
	chalkEquals__png= HX_CSTRING("assets/images/chalkEquals.png");
	chalkNumbers_0__png= HX_CSTRING("assets/images/chalkNumbers_0.png");
	chalkNumbers_1__png= HX_CSTRING("assets/images/chalkNumbers_1.png");
	chalkNumbers_2__png= HX_CSTRING("assets/images/chalkNumbers_2.png");
	chalkNumbers_3__png= HX_CSTRING("assets/images/chalkNumbers_3.png");
	chalkNumbers_4__png= HX_CSTRING("assets/images/chalkNumbers_4.png");
	chalkNumbers_5__png= HX_CSTRING("assets/images/chalkNumbers_5.png");
	chalkNumbers_6__png= HX_CSTRING("assets/images/chalkNumbers_6.png");
	chalkNumbers_7__png= HX_CSTRING("assets/images/chalkNumbers_7.png");
	chalkNumbers_8__png= HX_CSTRING("assets/images/chalkNumbers_8.png");
	chalkNumbers_9__png= HX_CSTRING("assets/images/chalkNumbers_9.png");
	cloud_blue__png= HX_CSTRING("assets/images/cloud_blue.png");
	coyPondImage__png= HX_CSTRING("assets/images/coyPondImage.png");
	dropletsAnimation__png= HX_CSTRING("assets/images/dropletsAnimation.png");
	dropletsAnimationS__png= HX_CSTRING("assets/images/dropletsAnimationS.png");
	dropletsAnimationT__png= HX_CSTRING("assets/images/dropletsAnimationT.png");
	dropletsAnimationU__png= HX_CSTRING("assets/images/dropletsAnimationU.png");
	fillingBottleAnimation__png= HX_CSTRING("assets/images/fillingBottleAnimation.png");
	fishIcon__png= HX_CSTRING("assets/images/fishIcon.png");
	fishItemAnimation__png= HX_CSTRING("assets/images/fishItemAnimation.png");
	gameTile__png= HX_CSTRING("assets/images/gameTile.png");
	gameTileBoarder__png= HX_CSTRING("assets/images/gameTileBoarder.png");
	gameTileOwned__png= HX_CSTRING("assets/images/gameTileOwned.png");
	goldCoin__png= HX_CSTRING("assets/images/goldCoin.png");
	greenHouseAnimation__png= HX_CSTRING("assets/images/greenHouseAnimation.png");
	greenHouseIconImage__png= HX_CSTRING("assets/images/greenHouseIconImage.png");
	GreenHouseItemAnimation__png= HX_CSTRING("assets/images/GreenHouseItemAnimation.png");
	greenHouseShrub__png= HX_CSTRING("assets/images/greenHouseShrub.png");
	greenHouseTree__png= HX_CSTRING("assets/images/greenHouseTree.png");
	greenHouseTreeAnimation__png= HX_CSTRING("assets/images/greenHouseTreeAnimation.png");
	holeAnimation__png= HX_CSTRING("assets/images/holeAnimation.png");
	images_go_here__txt= HX_CSTRING("assets/images/images-go-here.txt");
	islandMouseAnimation__png= HX_CSTRING("assets/images/islandMouseAnimation.png");
	juiceMachineAnimation__png= HX_CSTRING("assets/images/juiceMachineAnimation.png");
	juiceMachineIcon__png= HX_CSTRING("assets/images/juiceMachineIcon.png");
	juiceMachineSwitchAnimation__png= HX_CSTRING("assets/images/juiceMachineSwitchAnimation.png");
	koiFishAnimation__png= HX_CSTRING("assets/images/koiFishAnimation.png");
	koiFishItemDrag__png= HX_CSTRING("assets/images/koiFishItemDrag.png");
	koiPad__png= HX_CSTRING("assets/images/koiPad.png");
	koiPondAnimation__png= HX_CSTRING("assets/images/koiPondAnimation.png");
	koiPondImage__png= HX_CSTRING("assets/images/koiPondImage.png");
	koiRock__png= HX_CSTRING("assets/images/koiRock.png");
	koiSand__png= HX_CSTRING("assets/images/koiSand.png");
	marketSideBar__png= HX_CSTRING("assets/images/marketSideBar.png");
	marketSideBarShaft__png= HX_CSTRING("assets/images/marketSideBarShaft.png");
	marketSideBarTab__png= HX_CSTRING("assets/images/marketSideBarTab.png");
	market_seed__png= HX_CSTRING("assets/images/market_seed.png");
	meteorField__png= HX_CSTRING("assets/images/meteorField.png");
	meteorFieldMaster__png= HX_CSTRING("assets/images/meteorFieldMaster.png");
	minusOneImage__png= HX_CSTRING("assets/images/minusOneImage.png");
	minusOperation__png= HX_CSTRING("assets/images/minusOperation.png");
	mouseAnimation__png= HX_CSTRING("assets/images/mouseAnimation.png");
	multiOperation__png= HX_CSTRING("assets/images/multiOperation.png");
	optionsScreen__png= HX_CSTRING("assets/images/optionsScreen.png");
	packOfJuiceIcon__png= HX_CSTRING("assets/images/packOfJuiceIcon.png");
	packOfPiesIcon__png= HX_CSTRING("assets/images/packOfPiesIcon.png");
	pickedAppleLeaves__png= HX_CSTRING("assets/images/pickedAppleLeaves.png");
	pieIcon__png= HX_CSTRING("assets/images/pieIcon.png");
	pieMachineAnimation__png= HX_CSTRING("assets/images/pieMachineAnimation.png");
	pieMachineItem__png= HX_CSTRING("assets/images/pieMachineItem.png");
	plusOperation__png= HX_CSTRING("assets/images/plusOperation.png");
	rainCloudAnimation__png= HX_CSTRING("assets/images/rainCloudAnimation.png");
	rainCloudAnimationA__png= HX_CSTRING("assets/images/rainCloudAnimationA.png");
	repairAnimationS__png= HX_CSTRING("assets/images/repairAnimationS.png");
	rockyForeground__png= HX_CSTRING("assets/images/rockyForeground.png");
	rollingMoon__png= HX_CSTRING("assets/images/rollingMoon.png");
	seedItemAnimation__png= HX_CSTRING("assets/images/seedItemAnimation.png");
	sidewaysIsland__png= HX_CSTRING("assets/images/sidewaysIsland.png");
	spaceBackground__png= HX_CSTRING("assets/images/spaceBackground.png");
	sprinklerAlertAnimation__png= HX_CSTRING("assets/images/sprinklerAlertAnimation.png");
	sprinklerAlertAnimationS__png= HX_CSTRING("assets/images/sprinklerAlertAnimationS.png");
	sprinklerAnimation__png= HX_CSTRING("assets/images/sprinklerAnimation.png");
	sprinklerSwtichAnimation__png= HX_CSTRING("assets/images/sprinklerSwtichAnimation.png");
	sunAnimationA__png= HX_CSTRING("assets/images/sunAnimationA.png");
	sunAnimationB__png= HX_CSTRING("assets/images/sunAnimationB.png");
	sunAnimationC__png= HX_CSTRING("assets/images/sunAnimationC.png");
	sunTokenAnimation__png= HX_CSTRING("assets/images/sunTokenAnimation.png");
	systemStar__png= HX_CSTRING("assets/images/systemStar.png");
	systemStarAnimation__png= HX_CSTRING("assets/images/systemStarAnimation.png");
	tileAnimation__png= HX_CSTRING("assets/images/tileAnimation.png");
	tileAnimationTEST__png= HX_CSTRING("assets/images/tileAnimationTEST.png");
	tileSprite__png= HX_CSTRING("assets/images/tileSprite.png");
	titleBackground__png= HX_CSTRING("assets/images/titleBackground.png");
	titleEasy__png= HX_CSTRING("assets/images/titleEasy.png");
	titleImage__png= HX_CSTRING("assets/images/titleImage.png");
	titleImageProcessed__png= HX_CSTRING("assets/images/titleImageProcessed.png");
	titleScreenGrass__png= HX_CSTRING("assets/images/titleScreenGrass.png");
	treeAnim0__png= HX_CSTRING("assets/images/treeAnim0.png");
	treeAnim1__png= HX_CSTRING("assets/images/treeAnim1.png");
	treeAnim2__png= HX_CSTRING("assets/images/treeAnim2.png");
	treeAnim3__png= HX_CSTRING("assets/images/treeAnim3.png");
	treeAnim4__png= HX_CSTRING("assets/images/treeAnim4.png");
	treeAnim5__png= HX_CSTRING("assets/images/treeAnim5.png");
	treeAnim6__png= HX_CSTRING("assets/images/treeAnim6.png");
	treeAnim7__png= HX_CSTRING("assets/images/treeAnim7.png");
	treeAnimation__png= HX_CSTRING("assets/images/treeAnimation.png");
	treeAnimationA__png= HX_CSTRING("assets/images/treeAnimationA.png");
	treeAnimationB__png= HX_CSTRING("assets/images/treeAnimationB.png");
	treeDeathAnimation__png= HX_CSTRING("assets/images/treeDeathAnimation.png");
	treeDeathAnimation97__png= HX_CSTRING("assets/images/treeDeathAnimation97.png");
	treeDeathAnimation98__png= HX_CSTRING("assets/images/treeDeathAnimation98.png");
	treeDeathAnimation99__png= HX_CSTRING("assets/images/treeDeathAnimation99.png");
	treeSpriteA__png= HX_CSTRING("assets/images/treeSpriteA.png");
	treeSpriteA99__png= HX_CSTRING("assets/images/treeSpriteA99.png");
	tree_seed__png= HX_CSTRING("assets/images/tree_seed.png");
	truckBellAnimation__png= HX_CSTRING("assets/images/truckBellAnimation.png");
	truckBellImage__png= HX_CSTRING("assets/images/truckBellImage.png");
	tutCheckBoxAnimate__png= HX_CSTRING("assets/images/tutCheckBoxAnimate.png");
	tutHand__png= HX_CSTRING("assets/images/tutHand.png");
	tutHandAnimation__png= HX_CSTRING("assets/images/tutHandAnimation.png");
	tutMessageEight__png= HX_CSTRING("assets/images/tutMessageEight.png");
	tutMessageFive__png= HX_CSTRING("assets/images/tutMessageFive.png");
	tutMessageFour__png= HX_CSTRING("assets/images/tutMessageFour.png");
	tutMessageNine__png= HX_CSTRING("assets/images/tutMessageNine.png");
	tutMessageOne__png= HX_CSTRING("assets/images/tutMessageOne.png");
	tutMessageSeven__png= HX_CSTRING("assets/images/tutMessageSeven.png");
	tutMessageSix__png= HX_CSTRING("assets/images/tutMessageSix.png");
	tutMessageTen__png= HX_CSTRING("assets/images/tutMessageTen.png");
	tutMessageThree__png= HX_CSTRING("assets/images/tutMessageThree.png");
	tutMessageTwo__png= HX_CSTRING("assets/images/tutMessageTwo.png");
	upsideDownIsland__png= HX_CSTRING("assets/images/upsideDownIsland.png");
	waterfallAnimation__png= HX_CSTRING("assets/images/waterfallAnimation.png");
	waterfallImage__png= HX_CSTRING("assets/images/waterfallImage.png");
	waterFallTile__png= HX_CSTRING("assets/images/waterFallTile.png");
	weedA__png= HX_CSTRING("assets/images/weedA.png");
	weedB__png= HX_CSTRING("assets/images/weedB.png");
	BeppleSongONe__wav= HX_CSTRING("assets/music/BeppleSongONe.wav");
	BeppleSongOneOGG__ogg= HX_CSTRING("assets/music/BeppleSongOneOGG.ogg");
	music_goes_here__txt= HX_CSTRING("assets/music/music-goes-here.txt");
	alertButtonSound__wav= HX_CSTRING("assets/sounds/alertButtonSound.wav");
	buySignSound__wav= HX_CSTRING("assets/sounds/buySignSound.wav");
	calcButtonSound__wav= HX_CSTRING("assets/sounds/calcButtonSound.wav");
	cashRegisterSound__wav= HX_CSTRING("assets/sounds/cashRegisterSound.wav");
	coinsSound__wav= HX_CSTRING("assets/sounds/coinsSound.wav");
	correctAnswerSoundA__wav= HX_CSTRING("assets/sounds/correctAnswerSoundA.wav");
	fallenAppleSound__wav= HX_CSTRING("assets/sounds/fallenAppleSound.wav");
	fillingJuiceBottleSound__wav= HX_CSTRING("assets/sounds/fillingJuiceBottleSound.wav");
	greenHouseWindChimeSound__wav= HX_CSTRING("assets/sounds/greenHouseWindChimeSound.wav");
	growingTree__wav= HX_CSTRING("assets/sounds/growingTree.wav");
	growingTreeA__wav= HX_CSTRING("assets/sounds/growingTreeA.wav");
	growingTreeB__wav= HX_CSTRING("assets/sounds/growingTreeB.wav");
	growingTreeC__wav= HX_CSTRING("assets/sounds/growingTreeC.wav");
	growingTreeD__wav= HX_CSTRING("assets/sounds/growingTreeD.wav");
	incorrectAnswerSound__wav= HX_CSTRING("assets/sounds/incorrectAnswerSound.wav");
	juiceMachineSound__wav= HX_CSTRING("assets/sounds/juiceMachineSound.wav");
	koiClicked0__wav= HX_CSTRING("assets/sounds/koiClicked0.wav");
	koiClicked1__wav= HX_CSTRING("assets/sounds/koiClicked1.wav");
	koiClicked2__wav= HX_CSTRING("assets/sounds/koiClicked2.wav");
	koiClicked4__wav= HX_CSTRING("assets/sounds/koiClicked4.wav");
	koiPondPlacedSound__wav= HX_CSTRING("assets/sounds/koiPondPlacedSound.wav");
	koiPondSound__wav= HX_CSTRING("assets/sounds/koiPondSound.wav");
	marketSlideClosedSound__wav= HX_CSTRING("assets/sounds/marketSlideClosedSound.wav");
	marketSlideOpenSound__wav= HX_CSTRING("assets/sounds/marketSlideOpenSound.wav");
	mouseHoleSound__wav= HX_CSTRING("assets/sounds/mouseHoleSound.wav");
	mouseWalkingSound__wav= HX_CSTRING("assets/sounds/mouseWalkingSound.wav");
	pickedApplesGround__wav= HX_CSTRING("assets/sounds/pickedApplesGround.wav");
	pickedLeavesSound__wav= HX_CSTRING("assets/sounds/pickedLeavesSound.wav");
	pieBakedSound__wav= HX_CSTRING("assets/sounds/pieBakedSound.wav");
	placedMachineSound__wav= HX_CSTRING("assets/sounds/placedMachineSound.wav");
	plantingSeedSound__wav= HX_CSTRING("assets/sounds/plantingSeedSound.wav");
	pouringJuiceSound__wav= HX_CSTRING("assets/sounds/pouringJuiceSound.wav");
	raining__wav= HX_CSTRING("assets/sounds/raining.wav");
	sounds_go_here__txt= HX_CSTRING("assets/sounds/sounds-go-here.txt");
	sprinklerButtonSound__wav= HX_CSTRING("assets/sounds/sprinklerButtonSound.wav");
	sprinklerSound__wav= HX_CSTRING("assets/sounds/sprinklerSound.wav");
	sunshine__wav= HX_CSTRING("assets/sounds/sunshine.wav");
	truckButtonSound__wav= HX_CSTRING("assets/sounds/truckButtonSound.wav");
	truckSound__wav= HX_CSTRING("assets/sounds/truckSound.wav");
}

