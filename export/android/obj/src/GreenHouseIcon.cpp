#include <hxcpp.h>

#ifndef INCLUDED_GreenHouseIcon
#include <GreenHouseIcon.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_input_mouse_FlxMouse
#include <flixel/input/mouse/FlxMouse.h>
#endif
#ifndef INCLUDED_flixel_input_mouse_FlxMouseButton
#include <flixel/input/mouse/FlxMouseButton.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxInput
#include <flixel/interfaces/IFlxInput.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxPooled
#include <flixel/interfaces/IFlxPooled.h>
#endif
#ifndef INCLUDED_flixel_plugin_FlxPlugin
#include <flixel/plugin/FlxPlugin.h>
#endif
#ifndef INCLUDED_flixel_plugin_MouseEventManager
#include <flixel/plugin/MouseEventManager.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPoint
#include <flixel/util/FlxPoint.h>
#endif

Void GreenHouseIcon_obj::__construct(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y,bool dragType)
{
HX_STACK_FRAME("GreenHouseIcon","new",0x4923ec08,"GreenHouseIcon.new","GreenHouseIcon.hx",11,0xca5f6c68)
HX_STACK_THIS(this)
HX_STACK_ARG(__o_X,"X")
HX_STACK_ARG(__o_Y,"Y")
HX_STACK_ARG(dragType,"dragType")
Float X = __o_X.Default(0);
Float Y = __o_Y.Default(0);
{
	HX_STACK_LINE(18)
	this->homeY = (int)70;
	HX_STACK_LINE(17)
	this->homeX = (int)-121;
	HX_STACK_LINE(16)
	this->isMousedOver = true;
	HX_STACK_LINE(14)
	this->dragging = false;
	HX_STACK_LINE(22)
	super::__construct(X,Y,null());
	HX_STACK_LINE(23)
	this->dragType = dragType;
	HX_STACK_LINE(24)
	this->loadGraphic(HX_CSTRING("assets/images/greenHouseIconImage.png"),false,(int)12,(int)20,null(),null());
	HX_STACK_LINE(25)
	this->set_x(this->homeX);
	HX_STACK_LINE(26)
	this->set_y(this->homeY);
	HX_STACK_LINE(27)
	if ((dragType)){
		HX_STACK_LINE(28)
		this->homeX = (int)44;
		HX_STACK_LINE(29)
		this->homeY = (int)70;
	}
	HX_STACK_LINE(31)
	::flixel::plugin::MouseEventManager_obj::add(hx::ObjectPtr<OBJ_>(this),this->onDown_dyn(),this->onUp_dyn(),null(),null(),null(),null(),null());
}
;
	return null();
}

//GreenHouseIcon_obj::~GreenHouseIcon_obj() { }

Dynamic GreenHouseIcon_obj::__CreateEmpty() { return  new GreenHouseIcon_obj; }
hx::ObjectPtr< GreenHouseIcon_obj > GreenHouseIcon_obj::__new(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y,bool dragType)
{  hx::ObjectPtr< GreenHouseIcon_obj > result = new GreenHouseIcon_obj();
	result->__construct(__o_X,__o_Y,dragType);
	return result;}

Dynamic GreenHouseIcon_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< GreenHouseIcon_obj > result = new GreenHouseIcon_obj();
	result->__construct(inArgs[0],inArgs[1],inArgs[2]);
	return result;}

Void GreenHouseIcon_obj::onDown( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("GreenHouseIcon","onDown",0x45f69e79,"GreenHouseIcon.onDown","GreenHouseIcon.hx",37,0xca5f6c68)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(37)
		this->dragging = true;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(GreenHouseIcon_obj,onDown,(void))

Void GreenHouseIcon_obj::onUp( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("GreenHouseIcon","onUp",0xb6fa88b2,"GreenHouseIcon.onUp","GreenHouseIcon.hx",41,0xca5f6c68)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(42)
		this->dragging = false;
		HX_STACK_LINE(43)
		this->resetGreenHouseImagePos();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(GreenHouseIcon_obj,onUp,(void))

Void GreenHouseIcon_obj::dragGreenHouseImage( bool temp){
{
		HX_STACK_FRAME("GreenHouseIcon","dragGreenHouseImage",0x64cc5a52,"GreenHouseIcon.dragGreenHouseImage","GreenHouseIcon.hx",49,0xca5f6c68)
		HX_STACK_THIS(this)
		HX_STACK_ARG(temp,"temp")
		HX_STACK_LINE(49)
		if ((temp)){
			HX_STACK_LINE(51)
			this->set_x((::flixel::FlxG_obj::mouse->x - (int)10));
			HX_STACK_LINE(52)
			this->set_y((::flixel::FlxG_obj::mouse->y - (int)10));
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(GreenHouseIcon_obj,dragGreenHouseImage,(void))

Void GreenHouseIcon_obj::resetGreenHouseImagePos( ){
{
		HX_STACK_FRAME("GreenHouseIcon","resetGreenHouseImagePos",0xb2ae3d4d,"GreenHouseIcon.resetGreenHouseImagePos","GreenHouseIcon.hx",59,0xca5f6c68)
		HX_STACK_THIS(this)
		HX_STACK_LINE(59)
		if ((this->dragType)){
			HX_STACK_LINE(60)
			this->set_x(this->homeX);
			HX_STACK_LINE(61)
			this->set_y(this->homeY);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(GreenHouseIcon_obj,resetGreenHouseImagePos,(void))

bool GreenHouseIcon_obj::getDragging( ){
	HX_STACK_FRAME("GreenHouseIcon","getDragging",0xdad0032d,"GreenHouseIcon.getDragging","GreenHouseIcon.hx",67,0xca5f6c68)
	HX_STACK_THIS(this)
	HX_STACK_LINE(67)
	return this->dragging;
}


HX_DEFINE_DYNAMIC_FUNC0(GreenHouseIcon_obj,getDragging,return )

Void GreenHouseIcon_obj::setDragging( bool val){
{
		HX_STACK_FRAME("GreenHouseIcon","setDragging",0xe53d0a39,"GreenHouseIcon.setDragging","GreenHouseIcon.hx",73,0xca5f6c68)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(73)
		this->dragging = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(GreenHouseIcon_obj,setDragging,(void))

Void GreenHouseIcon_obj::checkMouseState( ){
{
		HX_STACK_FRAME("GreenHouseIcon","checkMouseState",0x6f90c83c,"GreenHouseIcon.checkMouseState","GreenHouseIcon.hx",79,0xca5f6c68)
		HX_STACK_THIS(this)
		struct _Function_1_1{
			inline static bool Block( ){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","GreenHouseIcon.hx",79,0xca5f6c68)
				{
					HX_STACK_LINE(79)
					::flixel::input::mouse::FlxMouseButton _this = ::flixel::FlxG_obj::mouse->_leftButton;		HX_STACK_VAR(_this,"_this");
					HX_STACK_LINE(79)
					return (bool((_this->current == (int)-1)) || bool((_this->current == (int)-2)));
				}
				return null();
			}
		};
		HX_STACK_LINE(79)
		if ((_Function_1_1::Block())){
			HX_STACK_LINE(80)
			this->dragging = false;
			HX_STACK_LINE(81)
			this->resetGreenHouseImagePos();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(GreenHouseIcon_obj,checkMouseState,(void))

Void GreenHouseIcon_obj::unregisterGreenHouseImageEvent( ){
{
		HX_STACK_FRAME("GreenHouseIcon","unregisterGreenHouseImageEvent",0x81d63c30,"GreenHouseIcon.unregisterGreenHouseImageEvent","GreenHouseIcon.hx",87,0xca5f6c68)
		HX_STACK_THIS(this)
		HX_STACK_LINE(87)
		::flixel::plugin::MouseEventManager_obj::remove(hx::ObjectPtr<OBJ_>(this));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(GreenHouseIcon_obj,unregisterGreenHouseImageEvent,(void))

Void GreenHouseIcon_obj::registerGreenHouseImageEvent( ){
{
		HX_STACK_FRAME("GreenHouseIcon","registerGreenHouseImageEvent",0xcbe5ac57,"GreenHouseIcon.registerGreenHouseImageEvent","GreenHouseIcon.hx",91,0xca5f6c68)
		HX_STACK_THIS(this)
		HX_STACK_LINE(91)
		::flixel::plugin::MouseEventManager_obj::add(hx::ObjectPtr<OBJ_>(this),this->onDown_dyn(),this->onUp_dyn(),null(),null(),null(),null(),null());
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(GreenHouseIcon_obj,registerGreenHouseImageEvent,(void))

Void GreenHouseIcon_obj::update( ){
{
		HX_STACK_FRAME("GreenHouseIcon","update",0xe8042361,"GreenHouseIcon.update","GreenHouseIcon.hx",94,0xca5f6c68)
		HX_STACK_THIS(this)
		HX_STACK_LINE(96)
		this->checkMouseState();
		HX_STACK_LINE(97)
		this->dragGreenHouseImage(this->dragging);
		HX_STACK_LINE(98)
		this->super::update();
	}
return null();
}


Void GreenHouseIcon_obj::destroy( ){
{
		HX_STACK_FRAME("GreenHouseIcon","destroy",0xa2513ca2,"GreenHouseIcon.destroy","GreenHouseIcon.hx",102,0xca5f6c68)
		HX_STACK_THIS(this)
		HX_STACK_LINE(104)
		::flixel::plugin::MouseEventManager_obj::remove(hx::ObjectPtr<OBJ_>(this));
		HX_STACK_LINE(105)
		this->super::destroy();
	}
return null();
}



GreenHouseIcon_obj::GreenHouseIcon_obj()
{
}

Dynamic GreenHouseIcon_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"onUp") ) { return onUp_dyn(); }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"homeX") ) { return homeX; }
		if (HX_FIELD_EQ(inName,"homeY") ) { return homeY; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"onDown") ) { return onDown_dyn(); }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"dragging") ) { return dragging; }
		if (HX_FIELD_EQ(inName,"dragType") ) { return dragType; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"getDragging") ) { return getDragging_dyn(); }
		if (HX_FIELD_EQ(inName,"setDragging") ) { return setDragging_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"isMousedOver") ) { return isMousedOver; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"checkMouseState") ) { return checkMouseState_dyn(); }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"dragGreenHouseImage") ) { return dragGreenHouseImage_dyn(); }
		break;
	case 23:
		if (HX_FIELD_EQ(inName,"resetGreenHouseImagePos") ) { return resetGreenHouseImagePos_dyn(); }
		break;
	case 28:
		if (HX_FIELD_EQ(inName,"registerGreenHouseImageEvent") ) { return registerGreenHouseImageEvent_dyn(); }
		break;
	case 30:
		if (HX_FIELD_EQ(inName,"unregisterGreenHouseImageEvent") ) { return unregisterGreenHouseImageEvent_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic GreenHouseIcon_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"homeX") ) { homeX=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"homeY") ) { homeY=inValue.Cast< int >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"dragging") ) { dragging=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"dragType") ) { dragType=inValue.Cast< bool >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"isMousedOver") ) { isMousedOver=inValue.Cast< bool >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void GreenHouseIcon_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("dragging"));
	outFields->push(HX_CSTRING("dragType"));
	outFields->push(HX_CSTRING("isMousedOver"));
	outFields->push(HX_CSTRING("homeX"));
	outFields->push(HX_CSTRING("homeY"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsBool,(int)offsetof(GreenHouseIcon_obj,dragging),HX_CSTRING("dragging")},
	{hx::fsBool,(int)offsetof(GreenHouseIcon_obj,dragType),HX_CSTRING("dragType")},
	{hx::fsBool,(int)offsetof(GreenHouseIcon_obj,isMousedOver),HX_CSTRING("isMousedOver")},
	{hx::fsInt,(int)offsetof(GreenHouseIcon_obj,homeX),HX_CSTRING("homeX")},
	{hx::fsInt,(int)offsetof(GreenHouseIcon_obj,homeY),HX_CSTRING("homeY")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("dragging"),
	HX_CSTRING("dragType"),
	HX_CSTRING("isMousedOver"),
	HX_CSTRING("homeX"),
	HX_CSTRING("homeY"),
	HX_CSTRING("onDown"),
	HX_CSTRING("onUp"),
	HX_CSTRING("dragGreenHouseImage"),
	HX_CSTRING("resetGreenHouseImagePos"),
	HX_CSTRING("getDragging"),
	HX_CSTRING("setDragging"),
	HX_CSTRING("checkMouseState"),
	HX_CSTRING("unregisterGreenHouseImageEvent"),
	HX_CSTRING("registerGreenHouseImageEvent"),
	HX_CSTRING("update"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(GreenHouseIcon_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(GreenHouseIcon_obj::__mClass,"__mClass");
};

#endif

Class GreenHouseIcon_obj::__mClass;

void GreenHouseIcon_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("GreenHouseIcon"), hx::TCanCast< GreenHouseIcon_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void GreenHouseIcon_obj::__boot()
{
}

