#include <hxcpp.h>

#ifndef INCLUDED_AppleShed
#include <AppleShed.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif

Void AppleShed_obj::__construct()
{
HX_STACK_FRAME("AppleShed","new",0xb8841280,"AppleShed.new","AppleShed.hx",10,0x3c85bcf0)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(15)
	this->maxShedLevel = (int)3;
	HX_STACK_LINE(20)
	super::__construct(null());
	HX_STACK_LINE(21)
	::flixel::group::FlxGroup _g = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(21)
	this->appleShedGroup = _g;
	HX_STACK_LINE(22)
	::flixel::FlxSprite _g1 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
	HX_STACK_LINE(22)
	this->appleShedSprite = _g1;
	HX_STACK_LINE(23)
	this->appleShedSprite->loadGraphic(HX_CSTRING("assets/images/appleShedAnimation.png"),true,(int)91,(int)70,null(),null());
	HX_STACK_LINE(24)
	this->appleShedSprite->animation->add(HX_CSTRING("shedEmpty"),Array_obj< int >::__new().Add((int)0),(int)1,false);
	HX_STACK_LINE(25)
	this->appleShedSprite->animation->add(HX_CSTRING("shedOne"),Array_obj< int >::__new().Add((int)1),(int)1,false);
	HX_STACK_LINE(26)
	this->appleShedSprite->animation->add(HX_CSTRING("shedTwo"),Array_obj< int >::__new().Add((int)2),(int)1,false);
	HX_STACK_LINE(27)
	this->appleShedSprite->animation->add(HX_CSTRING("shedThree"),Array_obj< int >::__new().Add((int)3),(int)1,false);
	HX_STACK_LINE(28)
	this->appleShedSprite->set_x((int)80);
	HX_STACK_LINE(29)
	this->appleShedSprite->set_y((int)220);
	HX_STACK_LINE(30)
	this->appleShedGroup->add(this->appleShedSprite);
	HX_STACK_LINE(31)
	this->add(this->appleShedGroup);
}
;
	return null();
}

//AppleShed_obj::~AppleShed_obj() { }

Dynamic AppleShed_obj::__CreateEmpty() { return  new AppleShed_obj; }
hx::ObjectPtr< AppleShed_obj > AppleShed_obj::__new()
{  hx::ObjectPtr< AppleShed_obj > result = new AppleShed_obj();
	result->__construct();
	return result;}

Dynamic AppleShed_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< AppleShed_obj > result = new AppleShed_obj();
	result->__construct();
	return result;}

Void AppleShed_obj::animateShed( int appleCount,int basketFull){
{
		HX_STACK_FRAME("AppleShed","animateShed",0xd8e09995,"AppleShed.animateShed","AppleShed.hx",35,0x3c85bcf0)
		HX_STACK_THIS(this)
		HX_STACK_ARG(appleCount,"appleCount")
		HX_STACK_ARG(basketFull,"basketFull")
		HX_STACK_LINE(36)
		int _g = ::Math_obj::floor((basketFull * .3));		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(36)
		if (((appleCount >= _g))){
			HX_STACK_LINE(37)
			this->appleShedSprite->animation->play(HX_CSTRING("shedOne"),null(),null());
		}
		HX_STACK_LINE(39)
		int _g1 = ::Math_obj::floor((basketFull * .7));		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(39)
		if (((appleCount >= _g1))){
			HX_STACK_LINE(40)
			this->appleShedSprite->animation->play(HX_CSTRING("shedTwo"),null(),null());
		}
		HX_STACK_LINE(42)
		if (((appleCount >= basketFull))){
			HX_STACK_LINE(43)
			this->appleShedSprite->animation->play(HX_CSTRING("shedThree"),null(),null());
		}
		HX_STACK_LINE(45)
		int _g2 = ::Math_obj::floor((basketFull * .3));		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(45)
		if (((appleCount < _g2))){
			HX_STACK_LINE(46)
			this->appleShedSprite->animation->play(HX_CSTRING("shedEmpty"),null(),null());
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(AppleShed_obj,animateShed,(void))

Void AppleShed_obj::update( ){
{
		HX_STACK_FRAME("AppleShed","update",0xc21157e9,"AppleShed.update","AppleShed.hx",52,0x3c85bcf0)
		HX_STACK_THIS(this)
		HX_STACK_LINE(52)
		this->super::update();
	}
return null();
}



AppleShed_obj::AppleShed_obj()
{
}

void AppleShed_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(AppleShed);
	HX_MARK_MEMBER_NAME(appleShedGroup,"appleShedGroup");
	HX_MARK_MEMBER_NAME(appleShedSprite,"appleShedSprite");
	HX_MARK_MEMBER_NAME(maxShedLevel,"maxShedLevel");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void AppleShed_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(appleShedGroup,"appleShedGroup");
	HX_VISIT_MEMBER_NAME(appleShedSprite,"appleShedSprite");
	HX_VISIT_MEMBER_NAME(maxShedLevel,"maxShedLevel");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic AppleShed_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"animateShed") ) { return animateShed_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"maxShedLevel") ) { return maxShedLevel; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"appleShedGroup") ) { return appleShedGroup; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"appleShedSprite") ) { return appleShedSprite; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic AppleShed_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 12:
		if (HX_FIELD_EQ(inName,"maxShedLevel") ) { maxShedLevel=inValue.Cast< int >(); return inValue; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"appleShedGroup") ) { appleShedGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"appleShedSprite") ) { appleShedSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void AppleShed_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("appleShedGroup"));
	outFields->push(HX_CSTRING("appleShedSprite"));
	outFields->push(HX_CSTRING("maxShedLevel"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(AppleShed_obj,appleShedGroup),HX_CSTRING("appleShedGroup")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(AppleShed_obj,appleShedSprite),HX_CSTRING("appleShedSprite")},
	{hx::fsInt,(int)offsetof(AppleShed_obj,maxShedLevel),HX_CSTRING("maxShedLevel")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("appleShedGroup"),
	HX_CSTRING("appleShedSprite"),
	HX_CSTRING("maxShedLevel"),
	HX_CSTRING("animateShed"),
	HX_CSTRING("update"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(AppleShed_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(AppleShed_obj::__mClass,"__mClass");
};

#endif

Class AppleShed_obj::__mClass;

void AppleShed_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("AppleShed"), hx::TCanCast< AppleShed_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void AppleShed_obj::__boot()
{
}

