#include <hxcpp.h>

#ifndef INCLUDED_JuiceIcon
#include <JuiceIcon.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_input_mouse_FlxMouse
#include <flixel/input/mouse/FlxMouse.h>
#endif
#ifndef INCLUDED_flixel_input_mouse_FlxMouseButton
#include <flixel/input/mouse/FlxMouseButton.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxInput
#include <flixel/interfaces/IFlxInput.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxPooled
#include <flixel/interfaces/IFlxPooled.h>
#endif
#ifndef INCLUDED_flixel_plugin_FlxPlugin
#include <flixel/plugin/FlxPlugin.h>
#endif
#ifndef INCLUDED_flixel_plugin_MouseEventManager
#include <flixel/plugin/MouseEventManager.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPoint
#include <flixel/util/FlxPoint.h>
#endif

Void JuiceIcon_obj::__construct(int X,int Y,bool dragType)
{
HX_STACK_FRAME("JuiceIcon","new",0xe2021c4b,"JuiceIcon.new","JuiceIcon.hx",11,0x8d113585)
HX_STACK_THIS(this)
HX_STACK_ARG(X,"X")
HX_STACK_ARG(Y,"Y")
HX_STACK_ARG(dragType,"dragType")
{
	HX_STACK_LINE(18)
	this->homeY = (int)70;
	HX_STACK_LINE(17)
	this->homeX = (int)-46;
	HX_STACK_LINE(16)
	this->isMousedOver = false;
	HX_STACK_LINE(14)
	this->dragging = false;
	HX_STACK_LINE(22)
	super::__construct(X,Y,null());
	HX_STACK_LINE(23)
	this->dragType = dragType;
	HX_STACK_LINE(24)
	this->loadGraphic(HX_CSTRING("assets/images/juiceMachineIcon.png"),false,(int)10,(int)20,null(),null());
	HX_STACK_LINE(25)
	this->set_x(this->homeX);
	HX_STACK_LINE(26)
	this->set_y(this->homeY);
	HX_STACK_LINE(27)
	if ((dragType)){
		HX_STACK_LINE(28)
		this->homeX = (int)119;
		HX_STACK_LINE(29)
		this->homeY = (int)70;
	}
	HX_STACK_LINE(31)
	::flixel::plugin::MouseEventManager_obj::add(hx::ObjectPtr<OBJ_>(this),this->onDown_dyn(),this->onUp_dyn(),null(),null(),null(),null(),null());
}
;
	return null();
}

//JuiceIcon_obj::~JuiceIcon_obj() { }

Dynamic JuiceIcon_obj::__CreateEmpty() { return  new JuiceIcon_obj; }
hx::ObjectPtr< JuiceIcon_obj > JuiceIcon_obj::__new(int X,int Y,bool dragType)
{  hx::ObjectPtr< JuiceIcon_obj > result = new JuiceIcon_obj();
	result->__construct(X,Y,dragType);
	return result;}

Dynamic JuiceIcon_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< JuiceIcon_obj > result = new JuiceIcon_obj();
	result->__construct(inArgs[0],inArgs[1],inArgs[2]);
	return result;}

Void JuiceIcon_obj::onDown( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("JuiceIcon","onDown",0xf25fba16,"JuiceIcon.onDown","JuiceIcon.hx",37,0x8d113585)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(37)
		this->dragging = true;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(JuiceIcon_obj,onDown,(void))

Void JuiceIcon_obj::onUp( ::flixel::FlxSprite sprite){
{
		HX_STACK_FRAME("JuiceIcon","onUp",0xe086930f,"JuiceIcon.onUp","JuiceIcon.hx",41,0x8d113585)
		HX_STACK_THIS(this)
		HX_STACK_ARG(sprite,"sprite")
		HX_STACK_LINE(43)
		this->dragging = false;
		HX_STACK_LINE(44)
		this->resetSeedPos();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(JuiceIcon_obj,onUp,(void))

Void JuiceIcon_obj::dragSeed( bool temp){
{
		HX_STACK_FRAME("JuiceIcon","dragSeed",0x2bd087ba,"JuiceIcon.dragSeed","JuiceIcon.hx",50,0x8d113585)
		HX_STACK_THIS(this)
		HX_STACK_ARG(temp,"temp")
		HX_STACK_LINE(50)
		if ((temp)){
			HX_STACK_LINE(52)
			this->set_x((::flixel::FlxG_obj::mouse->x - (int)8));
			HX_STACK_LINE(53)
			this->set_y((::flixel::FlxG_obj::mouse->y - (int)14));
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(JuiceIcon_obj,dragSeed,(void))

Void JuiceIcon_obj::resetSeedPos( ){
{
		HX_STACK_FRAME("JuiceIcon","resetSeedPos",0xb9e97149,"JuiceIcon.resetSeedPos","JuiceIcon.hx",60,0x8d113585)
		HX_STACK_THIS(this)
		HX_STACK_LINE(60)
		if ((this->dragType)){
			HX_STACK_LINE(61)
			this->set_x(this->homeX);
			HX_STACK_LINE(62)
			this->set_y(this->homeY);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceIcon_obj,resetSeedPos,(void))

bool JuiceIcon_obj::getDragging( ){
	HX_STACK_FRAME("JuiceIcon","getDragging",0xfa8cae70,"JuiceIcon.getDragging","JuiceIcon.hx",68,0x8d113585)
	HX_STACK_THIS(this)
	HX_STACK_LINE(68)
	return this->dragging;
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceIcon_obj,getDragging,return )

Void JuiceIcon_obj::setDragging( bool val){
{
		HX_STACK_FRAME("JuiceIcon","setDragging",0x04f9b57c,"JuiceIcon.setDragging","JuiceIcon.hx",75,0x8d113585)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(75)
		this->dragging = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(JuiceIcon_obj,setDragging,(void))

Void JuiceIcon_obj::checkMouseState( ){
{
		HX_STACK_FRAME("JuiceIcon","checkMouseState",0x3657d0ff,"JuiceIcon.checkMouseState","JuiceIcon.hx",81,0x8d113585)
		HX_STACK_THIS(this)
		struct _Function_1_1{
			inline static bool Block( ){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","JuiceIcon.hx",81,0x8d113585)
				{
					HX_STACK_LINE(81)
					::flixel::input::mouse::FlxMouseButton _this = ::flixel::FlxG_obj::mouse->_leftButton;		HX_STACK_VAR(_this,"_this");
					HX_STACK_LINE(81)
					return (bool((_this->current == (int)-1)) || bool((_this->current == (int)-2)));
				}
				return null();
			}
		};
		HX_STACK_LINE(81)
		if ((_Function_1_1::Block())){
			HX_STACK_LINE(83)
			this->dragging = false;
			HX_STACK_LINE(84)
			this->resetSeedPos();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceIcon_obj,checkMouseState,(void))

Void JuiceIcon_obj::unregisterJuiceEvent( ){
{
		HX_STACK_FRAME("JuiceIcon","unregisterJuiceEvent",0xeb2d352b,"JuiceIcon.unregisterJuiceEvent","JuiceIcon.hx",91,0x8d113585)
		HX_STACK_THIS(this)
		HX_STACK_LINE(91)
		::flixel::plugin::MouseEventManager_obj::remove(hx::ObjectPtr<OBJ_>(this));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceIcon_obj,unregisterJuiceEvent,(void))

Void JuiceIcon_obj::registerJuiceEvent( ){
{
		HX_STACK_FRAME("JuiceIcon","registerJuiceEvent",0x97997d52,"JuiceIcon.registerJuiceEvent","JuiceIcon.hx",95,0x8d113585)
		HX_STACK_THIS(this)
		HX_STACK_LINE(95)
		::flixel::plugin::MouseEventManager_obj::add(hx::ObjectPtr<OBJ_>(this),this->onDown_dyn(),this->onUp_dyn(),null(),null(),null(),null(),null());
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceIcon_obj,registerJuiceEvent,(void))

Void JuiceIcon_obj::update( ){
{
		HX_STACK_FRAME("JuiceIcon","update",0x946d3efe,"JuiceIcon.update","JuiceIcon.hx",98,0x8d113585)
		HX_STACK_THIS(this)
		HX_STACK_LINE(100)
		this->checkMouseState();
		HX_STACK_LINE(101)
		this->dragSeed(this->dragging);
		HX_STACK_LINE(102)
		this->super::update();
	}
return null();
}


Void JuiceIcon_obj::destroy( ){
{
		HX_STACK_FRAME("JuiceIcon","destroy",0xd1e04a65,"JuiceIcon.destroy","JuiceIcon.hx",106,0x8d113585)
		HX_STACK_THIS(this)
		HX_STACK_LINE(108)
		::flixel::plugin::MouseEventManager_obj::remove(hx::ObjectPtr<OBJ_>(this));
		HX_STACK_LINE(109)
		this->super::destroy();
	}
return null();
}



JuiceIcon_obj::JuiceIcon_obj()
{
}

Dynamic JuiceIcon_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"onUp") ) { return onUp_dyn(); }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"homeX") ) { return homeX; }
		if (HX_FIELD_EQ(inName,"homeY") ) { return homeY; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"onDown") ) { return onDown_dyn(); }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"dragging") ) { return dragging; }
		if (HX_FIELD_EQ(inName,"dragType") ) { return dragType; }
		if (HX_FIELD_EQ(inName,"dragSeed") ) { return dragSeed_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"getDragging") ) { return getDragging_dyn(); }
		if (HX_FIELD_EQ(inName,"setDragging") ) { return setDragging_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"isMousedOver") ) { return isMousedOver; }
		if (HX_FIELD_EQ(inName,"resetSeedPos") ) { return resetSeedPos_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"checkMouseState") ) { return checkMouseState_dyn(); }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"registerJuiceEvent") ) { return registerJuiceEvent_dyn(); }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"unregisterJuiceEvent") ) { return unregisterJuiceEvent_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic JuiceIcon_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"homeX") ) { homeX=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"homeY") ) { homeY=inValue.Cast< int >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"dragging") ) { dragging=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"dragType") ) { dragType=inValue.Cast< bool >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"isMousedOver") ) { isMousedOver=inValue.Cast< bool >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void JuiceIcon_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("dragging"));
	outFields->push(HX_CSTRING("dragType"));
	outFields->push(HX_CSTRING("isMousedOver"));
	outFields->push(HX_CSTRING("homeX"));
	outFields->push(HX_CSTRING("homeY"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsBool,(int)offsetof(JuiceIcon_obj,dragging),HX_CSTRING("dragging")},
	{hx::fsBool,(int)offsetof(JuiceIcon_obj,dragType),HX_CSTRING("dragType")},
	{hx::fsBool,(int)offsetof(JuiceIcon_obj,isMousedOver),HX_CSTRING("isMousedOver")},
	{hx::fsInt,(int)offsetof(JuiceIcon_obj,homeX),HX_CSTRING("homeX")},
	{hx::fsInt,(int)offsetof(JuiceIcon_obj,homeY),HX_CSTRING("homeY")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("dragging"),
	HX_CSTRING("dragType"),
	HX_CSTRING("isMousedOver"),
	HX_CSTRING("homeX"),
	HX_CSTRING("homeY"),
	HX_CSTRING("onDown"),
	HX_CSTRING("onUp"),
	HX_CSTRING("dragSeed"),
	HX_CSTRING("resetSeedPos"),
	HX_CSTRING("getDragging"),
	HX_CSTRING("setDragging"),
	HX_CSTRING("checkMouseState"),
	HX_CSTRING("unregisterJuiceEvent"),
	HX_CSTRING("registerJuiceEvent"),
	HX_CSTRING("update"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(JuiceIcon_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(JuiceIcon_obj::__mClass,"__mClass");
};

#endif

Class JuiceIcon_obj::__mClass;

void JuiceIcon_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("JuiceIcon"), hx::TCanCast< JuiceIcon_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void JuiceIcon_obj::__boot()
{
}

