#include <hxcpp.h>

#ifndef INCLUDED_KoiFish
#include <KoiFish.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_util_FlxRandom
#include <flixel/util/FlxRandom.h>
#endif

Void KoiFish_obj::__construct(hx::Null< Float >  __o_koiPondX,hx::Null< Float >  __o_koiPondY)
{
HX_STACK_FRAME("KoiFish","new",0x5402d4cf,"KoiFish.new","KoiFish.hx",13,0x187bf981)
HX_STACK_THIS(this)
HX_STACK_ARG(__o_koiPondX,"koiPondX")
HX_STACK_ARG(__o_koiPondY,"koiPondY")
Float koiPondX = __o_koiPondX.Default(0);
Float koiPondY = __o_koiPondY.Default(0);
{
	HX_STACK_LINE(21)
	this->koiFishSpeed = .1;
	HX_STACK_LINE(18)
	this->fishDirectionTime = (int)0;
	HX_STACK_LINE(29)
	super::__construct(null());
	HX_STACK_LINE(30)
	::flixel::group::FlxGroup _g = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(30)
	this->koiFishGroup = _g;
	HX_STACK_LINE(31)
	::flixel::FlxSprite _g1 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
	HX_STACK_LINE(31)
	this->koiFish = _g1;
	HX_STACK_LINE(32)
	this->koiFish->loadGraphic(HX_CSTRING("assets/images/koiFishAnimation.png"),true,(int)10,(int)10,null(),null());
	HX_STACK_LINE(33)
	this->koiFish->animation->add(HX_CSTRING("koiFishSwimUp"),Array_obj< int >::__new().Add((int)0).Add((int)1),(int)8,true);
	HX_STACK_LINE(34)
	this->koiFish->animation->add(HX_CSTRING("koiFishSwimRight"),Array_obj< int >::__new().Add((int)2).Add((int)3),(int)8,true);
	HX_STACK_LINE(35)
	this->koiFish->animation->add(HX_CSTRING("koiFishSwimLeft"),Array_obj< int >::__new().Add((int)4).Add((int)5),(int)8,true);
	HX_STACK_LINE(36)
	this->koiFish->animation->add(HX_CSTRING("koiFishSwimDown"),Array_obj< int >::__new().Add((int)6).Add((int)7),(int)8,true);
	HX_STACK_LINE(37)
	this->koiFish->animation->add(HX_CSTRING("koiFishSwimUpLeft"),Array_obj< int >::__new().Add((int)8).Add((int)9),(int)8,true);
	HX_STACK_LINE(38)
	this->koiFish->animation->add(HX_CSTRING("koiFishSwimUpRight"),Array_obj< int >::__new().Add((int)10).Add((int)11),(int)8,true);
	HX_STACK_LINE(39)
	this->koiFish->animation->add(HX_CSTRING("koiFishSwimDownRight"),Array_obj< int >::__new().Add((int)12).Add((int)13),(int)8,true);
	HX_STACK_LINE(40)
	this->koiFish->animation->add(HX_CSTRING("koiFishSwimDownLeft"),Array_obj< int >::__new().Add((int)14).Add((int)15),(int)8,true);
	HX_STACK_LINE(41)
	this->koiFish->set_x((koiPondX + (int)35));
	HX_STACK_LINE(42)
	this->koiFish->set_y((koiPondY + (int)35));
	HX_STACK_LINE(43)
	this->koiFishMaxSwimHeight = (this->koiFish->y + (int)13);
	HX_STACK_LINE(44)
	this->koiFishMaxSwimWidth = (this->koiFish->x + (int)10);
	HX_STACK_LINE(45)
	this->koiFishMaxSwimHeightNeg = (this->koiFish->y - (int)7);
	HX_STACK_LINE(46)
	this->koiFishMaxSwimWidthNeg = (this->koiFish->x - (int)8);
	HX_STACK_LINE(47)
	this->koiFishGroup->add(this->koiFish);
	HX_STACK_LINE(48)
	this->add(this->koiFishGroup);
}
;
	return null();
}

//KoiFish_obj::~KoiFish_obj() { }

Dynamic KoiFish_obj::__CreateEmpty() { return  new KoiFish_obj; }
hx::ObjectPtr< KoiFish_obj > KoiFish_obj::__new(hx::Null< Float >  __o_koiPondX,hx::Null< Float >  __o_koiPondY)
{  hx::ObjectPtr< KoiFish_obj > result = new KoiFish_obj();
	result->__construct(__o_koiPondX,__o_koiPondY);
	return result;}

Dynamic KoiFish_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< KoiFish_obj > result = new KoiFish_obj();
	result->__construct(inArgs[0],inArgs[1]);
	return result;}

::flixel::FlxSprite KoiFish_obj::getFishSprite( ){
	HX_STACK_FRAME("KoiFish","getFishSprite",0xcc3a8822,"KoiFish.getFishSprite","KoiFish.hx",52,0x187bf981)
	HX_STACK_THIS(this)
	HX_STACK_LINE(52)
	return this->koiFish;
}


HX_DEFINE_DYNAMIC_FUNC0(KoiFish_obj,getFishSprite,return )

Void KoiFish_obj::fishScared( ){
{
		HX_STACK_FRAME("KoiFish","fishScared",0xd0a93a49,"KoiFish.fishScared","KoiFish.hx",56,0x187bf981)
		HX_STACK_THIS(this)
		HX_STACK_LINE(56)
		this->koiFishSpeed = .4;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(KoiFish_obj,fishScared,(void))

Void KoiFish_obj::fishSwimAI( ){
{
		HX_STACK_FRAME("KoiFish","fishSwimAI",0x59f29e99,"KoiFish.fishSwimAI","KoiFish.hx",59,0x187bf981)
		HX_STACK_THIS(this)
		HX_STACK_LINE(61)
		hx::SubEq(this->fishDirectionTime,::flixel::FlxG_obj::elapsed);
		HX_STACK_LINE(62)
		if (((this->fishDirectionTime <= (int)0))){
			HX_STACK_LINE(63)
			Float _g = ::flixel::util::FlxRandom_obj::floatRanged((int)1,(int)2,null());		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(63)
			this->fishDirectionTime = _g;
			HX_STACK_LINE(64)
			int _g1 = ::flixel::util::FlxRandom_obj::intRanged((int)0,(int)7,null());		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(64)
			this->currentFishDirection = _g1;
			HX_STACK_LINE(65)
			this->lastFishDirection = this->currentFishDirection;
		}
		HX_STACK_LINE(68)
		if (((this->koiFish->x >= this->koiFishMaxSwimWidth))){
			HX_STACK_LINE(69)
			int _g2 = ::flixel::util::FlxRandom_obj::intRanged((int)4,(int)6,null());		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(69)
			this->currentFishDirection = _g2;
			HX_STACK_LINE(70)
			Float _g3 = ::flixel::util::FlxRandom_obj::floatRanged((int)1,(int)2,null());		HX_STACK_VAR(_g3,"_g3");
			HX_STACK_LINE(70)
			this->fishDirectionTime = _g3;
		}
		HX_STACK_LINE(72)
		if (((this->koiFish->x <= this->koiFishMaxSwimWidthNeg))){
			HX_STACK_LINE(73)
			int _g4 = ::flixel::util::FlxRandom_obj::intRanged((int)0,(int)2,null());		HX_STACK_VAR(_g4,"_g4");
			HX_STACK_LINE(73)
			this->currentFishDirection = _g4;
			HX_STACK_LINE(74)
			Float _g5 = ::flixel::util::FlxRandom_obj::floatRanged((int)1,(int)2,null());		HX_STACK_VAR(_g5,"_g5");
			HX_STACK_LINE(74)
			this->fishDirectionTime = _g5;
		}
		HX_STACK_LINE(76)
		if (((this->koiFish->y >= this->koiFishMaxSwimHeight))){
			HX_STACK_LINE(77)
			int _g6 = ::flixel::util::FlxRandom_obj::intRanged((int)6,(int)7,null());		HX_STACK_VAR(_g6,"_g6");
			HX_STACK_LINE(77)
			this->currentFishDirection = _g6;
			HX_STACK_LINE(78)
			Float _g7 = ::flixel::util::FlxRandom_obj::floatRanged((int)1,(int)2,null());		HX_STACK_VAR(_g7,"_g7");
			HX_STACK_LINE(78)
			this->fishDirectionTime = _g7;
		}
		HX_STACK_LINE(80)
		if (((this->koiFish->y <= this->koiFishMaxSwimHeightNeg))){
			HX_STACK_LINE(81)
			int _g8 = ::flixel::util::FlxRandom_obj::intRanged((int)3,(int)4,null());		HX_STACK_VAR(_g8,"_g8");
			HX_STACK_LINE(81)
			this->currentFishDirection = _g8;
			HX_STACK_LINE(82)
			Float _g9 = ::flixel::util::FlxRandom_obj::floatRanged((int)1,(int)2,null());		HX_STACK_VAR(_g9,"_g9");
			HX_STACK_LINE(82)
			this->fishDirectionTime = _g9;
		}
		HX_STACK_LINE(85)
		{
			HX_STACK_LINE(85)
			int _g = this->currentFishDirection;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(85)
			switch( (int)(_g)){
				case (int)0: {
					HX_STACK_LINE(88)
					{
						HX_STACK_LINE(88)
						::flixel::FlxSprite _g1 = this->koiFish;		HX_STACK_VAR(_g1,"_g1");
						HX_STACK_LINE(88)
						_g1->set_x((_g1->x + this->koiFishSpeed));
					}
					HX_STACK_LINE(89)
					{
						HX_STACK_LINE(89)
						::flixel::FlxSprite _g1 = this->koiFish;		HX_STACK_VAR(_g1,"_g1");
						HX_STACK_LINE(89)
						_g1->set_y((_g1->y - this->koiFishSpeed));
					}
					HX_STACK_LINE(90)
					this->koiFish->animation->play(HX_CSTRING("koiFishSwimUpRight"),null(),null());
				}
				;break;
				case (int)1: {
					HX_STACK_LINE(93)
					{
						HX_STACK_LINE(93)
						::flixel::FlxSprite _g1 = this->koiFish;		HX_STACK_VAR(_g1,"_g1");
						HX_STACK_LINE(93)
						_g1->set_x((_g1->x + this->koiFishSpeed));
					}
					HX_STACK_LINE(94)
					this->koiFish->animation->play(HX_CSTRING("koiFishSwimRight"),null(),null());
				}
				;break;
				case (int)2: {
					HX_STACK_LINE(97)
					{
						HX_STACK_LINE(97)
						::flixel::FlxSprite _g1 = this->koiFish;		HX_STACK_VAR(_g1,"_g1");
						HX_STACK_LINE(97)
						_g1->set_x((_g1->x + this->koiFishSpeed));
					}
					HX_STACK_LINE(98)
					{
						HX_STACK_LINE(98)
						::flixel::FlxSprite _g1 = this->koiFish;		HX_STACK_VAR(_g1,"_g1");
						HX_STACK_LINE(98)
						_g1->set_y((_g1->y + this->koiFishSpeed));
					}
					HX_STACK_LINE(99)
					this->koiFish->animation->play(HX_CSTRING("koiFishSwimDownRight"),null(),null());
				}
				;break;
				case (int)3: {
					HX_STACK_LINE(103)
					{
						HX_STACK_LINE(103)
						::flixel::FlxSprite _g1 = this->koiFish;		HX_STACK_VAR(_g1,"_g1");
						HX_STACK_LINE(103)
						_g1->set_y((_g1->y + this->koiFishSpeed));
					}
					HX_STACK_LINE(104)
					this->koiFish->animation->play(HX_CSTRING("koiFishSwimDown"),null(),null());
				}
				;break;
				case (int)4: {
					HX_STACK_LINE(107)
					{
						HX_STACK_LINE(107)
						::flixel::FlxSprite _g1 = this->koiFish;		HX_STACK_VAR(_g1,"_g1");
						HX_STACK_LINE(107)
						_g1->set_x((_g1->x - this->koiFishSpeed));
					}
					HX_STACK_LINE(108)
					{
						HX_STACK_LINE(108)
						::flixel::FlxSprite _g1 = this->koiFish;		HX_STACK_VAR(_g1,"_g1");
						HX_STACK_LINE(108)
						_g1->set_y((_g1->y + this->koiFishSpeed));
					}
					HX_STACK_LINE(109)
					this->koiFish->animation->play(HX_CSTRING("koiFishSwimDownLeft"),null(),null());
				}
				;break;
				case (int)5: {
					HX_STACK_LINE(112)
					{
						HX_STACK_LINE(112)
						::flixel::FlxSprite _g1 = this->koiFish;		HX_STACK_VAR(_g1,"_g1");
						HX_STACK_LINE(112)
						_g1->set_x((_g1->x - this->koiFishSpeed));
					}
					HX_STACK_LINE(113)
					this->koiFish->animation->play(HX_CSTRING("koiFishSwimLeft"),null(),null());
				}
				;break;
				case (int)6: {
					HX_STACK_LINE(116)
					{
						HX_STACK_LINE(116)
						::flixel::FlxSprite _g1 = this->koiFish;		HX_STACK_VAR(_g1,"_g1");
						HX_STACK_LINE(116)
						_g1->set_x((_g1->x - this->koiFishSpeed));
					}
					HX_STACK_LINE(117)
					{
						HX_STACK_LINE(117)
						::flixel::FlxSprite _g1 = this->koiFish;		HX_STACK_VAR(_g1,"_g1");
						HX_STACK_LINE(117)
						_g1->set_y((_g1->y - this->koiFishSpeed));
					}
					HX_STACK_LINE(118)
					this->koiFish->animation->play(HX_CSTRING("koiFishSwimUpLeft"),null(),null());
				}
				;break;
				case (int)7: {
					HX_STACK_LINE(121)
					{
						HX_STACK_LINE(121)
						::flixel::FlxSprite _g1 = this->koiFish;		HX_STACK_VAR(_g1,"_g1");
						HX_STACK_LINE(121)
						_g1->set_y((_g1->y - this->koiFishSpeed));
					}
					HX_STACK_LINE(122)
					this->koiFish->animation->play(HX_CSTRING("koiFishSwimUp"),null(),null());
				}
				;break;
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(KoiFish_obj,fishSwimAI,(void))

Void KoiFish_obj::calmFishDown( ){
{
		HX_STACK_FRAME("KoiFish","calmFishDown",0xb633a68a,"KoiFish.calmFishDown","KoiFish.hx",128,0x187bf981)
		HX_STACK_THIS(this)
		HX_STACK_LINE(128)
		if (((this->koiFishSpeed > .1))){
			HX_STACK_LINE(129)
			hx::SubEq(this->koiFishSpeed,.001);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(KoiFish_obj,calmFishDown,(void))

Void KoiFish_obj::update( ){
{
		HX_STACK_FRAME("KoiFish","update",0x58efb0fa,"KoiFish.update","KoiFish.hx",133,0x187bf981)
		HX_STACK_THIS(this)
		HX_STACK_LINE(134)
		this->fishSwimAI();
		HX_STACK_LINE(135)
		this->calmFishDown();
		HX_STACK_LINE(136)
		this->super::update();
	}
return null();
}


Void KoiFish_obj::destroy( ){
{
		HX_STACK_FRAME("KoiFish","destroy",0xff8194e9,"KoiFish.destroy","KoiFish.hx",139,0x187bf981)
		HX_STACK_THIS(this)
		HX_STACK_LINE(140)
		this->koiFish = null();
		HX_STACK_LINE(141)
		this->koiFishGroup;
		HX_STACK_LINE(142)
		this->super::destroy();
	}
return null();
}



KoiFish_obj::KoiFish_obj()
{
}

void KoiFish_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(KoiFish);
	HX_MARK_MEMBER_NAME(koiFishGroup,"koiFishGroup");
	HX_MARK_MEMBER_NAME(koiFish,"koiFish");
	HX_MARK_MEMBER_NAME(fishDirectionTime,"fishDirectionTime");
	HX_MARK_MEMBER_NAME(currentFishDirection,"currentFishDirection");
	HX_MARK_MEMBER_NAME(lastFishDirection,"lastFishDirection");
	HX_MARK_MEMBER_NAME(koiFishSpeed,"koiFishSpeed");
	HX_MARK_MEMBER_NAME(koiFishMaxSwimHeight,"koiFishMaxSwimHeight");
	HX_MARK_MEMBER_NAME(koiFishMaxSwimWidth,"koiFishMaxSwimWidth");
	HX_MARK_MEMBER_NAME(koiFishMaxSwimHeightNeg,"koiFishMaxSwimHeightNeg");
	HX_MARK_MEMBER_NAME(koiFishMaxSwimWidthNeg,"koiFishMaxSwimWidthNeg");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void KoiFish_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(koiFishGroup,"koiFishGroup");
	HX_VISIT_MEMBER_NAME(koiFish,"koiFish");
	HX_VISIT_MEMBER_NAME(fishDirectionTime,"fishDirectionTime");
	HX_VISIT_MEMBER_NAME(currentFishDirection,"currentFishDirection");
	HX_VISIT_MEMBER_NAME(lastFishDirection,"lastFishDirection");
	HX_VISIT_MEMBER_NAME(koiFishSpeed,"koiFishSpeed");
	HX_VISIT_MEMBER_NAME(koiFishMaxSwimHeight,"koiFishMaxSwimHeight");
	HX_VISIT_MEMBER_NAME(koiFishMaxSwimWidth,"koiFishMaxSwimWidth");
	HX_VISIT_MEMBER_NAME(koiFishMaxSwimHeightNeg,"koiFishMaxSwimHeightNeg");
	HX_VISIT_MEMBER_NAME(koiFishMaxSwimWidthNeg,"koiFishMaxSwimWidthNeg");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic KoiFish_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"koiFish") ) { return koiFish; }
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"fishScared") ) { return fishScared_dyn(); }
		if (HX_FIELD_EQ(inName,"fishSwimAI") ) { return fishSwimAI_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"koiFishGroup") ) { return koiFishGroup; }
		if (HX_FIELD_EQ(inName,"koiFishSpeed") ) { return koiFishSpeed; }
		if (HX_FIELD_EQ(inName,"calmFishDown") ) { return calmFishDown_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"getFishSprite") ) { return getFishSprite_dyn(); }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"fishDirectionTime") ) { return fishDirectionTime; }
		if (HX_FIELD_EQ(inName,"lastFishDirection") ) { return lastFishDirection; }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"koiFishMaxSwimWidth") ) { return koiFishMaxSwimWidth; }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"currentFishDirection") ) { return currentFishDirection; }
		if (HX_FIELD_EQ(inName,"koiFishMaxSwimHeight") ) { return koiFishMaxSwimHeight; }
		break;
	case 22:
		if (HX_FIELD_EQ(inName,"koiFishMaxSwimWidthNeg") ) { return koiFishMaxSwimWidthNeg; }
		break;
	case 23:
		if (HX_FIELD_EQ(inName,"koiFishMaxSwimHeightNeg") ) { return koiFishMaxSwimHeightNeg; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic KoiFish_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 7:
		if (HX_FIELD_EQ(inName,"koiFish") ) { koiFish=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"koiFishGroup") ) { koiFishGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"koiFishSpeed") ) { koiFishSpeed=inValue.Cast< Float >(); return inValue; }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"fishDirectionTime") ) { fishDirectionTime=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"lastFishDirection") ) { lastFishDirection=inValue.Cast< int >(); return inValue; }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"koiFishMaxSwimWidth") ) { koiFishMaxSwimWidth=inValue.Cast< Float >(); return inValue; }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"currentFishDirection") ) { currentFishDirection=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"koiFishMaxSwimHeight") ) { koiFishMaxSwimHeight=inValue.Cast< Float >(); return inValue; }
		break;
	case 22:
		if (HX_FIELD_EQ(inName,"koiFishMaxSwimWidthNeg") ) { koiFishMaxSwimWidthNeg=inValue.Cast< Float >(); return inValue; }
		break;
	case 23:
		if (HX_FIELD_EQ(inName,"koiFishMaxSwimHeightNeg") ) { koiFishMaxSwimHeightNeg=inValue.Cast< Float >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void KoiFish_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("koiFishGroup"));
	outFields->push(HX_CSTRING("koiFish"));
	outFields->push(HX_CSTRING("fishDirectionTime"));
	outFields->push(HX_CSTRING("currentFishDirection"));
	outFields->push(HX_CSTRING("lastFishDirection"));
	outFields->push(HX_CSTRING("koiFishSpeed"));
	outFields->push(HX_CSTRING("koiFishMaxSwimHeight"));
	outFields->push(HX_CSTRING("koiFishMaxSwimWidth"));
	outFields->push(HX_CSTRING("koiFishMaxSwimHeightNeg"));
	outFields->push(HX_CSTRING("koiFishMaxSwimWidthNeg"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(KoiFish_obj,koiFishGroup),HX_CSTRING("koiFishGroup")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(KoiFish_obj,koiFish),HX_CSTRING("koiFish")},
	{hx::fsFloat,(int)offsetof(KoiFish_obj,fishDirectionTime),HX_CSTRING("fishDirectionTime")},
	{hx::fsInt,(int)offsetof(KoiFish_obj,currentFishDirection),HX_CSTRING("currentFishDirection")},
	{hx::fsInt,(int)offsetof(KoiFish_obj,lastFishDirection),HX_CSTRING("lastFishDirection")},
	{hx::fsFloat,(int)offsetof(KoiFish_obj,koiFishSpeed),HX_CSTRING("koiFishSpeed")},
	{hx::fsFloat,(int)offsetof(KoiFish_obj,koiFishMaxSwimHeight),HX_CSTRING("koiFishMaxSwimHeight")},
	{hx::fsFloat,(int)offsetof(KoiFish_obj,koiFishMaxSwimWidth),HX_CSTRING("koiFishMaxSwimWidth")},
	{hx::fsFloat,(int)offsetof(KoiFish_obj,koiFishMaxSwimHeightNeg),HX_CSTRING("koiFishMaxSwimHeightNeg")},
	{hx::fsFloat,(int)offsetof(KoiFish_obj,koiFishMaxSwimWidthNeg),HX_CSTRING("koiFishMaxSwimWidthNeg")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("koiFishGroup"),
	HX_CSTRING("koiFish"),
	HX_CSTRING("fishDirectionTime"),
	HX_CSTRING("currentFishDirection"),
	HX_CSTRING("lastFishDirection"),
	HX_CSTRING("koiFishSpeed"),
	HX_CSTRING("koiFishMaxSwimHeight"),
	HX_CSTRING("koiFishMaxSwimWidth"),
	HX_CSTRING("koiFishMaxSwimHeightNeg"),
	HX_CSTRING("koiFishMaxSwimWidthNeg"),
	HX_CSTRING("getFishSprite"),
	HX_CSTRING("fishScared"),
	HX_CSTRING("fishSwimAI"),
	HX_CSTRING("calmFishDown"),
	HX_CSTRING("update"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(KoiFish_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(KoiFish_obj::__mClass,"__mClass");
};

#endif

Class KoiFish_obj::__mClass;

void KoiFish_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("KoiFish"), hx::TCanCast< KoiFish_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void KoiFish_obj::__boot()
{
}

