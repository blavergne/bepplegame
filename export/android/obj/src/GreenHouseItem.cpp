#include <hxcpp.h>

#ifndef INCLUDED_GreenHouseItem
#include <GreenHouseItem.h>
#endif
#ifndef INCLUDED_MarketItem
#include <MarketItem.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif

Void GreenHouseItem_obj::__construct()
{
HX_STACK_FRAME("GreenHouseItem","new",0x79189ea2,"GreenHouseItem.new","GreenHouseItem.hx",13,0xd3c58b0e)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(14)
	super::__construct();
	HX_STACK_LINE(15)
	this->initMarketItems();
}
;
	return null();
}

//GreenHouseItem_obj::~GreenHouseItem_obj() { }

Dynamic GreenHouseItem_obj::__CreateEmpty() { return  new GreenHouseItem_obj; }
hx::ObjectPtr< GreenHouseItem_obj > GreenHouseItem_obj::__new()
{  hx::ObjectPtr< GreenHouseItem_obj > result = new GreenHouseItem_obj();
	result->__construct();
	return result;}

Dynamic GreenHouseItem_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< GreenHouseItem_obj > result = new GreenHouseItem_obj();
	result->__construct();
	return result;}

Void GreenHouseItem_obj::initMarketItems( ){
{
		HX_STACK_FRAME("GreenHouseItem","initMarketItems",0x2d0d9cb6,"GreenHouseItem.initMarketItems","GreenHouseItem.hx",18,0xd3c58b0e)
		HX_STACK_THIS(this)
		HX_STACK_LINE(19)
		::flixel::FlxSprite _g = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(19)
		this->itemSprite = _g;
		HX_STACK_LINE(20)
		this->itemCost = (int)50;
		HX_STACK_LINE(21)
		this->itemName = HX_CSTRING("greenHouseItem");
		HX_STACK_LINE(22)
		this->itemCount = (int)1;
		HX_STACK_LINE(23)
		this->itemSprite->loadGraphic(HX_CSTRING("assets/images/GreenHouseItemAnimation.png"),true,(int)30,(int)30,false,HX_CSTRING("greenHouseItem"));
		HX_STACK_LINE(24)
		this->itemSprite->animation->add(HX_CSTRING("available"),Array_obj< int >::__new().Add((int)0),(int)1,false);
		HX_STACK_LINE(25)
		this->itemSprite->animation->add(HX_CSTRING("notAvailable"),Array_obj< int >::__new().Add((int)1),(int)1,false);
		HX_STACK_LINE(26)
		this->setItemAvailable(true);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(GreenHouseItem_obj,initMarketItems,(void))

Void GreenHouseItem_obj::setGreenHouseItemCount( int val){
{
		HX_STACK_FRAME("GreenHouseItem","setGreenHouseItemCount",0x361d859b,"GreenHouseItem.setGreenHouseItemCount","GreenHouseItem.hx",30,0xd3c58b0e)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(30)
		this->itemCount = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(GreenHouseItem_obj,setGreenHouseItemCount,(void))

Void GreenHouseItem_obj::update( ){
{
		HX_STACK_FRAME("GreenHouseItem","update",0x36558d07,"GreenHouseItem.update","GreenHouseItem.hx",33,0xd3c58b0e)
		HX_STACK_THIS(this)
		HX_STACK_LINE(34)
		this->setAvailableAnimation();
		HX_STACK_LINE(35)
		this->super::update();
	}
return null();
}



GreenHouseItem_obj::GreenHouseItem_obj()
{
}

Dynamic GreenHouseItem_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"initMarketItems") ) { return initMarketItems_dyn(); }
		break;
	case 22:
		if (HX_FIELD_EQ(inName,"setGreenHouseItemCount") ) { return setGreenHouseItemCount_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic GreenHouseItem_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	return super::__SetField(inName,inValue,inCallProp);
}

void GreenHouseItem_obj::__GetFields(Array< ::String> &outFields)
{
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo *sMemberStorageInfo = 0;
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("initMarketItems"),
	HX_CSTRING("setGreenHouseItemCount"),
	HX_CSTRING("update"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(GreenHouseItem_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(GreenHouseItem_obj::__mClass,"__mClass");
};

#endif

Class GreenHouseItem_obj::__mClass;

void GreenHouseItem_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("GreenHouseItem"), hx::TCanCast< GreenHouseItem_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void GreenHouseItem_obj::__boot()
{
}

