#include <hxcpp.h>

#ifndef INCLUDED_GoldCoin
#include <GoldCoin.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif

Void GoldCoin_obj::__construct()
{
HX_STACK_FRAME("GoldCoin","new",0x128a2de3,"GoldCoin.new","GoldCoin.hx",17,0x752d5eed)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(18)
	super::__construct(null());
	HX_STACK_LINE(19)
	::flixel::group::FlxGroup _g = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(19)
	this->coinGroup = _g;
	HX_STACK_LINE(20)
	::flixel::FlxSprite _g1 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
	HX_STACK_LINE(20)
	this->coin = _g1;
	HX_STACK_LINE(21)
	this->coin->loadGraphic(HX_CSTRING("assets/images/goldCoin.png"),false,null(),null(),null(),null());
	HX_STACK_LINE(22)
	this->coin->set_x((int)20);
	HX_STACK_LINE(23)
	this->coin->set_y((int)24);
	HX_STACK_LINE(24)
	this->coinGroup->add(this->coin);
	HX_STACK_LINE(25)
	this->add(this->coinGroup);
}
;
	return null();
}

//GoldCoin_obj::~GoldCoin_obj() { }

Dynamic GoldCoin_obj::__CreateEmpty() { return  new GoldCoin_obj; }
hx::ObjectPtr< GoldCoin_obj > GoldCoin_obj::__new()
{  hx::ObjectPtr< GoldCoin_obj > result = new GoldCoin_obj();
	result->__construct();
	return result;}

Dynamic GoldCoin_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< GoldCoin_obj > result = new GoldCoin_obj();
	result->__construct();
	return result;}

::flixel::FlxSprite GoldCoin_obj::getCoinSprite( ){
	HX_STACK_FRAME("GoldCoin","getCoinSprite",0x6c7ce3ef,"GoldCoin.getCoinSprite","GoldCoin.hx",29,0x752d5eed)
	HX_STACK_THIS(this)
	HX_STACK_LINE(29)
	return this->coin;
}


HX_DEFINE_DYNAMIC_FUNC0(GoldCoin_obj,getCoinSprite,return )

Void GoldCoin_obj::destroy( ){
{
		HX_STACK_FRAME("GoldCoin","destroy",0x3b2d07fd,"GoldCoin.destroy","GoldCoin.hx",32,0x752d5eed)
		HX_STACK_THIS(this)
		HX_STACK_LINE(33)
		this->coin = null();
		HX_STACK_LINE(34)
		this->coinGroup = null();
		HX_STACK_LINE(35)
		this->super::destroy();
	}
return null();
}



GoldCoin_obj::GoldCoin_obj()
{
}

void GoldCoin_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(GoldCoin);
	HX_MARK_MEMBER_NAME(coinGroup,"coinGroup");
	HX_MARK_MEMBER_NAME(coin,"coin");
	::flixel::group::FlxTypedGroup_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void GoldCoin_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(coinGroup,"coinGroup");
	HX_VISIT_MEMBER_NAME(coin,"coin");
	::flixel::group::FlxTypedGroup_obj::__Visit(HX_VISIT_ARG);
}

Dynamic GoldCoin_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"coin") ) { return coin; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"coinGroup") ) { return coinGroup; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"getCoinSprite") ) { return getCoinSprite_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic GoldCoin_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"coin") ) { coin=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"coinGroup") ) { coinGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void GoldCoin_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("coinGroup"));
	outFields->push(HX_CSTRING("coin"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(GoldCoin_obj,coinGroup),HX_CSTRING("coinGroup")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(GoldCoin_obj,coin),HX_CSTRING("coin")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("coinGroup"),
	HX_CSTRING("coin"),
	HX_CSTRING("getCoinSprite"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(GoldCoin_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(GoldCoin_obj::__mClass,"__mClass");
};

#endif

Class GoldCoin_obj::__mClass;

void GoldCoin_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("GoldCoin"), hx::TCanCast< GoldCoin_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void GoldCoin_obj::__boot()
{
}

