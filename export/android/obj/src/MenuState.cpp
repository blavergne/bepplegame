#include <hxcpp.h>

#ifndef INCLUDED_Apple
#include <Apple.h>
#endif
#ifndef INCLUDED_MenuState
#include <MenuState.h>
#endif
#ifndef INCLUDED_PlayState
#include <PlayState.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxGame
#include <flixel/FlxGame.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_FlxState
#include <flixel/FlxState.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_text_FlxText
#include <flixel/text/FlxText.h>
#endif
#ifndef INCLUDED_flixel_ui_FlxButton
#include <flixel/ui/FlxButton.h>
#endif
#ifndef INCLUDED_flixel_ui_FlxTypedButton
#include <flixel/ui/FlxTypedButton.h>
#endif
#ifndef INCLUDED_flixel_util_FlxDestroyUtil
#include <flixel/util/FlxDestroyUtil.h>
#endif
#ifndef INCLUDED_flixel_util_FlxSpriteUtil
#include <flixel/util/FlxSpriteUtil.h>
#endif
#ifndef INCLUDED_openfl__v2_display_DisplayObject
#include <openfl/_v2/display/DisplayObject.h>
#endif
#ifndef INCLUDED_openfl__v2_display_DisplayObjectContainer
#include <openfl/_v2/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_openfl__v2_display_IBitmapDrawable
#include <openfl/_v2/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_openfl__v2_display_InteractiveObject
#include <openfl/_v2/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_openfl__v2_display_Sprite
#include <openfl/_v2/display/Sprite.h>
#endif
#ifndef INCLUDED_openfl__v2_events_EventDispatcher
#include <openfl/_v2/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__v2_events_IEventDispatcher
#include <openfl/_v2/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_openfl__v2_system_System
#include <openfl/_v2/system/System.h>
#endif

Void MenuState_obj::__construct(Dynamic MaxSize)
{
HX_STACK_FRAME("MenuState","new",0xe563b1c4,"MenuState.new","MenuState.hx",16,0xdfbcb22c)
HX_STACK_THIS(this)
HX_STACK_ARG(MaxSize,"MaxSize")
{
	HX_STACK_LINE(40)
	this->apple_4Created = false;
	HX_STACK_LINE(39)
	this->apple_3Created = false;
	HX_STACK_LINE(38)
	this->apple_2Created = false;
	HX_STACK_LINE(37)
	this->apple_1Created = false;
	HX_STACK_LINE(36)
	this->apple_0Created = false;
	HX_STACK_LINE(35)
	this->apple_4Timer = (int)5;
	HX_STACK_LINE(34)
	this->apple_3Timer = .2;
	HX_STACK_LINE(33)
	this->apple_2Timer = (int)1;
	HX_STACK_LINE(32)
	this->apple_1Timer = (int)3;
	HX_STACK_LINE(31)
	this->apple_0Timer = (int)2;
	HX_STACK_LINE(16)
	super::__construct(MaxSize);
}
;
	return null();
}

//MenuState_obj::~MenuState_obj() { }

Dynamic MenuState_obj::__CreateEmpty() { return  new MenuState_obj; }
hx::ObjectPtr< MenuState_obj > MenuState_obj::__new(Dynamic MaxSize)
{  hx::ObjectPtr< MenuState_obj > result = new MenuState_obj();
	result->__construct(MaxSize);
	return result;}

Dynamic MenuState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< MenuState_obj > result = new MenuState_obj();
	result->__construct(inArgs[0]);
	return result;}

Void MenuState_obj::create( ){
{
		HX_STACK_FRAME("MenuState","create",0xe57b7c18,"MenuState.create","MenuState.hx",50,0xdfbcb22c)
		HX_STACK_THIS(this)
		HX_STACK_LINE(51)
		::flixel::FlxSprite _g = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(51)
		this->beppleTitle = _g;
		HX_STACK_LINE(52)
		::flixel::FlxSprite _g1 = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(52)
		this->backgroundSprite = _g1;
		HX_STACK_LINE(53)
		this->beppleTitle->loadGraphic(HX_CSTRING("assets/images/titleImageProcessed.png"),null(),null(),null(),null(),null());
		HX_STACK_LINE(54)
		this->backgroundSprite->loadGraphic(HX_CSTRING("assets/images/titleBackground.png"),null(),null(),null(),null(),null());
		HX_STACK_LINE(55)
		this->beppleTitle->set_x((int)100);
		HX_STACK_LINE(56)
		this->beppleTitle->set_y((int)50);
		HX_STACK_LINE(57)
		::flixel::ui::FlxButton _g2 = ::flixel::ui::FlxButton_obj::__new((int)0,(int)0,HX_CSTRING("Normal"),this->startGame_dyn());		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(57)
		this->playBtn = _g2;
		HX_STACK_LINE(58)
		::flixel::text::FlxText _g3 = ::flixel::text::FlxText_obj::__new((int)0,(int)0,(int)100,HX_CSTRING("Bepple"),(int)20,false);		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(58)
		this->gameTitle = _g3;
		HX_STACK_LINE(59)
		::flixel::ui::FlxButton _g4 = ::flixel::ui::FlxButton_obj::__new((int)0,(int)0,HX_CSTRING("Tutorial"),this->startTutorial_dyn());		HX_STACK_VAR(_g4,"_g4");
		HX_STACK_LINE(59)
		this->tutButton = _g4;
		HX_STACK_LINE(60)
		::flixel::ui::FlxButton _g5 = ::flixel::ui::FlxButton_obj::__new((int)0,(int)0,HX_CSTRING("Exit"),this->exitGame_dyn());		HX_STACK_VAR(_g5,"_g5");
		HX_STACK_LINE(60)
		this->exitButton = _g5;
		HX_STACK_LINE(61)
		::flixel::ui::FlxButton _g6 = ::flixel::ui::FlxButton_obj::__new((int)0,(int)0,HX_CSTRING("Easy"),this->startMathless_dyn());		HX_STACK_VAR(_g6,"_g6");
		HX_STACK_LINE(61)
		this->mathlessBepple = _g6;
		HX_STACK_LINE(62)
		this->gameTitle->set_alignment(HX_CSTRING("center"));
		HX_STACK_LINE(63)
		::flixel::util::FlxSpriteUtil_obj::screenCenter(this->gameTitle,true,true);
		HX_STACK_LINE(64)
		{
			HX_STACK_LINE(64)
			::flixel::text::FlxText _g7 = this->gameTitle;		HX_STACK_VAR(_g7,"_g7");
			HX_STACK_LINE(64)
			_g7->set_y((_g7->y - (int)30));
		}
		HX_STACK_LINE(65)
		::flixel::util::FlxSpriteUtil_obj::screenCenter(this->playBtn,null(),null());
		HX_STACK_LINE(66)
		::flixel::util::FlxSpriteUtil_obj::screenCenter(this->tutButton,null(),null());
		HX_STACK_LINE(67)
		{
			HX_STACK_LINE(67)
			::flixel::ui::FlxButton _g7 = this->tutButton;		HX_STACK_VAR(_g7,"_g7");
			HX_STACK_LINE(67)
			_g7->set_y((_g7->y + (int)70));
		}
		HX_STACK_LINE(68)
		::flixel::util::FlxSpriteUtil_obj::screenCenter(this->mathlessBepple,null(),null());
		HX_STACK_LINE(69)
		{
			HX_STACK_LINE(69)
			::flixel::ui::FlxButton _g7 = this->playBtn;		HX_STACK_VAR(_g7,"_g7");
			HX_STACK_LINE(69)
			_g7->set_y((_g7->y + (int)35));
		}
		HX_STACK_LINE(70)
		::flixel::util::FlxSpriteUtil_obj::screenCenter(this->exitButton,null(),null());
		HX_STACK_LINE(71)
		{
			HX_STACK_LINE(71)
			::flixel::ui::FlxButton _g7 = this->exitButton;		HX_STACK_VAR(_g7,"_g7");
			HX_STACK_LINE(71)
			_g7->set_y((_g7->y + (int)105));
		}
		HX_STACK_LINE(72)
		this->add(this->backgroundSprite);
		HX_STACK_LINE(73)
		this->add(this->beppleTitle);
		HX_STACK_LINE(74)
		this->add(this->playBtn);
		HX_STACK_LINE(75)
		this->add(this->tutButton);
		HX_STACK_LINE(76)
		this->add(this->mathlessBepple);
		HX_STACK_LINE(78)
		this->super::create();
	}
return null();
}


bool MenuState_obj::getTutorialOn( ){
	HX_STACK_FRAME("MenuState","getTutorialOn",0x6456daf7,"MenuState.getTutorialOn","MenuState.hx",83,0xdfbcb22c)
	HX_STACK_THIS(this)
	HX_STACK_LINE(83)
	return ::MenuState_obj::tutorialOn;
}


HX_DEFINE_DYNAMIC_FUNC0(MenuState_obj,getTutorialOn,return )

Void MenuState_obj::appleMachine( ){
{
		HX_STACK_FRAME("MenuState","appleMachine",0x9bfa6f69,"MenuState.appleMachine","MenuState.hx",86,0xdfbcb22c)
		HX_STACK_THIS(this)
		HX_STACK_LINE(87)
		hx::SubEq(this->apple_0Timer,::flixel::FlxG_obj::elapsed);
		HX_STACK_LINE(88)
		hx::SubEq(this->apple_1Timer,::flixel::FlxG_obj::elapsed);
		HX_STACK_LINE(89)
		hx::SubEq(this->apple_2Timer,::flixel::FlxG_obj::elapsed);
		HX_STACK_LINE(90)
		hx::SubEq(this->apple_3Timer,::flixel::FlxG_obj::elapsed);
		HX_STACK_LINE(91)
		hx::SubEq(this->apple_4Timer,::flixel::FlxG_obj::elapsed);
		HX_STACK_LINE(92)
		if (((bool((this->apple_0Timer <= (int)0)) && bool(!(this->apple_0Created))))){
			HX_STACK_LINE(93)
			::Apple _g = ::Apple_obj::__new();		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(93)
			this->apple_0 = _g;
			HX_STACK_LINE(94)
			this->apple_0->setAppleX((int)156);
			HX_STACK_LINE(95)
			this->apple_0->setAppleY((int)60);
			HX_STACK_LINE(96)
			this->apple_0->incAppleGrowth();
			HX_STACK_LINE(97)
			this->apple_0->growApple();
			HX_STACK_LINE(98)
			this->apple_0->incAppleGrowth();
			HX_STACK_LINE(99)
			this->apple_0->growApple();
			HX_STACK_LINE(100)
			this->apple_0Created = true;
			HX_STACK_LINE(101)
			this->add(this->apple_0);
		}
		HX_STACK_LINE(103)
		if (((bool((this->apple_1Timer <= (int)0)) && bool(!(this->apple_1Created))))){
			HX_STACK_LINE(104)
			::Apple _g1 = ::Apple_obj::__new();		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(104)
			this->apple_1 = _g1;
			HX_STACK_LINE(105)
			this->apple_1->setAppleX((int)250);
			HX_STACK_LINE(106)
			this->apple_1->setAppleY((int)60);
			HX_STACK_LINE(107)
			this->apple_1->incAppleGrowth();
			HX_STACK_LINE(108)
			this->apple_1->growApple();
			HX_STACK_LINE(109)
			this->apple_1->incAppleGrowth();
			HX_STACK_LINE(110)
			this->apple_1->growApple();
			HX_STACK_LINE(111)
			this->apple_1Created = true;
			HX_STACK_LINE(112)
			this->add(this->apple_1);
		}
		HX_STACK_LINE(114)
		if (((bool((this->apple_2Timer <= (int)0)) && bool(!(this->apple_2Created))))){
			HX_STACK_LINE(115)
			::Apple _g2 = ::Apple_obj::__new();		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(115)
			this->apple_2 = _g2;
			HX_STACK_LINE(116)
			this->apple_2->setAppleX((int)310);
			HX_STACK_LINE(117)
			this->apple_2->setAppleY((int)132);
			HX_STACK_LINE(118)
			this->apple_2->incAppleGrowth();
			HX_STACK_LINE(119)
			this->apple_2->growApple();
			HX_STACK_LINE(120)
			this->apple_2->incAppleGrowth();
			HX_STACK_LINE(121)
			this->apple_2->growApple();
			HX_STACK_LINE(122)
			this->apple_2Created = true;
			HX_STACK_LINE(123)
			this->add(this->apple_2);
		}
		HX_STACK_LINE(125)
		if (((bool((this->apple_3Timer <= (int)0)) && bool(!(this->apple_3Created))))){
			HX_STACK_LINE(126)
			::Apple _g3 = ::Apple_obj::__new();		HX_STACK_VAR(_g3,"_g3");
			HX_STACK_LINE(126)
			this->apple_3 = _g3;
			HX_STACK_LINE(127)
			this->apple_3->setAppleX((int)450);
			HX_STACK_LINE(128)
			this->apple_3->setAppleY((int)153);
			HX_STACK_LINE(129)
			this->apple_3->incAppleGrowth();
			HX_STACK_LINE(130)
			this->apple_3->growApple();
			HX_STACK_LINE(131)
			this->apple_3->incAppleGrowth();
			HX_STACK_LINE(132)
			this->apple_3->growApple();
			HX_STACK_LINE(133)
			this->apple_3Created = true;
			HX_STACK_LINE(134)
			this->add(this->apple_3);
		}
		HX_STACK_LINE(136)
		if (((bool((this->apple_4Timer <= (int)0)) && bool(!(this->apple_4Created))))){
			HX_STACK_LINE(137)
			::Apple _g4 = ::Apple_obj::__new();		HX_STACK_VAR(_g4,"_g4");
			HX_STACK_LINE(137)
			this->apple_4 = _g4;
			HX_STACK_LINE(138)
			this->apple_4->setAppleX((int)500);
			HX_STACK_LINE(139)
			this->apple_4->setAppleY((int)82);
			HX_STACK_LINE(140)
			this->apple_4->incAppleGrowth();
			HX_STACK_LINE(141)
			this->apple_4->growApple();
			HX_STACK_LINE(142)
			this->apple_4->incAppleGrowth();
			HX_STACK_LINE(143)
			this->apple_4->growApple();
			HX_STACK_LINE(144)
			this->apple_4Created = true;
			HX_STACK_LINE(145)
			this->add(this->apple_4);
		}
		HX_STACK_LINE(147)
		if (((this->apple_0 != null()))){
			HX_STACK_LINE(148)
			if (((this->apple_0->getAppleSprite()->y > (int)480))){
				HX_STACK_LINE(149)
				this->apple_0->destroy();
				HX_STACK_LINE(150)
				this->apple_0 = null();
				HX_STACK_LINE(151)
				this->apple_0Timer = (int)3;
				HX_STACK_LINE(152)
				this->apple_0Created = false;
			}
		}
		HX_STACK_LINE(155)
		if (((this->apple_1 != null()))){
			HX_STACK_LINE(156)
			if (((this->apple_1->getAppleSprite()->y > (int)480))){
				HX_STACK_LINE(157)
				this->apple_1->destroy();
				HX_STACK_LINE(158)
				this->apple_1 = null();
				HX_STACK_LINE(159)
				this->apple_1Timer = (int)2;
				HX_STACK_LINE(160)
				this->apple_1Created = false;
			}
		}
		HX_STACK_LINE(163)
		if (((this->apple_2 != null()))){
			HX_STACK_LINE(164)
			if (((this->apple_2->getAppleSprite()->y > (int)480))){
				HX_STACK_LINE(165)
				this->apple_2->destroy();
				HX_STACK_LINE(166)
				this->apple_2 = null();
				HX_STACK_LINE(167)
				this->apple_2Timer = 2.7;
				HX_STACK_LINE(168)
				this->apple_2Created = false;
			}
		}
		HX_STACK_LINE(171)
		if (((this->apple_3 != null()))){
			HX_STACK_LINE(172)
			if (((this->apple_3->getAppleSprite()->y > (int)480))){
				HX_STACK_LINE(173)
				this->apple_3->destroy();
				HX_STACK_LINE(174)
				this->apple_3 = null();
				HX_STACK_LINE(175)
				this->apple_3Timer = 1.5;
				HX_STACK_LINE(176)
				this->apple_3Created = false;
			}
		}
		HX_STACK_LINE(179)
		if (((this->apple_4 != null()))){
			HX_STACK_LINE(180)
			if (((this->apple_4->getAppleSprite()->y > (int)480))){
				HX_STACK_LINE(181)
				this->apple_4->destroy();
				HX_STACK_LINE(182)
				this->apple_4 = null();
				HX_STACK_LINE(183)
				this->apple_4Timer = 3.3;
				HX_STACK_LINE(184)
				this->apple_4Created = false;
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(MenuState_obj,appleMachine,(void))

Void MenuState_obj::destroy( ){
{
		HX_STACK_FRAME("MenuState","destroy",0xf9ac905e,"MenuState.destroy","MenuState.hx",194,0xdfbcb22c)
		HX_STACK_THIS(this)
		HX_STACK_LINE(195)
		::flixel::ui::FlxButton _g = ::flixel::util::FlxDestroyUtil_obj::destroy(this->playBtn);		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(195)
		this->playBtn = _g;
		HX_STACK_LINE(196)
		::flixel::ui::FlxButton _g1 = ::flixel::util::FlxDestroyUtil_obj::destroy(this->tutButton);		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(196)
		this->tutButton = _g1;
		HX_STACK_LINE(197)
		this->super::destroy();
	}
return null();
}


Void MenuState_obj::update( ){
{
		HX_STACK_FRAME("MenuState","update",0xf0719b25,"MenuState.update","MenuState.hx",204,0xdfbcb22c)
		HX_STACK_THIS(this)
		HX_STACK_LINE(205)
		this->appleMachine();
		HX_STACK_LINE(206)
		this->super::update();
	}
return null();
}


Void MenuState_obj::startGame( ){
{
		HX_STACK_FRAME("MenuState","startGame",0x336371d8,"MenuState.startGame","MenuState.hx",209,0xdfbcb22c)
		HX_STACK_THIS(this)
		HX_STACK_LINE(210)
		::MenuState_obj::mathless = false;
		HX_STACK_LINE(211)
		::MenuState_obj::tutorialOn = false;
		HX_STACK_LINE(212)
		{
			HX_STACK_LINE(212)
			::flixel::FlxState State = ::PlayState_obj::__new(null());		HX_STACK_VAR(State,"State");
			HX_STACK_LINE(212)
			::flixel::FlxG_obj::game->_requestedState = State;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(MenuState_obj,startGame,(void))

Void MenuState_obj::exitGame( ){
{
		HX_STACK_FRAME("MenuState","exitGame",0xb093924c,"MenuState.exitGame","MenuState.hx",216,0xdfbcb22c)
		HX_STACK_THIS(this)
		HX_STACK_LINE(216)
		::openfl::_v2::system::System_obj::exit((int)0);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(MenuState_obj,exitGame,(void))

Void MenuState_obj::startTutorial( ){
{
		HX_STACK_FRAME("MenuState","startTutorial",0xc865f884,"MenuState.startTutorial","MenuState.hx",219,0xdfbcb22c)
		HX_STACK_THIS(this)
		HX_STACK_LINE(220)
		::MenuState_obj::mathless = false;
		HX_STACK_LINE(221)
		::MenuState_obj::tutorialOn = true;
		HX_STACK_LINE(222)
		{
			HX_STACK_LINE(222)
			::flixel::FlxState State = ::PlayState_obj::__new(null());		HX_STACK_VAR(State,"State");
			HX_STACK_LINE(222)
			::flixel::FlxG_obj::game->_requestedState = State;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(MenuState_obj,startTutorial,(void))

Void MenuState_obj::startMathless( ){
{
		HX_STACK_FRAME("MenuState","startMathless",0xec8f4207,"MenuState.startMathless","MenuState.hx",225,0xdfbcb22c)
		HX_STACK_THIS(this)
		HX_STACK_LINE(226)
		::MenuState_obj::mathless = true;
		HX_STACK_LINE(227)
		::MenuState_obj::tutorialOn = false;
		HX_STACK_LINE(228)
		{
			HX_STACK_LINE(228)
			::flixel::FlxState State = ::PlayState_obj::__new(null());		HX_STACK_VAR(State,"State");
			HX_STACK_LINE(228)
			::flixel::FlxG_obj::game->_requestedState = State;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(MenuState_obj,startMathless,(void))

bool MenuState_obj::mathless;

bool MenuState_obj::tutorialOn;

int MenuState_obj::windowLength;

int MenuState_obj::windowWidth;


MenuState_obj::MenuState_obj()
{
}

void MenuState_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(MenuState);
	HX_MARK_MEMBER_NAME(backgroundSprite,"backgroundSprite");
	HX_MARK_MEMBER_NAME(playBtn,"playBtn");
	HX_MARK_MEMBER_NAME(tutButton,"tutButton");
	HX_MARK_MEMBER_NAME(mathlessBepple,"mathlessBepple");
	HX_MARK_MEMBER_NAME(exitButton,"exitButton");
	HX_MARK_MEMBER_NAME(gameTitle,"gameTitle");
	HX_MARK_MEMBER_NAME(beppleTitle,"beppleTitle");
	HX_MARK_MEMBER_NAME(apple_0,"apple_0");
	HX_MARK_MEMBER_NAME(apple_1,"apple_1");
	HX_MARK_MEMBER_NAME(apple_2,"apple_2");
	HX_MARK_MEMBER_NAME(apple_3,"apple_3");
	HX_MARK_MEMBER_NAME(apple_4,"apple_4");
	HX_MARK_MEMBER_NAME(apple_0Timer,"apple_0Timer");
	HX_MARK_MEMBER_NAME(apple_1Timer,"apple_1Timer");
	HX_MARK_MEMBER_NAME(apple_2Timer,"apple_2Timer");
	HX_MARK_MEMBER_NAME(apple_3Timer,"apple_3Timer");
	HX_MARK_MEMBER_NAME(apple_4Timer,"apple_4Timer");
	HX_MARK_MEMBER_NAME(apple_0Created,"apple_0Created");
	HX_MARK_MEMBER_NAME(apple_1Created,"apple_1Created");
	HX_MARK_MEMBER_NAME(apple_2Created,"apple_2Created");
	HX_MARK_MEMBER_NAME(apple_3Created,"apple_3Created");
	HX_MARK_MEMBER_NAME(apple_4Created,"apple_4Created");
	::flixel::FlxState_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void MenuState_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(backgroundSprite,"backgroundSprite");
	HX_VISIT_MEMBER_NAME(playBtn,"playBtn");
	HX_VISIT_MEMBER_NAME(tutButton,"tutButton");
	HX_VISIT_MEMBER_NAME(mathlessBepple,"mathlessBepple");
	HX_VISIT_MEMBER_NAME(exitButton,"exitButton");
	HX_VISIT_MEMBER_NAME(gameTitle,"gameTitle");
	HX_VISIT_MEMBER_NAME(beppleTitle,"beppleTitle");
	HX_VISIT_MEMBER_NAME(apple_0,"apple_0");
	HX_VISIT_MEMBER_NAME(apple_1,"apple_1");
	HX_VISIT_MEMBER_NAME(apple_2,"apple_2");
	HX_VISIT_MEMBER_NAME(apple_3,"apple_3");
	HX_VISIT_MEMBER_NAME(apple_4,"apple_4");
	HX_VISIT_MEMBER_NAME(apple_0Timer,"apple_0Timer");
	HX_VISIT_MEMBER_NAME(apple_1Timer,"apple_1Timer");
	HX_VISIT_MEMBER_NAME(apple_2Timer,"apple_2Timer");
	HX_VISIT_MEMBER_NAME(apple_3Timer,"apple_3Timer");
	HX_VISIT_MEMBER_NAME(apple_4Timer,"apple_4Timer");
	HX_VISIT_MEMBER_NAME(apple_0Created,"apple_0Created");
	HX_VISIT_MEMBER_NAME(apple_1Created,"apple_1Created");
	HX_VISIT_MEMBER_NAME(apple_2Created,"apple_2Created");
	HX_VISIT_MEMBER_NAME(apple_3Created,"apple_3Created");
	HX_VISIT_MEMBER_NAME(apple_4Created,"apple_4Created");
	::flixel::FlxState_obj::__Visit(HX_VISIT_ARG);
}

Dynamic MenuState_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"create") ) { return create_dyn(); }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"playBtn") ) { return playBtn; }
		if (HX_FIELD_EQ(inName,"apple_0") ) { return apple_0; }
		if (HX_FIELD_EQ(inName,"apple_1") ) { return apple_1; }
		if (HX_FIELD_EQ(inName,"apple_2") ) { return apple_2; }
		if (HX_FIELD_EQ(inName,"apple_3") ) { return apple_3; }
		if (HX_FIELD_EQ(inName,"apple_4") ) { return apple_4; }
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"mathless") ) { return mathless; }
		if (HX_FIELD_EQ(inName,"exitGame") ) { return exitGame_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"tutButton") ) { return tutButton; }
		if (HX_FIELD_EQ(inName,"gameTitle") ) { return gameTitle; }
		if (HX_FIELD_EQ(inName,"startGame") ) { return startGame_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"tutorialOn") ) { return tutorialOn; }
		if (HX_FIELD_EQ(inName,"exitButton") ) { return exitButton; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"windowWidth") ) { return windowWidth; }
		if (HX_FIELD_EQ(inName,"beppleTitle") ) { return beppleTitle; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"windowLength") ) { return windowLength; }
		if (HX_FIELD_EQ(inName,"apple_0Timer") ) { return apple_0Timer; }
		if (HX_FIELD_EQ(inName,"apple_1Timer") ) { return apple_1Timer; }
		if (HX_FIELD_EQ(inName,"apple_2Timer") ) { return apple_2Timer; }
		if (HX_FIELD_EQ(inName,"apple_3Timer") ) { return apple_3Timer; }
		if (HX_FIELD_EQ(inName,"apple_4Timer") ) { return apple_4Timer; }
		if (HX_FIELD_EQ(inName,"appleMachine") ) { return appleMachine_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"getTutorialOn") ) { return getTutorialOn_dyn(); }
		if (HX_FIELD_EQ(inName,"startTutorial") ) { return startTutorial_dyn(); }
		if (HX_FIELD_EQ(inName,"startMathless") ) { return startMathless_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"mathlessBepple") ) { return mathlessBepple; }
		if (HX_FIELD_EQ(inName,"apple_0Created") ) { return apple_0Created; }
		if (HX_FIELD_EQ(inName,"apple_1Created") ) { return apple_1Created; }
		if (HX_FIELD_EQ(inName,"apple_2Created") ) { return apple_2Created; }
		if (HX_FIELD_EQ(inName,"apple_3Created") ) { return apple_3Created; }
		if (HX_FIELD_EQ(inName,"apple_4Created") ) { return apple_4Created; }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"backgroundSprite") ) { return backgroundSprite; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic MenuState_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 7:
		if (HX_FIELD_EQ(inName,"playBtn") ) { playBtn=inValue.Cast< ::flixel::ui::FlxButton >(); return inValue; }
		if (HX_FIELD_EQ(inName,"apple_0") ) { apple_0=inValue.Cast< ::Apple >(); return inValue; }
		if (HX_FIELD_EQ(inName,"apple_1") ) { apple_1=inValue.Cast< ::Apple >(); return inValue; }
		if (HX_FIELD_EQ(inName,"apple_2") ) { apple_2=inValue.Cast< ::Apple >(); return inValue; }
		if (HX_FIELD_EQ(inName,"apple_3") ) { apple_3=inValue.Cast< ::Apple >(); return inValue; }
		if (HX_FIELD_EQ(inName,"apple_4") ) { apple_4=inValue.Cast< ::Apple >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"mathless") ) { mathless=inValue.Cast< bool >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"tutButton") ) { tutButton=inValue.Cast< ::flixel::ui::FlxButton >(); return inValue; }
		if (HX_FIELD_EQ(inName,"gameTitle") ) { gameTitle=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"tutorialOn") ) { tutorialOn=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"exitButton") ) { exitButton=inValue.Cast< ::flixel::ui::FlxButton >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"windowWidth") ) { windowWidth=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"beppleTitle") ) { beppleTitle=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"windowLength") ) { windowLength=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"apple_0Timer") ) { apple_0Timer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"apple_1Timer") ) { apple_1Timer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"apple_2Timer") ) { apple_2Timer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"apple_3Timer") ) { apple_3Timer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"apple_4Timer") ) { apple_4Timer=inValue.Cast< Float >(); return inValue; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"mathlessBepple") ) { mathlessBepple=inValue.Cast< ::flixel::ui::FlxButton >(); return inValue; }
		if (HX_FIELD_EQ(inName,"apple_0Created") ) { apple_0Created=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"apple_1Created") ) { apple_1Created=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"apple_2Created") ) { apple_2Created=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"apple_3Created") ) { apple_3Created=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"apple_4Created") ) { apple_4Created=inValue.Cast< bool >(); return inValue; }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"backgroundSprite") ) { backgroundSprite=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void MenuState_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("backgroundSprite"));
	outFields->push(HX_CSTRING("playBtn"));
	outFields->push(HX_CSTRING("tutButton"));
	outFields->push(HX_CSTRING("mathlessBepple"));
	outFields->push(HX_CSTRING("exitButton"));
	outFields->push(HX_CSTRING("gameTitle"));
	outFields->push(HX_CSTRING("beppleTitle"));
	outFields->push(HX_CSTRING("apple_0"));
	outFields->push(HX_CSTRING("apple_1"));
	outFields->push(HX_CSTRING("apple_2"));
	outFields->push(HX_CSTRING("apple_3"));
	outFields->push(HX_CSTRING("apple_4"));
	outFields->push(HX_CSTRING("apple_0Timer"));
	outFields->push(HX_CSTRING("apple_1Timer"));
	outFields->push(HX_CSTRING("apple_2Timer"));
	outFields->push(HX_CSTRING("apple_3Timer"));
	outFields->push(HX_CSTRING("apple_4Timer"));
	outFields->push(HX_CSTRING("apple_0Created"));
	outFields->push(HX_CSTRING("apple_1Created"));
	outFields->push(HX_CSTRING("apple_2Created"));
	outFields->push(HX_CSTRING("apple_3Created"));
	outFields->push(HX_CSTRING("apple_4Created"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("mathless"),
	HX_CSTRING("tutorialOn"),
	HX_CSTRING("windowLength"),
	HX_CSTRING("windowWidth"),
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(MenuState_obj,backgroundSprite),HX_CSTRING("backgroundSprite")},
	{hx::fsObject /*::flixel::ui::FlxButton*/ ,(int)offsetof(MenuState_obj,playBtn),HX_CSTRING("playBtn")},
	{hx::fsObject /*::flixel::ui::FlxButton*/ ,(int)offsetof(MenuState_obj,tutButton),HX_CSTRING("tutButton")},
	{hx::fsObject /*::flixel::ui::FlxButton*/ ,(int)offsetof(MenuState_obj,mathlessBepple),HX_CSTRING("mathlessBepple")},
	{hx::fsObject /*::flixel::ui::FlxButton*/ ,(int)offsetof(MenuState_obj,exitButton),HX_CSTRING("exitButton")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(MenuState_obj,gameTitle),HX_CSTRING("gameTitle")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(MenuState_obj,beppleTitle),HX_CSTRING("beppleTitle")},
	{hx::fsObject /*::Apple*/ ,(int)offsetof(MenuState_obj,apple_0),HX_CSTRING("apple_0")},
	{hx::fsObject /*::Apple*/ ,(int)offsetof(MenuState_obj,apple_1),HX_CSTRING("apple_1")},
	{hx::fsObject /*::Apple*/ ,(int)offsetof(MenuState_obj,apple_2),HX_CSTRING("apple_2")},
	{hx::fsObject /*::Apple*/ ,(int)offsetof(MenuState_obj,apple_3),HX_CSTRING("apple_3")},
	{hx::fsObject /*::Apple*/ ,(int)offsetof(MenuState_obj,apple_4),HX_CSTRING("apple_4")},
	{hx::fsFloat,(int)offsetof(MenuState_obj,apple_0Timer),HX_CSTRING("apple_0Timer")},
	{hx::fsFloat,(int)offsetof(MenuState_obj,apple_1Timer),HX_CSTRING("apple_1Timer")},
	{hx::fsFloat,(int)offsetof(MenuState_obj,apple_2Timer),HX_CSTRING("apple_2Timer")},
	{hx::fsFloat,(int)offsetof(MenuState_obj,apple_3Timer),HX_CSTRING("apple_3Timer")},
	{hx::fsFloat,(int)offsetof(MenuState_obj,apple_4Timer),HX_CSTRING("apple_4Timer")},
	{hx::fsBool,(int)offsetof(MenuState_obj,apple_0Created),HX_CSTRING("apple_0Created")},
	{hx::fsBool,(int)offsetof(MenuState_obj,apple_1Created),HX_CSTRING("apple_1Created")},
	{hx::fsBool,(int)offsetof(MenuState_obj,apple_2Created),HX_CSTRING("apple_2Created")},
	{hx::fsBool,(int)offsetof(MenuState_obj,apple_3Created),HX_CSTRING("apple_3Created")},
	{hx::fsBool,(int)offsetof(MenuState_obj,apple_4Created),HX_CSTRING("apple_4Created")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("backgroundSprite"),
	HX_CSTRING("playBtn"),
	HX_CSTRING("tutButton"),
	HX_CSTRING("mathlessBepple"),
	HX_CSTRING("exitButton"),
	HX_CSTRING("gameTitle"),
	HX_CSTRING("beppleTitle"),
	HX_CSTRING("apple_0"),
	HX_CSTRING("apple_1"),
	HX_CSTRING("apple_2"),
	HX_CSTRING("apple_3"),
	HX_CSTRING("apple_4"),
	HX_CSTRING("apple_0Timer"),
	HX_CSTRING("apple_1Timer"),
	HX_CSTRING("apple_2Timer"),
	HX_CSTRING("apple_3Timer"),
	HX_CSTRING("apple_4Timer"),
	HX_CSTRING("apple_0Created"),
	HX_CSTRING("apple_1Created"),
	HX_CSTRING("apple_2Created"),
	HX_CSTRING("apple_3Created"),
	HX_CSTRING("apple_4Created"),
	HX_CSTRING("create"),
	HX_CSTRING("getTutorialOn"),
	HX_CSTRING("appleMachine"),
	HX_CSTRING("destroy"),
	HX_CSTRING("update"),
	HX_CSTRING("startGame"),
	HX_CSTRING("exitGame"),
	HX_CSTRING("startTutorial"),
	HX_CSTRING("startMathless"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(MenuState_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(MenuState_obj::mathless,"mathless");
	HX_MARK_MEMBER_NAME(MenuState_obj::tutorialOn,"tutorialOn");
	HX_MARK_MEMBER_NAME(MenuState_obj::windowLength,"windowLength");
	HX_MARK_MEMBER_NAME(MenuState_obj::windowWidth,"windowWidth");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(MenuState_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(MenuState_obj::mathless,"mathless");
	HX_VISIT_MEMBER_NAME(MenuState_obj::tutorialOn,"tutorialOn");
	HX_VISIT_MEMBER_NAME(MenuState_obj::windowLength,"windowLength");
	HX_VISIT_MEMBER_NAME(MenuState_obj::windowWidth,"windowWidth");
};

#endif

Class MenuState_obj::__mClass;

void MenuState_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("MenuState"), hx::TCanCast< MenuState_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void MenuState_obj::__boot()
{
	mathless= false;
	windowLength= (int)640;
	windowWidth= (int)480;
}

