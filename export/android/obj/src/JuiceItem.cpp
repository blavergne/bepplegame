#include <hxcpp.h>

#ifndef INCLUDED_JuiceItem
#include <JuiceItem.h>
#endif
#ifndef INCLUDED_MarketItem
#include <MarketItem.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif

Void JuiceItem_obj::__construct()
{
HX_STACK_FRAME("JuiceItem","new",0x11f6cee5,"JuiceItem.new","JuiceItem.hx",13,0x9677542b)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(14)
	super::__construct();
	HX_STACK_LINE(15)
	this->initMarketItems();
}
;
	return null();
}

//JuiceItem_obj::~JuiceItem_obj() { }

Dynamic JuiceItem_obj::__CreateEmpty() { return  new JuiceItem_obj; }
hx::ObjectPtr< JuiceItem_obj > JuiceItem_obj::__new()
{  hx::ObjectPtr< JuiceItem_obj > result = new JuiceItem_obj();
	result->__construct();
	return result;}

Dynamic JuiceItem_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< JuiceItem_obj > result = new JuiceItem_obj();
	result->__construct();
	return result;}

Void JuiceItem_obj::initMarketItems( ){
{
		HX_STACK_FRAME("JuiceItem","initMarketItems",0xf3d4a579,"JuiceItem.initMarketItems","JuiceItem.hx",18,0x9677542b)
		HX_STACK_THIS(this)
		HX_STACK_LINE(19)
		::flixel::FlxSprite _g = ::flixel::FlxSprite_obj::__new(null(),null(),null());		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(19)
		this->itemSprite = _g;
		HX_STACK_LINE(20)
		this->itemCost = (int)10;
		HX_STACK_LINE(21)
		this->itemName = HX_CSTRING("juiceItem");
		HX_STACK_LINE(22)
		this->itemCount = (int)1;
		HX_STACK_LINE(23)
		this->itemSprite->loadGraphic(HX_CSTRING("assets/images/appleJuiceItemImage.png"),true,(int)30,(int)30,false,HX_CSTRING("juiceItem"));
		HX_STACK_LINE(24)
		this->itemSprite->animation->add(HX_CSTRING("available"),Array_obj< int >::__new().Add((int)0),(int)1,false);
		HX_STACK_LINE(25)
		this->itemSprite->animation->add(HX_CSTRING("notAvailable"),Array_obj< int >::__new().Add((int)1),(int)1,false);
		HX_STACK_LINE(26)
		this->setItemAvailable(true);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(JuiceItem_obj,initMarketItems,(void))

Void JuiceItem_obj::setJuiceItemCount( int val){
{
		HX_STACK_FRAME("JuiceItem","setJuiceItemCount",0xa2072bc3,"JuiceItem.setJuiceItemCount","JuiceItem.hx",30,0x9677542b)
		HX_STACK_THIS(this)
		HX_STACK_ARG(val,"val")
		HX_STACK_LINE(30)
		this->itemCount = val;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(JuiceItem_obj,setJuiceItemCount,(void))

Void JuiceItem_obj::update( ){
{
		HX_STACK_FRAME("JuiceItem","update",0xe2bea8a4,"JuiceItem.update","JuiceItem.hx",33,0x9677542b)
		HX_STACK_THIS(this)
		HX_STACK_LINE(34)
		this->setAvailableAnimation();
		HX_STACK_LINE(35)
		this->super::update();
	}
return null();
}



JuiceItem_obj::JuiceItem_obj()
{
}

Dynamic JuiceItem_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"initMarketItems") ) { return initMarketItems_dyn(); }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"setJuiceItemCount") ) { return setJuiceItemCount_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic JuiceItem_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	return super::__SetField(inName,inValue,inCallProp);
}

void JuiceItem_obj::__GetFields(Array< ::String> &outFields)
{
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo *sMemberStorageInfo = 0;
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("initMarketItems"),
	HX_CSTRING("setJuiceItemCount"),
	HX_CSTRING("update"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(JuiceItem_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(JuiceItem_obj::__mClass,"__mClass");
};

#endif

Class JuiceItem_obj::__mClass;

void JuiceItem_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("JuiceItem"), hx::TCanCast< JuiceItem_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void JuiceItem_obj::__boot()
{
}

