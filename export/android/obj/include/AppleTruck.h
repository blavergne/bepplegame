#ifndef INCLUDED_AppleTruck
#define INCLUDED_AppleTruck

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/group/FlxGroup.h>
HX_DECLARE_CLASS0(AppleTruck)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,group,FlxGroup)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)
HX_DECLARE_CLASS2(flixel,system,FlxSound)


class HXCPP_CLASS_ATTRIBUTES  AppleTruck_obj : public ::flixel::group::FlxGroup_obj{
	public:
		typedef ::flixel::group::FlxGroup_obj super;
		typedef AppleTruck_obj OBJ_;
		AppleTruck_obj();
		Void __construct();

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< AppleTruck_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~AppleTruck_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("AppleTruck"); }

		::flixel::group::FlxGroup appleTruckGroup;
		::flixel::FlxSprite appleTruckSprite;
		Float truckTravelTime;
		int truckCapacity;
		int truckSize;
		bool pickUpReady;
		bool loadingApples;
		bool truckIsReset;
		bool truckSoundReady;
		::flixel::system::FlxSound truckSound;
		virtual bool getLoadingApples( );
		Dynamic getLoadingApples_dyn();

		virtual int getAppleTruckCapacity( );
		Dynamic getAppleTruckCapacity_dyn();

		virtual bool getTruckIsReset( );
		Dynamic getTruckIsReset_dyn();

		virtual Void decTruckCapacity( );
		Dynamic decTruckCapacity_dyn();

		virtual Void setTruckCapacity( int val);
		Dynamic setTruckCapacity_dyn();

		virtual Void setTruckSize( int val);
		Dynamic setTruckSize_dyn();

		virtual Void setLoadingApples( bool val);
		Dynamic setLoadingApples_dyn();

		virtual Void setPickUpReady( bool val);
		Dynamic setPickUpReady_dyn();

		virtual Void truckPickup( );
		Dynamic truckPickup_dyn();

		virtual Void truckGoHome( );
		Dynamic truckGoHome_dyn();

		virtual Void destroySounds( );
		Dynamic destroySounds_dyn();

		virtual Void destroy( );

		virtual Void update( );

};


#endif /* INCLUDED_AppleTruck */ 
