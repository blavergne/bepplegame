#ifndef INCLUDED_haxe_crypto_Sha1
#define INCLUDED_haxe_crypto_Sha1

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS2(haxe,crypto,Sha1)
namespace haxe{
namespace crypto{


class HXCPP_CLASS_ATTRIBUTES  Sha1_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef Sha1_obj OBJ_;
		Sha1_obj();
		Void __construct();

	public:
		inline void *operator new( size_t inSize, bool inContainer=false)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< Sha1_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~Sha1_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		::String __ToString() const { return HX_CSTRING("Sha1"); }

		virtual Array< int > doEncode( Array< int > x);
		Dynamic doEncode_dyn();

		virtual int ft( int t,int b,int c,int d);
		Dynamic ft_dyn();

		virtual int kt( int t);
		Dynamic kt_dyn();

		virtual ::String hex( Array< int > a);
		Dynamic hex_dyn();

		static ::String encode( ::String s);
		static Dynamic encode_dyn();

		static Array< int > str2blks( ::String s);
		static Dynamic str2blks_dyn();

};

} // end namespace haxe
} // end namespace crypto

#endif /* INCLUDED_haxe_crypto_Sha1 */ 
