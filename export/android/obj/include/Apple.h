#ifndef INCLUDED_Apple
#define INCLUDED_Apple

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/group/FlxGroup.h>
HX_DECLARE_CLASS0(Apple)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,group,FlxGroup)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)
HX_DECLARE_CLASS2(flixel,system,FlxSound)


class HXCPP_CLASS_ATTRIBUTES  Apple_obj : public ::flixel::group::FlxGroup_obj{
	public:
		typedef ::flixel::group::FlxGroup_obj super;
		typedef Apple_obj OBJ_;
		Apple_obj();
		Void __construct();

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< Apple_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~Apple_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("Apple"); }

		::flixel::group::FlxGroup appleGroup;
		::flixel::FlxSprite apple;
		::flixel::FlxSprite basket;
		::flixel::FlxSprite pickedAppleLeaves;
		::flixel::system::FlxSound pickedLeavesSound;
		::flixel::system::FlxSound pickedAppleGround;
		::flixel::system::FlxSound fallenAppleSound;
		int appleGrowth;
		bool iAmRipe;
		bool iAmVisible;
		bool iAmDead;
		bool appleDropped;
		bool rotLock;
		bool iAmRotten;
		bool flyToBasket;
		bool inBasket;
		bool leavesFallOnce;
		bool stillOnTree;
		bool appleFreeFall;
		Float timeToDrop;
		Float rotTimer;
		Float myInitialY;
		int randomXPos;
		int dropRate;
		Float rise;
		Float run;
		int randomYPos;
		Float myRipeCountDown;
		virtual int getRandomX( );
		Dynamic getRandomX_dyn();

		virtual int getRandomY( );
		Dynamic getRandomY_dyn();

		virtual Float getMyXPos( );
		Dynamic getMyXPos_dyn();

		virtual bool getAppleDropped( );
		Dynamic getAppleDropped_dyn();

		virtual Float getMyYPos( );
		Dynamic getMyYPos_dyn();

		virtual ::flixel::FlxSprite getAppleSprite( );
		Dynamic getAppleSprite_dyn();

		virtual Float getRotTimer( );
		Dynamic getRotTimer_dyn();

		virtual bool getAppleVisible( );
		Dynamic getAppleVisible_dyn();

		virtual bool getIAmRotten( );
		Dynamic getIAmRotten_dyn();

		virtual bool getAppleFreeFall( );
		Dynamic getAppleFreeFall_dyn();

		virtual bool getIAmRipe( );
		Dynamic getIAmRipe_dyn();

		virtual int getAppleGrowth( );
		Dynamic getAppleGrowth_dyn();

		virtual Float getMyInitialY( );
		Dynamic getMyInitialY_dyn();

		virtual bool getIAmDead( );
		Dynamic getIAmDead_dyn();

		virtual bool getInBasket( );
		Dynamic getInBasket_dyn();

		virtual Void setMyInitialY( Float val);
		Dynamic setMyInitialY_dyn();

		virtual bool getStillOnTree( );
		Dynamic getStillOnTree_dyn();

		virtual Void setDropRate( int val);
		Dynamic setDropRate_dyn();

		virtual Void setFlyToBasket( bool val);
		Dynamic setFlyToBasket_dyn();

		virtual Void ripenApple( );
		Dynamic ripenApple_dyn();

		virtual Void pauseRipen( );
		Dynamic pauseRipen_dyn();

		virtual Void unpauseRipen( );
		Dynamic unpauseRipen_dyn();

		virtual Void growApple( );
		Dynamic growApple_dyn();

		virtual Void revealApples( );
		Dynamic revealApples_dyn();

		virtual Void setAppleGrowth( int val);
		Dynamic setAppleGrowth_dyn();

		virtual Void setAppleX( Float xVal);
		Dynamic setAppleX_dyn();

		virtual Void setAppleY( Float yVal);
		Dynamic setAppleY_dyn();

		virtual Void setLeavesFallOnce( bool val);
		Dynamic setLeavesFallOnce_dyn();

		virtual Void incAppleGrowth( );
		Dynamic incAppleGrowth_dyn();

		virtual Void stopFreeFall( );
		Dynamic stopFreeFall_dyn();

		virtual Void dropApple( );
		Dynamic dropApple_dyn();

		virtual Void rotApple( );
		Dynamic rotApple_dyn();

		virtual Void setRiseRun( ::flixel::FlxSprite basketSprite);
		Dynamic setRiseRun_dyn();

		virtual Void sendAppleToBasket( );
		Dynamic sendAppleToBasket_dyn();

		virtual Void leavesFallAnimation( );
		Dynamic leavesFallAnimation_dyn();

		virtual Void resolveAnimation( );
		Dynamic resolveAnimation_dyn();

		virtual Void destroySounds( );
		Dynamic destroySounds_dyn();

		virtual Void update( );

		virtual Void destroy( );

};


#endif /* INCLUDED_Apple */ 
