#ifndef INCLUDED_KoiPond
#define INCLUDED_KoiPond

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/group/FlxGroup.h>
HX_DECLARE_CLASS0(KoiFish)
HX_DECLARE_CLASS0(KoiPond)
HX_DECLARE_CLASS0(Tile)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,group,FlxGroup)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)
HX_DECLARE_CLASS2(flixel,system,FlxSound)


class HXCPP_CLASS_ATTRIBUTES  KoiPond_obj : public ::flixel::group::FlxGroup_obj{
	public:
		typedef ::flixel::group::FlxGroup_obj super;
		typedef KoiPond_obj OBJ_;
		KoiPond_obj();
		Void __construct(::Tile tileSprite);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< KoiPond_obj > __new(::Tile tileSprite);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~KoiPond_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("KoiPond"); }

		::flixel::group::FlxGroup koiPondGroup;
		::flixel::FlxSprite koiPondSprite;
		::flixel::FlxSprite koiRock;
		::flixel::FlxSprite koiFishSprite;
		::flixel::FlxSprite koiPadSprite;
		::flixel::FlxSprite koiPadSpriteB;
		::flixel::FlxSprite koiPadSpriteC;
		::flixel::FlxSprite koiSandSprite;
		::KoiFish koiFish;
		::KoiFish koiFishB;
		::flixel::system::FlxSound koiPondSound;
		::flixel::system::FlxSound koiPondClickedSound0;
		::flixel::system::FlxSound koiPondClickedSound1;
		::flixel::system::FlxSound koiPondClickedSound2;
		::flixel::system::FlxSound koiPondClickedSound3;
		virtual Void splashSounds( ::flixel::FlxSprite sprite);
		Dynamic splashSounds_dyn();

		virtual Void update( );

		virtual Void destroy( );

};


#endif /* INCLUDED_KoiPond */ 
