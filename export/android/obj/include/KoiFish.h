#ifndef INCLUDED_KoiFish
#define INCLUDED_KoiFish

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/group/FlxGroup.h>
HX_DECLARE_CLASS0(KoiFish)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,group,FlxGroup)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  KoiFish_obj : public ::flixel::group::FlxGroup_obj{
	public:
		typedef ::flixel::group::FlxGroup_obj super;
		typedef KoiFish_obj OBJ_;
		KoiFish_obj();
		Void __construct(hx::Null< Float >  __o_koiPondX,hx::Null< Float >  __o_koiPondY);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< KoiFish_obj > __new(hx::Null< Float >  __o_koiPondX,hx::Null< Float >  __o_koiPondY);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~KoiFish_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("KoiFish"); }

		::flixel::group::FlxGroup koiFishGroup;
		::flixel::FlxSprite koiFish;
		Float fishDirectionTime;
		int currentFishDirection;
		int lastFishDirection;
		Float koiFishSpeed;
		Float koiFishMaxSwimHeight;
		Float koiFishMaxSwimWidth;
		Float koiFishMaxSwimHeightNeg;
		Float koiFishMaxSwimWidthNeg;
		virtual ::flixel::FlxSprite getFishSprite( );
		Dynamic getFishSprite_dyn();

		virtual Void fishScared( );
		Dynamic fishScared_dyn();

		virtual Void fishSwimAI( );
		Dynamic fishSwimAI_dyn();

		virtual Void calmFishDown( );
		Dynamic calmFishDown_dyn();

		virtual Void update( );

		virtual Void destroy( );

};


#endif /* INCLUDED_KoiFish */ 
