#ifndef INCLUDED_Greenhouse
#define INCLUDED_Greenhouse

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/group/FlxGroup.h>
HX_DECLARE_CLASS0(Greenhouse)
HX_DECLARE_CLASS0(Tile)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,group,FlxGroup)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)
HX_DECLARE_CLASS2(flixel,system,FlxSound)


class HXCPP_CLASS_ATTRIBUTES  Greenhouse_obj : public ::flixel::group::FlxGroup_obj{
	public:
		typedef ::flixel::group::FlxGroup_obj super;
		typedef Greenhouse_obj OBJ_;
		Greenhouse_obj();
		Void __construct(::Tile tileSprite);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< Greenhouse_obj > __new(::Tile tileSprite);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~Greenhouse_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("Greenhouse"); }

		::flixel::group::FlxGroup greenHouseGroup;
		::flixel::FlxSprite greenHouseSprite;
		::flixel::FlxSprite greenHouseShrub0;
		::flixel::FlxSprite greenHouseTree;
		::flixel::system::FlxSound windChimeSound;
		virtual Void setGreenHouseInPlay( bool val);
		Dynamic setGreenHouseInPlay_dyn();

		virtual Void update( );

		virtual Void destroy( );

		static bool greenHouseInPlay;
};


#endif /* INCLUDED_Greenhouse */ 
