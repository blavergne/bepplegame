#ifndef INCLUDED_KoiItem
#define INCLUDED_KoiItem

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <MarketItem.h>
HX_DECLARE_CLASS0(KoiItem)
HX_DECLARE_CLASS0(MarketItem)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  KoiItem_obj : public ::MarketItem_obj{
	public:
		typedef ::MarketItem_obj super;
		typedef KoiItem_obj OBJ_;
		KoiItem_obj();
		Void __construct();

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< KoiItem_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~KoiItem_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		::String __ToString() const { return HX_CSTRING("KoiItem"); }

		virtual Void initMarketItems( );
		Dynamic initMarketItems_dyn();

		virtual Void setFishItemCount( int val);
		Dynamic setFishItemCount_dyn();

		virtual Void update( );

};


#endif /* INCLUDED_KoiItem */ 
