#ifndef INCLUDED_JuiceMachine
#define INCLUDED_JuiceMachine

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/group/FlxGroup.h>
HX_DECLARE_CLASS0(JuiceMachine)
HX_DECLARE_CLASS0(Tile)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,group,FlxGroup)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)
HX_DECLARE_CLASS2(flixel,system,FlxSound)


class HXCPP_CLASS_ATTRIBUTES  JuiceMachine_obj : public ::flixel::group::FlxGroup_obj{
	public:
		typedef ::flixel::group::FlxGroup_obj super;
		typedef JuiceMachine_obj OBJ_;
		JuiceMachine_obj();
		Void __construct(::Tile tileSprite);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< JuiceMachine_obj > __new(::Tile tileSprite);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~JuiceMachine_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("JuiceMachine"); }

		::flixel::group::FlxGroup juiceMachineGroup;
		::flixel::FlxSprite juiceMachineSprite;
		::flixel::FlxSprite juiceMachineAlertSprite;
		::flixel::FlxSprite juiceMachineBottleAnim;
		::flixel::FlxSprite juiceMachineFlyingBottle;
		::flixel::FlxSprite minusOneSprite;
		::flixel::FlxSprite repairAlertSprite;
		::flixel::system::FlxSound fillingJuiceBottleSound;
		::flixel::system::FlxSound juiceMachineSound;
		Float myRelativeX;
		Float myRelativeY;
		Float rise;
		Float run;
		Float electricUtilityTimer;
		Float machineReliabilityTimer;
		int goldCount;
		int appleCount;
		bool payElectricBill;
		bool machineOn;
		bool machineAlertReset;
		bool machineClicked;
		bool fillingBottle;
		bool repairClicked;
		bool animateRepairSprite;
		bool bottleFlying;
		bool inJuicePack;
		::Tile myTile;
		bool mathless;
		virtual ::Tile getMyTile( );
		Dynamic getMyTile_dyn();

		virtual bool getInJuicePack( );
		Dynamic getInJuicePack_dyn();

		virtual ::flixel::FlxSprite getJuiceMachineFlyingBotte( );
		Dynamic getJuiceMachineFlyingBotte_dyn();

		virtual ::flixel::FlxSprite getJuiceMachineSprite( );
		Dynamic getJuiceMachineSprite_dyn();

		virtual Float getMyRelativeX( );
		Dynamic getMyRelativeX_dyn();

		virtual Float getMyRelativeY( );
		Dynamic getMyRelativeY_dyn();

		virtual bool getMachineOn( );
		Dynamic getMachineOn_dyn();

		virtual bool getFillingBottle( );
		Dynamic getFillingBottle_dyn();

		virtual bool getPayElectricBill( );
		Dynamic getPayElectricBill_dyn();

		virtual bool getRepairClicked( );
		Dynamic getRepairClicked_dyn();

		virtual bool getAnimateRepairSprite( );
		Dynamic getAnimateRepairSprite_dyn();

		virtual bool getBottleFlying( );
		Dynamic getBottleFlying_dyn();

		virtual Void setAnimateRepairSprite( bool val);
		Dynamic setAnimateRepairSprite_dyn();

		virtual Void setInJuicePack( bool val);
		Dynamic setInJuicePack_dyn();

		virtual Void setRepairClicked( bool val);
		Dynamic setRepairClicked_dyn();

		virtual Void setPayElectricBill( bool val);
		Dynamic setPayElectricBill_dyn();

		virtual Void setFillingBottle( bool val);
		Dynamic setFillingBottle_dyn();

		virtual Void setMachineOn( bool val);
		Dynamic setMachineOn_dyn();

		virtual Void setMyRelativeX( Float val);
		Dynamic setMyRelativeX_dyn();

		virtual Void setMyRelativeY( Float val);
		Dynamic setMyRelativeY_dyn();

		virtual Void setGoldCount( int val);
		Dynamic setGoldCount_dyn();

		virtual Void setAppleCount( int val);
		Dynamic setAppleCount_dyn();

		virtual Void setBottleFlying( bool val);
		Dynamic setBottleFlying_dyn();

		virtual Void registerMachineEvents( );
		Dynamic registerMachineEvents_dyn();

		virtual Void registerMachineButtonEvent( );
		Dynamic registerMachineButtonEvent_dyn();

		virtual Void unregisterMachineEvents( );
		Dynamic unregisterMachineEvents_dyn();

		virtual Void repairAlertClicked( ::flixel::FlxSprite sprite);
		Dynamic repairAlertClicked_dyn();

		virtual Void turnMachineOn( ::flixel::FlxSprite sprite);
		Dynamic turnMachineOn_dyn();

		virtual Void bottleFadeIn( );
		Dynamic bottleFadeIn_dyn();

		virtual Void payElectricUtility( );
		Dynamic payElectricUtility_dyn();

		virtual Void machineReliability( );
		Dynamic machineReliability_dyn();

		virtual Void resetRepairAlertTimer( );
		Dynamic resetRepairAlertTimer_dyn();

		virtual Void repairSpriteAnimation( );
		Dynamic repairSpriteAnimation_dyn();

		virtual Void setRiseRun( );
		Dynamic setRiseRun_dyn();

		virtual Void sendBottleToPack( );
		Dynamic sendBottleToPack_dyn();

		virtual Void update( );

		virtual Void destroy( );

};


#endif /* INCLUDED_JuiceMachine */ 
