#ifndef INCLUDED_GreenHouseItem
#define INCLUDED_GreenHouseItem

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <MarketItem.h>
HX_DECLARE_CLASS0(GreenHouseItem)
HX_DECLARE_CLASS0(MarketItem)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  GreenHouseItem_obj : public ::MarketItem_obj{
	public:
		typedef ::MarketItem_obj super;
		typedef GreenHouseItem_obj OBJ_;
		GreenHouseItem_obj();
		Void __construct();

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< GreenHouseItem_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~GreenHouseItem_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		::String __ToString() const { return HX_CSTRING("GreenHouseItem"); }

		virtual Void initMarketItems( );
		Dynamic initMarketItems_dyn();

		virtual Void setGreenHouseItemCount( int val);
		Dynamic setGreenHouseItemCount_dyn();

		virtual Void update( );

};


#endif /* INCLUDED_GreenHouseItem */ 
