#ifndef INCLUDED_flixel_addons_api_FlxGameJolt
#define INCLUDED_flixel_addons_api_FlxGameJolt

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS0(IMap)
HX_DECLARE_CLASS3(flixel,addons,api,FlxGameJolt)
HX_DECLARE_CLASS2(haxe,ds,StringMap)
HX_DECLARE_CLASS3(openfl,_v2,display,BitmapData)
HX_DECLARE_CLASS3(openfl,_v2,display,IBitmapDrawable)
HX_DECLARE_CLASS3(openfl,_v2,events,Event)
HX_DECLARE_CLASS3(openfl,_v2,events,EventDispatcher)
HX_DECLARE_CLASS3(openfl,_v2,events,IEventDispatcher)
HX_DECLARE_CLASS3(openfl,_v2,net,URLLoader)
namespace flixel{
namespace addons{
namespace api{


class HXCPP_CLASS_ATTRIBUTES  FlxGameJolt_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef FlxGameJolt_obj OBJ_;
		FlxGameJolt_obj();
		Void __construct();

	public:
		inline void *operator new( size_t inSize, bool inContainer=false)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< FlxGameJolt_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~FlxGameJolt_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		::String __ToString() const { return HX_CSTRING("FlxGameJolt"); }

		static int hashType;
		static bool verbose;
		static bool initialized;
		static bool get_initialized( );
		static Dynamic get_initialized_dyn();

		static int HASH_MD5;
		static int HASH_SHA1;
		static int TROPHIES_MISSING;
		static int TROPHIES_ACHIEVED;
		static Dynamic _callBack;
		static int _gameID;
		static ::String _privateKey;
		static ::String _userName;
		static ::String _userToken;
		static ::String _idURL;
		static bool _initialized;
		static bool _verifyAuth;
		static bool _getImage;
		static ::openfl::_v2::net::URLLoader _loader;
		static ::String URL_API;
		static ::String RETURN_TYPE;
		static ::String URL_GAME_ID;
		static ::String URL_USER_NAME;
		static ::String URL_USER_TOKEN;
		static Void init( int GameID,::String PrivateKey,hx::Null< bool >  AutoAuth,::String UserName,::String UserToken,Dynamic Callback);
		static Dynamic init_dyn();

		static Void fetchUser( Dynamic UserID,::String UserName,Array< int > UserIDs,Dynamic Callback);
		static Dynamic fetchUser_dyn();

		static Void authUser( ::String UserName,::String UserToken,Dynamic Callback);
		static Dynamic authUser_dyn();

		static Void openSession( Dynamic Callback);
		static Dynamic openSession_dyn();

		static Void pingSession( hx::Null< bool >  Active,Dynamic Callback);
		static Dynamic pingSession_dyn();

		static Void closeSession( Dynamic Callback);
		static Dynamic closeSession_dyn();

		static Void fetchTrophy( hx::Null< int >  DataType,Dynamic Callback);
		static Dynamic fetchTrophy_dyn();

		static Void addTrophy( int TrophyID,Dynamic Callback);
		static Dynamic addTrophy_dyn();

		static Void fetchScore( Dynamic Limit,Dynamic Callback);
		static Dynamic fetchScore_dyn();

		static Void addScore( ::String Score,Float Sort,Dynamic TableID,hx::Null< bool >  AllowGuest,::String GuestName,::String ExtraData,Dynamic Callback);
		static Dynamic addScore_dyn();

		static Void getTables( Dynamic Callback);
		static Dynamic getTables_dyn();

		static Void fetchData( ::String Key,hx::Null< bool >  User,Dynamic Callback);
		static Dynamic fetchData_dyn();

		static Void setData( ::String Key,::String Value,hx::Null< bool >  User,Dynamic Callback);
		static Dynamic setData_dyn();

		static Void updateData( ::String Key,::String Operation,::String Value,hx::Null< bool >  User,Dynamic Callback);
		static Dynamic updateData_dyn();

		static Void removeData( ::String Key,hx::Null< bool >  User,Dynamic Callback);
		static Dynamic removeData_dyn();

		static Void getAllKeys( hx::Null< bool >  User,Dynamic Callback);
		static Dynamic getAllKeys_dyn();

		static Void sendLoaderRequest( ::String URLString,Dynamic Callback);
		static Dynamic sendLoaderRequest_dyn();

		static Void parseData( ::openfl::_v2::events::Event e);
		static Dynamic parseData_dyn();

		static Void verifyAuthentication( ::haxe::ds::StringMap ReturnMap);
		static Dynamic verifyAuthentication_dyn();

		static Void resetUser( ::String UserName,::String UserToken,Dynamic Callback);
		static Dynamic resetUser_dyn();

		static Void fetchTrophyImage( int ID,Dynamic Callback);
		static Dynamic fetchTrophyImage_dyn();

		static Void fetchAvatarImage( Dynamic Callback);
		static Dynamic fetchAvatarImage_dyn();

		static Void retrieveImage( ::haxe::ds::StringMap ImageMap);
		static Dynamic retrieveImage_dyn();

		static Void returnImage( ::openfl::_v2::events::Event e);
		static Dynamic returnImage_dyn();

		static ::String encryptURL( ::String Url);
		static Dynamic encryptURL_dyn();

		static bool gameInit;
		static bool get_gameInit( );
		static Dynamic get_gameInit_dyn();

		static bool authenticated;
		static bool get_authenticated( );
		static Dynamic get_authenticated_dyn();

		static ::String username;
		static ::String get_username( );
		static Dynamic get_username_dyn();

		static ::String usertoken;
		static ::String get_usertoken( );
		static Dynamic get_usertoken_dyn();

		static bool isQuickPlay;
		static bool get_isQuickPlay( );
		static Dynamic get_isQuickPlay_dyn();

		static bool isEmbeddedFlash;
		static bool get_isEmbeddedFlash( );
		static Dynamic get_isEmbeddedFlash_dyn();

};

} // end namespace flixel
} // end namespace addons
} // end namespace api

#endif /* INCLUDED_flixel_addons_api_FlxGameJolt */ 
