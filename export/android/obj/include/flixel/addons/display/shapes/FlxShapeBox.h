#ifndef INCLUDED_flixel_addons_display_shapes_FlxShapeBox
#define INCLUDED_flixel_addons_display_shapes_FlxShapeBox

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/addons/display/shapes/FlxShape.h>
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS4(flixel,addons,display,shapes,FlxShape)
HX_DECLARE_CLASS4(flixel,addons,display,shapes,FlxShapeBox)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)
HX_DECLARE_CLASS3(openfl,_v2,geom,Matrix)
namespace flixel{
namespace addons{
namespace display{
namespace shapes{


class HXCPP_CLASS_ATTRIBUTES  FlxShapeBox_obj : public ::flixel::addons::display::shapes::FlxShape_obj{
	public:
		typedef ::flixel::addons::display::shapes::FlxShape_obj super;
		typedef FlxShapeBox_obj OBJ_;
		FlxShapeBox_obj();
		Void __construct(Float X,Float Y,Float W,Float H,Dynamic LineStyle_,Dynamic FillStyle_);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< FlxShapeBox_obj > __new(Float X,Float Y,Float W,Float H,Dynamic LineStyle_,Dynamic FillStyle_);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~FlxShapeBox_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		::String __ToString() const { return HX_CSTRING("FlxShapeBox"); }

		Float shapeWidth;
		Float shapeHeight;
		virtual Void drawSpecificShape( ::openfl::_v2::geom::Matrix matrix);

		virtual Float set_shapeWidth( Float f);
		Dynamic set_shapeWidth_dyn();

		virtual Float set_shapeHeight( Float f);
		Dynamic set_shapeHeight_dyn();

};

} // end namespace flixel
} // end namespace addons
} // end namespace display
} // end namespace shapes

#endif /* INCLUDED_flixel_addons_display_shapes_FlxShapeBox */ 
