#ifndef INCLUDED_flixel_addons_display_shapes_FlxShape
#define INCLUDED_flixel_addons_display_shapes_FlxShape

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/FlxSprite.h>
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS4(flixel,addons,display,shapes,FlxShape)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)
HX_DECLARE_CLASS3(openfl,_v2,geom,Matrix)
namespace flixel{
namespace addons{
namespace display{
namespace shapes{


class HXCPP_CLASS_ATTRIBUTES  FlxShape_obj : public ::flixel::FlxSprite_obj{
	public:
		typedef ::flixel::FlxSprite_obj super;
		typedef FlxShape_obj OBJ_;
		FlxShape_obj();
		Void __construct(Float X,Float Y,Float CanvasWidth,Float CanvasHeight,Dynamic LineStyle_,Dynamic FillStyle_,hx::Null< Float >  __o_TrueWidth,hx::Null< Float >  __o_TrueHeight);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< FlxShape_obj > __new(Float X,Float Y,Float CanvasWidth,Float CanvasHeight,Dynamic LineStyle_,Dynamic FillStyle_,hx::Null< Float >  __o_TrueWidth,hx::Null< Float >  __o_TrueHeight);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~FlxShape_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("FlxShape"); }

		Dynamic lineStyle;
		Dynamic fillStyle;
		::String shape_id;
		bool shapeDirty;
		Dynamic _drawStyle;
		virtual Void destroy( );

		virtual Void drawSpecificShape( ::openfl::_v2::geom::Matrix matrix);
		Dynamic drawSpecificShape_dyn();

		virtual Void redrawShape( );
		Dynamic redrawShape_dyn();

		virtual Void draw( );

		virtual Void fixBoundaries( Float trueWidth,Float trueHeight);
		Dynamic fixBoundaries_dyn();

		virtual Dynamic set_lineStyle( Dynamic ls);
		Dynamic set_lineStyle_dyn();

		virtual Dynamic set_fillStyle( Dynamic fs);
		Dynamic set_fillStyle_dyn();

		virtual ::openfl::_v2::geom::Matrix getStrokeOffsetMatrix( ::openfl::_v2::geom::Matrix matrix);
		Dynamic getStrokeOffsetMatrix_dyn();

};

} // end namespace flixel
} // end namespace addons
} // end namespace display
} // end namespace shapes

#endif /* INCLUDED_flixel_addons_display_shapes_FlxShape */ 
