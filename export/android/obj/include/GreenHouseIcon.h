#ifndef INCLUDED_GreenHouseIcon
#define INCLUDED_GreenHouseIcon

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/FlxSprite.h>
HX_DECLARE_CLASS0(GreenHouseIcon)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  GreenHouseIcon_obj : public ::flixel::FlxSprite_obj{
	public:
		typedef ::flixel::FlxSprite_obj super;
		typedef GreenHouseIcon_obj OBJ_;
		GreenHouseIcon_obj();
		Void __construct(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y,bool dragType);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< GreenHouseIcon_obj > __new(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y,bool dragType);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~GreenHouseIcon_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		::String __ToString() const { return HX_CSTRING("GreenHouseIcon"); }

		bool dragging;
		bool dragType;
		bool isMousedOver;
		int homeX;
		int homeY;
		virtual Void onDown( ::flixel::FlxSprite sprite);
		Dynamic onDown_dyn();

		virtual Void onUp( ::flixel::FlxSprite sprite);
		Dynamic onUp_dyn();

		virtual Void dragGreenHouseImage( bool temp);
		Dynamic dragGreenHouseImage_dyn();

		virtual Void resetGreenHouseImagePos( );
		Dynamic resetGreenHouseImagePos_dyn();

		virtual bool getDragging( );
		Dynamic getDragging_dyn();

		virtual Void setDragging( bool val);
		Dynamic setDragging_dyn();

		virtual Void checkMouseState( );
		Dynamic checkMouseState_dyn();

		virtual Void unregisterGreenHouseImageEvent( );
		Dynamic unregisterGreenHouseImageEvent_dyn();

		virtual Void registerGreenHouseImageEvent( );
		Dynamic registerGreenHouseImageEvent_dyn();

		virtual Void update( );

		virtual Void destroy( );

};


#endif /* INCLUDED_GreenHouseIcon */ 
