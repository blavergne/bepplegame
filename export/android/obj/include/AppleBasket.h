#ifndef INCLUDED_AppleBasket
#define INCLUDED_AppleBasket

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/group/FlxGroup.h>
HX_DECLARE_CLASS0(Apple)
HX_DECLARE_CLASS0(AppleBasket)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,group,FlxGroup)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  AppleBasket_obj : public ::flixel::group::FlxGroup_obj{
	public:
		typedef ::flixel::group::FlxGroup_obj super;
		typedef AppleBasket_obj OBJ_;
		AppleBasket_obj();
		Void __construct();

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< AppleBasket_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~AppleBasket_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("AppleBasket"); }

		::flixel::group::FlxGroup basketGroup;
		::flixel::FlxSprite basket;
		int basketFull;
		Array< ::Dynamic > flyingAppleArray;
		bool appleFlying;
		virtual ::flixel::FlxSprite getBasketSprite( );
		Dynamic getBasketSprite_dyn();

		virtual int getBasketFullVal( );
		Dynamic getBasketFullVal_dyn();

		virtual bool getAppleFlying( );
		Dynamic getAppleFlying_dyn();

		virtual ::Apple getAppleSpriteFromBasket( );
		Dynamic getAppleSpriteFromBasket_dyn();

		virtual Void setAppleFlying( bool val);
		Dynamic setAppleFlying_dyn();

		virtual Void initAppleArray( );
		Dynamic initAppleArray_dyn();

		virtual Void animateBasket( int appleCount);
		Dynamic animateBasket_dyn();

		virtual Void update( );

		static Float basketPosX;
		static Float basketPosY;
};


#endif /* INCLUDED_AppleBasket */ 
