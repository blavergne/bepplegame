#ifndef INCLUDED_TileBoard
#define INCLUDED_TileBoard

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/FlxSprite.h>
HX_DECLARE_CLASS0(Tile)
HX_DECLARE_CLASS0(TileBoard)
HX_DECLARE_CLASS0(TileBoarder)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  TileBoard_obj : public ::flixel::FlxSprite_obj{
	public:
		typedef ::flixel::FlxSprite_obj super;
		typedef TileBoard_obj OBJ_;
		TileBoard_obj();
		Void __construct(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< TileBoard_obj > __new(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~TileBoard_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("TileBoard"); }

		Array< ::Dynamic > boardArray;
		Array< ::Dynamic > boardBoarderArray;
		int boardSize;
		int tileNumber;
		virtual Void allocateMem( );
		Dynamic allocateMem_dyn();

		virtual Array< ::Dynamic > getBoardArray( );
		Dynamic getBoardArray_dyn();

		virtual Void destroyAllTiles( );
		Dynamic destroyAllTiles_dyn();

		virtual Array< ::Dynamic > getBoardBoarderArray( );
		Dynamic getBoardBoarderArray_dyn();

};


#endif /* INCLUDED_TileBoard */ 
