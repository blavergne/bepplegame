#ifndef INCLUDED_MenuState
#define INCLUDED_MenuState

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/FlxState.h>
HX_DECLARE_CLASS0(Apple)
HX_DECLARE_CLASS0(MenuState)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS1(flixel,FlxState)
HX_DECLARE_CLASS2(flixel,group,FlxGroup)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)
HX_DECLARE_CLASS2(flixel,text,FlxText)
HX_DECLARE_CLASS2(flixel,ui,FlxButton)
HX_DECLARE_CLASS2(flixel,ui,FlxTypedButton)


class HXCPP_CLASS_ATTRIBUTES  MenuState_obj : public ::flixel::FlxState_obj{
	public:
		typedef ::flixel::FlxState_obj super;
		typedef MenuState_obj OBJ_;
		MenuState_obj();
		Void __construct(Dynamic MaxSize);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< MenuState_obj > __new(Dynamic MaxSize);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~MenuState_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("MenuState"); }

		::flixel::FlxSprite backgroundSprite;
		::flixel::ui::FlxButton playBtn;
		::flixel::ui::FlxButton tutButton;
		::flixel::ui::FlxButton mathlessBepple;
		::flixel::ui::FlxButton exitButton;
		::flixel::text::FlxText gameTitle;
		::flixel::FlxSprite beppleTitle;
		::Apple apple_0;
		::Apple apple_1;
		::Apple apple_2;
		::Apple apple_3;
		::Apple apple_4;
		Float apple_0Timer;
		Float apple_1Timer;
		Float apple_2Timer;
		Float apple_3Timer;
		Float apple_4Timer;
		bool apple_0Created;
		bool apple_1Created;
		bool apple_2Created;
		bool apple_3Created;
		bool apple_4Created;
		virtual Void create( );

		virtual bool getTutorialOn( );
		Dynamic getTutorialOn_dyn();

		virtual Void appleMachine( );
		Dynamic appleMachine_dyn();

		virtual Void destroy( );

		virtual Void update( );

		virtual Void startGame( );
		Dynamic startGame_dyn();

		virtual Void exitGame( );
		Dynamic exitGame_dyn();

		virtual Void startTutorial( );
		Dynamic startTutorial_dyn();

		virtual Void startMathless( );
		Dynamic startMathless_dyn();

		static bool mathless;
		static bool tutorialOn;
		static int windowLength;
		static int windowWidth;
};


#endif /* INCLUDED_MenuState */ 
