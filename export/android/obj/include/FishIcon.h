#ifndef INCLUDED_FishIcon
#define INCLUDED_FishIcon

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/FlxSprite.h>
HX_DECLARE_CLASS0(FishIcon)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  FishIcon_obj : public ::flixel::FlxSprite_obj{
	public:
		typedef ::flixel::FlxSprite_obj super;
		typedef FishIcon_obj OBJ_;
		FishIcon_obj();
		Void __construct(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y,bool dragType);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< FishIcon_obj > __new(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y,bool dragType);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~FishIcon_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		::String __ToString() const { return HX_CSTRING("FishIcon"); }

		bool dragging;
		bool dragType;
		bool isMousedOver;
		int homeX;
		int homeY;
		virtual Void onDown( ::flixel::FlxSprite sprite);
		Dynamic onDown_dyn();

		virtual Void onUp( ::flixel::FlxSprite sprite);
		Dynamic onUp_dyn();

		virtual Void dragSeed( bool temp);
		Dynamic dragSeed_dyn();

		virtual Void resetSeedPos( );
		Dynamic resetSeedPos_dyn();

		virtual bool getDragging( );
		Dynamic getDragging_dyn();

		virtual Void setDragging( bool val);
		Dynamic setDragging_dyn();

		virtual Void checkMouseState( );
		Dynamic checkMouseState_dyn();

		virtual Void unregisterFishEvent( );
		Dynamic unregisterFishEvent_dyn();

		virtual Void registerFishEvent( );
		Dynamic registerFishEvent_dyn();

		virtual Void update( );

		virtual Void destroy( );

};


#endif /* INCLUDED_FishIcon */ 
