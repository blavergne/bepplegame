#ifndef INCLUDED_Island
#define INCLUDED_Island

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/group/FlxGroup.h>
HX_DECLARE_CLASS0(Island)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS2(flixel,group,FlxGroup)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  Island_obj : public ::flixel::group::FlxGroup_obj{
	public:
		typedef ::flixel::group::FlxGroup_obj super;
		typedef Island_obj OBJ_;
		Island_obj();
		Void __construct();

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< Island_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~Island_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("Island"); }

		::flixel::group::FlxGroup islandGroup;
		Array< ::Dynamic > islandArray;
		::flixel::FlxSprite islandA;
		::flixel::FlxSprite islandB;
		::flixel::FlxSprite islandC;
		::flixel::FlxSprite islandD;
		::flixel::FlxSprite islandE;
		::flixel::FlxSprite islandF;
		::flixel::FlxSprite islandG;
		::flixel::FlxSprite islandH;
		::flixel::FlxSprite islandI;
		::flixel::FlxSprite islandJ;
		::flixel::FlxSprite islandK;
		int randomIslandIndex;
		int randomYPosition;
		Float randomIslandSpeed;
		::flixel::FlxSprite chosenIsland;
		virtual Void init( );
		Dynamic init_dyn();

		virtual ::flixel::FlxSprite getIslandSprite( );
		Dynamic getIslandSprite_dyn();

		virtual Void islandSpeed( );
		Dynamic islandSpeed_dyn();

		virtual Void destroyIsland( );
		Dynamic destroyIsland_dyn();

		virtual Void update( );

		virtual Void destroy( );

};


#endif /* INCLUDED_Island */ 
