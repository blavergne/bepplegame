package ;

import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.FlxG;
import flixel.group.FlxSpriteGroup;
import flixel.plugin.MouseEventManager;
/**
 * ...
 * @author BrioLavone
 */
class Tutorial extends FlxGroup
{

	private var tutMessageArray:Array<FlxSprite>;
	private var tutorialGroup:FlxGroup;
	private var tutHand:FlxSprite;
	private var tutSeed:FlxSprite;
	private var tutMessageOne:FlxSprite;
	private var tutMessageTwo:FlxSprite;
	private var tutMessageThree:FlxSprite;
	private var tutMessageFour:FlxSprite;
	private var tutMessageFive:FlxSprite;
	private var tutMessageSix:FlxSprite;
	private var tutMessageSeven:FlxSprite;
	private var tutMessageEight:FlxSprite;
	private var tutMessageNine:FlxSprite;
	private var tutMessageTen:FlxSprite;
	private var tutTeacher:FlxSprite;
	private var tutTimerOne:Float = 1;
	private var tutTimerTwo:Float = 1;
	private var fingerPressTimer:Float = 1;
	private var fingerHoverTimer:Float = 1;
	private var tutSectionNumber:Float;
	private var messageFadeDirection:Int = 0;
	private var currentMessageIndex:Int = 0;
	private var tutTreeGrowthVal:Int;
	private var handMovingToSeed:Bool = false;
	private var moveHandWithSeed:Bool = false;
	private var handMovingToTile:Bool = false;
	
	public function new(tutorialType:Float) 
	{
		super();
		init(tutorialType);
		
	}
	
	public function getCurrentTutSectionNumber() {
		return tutSectionNumber;
	}
	
	private function init(tutType:Float) {
		tutMessageArray = new Array<FlxSprite>();
		tutorialGroup = new FlxGroup();
		tutHand = new FlxSprite();
		tutSeed = new FlxSprite();
		tutMessageOne = new FlxSprite();
		tutMessageTwo = new FlxSprite();
		tutMessageThree = new FlxSprite();
		tutMessageFour = new FlxSprite();
		tutMessageFive = new FlxSprite();
		tutMessageSix = new FlxSprite();
		tutMessageSeven = new FlxSprite();
		tutMessageEight = new FlxSprite();
		tutMessageNine = new FlxSprite();
		tutMessageTen = new FlxSprite();
		tutHand.loadGraphic(AssetPaths.tutHandAnimation__png,true , 30, 28);
		tutHand.animation.add("handHover", [0], 1, false);
		tutHand.animation.add("handClick", [1], 1, false);
		tutSeed.loadGraphic(AssetPaths.tree_seed__png, false);
		tutMessageOne.loadGraphic(AssetPaths.tutMessageOne__png, false);
		tutMessageTwo.loadGraphic(AssetPaths.tutMessageTwo__png, false);
		tutMessageThree.loadGraphic(AssetPaths.tutMessageThree__png, false);
		tutMessageFour.loadGraphic(AssetPaths.tutMessageFour__png, false);
		tutMessageFive.loadGraphic(AssetPaths.tutMessageFive__png, false);
		tutMessageSix.loadGraphic(AssetPaths.tutMessageSix__png, false);
		tutMessageSeven.loadGraphic(AssetPaths.tutMessageSeven__png, false);
		tutMessageEight.loadGraphic(AssetPaths.tutMessageEight__png, false);
		tutMessageNine.loadGraphic(AssetPaths.tutMessageNine__png, false);
		tutMessageTen.loadGraphic(AssetPaths.tutMessageTen__png, false);
		tutMessageArray[0] = tutMessageOne;
		tutMessageArray[1] = tutMessageTwo;
		tutMessageArray[2] = tutMessageThree;
		tutMessageArray[3] = tutMessageFour;
		tutMessageArray[4] = tutMessageFive;
		tutMessageArray[5] = tutMessageSix;
		tutMessageArray[6] = tutMessageSeven;
		tutMessageArray[7] = tutMessageEight;
		tutMessageArray[8] = tutMessageNine;
		tutMessageArray[9] = tutMessageTen;
		tutHandPos(tutType);
		tutMessagePos();
		tutHand.alpha = 0;
		tutSeed.alpha = 0;
		tutMessageOne.alpha = 0;
		tutMessageTwo.alpha = 0;
		tutMessageThree.alpha = 0;
		tutMessageThree.alpha = 0;
		tutMessageFour.alpha = 0;
		tutMessageFive.alpha = 0;
		tutMessageSix.alpha = 0;
		tutMessageSeven.alpha = 0;
		tutMessageEight.alpha = 0;
		tutMessageNine.alpha = 0;
		tutMessageTen.alpha = 0;
		eventInit();
		tutorialGroup.add(tutHand);
		tutorialGroup.add(tutSeed);
		setTutType(tutType);
		if(MenuState.tutorialOn){
			add(tutorialGroup);
		}
		tutSectionNumber = tutType;
	}
	
	//public member functions
	//
	//
	
	//get tutorial tree growthPhase value to signal tutMessageFour section
	public function getTutTreeGrowthVal(growthValue:Int) {
		tutTreeGrowthVal = growthValue;
	}
	
	//private member functions
	//
	//
	private function eventInit() {
		MouseEventManager.add(tutMessageOne, nextTutMessage, null, null, null);
		MouseEventManager.add(tutMessageSix, nextTutMessage, null, null, null);
		MouseEventManager.add(tutMessageFour, destroyCurrentMessage, null, null, null);
		MouseEventManager.add(tutMessageSeven, destroyCurrentMessage, null, null, null);
		MouseEventManager.add(tutMessageEight, nextTutMessage, null, null, null);
		MouseEventManager.add(tutMessageNine, destroyCurrentMessage, null, null, null);
		MouseEventManager.add(tutMessageTen, destroyCurrentMessage, null, null, null);
	}
	
	private function nextTutMessage(sprite:FlxSprite) {
		tutMessageArray[currentMessageIndex].destroy();
		currentMessageIndex++;
	}
	
	private function setTutType(tutType:Float) {
		if(MenuState.tutorialOn){
			if(tutType == 0){	
				tutorialGroup.add(tutMessageArray[0]);
				tutorialGroup.add(tutMessageArray[1]);
			}
			else if (tutType == 1) {
				currentMessageIndex = 2;
				tutorialGroup.add(tutMessageArray[currentMessageIndex]);
			}
			else if (tutType == 2) {
				currentMessageIndex = 3;
				tutorialGroup.add(tutMessageArray[currentMessageIndex]);
			}
			else if (tutType == 3) {
				currentMessageIndex = 4;
				tutorialGroup.add(tutMessageArray[currentMessageIndex]);
			}
			else if (tutType == 4) {
				currentMessageIndex = 5;
				tutorialGroup.add(tutMessageArray[currentMessageIndex]);
				tutorialGroup.add(tutMessageArray[currentMessageIndex+1]);
			}
			else if (tutType == 1.5) {
				currentMessageIndex = 7;
				tutorialGroup.add(tutMessageArray[currentMessageIndex]);
				tutorialGroup.add(tutMessageArray[currentMessageIndex+1]);
			}
			else if (tutType == 5) {
				currentMessageIndex = 9;
				tutorialGroup.add(tutMessageArray[currentMessageIndex]);
			}
		}
	}
	
	private function destroyCurrentMessage(sprite:FlxSprite) {
		if(tutSectionNumber == 2){
			tutMessageArray[3].destroy();
		}
		if(tutSectionNumber == 3){
			tutMessageArray[4].destroy();
		}
		if(tutSectionNumber == 4){
			tutMessageArray[6].destroy();
		}
		if(tutSectionNumber == 1.5){
			tutMessageArray[8].destroy();
		}
		if (tutSectionNumber == 5) {
			tutMessageArray[9].destroy();
		}
	}
	
	//set initial positions for tutHands depending on type
	private function tutHandPos(tutType:Float) {
		if (tutType == 0) {
			tutHand.x = 200;
			tutHand.y = 200;
		}
		else if(tutType == 1){
			tutHand.x = 210;
			tutHand.y = 255;
		}
		else if (tutType == 3) {
			tutHand.x = 25;
			tutHand.y = 230;
		}
		else if (tutType == 4) {
			tutHand.x = 625;
			tutHand.y = 50;
		}
	}
	
	private function tutMessagePos() {
		tutMessageOne.x = 350;
		tutMessageOne.y = 20;
		tutMessageTwo.x = 350;
		tutMessageTwo.y = 20;
		tutMessageThree.x = 350;
		tutMessageThree.y = 20;
		tutMessageFour.x = 350;
		tutMessageFour.y = 20;
		tutMessageFive.x = 350;
		tutMessageFive.y = 20;
		tutMessageSix.x = 350;
		tutMessageSix.y = 20;
		tutMessageSeven.x = 350;
		tutMessageSeven.y = 20;
		tutMessageEight.x = 350;
		tutMessageEight.y = 20;
		tutMessageNine.x = 350;
		tutMessageNine.y = 20;
		tutMessageTen.x = 350;
		tutMessageTen.y = 20;
	}
	
	private function tutorialRun() {
		messageFade(messageFadeDirection);
		if (tutSectionNumber == 0) {
			tutTimerOne -= FlxG.elapsed;
			if (tutTimerOne <= 0 && tutMessageTwo.alpha >= 1) {
				tutHandFade(0);
			}
			if (handMovingToTile) {
				grabSeed();
			}
		}
		if (tutSectionNumber == 1) {
			tutTimerTwo -= FlxG.elapsed;
			if (tutTimerTwo <= 0) {
				tutHandFade(0);
			}
		}
		if (tutSectionNumber == 2) {
			currentMessageIndex = 3;
		}
		if (tutSectionNumber == 3) {
			currentMessageIndex = 4;
			handClickTruckButton();
		}
		if (tutSectionNumber == 4 && currentMessageIndex == 6) {
			handClickShopTab();
		}
	}
	
	private function grabSeed() {
		tutSeed.x = tutHand.x - 8;
		tutSeed.y = tutHand.y - 6;
		tutSeed.alpha = .5;
		handMovingToTile = true;
	}
	
	private function tutHandFade(fadeDirection:Int) {
		if (fadeDirection == 0) {
			if (tutHand.alpha < 1) {
				tutHand.alpha += .01;	
			}
		}
		else {
			if (tutHand.alpha > 0) {
				tutHand.alpha -= .06;
			}
		}		
	}
	
	private function messageFade(messageFadeDirection:Int) {
		if(messageFadeDirection == 0){
			if (tutMessageArray[currentMessageIndex].alpha < 1) {
				tutMessageArray[currentMessageIndex].alpha += .01;
			}
		}
		else {
			if (tutMessageArray[currentMessageIndex].alpha > 0) {
				tutMessageArray[currentMessageIndex].alpha -= .03;
			}
		}
	}
	
	private function handClickTruckButton() {
		tutHandFade(0);
		if (tutHand.alpha >= 1) {
			if (tutHand.x <= 60) {
				tutHand.x += 1;
			}
			if (tutHand.x >= 60) {
				fingerHoverTimer -= FlxG.elapsed;
				if(fingerHoverTimer <= 0){
					fingerPressTimer -= FlxG.elapsed;
					tutHand.animation.play("handClick");
					if (fingerPressTimer <= 0) {
						tutHand.animation.play("handHover");
						fingerPressTimer = 1;
						fingerHoverTimer = 1;
						tutHand.x = 25;
					}
				}
			}
		}
	}
	
	private function handClickShopTab() {
		tutHandFade(0);
		if (tutHand.alpha >= 1 && currentMessageIndex == 6) {
			if (tutHand.y >= 15) {
				tutHand.y -= 1;
			}
			if (tutHand.y <= 15) {
				fingerHoverTimer -= FlxG.elapsed;
				if(fingerHoverTimer <= 0){
					fingerPressTimer -= FlxG.elapsed;
					tutHand.animation.play("handClick");
					if (fingerPressTimer <= 0) {
						tutHand.animation.play("handHover");
						fingerPressTimer = 1;
						fingerHoverTimer = 1;
						tutHand.y = 50;
					}
				}
			}
		}
		
	}
	
	private function moveToSeed() {		
		if (tutHand.alpha >= 1 && tutMessageTwo.alpha >= 1 && tutSectionNumber == 0) {
			if (tutHand.x >= 145 && !handMovingToTile) {
				tutHand.x -= .5;
				tutHand.animation.play("handHover");
			}
			if (tutHand.y >= 85 && !handMovingToTile) {
				tutHand.y -= 1.5;
			}
			if (tutHand.x <= 145 && tutHand.y <= 85) {
				handMovingToTile = true;
			}
		}
	}
	
	private function moveToTile() {
		if (tutSectionNumber == 0) {
			
			if (tutHand.x <= 200 && handMovingToTile) {
					tutHand.x += 1.3;
					tutHand.animation.play("handClick");
				}
			if (tutHand.y <= 200 && handMovingToTile) {
				tutHand.y += 1.3;
			}
			if (tutHand.x >= 200 && tutHand.y >= 200 && handMovingToTile) {
				tutSeed.alpha = 0;
				handMovingToTile = false;
				tutTimerOne = 2;
			}
		}
	}
	
	private function moveToDropAlert() {		
		if(tutHand.alpha >= 1 && tutSectionNumber == 1){
			if (tutHand.y >= 230) {
				tutHand.y -= 1;
			}
			if (tutHand.y <= 230) {
				fingerHoverTimer -= FlxG.elapsed;
				if(fingerHoverTimer <= 0){
					fingerPressTimer -= FlxG.elapsed;
					tutHand.animation.play("handClick");
					if (fingerPressTimer <= 0) {
						tutHand.animation.play("handHover");
						fingerPressTimer = 1;
						fingerHoverTimer = 1;
						tutHand.y = 255;
					}
				}
			}
		}
	}
	
	override function update() {
		tutorialRun();
		moveToSeed();
		moveToTile();
		moveToDropAlert();
		super.update();
	}
	
	override function destroy() {
		MouseEventManager.remove(tutMessageOne);
		MouseEventManager.remove(tutMessageFour);
		MouseEventManager.remove(tutMessageSix);
		MouseEventManager.remove(tutMessageSeven);
		MouseEventManager.remove(tutMessageEight);
		MouseEventManager.remove(tutMessageNine);
		MouseEventManager.remove(tutMessageTen);
		tutorialGroup = null;
		tutHand = null;
		tutSeed = null;
		tutMessageArray = null;
		tutTeacher = null;
		tutMessageOne = null;
		tutMessageTwo = null;
		tutMessageThree = null;
		tutMessageFour = null;
		tutMessageFive = null;
		tutMessageSix = null;
		tutMessageSeven = null;
		tutMessageEight = null;
		tutMessageNine = null;
		tutMessageTen = null;
		destroy;
		super.destroy();
	}
	
}