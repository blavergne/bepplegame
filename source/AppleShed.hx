package ;

import flixel.FlxSprite;
import flixel.group.FlxGroup;

/**Apple shed object mainly used for visual representation of apple gathering and selling
 * ...
 * @author ...
 */
class AppleShed extends FlxGroup
{

	private var appleShedGroup:FlxGroup;
	private var appleShedSprite:FlxSprite;
	private var maxShedLevel:Int = 3;
	
	public function new() 
	{
		
		super();
		appleShedGroup = new FlxGroup();
		appleShedSprite = new FlxSprite();
		appleShedSprite.loadGraphic(AssetPaths.appleShedAnimation__png, true, 91, 70);
		appleShedSprite.animation.add("shedEmpty", [0], 1, false);
		appleShedSprite.animation.add("shedOne", [1], 1, false);
		appleShedSprite.animation.add("shedTwo", [2], 1, false);
		appleShedSprite.animation.add("shedThree", [3], 1, false);
		appleShedSprite.x = 80;
		appleShedSprite.y = 220;
		appleShedGroup.add(appleShedSprite);
		add(appleShedGroup);
		
	}
	
	public function animateShed(appleCount:Int, basketFull:Int) {
		if (appleCount >= Math.floor(basketFull*.3)) {
			appleShedSprite.animation.play("shedOne");
		}
		if (appleCount >= Math.floor(basketFull*.7)) {
			appleShedSprite.animation.play("shedTwo");
		}
		if (appleCount >= basketFull) {
			appleShedSprite.animation.play("shedThree");
		}
		if(appleCount < Math.floor(basketFull*.3)){
			appleShedSprite.animation.play("shedEmpty");
		}
	}
	
	override public function update() {
	
		super.update();
	}
	
}