package ;

import flixel.FlxSprite;
import flixel.FlxG;
import openfl.events.MouseEvent;

/**
 * ...
 * @author BrioLavone
 */
class TileBoarder extends FlxSprite
{
	
	var boarderStatus:String = "off";
	
	public function new(X:Float=0, Y:Float=0) 
	{
		super(X, Y);
		loadGraphic(AssetPaths.gameTileBoarder__png, false);
		this.x = 223;
		this.y = 120;
		alpha = 0.0;
		
	}
	
	public function boarderSwitch(val:String) {
		
		switch(val) {
			
			case "on":
				alpha = 1.0;
				boarderStatus = "on";
			case "off":
				alpha = 0.0;
				boarderStatus = "off";
			
		}
		
	}
	
	public function getBoarderStatus():String {
		
		return boarderStatus;
		
	}
	
	override public function draw():Void {
		
		super.draw();
		
	}
	
	override public function update():Void {
		
		super.update();
		
	}
	
}