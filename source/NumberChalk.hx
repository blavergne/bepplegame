package ;

import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.util.FlxRandom;

/**
 * ...
 * @author ...
 */
class NumberChalk extends FlxGroup
{

	private var myRandNum:FlxSprite;
	private var myIndex:Int;
	private var c0:FlxSprite;
	private var c1:FlxSprite;
	private var c2:FlxSprite;
	private var c3:FlxSprite;
	private var c4:FlxSprite;
	private var c5:FlxSprite;
	private var c6:FlxSprite;
	private var c7:FlxSprite;
	private var c8:FlxSprite;
	private var c9:FlxSprite;
	private var numRandArray:Array<FlxSprite>;
	
	public function new() 
	{
		super();
		init();
		
	}
	
	private function init() {
		
		numRandArray = new Array<FlxSprite>();
		myRandNum = new FlxSprite();
		c0 = new FlxSprite();
		c1 = new FlxSprite();
		c2 = new FlxSprite();
		c3 = new FlxSprite();
		c4 = new FlxSprite();
		c5 = new FlxSprite();
		c6 = new FlxSprite();
		c7 = new FlxSprite();
		c8 = new FlxSprite();
		c9 = new FlxSprite();
		c0.loadGraphic(AssetPaths.chalkNumbers_0__png, false);
		c1.loadGraphic(AssetPaths.chalkNumbers_1__png, false);
		c2.loadGraphic(AssetPaths.chalkNumbers_2__png, false);
		c3.loadGraphic(AssetPaths.chalkNumbers_3__png, false);
		c4.loadGraphic(AssetPaths.chalkNumbers_4__png, false);
		c5.loadGraphic(AssetPaths.chalkNumbers_5__png, false);
		c6.loadGraphic(AssetPaths.chalkNumbers_6__png, false);
		c7.loadGraphic(AssetPaths.chalkNumbers_7__png, false);
		c8.loadGraphic(AssetPaths.chalkNumbers_8__png, false);
		c9.loadGraphic(AssetPaths.chalkNumbers_9__png, false);
		c1.scale.y = .6;
		c1.scale.x = .6;
		c2.scale.y = .6;
		c2.scale.x = .6;
		c3.scale.y = .6;
		c3.scale.x = .6;
		c4.scale.y = .6;
		c4.scale.x = .6;
		c5.scale.y = .6;
		c5.scale.x = .6;
		c6.scale.y = .6;
		c6.scale.x = .6;
		c7.scale.y = .6;
		c7.scale.x = .6;
		c8.scale.y = .6;
		c8.scale.x = .6;
		c9.scale.y = .6;
		c9.scale.x = .6;
		c0.scale.y = .6;
		c0.scale.x = .6;
		numRandArray[0] = c0; 
		numRandArray[1] = c1;
		numRandArray[2] = c2;
		numRandArray[3] = c3;
		numRandArray[4] = c4;
		numRandArray[5] = c5;
		numRandArray[6] = c6;
		numRandArray[7] = c7;
		numRandArray[8] = c8;
		numRandArray[9] = c9;
		
	}
	
	public function getRandomNumber() {
		
		myIndex = FlxRandom.intRanged(0, 9);
		myRandNum = numRandArray[myIndex];
		return myRandNum;
		
	}
	
	public function getMyIndex() {
		
		return myIndex;
		
	}

	override public function destroy() {
		numRandArray = null;
		myRandNum = null;
		c0 = null;
		c1 = null;
		c2 = null;
		c3 = null;
		c4 = null;
		c5 = null;
		c6 = null;
		c7 = null;
		c8 = null;
		c9 =  null;
		super.destroy();
	}
	
}