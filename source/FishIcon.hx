package ;

import flixel.FlxSprite;
import flixel.plugin.MouseEventManager;
import flixel.FlxG;

/**
 * ...
 * @author BrioLavone
 */
class FishIcon extends FlxSprite
{

	private var dragging:Bool = false;
	private var dragType:Bool;
	private var isMousedOver:Bool = false;
	private var homeX:Int = -99;
	private var homeY:Int = 72;
	
	public function new(X:Float=0, Y:Float=0, dragType:Bool) 
	{
		super(X, Y);
		this.dragType = dragType;
		loadGraphic(AssetPaths.fishIcon__png,false, 15, 19);
		this.x = homeX;
		this.y = homeY;
		if (dragType) {
			homeX = 66;
			homeY = 72;
		}
		MouseEventManager.add(this, onDown, onUp, null, null);
	}
	
	private function onDown(sprite:FlxSprite) {
		
		dragging = true;
		
	}

	private function onUp(sprite:FlxSprite) {
		
		dragging = false;
		resetSeedPos();
		
	}
	
	private function dragSeed(temp) {
		
		if (temp) {
			
			this.x = FlxG.mouse.x - 5;
			this.y = FlxG.mouse.y - 8;
			
		}
			
	}
	
	public function resetSeedPos() {
		if(dragType){
			this.x = homeX;
			this.y = homeY;
		}
	}
	
	public function getDragging() {
		
		return dragging;
		
	}
	
	public function setDragging(val:Bool) {
		
		dragging = val;
		
	}
	
	public function checkMouseState() {
		
		if (FlxG.mouse.justReleased) {
			
			dragging = false;
			resetSeedPos();
			
		}
		
	}
	
	public function unregisterFishEvent() {
		MouseEventManager.remove(this);
	}
	
	public function registerFishEvent() {
		MouseEventManager.add(this, onDown, onUp, null, null);
	}
	
	override public function update():Void {
		
		checkMouseState();
		dragSeed(dragging);
		super.update();
		
	}
	
	override public function destroy():Void {
		
		MouseEventManager.remove(this);
		super.destroy();
		
	}
	
}