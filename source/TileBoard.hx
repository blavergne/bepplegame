package ;

import flixel.FlxSprite;

/**
 * ...
 * @author BrioLavone
 */
class TileBoard extends FlxSprite
{

	private var boardArray:Array<Array<Tile>>;
	private var boardBoarderArray:Array<Array<TileBoarder>>;
	private var boardSize:Int = 3;
	private var tileNumber:Int = 0;
	
	public function new(X:Float=0, Y:Float=0) 
	{
		super(X, Y);
		
		allocateMem();
		
	}
	
	private function allocateMem() {
		
		boardArray = new Array<Array<Tile>>();
		boardBoarderArray = new Array<Array<TileBoarder>>();
		
		for (i in 0...boardSize) {
			
			boardArray[i] = new Array();
			boardBoarderArray[i] = new Array();
			
			for (j in 0...boardSize) {
				
				(boardArray[i])[j] = new Tile();
				(boardBoarderArray[i])[j] = new TileBoarder();
				
			}
		}
		
	}
	
	public function getBoardArray(){
		
		return boardArray;
		
	}
	
	public function destroyAllTiles() {
		for (i in 0...boardSize) {
			for (j in 0...boardSize) {
				(boardArray[i])[j] = null;
				(boardBoarderArray[i])[j] = null;
			}
		}
	}
	
	public function getBoardBoarderArray() {
		
		return boardBoarderArray;
		
	}
	
}