package ;

import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.plugin.MouseEventManager;
import flixel.system.FlxSound;
import flixel.text.FlxText;
import flixel.FlxG;
import flixel.util.FlxDestroyUtil;
/**
 * ...
 * @author ...
 */
class Market extends FlxGroup
{

	private var marketGroup:FlxGroup;
	private var marketSideBarSprite:FlxSprite;
	private var marketSideBarTabSprite:FlxSprite;
	private var marketSideBarShaftSprite:FlxSprite;
	private var seedItem:SeedItem;
	private var juiceItem:JuiceItem;
	private var pieItem:PieItem;
	private var koiFishItem:KoiItem;
	private var greenHouseItem:GreenHouseItem;
	private var seedsAvailableText:FlxText;
	private var greenHouseItemCountFieldText:FlxText;
	private var fishItemCountFieldText:FlxText;
	private var seedItemCountFieldText:FlxText;
	private var juiceItemCountFieldText:FlxText;
	private var pieItemCountFieldText:FlxText;
	private var greenHouseItemCostFieldText:FlxText;
	private var seedItemCostFieldText:FlxText;
	private var juiceItemCostFieldText:FlxText;
	private var pieItemCostFieldText:FlxText;
	private var fishItemCostFieldText:FlxText;
	private var juicePackCountText:FlxText;
	private var piePackCountText:FlxText;
	private var itemArray:Array<MarketItem>;
	private var coin:GoldCoin;
	private var coinCount:Int = 0;
	private var coinCountString:String = "";
	private var coinCountFieldText:FlxText;
	private var piePack:Int = 0;
	private var juicePack:Int = 0;
	private var seedCount:Int = 1;
	private var juiceCount:Int = 0;
	private var pieCount:Int = 0;
	private var fishCount:Int = 0;
	private var greenHouseCount:Int = 0;
	private var waterClientCount:Int = 0;
	private var electricClientCount:Int = 0;
	private var shiftX:Int = 41;
	private	var shiftY:Int = 2;
	private var seedCountString:String = "";
	private var seedCountText:FlxText;
	private var juiceCountText:FlxText;
	private var fishCountText:FlxText;
	private var pieCountText:FlxText;
	private var greenHouseCountText:FlxText;
	private var marketCloseTimer:Float = 10;
	private var replenishSeedTimer:Float = 180;
	private var replenishJuiceMachineTimer:Float = 300;
	private var replenishPieMachineSeedTimer:Float = 300;
	private var waterBillTimer:Float = 30;
	private var electricBillTimer:Float = 10;
	private var marketOpen:Bool = false;
	private var turn:Bool = true;
	private var closedSoundPlayed:Bool = false;
	private var marketOpenSound:FlxSound;
	private var marketClosedSound:FlxSound;
	private var purchaseItemSound:FlxSound;
	
	public function new() 
	{
		super();
		marketGroup = new FlxGroup();
		marketSideBarSprite = new FlxSprite();
		marketSideBarTabSprite = new FlxSprite();
		marketSideBarShaftSprite = new FlxSprite();
		seedItem = new SeedItem();
		juiceItem = new JuiceItem();
		pieItem = new PieItem();
		coin = new GoldCoin();
		koiFishItem = new KoiItem();
		greenHouseItem = new GreenHouseItem();
		seedCountText = new FlxText(-21, 90, 16, seedCountString, 8, false);
		juiceCountText = new FlxText(-45, 90, 16, Std.string(juiceCount), 8, false);
		pieCountText = new FlxText( -71, 90, 16, Std.string(pieCount), 8, false);
		fishCountText = new FlxText( -97, 90, 16, Std.string(fishCount), 8, false);
		greenHouseCountText = new FlxText( -119, 90, 16, Std.string(greenHouseCount), 8, false);
		juicePackCountText = new FlxText(180, 43, 32, Std.string(juicePack), 12, false);
		piePackCountText = new FlxText(237, 43, 32, Std.string(juicePack), 12, false);
		seedCountText.color = 0xFFFFFFF;
		juiceCountText.color = 0xFFFFFFF;
		pieCountText.color = 0xFFFFFFF;
		fishCountText.color = 0xFFFFFFF;
		piePackCountText.color = 0xFFFFFFF;
		juicePackCountText.color = 0xFFFFFFF;
		greenHouseCountText.color = 0xFFFFFFF;
		coinCountFieldText = new FlxText(53, 38, 64, coinCountString, 16, false);
		coinCountFieldText.color = 0xFFFFFFF;
		itemArray = new Array<MarketItem>();
		marketSideBarSprite.loadGraphic(AssetPaths.marketSideBar__png, false);
		marketSideBarTabSprite.loadGraphic(AssetPaths.marketSideBarTab__png, false);
		marketSideBarShaftSprite.loadGraphic(AssetPaths.marketSideBarShaft__png, false);
		marketSideBarTabSprite.x = 603;
		marketSideBarShaftSprite.x = 640;
		seedItemCountFieldText = new FlxText(marketSideBarTabSprite.x + 60, marketSideBarTabSprite.y + 2, 32, Std.string(seedItem.getItemCount()), 8);
		juiceItemCountFieldText = new FlxText(marketSideBarTabSprite.x + 60, marketSideBarTabSprite.y + 38, 32, Std.string(juiceItem.getItemCount()), 8);
		pieItemCountFieldText = new FlxText(marketSideBarTabSprite.x + 60, marketSideBarTabSprite.y + 74, 32, Std.string(juiceItem.getItemCount()), 8);
		fishItemCountFieldText = new FlxText(marketSideBarTabSprite.x + 60, marketSideBarTabSprite.y + 110, 32, Std.string(koiFishItem.getItemCount()), 8);
		greenHouseItemCountFieldText = new FlxText(marketSideBarTabSprite.x+60, marketSideBarTabSprite.y + 146, 32, Std.string(greenHouseItem.getItemCount()), 8);
		seedItemCostFieldText = new FlxText(marketSideBarTabSprite.x+42, marketSideBarTabSprite.y+2, 32, Std.string(seedItem.getItemCost()), 8);
		juiceItemCostFieldText = new FlxText(marketSideBarTabSprite.x + 41, marketSideBarTabSprite.y+38, 32, Std.string(juiceItem.getItemCost()), 8);
		pieItemCostFieldText = new FlxText(marketSideBarTabSprite.x + 41, marketSideBarTabSprite.y + 74, 32, Std.string(juiceItem.getItemCost()), 8);
		fishItemCostFieldText = new FlxText(marketSideBarTabSprite.x + 41, marketSideBarTabSprite.y + 110, 32, Std.string(koiFishItem.getItemCost()), 8);
		greenHouseItemCostFieldText = new FlxText(marketSideBarTabSprite.x + 41, marketSideBarTabSprite.y + 146, 32, Std.string(greenHouseItem.getItemCost()), 8);
		seedItemCountFieldText.alpha = .2;
		juiceItemCountFieldText.alpha = .2;
		pieItemCountFieldText.alpha = .2;
		fishItemCountFieldText.alpha = .2;
		greenHouseItemCountFieldText.alpha = .2;
		seedItemCountFieldText.color = 0x0000000;
		juiceItemCountFieldText.color = 0x0000000;
		pieItemCountFieldText.color = 0x0000000;
		fishItemCountFieldText.color = 0x0000000;
		greenHouseItemCountFieldText.color = 0x0000000;
		seedItemCostFieldText.color = 0x0cccc51;
		juiceItemCostFieldText.color = 0x0cccc51;
		pieItemCostFieldText.color = 0x0cccc51;
		fishItemCostFieldText.color = 0x0cccc51;
		greenHouseItemCostFieldText.color = 0x0cccc51;
		//marketGroup.add(marketSideBarSprite);
		marketGroup.add(marketSideBarTabSprite);
		marketGroup.add(marketSideBarShaftSprite);
		marketGroup.add(coin);
		fillItemArray();
		registerEvents();
		positionArrayItems();
		add(marketGroup);
	}
	
	public function getMarketOpen() {
		return marketOpen;
	}
	
	public function getCoinCount() {
		return coinCount;
	}
	
	public function getSeedCount() {
		return seedCount;
	}
	
	public function getJuiceCount() {
		return juiceCount;
	}
	
	public function getPieCount() {
		return pieCount;
	}
	
	public function getFishCount() {
		return fishCount;
	}
	
	public function getGreenHouseCount() {
		return greenHouseCount;
	}
	
	public function getJuicePackCount() {
		return juicePack;
	}
	
	public function getPiePackCount() {
		return piePack;
	}
	
	public function stockJuicePacks(val:Int) {
		juicePack += val;
	}
	
	public function stockPiePacks(val:Int) {
		piePack += val;
	}
	
	public function loadJuicePacksDec() {
		juicePack -= 10;
	}
	
	public function loadPiePacksDec() {
		piePack -= 10;
	}
	
	public function setCountTextXClose(val:Int) {
		seedCountText.x -= val;
		juiceCountText.x -= val;
		pieCountText.x -= val;
		fishCountText.x -= val;
		greenHouseCountText.x -= val;
	}
	
	public function setCountTextXOpen(val:Int) {
		seedCountText.x += val;
		juiceCountText.x += val;
		pieCountText.x += val;
		fishCountText.x += val;
		greenHouseCountText.x += val;
	}
	
	public function setJuicePackCount(val:Int) {
		juicePack = val;
	}
	
	public function setPiePackCount(val:Int) {
		piePack = val;
	}
	
	public function setSeedCount(val:Int) {
		seedCount = val;
	}
	
	public function setseedItemCount(val:Int) {
		seedItem.setSeedItemCount(val);
	}
	
	public function setKoiPondItemCount(val:Int) {
		koiFishItem.setFishItemCount(val);
	}
	
	public function setGreenHouseItemCount(val:Int) {
		greenHouseItem.setGreenHouseItemCount(val);
	}
	
	public function setPieCount(val:Int) {
		pieCount = val;
	}
	
	public function setFishCount(val:Int) {
		fishCount = val;
	}
	
	public function setGreenHouseCount(val:Int) {
		greenHouseCount = val;
	}
	
	public function setPieItemCount(val:Int) {
		pieItem.setPieItemCount(val);
	}
	
	public function setJuiceCount(val:Int) {
		juiceCount = val;
	}
	
	public function setJuiceItemCount(val:Int) {
		juiceItem.setJuiceItemCount(val);
	}
	
	public function setCoinCount(val:Int) {
		coinCount = val;
	}
	
	public function setWaterClientCount(val:Int) {
		waterClientCount = val;
	}
	
	public function setElectricClientCount(val:Int) {
		electricClientCount = val;
	}
	
	public function addSubtractCoins(operation:String, perTen:Int) {
		switch(operation) {
			case "+":
				coinCount += perTen;
			case "-":
				coinCount -= perTen;	
		}
	}
	
	private function displayCoinCount() {
		coinCountFieldText.text = Std.string(coinCount);
		add(coinCountFieldText);
	}
	
	private function displaySeedCount() {
		
		seedCountText.text = Std.string(seedCount);
		juiceCountText.text = Std.string(juiceCount);
		pieCountText.text = Std.string(pieCount);
		fishCountText.text = Std.string(fishCount);
		greenHouseCountText.text = Std.string(greenHouseCount);
		
		add(seedCountText);
		add(juiceCountText);
		add(pieCountText);
		add(fishCountText);
		add(greenHouseCountText);
	}
	
	private function displayItemCountText() {
		seedItemCountFieldText.text = Std.string(seedItem.getItemCount());
		juiceItemCountFieldText.text = Std.string(juiceItem.getItemCount());
		fishItemCountFieldText.text = Std.string(koiFishItem.getItemCount());
		seedItemCostFieldText.text = Std.string(seedItem.getItemCost());
		juiceItemCostFieldText.text = Std.string(juiceItem.getItemCost());
		pieItemCountFieldText.text = Std.string(pieItem.getItemCount());
		pieItemCostFieldText.text = Std.string(pieItem.getItemCost());
		fishItemCostFieldText.text = Std.string(koiFishItem.getItemCost());
		juicePackCountText.text = Std.string(juicePack);
		piePackCountText.text = Std.string(piePack);
		greenHouseItemCostFieldText.text = Std.string(greenHouseItem.getItemCost());
		greenHouseItemCountFieldText.text = Std.string(greenHouseItem.getItemCount());
		marketGroup.add(juicePackCountText);
		marketGroup.add(seedItemCostFieldText);
		marketGroup.add(juiceItemCostFieldText);
		marketGroup.add(juiceItemCountFieldText);
		marketGroup.add(seedItemCountFieldText);
		marketGroup.add(greenHouseItemCountFieldText);
		marketGroup.add(pieItemCostFieldText);
		marketGroup.add(pieItemCountFieldText);
		marketGroup.add(fishItemCountFieldText);
		marketGroup.add(fishItemCostFieldText);
		marketGroup.add(greenHouseItemCostFieldText);
		marketGroup.add(piePackCountText);
	}
	
	public function decSeedCount() {
		seedCount--;
	}
	
	public function incSeedCount() {
		seedCount++;
	}
	
	public function decjuiceCount(val:Int ) {
		juiceCount -= val;
	}
	
	public function decjuicePackCount(val:Int ) {
		juicePack-= val;
	}
	
	public function incJuiceCount() {
		juiceCount++;
	}
	
	public function decPieCount() {
		pieCount--;
	}
	
	public function decFishCount() {
		fishCount--;
	}
	
	public function decGreenHouseCount() {
		greenHouseCount--;
	}
	
	public function incPieCount() {
		pieCount++;
	}
	
	public function incfishCount() {
		fishCount++;
	}
	
	public function incGreenHouseCount() {
		greenHouseCount++;
	}
	
	public function coinPaymentUtil(val:Int) {
		coinCount -= val;
	}
	
	private function positionArrayItems() {
		for (i in 0...itemArray.length) {
			itemArray[i].getItemSprite().x = marketSideBarTabSprite.x + shiftX;
			itemArray[i].getItemSprite().y = marketSideBarTabSprite.y + shiftY;
			marketGroup.add(itemArray[i].getItemSprite());
			shiftY += 36;
		}
	}
	
	private function fillItemArray() {
		itemArray[0] = seedItem;
		itemArray[1] = juiceItem;
		itemArray[2] = pieItem;
		itemArray[3] = koiFishItem;
		itemArray[4] = greenHouseItem;
	}
	
	public function registerEvents() {
		MouseEventManager.add(marketSideBarTabSprite, openMarket, null, null, null);
		for (i in 0...itemArray.length) {
			MouseEventManager.add(itemArray[i].getItemSprite(), selectItem, null, null, null);
		}
	}
	
	public function registerItemEvents() {
		for (i in 0...itemArray.length) {
			MouseEventManager.add(itemArray[i].getItemSprite(), selectItem, null, null, null);
		}
	}
	
	public function unregisterItemEvents() {
		for (i in 0...itemArray.length) {
			MouseEventManager.remove(itemArray[i].getItemSprite());
		}
	}
	
	public function unregisterEvents() {
		MouseEventManager.remove(marketSideBarTabSprite);
		MouseEventManager.remove(marketSideBarShaftSprite);
		for (i in 0...itemArray.length) {
			MouseEventManager.remove(itemArray[i].getItemSprite());
		}
	}
	
	private function selectItem(sprite:FlxSprite) {
		for (i in 0...itemArray.length) {
			if (itemArray[i].getItemSprite() == sprite) {
				marketCloseTimer = 10;
				if (coinCount >= itemArray[i].getItemCost() && itemArray[i].getItemCount() > 0) {
					purchaseItemSound = FlxG.sound.load(AssetPaths.cashRegisterSound__wav, .3);
					purchaseItemSound.play();
					itemArray[i].decItemCount();
					coinCount -= itemArray[i].getItemCost();
					purchaseItem(itemArray[i]);
					if (itemArray[i].getItemCount() <= 0) {
						itemArray[i].setItemAvailable(false);
						itemArray[i].setAvailableAnimation();
					}
				}
			}
		}
	}

	private function purchaseItem(item:MarketItem) {
		switch(item.getItemName()) {
			case "seedItem":
				seedCount += 1;
			case "juiceItem":
				juiceCount += 1;
			case "pieItem":
				pieCount += 1;
			case "koiItem":
				fishCount += 1;
			case "greenHouseItem":
				greenHouseCount += 1;	
		}
	}
	
	private function openMarket(sprite:FlxSprite) {
		if (turn) {
			marketOpenSound = FlxG.sound.load(AssetPaths.marketSlideOpenSound__wav);
			marketOpenSound.play();
			marketOpen = true;
			turn = false;
			closedSoundPlayed = false;
		}
		else {
			marketClosedSound = FlxG.sound.load(AssetPaths.marketSlideClosedSound__wav);
			marketClosedSound.play();
			marketOpen = false;
			turn = true;
		}
	}
	
	private function slideMarketOut(sprite:FlxSprite) {
		if (marketOpen) {
			registerItemEvents();
			marketCloseTimer -= FlxG.elapsed;
			if(marketSideBarTabSprite.x >= 565){
				marketSideBarTabSprite.x -= 2;
				marketSideBarShaftSprite.x -= 2;
				shiftItemsOpen();
			}
		}
		else {
			unregisterItemEvents();
			if (marketSideBarTabSprite.x <= 602) {
				if (!closedSoundPlayed) {
					marketClosedSound = FlxG.sound.load(AssetPaths.marketSlideClosedSound__wav);
					marketClosedSound.play();
					closedSoundPlayed = true;
				}
				marketCloseTimer = 10;
				marketSideBarTabSprite.x += 2;
				marketSideBarShaftSprite.x += 2;
				shiftItemsClose();
			}
		}
	}
	
	private function timedMarketClose() {
		if (marketCloseTimer < 0) {
			marketCloseTimer = 10;
			marketOpen = false;
			turn = true;
		}
	}
	
	private function shiftItemsOpen() {
		for (i in 0...itemArray.length) {
			itemArray[i].getItemSprite().x -= 2;
		}
		seedItemCountFieldText.x -= 2;
		juiceItemCountFieldText.x -= 2;
		seedItemCostFieldText.x -= 2;
		juiceItemCostFieldText.x -= 2;
		pieItemCountFieldText.x -= 2;
		pieItemCostFieldText.x -= 2;
		fishItemCostFieldText.x -= 2;
		fishItemCountFieldText.x -= 2;
		greenHouseItemCostFieldText.x -= 2;
		greenHouseItemCountFieldText.x -= 2;
	}
	
	private function shiftItemsClose() {
		for (i in 0...itemArray.length) {
			itemArray[i].getItemSprite().x += 2;
		}
		seedItemCountFieldText.x += 2;
		juiceItemCountFieldText.x += 2;
		seedItemCostFieldText.x += 2;
		juiceItemCostFieldText.x += 2;
		pieItemCountFieldText.x += 2;
		pieItemCostFieldText.x += 2;
		fishItemCostFieldText.x += 2;
		fishItemCountFieldText.x += 2;
		greenHouseItemCostFieldText.x += 2;
		greenHouseItemCountFieldText.x += 2;
	}	
	
	private function replenishItems() {
		replenishSeedTimer -= FlxG.elapsed;
		replenishJuiceMachineTimer -= FlxG.elapsed;
		replenishPieMachineSeedTimer -= FlxG.elapsed;
		if (itemArray[0].getItemCount() > 0 || itemArray[1].getItemCount() > 0 || itemArray[2].getItemCount() > 0){
			itemArray[0].setItemAvailable(true);
			itemArray[0].setAvailableAnimation();
			itemArray[1].setItemAvailable(true);
			itemArray[1].setAvailableAnimation();
			itemArray[2].setItemAvailable(true);
			itemArray[2].setAvailableAnimation();
		}
		if (itemArray[0].getItemCount() == 0) {
			itemArray[0].setItemAvailable(false);
			itemArray[0].setAvailableAnimation();
		}
		if (itemArray[1].getItemCount() == 0) {
			itemArray[1].setItemAvailable(false);
			itemArray[1].setAvailableAnimation();
		}
		if (itemArray[2].getItemCount() == 0) {
			itemArray[2].setItemAvailable(false);
			itemArray[2].setAvailableAnimation();
		}
		if (replenishSeedTimer < 0) {
			replenishSeedTimer = 180;
			if (itemArray[0].getItemCount() < 2) {
				itemArray[0].incItemCount();
			}
		}
		if (replenishJuiceMachineTimer < 0) {
			replenishJuiceMachineTimer = 300;
			if (itemArray[1].getItemCount() < 1) {
				itemArray[1].incItemCount();
			}
		}
		if (replenishPieMachineSeedTimer < 0) {
			replenishPieMachineSeedTimer = 300;
			if (itemArray[2].getItemCount() < 1) {
				itemArray[2].incItemCount();
			}
		}
		if (koiFishItem.getItemCount() > 0) {
			koiFishItem.setItemAvailable(true);
			koiFishItem.setAvailableAnimation();
		}
		if (greenHouseItem.getItemCount() > 0) {
			greenHouseItem.setItemAvailable(true);
			greenHouseItem.setAvailableAnimation();
		}
	}
	
	private function waterBillCoinPayment() {	
		waterBillTimer -= FlxG.elapsed;
		if (waterBillTimer < 0) {
			coinCount -= waterClientCount;
			waterBillTimer = 30;
		}
	}
	
	private function ElectricBillCoinPayment() {	
		electricBillTimer -= FlxG.elapsed;
		if (electricBillTimer < 0) {
			coinCount -= electricClientCount * 2;
			electricBillTimer = 10;
		}
	}
	
	private function destroySound() {
		if (marketOpenSound != null && !marketOpenSound.playing) {
			marketOpenSound = FlxDestroyUtil.destroy(marketOpenSound);
			marketOpenSound = null;
			//trace("destroying marketOpenSound");
		}
		if (marketClosedSound != null && !marketClosedSound.playing) {
			marketClosedSound = FlxDestroyUtil.destroy(marketClosedSound);
			marketClosedSound = null;
			//trace("destroying marketClosedSound");
		}
		if (purchaseItemSound != null && !purchaseItemSound.playing) {
			purchaseItemSound = FlxDestroyUtil.destroy(purchaseItemSound);
			purchaseItemSound = null;
			//trace("destroying purchaseItemSound");
		}
	}
	
	override public function update() {
		slideMarketOut(null);
		displayItemCountText(); 
		replenishItems();
		timedMarketClose();
		displaySeedCount();
		displayCoinCount();
		destroySound();
		super.update();
	}
	
	override public function destroy() {
		marketGroup = null;
		marketSideBarSprite = null;
		marketSideBarTabSprite = null;
		marketSideBarShaftSprite = null;
		koiFishItem = null;
		seedItem = null;
		juiceItem = null;
		pieItem = null;
		coin = null;
		seedCountText = null;
		juiceCountText = null;
		pieCountText = null;
		fishCountText = null;
		greenHouseCountText = null;
		juicePackCountText = null;
		piePackCountText = null;
		greenHouseItem = null;
		itemArray = null;
		seedItemCountFieldText = null;
		juiceItemCountFieldText = null;
		pieItemCountFieldText = null;
		seedItemCostFieldText = null;
		juiceItemCostFieldText = null;
		pieItemCostFieldText = null;
		fishItemCostFieldText = null;
		fishItemCountFieldText = null;
		greenHouseItemCountFieldText = null;
		greenHouseItemCostFieldText = null;
		super.destroy();
	}
	
}