package;

import flixel.addons.api.FlxGameJolt;
import flixel.addons.editors.tiled.TiledLayer;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.group.FlxSpriteGroup;
import flixel.system.FlxSound;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxMath;
import flixel.plugin.MouseEventManager;
import haxe.ds.Option;
import openfl.display.BitmapData;
import openfl.events.Event;
import flixel.util.FlxCollision;
import flixel.group.FlxGroup;
import flash.text.TextField;
import flixel.util.FlxSort;
import flixel.util.FlxTimer;
import flixel.util.FlxRandom;
import openfl.events.MouseEvent;
import flixel.util.FlxDestroyUtil;
import flash.system.System;

/**
 * A FlxState which can be used for the actual gameplay. main state.
 */
class PlayState extends FlxState
{
	private var menuButton:FlxButton;
	private var menuScreen:OptionsScreen;
	private var tutorial:Tutorial;
	private var weed:Weed;
	private var systemStar:SystemStar;
	private var backgroundSprite:FlxSprite;
	private var buildBar:FlxSprite;
	private var buildBarTab:FlxSprite;
	private var buildBarShaft:FlxSprite;
	private var aboveGrassLayer:FlxGroup;
	private var plantLayer:FlxGroup;
	private var machineLayer:FlxGroup;
	private var belowBottomSprites:FlxGroup;
	private var bottomeSprites:FlxGroup;
	private var floatingIsland:Island;
	private var tileBoard:TileBoard;
	private var lastTileClicked:Tile = null;
	private var tileBoardBorder:TileBoarder;
	private var FPSMEMdisplay:FPS_Mem;
	private	var board:Array<Array<Tile>>;
	private var boarder:Array<Array<TileBoarder>>;
	private var treeArray:Array<Tree>; 
	private var juiceMachineArray:Array<JuiceMachine>;
	private var pieMachineArray:Array<PieMachine>;
	private var flyingJuiceBottleArray:Array<JuiceIcon>;
	private var numPad:NumberPad;
	private var bb:ProblemBoard;
	private var menuVisible:Int = 0;
	private var appleCount:Int = 0;
	private var buySignCost:Int = 2;
	private var applePerTenCost:Int = 5;
	private var juicePerTenCost:Int = 15;
	private var piePerTenCost:Int = 25;
	private var currentTruckLvl = 1;
	private var randomIslandCreationTimer:Float; 
	private var buildBarOpenTimer:Float = 15;
	private var loadAppleTimer:Float = 1;
	private var leaveTruckTimer:Float = 2;
	private var appleForJuiceTimer:Float = 1;
	private var appleAndJuiceForPieTimer:Float = 1;
	private var buildBarOpenSpeed:Int = 3;
	private var islandCreationTimer:Float = 1;
	private var appleCountString:String = "";
	private var answerString:String = "";
	private var boardIsDown:Bool = false;
	private var appleDisplayLock:Bool = true;
	private var asteroidBelt:AsteroidBelt;
	private var greenHouse:Greenhouse;
	private var bridge:FlxSprite;
	private var buySign:FlxSprite;
	private var foreGroundRocks:FlxSprite;
	private var answerFieldText:FlxText;
	private var appleCountFieldText:FlxText;
	private var buySignCostFieldText:FlxText;
	private var market:Market;
	private var truckButton:TruckButton;
	private var juiceMachine:JuiceMachine;
	private var pieMachine:PieMachine;
	private var appleShed:AppleShed;
	private var appleTruck:AppleTruck;
	private var tree:Tree;
	private var basket:AppleBasket;
	private var currentTargetTree:Tree;
	private var currentTargetJuiceMachine:JuiceMachine;
	private var currentTargetPieMachine:PieMachine;
	private var juiceMachineIcon:JuiceIcon;
	private var pieMachineIcon:PieIcon;
	private var juiceMachineIconDrag:JuiceIcon;
	private var pieMachineIconDrag:PieIcon;
	private var fishIcon:FishIcon;
	private var fishIconDrag:FishIcon;
	private var juicePackIcon:JuicePackIcon;
	private var piePackIcon:PiePackIcon;
	private var seed:Seed;
	private var seedDrag:Seed;
	private var greenHouseBarIcon:GreenHouseIcon;
	private var greenHouseBarIconDrag:GreenHouseIcon;
	private var cloud:Cloud;
	private var sun:SunToken;
	private var currentTileBorder:TileBoarder;
	private var overlaysInUse:Bool = false;
	private var sunClicked:Bool = false;
	private var dropletClicked:Bool = false;
	private var ownedTile:Bool = false;
	private var closeTreeGrowthCheck:Bool = false;
	private var closeAppleCountCheck:Bool = false;
	private var marketOpenCheck:Bool = true;
	private var tutorialOn:Bool;
	private var buildBarIsOpen:Bool = false;
	private var truckTutSection:Bool = true;
	private var callBackStopOne:Bool = true;
	private var callBackStopTwo:Bool = false;
	private var tutorialWrongAnswersSkip:Bool = true;
	private var closeSecondTreeGrowthCheck:Bool = false;
	private var tutUnpauseAlertsDone:Bool = false;
	private var tutBuildBarBack:Bool = true;
	private var coinsSound:FlxSound;
	private var alertButtonSound:FlxSound;
	private var correctAnswerSound:FlxSound;
	private var incorrectAnswerSound:FlxSound;
	private var plantingSeedSound:FlxSound;
	private var buySignSound:FlxSound;
	private var buyLandSound:FlxSound;
	private var placedMachineSound:FlxSound;
	private var truckButtonSound:FlxSound;
	private var koiPondPlacedSound:FlxSound;
	private var mathless:Bool = MenuState.mathless;
	private var koiPond:KoiPond;
	
	/**
	 * Function that is called up when to state is created to set it up. 
	 */
	override public function create():Void
	{	
		initPlaystate();
		//FlxG.sound.playMusic(AssetPaths.BeppleSongOneOGG__ogg, .5, true);
		
		super.create();	
	}
	
	//initialize starting game configuration
	private function initPlaystate() {
		//menuScreen = new OptionsScreen();
		//FPSMEMdisplay = new FPS_Mem(10, 10, 0xffffff);
		//greenHouse = new Greenhouse();
		menuButton = new FlxButton(550, 450, "Menu", openMenu);
		tutorialOn = MenuState.tutorialOn;
		trace(tutorialOn);
		bgColor = 0xff66FFFF; 
		if(tutorialOn){
			tutorial = new Tutorial(0);
		}
		else {
			if(tutorial != null){	
				tutorial.destroy();
				tutorial = null;
			}
		}
		tileBoard = new TileBoard();
		currentTileBorder = new TileBoarder();
		market = new Market();
		appleShed = new AppleShed();
		truckButton = new TruckButton();
		appleTruck = new AppleTruck();
		basket = new AppleBasket();
		seed = new Seed(0, 0, false);
		fishIcon = new FishIcon(0, 0, false);
		greenHouseBarIcon = new GreenHouseIcon(0, 0, false);
		systemStar = new SystemStar();
		buildBar = new FlxSprite();
		buildBarTab = new FlxSprite();
		buildBarShaft = new FlxSprite();
		juiceMachineIcon = new JuiceIcon(118, 70, false);
		juicePackIcon = new JuicePackIcon();
		piePackIcon = new PiePackIcon();
		pieMachineIcon = new PieIcon(0, 0, false);
		MouseEventManager.remove(seed);
		MouseEventManager.remove(juiceMachineIcon);
		MouseEventManager.remove(pieMachineIcon);
		MouseEventManager.remove(fishIcon);
		MouseEventManager.remove(greenHouseBarIcon);
		MouseEventManager.add(truckButton.getTruckButtonSprite(), callTruck, null, null, null);
		MouseEventManager.add(buildBarTab, buildBarOpen, null, null, null);
		seedDrag = new Seed(0, 0, true);
		juiceMachineIconDrag = new JuiceIcon(0, 0, true);
		pieMachineIconDrag = new PieIcon(0, 0, true);
		fishIconDrag = new FishIcon(0, 0, true);
		greenHouseBarIconDrag = new GreenHouseIcon(0, 0, true);
		bb = new ProblemBoard();
		buySign = new FlxSprite();
		aboveGrassLayer = new FlxGroup();
		belowBottomSprites = new FlxGroup();
		bottomeSprites = new FlxGroup();
		plantLayer = new FlxGroup();
		machineLayer = new FlxGroup();
		backgroundSprite = new FlxSprite();
		asteroidBelt = new AsteroidBelt(-60);
		foreGroundRocks = new FlxSprite();
		bridge = new FlxSprite();
		buildBar.loadGraphic(AssetPaths.buildBar__png, false);
		buildBarTab.loadGraphic(AssetPaths.buildBarTab__png, false);
		buildBarShaft.loadGraphic(AssetPaths.buildBarShaft__png, false);
		backgroundSprite.loadGraphic(AssetPaths.spaceBackground__png, false);
		buySign.loadGraphic(AssetPaths.buyLandButton__png, false);
		foreGroundRocks.loadGraphic(AssetPaths.rockyForeground__png, false);
		bridge.loadGraphic(AssetPaths.bridgeImage__png, false);
		appleCountFieldText = new FlxText(123, 43, 32, appleCountString, 12, false);
		belowBottomSprites.add(backgroundSprite);
		belowBottomSprites.add(asteroidBelt);
		belowBottomSprites.add(systemStar);
		aboveGrassLayer.add(truckButton);
		appleCountFieldText.color = 0xFFFFFFF;
		seedDrag.alpha = .5;
		juiceMachineIconDrag.alpha = .5;
		pieMachineIconDrag.alpha = .5;
		fishIconDrag.alpha = .5;
		greenHouseBarIconDrag.alpha = .5;
		foreGroundRocks.x = 30;
		foreGroundRocks.y = 265;
		bridge.y = 260;
		bridge.x = -55;
		buildBarShaft.y = 65;
		buildBarShaft.x = -185;
		buildBarTab.y = 65;
		buildBarTab.x = -4;
		//flash.Lib.current.addChild(FPSMEMdisplay);
		add(belowBottomSprites);
		add(bottomeSprites);
		add(floatingIsland);
		add(buildBarShaft);
		add(buildBarTab);
		add(basket);
		add(juicePackIcon);
		add(piePackIcon);
		drawBoard();
		add(market);
		add(appleShed);
		//plantWeeds();
		add(machineLayer);
		add(plantLayer);
		add(bridge);
		add(appleTruck);
		add(seed);
		add(seedDrag);
		add(juiceMachineIcon);
		add(juiceMachineIconDrag);
		add(pieMachineIcon);
		add(pieMachineIconDrag);
		add(fishIcon);
		add(fishIconDrag);
		add(greenHouseBarIcon);
		add(greenHouseBarIconDrag);
		if (tutorialOn) {
			add(tutorial);
			buildBarIsOpen = true;
			buildBarOpenTimer = -1;
		}
		add(bb);
		numPad = new NumberPad();
		answerFieldText = new FlxText(235, 132, 105, numPad.getPlayerAnswerString(), 24, false);
		answerFieldText.color = 0x000000;
		add(aboveGrassLayer);
		add(numPad);
		add(menuButton);
	}
	
	private function openMenu() {
		if (menuVisible == 0) {
			menuScreen = new OptionsScreen();
			menuScreen.menuVisible(1);
			add(menuScreen);
			releaseAllClickEvents(1);
			menuVisible = 1;
		}
		else {
			menuScreen.destroy();
			releaseAllClickEvents(0);
			menuVisible = 0;
		}
	}
	
	//removes all mouse events during menu screen
	private function releaseAllClickEvents(val:Int) {
		if (val == 1) {
			numPad.unregisterNumPadEvents();
			seedDrag.unregisterSeedEvent();
			pieMachineIconDrag.unregisterPieEvent();
			juiceMachineIconDrag.unregisterJuiceEvent();
			fishIconDrag.unregisterFishEvent();
			greenHouseBarIconDrag.unregisterGreenHouseImageEvent();
			MouseEventManager.remove(truckButton.getTruckButtonSprite());
			market.unregisterEvents();
			for (k in 0...treeArray.length) {
				if(treeArray[k] != null){
					treeArray[k].unregisterAlertEvents();
					treeArray[k].unregisterTreeClickedMouseEvent();
					treeArray[k].unregisterAppleEvent();
				}
			}
			for (i in 0...board.length) {
				for (j in 0...board.length) {
					MouseEventManager.remove(board[i][j]);	
				}
			}
		}
		else {
			seedDrag.registerSeedEvent();
			pieMachineIconDrag.registerPieEvent();
			juiceMachineIconDrag.registerJuiceEvent();
			fishIconDrag.registerFishEvent();
			greenHouseBarIconDrag.registerGreenHouseImageEvent();
			MouseEventManager.add(truckButton.getTruckButtonSprite(), callTruck, null, null, null);
			market.registerEvents();
			for (i in 0...board.length) {
				for (j in 0...board.length) {
					if(!(board[i])[j].getHasObject()){
						MouseEventManager.add((board[i])[j], buyButtonPopUp, placeObject, null, null);
					}
				}
			}
			for (k in 0...treeArray.length) {
				if (treeArray[k] != null) {
					MouseEventManager.add(treeArray[k].getTreeSprite(), treeClicked, null, null, null);
					treeArray[k].registerAlertEvents();
					treeArray[k].registerTreeClickedMouseEvent();
					treeArray[k].registerAppleEvent();
				}
			}
			numPad.registerEvent();
		}
	}
	
	//instantiate tree array memory. pointing at null.
	private function initTreeArray() {
		for (i in 0...9) {
			treeArray[i] = null;
		}
	}
	
	//positions static weed sprites
	private function plantWeeds() {
		weed = new Weed(1);
		weed.getChosenWeed().x = 150;
		weed.getChosenWeed().y = 260;
		plantLayer.add(weed);
		weed = new Weed(1);
		weed.getChosenWeed().x = 450;
		weed.getChosenWeed().y = 260;
		plantLayer.add(weed);
		weed = new Weed(1);
		weed.getChosenWeed().x = 330;
		weed.getChosenWeed().y = 375;
		plantLayer.add(weed);
		weed = new Weed(1);
		weed.getChosenWeed().x = 305;
		weed.getChosenWeed().y = 190;
		plantLayer.add(weed);
		weed = new Weed(1);
		weed.getChosenWeed().x = 70;
		weed.getChosenWeed().y = 260;
		plantLayer.add(weed);
		weed = new Weed(1);
		weed.getChosenWeed().x = 150;
		weed.getChosenWeed().y = 190;
		plantLayer.add(weed);
		weed = new Weed(0);
		weed.getChosenWeed().x = 365;
		weed.getChosenWeed().y = 225;
		plantLayer.add(weed);
		weed = new Weed(0);
		weed.getChosenWeed().x = 170;
		weed.getChosenWeed().y = 240;
		plantLayer.add(weed);
		weed = new Weed(0);
		weed.getChosenWeed().x = 190;
		weed.getChosenWeed().y = 320;
		plantLayer.add(weed);
	}
	
	//creates floating sprite islands, adds them to lowest z-layer, reset island timer
	private function createFloatingIsland() {
		if(islandCreationTimer <= 0){
			floatingIsland = new Island();
			bottomeSprites.add(floatingIsland);
			islandCreationTimer = FlxRandom.intRanged(2, 10);
		}
		islandCreationTimer -= FlxG.elapsed;
	}
	
	//instantiate flying bottle array elements
	private function initFlyingJuiceBottleArray() {
		for (i in 0...9) {
			flyingJuiceBottleArray[i] = null; 
		}
	}
	
	//instantiate juice machine array elements
	private function initJuiceMachineArray() {
		for (i in 0...9) {
			juiceMachineArray[i] = null;
		}
	}
	
	//instantiate juice machine array elements
	private function initPieMachineArray() {
		for (i in 0...9) {
			pieMachineArray[i] = null;
		}
	}
	
	//adds currently created tree object to tree array
	private function addFlyingBottleArray(flyingBottle:JuiceIcon) {
		for (i in 0...flyingJuiceBottleArray.length) {
			if (flyingJuiceBottleArray[i] == null) {
				flyingJuiceBottleArray[i] = flyingBottle;
				return;
			}
		}
	}
	
	//adds currently created tree object to tree array
	private function addTreeArray(treeObj:Tree) {
		for (i in 0...treeArray.length) {
			if (treeArray[i] == null) {
				treeArray[i] = treeObj;
				return;
			}
		}
	}
	
	//adds currently created juice machine object to juice machine array
	private function addJuiceMachineArray(machineObj:JuiceMachine) {
		for (i in 0...juiceMachineArray.length) {
			if (juiceMachineArray[i] == null) {
				juiceMachineArray[i] = machineObj;
				return;
			}
		}
	}
	
	//adds currently created pie machine object to pie machine array
	private function addPieMachineArray(machineObj:PieMachine) {
		for (i in 0...pieMachineArray.length) {
			if (pieMachineArray[i] == null) {
				pieMachineArray[i] = machineObj;
				return;
			}
		}
	}
	
	//activates grass tile boarder when relevant sprite ares hovering over them
	public function tileBoarderFlip() {
		
		for (i in 0...3) {
			for (j in 0...3) {
				
				if (FlxCollision.pixelPerfectPointCheck(FlxG.mouse.screenX, FlxG.mouse.screenY, (board[i])[j]) && seedDrag.getDragging() ||
					FlxCollision.pixelPerfectPointCheck(FlxG.mouse.screenX, FlxG.mouse.screenY, (board[i])[j]) && juiceMachineIconDrag.getDragging() || 
					FlxCollision.pixelPerfectPointCheck(FlxG.mouse.screenX, FlxG.mouse.screenY, (board[i])[j]) && pieMachineIconDrag.getDragging() ||
					FlxCollision.pixelPerfectPointCheck(FlxG.mouse.screenX, FlxG.mouse.screenY, (board[i])[j]) && fishIconDrag.getDragging() ||
					FlxCollision.pixelPerfectPointCheck(FlxG.mouse.screenX, FlxG.mouse.screenY, (board[i])[j]) && greenHouseBarIconDrag.getDragging()) {
					
					(boarder[i])[j].boarderSwitch("on");
					
				}
				else {
					
					(boarder[i])[j].boarderSwitch("off");
						
				}
				
			}
		}
	}
	
	//places the buy sign sprite over currently clicked and unpurchased grass tile.
	public function buyButtonPopUp(sprite:Tile) {
		if (!numPad.getPadScrollingIn() && !sprite.getTileOwned()) {
			buySign.x = sprite.x + 70;
			buySign.y = sprite.y + 38;
			aboveGrassLayer.remove(buySignCostFieldText);
			MouseEventManager.add(buySign, buyLand, null, null, null);
			if (!sprite.getTileClicked()) {
				buySignSound = FlxG.sound.load(AssetPaths.buySignSound__wav);
				buySignSound.play();
				buySign.revive();
				aboveGrassLayer.add(buySign);
				buySignCostFieldText = new FlxText(buySign.x+33, buySign.y+1, 16, Std.string(buySignCost), 12);
				buySignCostFieldText.color = 0x0e5e55b;
				aboveGrassLayer.add(buySignCostFieldText);
				sprite.setTileClicked(true);
				if (lastTileClicked != sprite && lastTileClicked != null) {
					lastTileClicked.setTileClicked(false);
				}
			}
			else {
				buySign.kill();
				aboveGrassLayer.remove(buySignCostFieldText);
				sprite.setTileClicked(false);
			}
			lastTileClicked = sprite;
		}
	}
	
	
	private function displayAppleCount() {
		
		for (i in 0...treeArray.length) {
			if (treeArray[i] != null) {
				treeArray[i].currentGoldCount(market.getCoinCount());
				if (treeArray[i].getAppleStored() && appleCount < basket.getBasketFullVal()) {
					appleCount++;
					//basket.animateBasket(appleCount);
					appleShed.animateShed(appleCount, basket.getBasketFullVal());
					treeArray[i].setAppleStored(false);
				}
				treeArray[i].setAppleClicked(false);
			}
		}
		if ((appleCount >= 10 || market.getJuicePackCount() >= 10 || market.getPiePackCount() >= 10) && !truckButton.getButtonClickable() && appleTruck.getTruckIsReset()) {
			truckButton.setButtonLevel(1);
			MouseEventManager.add(truckButton.getTruckButtonSprite(), callTruck, null, null, null);
			truckButton.buttonSetting();
			truckButton.setButtonClickable(true);
		}
		if (appleCount < 10 && market.getJuicePackCount() < 10 && market.getPiePackCount() < 10) {
			truckButton.setButtonLevel(0);
			truckButton.buttonSetting();
			truckButton.setButtonClickable(false);
		}
		appleCountFieldText.text = Std.string(appleCount);
		add(appleCountFieldText);
		
	}
	
	private function displayPadNumbers() {
		
		answerFieldText.text = numPad.getPlayerAnswerString();
		answerFieldText.y = numPad.getPadY() + 12;
		add(answerFieldText);
		
	}
	
	private function placeObject(sprite:Tile) {
		if (FlxCollision.pixelPerfectCheck(seedDrag, sprite) && FlxCollision.pixelPerfectPointCheck(FlxG.mouse.screenX, FlxG.mouse.screenY, sprite) && !sprite.getHasObject() && sprite.getTileOwned()) {
			plantingSeedSound = FlxG.sound.load(AssetPaths.plantingSeedSound__wav);
			plantingSeedSound.play(true);
			market.decSeedCount();
			tree = new Tree(basket.getBasketSprite());
			//tree.registerAlertEvents();
			tree.setTreeGrowthPhase(6);
			tree.setMyTile(sprite);
			MouseEventManager.add(tree.getTreeSprite(), treeClicked, null, null, null);
			tree.setTreeGroundColor(sprite.getTileOwned());
			tree.setTreeX(sprite.x + 67);
			tree.setTreeY(sprite.y - 25);
			tree.positionAlerts();
			sprite.setHasObject(true);
			addTreeArray(tree);
			secondTreeTutMessage();
			tree.growTree();
			aboveGrassLayer.add(tree);
			if (tutorialOn) {
				treeArray[0].setTreeThirstTut(9999); 
				treeArray[0].setTreeSunlightTut(9999);
				tutorial.destroy();
				tutorial = new Tutorial(1);
				add(tutorial);
			}
		}
		if (FlxCollision.pixelPerfectCheck(juiceMachineIconDrag, sprite) && FlxCollision.pixelPerfectPointCheck(FlxG.mouse.screenX, FlxG.mouse.screenY, sprite) && !sprite.getHasObject() && sprite.getTileOwned()) {
			placedMachineSound = FlxG.sound.load(AssetPaths.placedMachineSound__wav, .6);
			placedMachineSound.play();
			market.decjuiceCount(1);
			juiceMachine = new JuiceMachine(sprite);
			juiceMachine.setRiseRun();
			sprite.setHasObject(true);
			addJuiceMachineArray(juiceMachine);
			machineLayer.add(juiceMachine);
		}
		if (FlxCollision.pixelPerfectCheck(pieMachineIconDrag, sprite) && FlxCollision.pixelPerfectPointCheck(FlxG.mouse.screenX, FlxG.mouse.screenY, sprite) && !sprite.getHasObject() && sprite.getTileOwned()) {
			placedMachineSound = FlxG.sound.load(AssetPaths.placedMachineSound__wav, .6);
			placedMachineSound.play();
			market.decPieCount();
			pieMachine = new PieMachine(sprite);
			pieMachine.setRiseRun();
			sprite.setHasObject(true);
			addPieMachineArray(pieMachine);
			machineLayer.add(pieMachine);
		}
		if (FlxCollision.pixelPerfectCheck(fishIconDrag, sprite) && FlxCollision.pixelPerfectPointCheck(FlxG.mouse.screenX, FlxG.mouse.screenY, sprite) && !sprite.getHasObject() && sprite.getTileOwned()) {
			placedMachineSound = FlxG.sound.load(AssetPaths.koiPondPlacedSound__wav, 1);
			placedMachineSound.play();
			market.decFishCount();
			koiPond = new KoiPond(sprite);
			sprite.setHasObject(true);
			machineLayer.add(koiPond);
		}
		if (FlxCollision.pixelPerfectCheck(greenHouseBarIconDrag, sprite) && FlxCollision.pixelPerfectPointCheck(FlxG.mouse.screenX, FlxG.mouse.screenY, sprite) && !sprite.getHasObject() && sprite.getTileOwned()) {
			placedMachineSound = FlxG.sound.load(AssetPaths.koiPondPlacedSound__wav, 1);
			placedMachineSound.play();
			market.decGreenHouseCount();
			greenHouse = new Greenhouse(sprite);
			sprite.setHasObject(true);
			aboveGrassLayer.add(greenHouse);
		}
	}
	
	private function treeClicked(sprite:FlxSprite) {
		//prevent from clicking tile behind tree sprite for now. maybe open up a menu later.
		
	}
	
	private function secondTreeTutMessage() {
		if (treeArray[1] != null && tutorialOn) {
			tutorial = new Tutorial(5);
			if (tutorialOn) {
				add(tutorial);
			}
			tutorialOn = false;
			//trace("TUT OVER");
		}
	}
	
	private function buyLand(sprite:FlxSprite) {
		for (i in 0...3) {
			for (j in 0...3) {
				if (FlxCollision.pixelPerfectPointCheck(FlxG.mouse.screenX, FlxG.mouse.screenY, (board[i])[j]) && market.getCoinCount() >= buySignCost) {
					buyLandSound = FlxG.sound.load(AssetPaths.cashRegisterSound__wav, .3);
					buyLandSound.play();
					(board[i])[j].setAsOwned();
					(board[i])[j].setHasObject(false);
					buySign.kill();
					market.addSubtractCoins("-", buySignCost);
					aboveGrassLayer.remove(buySignCostFieldText);
					buySignCost++;
				}
			}
		}
	}
	
	private function callTruck(sprite:FlxSprite) {
		if ((appleCount >= 10 || market.getJuicePackCount() >= 10 || market.getPiePackCount() >= 10) && appleTruck.getTruckIsReset()) {
			if(tutorial != null){	
				if(tutorial.getCurrentTutSectionNumber() == 1 && tutorialOn){
						tutorial.destroy();
				}
			}
			if(tutorialOn){
				tutorial.destroy();
			}
			truckButtonSound = FlxG.sound.load(AssetPaths.truckButtonSound__wav, .5);
			truckButtonSound.play();
			appleTruck.setPickUpReady(true);
			truckButton.setButtonLevel(2);
			truckButton.buttonSetting();
			MouseEventManager.remove(truckButton.getTruckButtonSprite());
		}
	}
	
	private function truckLoadingGoods() {
		if (appleTruck.getLoadingApples()) {
			if (truckTutSection) {
				//TODO create conditional for on/off tutorial condition
				if(tutorialOn){
					tutorial.destroy();
					tutorial = new Tutorial (4);
					add(tutorial);
				}
				truckTutSection = false;
			}
			loadAppleTimer -= FlxG.elapsed;
			if (loadAppleTimer < 0) {
				if (appleTruck.getAppleTruckCapacity() > 0 && appleCount >= 10) {
					appleCount -= 10;
					appleTruck.decTruckCapacity();
					//basket.animateBasket(appleCount);
					appleShed.animateShed(appleCount, basket.getBasketFullVal());
					market.addSubtractCoins("+", applePerTenCost);
					coinsSound = FlxG.sound.load(AssetPaths.coinsSound__wav);
					coinsSound.play();
					loadAppleTimer = 1;
				}
				if (appleTruck.getAppleTruckCapacity() > 0 && market.getJuicePackCount() >= 10) {
					market.loadJuicePacksDec();
					appleTruck.decTruckCapacity();
					market.addSubtractCoins("+", juicePerTenCost);
					coinsSound = FlxG.sound.load(AssetPaths.coinsSound__wav);
					coinsSound.play();
					loadAppleTimer = 1;
				}
				if (appleTruck.getAppleTruckCapacity() > 0 && market.getPiePackCount() >= 10) {
					market.loadPiePacksDec();
					appleTruck.decTruckCapacity();
					market.addSubtractCoins("+", piePerTenCost);
					coinsSound = FlxG.sound.load(AssetPaths.coinsSound__wav);
					coinsSound.play();
					loadAppleTimer = 1;
				}
			}
			if (appleTruck.getAppleTruckCapacity() == 0 || (appleCount < 10 && market.getJuicePackCount() < 10 && market.getPiePackCount() < 10)) {
				leaveTruckTimer -= FlxG.elapsed;
				if (leaveTruckTimer < 0) {
					truckButton.setButtonLevel(0);
					truckButton.buttonSetting();
					loadAppleTimer = 1;
					appleTruck.setTruckSize(currentTruckLvl);
					appleTruck.setLoadingApples(false);
					appleTruck.setPickUpReady(false);
					truckButton.setButtonClickable(false);
					leaveTruckTimer = 2;
				}
			}
		}
	}
	
	private function stopSeedDragIfBoard() {
		
		if (overlaysInUse || market.getSeedCount() < 1) {
			
			seedDrag.setDragging(false);
			
		}
		
	}
	
	private function stopJuiceDragIfBoard() {
		
		if (overlaysInUse || market.getJuiceCount() < 1) {
			
			juiceMachineIconDrag.setDragging(false);
			
		}
		
	}
	
	
	private function stopPieDragIfBoard() {
		
		if (overlaysInUse || market.getPieCount() < 1) {
			
			pieMachineIconDrag.setDragging(false);
			
		}
		
	}
	
	private function stopFishDragIfBoard() {
		
		if (overlaysInUse || market.getFishCount() < 1) {
			
			fishIconDrag.setDragging(false);
			
		}
		
	}
	
	private function stopGreenHouseDragIfBoard() {
		
		if (overlaysInUse || market.getGreenHouseCount() < 1) {
			
			greenHouseBarIconDrag.setDragging(false);
			
		}
		
	}
	
	private function drawBoard() {
		add(foreGroundRocks);
		board = tileBoard.getBoardArray();
		boarder = tileBoard.getBoardBoarderArray();
		treeArray = new Array<Tree>();
		juiceMachineArray = new Array<JuiceMachine>();
		pieMachineArray = new Array<PieMachine>();
		flyingJuiceBottleArray = new Array<JuiceIcon>();
		initTreeArray();
		initJuiceMachineArray();
		initPieMachineArray();
		initFlyingJuiceBottleArray();
		var shiftX:Int = 0;
		var shiftY:Int = 0;
		var newPosX:Int = 0;
		var newPosY:Int = 0;

		for (i in 0...board.length) {
			
			shiftX = 0;
			shiftY = 0;
			
			for (j in 0...board.length) {
				
				(board[i])[j].x += newPosX + shiftX;
				(board[i])[j].y += newPosY + shiftY;
				(boarder[i])[j].x += newPosX + shiftX;
				(boarder[i])[j].y += newPosY + shiftY;
				MouseEventManager.add((board[i])[j], buyButtonPopUp, placeObject, null, null);
				add((board[i])[j]);
				add((boarder[i])[j]);
				shiftX += 96;
				shiftY += 48;
				
			}
			
			newPosX -= 96;
			newPosY += 48;
			
		}
		(board[2])[0].setHasObject(true);
		(board[2])[0].setAsOwned();
		(board[1])[0].setAsOwned();
	}

	
	public function letItRain(treeObj:Tree) {
		cloud = new Cloud();
		cloud.setCloudX(treeObj.getTreeX());
		cloud.setCloudY(treeObj.getTreeY() - 50);
		aboveGrassLayer.add(cloud);
		cloud = null;
	}
	
	public function letItShine(treeObj:Tree) {
		
		sun = new SunToken();
		sun.setSunX(treeObj.getTreeX() - 2);
		sun.setSunY(treeObj.getTreeY() - 50);
		aboveGrassLayer.add(sun);
		sun = null;
	}
	
	private function unregisterAllAlertEvents() {
		
		for (i in 0...treeArray.length) {
			if(treeArray[i] != null){
				treeArray[i].unregisterAlertEvents();
			}
		}
	}
	
	private function registerAllAlertEvents() {
		
		for (i in 0...treeArray.length) {
			if(treeArray[i] != null){
				treeArray[i].registerAlertEvents();
			}
		}
		
	}
	
	private function checkForAlertClick() {
		
		for (i in 0...treeArray.length) {
			if(treeArray[i] != null){
				if (treeArray[i].getDropletClicked() || treeArray[i].getSunClicked()) {
					alertButtonSound = FlxG.sound.load(AssetPaths.alertButtonSound__wav, .3);
					alertButtonSound.play();
					if(tutorial != null){
						if (tutorial.getCurrentTutSectionNumber() == 3 && !tutUnpauseAlertsDone && tutorialOn) {
							tutorial.destroy();
							treeArray[0].unpauseAlertTimers();
							tutUnpauseAlertsDone = true;
						}
						if((tutorial.getCurrentTutSectionNumber() == 1 || tutorial.getCurrentTutSectionNumber() == 4) && tutorialOn){
							tutorial.destroy();
						}
					}
					if (treeArray[i].getSunClicked()) {
						sunClicked = true;
						dropletClicked = false;
					}
					if (treeArray[i].getDropletClicked()) {
						dropletClicked = true;
						sunClicked = false;
					}
					//members.remove(numPad);
					//members.remove(answerFieldText);
					currentTargetTree = treeArray[i];
					if(!mathless){
						bb.destroy();
						bb = null;
						bb = new ProblemBoard();
						unregisterAllAlertEvents();
						add(numPad);
						add(bb);
						add(answerFieldText);
						numPad.setPadScrollingIn(true);
						numPad.registerEvent();
						bb.pickOperation("tree");
						bb.setBoardInUse(true);
						overlaysInUse = true;
					}
					treeArray[i].setDropletClicked(false);
					treeArray[i].setSunClicked(false);
				}
			}
		}
		for (i in 0...juiceMachineArray.length) {
			if (juiceMachineArray[i] != null) {
				if (juiceMachineArray[i].getRepairClicked()) {
					alertButtonSound = FlxG.sound.load(AssetPaths.alertButtonSound__wav, .6);
					alertButtonSound.play();
					currentTargetJuiceMachine = juiceMachineArray[i];
					juiceMachineArray[i].setRepairClicked(false);
					if(!mathless){
						members.remove(numPad);
						members.remove(bb);
						members.remove(answerFieldText);
						juiceMachineArray[i].unregisterMachineEvents();
						add(numPad);
						add(bb);
						add(answerFieldText);
						numPad.setPadScrollingIn(true);
						numPad.registerEvent();
						bb.pickOperation("machine");
						bb.setBoardInUse(true);
						overlaysInUse = true;
					}
					else {
						currentTargetJuiceMachine.registerMachineButtonEvent();
					}
				}
			}
		}
		for (i in 0...pieMachineArray.length) {
			if (pieMachineArray[i] != null) {
				if (pieMachineArray[i].getRepairClicked()) {
					alertButtonSound = FlxG.sound.load(AssetPaths.alertButtonSound__wav, .6);
					alertButtonSound.play();
					currentTargetPieMachine = pieMachineArray[i];
					pieMachineArray[i].setRepairClicked(false);
					if(!mathless){
						members.remove(numPad);
						members.remove(bb);
						members.remove(answerFieldText);
						pieMachineArray[i].unregisterMachineEvents();
						add(numPad);
						add(bb);
						add(answerFieldText);
						numPad.setPadScrollingIn(true);
						numPad.registerEvent();
						bb.pickOperation("machine");
						bb.setBoardInUse(true);
						overlaysInUse = true;
					}
					else {
						currentTargetPieMachine.registerMachineButtonEvent();
					}
				}
			}
		}
	}
	
	private function submitAnswer() {
		
		if (numPad.getAnswerButtonPressed() || mathless) {
			if(!mathless){
				numPad.unregisterNumPadEvents();
			}
			if (numPad.getPlayerAnswerString() == bb.getProblemAnswerString() || mathless) {
				if(!mathless){	
					correctAnswerSound = FlxG.sound.load(AssetPaths.correctAnswerSoundA__wav);
					correctAnswerSound.play();
					bb.setBoarderColor("correctAnswer");
				}
				if (dropletClicked) {
					if (tutorialOn && treeArray[0].getTreeGrowthPhase() < 6) {
						treeArray[0].setThirstValue(0);
						treeArray[0].incGrowTree();
						treeArray[0].setSunlightValue(1);
					}
					currentTargetTree.setTreeWatered();
					currentTargetTree.incGrowTree();
					letItRain(currentTargetTree);
					currentTargetTree.resetDropTimer();
					dropletClicked = false;
				}
				else if (sunClicked) {
					if (tutorialOn && treeArray[0].getTreeGrowthPhase() < 6) {
						treeArray[0].setSunlightValue(0);
						treeArray[0].incGrowTree();
						treeArray[0].setThirstValue(1);
					}
					currentTargetTree.setTreeSunLight();
					currentTargetTree.incGrowTree();
					letItShine(currentTargetTree);
					currentTargetTree.resetSunTimer();
					sunClicked = false;
				}
				else if (currentTargetJuiceMachine != null){
					currentTargetJuiceMachine.setMachineOn(false);
					currentTargetJuiceMachine.resetRepairAlertTimer();
					currentTargetJuiceMachine.setAnimateRepairSprite(false);
					currentTargetJuiceMachine = null;
				}
				else if (currentTargetPieMachine != null){
					currentTargetPieMachine.setMachineOn(false);
					currentTargetPieMachine.resetRepairAlertTimer();
					currentTargetPieMachine.setAnimateRepairSprite(false);
					currentTargetPieMachine = null;
				}
			}
			else {
				if(tutorialOn){
					if(treeArray[0].getTreeGrowthPhase() > 5){
						bb.setBoarderColor("incorrectAnswer");
						incorrectAnswerSound = FlxG.sound.load(AssetPaths.incorrectAnswerSound__wav);
						incorrectAnswerSound.play();
						if (dropletClicked) {
							currentTargetTree.setTreeThirsty(null);
						}
						else if (sunClicked) {
							currentTargetTree.setTreeNoLight(null);
						}
					}
				}
				else {
					if (!mathless) {
						bb.setBoarderColor("incorrectAnswer");
						incorrectAnswerSound = FlxG.sound.load(AssetPaths.incorrectAnswerSound__wav);
						incorrectAnswerSound.play();
						if (dropletClicked) {
							currentTargetTree.setTreeThirsty(null);
						}
						else if (sunClicked) {
							currentTargetTree.setTreeNoLight(null);
						}
					}
				}
			}
			if(!mathless){
				bb.setBoardInUse(false);
				numPad.setPadScrollingIn(false);
				overlaysInUse = false;
				numPad.resetAnswerDigits();
				bb.resetProblemString();
				registerAllAlertEvents();
				numPad.setAnswerButtonPressed(false);			
			}
		}
		
	}
	
	private function destroyObjects() {
		for (i in 0...treeArray.length) {
			if (treeArray[i] != null) {
				if (treeArray[i].getAmIDeadYet()) {
					MouseEventManager.remove(treeArray[i].getTreeSprite());
					treeArray[i].getMyTile().setHasObject(false);
					if (currentTargetTree == treeArray[i]) {
						currentTargetTree = null;
						//trace("currentTargetTree deleted ");
					}
					treeArray[i].destroy();
					treeArray[i] = null;
				}
			}
		}
	}
	
	private function waterBill() {
		var clientCount:Int = 0;
		for (i in 0...treeArray.length) {
			if (treeArray[i] != null) {
				if (treeArray[i].getSprinklerSystemOn()) {
					clientCount++;
				}
			}
		}
		market.setWaterClientCount(clientCount);
	}
	
	private function electricBill() {
		var clientCount:Int = 0;
		for (i in 0...juiceMachineArray.length) {
			if (juiceMachineArray[i] != null) {
				if (juiceMachineArray[i].getMachineOn()) {
					clientCount++;
				}
			}
		}
		for (i in 0...pieMachineArray.length) {
			if (pieMachineArray[i] != null) {
				if (pieMachineArray[i].getMachineOn()) {
					clientCount++;
				}
			}
		}
		market.setElectricClientCount(clientCount);
	}
	
	private function appleBill(machinesInUse:Int) {
		appleForJuiceTimer -= FlxG.elapsed;
		if(appleCount > 1) {
			if (appleForJuiceTimer < 0) {
				appleCount -= machinesInUse*2;
				market.stockJuicePacks(machinesInUse);
				//basket.animateBasket(appleCount);
				appleForJuiceTimer = 1;
			}
		}
		else {
			for (i in 0...juiceMachineArray.length) {
				if (juiceMachineArray[i] != null) {
					juiceMachineArray[i].setFillingBottle(false);
					juiceMachineArray[i].setMachineOn(true);
					juiceMachineArray[i].turnMachineOn(null);
				}
			}
		}
	}
	
	private function appleAndJuiceBill(machinesInUse:Int) {
		appleAndJuiceForPieTimer -= FlxG.elapsed;
		if (appleCount > 1 && market.getJuicePackCount() > 0) {
			if (appleAndJuiceForPieTimer < 0) {
				appleCount -= machinesInUse * 2;
				market.decjuicePackCount(machinesInUse);
				market.stockPiePacks(machinesInUse);
				//basket.animateBasket(appleCount);
				appleAndJuiceForPieTimer = 1;
			}
		}
		else {
			for (i in 0...juiceMachineArray.length) {
				if (pieMachineArray[i] != null) {
					pieMachineArray[i].setMachineOn(true);
					pieMachineArray[i].turnMachineOn(null);
				}
			}
		}
	}
	
	private function turnWaterOff() {
		if (market.getCoinCount() <= 0) {
			market.setCoinCount(0);
			for (i in 0...treeArray.length) {
				if (treeArray[i] != null) {
					treeArray[i].setSprinklerSystemOn(false);
				}
			}
		}
	}
	
	private function applesAndJuiceForPie() {
		for (i in 0...pieMachineArray.length) {
			if (pieMachineArray[i] != null) {
				if (appleCount > 1 && market.getJuicePackCount() > 0 && pieMachineArray[i].getMachineOn()) {
					appleAndJuiceForPieTimer -= FlxG.elapsed;
					if (appleAndJuiceForPieTimer < 0) {
						appleCount -= 2;
						market.decjuicePackCount(1);
						pieMachineArray[i].setPastryFlying(true);
						add(pieMachineArray[i].getPieMachineFlyingPastry());
						appleAndJuiceForPieTimer = 1;
					}
				}
				else {
					if (pieMachineArray[i] != null) {
						pieMachineArray[i].setMachineOn(true);
						pieMachineArray[i].turnMachineOn(null);
					}
				}
				if (pieMachineArray[i].getInPiePack()) {
						market.stockPiePacks(1);
						pieMachineArray[i].setInPiePack(false);
				}
			}
		}
	}
	
	private function applesForJuice() {
		for (i in 0...juiceMachineArray.length) {
			if (juiceMachineArray[i] != null) {
				if (appleCount > 1 && juiceMachineArray[i].getMachineOn()) {
					appleForJuiceTimer -= FlxG.elapsed;
					if (appleForJuiceTimer < 0) {
						appleCount -= 2;
						juiceMachineArray[i].setBottleFlying(true);
						add(juiceMachineArray[i].getJuiceMachineFlyingBotte());
						appleForJuiceTimer = 1;
					}
				}
				else {
					if (juiceMachineArray[i] != null) {
						juiceMachineArray[i].setFillingBottle(false);
						juiceMachineArray[i].setMachineOn(true);
						juiceMachineArray[i].turnMachineOn(null);
					}
				}
				if (juiceMachineArray[i].getInJuicePack()) {
						market.stockJuicePacks(1);
						juiceMachineArray[i].setInJuicePack(false);
				}
			}
		}
	}
	
	private function waterSprinklerBill() {
		for (i in 0...treeArray.length) {
			if (treeArray[i] != null) {
				if (treeArray[i].getPayWaterBill()) {
					market.coinPaymentUtil(1);
					treeArray[i].setPayWaterBill(false);
				}
			}
		}
	}
	
	private function factoryElectricBill() {
		for (i in 0...juiceMachineArray.length) {
			if (juiceMachineArray[i] != null) {
				if (juiceMachineArray[i].getPayElectricBill()) {
					market.coinPaymentUtil(1);
					juiceMachineArray[i].setPayElectricBill(false);
				}
			}
		}
		for (i in 0...pieMachineArray.length) {
			if (pieMachineArray[i] != null) {
				if (pieMachineArray[i].getPayElectricBill()) {
					market.coinPaymentUtil(1);
					pieMachineArray[i].setPayElectricBill(false);
				}
			}
		}
	}
	
	private function removeCallBackOnPad() {
		if (numPad.getPadScrollingIn() && callBackStopOne) {
			for (i in 0...treeArray.length) {
				if (treeArray[i] != null) {
					treeArray[i].unregisterTreeClickedMouseEvent();
				}
			}
			for (i in 0...juiceMachineArray.length) {
				if (juiceMachineArray[i] != null) {
					juiceMachineArray[i].unregisterMachineEvents();
				}
			}
			for (i in 0...pieMachineArray.length) {
				if (pieMachineArray[i] != null) {
					pieMachineArray[i].unregisterMachineEvents();
				}
			}
			callBackStopOne = false;
			callBackStopTwo = true;
		}
		if (!numPad.getPadScrollingIn() && callBackStopTwo) {
			for (i in 0...treeArray.length) {
				if (treeArray[i] != null) {
					treeArray[i].registerTreeClickedMouseEvent();
				}
			}
			for (i in 0...juiceMachineArray.length) {
				if (juiceMachineArray[i] != null) {
					juiceMachineArray[i].registerMachineEvents();
				}
			}
			for (i in 0...pieMachineArray.length) {
				if (pieMachineArray[i] != null) {
					pieMachineArray[i].registerMachineEvents();
				}
			}
			callBackStopOne = true;
			callBackStopTwo = false;
		}
	}
	
	private function createAsteroidBelt() {
		if (asteroidBelt.getAsteroidBeltSprite().x == 0) {
			asteroidBelt = new AsteroidBelt(-700);
			belowBottomSprites.add(asteroidBelt);
		}
	}
	
	//Checks for fully grown tree during tutorial
	private function currentTreeGrowthValue() {
		if(tutorial != null){
			if(treeArray[0] != null && tutorial.getCurrentTutSectionNumber() < 2 && tutorialOn){ 
				tutorial.getTutTreeGrowthVal(treeArray[0].getTreeGrowthPhase());
				if (treeArray[0].getTreeGrowthPhase() == 6 && !closeTreeGrowthCheck && tutorialOn) {
					treeArray[0].setTreeSunlightTut(10);
					treeArray[0].setTreeThirstTut(15);
					tutorial.destroy();
					tutorial = new Tutorial(2);
					add(tutorial);
					closeTreeGrowthCheck = true;
				}
			}
			if (appleCount == 10 && !closeAppleCountCheck && tutorialOn) {
				tutorial.destroy();
				tutorial = new Tutorial(3);
				add(tutorial);
				treeArray[0].setSunlightValue(1);
				treeArray[0].pauseAlertTimers();
				closeAppleCountCheck = true;
			}
			if(treeArray[0] != null && tutorialOn){
				if (treeArray[0].getTreeGrowthPhase() == 1 && tutorialOn && !closeSecondTreeGrowthCheck) {
					tutorial.destroy();
					tutorial = new Tutorial(1.5);
					add(tutorial);
					closeSecondTreeGrowthCheck = true;
				}
			}
		}
	}
	
	//Market tutHand destroy
	private function marketTutOpened() {
		//TODO add conditional for the off/on tutorial
		if (marketOpenCheck && tutorialOn) {
			if (tutorial.getCurrentTutSectionNumber() == 4) {
				treeArray[0].setThirstValue(0);
				treeArray[0].setSunlightValue(1);
				if (market.getMarketOpen()) {
					tutorial.destroy();
					marketOpenCheck = false;
					tutorialOn = false;
				}
			}
		}
	}
	
	private function restartGameOver() {
		if(menuScreen != null){	
			if (menuScreen.getGameRestart() == true) {
				/*
				tutorial.destroy();
				tutorialOn = false;
				add(tutorial);
				appleCount = 0;
				market.setSeedCount(1);
				market.setPieCount(0);
				market.setJuiceCount(0);
				market.setCoinCount(0);
				market.setFishCount(0);
				market.setseedItemCount(2);
				market.setPieItemCount(1);
				market.setJuiceItemCount(1);
				market.setPiePackCount(0);
				market.setJuicePackCount(0);
				market.setGreenHouseItemCount(1);
				market.setKoiPondItemCount(1);
				
				if(greenHouse != null){
					greenHouse.destroy();
					greenHouse = null;
				}
				if(koiPond != null){
					koiPond.destroy();
					koiPond = null;
				}
				for (i in 0...treeArray.length) {
					if (treeArray[i] != null) {
						treeArray[i].destroy();
						treeArray[i] = null;
					}
				}
				for (i in 0...board.length) {
					for (j in 0...board.length) {
						board[i][j].setAsUnowned();
						board[i][j].setHasObject(false);
					}
				}
				for (i in 0...juiceMachineArray.length) {
					if (juiceMachineArray[i] != null) {
						juiceMachineArray[i].destroy();
						juiceMachineArray[i] = null;
					}
				}
				for (i in 0...pieMachineArray.length) {
					if (pieMachineArray[i] != null) {
						pieMachineArray[i].destroy();
						pieMachineArray[i] = null;
					}
				}
				for (i in 0...board.length) {
					for (j in 0...board.length) {
						board[i][j].setTileClicked(false);
					}
				}
				board[1][0].setAsOwned();
				board[2][0].setAsOwned();
				board[2][0].setHasObject(true);
				buySignCost = 2;
				menuScreen.setGameRestart(false);*/
				MouseEventManager.remove(seed);
				MouseEventManager.remove(juiceMachineIcon);
				MouseEventManager.remove(pieMachineIcon);
				MouseEventManager.remove(fishIcon);
				MouseEventManager.remove(greenHouseBarIcon);
				MouseEventManager.remove(truckButton.getTruckButtonSprite());
				MouseEventManager.remove(buildBarTab);
				MouseEventManager.remove(buildBarShaft);
				MouseEventManager.remove(buySign);
				/*for (i in 0...board.length) {
					for (j in 0...board.length) {
						MouseEventManager.remove((board[i])[j]);
					}
				}
				for (k in 0...treeArray.length) {
					if (treeArray[k] != null) {
						MouseEventManager.remove(treeArray[k].getTreeSprite());
						treeArray[k].unregisterAlertEvents();
						treeArray[k].unregisterTreeClickedMouseEvent();
						treeArray[k].unregisterAppleEvent();
					}
				}*/
				bb.setBoardInUse(false);
				floatingIsland = null;
				Greenhouse.greenHouseInPlay = false;
				tutorial = null;
				FlxG.resetState();
			}
			if (menuScreen.getGameEnd()) {
				if(greenHouse != null){
					greenHouse.destroy();
					greenHouse = null;
				}
				if(koiPond != null){
					koiPond.destroy();
					koiPond = null;
				}
				for (i in 0...treeArray.length) {
					if (treeArray[i] != null) {
						treeArray[i].destroy();
						treeArray[i] = null;
					}
				}
				for (i in 0...board.length) {
					for (j in 0...board.length) {
						board[i][j].setAsUnowned();
						board[i][j].setHasObject(false);
					}
				}
				for (i in 0...juiceMachineArray.length) {
					if (juiceMachineArray[i] != null) {
						juiceMachineArray[i].destroy();
						juiceMachineArray[i] = null;
					}
				}
				for (i in 0...pieMachineArray.length) {
					if (pieMachineArray[i] != null) {
						pieMachineArray[i].destroy();
						pieMachineArray[i] = null;
					}
				}
				
				tileBoard.destroyAllTiles();
				System.exit(0);
			}
		}
	}
	
	private function destroySound() {
		if (coinsSound != null && !coinsSound.playing) {
			coinsSound = FlxDestroyUtil.destroy(coinsSound);
			coinsSound = null;
			//trace("destroying counsound");
		}
		if (alertButtonSound != null && !alertButtonSound.playing) {
			alertButtonSound = FlxDestroyUtil.destroy(alertButtonSound);
			alertButtonSound = null;
			//trace("destroying alertButtonSound");
		}
		if (correctAnswerSound != null && !correctAnswerSound.playing) {
			correctAnswerSound = FlxDestroyUtil.destroy(correctAnswerSound);
			correctAnswerSound = null;
			//trace("destroying correctAnswerSound");
		}
		if (incorrectAnswerSound != null && !incorrectAnswerSound.playing) {
			incorrectAnswerSound = FlxDestroyUtil.destroy(incorrectAnswerSound);
			incorrectAnswerSound = null;
			//trace("destroying incorrectAnswerSound");
		}
		if (plantingSeedSound != null && !plantingSeedSound.playing) {
			plantingSeedSound = FlxDestroyUtil.destroy(plantingSeedSound);
			plantingSeedSound = null;
			//trace("destroying plantingSeedSound");
		}
		if (placedMachineSound != null && !placedMachineSound.playing) {
			placedMachineSound = FlxDestroyUtil.destroy(placedMachineSound);
			placedMachineSound = null;
			//trace("destroying plantingSeedSound");
		}
		if (buyLandSound != null && !buyLandSound.playing) {
			buyLandSound = FlxDestroyUtil.destroy(buyLandSound);
			buyLandSound = null;
			//trace("destroying buyLandSound");
		}
		if (buySignSound != null && !buySignSound.playing) {
			buySignSound = FlxDestroyUtil.destroy(buySignSound);
			buySignSound = null;
			//trace("destroying buySignSound");
		}
		if (truckButtonSound != null && !truckButtonSound.playing) {
			truckButtonSound = FlxDestroyUtil.destroy(truckButtonSound);
			truckButtonSound = null;
			//trace("destroying truckButtonSound");
		}
		if (koiPondPlacedSound != null && !koiPondPlacedSound.playing) {
			koiPondPlacedSound = FlxDestroyUtil.destroy(koiPondPlacedSound);
			koiPondPlacedSound = null;
			//trace("destroying koiPondPlacedSound");
		}
	}
	
	private function buildBarOpen(sprite:FlxSprite) {
		if (!buildBarIsOpen) {
			buildBarIsOpen = true;	
		}
		else {
			buildBarIsOpen = false;
		}
	}
	
	private function buildBarActions() {
		if (buildBarIsOpen) {
			buildBarOpenTimer -= FlxG.elapsed;
			if(buildBarShaft.x < -21){
				buildBarShaft.x += buildBarOpenSpeed;
				buildBarTab.x += buildBarOpenSpeed;
				seed.x += buildBarOpenSpeed;
				seedDrag.x += buildBarOpenSpeed;
				juiceMachineIcon.x += buildBarOpenSpeed;
				juiceMachineIconDrag.x += buildBarOpenSpeed;
				pieMachineIcon.x += buildBarOpenSpeed;
				pieMachineIconDrag.x += buildBarOpenSpeed;
				fishIcon.x += buildBarOpenSpeed;
				fishIconDrag.x += buildBarOpenSpeed;
				greenHouseBarIcon.x += buildBarOpenSpeed;
				greenHouseBarIconDrag.x += buildBarOpenSpeed;
				market.setCountTextXOpen(buildBarOpenSpeed);
			}
			if (buildBarOpenTimer <= 0 && buildBarOpenTimer > -1) {
				buildBarIsOpen = false;
			}
		}
		else {
			buildBarOpenTimer = 15;
			if (buildBarShaft.x > -185) {
				buildBarShaft.x -= buildBarOpenSpeed;
				buildBarTab.x -= buildBarOpenSpeed;
				seed.x -= buildBarOpenSpeed;
				seedDrag.x -= buildBarOpenSpeed;
				juiceMachineIcon.x -= buildBarOpenSpeed;
				juiceMachineIconDrag.x -= buildBarOpenSpeed;
				pieMachineIcon.x -= buildBarOpenSpeed;
				pieMachineIconDrag.x -= buildBarOpenSpeed;
				fishIcon.x -= buildBarOpenSpeed;
				fishIconDrag.x -= buildBarOpenSpeed;
				greenHouseBarIcon.x -= buildBarOpenSpeed;
				greenHouseBarIconDrag.x -= buildBarOpenSpeed;
				market.setCountTextXClose(buildBarOpenSpeed);
			}
		}
		if (buildBarShaft.x >= -21) {
			seedDrag.revive();	
			juiceMachineIconDrag.revive();
			pieMachineIconDrag.revive();
			fishIconDrag.revive();
			greenHouseBarIconDrag.revive();
		}
		else {
			seedDrag.kill();
			juiceMachineIconDrag.kill();
			pieMachineIconDrag.kill();
			fishIconDrag.kill();
			greenHouseBarIconDrag.kill();
		}
		
	}
	
	/**
	 * Function that is called once every frame.
	 */
	override public function update():Void
	{
		removeCallBackOnPad();//removess all game callbacks when numpad is up
		factoryElectricBill();
		waterSprinklerBill();
		applesForJuice();
		applesAndJuiceForPie();
		turnWaterOff();
		destroyObjects();
		checkForAlertClick();
		submitAnswer();
		stopSeedDragIfBoard();
		stopJuiceDragIfBoard();
		stopPieDragIfBoard();
		stopFishDragIfBoard();
		stopGreenHouseDragIfBoard();
		tileBoarderFlip();
		displayAppleCount();
		displayPadNumbers();
		truckLoadingGoods();
		createFloatingIsland();
		createAsteroidBelt();
		currentTreeGrowthValue();
		marketTutOpened();
		restartGameOver();
		destroySound();
		buildBarActions();
		super.update();
	}
	
	/**
	 * Function that is called when this state is destroyed - you might want to 
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		super.destroy();
	}

}