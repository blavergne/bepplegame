package ;

import flixel.addons.display.FlxSkewedSprite;
import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.FlxG;
import flixel.system.FlxSound;
import flixel.util.FlxDestroyUtil;

/**Apple truck
 * ...
 * @author ...
 */
class AppleTruck extends FlxGroup
{

	private var appleTruckGroup:FlxGroup;
	private var appleTruckSprite:FlxSprite;
	private var truckTravelTime:Float = 5;
	private var truckCapacity:Int;
	private var truckSize:Int;
	private var pickUpReady:Bool = false;
	private var loadingApples:Bool = false;
	private var truckIsReset:Bool = false;
	private var truckSoundReady:Bool = true;
	private var truckSound:FlxSound;
	
	public function new() 
	{
		super();
		appleTruckSprite = new FlxSprite();
		appleTruckGroup = new FlxGroup();
		appleTruckSprite.loadGraphic(AssetPaths.appleTruck__png, false);
		appleTruckSprite.x = -55;
		appleTruckSprite.y = 305;
		setTruckSize(1);
		appleTruckGroup.add(appleTruckSprite);
		add(appleTruckGroup);
	}
	
	public function getLoadingApples() {
		return loadingApples;
	}
	
	public function getAppleTruckCapacity() {
		return truckCapacity;
	}
	
	public function getTruckIsReset() {
		return truckIsReset;
	}
	
	public function decTruckCapacity() {
		truckCapacity -= 10;
	}
	
	public function setTruckCapacity(val:Int) {
		truckCapacity = val;
	}
	
	public function setTruckSize(val:Int) {
		switch(val) {
			case 0:
				truckCapacity = 10;
			case 1:
				truckCapacity = 20;
			case 2:
				truckCapacity = 30;
			case 3:
				truckCapacity = 40;
		}
	}
	
	public function setLoadingApples(val:Bool) {
		loadingApples = val;
	}
	
	public function setPickUpReady(val:Bool) {
		pickUpReady = val;
	}
	
	private function truckPickup() {
		if (pickUpReady) {
			truckTravelTime -= FlxG.elapsed;
			if(truckSoundReady){
				truckSound = FlxG.sound.load(AssetPaths.truckSound__wav, .7);
				truckSound.play();
				truckSoundReady = false;
			}
			if (appleTruckSprite.x < 35 && truckTravelTime < 0) {
				truckIsReset = false;
				appleTruckSprite.x += 1;
				appleTruckSprite.y -= .5;
			}
			if (appleTruckSprite.x == 35) {
				loadingApples = true;
			}
		}
	}
	
	private function truckGoHome() {
		if (!pickUpReady) {
			if (appleTruckSprite.x > -55 && appleTruckSprite.y < 370) {
				appleTruckSprite.x -= 1;
				appleTruckSprite.y += .5;
				truckTravelTime = 5;
			}
			if (appleTruckSprite.x == -55) {
				truckIsReset = true;
				truckSoundReady = true;
			}
		}
	}

	private function destroySounds() {
		if (truckSound != null && !truckSound.playing) {
			try{
				truckSound = FlxDestroyUtil.destroy(truckSound);
				//trace("destroying truckSound");
			}
			catch (e:Dynamic){
				//trace("Error" + " " + e);
			}
		}
	}
	
	override public function destroy() {
		appleTruckSprite = null;
		appleTruckGroup = null;
		truckSound = null;
		super.destroy();
	}
	
	override public function update() {
		truckGoHome();
		truckPickup();
		destroySounds();
		super.update();
	}
	
}