package ;

import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.plugin.MouseEventManager;
import flixel.FlxG;
import flixel.system.FlxSound;

/**
 * ...
 * @author ...
 */
class Cloud extends FlxGroup
{

	private var cloudGroup:FlxGroup = new FlxGroup();
	private var cloud:FlxSprite;
	private var rainSound:FlxSound;
	var cloudDone:Bool = false;
	var destroyCloud:Bool = false;
	
	public function new() 
	{
		
		super();
		cloud = new FlxSprite();
		cloud.loadGraphic(AssetPaths.rainCloudAnimationA__png, true);
		rainSound = FlxG.sound.load(AssetPaths.raining__wav);
		rainSound.play();
		cloud.animation.add("rain", [0, 1], 10, true);
		cloud.animation.play("rain");
		cloud.alpha = 0.0;
		cloudGroup.add(cloud);
		add(cloudGroup);
		
	}
	
	public function getCloudAlpha() {
		
		return cloud.alpha;
		
	}
	
	public function setCloudX(xVal:Float) {
		
		cloud.x = xVal;
		
	}
	public function setCloudY(yVal:Float) {
		
		cloud.y = yVal;
		
	}
	
	private function fadeCloud() {
		
		if (cloud.alpha < 1 && !cloudDone) {
			cloud.alpha += .01;
		}
		else {
			cloudDone = true;
			cloud.alpha -= .005;
			if (cloud.alpha <= 0) {
				destroyCloud = true;
				//trace("destroy cloud");
				destroy();
			}
		}
		
	}
	
	override public function update() {
		
		fadeCloud();

		if(!destroyCloud){
			super.update();
		}
	}
	
	override public function destroy() {
		cloudGroup = null;
		cloud = null;
		rainSound = null;
		destroy;
		super.destroy();
		
	}
}