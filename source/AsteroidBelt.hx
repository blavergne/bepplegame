package ;

import flixel.FlxSprite;
import flixel.group.FlxGroup;
/**
 * ...
 * @author BrioLavone
 */
class AsteroidBelt extends FlxGroup
{

	private var asteroidBelt:FlxSprite;
	
	public function new(beltXPos:Int) 
	{
		super();
		asteroidBelt = new FlxSprite();
		asteroidBelt.loadGraphic(AssetPaths.meteorField__png, false);
		asteroidBelt.y = 120;
		asteroidBelt.x = beltXPos;
		add(asteroidBelt);
	}
	
	public function getAsteroidBeltSprite() {
		return asteroidBelt;
	}
	
	public function setAsteroidBeltSprite(val:Int) {
		asteroidBelt.x = val;
	}
	
	public function asteroidFloat() {
		asteroidBelt.x += .25;
	}
	
	public function destroyAsteroids() {
		if (asteroidBelt.x >= 640) {
			//trace("destroy asteroid belt");
			destroy();
			asteroidBelt = null;
		}
	}
	
	override public function destroy() {
		asteroidBelt = null;
		super.destroy();
	}
	
	override public function update() {
		asteroidFloat();
		destroyAsteroids();
		if(asteroidBelt != null){
			super.update();
		}
	}
}