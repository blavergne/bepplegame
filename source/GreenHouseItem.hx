package ;

import flixel.FlxSprite;

/**
 * ...
 * @author ...
 */
class GreenHouseItem extends MarketItem
{

	public function new() 
	{
		super();
		initMarketItems();
	}
	
	private function initMarketItems() {
		itemSprite = new FlxSprite();
		itemCost = 50;
		itemName = "greenHouseItem";
		itemCount = 1;
		itemSprite.loadGraphic(AssetPaths.GreenHouseItemAnimation__png, true, 30, 30, false , "greenHouseItem");
		itemSprite.animation.add("available", [0], 1, false);
		itemSprite.animation.add("notAvailable", [1], 1, false);
		setItemAvailable(true);
	}
	
	public function setGreenHouseItemCount(val:Int) {
		itemCount = val;
	}
	
	override public function update() {
		setAvailableAnimation();
		super.update();
	}
	
}