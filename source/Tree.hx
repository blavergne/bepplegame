package ;

import flixel.addons.display.FlxExtendedSprite;
import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.plugin.MouseEventManager;
import flixel.input.keyboard.FlxKeyboard;
import flixel.FlxG;
import flixel.system.FlxSound;
import flixel.util.FlxCollision;
import flixel.util.FlxColor;
import flixel.text.FlxText;
import flixel.util.FlxTimer;
import flixel.tweens.FlxTween;
import flixel.tweens.FlxEase;
import flixel.util.FlxDestroyUtil;

/**
 * ...
 * @author BrioLavone
 */
class Tree extends FlxGroup
{

	private var treeGroup:FlxGroup = new FlxGroup();
	private var appleArray:Array<Apple>;
	private var apple:Apple;
	private var myTile:Tile;
	private var mouse:Mouse;
	private var treeGround:FlxSprite;
	private var tree:FlxSprite;
	private var droplet:FlxSprite;
	private var dropletA:FlxSprite;
	private var dropletB:FlxSprite;
	private var sunA:FlxSprite;
	private var sunB:FlxSprite;
	private var sunC:FlxSprite;
	private var sprinklerSprite:FlxSprite;
	private var sprinklerButtonSprite:FlxSprite;
	private var minusOneSprite:FlxSprite;
	private var basketSprite:FlxSprite;
	private var sprinklerSound:FlxSound;
	private var treeGrowthSound:FlxSound;
	private var sprinklerButtonSound:FlxSound;
	private var boardObj:FlxGroup;
	private var answerString:String;
	private var maxThirst:Int = 3;
	private var minThirst:Int = -3;
	private var treeGrowthPhase:Int = 0; 
	private var treeThirst:Int = 1;
	private var treeSunlight:Int = 0;
	private var appleCount:Int = 0;
	private var appleIndex:Int = 0;
	private var adultTree:Int = 6;
	private var takenAppleIndex:Int;
	private var goldCount:Int;
	private var alertDropPauseTimer:Float = 1;
	private var alertSunPauseTimer:Float = 1;
	private var treeThirstTimer:Float = 15;
	private var treeSunLightTimer:Float = 10;
	private var oldTreeSunLightTimer:Float = 0;
	private var waterUtlilityTimer:Float = 30;
	private var sprinklerOutTime:Float = 2;
	private var timeToTreeDeath:Float = 10;
	private var appleSeperator:Float = 0;
	private var mouseTimer:Float = 0;
	private var sunAlertCoolDown:Float = 0;
	private var dropletAlertCoolDown:Float = 0;
	private var sprinklerAnimationTimer:Float = 0;
	private var dropAlertTime:FlxTimer;
	private var sunlightAlertTime:FlxTimer;
	private var oldSunTimerChanged:Bool = false;
	private var mouseInPlay:Bool = false;
	private var dropletClicked:Bool = false;
	private var sunClicked:Bool = false;
	private var treeClicked:Bool = false;
	private var appleLock:Bool = true;
	private var amIdeadYet:Bool = false;
	private var appleClicked:Bool = false;
	private var takenAppleCarried:Bool = false;
	private var checkingCollisions:Bool = true;
	private var sprinklerSystemOn:Bool = false;
	private var sprinklerOutPhase:Bool = true;
	private var sprinklerInPhase:Bool = true;
	private var extendedTime:Bool = true;
	private var payWaterBill:Bool = false;
	private var appleStored:Bool = false;
	private var deathRattle:Bool = false;
	private var mathless:Bool = false;
	private var greenHouseInPlay:Bool = false;
	private var dropAlertUsed:Bool = false;
	private var sunAlertUsed:Bool = false;
	private static var universalAlertClicked = false;
	
	public function new(basket:FlxSprite) 
	{
		super();
		appleArray = new Array<Apple>();
		dropAlertTime = new FlxTimer();
		sunlightAlertTime = new FlxTimer();
		boardObj = new FlxGroup();
		treeGround = new FlxSprite();
		tree = new FlxSprite();
		droplet = new FlxSprite();
		dropletA = new FlxSprite();
		dropletB = new FlxSprite();
		sunA = new FlxSprite();
		sunB = new FlxSprite();
		sunC = new FlxSprite();
		sprinklerSprite = new FlxSprite();
		minusOneSprite = new FlxSprite();
		mathless = MenuState.mathless;
		basketSprite = basket;
		minusOneSprite.loadGraphic(AssetPaths.minusOneImage__png);
		minusOneSprite.alpha = 0;
		sprinklerSprite.loadGraphic(AssetPaths.sprinklerAnimation__png, true, 64, 64);
		sprinklerSprite.animation.add("sprinklerOut", [0, 1 , 2, 3], 5, false);
		sprinklerSprite.animation.add("sprinklerSpray", [4, 5, 6, 7, 8, 9], 10, true);
		sprinklerSprite.animation.add("sprinklerIn", [3, 2, 1, 0], 10, false);
		sprinklerButtonSprite = new FlxSprite();
		sprinklerButtonSprite.loadGraphic(AssetPaths.sprinklerSwtichAnimation__png, true, 16, 30);
		sprinklerButtonSprite.animation.add("sprinklerSwitchOn", [1], 1, false);
		sprinklerButtonSprite.animation.add("sprinklerSwitchOff", [0], 1, false);
		sprinklerButtonSprite.animation.play("sprinklerSwitchOff");
		MouseEventManager.add(sprinklerButtonSprite, purchaseSprinklerBuff, null, null, null);
		sprinklerSprite.alpha = 0;
		sprinklerButtonSprite.alpha = 0;
		treeGround.makeGraphic(100, 2);
		treeGround.alpha = 1;
		tree.loadGraphic(AssetPaths.treeAnimationB__png, true, 71, 74);
		droplet.loadGraphic(AssetPaths.dropletsAnimationS__png, true, 18, 18);
		droplet.animation.add("droplet", [0, 1, 2, 3, 4, 5, 6, 7, 8], 15, true);
		dropletA.loadGraphic(AssetPaths.dropletsAnimationT__png, true, 18, 18);
		dropletA.animation.add("dropletA", [0, 1, 2, 3, 4, 5, 6, 7, 8], 15, true);
		dropletB.loadGraphic(AssetPaths.dropletsAnimationU__png, true, 18, 18);
		dropletB.animation.add("dropletB", [0, 1, 2, 3, 4, 5, 6, 7, 8], 15, true);
		sunA.loadGraphic(AssetPaths.sunAnimationA__png, true, 18, 18);
		sunA.animation.add("sunA", [0, 1, 2, 3, 4, 5], 10, true);
		sunB.loadGraphic(AssetPaths.sunAnimationB__png, true, 18, 18);
		sunB.animation.add("sunB", [0, 1, 2, 3, 4, 5], 10, true);
		sunC.loadGraphic(AssetPaths.sunAnimationC__png, true, 18, 18);
		sunC.animation.add("sunC", [0, 1, 2, 3, 4, 5], 10, true);
		tree.animation.add("0", [0], 1, false);
		tree.animation.add("1", [1, 2, 3], 1, false);
		tree.animation.add("2", [4, 5, 6, 7], 1, false);
		tree.animation.add("3", [8, 9, 10, 11], 1, false);
		tree.animation.add("4", [12, 13, 14, 15], 1, false);
		tree.animation.add("5", [16, 17, 18, 19], 1, false);
		tree.animation.add("6", [20, 21, 22, 23], 1, false);
		tree.animation.add("7", [24], 1, false);
		tree.animation.add("8", [25], 1, false);
		tree.animation.add("9", [26], 1, false);
		droplet.alpha = 0;
		sunA.alpha = 0;
		sunB.alpha = 0;
		sunC.alpha = 0;
		initAppleArray();
		registerAlertEvents();
		growTree();
		treeGroup.add(sprinklerButtonSprite);
		treeGroup.add(tree);
		treeGroup.add(dropletB);
		treeGroup.add(dropletA);
		treeGroup.add(droplet);
		treeGroup.add(sunC);
		treeGroup.add(sunB);
		treeGroup.add(sunA);
		treeGroup.add(boardObj);
		treeGroup.add(treeGround);
		treeGroup.add(minusOneSprite);
		treeGroup.add(sprinklerSprite);
		add(treeGroup);
		dropAlertTime.start(treeThirstTimer, setTreeThirsty, 0);
		sunlightAlertTime.start(treeSunLightTimer, setTreeNoLight, 0);
	}
	
	public function getTreeClicked() {
		return treeClicked;
	}
	
	public function getTreeSprite() {
		return tree;
	}
	
	public function getTreeX() {
		
		return tree.x;
		
	}
	
	public function getTreeY() {
		
		return tree.y;
		
	}
	
	public function getAmIDeadYet() {
		return amIdeadYet;
	}
	
	public function getTreeThirst() {
		
		return treeThirst;
		
	}
	
	public function getAppleStored() {
		return appleStored;
	}
	
	public function setAppleStored(val:Bool) {
		appleStored = val;
	}
	
	public function getDropletClicked() {
		
		return dropletClicked;
		
	}
	
	public function getSunClicked() {
		
		return sunClicked;
		
	}

	public function getUniversalAlertClicked() {
		
		return universalAlertClicked;
		
	}
	
	public function getMyTile() {
		return myTile;
	}
	
	public function getAppleClicked() {
		return appleClicked;
	}
	
	public function getTreeGrowthPhase() {
		return treeGrowthPhase;
	}
	
	public function getSprinklerSystemOn() {
		return sprinklerSystemOn;
	}
	
	public function getSprinklerButtonSprite() {
		return sprinklerButtonSprite;
	}
	
	public function getPayWaterBill() {
		return payWaterBill;
	}
	
	public function getAppleArray() {
		return appleArray;
	}
	
	//set members
	//
	//
	public function setTreeThirstTut(tutThirstTimer:Int) {
		dropAlertTime.cancel;
		dropAlertTime.start(tutThirstTimer, setTreeThirsty, 0);
	}
	
	public function setTreeSunlightTut(tutSunlightTimer:Int) {
		sunlightAlertTime.cancel;
		sunlightAlertTime.start(tutSunlightTimer, setTreeNoLight, 0);
	}
	
	public function setTreeThistTutOff() {
		dropAlertTime.start(treeThirstTimer, setTreeThirsty, 0);
	}
	
	public function setTreeSunlightTutOff() {
		sunlightAlertTime.start(treeSunLightTimer, setTreeNoLight, 0);
	}
	
	public function setPayWaterBill(val:Bool) {
		payWaterBill = val;
	}
	
	public function setSprinklerSystemOn(val:Bool) {
		sprinklerSystemOn = val;
	}
	
	public function setTreeClicked(val:Bool) {
		treeClicked = val;
	}
	
	public function setAppleClicked(val:Bool) {
		appleClicked = val;
	}
	
	public function setMyTile(val:Tile) {
		myTile = val;
	}
	
	public function setTreeGroundColor(owned:Bool) {
		if(owned){
			treeGround.color = 0x3eaf2d;
		}
		else {
			treeGround.color = 0x00f6d01;
		}
	}
	
	public function setTreeGrowthPhase(val:Int) {
		treeGrowthPhase = val;
	}
	
	public function setUniversalAlertClicked(val:Bool) {
		
		universalAlertClicked = val;
		
	}
	
	public function setDropletClicked(val:Bool) {
		
		dropletClicked = val;
		
	}
	
	public function setSunClicked(val:Bool) {
		
		sunClicked = val;
		
	}
	
	public function setTreeX(xVal:Float) {
		
		tree.x = xVal;
		
	}
	
	public function setTreeY(yVal:Float) {
		
		tree.y = yVal;
		
	}
	
	public function setTreeWatered() {
		if(treeThirst > -3){
			treeThirst--;
		}
	}
	
	public function setTreeThirsty(timer:FlxTimer) {
		if (treeThirst < 3) {
			treeThirst++;
		}
		
	}
	
	public function setTreeSunLight() {
		if(treeSunlight > -3){
			treeSunlight--;
		}
	}
	
	public function setOldTreeSunLightTimer(oldSunlightTimerVal:Float) {
		oldTreeSunLightTimer = oldSunlightTimerVal;
	}
	
	public function setThirstValue(thirstVal:Int) {
		treeThirst = thirstVal;
	}

	public function setSunlightValue(sunlightVal:Int) {
		treeSunlight = sunlightVal;
	}
	
	public function setTreeNoLight(timer:FlxTimer) {
		if (treeSunlight < 3 && !amIdeadYet) {
			treeSunlight++;
			sunlightAlertTime.start(treeSunLightTimer, setTreeNoLight, 0);
		}
		
	}
	
	
	//member functions
	//
	//
	public function pauseAlertTimers() {
		dropAlertTime.start(999, setTreeThirsty, 0);
		//trace("4");
		sunlightAlertTime.start(999, setTreeNoLight, 0);
	}
	
	public function unpauseAlertTimers() {
		dropAlertTime.start(treeThirstTimer, setTreeThirsty, 0);
		//trace("5");
		sunlightAlertTime.start(treeSunLightTimer, setTreeNoLight, 0);
	}
	
	private function initAppleArray() {
		for (i in 0...3) {
			appleArray[i] = null;
		}
	}
	
	public function resetDropTimer() {
		
		dropAlertTime.reset();
		
	}
	
	public function resetSunTimer() {
		
		sunlightAlertTime.reset();
		
	}
	
	public function registerTreeClickedMouseEvent() {
		MouseEventManager.add(sprinklerButtonSprite, purchaseSprinklerBuff, null, null, null);
	}
	
	public function unregisterTreeClickedMouseEvent() {
		MouseEventManager.remove(sprinklerButtonSprite);
	}
	
	public function registerAppleEvent() {
		for (i in 0...3) {
			if(appleArray[i] != null){
				MouseEventManager.add(appleArray[i].getAppleSprite(), pickApple, null, null, null);
			}
		}
	}
	
	public function unregisterAppleEvent() {
		for (i in 0...3) {
			if(appleArray[i] != null){
				MouseEventManager.remove(appleArray[i].getAppleSprite());
			}
		}
	}
	
	public function registerAlertEvents() {
		MouseEventManager.add(droplet, openProblem, null, null, null);
		MouseEventManager.add(dropletA, openProblem, null, null, null);
		MouseEventManager.add(dropletB, openProblem, null, null, null);
		MouseEventManager.add(sunA, openProblem, null, null, null);
		MouseEventManager.add(sunB, openProblem, null, null, null);
		MouseEventManager.add(sunC, openProblem, null, null, null);
	}
	
	public function unregisterAlertEvents() {
		
		MouseEventManager.remove(droplet);
		MouseEventManager.remove(dropletA);
		MouseEventManager.remove(dropletB);
		MouseEventManager.remove(sunA);
		MouseEventManager.remove(sunB);
		MouseEventManager.remove(sunC);
		
	}
	
	public function registerSunAlertEvents() {
		MouseEventManager.add(sunA, openProblem, null, null, null);
		MouseEventManager.add(sunB, openProblem, null, null, null);
		MouseEventManager.add(sunC, openProblem, null, null, null);
	}
	
	public function unregisterSunAlertEvents() {
		MouseEventManager.remove(sunA);
		MouseEventManager.remove(sunB);
		MouseEventManager.remove(sunC);
	}
	
	public function registerDropletAlertEvents() {
		MouseEventManager.add(droplet, openProblem, null, null, null);
		MouseEventManager.add(dropletA, openProblem, null, null, null);
		MouseEventManager.add(dropletB, openProblem, null, null, null);
	}
	
	public function unregisterDropletAlertEvents() {
		MouseEventManager.remove(droplet);
		MouseEventManager.remove(dropletA);
		MouseEventManager.remove(dropletB);
	}
	
	private function openProblem(sprite:FlxSprite) {
		
		if (sprite == droplet || sprite == dropletA || sprite == dropletB) {
			dropletClicked = true;
			dropAlertUsed = true;
		}
		if (sprite == sunA || sprite == sunB || sprite == sunC) {
			sunClicked = true;
			sunAlertUsed = true;
		}
		
	}
	
	/*prevents user from clicking droplet or sun alert sprites more than once before sprite fades.
	 */
	private function alertPause() {
		if (mathless) {
			if (dropAlertUsed) {
				unregisterDropletAlertEvents();
				alertDropPauseTimer -= FlxG.elapsed;
				if (alertDropPauseTimer <= 0) {
					registerDropletAlertEvents();
					dropAlertUsed = false;
					alertDropPauseTimer = 1;
				}
			}
			if (sunAlertUsed) {
				unregisterSunAlertEvents();
				alertSunPauseTimer -= FlxG.elapsed;
				if (alertSunPauseTimer <= 0) {
					registerSunAlertEvents();
					sunAlertUsed = false;
					alertSunPauseTimer = 1;
				}
			}
		}
	}
	
	private function purchaseSprinklerBuff(sprite:FlxSprite) {
		if (goldCount > 0) {
			if (!sprinklerSystemOn) {
				if (sprinklerButtonSprite.animation.finished) {	
					sprinklerButtonSound = FlxG.sound.load(AssetPaths.sprinklerButtonSound__wav, .3);
					sprinklerButtonSound.play();
					sprinklerButtonSprite.animation.play("sprinklerSwitchOn");
					sprinklerSystemOn = true;
				}
			}
			else {
				if (sprinklerButtonSprite.animation.finished) {
					sprinklerButtonSound = FlxG.sound.load(AssetPaths.sprinklerButtonSound__wav, .3);
					sprinklerButtonSound.play();
					sprinklerButtonSprite.animation.play("sprinklerSwitchOff");
					sprinklerSystemOn = false;
				}
			}
		}
	}
	
	private function treeThirstCheck() {
		
		if (treeThirst >= 1) {
			
			warnThirst(true);
			
		}
		if (treeThirst <= 0) {
			
			warnThirst(false);
			
		}
		
	}
	
	private function treeSunlightCheck() {
		
		if (treeSunlight >= 1) {
			
			warnSunlight(true);
			
		}
		if (treeSunlight <= 0) {
			
			warnSunlight(false);
			
		}
		
	}
	
	private function warnThirst(val:Bool) {
		
		switch(val) {
		
			case true:
				droplet.animation.play("droplet");
				dropletA.animation.play("dropletA");
				dropletB.animation.play("dropletB");
				if (treeThirst == 1) {
					droplet.alpha += .01;	
					dropletA.alpha -= .1;
					dropletB.alpha -= .1;
				}
				if (treeThirst == 2) {
					dropletA.alpha += .01;	
					droplet.alpha -= .1;
					dropletB.alpha -= .1;
				}
				if (treeThirst == 3) {
					dropletB.alpha += .01;	
					droplet.alpha -= .1;
					dropletA.alpha -= .1;
				}
				if (treeThirst == -99) {
					dropletB.alpha -= .1;	
					droplet.alpha -= .1;
					dropletA.alpha -= .1;
				}
			case false:	
				dropletA.alpha -= .1;
				dropletB.alpha -= .1;
				droplet.alpha -= .1;
		}		
	}
	
	private function warnSunlight(val:Bool) {
		
		switch(val) {
		
			case true:
				sunA.animation.play("sunA");
				sunB.animation.play("sunB");
				sunC.animation.play("sunC");
				if (treeSunlight == 1) {
					sunA.alpha += .01;	
					sunB.alpha -= .1;
					sunC.alpha -= .1;
				}
				if (treeSunlight == 2) {
					sunB.alpha += .01;	
					sunA.alpha -= .1;
					sunC.alpha -= .1;
				}
				if (treeSunlight == 3) {
					sunC.alpha += .01;	
					sunA.alpha -= .1;
					sunB.alpha -= .1;
				}
				
			case false:	
				sunA.alpha -= .1;
				sunB.alpha -= .1;
				sunC.alpha -= .1;
		}		
	}
	
	private function appleIncubator() {
		releaseAppleMem();
		if (treeGrowthPhase == adultTree && treeThirst < 1 && treeSunlight < 1) {
			appleSeperator -= FlxG.elapsed;
			if (appleCount < 3 && appleSeperator < 0 && appleLock) {
				appleLock = false;
				apple = new Apple();
				MouseEventManager.add(apple.getAppleSprite(), pickApple, null, null, null);
				apple.setAppleX(getTreeX() + apple.getRandomX());
				apple.setAppleY(getTreeY() + apple.getRandomY());
				while (checkAppleOverlap()) {
					apple.setAppleX(getTreeX() + apple.getRandomX());
					apple.setAppleY(getTreeY() + apple.getRandomY());
				}
				apple.setRiseRun(basketSprite);
				apple.setMyInitialY(apple.getMyYPos());
				//collideGroundCheck();
				addAppleToArray();
				treeGroup.add(apple);
				appleCount++;
				appleSeperator = 5;

			}	
			appleStuff();
		}
	
		if (treeGrowthPhase >= adultTree) {
			if (((treeThirst > 1 && treeSunlight > 1) || (treeThirst > 2 || treeSunlight > 2)) || deathRattle) {
				timeToTreeDeath -= FlxG.elapsed;
				if (timeToTreeDeath < 0) {
					treeGrowthPhase++;
					deathRattle = true;
					if (treeGrowthPhase == 10) {
						amIdeadYet = true;
					}
					droplet.alpha = 0;
					dropletA.alpha = 0;
					dropletB.alpha = 0;
					sunA.alpha = 0;
					sunB.alpha = 0;
					sunC.alpha = 0;
					//treeSunLightTimer = 9999;
					//treeThirstTimer = 9999;
					treeThirst = -99;
					treeSunlight = -99;
					killAllApples();
					growTree();
					myTile.setHasObject(false);
					timeToTreeDeath = 10;
				}
			}
		}
		
	}

	private function releaseAppleMem() {
		for (i in 0...appleArray.length) {
			if (appleArray[i] != null) {
				if (appleArray[i].getInBasket()) {
					MouseEventManager.remove(appleArray[i].getAppleSprite());
					appleArray[i].getAppleSprite().kill();
					//trace("apple destroyed");
					appleArray[i].destroy();
					appleArray[i] = null;
					appleStored = true;
					appleCount--;
				}
			}
		}
	}
	
	private function killAllApples() {
		for (i in 0...appleArray.length) {
			if (appleArray[i] != null) {
				appleArray[i].destroy();
				appleArray[i] = null;
			}
		}
	}
	
	private function addAppleToArray() {
		for (i in 0...appleArray.length) {
			if (appleArray[i] == null) {
				appleArray[i] = apple;
				return;
			}
		}
		
	}
	
	private function appleStuff() {
		
		for (i in 0...appleArray.length ) {
			if (appleArray[i] != null && appleArray[i].getAppleGrowth() < 1) {	
				appleArray[i].ripenApple();
				if (appleArray[i].getAppleVisible()) {
					appleLock = true;
				}
			}
		}
	}
	
	private function pickApple(sprite:FlxSprite) {
		for (i in 0...appleArray.length) {
			if(appleArray[i] != null){	
				if (appleArray[i].getAppleSprite() == sprite && appleArray[i].getIAmRipe()) {
					//appleArray[i].getAppleSprite().kill();
					//appleArray[i].destroy();
					//appleArray[i] = null;
					//pickedLeavesSound.play();
					appleArray[i].setLeavesFallOnce(true);
					appleArray[i].setFlyToBasket(true);
					appleClicked = true;
				}
			}
		}
	}
	
	private function killDeadApple() {
		for (i in 0...appleArray.length) {
			if (appleArray[i] != null) {
				if (appleArray[i].getIAmDead()) {
					appleArray[i].getAppleSprite().kill();
					appleArray[i].destroy();
					appleArray[i] = null;
					appleCount--;
				}
			}
		}
	}
	
	private function checkAppleOverlap() {
		
		for (i in 0...appleArray.length) {
		
			if (appleArray[i] != null && ((apple.getMyXPos() >= appleArray[i].getMyXPos() - 10 && apple.getMyXPos() <= appleArray[i].getMyXPos() + 10) && 
				(apple.getMyYPos() >= appleArray[i].getMyYPos() - 10 && apple.getMyYPos() <= appleArray[i].getMyYPos() + 10))) {	
				return true;
			}
			
		}
		return false;
	}

	private function mouseAttack() {
		mouseTimer -= FlxG.elapsed;
		if (checkForAppleRotTime() && !mouseInPlay) {
			if (mouseTimer < 0) {
				mouseInPlay = true;
				checkingCollisions = true;
				mouse = new Mouse();
				mouse.getMouseSprite().x = getTreeX() + 80;
				mouse.getMouseSprite().y = getTreeY() + 61;
				mouse.getHoleSprite().x = getTreeX() + 80;
				mouse.getHoleSprite().y = getTreeY() + 61;
				add(mouse);
			}
		}
	}
	
	private function checkForAppleRotTime():Bool {
		for (i in 0...appleArray.length) {
			if (appleArray[i] != null) {
				if (appleArray[i].getRotTimer() <= 10) {
					return true;
				}
			}
		}
		return false;
	}
	
	private function checkMouseDone() {
		if(mouseInPlay){	
			if (mouse.getMouseAllDone()) {
				mouse.destroy();
				mouse = null;
				mouseInPlay = false;
				takenAppleCarried = false;
				mouseTimer = 10;
			}
		}
	}
	 
	private function checkForMouseCollisions() {
		if(mouseInPlay && !takenAppleCarried && checkingCollisions){
			for (i in 0...appleArray.length) {	
				if(appleArray[i] != null){
					if (FlxCollision.pixelPerfectCheck(mouse.getMouseSprite(), appleArray[i].getAppleSprite()) && !appleArray[i].getIAmRotten()) {
						appleArray[i].setDropRate(0);
						appleArray[i].stopFreeFall();
						MouseEventManager.remove(appleArray[i].getAppleSprite());
						takenAppleIndex = i;
						takenAppleCarried = true;
						mouse.setMouseCollision(true);
					}
				}
			}
			if (mouse.getMouseSprite().x == mouse.getHoleSprite().x - 90) {
				mouse.faceRight();
				mouse.setMouseCollision(true);
			}
		}
	}
	
	private function carryApple() {
		if (takenAppleCarried && checkingCollisions) {	
			if(appleArray[takenAppleIndex] != null){
				appleArray[takenAppleIndex].getAppleSprite().x = mouse.getMouseSprite().x + 8;
				appleArray[takenAppleIndex].getAppleSprite().y = mouse.getMouseSprite().y;
				if (appleArray[takenAppleIndex].getAppleSprite().x == mouse.getHoleSprite().x) {
					checkingCollisions = false;
					appleArray[takenAppleIndex].destroy();
					appleArray[takenAppleIndex] = null;
					appleCount--;
				}
			}
		}
	}
	
	public function incGrowTree() {
		
		if(treeThirst < 1 && treeSunlight < 1 && treeGrowthPhase < 6){
			treeGrowthPhase++;
			growTree();
		}
	
	}
	
	public function growTree() {
		
		switch(treeGrowthPhase) {
			
			case 0:
				tree.animation.play("0");
			case 1:
				tree.animation.play("1");
				treeGrowthSound = FlxG.sound.load(AssetPaths.growingTreeA__wav);
				treeGrowthSound.play();
			case 2:
				tree.animation.play("2");
				treeGrowthSound = FlxG.sound.load(AssetPaths.growingTree__wav, .5);
				treeGrowthSound.play();
			case 3:
				tree.animation.play("3");
				treeGrowthSound = FlxG.sound.load(AssetPaths.growingTreeB__wav, .5);
				treeGrowthSound.play();
			case 4:
				tree.animation.play("4");
				treeGrowthSound = FlxG.sound.load(AssetPaths.growingTreeC__wav, .5);
				treeGrowthSound.play();
			case 5:
				tree.animation.play("5");
				treeGrowthSound = FlxG.sound.load(AssetPaths.growingTreeA__wav, .5);
				treeGrowthSound.play();
			case 6:
				tree.animation.play("6");
				treeGrowthSound = FlxG.sound.load(AssetPaths.growingTreeD__wav, .2);
				treeGrowthSound.play();
			case 7:
				tree.animation.play("7");
			case 8:
				tree.animation.play("8");
			case 9:
				tree.animation.play("9");
				
		}
		
	}
	
	public function positionAlerts() {
		
		droplet.x = getTreeX() + 9;
		droplet.y = getTreeY() + 80;
		dropletA.x = getTreeX() + 9;
		dropletA.y = getTreeY() + 80;
		dropletB.x = getTreeX() + 9;
		dropletB.y = getTreeY() + 80;
		sunA.x = getTreeX() + 32;
		sunA.y = getTreeY() + 80;
		sunB.x = getTreeX() + 32;
		sunB.y = getTreeY() + 80;
		sunC.x = getTreeX() + 32;
		sunC.y = getTreeY() + 80;
		treeGround.x = getTreeX() - 20;
		treeGround.y = getTreeY() + 75;
		sprinklerSprite.x = getTreeX() - 15;
		sprinklerSprite.y = getTreeY() + 20;
		sprinklerButtonSprite.x = getTreeX() - 35;
		sprinklerButtonSprite.y = getTreeY() + 35;
	}
	
	private function collideGroundCheck() {
		for (i in 0...appleArray.length) {
			if (appleArray[i] != null) {
				if (FlxCollision.pixelPerfectCheck(appleArray[i].getAppleSprite(), treeGround)) {
					appleArray[i].stopFreeFall();
					appleArray[i].setDropRate(0);
				}
			}
		}
	}
	
	private function sprinklerAnimation() {
		if (treeGrowthPhase == 6) {
			sprinklerButtonSprite.alpha += .1;
		}
		if (sprinklerSystemOn) {
			sprinklerOutTime -= FlxG.elapsed;
			treeThirst = 0;
			warnThirst(false);
			if (!oldSunTimerChanged) {
				oldTreeSunLightTimer = sunlightAlertTime.timeLeft;
				oldSunTimerChanged = true;
			}
			treeSunLightTimer = 20;
			if(extendedTime){
				sunlightAlertTime.start(treeSunLightTimer, setTreeNoLight, 0);
				extendedTime = false;
			}
			if (sprinklerOutPhase) {
				sprinklerSound = FlxG.sound.load(AssetPaths.sprinklerSound__wav);
				sprinklerSprite.alpha = 1;
				sprinklerSprite.animation.play("sprinklerOut");
				sprinklerOutPhase = false;
			}
			if (sprinklerOutTime < 0) {
				sprinklerSprite.animation.play("sprinklerSpray");
				sprinklerSound.play();
				sprinklerOutTime = -1;
			}
		}
		else {
			if (!sprinklerOutPhase) {
				sprinklerOutTime = 2;
				sprinklerSprite.animation.play("sprinklerIn");
				sprinklerSound.stop();
				sprinklerSound = FlxDestroyUtil.destroy(sprinklerSound);
				sprinklerOutPhase = true;
				oldSunTimerChanged = false;
				treeSunLightTimer = 10;
				sunlightAlertTime.start(oldTreeSunLightTimer, setTreeNoLight, 0);
				extendedTime = true;
				if(sprinklerInPhase){
					sprinklerSprite.animation.play("sprinklerIn");
					sprinklerButtonSprite.animation.play("sprinklerOn");
					sprinklerInPhase = false;
				}
			}
		}
		if (goldCount <= 0) {
			sprinklerButtonSprite.animation.play("sprinklerSwitchOff");
		}
	}
	
	public function currentGoldCount(gold:Int) {
		goldCount = gold;
	}
	
	public function payWaterUtility() {
		if(sprinklerSystemOn){
			waterUtlilityTimer-= FlxG.elapsed;
			if (waterUtlilityTimer < 0) {
				minusOneSprite.x = getTreeX() + 18;
				minusOneSprite.y = getTreeY() + 15;
				minusOneSprite.alpha = 1;
				FlxTween.tween(minusOneSprite, { alpha:0, y:minusOneSprite.y - 16 }, 1);
				payWaterBill = true;
				waterUtlilityTimer = 30;
			}
		}
	}
	
	private function destroySounds() {
		if (treeGrowthSound != null && !treeGrowthSound.playing) {
			treeGrowthSound = FlxDestroyUtil.destroy(treeGrowthSound);
			//trace("destroying treegrowthsound");
		}
		if (sprinklerButtonSound != null && !sprinklerButtonSound.playing) {
			sprinklerButtonSound = FlxDestroyUtil.destroy(sprinklerButtonSound);
			//trace("destroying sprinklerButtonSound");
		}
		if (treeGrowthSound != null && !treeGrowthSound.playing) {
			treeGrowthSound = FlxDestroyUtil.destroy(treeGrowthSound);
			//trace("destroying treeGrowthSound");
		}
	}
	
	private function greenHouseEffect() {
		if (Greenhouse.greenHouseInPlay) {
			treeSunlight = 0;
		}
	}
	
	private function removeFallingAppleEvent() {
		for (i in 0...appleArray.length) {
			if (appleArray[i] != null) {
				if (appleArray[i].getAppleFreeFall()) {
					MouseEventManager.remove(appleArray[i].getAppleSprite());
				}
				else {
					if(appleArray[i].getAppleOnGround()){
						MouseEventManager.add(appleArray[i].getAppleSprite(), pickApple, null, null, null);
					}
				}
			}
		}
	}
	
	override public function draw():Void {
		
		super.draw();
		
	}
	
	override public function kill() {
		kill;
		super.kill();
	}
	
	override public function update():Void {
		removeFallingAppleEvent();
		payWaterUtility();
		sprinklerAnimation();
		greenHouseEffect();
		checkMouseDone();
		carryApple();
		checkForMouseCollisions();
		killDeadApple();
		collideGroundCheck();
		appleIncubator();
		treeSunlightCheck();
		treeThirstCheck();
		mouseAttack();
		destroySounds();
		alertPause();
		super.update();
		
	}
	
	override public function destroy():Void {
		//sunlightAlertTime.cancel();
		unregisterTreeClickedMouseEvent();
		//unregisterAppleEvent();
		unregisterAlertEvents();
		sprinklerSound = FlxDestroyUtil.destroy(sprinklerSound);
		treeGroup = null;
		appleArray = null;
		dropAlertTime = null;
		sunlightAlertTime = null;
		boardObj = null;
		treeGround = null;
		tree = null;
		droplet = null;
		dropletA = null;
		dropletB = null;
		sunA = null;
		sunB = null;
		sunC = null;
		sprinklerSprite = null;
		minusOneSprite = null;
		basketSprite = null;
		mouse = null;
		sprinklerButtonSprite = null;
		myTile = null;
		sprinklerSound = null;
		treeGrowthSound = null;
		super.destroy();
	}
	
}