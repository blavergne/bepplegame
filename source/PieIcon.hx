package ;

import flixel.FlxSprite;
import flixel.plugin.MouseEventManager;
import flixel.FlxG;

/**
 * ...
 * @author BrioLavone
 */
class PieIcon extends FlxSprite
{

	private var dragging:Bool = false;
	private var dragType:Bool;
	private var isMousedOver:Bool = false;
	private var homeX:Int = -79;
	private var homeY:Int = 78;
	
	public function new(X:Float=0, Y:Float=0, dragType:Bool) 
	{
		super(X, Y);
		this.dragType = dragType;
		loadGraphic(AssetPaths.pieIcon__png, false, 24, 13);
		this.x = homeX;
		this.y = homeY;
		if (dragType) {
			homeX = 86;
			homeY = 78;
		}
		MouseEventManager.add(this, onDown, onUp, null, null);
		
	}
	
	private function onDown(sprite:FlxSprite) {
		
		dragging = true;
		
	}

	private function onUp(sprite:FlxSprite) {
		
		dragging = false;
		resetSeedPos();
		
	}
	
	private function dragSeed(temp) {
		
		if (temp) {
			
			this.x = FlxG.mouse.x - 12;
			this.y = FlxG.mouse.y - 8;
			
		}
			
	}
	
	public function resetSeedPos() {
		if(dragType){
			this.x = homeX;
			this.y = homeY;
		}
	}
	
	public function getDragging() {
		
		return dragging;
		
	}
	
	public function setDragging(val:Bool) {
		
		dragging = val;
		
	}
	
	public function checkMouseState() {
		
		if (FlxG.mouse.justReleased) {
			
			dragging = false;
			resetSeedPos();
			
		}
		
	}
	
	public function unregisterPieEvent() {
		MouseEventManager.remove(this);
	}
	
	public function registerPieEvent() {
		MouseEventManager.add(this, onDown, onUp, null, null);
	}
	
	override public function update():Void {
		
		checkMouseState();
		dragSeed(dragging);
		super.update();
		
	}
	
	override public function destroy():Void {
		
		MouseEventManager.remove(this);
		super.destroy();
		
	}
	
}