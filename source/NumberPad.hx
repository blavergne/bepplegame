package ;

import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.system.FlxSound;
import flixel.util.FlxSort;
import flixel.plugin.MouseEventManager;
import flixel.FlxG;

/**
 * ...
 * @author ...
 */
class NumberPad extends FlxGroup
{

	private var numPadGroup:FlxGroup = new FlxGroup();
	private var numberPad:FlxSprite;
	private var numZero:FlxSprite;
	private var numOne:FlxSprite;
	private var numTwo:FlxSprite;
	private var numThree:FlxSprite;
	private var numFour:FlxSprite;
	private var numFive:FlxSprite;
	private var numSix:FlxSprite;
	private var numSeven:FlxSprite;
	private var numEight:FlxSprite;
	private var numNine:FlxSprite;
	private var answerButt:FlxSprite;
	private var clearButt:FlxSprite;
	private var answerDigits:String = "";
	private var answerButtonPressed:Bool = false;
	private var padScrollingIn:Bool = false;
	private var padPressSound:FlxSound;
	
	
	public function new() 
	{
		
		super();
		init();
		registerEvent();
		positionPieces();
		numPadSetVisible();
		
	}
	
	public function getPlayerAnswerString() {
		
		return answerDigits;
		
	}
	
	public function getAnswerButtonPressed() {
		
		return answerButtonPressed;
		
	}
	
	public function getPadScrollingIn() {
		return padScrollingIn;
	}
	
	public function setAnswerButtonPressed(val:Bool) {
		
		answerButtonPressed = val;
		
	}
	
	public function setPadScrollingIn(val:Bool) {
		
		padScrollingIn = val;
		
	}
	
	public function resetAnswerDigits() {
		
		answerDigits = "";
		
	}
	
	private function init() {
		
		numberPad = new FlxSprite(); 
		numZero = new FlxSprite(); 
		numOne = new FlxSprite();
		numTwo = new FlxSprite();
		numThree = new FlxSprite();
		numFour = new FlxSprite();
		numFive = new FlxSprite();
		numSix = new FlxSprite();
		numSeven = new FlxSprite();
		numEight = new FlxSprite();
		numNine = new FlxSprite();
		answerButt = new FlxSprite();
		clearButt = new FlxSprite();
		numberPad.loadGraphic(AssetPaths.calcImage__png, false);
		numZero.loadGraphic(AssetPaths.buttonZero__png, false, 0, 0, false, "zero");
		numOne.loadGraphic(AssetPaths.buttonOne__png, false);
		numTwo.loadGraphic(AssetPaths.buttonTwo__png, false);
		numThree.loadGraphic(AssetPaths.buttonThree__png, false);
		numFour.loadGraphic(AssetPaths.buttonFour__png, false);
		numFive.loadGraphic(AssetPaths.buttonFive__png, false);
		numSix.loadGraphic(AssetPaths.buttonSix__png, false);
		numSeven.loadGraphic(AssetPaths.buttonSeven__png, false);
		numEight.loadGraphic(AssetPaths.buttonEight__png, false);
		numNine.loadGraphic(AssetPaths.buttonNine__png, false);
		answerButt.loadGraphic(AssetPaths.answerButton__png, false);
		clearButt.loadGraphic(AssetPaths.buttonClear__png, false);
		numberPad.x = 223;
		numberPad.y = 520;//120
		numPadGroup.add(numberPad);
		numPadGroup.add(numZero);
		numPadGroup.add(numOne);
		numPadGroup.add(numTwo);
		numPadGroup.add(numThree);
		numPadGroup.add(numFour);
		numPadGroup.add(numFive);
		numPadGroup.add(numSix);
		numPadGroup.add(numSeven);
		numPadGroup.add(numEight);
		numPadGroup.add(numNine);
		numPadGroup.add(answerButt);
		numPadGroup.add(clearButt);
		add(numPadGroup);
		
	}
	
	public function getPadY() {
		
		return numberPad.y;
		
	}
	//register number tokens to mouse event.
	public function registerEvent() {
		
		MouseEventManager.add(clearButt, padPress, null, null, null);
		MouseEventManager.add(answerButt, padPress, null, null, null);
		MouseEventManager.add(numZero, padPress, null, null, null);
		MouseEventManager.add(numOne, padPress, null, null, null);
		MouseEventManager.add(numTwo, padPress, null, null, null);
		MouseEventManager.add(numThree, padPress, null, null, null);
		MouseEventManager.add(numFour, padPress, null, null, null);
		MouseEventManager.add(numFive, padPress, null, null, null);
		MouseEventManager.add(numSix, padPress, null, null, null);
		MouseEventManager.add(numSeven, padPress, null, null, null);
		MouseEventManager.add(numEight, padPress, null, null, null);
		MouseEventManager.add(numNine, padPress, null, null, null);
		
	}
	
	public function unregisterNumPadEvents() {
		MouseEventManager.remove(clearButt);
		MouseEventManager.remove(answerButt);
		MouseEventManager.remove(numZero);
		MouseEventManager.remove(numOne);
		MouseEventManager.remove(numTwo);
		MouseEventManager.remove(numThree);
		MouseEventManager.remove(numFour);
		MouseEventManager.remove(numFive);
		MouseEventManager.remove(numSix);
		MouseEventManager.remove(numSeven);
		MouseEventManager.remove(numEight);
		MouseEventManager.remove(numNine);
	}
	
	private function positionPieces() {
		
		answerButt.x = numberPad.x + 129;
		answerButt.y = numberPad.y + 18;
		clearButt.x = numberPad.x + 135;
		clearButt.y = numberPad.y + 230;
		
		numOne.x = numberPad.x + 8;
		numOne.y = numberPad.y + 50;//50
		numTwo.x = numberPad.x + 72;
		numTwo.y = numberPad.y + 50;
		numThree.x = numberPad.x + 135;
		numThree.y = numberPad.y + 50;
		
		numFour.x = numberPad.x + 8;
		numFour.y = numberPad.y + 110;//110
		numFive.x = numberPad.x + 72;
		numFive.y = numberPad.y + 110;
		numSix.x = numberPad.x + 135;
		numSix.y = numberPad.y + 110;
		
		numSeven.x = numberPad.x + 8;
		numSeven.y = numberPad.y + 170;//170
		numEight.x = numberPad.x + 72;
		numEight.y = numberPad.y + 170;
		numNine.x = numberPad.x + 135;
		numNine.y = numberPad.y + 170;
		
		numZero.x = numberPad.x + 72;
		numZero.y = numberPad.y + 230;//230
		
	}
	
	private function numPadSetVisible() {
		
		numberPad.alpha = .5;
		numZero.alpha = .7;
		numOne.alpha = .7;
		numTwo.alpha = .7;
		numThree.alpha = .7;
		numFour.alpha = .7;
		numFive.alpha = .7;
		numSix.alpha = .7;
		numSeven.alpha = .7;
		numEight.alpha = .7;
		numNine.alpha = .7;

	}
	
	private function scrolling() {
			
		switch(padScrollingIn){
				
			case true:
				if(numberPad.y >= 120){
					numberPad.y -= 10;
				}
				if (numberPad.y < 120) {
					numberPad.y = 120;
				}
			case false:
				if(numberPad.y <= 520){
					numberPad.y += 12;
				}
				if (numberPad.y > 520) {
					numberPad.y = 520;
				}
		}
	}
	
	private function padPress(currentButton:FlxSprite) {

		if (answerDigits.length <= 4) {
			
			if (currentButton == numZero) {
				padPressSound = FlxG.sound.load(AssetPaths.calcButtonSound__wav);
				padPressSound.play();
				answerDigits += "0";
				
			}
			if (currentButton == numOne) {
				padPressSound = FlxG.sound.load(AssetPaths.calcButtonSound__wav);
				padPressSound.play();
				answerDigits += "1";
				
			}
			if (currentButton == numTwo) {
				padPressSound = FlxG.sound.load(AssetPaths.calcButtonSound__wav);
				padPressSound.play();
				answerDigits += "2";
				
			}
			if (currentButton == numThree) {
				padPressSound = FlxG.sound.load(AssetPaths.calcButtonSound__wav);
				padPressSound.play();
				answerDigits += "3";
				
			}
			if (currentButton == numFour) {
				padPressSound = FlxG.sound.load(AssetPaths.calcButtonSound__wav);
				padPressSound.play();
				answerDigits += "4";
				
			}
			if (currentButton == numFive) {
				padPressSound = FlxG.sound.load(AssetPaths.calcButtonSound__wav);
				padPressSound.play();
				answerDigits += "5";	
				
			}
			if (currentButton == numSix) {
				padPressSound = FlxG.sound.load(AssetPaths.calcButtonSound__wav);
				padPressSound.play();
				answerDigits += "6";
				
			}
			if (currentButton == numSeven) {
				padPressSound = FlxG.sound.load(AssetPaths.calcButtonSound__wav);
				padPressSound.play();
				answerDigits += "7";	
				
			}
			if (currentButton == numEight) {
				padPressSound = FlxG.sound.load(AssetPaths.calcButtonSound__wav);
				padPressSound.play();
				answerDigits += "8";
				
			}
			if (currentButton == numNine) {
				padPressSound = FlxG.sound.load(AssetPaths.calcButtonSound__wav);
				padPressSound.play();
				answerDigits += "9";
				
			}	
		}
		
		if (currentButton == answerButt) {
			answerButtonPressed = true;
			
		}
		if (currentButton == clearButt) {
			padPressSound = FlxG.sound.load(AssetPaths.calcButtonSound__wav);
			padPressSound.play();
			answerDigits = "";
			
		}
		
	}
	
	override public function draw() {
		
		super.draw();
		
	}
	
	override public function update() {
		
		scrolling();
		positionPieces();
		padPress(null);
		super.update();
		
	}
	
	override public function destroy() {
		numberPad = null; 
		numZero = null; 
		numOne = null;
		numTwo = null;
		numThree = null;
		numFour = null;
		numFive = null;
		numSix = null;
		numSeven = null;
		numEight = null;
		numNine = null;
		answerButt = null;
		clearButt = null;
		super.destroy();
	}
	
}