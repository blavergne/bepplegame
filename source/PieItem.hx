package ;

import flixel.FlxSprite;

/**
 * ...
 * @author ...
 */
class PieItem extends MarketItem
{

	public function new() 
	{
		super();
		initMarketItems();
	}
	
	private function initMarketItems() {
		itemSprite = new FlxSprite();
		itemCost = 20;
		itemName = "pieItem";
		itemCount = 1;
		itemSprite.loadGraphic(AssetPaths.pieMachineItem__png, true, 30, 30, false, "pieItem");
		itemSprite.animation.add("available", [0], 1, false);
		itemSprite.animation.add("notAvailable", [1], 1, false);
		setItemAvailable(true);
	}
	
	public function setPieItemCount(val:Int) {
		itemCount = val;
	}
	
	override public function update() {
		setAvailableAnimation();
		super.update();
	}
	
}