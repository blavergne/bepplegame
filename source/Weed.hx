package ;
import flixel.group.FlxGroup;
import flixel.FlxSprite;
import flixel.util.FlxRandom;
/**
 * ...
 * @author BrioLavone
 */
class Weed extends FlxGroup
{

	private var weedArray:Array<FlxSprite>;
	private var weedGroup:FlxGroup;
	private var weedA:FlxSprite;
	private var weedB:FlxSprite;
	private var chosenWeed:FlxSprite;
	
	public function new(weedIndex:Int) 
	{
		
		super();
		init(weedIndex);
		
	}
	
	private function init(weedIndex:Int) {
		var weedArrayIndex:Int;
		weedArray = new Array<FlxSprite>();
		weedGroup = new FlxGroup();
		weedA = new FlxSprite();
		weedB = new FlxSprite();
		chosenWeed = new FlxSprite();
		weedA.loadGraphic(AssetPaths.weedA__png, false);
		weedB.loadGraphic(AssetPaths.weedB__png, false);
		weedArray[0] = weedA;
		weedArray[1] = weedB;
		chosenWeed = weedArray[weedIndex];
		weedGroup.add(chosenWeed);
		add(weedGroup);
	}
	
	public function getChosenWeed() {
		return chosenWeed;
	}
	
}