package ;

import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.FlxG;
import flixel.system.FlxSound;

/**
 * ...
 * @author ...
 */
class SunToken extends FlxGroup
{

	private var sunGroup:FlxGroup = new FlxGroup();
	private var sun:FlxSprite;
	private var sunSound:FlxSound;
	var sunDone:Bool = false;
	var destroySun:Bool = false;
	
	public function new() 
	{
		
		super();
		sun = new FlxSprite();
		sun.loadGraphic(AssetPaths.sunTokenAnimation__png, true, 64, 64);
		sun.animation.add("sun", [0, 1, 2, 3, 4, 5, 6, 7], 10, true);
		sun.animation.play("sun");
		sunSound = FlxG.sound.load(AssetPaths.sunshine__wav);
		sunSound.play();
		sun.alpha = 0.0;
		sun.scale.x = .6;
		sun.scale.y = .6;
		sunGroup.add(sun);
		add(sunGroup);
		
	}
	
	public function getSunAlpha() {
		
		return sun.alpha;
		
	}
	
	public function setSunX(xVal:Float) {
		
		sun.x = xVal;
		
	}
	public function setSunY(yVal:Float) {
		
		sun.y = yVal;
		
	}
	
	private function fadeSun() {
		
		if (sun.alpha < 1 && !sunDone) {
			
			sun.alpha += .01;
			
		}
		else {
			
			sunDone = true;
			sun.alpha -= .005;	
			
			if (sun.alpha <= 0) {
				destroySun = true;
				destroy();
			}
			
		}
		
	}
	
	override public function update() {
		
		fadeSun();
		
		if(!destroySun){
			super.update();
		}
	}
	
	override public function destroy() {
		sunGroup = null;
		sun = null;
		sunSound = null;
		destroy;
		super.destroy();
		
	}
	
}