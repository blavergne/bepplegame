package ;

import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.plugin.MouseEventManager;
import flixel.FlxG;
import flixel.system.FlxSound;
import flixel.tweens.FlxTween;
import flixel.util.FlxDestroyUtil;

/**
 * ...
 * @author ...
 */
class JuiceMachine extends FlxGroup
{
	private var juiceMachineGroup:FlxGroup;
	private var juiceMachineSprite:FlxSprite;
	private var juiceMachineAlertSprite:FlxSprite;
	private var juiceMachineBottleAnim:FlxSprite;
	private var juiceMachineFlyingBottle:FlxSprite;
	private var minusOneSprite:FlxSprite;
	private var repairAlertSprite:FlxSprite;
	private var fillingJuiceBottleSound:FlxSound;
	private var juiceMachineSound:FlxSound;
	private var myRelativeX:Float;
	private var myRelativeY:Float;
	private var rise:Float;
	private var run:Float;
	private var electricUtilityTimer:Float = 15;
	private var machineReliabilityTimer:Float = 20;
	private var goldCount:Int;
	private var appleCount:Int;
	private var payElectricBill:Bool = false;
	private var machineOn:Bool = false;
	private var machineAlertReset:Bool = false;
	private var machineClicked:Bool = false;
	private var fillingBottle:Bool = false;
	private var repairClicked:Bool = false;
	private var animateRepairSprite:Bool = false;
	private var bottleFlying:Bool = false;
	private var inJuicePack:Bool = false;
	private var myTile:Tile;
	private var mathless:Bool = MenuState.mathless;
	
	public function new(tileSprite:Tile) 
	{
		super();
		juiceMachineGroup = new FlxGroup();
		juiceMachineSprite = new FlxSprite();
		juiceMachineBottleAnim = new FlxSprite();
		juiceMachineAlertSprite = new FlxSprite();
		juiceMachineFlyingBottle = new FlxSprite(); 
		minusOneSprite = new FlxSprite();
		repairAlertSprite = new FlxSprite();
		myTile = tileSprite;
		minusOneSprite.loadGraphic(AssetPaths.minusOneImage__png, false);
		minusOneSprite.alpha = 0;
		juiceMachineSprite.loadGraphic(AssetPaths.juiceMachineAnimation__png, true, 86, 84);
		juiceMachineSprite.animation.add("machineOn", [1, 2, 3, 4, 5], 5, true);
		juiceMachineSprite.animation.add("machineOff", [0], 1, false);
		juiceMachineSprite.animation.play("machineOff");
		juiceMachineAlertSprite.loadGraphic(AssetPaths.juiceMachineSwitchAnimation__png, true, 32, 18);
		juiceMachineAlertSprite.animation.add("switchOn", [1], 1, false);
		juiceMachineAlertSprite.animation.add("switchOff", [0], 1, false);
		juiceMachineAlertSprite.animation.play("switchOn");
		juiceMachineBottleAnim.loadGraphic(AssetPaths.fillingBottleAnimation__png, true, 10, 20);
		juiceMachineBottleAnim.animation.add("fillingBottle", [0, 1, 2, 3, 4, 5], 1, true);
		juiceMachineBottleAnim.alpha = 0;
		juiceMachineFlyingBottle.loadGraphic(AssetPaths.juiceMachineIcon__png);
		repairAlertSprite.loadGraphic(AssetPaths.repairAnimationS__png, true, 16, 16);
		repairAlertSprite.animation.add("repairMe", [0, 1, 2, 3, 4], 10, true);
		repairAlertSprite.alpha = 0;
		juiceMachineFlyingBottle.alpha = 0;
		juiceMachineFlyingBottle.x = tileSprite.x + 90;
		juiceMachineFlyingBottle.y = tileSprite.y + 5;
		juiceMachineBottleAnim.x = tileSprite.x + 64;
		juiceMachineBottleAnim.y = tileSprite.y + 50;
		juiceMachineSprite.x = tileSprite.x + 57;
		juiceMachineSprite.y = tileSprite.y - 15;
		juiceMachineAlertSprite.x = tileSprite.x + 78;
		juiceMachineAlertSprite.y = tileSprite.y + 3;
		repairAlertSprite.x = tileSprite.x + 96;
		repairAlertSprite.y = tileSprite.y + 67;
		registerMachineEvents();
		juiceMachineGroup.add(juiceMachineSprite);
		juiceMachineGroup.add(juiceMachineAlertSprite);
		juiceMachineGroup.add(juiceMachineBottleAnim);
		juiceMachineGroup.add(minusOneSprite);
		//juiceMachineGroup.add(juiceMachineFlyingBottle);
		juiceMachineGroup.add(repairAlertSprite);
		add(juiceMachineGroup);
		
	}
	
	public function getMyTile() {
		return myTile;
	}
	
	public function getInJuicePack() {
		return inJuicePack;
	}
	
	public function getJuiceMachineFlyingBotte() {
		return juiceMachineFlyingBottle;
	}
	
	public function getJuiceMachineSprite() {
		return juiceMachineSprite;
	}

	public function getMyRelativeX() {
		return myRelativeX;
	}
	
	public function getMyRelativeY() {
		return myRelativeY;
	}
	
	public function getMachineOn() {
		return machineOn;
	}
	
	public function getFillingBottle() {
		return fillingBottle;
	}
	
	public function getPayElectricBill() {
		return payElectricBill;
	}
	
	public function getRepairClicked() {
		return repairClicked;
	}
	
	public function getAnimateRepairSprite() {
		return animateRepairSprite;
	}
	
	public function getBottleFlying() {
		return bottleFlying;
	}
	
	//set members functions
	public function setAnimateRepairSprite(val:Bool) {
		animateRepairSprite = val;
	}
	
	public function setInJuicePack(val:Bool) {
		inJuicePack = val;
	}
	
	public function setRepairClicked(val:Bool) {
		repairClicked = val;
	}
	
	public function setPayElectricBill(val:Bool) {
		payElectricBill = val;
	}
	
	public function setFillingBottle(val:Bool) {
		fillingBottle = val;
	}
	
	public function setMachineOn(val:Bool) {
		machineOn = val;
	}
	
	public function setMyRelativeX(val:Float) {
		myRelativeX = val;
	}
	
	public function setMyRelativeY(val:Float) {
		myRelativeY = val;
	}
	
	public function setGoldCount(val:Int) {
		goldCount = val;
	}
	
	public function setAppleCount(val:Int) {
		appleCount = val;
	}
	
	public function setBottleFlying(val:Bool) {
		bottleFlying = val;
	}
	
	//member functions
	public function registerMachineEvents() {
		MouseEventManager.add(juiceMachineAlertSprite, turnMachineOn, null, null, null);
		MouseEventManager.add(repairAlertSprite, repairAlertClicked, null, null, null);
	}
	
	public function registerMachineButtonEvent() {
		MouseEventManager.add(juiceMachineAlertSprite, turnMachineOn, null, null, null);
	}
	
	public function unregisterMachineEvents() {
		MouseEventManager.remove(juiceMachineAlertSprite);
		MouseEventManager.remove(repairAlertSprite);
	}
	
	private function repairAlertClicked(sprite:FlxSprite) {
		repairClicked = true;
	}
	
	public function turnMachineOn(sprite:FlxSprite) {
		
		if (!machineOn) {
			fillingBottle = true;
			juiceMachineSound = FlxG.sound.load(AssetPaths.juiceMachineSound__wav, .2, true);
			juiceMachineSound.play();
			fillingJuiceBottleSound = FlxG.sound.load(AssetPaths.fillingJuiceBottleSound__wav, 1, true);
			fillingJuiceBottleSound.play();
			juiceMachineBottleAnim.animation.play("fillingBottle");
			juiceMachineAlertSprite.animation.play("switchOff");
			juiceMachineSprite.animation.play("machineOn");
			machineOn = true;
		}
		else {
			fillingBottle = false;
			juiceMachineAlertSprite.animation.play("switchOn");
			juiceMachineSprite.animation.play("machineOff");
			if (juiceMachineSound != null && fillingJuiceBottleSound != null) {
				juiceMachineSound.stop();
				fillingJuiceBottleSound.stop();
				juiceMachineSound = FlxDestroyUtil.destroy(juiceMachineSound);
				fillingJuiceBottleSound = FlxDestroyUtil.destroy(fillingJuiceBottleSound);
				//trace("destroying machine sounds");
			}
			machineOn = false;
		}
	}
	
	private function bottleFadeIn() {
		if (fillingBottle) {
			juiceMachineBottleAnim.alpha += .02;
		}
		else {
			juiceMachineBottleAnim.alpha -= .02;
		}
	 }
	
	public function payElectricUtility() {
		if(machineOn){
			electricUtilityTimer -= FlxG.elapsed;
			if (electricUtilityTimer < 0) {
				minusOneSprite.x = juiceMachineSprite.x + 30;
				minusOneSprite.y = juiceMachineSprite.y + 5;
				minusOneSprite.alpha = 1;
				FlxTween.tween(minusOneSprite, { alpha:0, y:minusOneSprite.y - 16 }, 1);
				payElectricBill = true;
				electricUtilityTimer = 15;
			}
		}
	}
	
	private function machineReliability() {
		if(machineOn){
			machineReliabilityTimer -= FlxG.elapsed;
			if (machineReliabilityTimer < 0) {
				animateRepairSprite = true;
			}
		}
	}
	
	public function resetRepairAlertTimer() {
		machineReliabilityTimer = 20;
	}
	
	private function repairSpriteAnimation() {
		if (animateRepairSprite) {
			repairAlertSprite.alpha += .01;
			repairAlertSprite.animation.play("repairMe");
			machineReliabilityTimer = -1;
			MouseEventManager.remove(juiceMachineAlertSprite);
			machineOn = true;
			turnMachineOn(null);
		}
		else {
			repairAlertSprite.alpha -= .1;
		}
	}
	
	public function setRiseRun() {
		run = -(JuicePackIcon.JuicePackPosX + 20 - juiceMachineFlyingBottle.x);
		rise = -(JuicePackIcon.JuicePackPosY + 20 - juiceMachineFlyingBottle.y);
	}
	
	public function sendBottleToPack() {
		if (bottleFlying) {
			juiceMachineFlyingBottle.alpha = 1;
			if (juiceMachineFlyingBottle.y >= JuicePackIcon.JuicePackPosY + 20) {
				juiceMachineFlyingBottle.y -= rise/25;
			}
			if (juiceMachineFlyingBottle.x >= JuicePackIcon.JuicePackPosX + 20) {
				juiceMachineFlyingBottle.x -= run/25;
			}
			if (juiceMachineFlyingBottle.x <= JuicePackIcon.JuicePackPosX + 22 && juiceMachineFlyingBottle.y <= JuicePackIcon.JuicePackPosY + 22) {
				inJuicePack = true;
				bottleFlying = false;
				juiceMachineFlyingBottle.alpha = 0;
				juiceMachineFlyingBottle.x = myTile.x + 90;
				juiceMachineFlyingBottle.y = myTile.y + 5;
			}
		}
	}
	
	override public function update() {
		machineReliability();
		repairSpriteAnimation();
		payElectricUtility();
		bottleFadeIn();
		sendBottleToPack();
		super.update();
	}
	
	override public function destroy() {
		destroy;
		super.destroy();
	}
	
}