package ;

import flixel.FlxSprite;
import flixel.group.FlxGroup;

/**
 * ...
 * @author ...
 */
class PiePackIcon extends FlxGroup
{
	private var piePackGroup:FlxGroup;
	private var piePackSprite:FlxSprite;
	public static var piePackPosX:Float;
	public static var piePackPosY:Float;
	
	public function new() 
	{
		super();
		piePackGroup = new FlxGroup();
		piePackSprite = new FlxSprite();
		piePackSprite.loadGraphic(AssetPaths.packOfPiesIcon__png, false);
		piePackSprite.x = 207;
		piePackSprite.y = 36;
		piePackPosX = piePackSprite.x;
		piePackPosY = piePackSprite.y;
		piePackGroup.add(piePackSprite);
		add(piePackGroup);
	}
	
	public function getPiePackSprite() {
		return piePackSprite;
	}
	
}