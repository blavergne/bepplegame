package ;

import flixel.FlxSprite;
import flixel.group.FlxGroup;

/**
 * ...
 * @author ...
 */
class GoldCoin extends FlxGroup
{
	
	private var coinGroup:FlxGroup;
	private var coin:FlxSprite;
	
	public function new() 
	{
		super();
		coinGroup = new FlxGroup();
		coin = new FlxSprite();
		coin.loadGraphic(AssetPaths.goldCoin__png, false);
		coin.x = 20;
		coin.y = 24;
		coinGroup.add(coin);
		add(coinGroup);
	}
	
	public function getCoinSprite() {
		return coin;
	}
	
	override public function destroy() {
		coin = null;
		coinGroup = null;
		super.destroy();
	}
	
}