package ;

import flixel.FlxSprite;

/**
 * ...
 * @author ...
 */
class JuiceItem extends MarketItem
{

	public function new() 
	{
		super();
		initMarketItems();
	}
	
	private function initMarketItems() {
		itemSprite = new FlxSprite();
		itemCost = 10;
		itemName = "juiceItem";
		itemCount = 1;
		itemSprite.loadGraphic(AssetPaths.appleJuiceItemImage__png, true, 30, 30, false, "juiceItem");
		itemSprite.animation.add("available", [0], 1, false);
		itemSprite.animation.add("notAvailable", [1], 1, false);
		setItemAvailable(true);
	}
	
	public function setJuiceItemCount(val:Int) {
		itemCount = val;
	}
	
	override public function update() {
		setAvailableAnimation();
		super.update();
	}
	
}