package ;

import flixel.FlxSprite;
import flixel.plugin.MouseEventManager;
import flixel.FlxG;

/**
 * ...
 * @author BrioLavone
 */
class JuiceIcon extends FlxSprite
{

	private var dragging:Bool = false;
	private var dragType:Bool;
	private var isMousedOver:Bool = false;
	private var homeX:Int = -46;
	private var homeY:Int = 70;
	
	public function new(X:Int, Y:Int, dragType:Bool) 
	{
		super(X, Y);
		this.dragType = dragType;
		loadGraphic(AssetPaths.juiceMachineIcon__png, false, 10, 20);
		this.x = homeX;
		this.y = homeY;
		if (dragType) {
			homeX = 119;
			homeY = 70; 
		}
		MouseEventManager.add(this, onDown, onUp, null, null);
		
	}
	
	private function onDown(sprite:FlxSprite) {
		
		dragging = true;
		
	}

	private function onUp(sprite:FlxSprite) {
		
		dragging = false;
		resetSeedPos();
		
	}

	private function dragSeed(temp) {
		
		if (temp) {
			
			this.x = FlxG.mouse.x - 8;
			this.y = FlxG.mouse.y - 14;
			
		}
			
	}
	
	public function resetSeedPos() {
		if(dragType){
			this.x = homeX;
			this.y = homeY;
		}
	}
	
	public function getDragging() {
		
		return dragging;
		
	}
	
	//set functions
	public function setDragging(val:Bool) {
		
		dragging = val;
		
	}
	
	public function checkMouseState() {
		
		if (FlxG.mouse.justReleased) {
			
			dragging = false;
			resetSeedPos();
			
		}
		
	}
	
	public function unregisterJuiceEvent() {
		MouseEventManager.remove(this);
	}
	
	public function registerJuiceEvent() {
		MouseEventManager.add(this, onDown, onUp, null, null);
	}
	
	override public function update():Void {
		
		checkMouseState();
		dragSeed(dragging);
		super.update();
		
	}
	
	override public function destroy():Void {
		
		MouseEventManager.remove(this);
		super.destroy();
		
	}
	
}