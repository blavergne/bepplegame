package ;
import flixel.FlxSprite;
import flixel.text.FlxText;

/**
 * ...
 * @author ...
 */
class MarketItem extends FlxSprite
{
	private var itemSprite:FlxSprite;
	private var itemName:String;
	private var itemCost:Int;
	private var itemCount:Int;
	private var itemAvailable:Bool;
	private var itemClicked:Bool;
	private var itemCountFieldText:FlxText;
	
	public function new() 
	{
		super();
		
	}
	
	public function getItemSprite() {
		return itemSprite;
	}
	
	public function getItemCost() {
		return itemCost;
	}
	
	public function getItemName() {
		return itemName;
	}
	
	public function getItemCount() {
		return itemCount;
	}
	
	public function getItemClicked() {
		return itemClicked;
	}
	
	public function decItemCost() {
		itemCost--;
	}
	
	public function incItemCost() {
		itemCost++;
	}
	
	public function decItemCount() {
		itemCount--;
	}
	
	public function incItemCount() {
		itemCount++;
	}
	
	public function setItemAvailable(available:Bool ) {
		itemAvailable = available;
	}
	
	public function getItemAvailable() {
		return itemAvailable;
	}
	
	public function setAvailableAnimation() {
		if (itemAvailable) {
			itemSprite.animation.play("available");
		}
		else {
			itemSprite.animation.play("notAvailable");
		}
	}
}