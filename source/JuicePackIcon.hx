package ;

import flixel.FlxSprite;
import flixel.group.FlxGroup;

/**
 * ...
 * @author ...
 */
class JuicePackIcon extends FlxGroup
{
	private var juicePackGroup:FlxGroup;
	private var juicePackSprite:FlxSprite;
	public static var JuicePackPosX:Float;
	public static var JuicePackPosY:Float;
	
	public function new() 
	{
		super();
		juicePackGroup = new FlxGroup();
		juicePackSprite = new FlxSprite();
		juicePackSprite.loadGraphic(AssetPaths.packOfJuiceIcon__png, false);
		juicePackSprite.x = 150;
		juicePackSprite.y = 36;
		JuicePackPosX = juicePackSprite.x;
		JuicePackPosY = juicePackSprite.y;
		juicePackGroup.add(juicePackSprite);
		add(juicePackGroup);
	}
	
	public function getJuicePackSprite() {
		return juicePackSprite;
	}
	
}