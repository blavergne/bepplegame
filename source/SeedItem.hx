package ;

import flixel.FlxSprite;

/**
 * ...
 * @author ...
 */
class SeedItem extends MarketItem
{
	
	public function new() 
	{
		super();
		initMarketItems();
	}
	
	private function initMarketItems() {
		itemSprite = new FlxSprite();
		itemCost = 2;
		itemName = "seedItem";
		itemCount = 2;
		itemSprite.loadGraphic(AssetPaths.seedItemAnimation__png, true, 30, 30, false, "seedItem");
		itemSprite.animation.add("available", [0], 1, false);
		itemSprite.animation.add("notAvailable", [1], 1, false);
		setItemAvailable(true);
	}
	
	public function setSeedItemCount(val:Int) {
		itemCount = val;
	}
	
	override public function update() {
		setAvailableAnimation();
		super.update();
	}
	
}