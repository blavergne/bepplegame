package ;

import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.util.FlxRandom;

/**
 * ...
 * @author BrioLavone
 */
class Island extends FlxGroup
{
	
	private var islandGroup:FlxGroup;
	private var islandArray:Array<FlxSprite>;
	private var islandA:FlxSprite;
	private var islandB:FlxSprite;
	private var islandC:FlxSprite;
	private var islandD:FlxSprite;
	private var islandE:FlxSprite;
	private var islandF:FlxSprite;
	private var islandG:FlxSprite;
	private var islandH:FlxSprite;
	private var islandI:FlxSprite;
	private var islandJ:FlxSprite;
	private var islandK:FlxSprite;
	private var randomIslandIndex:Int;
	private var randomYPosition:Int; 
	private var randomIslandSpeed:Float;
	private var chosenIsland:FlxSprite;
	
	public function new() 
	{
		super();
		init();
	}
	
	private function init() {
		
		islandGroup = new FlxGroup();
		islandArray = new Array<FlxSprite>();
		islandA = new FlxSprite();
		islandB = new FlxSprite();
		islandC = new FlxSprite();
		islandD = new FlxSprite();
		islandE = new FlxSprite();
		islandF = new FlxSprite();
		islandG = new FlxSprite();
		islandH = new FlxSprite();
		islandI = new FlxSprite();
		islandJ = new FlxSprite();
		islandK = new FlxSprite();
		islandA.loadGraphic(AssetPaths.backGroundMassA__png, false);
		islandB.loadGraphic(AssetPaths.backGroundMassB__png, false);
		islandC.loadGraphic(AssetPaths.backGroundMassC__png, false);
		islandD.loadGraphic(AssetPaths.backGroundMassD__png, false);
		islandE.loadGraphic(AssetPaths.backGroundMassE__png, false);
		islandF.loadGraphic(AssetPaths.islandMouseAnimation__png, true, 80, 52);
		islandF.animation.add("spaceMouse", [0, 1, 2, 3, 4, 5, 6, 7, 8], 10, true);
		islandF.animation.play("spaceMouse");
		islandG.loadGraphic(AssetPaths.backGroundMassG__png, false);
		islandH.loadGraphic(AssetPaths.backGroundMassH__png, false);
		islandI.loadGraphic(AssetPaths.rollingMoon__png, true, 15, 15);
		islandI.animation.add("rollingMoon", [0, 1, 2, 3, 4, 5], 10, true);
		islandI.animation.play("rollingMoon");
		islandJ.loadGraphic(AssetPaths.upsideDownIsland__png, false);
		islandK.loadGraphic(AssetPaths.sidewaysIsland__png, false);
		islandArray[0] = islandA;
		islandArray[1] = islandB; 
 		islandArray[2] = islandC;
		islandArray[3] = islandD;
		islandArray[4] = islandE;
		islandArray[5] = islandF;
		islandArray[6] = islandG;
		islandArray[7] = islandH;
		islandArray[8] = islandI;
		islandArray[9] = islandJ;
		islandArray[10] = islandK;
		
		randomIslandIndex = FlxRandom.intRanged(0, 10);
		randomYPosition = FlxRandom.intRanged(0, 400);
		
		chosenIsland = islandArray[randomIslandIndex];
		chosenIsland.y = randomYPosition;
		if (chosenIsland.width > 100) {
			chosenIsland.x = -200;
		}else {
			chosenIsland.x = -80;	
		}
		
		islandGroup.add(chosenIsland);
		add(islandGroup);
	}
	
	public function getIslandSprite():FlxSprite {
		return chosenIsland;
	}
	
	private function islandSpeed():Void {
		if (chosenIsland.width <= 25) {
			chosenIsland.x += .2;
		}
		else if (chosenIsland.width <= 29 && chosenIsland.width > 25) {
			chosenIsland.x += .3;
		}
		else if (chosenIsland.width <= 60 && chosenIsland.width > 29) {
			chosenIsland.x += .6;
		}
		else if (chosenIsland.width <= 80 && chosenIsland.width > 60) {
			chosenIsland.x += .7;
		}
		else if (chosenIsland.width <= 198 && chosenIsland.width > 80) {
			chosenIsland.x += .8;
		}
		else {
			chosenIsland.x += .4;
		}
		
	}
	
	private function destroyIsland():Void {
		if (chosenIsland.x >= 700) {
			//trace("Island Destroyed");
			destroy();
			chosenIsland = null;
		}
	}
	
	override function update() {
		islandSpeed();
		destroyIsland();
		if (chosenIsland != null ) {
			super.update();
		}
	}
	
	override function destroy() {
		islandGroup = null;
		islandArray = null;
		islandA = null;
		islandB = null;
		islandB = null;
		islandC = null;
		islandD = null;
		islandE = null;
		islandF = null;
		islandG = null;
		islandH = null;
		islandI = null;
		islandJ = null;
		islandK = null;
		super.destroy();
	}
	
}