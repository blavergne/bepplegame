package ;

import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.system.FlxSound;
import flixel.FlxG;
import flixel.util.FlxDestroyUtil;

/**
 * ...
 * @author ...
 */
class Greenhouse extends FlxGroup
{

	private var greenHouseGroup:FlxGroup;
	private var greenHouseSprite:FlxSprite;
	private var greenHouseShrub0:FlxSprite;
	private var greenHouseTree:FlxSprite;
	private var windChimeSound:FlxSound;
	public static var greenHouseInPlay:Bool = false;
	
	public function new(tileSprite:Tile) 
	{
		super();
		greenHouseInPlay = true;
		greenHouseGroup = new FlxGroup();
		greenHouseSprite = new FlxSprite();
		greenHouseShrub0 = new FlxSprite();
		greenHouseTree = new FlxSprite();
		greenHouseSprite.loadGraphic(AssetPaths.greenHouseAnimation__png, true, 81, 69);
		greenHouseShrub0.loadGraphic(AssetPaths.greenHouseShrub__png, false);
		greenHouseTree.loadGraphic(AssetPaths.greenHouseTreeAnimation__png, true, 68, 83);
		greenHouseTree.animation.add("chimesAnimate", [0, 1, 2, 3], 2, true);
		greenHouseTree.animation.play("chimesAnimate");
		windChimeSound = FlxG.sound.load(AssetPaths.greenHouseWindChimeSound__wav, .1, true);
		windChimeSound.play();
		greenHouseSprite.x = tileSprite.x + 60;
		greenHouseSprite.y = tileSprite.y;
		greenHouseShrub0.x = greenHouseSprite.x + 65;
		greenHouseShrub0.y = greenHouseSprite.y + 40;
		greenHouseTree.x = greenHouseSprite.x - 35;
		greenHouseTree.y = greenHouseSprite.y - 25;
		greenHouseGroup.add(greenHouseSprite);
		greenHouseGroup.add(greenHouseTree);
		greenHouseGroup.add(greenHouseShrub0);
		add(greenHouseGroup);
		
	}
	
	public function setGreenHouseInPlay(val:Bool) {
		greenHouseInPlay = val;
	}
	
	override function update() {
		super.update();
	}
	
	override function destroy() {
		windChimeSound = FlxDestroyUtil.destroy(windChimeSound);
		greenHouseGroup = null;
		greenHouseSprite = null; 
		greenHouseTree = null;
		windChimeSound = null;
		greenHouseShrub0 = null;
		super.destroy();
	}
	
}