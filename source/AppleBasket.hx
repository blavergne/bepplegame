package ;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.text.FlxText;

/**basket class for showing filled apple basket animation and dictate basketFull number
 * 
 * @author BrioLavone
 */

class AppleBasket extends FlxGroup
{

	private var basketGroup:FlxGroup;
	private var basket:FlxSprite;
	private var basketFull:Int = 20;
	private var flyingAppleArray:Array<Apple>;
	public static var basketPosX:Float;
	public static var basketPosY:Float;
	public var appleFlying:Bool = false;
	
	public function new() 
	{
		
		super();
		basketGroup = new FlxGroup();
		basket = new FlxSprite();
		flyingAppleArray = new Array<Apple>();
		basket.x = 90;
		basket.y = 25;
		initAppleArray();
		basket.loadGraphic(AssetPaths.basketAnimation__png, true, 36, 36);
		basket.animation.add("appleA", [0], 1, false);
		basket.animation.add("appleB", [1], 1, false);
		basket.animation.add("appleC", [2], 1, false);
		basket.animation.add("appleD", [3], 1, false);
		basket.animation.play("appleD");
		basketPosX = basket.x;
		basketPosY = basket.y;
		basketGroup.add(basket);
		add(basketGroup);
		
	}
	
	public function getBasketSprite() {
		return basket;
	}
	
	public function getBasketFullVal() {
		return basketFull;
	}
	
	public function getAppleFlying() {
		return appleFlying;
	}
	
	public function getAppleSpriteFromBasket() {
		return flyingAppleArray[0];
	}
	
	//set memebers
	public function setAppleFlying(val:Bool) {
		appleFlying = val;
	}
	//member functions
	private function initAppleArray() {
		for (i in 0...9) {
			flyingAppleArray[i] = new Apple();
			flyingAppleArray[i].getAppleSprite().alpha = 1;
			flyingAppleArray[i].setAppleGrowth(2);
			flyingAppleArray[i].growApple();
			flyingAppleArray[i].getAppleSprite().x = basket.x + 11;
			flyingAppleArray[i].getAppleSprite().y = basket.y + 11;
		}
	}
	
	public function animateBasket(appleCount:Int) {
		if (appleCount >= Math.floor(basketFull*.3) && appleCount < Math.floor(basketFull*.7)) {
			basket.animation.play("appleB");
		}
		if (appleCount >= Math.floor(basketFull*.7) && appleCount < basketFull) {
			basket.animation.play("appleC");
		}
		if (appleCount >= basketFull) {
			basket.animation.play("appleD");
		}
		if(appleCount < Math.floor(basketFull*.3)){
			basket.animation.play("appleA");
		}
	}

	override public function update() {
		
		super.update();
	}
	
}