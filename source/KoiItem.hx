package ;

import flixel.FlxSprite;

/**
 * ...
 * @author ...
 */
class KoiItem extends MarketItem
{
	
	public function new() 
	{
		super();
		initMarketItems();
	}
	
	private function initMarketItems() {
		itemSprite = new FlxSprite();
		itemCost = 35;
		itemName = "koiItem";
		itemCount = 1;
		itemSprite.loadGraphic(AssetPaths.fishItemAnimation__png, true, 30, 30, false, "koiItem");
		itemSprite.animation.add("available", [0], 1, false);
		itemSprite.animation.add("notAvailable", [1], 1, false);
		setItemAvailable(true);
	}
	
	public function setFishItemCount(val:Int) {
		itemCount = val;
	}
	
	override public function update() {
		trace(itemCount);
		setAvailableAnimation();
		super.update();
	}
	
}