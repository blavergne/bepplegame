package ;

import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.system.FlxSound;
/**
 * ...
 * @author ...
 */
class Mouse extends FlxGroup
{
	
	private var mouseGroup:FlxGroup;
	private var mouse:FlxSprite;
	private var mouseHole:FlxSprite;
	private var mouseHoleSound:FlxSound;
	private var mouseWalkSound:FlxSound;
	private var mouseWalking:Bool = false;
	private var turnAround:Bool = false;
	private var goHome:Bool = false;
	private var timeToDie:Bool = false;
	private var holeClosed:Bool = false;
	private var removeMe:Bool = false;
	private var firstPhase:Bool = true;
	private var secondPhase:Bool = false;
	private var thirdPhase:Bool = false;
	private var mouseCollision:Bool = false;
	private var timeToCloseHole:Bool = false;
	private var firstFinishRetired:Bool = false;
	private var secondFinishRetired:Bool = false;
	private var thirdFinishRetired:Bool = false;
	private var mouseAllDone:Bool = false;
	private var lastCheck:Bool = false;
	private var mouseWalkPause:Float = 0;
	private var mouseJumpPause:Float = 1;
	private var homeAnimationPause:Float = 3;
	private var mouseMovementSpeed:Int = 1;
	
	public function new() 
	{
		super();
		initMouse();
		
	}
	
	private function initMouse() {
		mouseGroup = new FlxGroup();
		mouse = new FlxSprite();
		mouseHole = new FlxSprite();
		mouseHole.loadGraphic(AssetPaths.holeAnimation__png, true, 16, 16);
		mouseHole.animation.add("makeHole", [0, 1, 2, 3], 6, false);
		mouseHole.animation.add("closeHole", [3, 2, 1, 0], 2, false);
		mouse.loadGraphic(AssetPaths.mouseAnimation__png, true, 16, 16);
		mouse.animation.add("mouseJump", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17], 10, false);
		mouse.animation.add("mouseHome", [17, 16, 15, 14, 13, 12, 11], 10, false);
		mouse.animation.add("mouseWalk", [18, 19, 20, 21], 10, true);
		mouseHoleSound = FlxG.sound.load(AssetPaths.mouseHoleSound__wav);
		mouseWalkSound = FlxG.sound.load(AssetPaths.mouseWalkingSound__wav, 1, true);
		mouse.setFacingFlip(FlxObject.LEFT, false, false);
		mouse.setFacingFlip(FlxObject.RIGHT, true, false);
		mouseGroup.add(mouse);
		mouseGroup.add(mouseHole);
		add(mouseGroup);
		
	}
	
	private function mouseWork() {
		if (firstPhase) {
			mouseHoleSound.play();
			mouseHole.animation.play("makeHole");
			mouse.animation.play("mouseJump");
			firstPhase = false;
		}
		if (mouse.animation.finished && !firstFinishRetired){
			secondPhase = true;
			firstFinishRetired = true;
		}
		if (secondPhase) {
			mouseWalkPause -= FlxG.elapsed;
			if (mouseWalkPause < 0) {
				mouseWalkSound.play();
				mouse.animation.play("mouseWalk");
				mouseWalkPause = .1;
				if(!mouseCollision){
					mouse.x -= 2;
				}
				else {
					turnAround = true;
					mouse.facing = FlxObject.RIGHT;
					mouse.x += 2;
				}
			}
		}
		
		if (turnAround && (mouse.x == mouseHole.x)) {
			mouseWalkSound.stop();
			secondPhase = false;
			mouse.animation.play("mouseHome");
			turnAround = false;
		}
		if (mouse.animation.finished && !secondFinishRetired){ 
			thirdPhase = true;
			secondFinishRetired = true;
		}
		if (thirdPhase) {
			mouseHoleSound.play();
			mouseHole.animation.play("closeHole");
			thirdPhase = false;
			lastCheck = true;
		}
		if (mouseHole.animation.finished && !thirdFinishRetired && lastCheck) {
			mouseAllDone = true;
			thirdFinishRetired = true;
		}
		
	}
	
	public function getMouseSprite() {
		return mouse;
	}
	
	public function getMouseAllDone() {
		return mouseAllDone;
	}
	
	public function getHoleSprite() {
		return mouseHole;
	}
	
	public function setMouseCollision(val:Bool) {
		mouseCollision = true;
	}
	
	override public function destroy() {
		mouseGroup = null;
		mouse = null;
		mouseHole = null;
		mouseHoleSound = null;
		mouseWalkSound = null;
		super.destroy();
	}
	
	public function faceRight() {
		mouse.facing = FlxObject.RIGHT;
	}
	
	override public function update() {
		
		mouseWork();
		super.update();
	}
	
}