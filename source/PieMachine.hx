package ;

import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.plugin.MouseEventManager;
import flixel.FlxG;
import flixel.system.FlxSound;
import flixel.tweens.FlxTween;
import flixel.util.FlxDestroyUtil;
/**
 * ...
 * @author ...
 */
class PieMachine extends FlxGroup
{

	private var pieMachineGroup:FlxGroup;
	private var pieMachineSprite:FlxSprite;
	private var pieMachineAlertSprite:FlxSprite;
	private var minusOneSprite:FlxSprite;
	private var repairAlertSprite:FlxSprite;
	private var pieMachineFlyingPastry:FlxSprite;
	private var pieBakingSound:FlxSound;
	private var pieMachineSound:FlxSound;
	private var myRelativeX:Float;
	private var myRelativeY:Float;
	private var electricUtilityTimer:Float = 10;
	private var machineReliabilityTimer:Float = 15;
	private var rise:Float;
	private var run:Float;
	private var goldCount:Int;
	private var appleCount:Int;
	private var payElectricBill:Bool = false;
	private var machineOn:Bool = false;
	private var machineAlertReset:Bool = false;
	private var machineClicked:Bool = false;
	private var repairClicked:Bool = false;
	private var animateRepairSprite:Bool = false;
	private var pastryFlying:Bool = false;
	private var inPiePack:Bool = false;
	private var myTile:Tile;
	
	public function new(tileSprite:Tile) 
	{
		super();
		myTile = tileSprite;
		pieMachineGroup = new FlxGroup();
		pieMachineSprite = new FlxSprite();
		pieMachineAlertSprite = new FlxSprite();
		minusOneSprite = new FlxSprite();
		repairAlertSprite = new FlxSprite();
		pieMachineFlyingPastry = new FlxSprite();
		minusOneSprite.loadGraphic(AssetPaths.minusOneImage__png, false);
		minusOneSprite.alpha = 0;
		pieMachineSprite.loadGraphic(AssetPaths.pieMachineAnimation__png, true, 86, 84);
		pieMachineSprite.animation.add("machineOn", [0, 1, 2, 3, 4], 5, true);
		pieMachineSprite.animation.add("machineOff", [0], 1, false);
		pieMachineSprite.animation.play("machineOff");
		pieMachineAlertSprite.loadGraphic(AssetPaths.juiceMachineSwitchAnimation__png, true, 32, 18);
		pieMachineAlertSprite.animation.add("switchOn", [1], 1, false);
		pieMachineAlertSprite.animation.add("switchOff", [0], 1, false);
		pieMachineAlertSprite.animation.play("switchOn");
		repairAlertSprite.loadGraphic(AssetPaths.repairAnimationS__png, true, 16, 16);
		repairAlertSprite.animation.add("repairMe", [0, 1, 2, 3, 4], 10, true);
		pieMachineFlyingPastry.loadGraphic(AssetPaths.pieIcon__png);
		repairAlertSprite.alpha = 0;
		pieMachineFlyingPastry.x = tileSprite.x + 90;
		pieMachineFlyingPastry.y = tileSprite.y + 5;
		pieMachineSprite.x = tileSprite.x + 57;
		pieMachineSprite.y = tileSprite.y - 15;
		pieMachineAlertSprite.x = tileSprite.x + 78;
		pieMachineAlertSprite.y = tileSprite.y + 3;
		repairAlertSprite.x = tileSprite.x + 96;
		repairAlertSprite.y = tileSprite.y + 67;
		registerMachineEvents();
		pieMachineGroup.add(pieMachineSprite);
		pieMachineGroup.add(pieMachineAlertSprite);
		pieMachineGroup.add(repairAlertSprite);
		pieMachineGroup.add(minusOneSprite);
		add(pieMachineGroup);
		
	}
	
	public function getpieMachineSprite() {
		return pieMachineSprite;
	}

	public function getInPiePack() {
		return inPiePack;
	}
	
	public function getMyRelativeX() {
		return myRelativeX;
	}
	
	public function getPastryFlying() {
		return pastryFlying;
	}
	
	public function getPieMachineFlyingPastry() {
		return pieMachineFlyingPastry;
	}
	
	public function getMyRelativeY() {
		return myRelativeY;
	}
	
	public function getMachineOn() {
		return machineOn;
	}
	
	public function getPayElectricBill() {
		return payElectricBill;
	}
	
	public function getRepairClicked() {
		return repairClicked;
	}
	
	public function getAnimateRepairSprite() {
		return animateRepairSprite;
	}
	
	public function setAnimateRepairSprite(val:Bool) {
		animateRepairSprite = val;
	}
	
	public function setPastryFlying(val:Bool) {
		pastryFlying = val;
	}
	
	public function setRepairClicked(val:Bool) {
		repairClicked = val;
	}
	
	public function setInPiePack(val:Bool) {
		inPiePack = val;
	}
	
	public function setPayElectricBill(val:Bool) {
		payElectricBill = val;
	}
	
	public function setMachineOn(val:Bool) {
		machineOn = val;
	}
	
	public function setMyRelativeX(val:Float) {
		myRelativeX = val;
	}
	
	public function setMyRelativeY(val:Float) {
		myRelativeY = val;
	}
	
	public function setGoldCount(val:Int) {
		goldCount = val;
	}
	
	public function setAppleCount(val:Int) {
		appleCount = val;
	}
	
	public function registerMachineEvents() {
		MouseEventManager.add(pieMachineAlertSprite, turnMachineOn, null, null, null);
		MouseEventManager.add(repairAlertSprite, repairAlertClicked, null, null, null);
	}
	
	public function unregisterMachineEvents() {
		MouseEventManager.remove(pieMachineAlertSprite);
		MouseEventManager.remove(repairAlertSprite);
	}
	
	public function registerMachineButtonEvent() {
		MouseEventManager.add(pieMachineAlertSprite, turnMachineOn, null, null, null);
	}
	
	private function repairAlertClicked(sprite:FlxSprite) {
		repairClicked = true;
	}
	
	public function turnMachineOn(sprite:FlxSprite) {
		
		if (!machineOn) {
			pieMachineSound = FlxG.sound.load(AssetPaths.juiceMachineSound__wav, .2);
			pieMachineSound.play();
			pieBakingSound = FlxG.sound.load(AssetPaths.pieBakedSound__wav, 1 , true);
			pieBakingSound.play();
			pieMachineAlertSprite.animation.play("switchOff");
			pieMachineSprite.animation.play("machineOn");
			machineOn = true;
		}
		else {
			pieMachineAlertSprite.animation.play("switchOn");
			pieMachineSprite.animation.play("machineOff");
			if (pieBakingSound != null && pieMachineSound != null) {
				pieMachineSound.stop();
				pieBakingSound.stop();
				pieBakingSound = FlxDestroyUtil.destroy(pieBakingSound);
				pieMachineSound = FlxDestroyUtil.destroy(pieMachineSound);
			}
			machineOn = false;
		}
	}
	
	public function payElectricUtility() {
		if(machineOn){
			electricUtilityTimer -= FlxG.elapsed;
			if (electricUtilityTimer < 0) {
				minusOneSprite.x = pieMachineSprite.x + 30;
				minusOneSprite.y = pieMachineSprite.y + 5;
				minusOneSprite.alpha = 1;
				FlxTween.tween(minusOneSprite, { alpha:0, y:minusOneSprite.y - 16 }, 1);
				payElectricBill = true;
				electricUtilityTimer = 10;
			}
		}
	}
	
	private function machineReliability() {
		if(machineOn){
			machineReliabilityTimer -= FlxG.elapsed;
			if (machineReliabilityTimer < 0) {
				animateRepairSprite = true;
			}
		}
	}
	
	public function resetRepairAlertTimer() {
		machineReliabilityTimer = 15;
	}
	
	private function repairSpriteAnimation() {
		if (animateRepairSprite) {
			repairAlertSprite.alpha += .01;
			repairAlertSprite.animation.play("repairMe");
			machineReliabilityTimer = -1;
			MouseEventManager.remove(pieMachineAlertSprite);
			machineOn = true;
			turnMachineOn(null);
		}
		else {
			repairAlertSprite.alpha -= .1;
		}
	}
	
	public function setRiseRun() {
		run = -(PiePackIcon.piePackPosX + 20 - pieMachineFlyingPastry.x);
		rise = -(PiePackIcon.piePackPosY + 20 - pieMachineFlyingPastry.y);
	}
	
	public function sendPastryToPack() {
		if (pastryFlying) {
			pieMachineFlyingPastry.alpha = 1;
			if (pieMachineFlyingPastry.y >= PiePackIcon.piePackPosY + 20) {
					pieMachineFlyingPastry.y -= rise/25;
				}
			if (pieMachineFlyingPastry.x >= PiePackIcon.piePackPosX + 20) {
				pieMachineFlyingPastry.x -= run/25;
			}
			if (pieMachineFlyingPastry.x <= PiePackIcon.piePackPosX + 22 && pieMachineFlyingPastry.y <= PiePackIcon.piePackPosY + 22) {
				inPiePack = true;
				pastryFlying = false;
				pieMachineFlyingPastry.alpha = 0;
				pieMachineFlyingPastry.x = myTile.x + 90;
				pieMachineFlyingPastry.y = myTile.y + 5;
			}
		}
	}
	
	override public function update() {
		machineReliability();
		repairSpriteAnimation();
		payElectricUtility();
		sendPastryToPack();
		super.update();
	}
	
	override public function destroy() {
		myTile = null;
		pieMachineGroup = null;
		pieMachineSprite = null;
		pieMachineAlertSprite = null;
		minusOneSprite = null;
		repairAlertSprite = null;
		pieMachineFlyingPastry = null;
		super.destroy();
	}
	
}