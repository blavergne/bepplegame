package ;

import flixel.FlxSprite;
import flixel.plugin.MouseEventManager;
import flixel.FlxG;

/**
 * ...
 * @author BrioLavone
 */
class Seed extends FlxSprite
{

	private var dragging:Bool = false;
	private var dragType:Bool;
	private var isMousedOver:Bool = true;
	private var homeX:Int = -26;
	private var homeY:Int = 70;
	
	public function new(X:Float=0, Y:Float=0, dragType:Bool) 
	{
		super(X, Y);
		this.dragType = dragType;
		loadGraphic(AssetPaths.tree_seed__png, false, 18, 21);
		this.x = homeX;
		this.y = homeY;
		if (dragType) {
			homeX = 139;
			homeY = 70;
		}
		MouseEventManager.add(this, onDown, onUp, null, null);
		
	}
	
	private function onDown(sprite:FlxSprite) {
		
		dragging = true;
		
	}

	private function onUp(sprite:FlxSprite) {
		dragging = false;
		resetSeedPos();
		
	}
	
	private function dragSeed(temp) {
		
		if (temp) {
			
			this.x = FlxG.mouse.x - 10;
			this.y = FlxG.mouse.y - 10;
			
		}
			
	}
	
	public function resetSeedPos() {
		if(dragType){
			this.x = homeX;
			this.y = homeY;
		}
	}
	
	public function getDragging() {
		
		return dragging;
		
	}
	
	public function setDragging(val:Bool) {
		
		dragging = val;
		
	}
	
	public function checkMouseState() {
		
		if (FlxG.mouse.justReleased) {
			dragging = false;
			resetSeedPos();
		}
		
	}
	
	public function unregisterSeedEvent() {
		MouseEventManager.remove(this);
	}
	
	public function registerSeedEvent() {
		MouseEventManager.add(this, onDown, onUp, null, null);
	}
	
	override public function update():Void {
		
		checkMouseState();
		dragSeed(dragging);
		super.update();
		
	}
	
	override public function destroy():Void {
		
		MouseEventManager.remove(this);
		super.destroy();
		
	}
	
}