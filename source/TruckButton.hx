package ;

import flixel.group.FlxGroup;
import flixel.FlxSprite;
/**
 * ...
 * @author ...
 */
class TruckButton extends FlxGroup
{

	private var truckButtonGroup:FlxGroup;
	private var truckButton:FlxSprite;
	private var buttonLevel:Int = 0;
	private var buttonClickable:Bool = false;
	
	public function new() 
	{
		super();
		initTruckButton();	
	}
	
	private function initTruckButton() {
		truckButtonGroup = new FlxGroup();
		truckButton = new FlxSprite();
		truckButton.loadGraphic(AssetPaths.truckBellAnimation__png, true, 32, 66);
		truckButton.animation.add("truckButtonA", [0], 1, false);
		truckButton.animation.add("truckButtonB", [4], 10, false);
		truckButton.animation.add("truckButtonC", [1,2,3,4,5,6,5,4,3,2,1,0], 10, false);
		truckButton.x = 50;
		truckButton.y = 195;
		truckButtonGroup.add(truckButton);
		add(truckButtonGroup);
	}
	
	public function getTruckButtonSprite() {
		return truckButton;
	}
	
	public function getButtonClickable() {
		return buttonClickable;
	}
	
	public function setButtonClickable(val:Bool) {
		buttonClickable = val;
	}
	
	public function setButtonLevel(val:Int) {
		buttonLevel = val;
	}
	
	public function buttonSetting() {
		switch(buttonLevel) {
			case 0:
				truckButton.animation.play("truckButtonA");
			case 1:
				truckButton.animation.play("truckButtonB");	
			case 2:
				truckButton.animation.play("truckButtonC");	
		}
	}
	
	override public function update() {

		super.update();
	}
	
}