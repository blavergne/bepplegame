package ;

import flixel.FlxState;

/**
 * ...
 * @author BrioLavone
 * LoseState will trigger when player has zero trees left && no money to buy additional seeds. 
 * state will fade in. Exit button will be center of screen. Temp LoseState animation.
 */
class LoseState extends FlxState
{
var playBtn:FlxButton;
	var gameTitle:FlxText;
	
	/**
	 * Function that is called up when to state is created to set it up. 
	 */
	override public function create():Void
	{
		playBtn = new FlxButton(0, 0, "EXIT", exitGame);
		gameTitle = new FlxText(0, 0, 100, "Bepple", 20, false);
		gameTitle.alignment = "center";
		gameTitle.screenCenter(true, true);
		gameTitle.y -= 30;
		playBtn.screenCenter();
		add(gameTitle);
		add(playBtn);
		super.create();
		
	}
	
	/**
	 * Function that is called when this state is destroyed - you might want to 
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		
		super.destroy();
		playBtn = FlxDestroyUtil.destroy(playBtn);
		
	}

	/**
	 * Function that is called once every frame.
	 */
	override public function update():Void
	{
		super.update();
	}	
	
	private function exitGame():Void {
		
		//FlxG.switchState(new PlayState());
		trace("Close game");
		
	}
}