package ;

import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.system.ui.FlxSystemButton;
import flixel.util.FlxRandom;
import flixel.FlxG;
import flixel.system.FlxSound;
import flixel.plugin.MouseEventManager;
import flixel.util.FlxDestroyUtil;

/**
 * ...
 * @author ...
 */
class KoiPond extends FlxGroup
{

	private var koiPondGroup:FlxGroup;
	private var koiPondSprite:FlxSprite;
	private var koiRock:FlxSprite;
	private var koiFishSprite:FlxSprite;
	private var koiPadSprite:FlxSprite;
	private var koiPadSpriteB:FlxSprite;
	private var koiPadSpriteC:FlxSprite;
	private var koiSandSprite:FlxSprite;
	private var koiFish:KoiFish;
	private var koiFishB:KoiFish;
	private var koiPondSound:FlxSound;
	private var koiPondClickedSound0:FlxSound;
	private var koiPondClickedSound1:FlxSound;
	private var koiPondClickedSound2:FlxSound;
	private var koiPondClickedSound3:FlxSound;
	
	public function new(tileSprite:Tile) 
	{
		super();
		koiPondGroup = new FlxGroup();
		koiPondSprite = new FlxSprite();
		koiRock = new FlxSprite();
		koiPadSprite = new FlxSprite();
		koiPadSpriteB = new FlxSprite();
		koiPadSpriteC = new FlxSprite();
		koiSandSprite = new FlxSprite();
		MouseEventManager.add(koiPondSprite, splashSounds, null, null, null);
		koiPondSound = FlxG.sound.load(AssetPaths.koiPondSound__wav, .05, true);
		koiPondSound.play();
		koiPondClickedSound0 = FlxG.sound.load(AssetPaths.koiClicked0__wav, .3, false);
		koiPondClickedSound1 = FlxG.sound.load(AssetPaths.koiClicked1__wav, .3, false);
		koiPondClickedSound2 = FlxG.sound.load(AssetPaths.koiClicked2__wav, .3, false);
		koiPondClickedSound3 = FlxG.sound.load(AssetPaths.koiClicked4__wav, .3, false);
		koiRock.loadGraphic(AssetPaths.koiRock__png, false);
		koiPadSprite.loadGraphic(AssetPaths.koiPad__png, false);
		koiPadSpriteB.loadGraphic(AssetPaths.koiPad__png, false);
		koiPadSpriteC.loadGraphic(AssetPaths.koiPad__png, false);
		koiSandSprite.loadGraphic(AssetPaths.koiSand__png, false);
		koiPondSprite.loadGraphic(AssetPaths.koiPondAnimation__png, true, 76, 72);
		koiPondSprite.animation.add("koiPondAnimate", [0, 1], 5, true);
		koiPondSprite.animation.play("koiPondAnimate");
		koiPondSprite.x = tileSprite.x + 60;
		koiPondSprite.y = tileSprite.y + 8;
		koiSandSprite.x = koiPondSprite.x;
		koiSandSprite.y = koiPondSprite.y;
		koiRock.x = koiPondSprite.x + 75;
		koiRock.y = koiPondSprite.y + 20;
		koiPadSprite.x = koiPondSprite.x - 20;
		koiPadSprite.y = koiPondSprite.y + 30;
		koiPadSpriteB.x = koiPondSprite.x - 18;
		koiPadSpriteB.y = koiPondSprite.y + 22;
		koiPadSpriteC.x = koiPondSprite.x - 14;
		koiPadSpriteC.y = koiPondSprite.y + 39;
		koiFish = new KoiFish(koiPondSprite.x, koiPondSprite.y);
		koiFishB = new KoiFish(koiPondSprite.x, koiPondSprite.y);
		koiPondGroup.add(koiSandSprite);
		koiPondGroup.add(koiPondSprite);
		koiPondGroup.add(koiFish);
		koiPondGroup.add(koiFishB);
		koiPondGroup.add(koiRock);
		koiPondGroup.add(koiPadSpriteB);
		koiPondGroup.add(koiPadSpriteC);
		koiPondGroup.add(koiPadSprite);
		add(koiPondGroup);
		
	}
	
	private function splashSounds(sprite:FlxSprite) {
		var clickSound:Int = FlxRandom.intRanged(0, 3);
		switch(clickSound) {
			case 0:
				koiPondClickedSound0.play();
			case 1:
				koiPondClickedSound1.play();
			case 2:
				koiPondClickedSound2.play();
			case 3:	
				koiPondClickedSound3.play();
		}
		koiFish.fishScared();
		koiFishB.fishScared();
	}
	
	override public function update() {
		super.update();
	}
	
	override public function destroy() {
		koiPondSound = FlxDestroyUtil.destroy(koiPondSound);
		koiPondSprite = null;
		koiFish = null;
		koiFishB = null;
		koiFishSprite = null;
		koiPondSound = null;
		koiPadSprite = null;
		koiPadSpriteB = null;
		koiPadSpriteC = null;
		koiRock = null;
		koiSandSprite = null;
		koiPondClickedSound0 = null;
		koiPondClickedSound1 = null;
		koiPondClickedSound2 = null;
		koiPondClickedSound3 = null;
		koiPondGroup = null;
		super.destroy();
	}
}