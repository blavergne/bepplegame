package ;
import flixel.group.FlxGroup;
import flixel.FlxSprite;
/**
 * ...
 * @author BrioLavone
 */
class SystemStar extends FlxGroup
{

	private var systemStarGroup:FlxGroup;
	private var systemStarSprite:FlxSprite;
	
	public function new() 
	{
		super();
		init();
		
	}
	
	private function init() {
		systemStarGroup = new FlxGroup();
		systemStarSprite = new FlxSprite();
		systemStarSprite.loadGraphic(AssetPaths.systemStarAnimation__png, true, 34, 34);
		systemStarSprite.animation.add("starAnimation", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 12, 12, 12], 2, true);
		systemStarSprite.animation.play("starAnimation");
		systemStarSprite.x = 500;
		systemStarSprite.y = 40;
		systemStarGroup.add(systemStarSprite);
		add(systemStarGroup);
	}
	
}