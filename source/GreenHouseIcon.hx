package ;

import flixel.FlxSprite;
import flixel.plugin.MouseEventManager;
import flixel.FlxG;

/**
 * ...
 * @author BrioLavone
 */
class GreenHouseIcon extends FlxSprite
{

	private var dragging:Bool = false;
	private var dragType:Bool;
	private var isMousedOver:Bool = true;
	private var homeX:Int = -123;
	private var homeY:Int = 70;
	
	public function new(X:Float=0, Y:Float=0, dragType:Bool) 
	{
		super(X, Y);
		this.dragType = dragType;
		loadGraphic(AssetPaths.greenHouseIconImage__png, false, 17, 22);
		this.x = homeX;
		this.y = homeY;
		if (dragType) {
			homeX = 42;
			homeY = 70;
		}
		MouseEventManager.add(this, onDown, onUp, null, null);
		
	}
	
	private function onDown(sprite:FlxSprite) {
		
		dragging = true;
		
	}

	private function onUp(sprite:FlxSprite) {
		dragging = false;
		resetGreenHouseImagePos();
		
	}
	
	private function dragGreenHouseImage(temp) {
		
		if (temp) {
			
			this.x = FlxG.mouse.x - 10;
			this.y = FlxG.mouse.y - 10;
			
		}
			
	}
	
	public function resetGreenHouseImagePos() {
		if(dragType){
			this.x = homeX;
			this.y = homeY;
		}
	}
	
	public function getDragging() {
		
		return dragging;
		
	}
	
	public function setDragging(val:Bool) {
		
		dragging = val;
		
	}
	
	public function checkMouseState() {
		
		if (FlxG.mouse.justReleased) {
			dragging = false;
			resetGreenHouseImagePos();
		}
		
	}
	
	public function unregisterGreenHouseImageEvent() {
		MouseEventManager.remove(this);
	}
	
	public function registerGreenHouseImageEvent() {
		MouseEventManager.add(this, onDown, onUp, null, null);
	}
	
	override public function update():Void {
		
		checkMouseState();
		dragGreenHouseImage(dragging);
		super.update();
		
	}
	
	override public function destroy():Void {
		
		MouseEventManager.remove(this);
		super.destroy();
		
	}
	
}