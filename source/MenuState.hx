package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState; 
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxMath;
import flixel.util.FlxDestroyUtil;
import flash.system.System;
using flixel.util.FlxSpriteUtil;

/**
 * A FlxState which can be used for the game's menu.
 */
class MenuState extends FlxState
{
	
	private var backgroundSprite:FlxSprite;
	private var playBtn:FlxButton;
	private var tutButton:FlxButton;
	private var mathlessBepple:FlxButton;
	private var exitButton:FlxButton;
	private var gameTitle:FlxText;
	private var beppleTitle:FlxSprite;
	private var apple_0:Apple;
	private var apple_1:Apple;
	private var apple_2:Apple;
	private var apple_3:Apple;
	private var apple_4:Apple;
	private var apple_0Timer:Float = 2;
	private var apple_1Timer:Float = 3;
	private var apple_2Timer:Float = 1;
	private var apple_3Timer:Float = .2;
	private var apple_4Timer:Float = 5;
	private var apple_0Created:Bool = false;
	private var apple_1Created:Bool = false;
	private var apple_2Created:Bool = false;
	private var apple_3Created:Bool = false;
	private var apple_4Created:Bool = false;
	public static var mathless:Bool = false;
	public static var tutorialOn:Bool;
	static var windowLength:Int = 640;
	static var windowWidth:Int = 480;
	
	/**
	 * Function that is called up when to state is created to set it up. 
	 */
	override public function create():Void
	{
		beppleTitle = new FlxSprite();
		backgroundSprite = new FlxSprite();
		beppleTitle.loadGraphic(AssetPaths.titleImageProcessed__png);
		backgroundSprite.loadGraphic(AssetPaths.titleBackground__png);
		beppleTitle.x = 100;
		beppleTitle.y = 50;
		playBtn = new FlxButton(0, 0, "Normal", startGame);
		gameTitle = new FlxText(0, 0, 100, "Bepple", 20, false);
		tutButton = new FlxButton(0, 0, "Tutorial", startTutorial);
		exitButton = new FlxButton(0, 0, "Exit", exitGame);
		mathlessBepple = new FlxButton(0, 0, "Easy", startMathless);
		gameTitle.alignment = "center";
		gameTitle.screenCenter(true, true);
		gameTitle.y -= 30;
		playBtn.screenCenter();
		tutButton.screenCenter();
		tutButton.y += 70;
		mathlessBepple.screenCenter();
		playBtn.y += 35;
		exitButton.screenCenter();
		exitButton.y += 105;
		add(backgroundSprite);
		add(beppleTitle);
		add(playBtn);
		add(tutButton);
		add(mathlessBepple);
		//add(exitButton);
		super.create();
		
	}
	
	public function getTutorialOn() {
		return tutorialOn;
	}
	
	private function appleMachine() {
		apple_0Timer -= FlxG.elapsed;
		apple_1Timer -= FlxG.elapsed;
		apple_2Timer -= FlxG.elapsed;
		apple_3Timer -= FlxG.elapsed;
		apple_4Timer -= FlxG.elapsed;
		if (apple_0Timer <= 0 && !apple_0Created) {
			apple_0 = new Apple();
			apple_0.setAppleX(156);
			apple_0.setAppleY(60);
			apple_0.incAppleGrowth();
			apple_0.growApple();
			apple_0.incAppleGrowth();
			apple_0.growApple();
			apple_0Created = true;
			add(apple_0);
		}
		if (apple_1Timer <= 0 && !apple_1Created) {
			apple_1 = new Apple();
			apple_1.setAppleX(250);
			apple_1.setAppleY(60);
			apple_1.incAppleGrowth();
			apple_1.growApple();
			apple_1.incAppleGrowth();
			apple_1.growApple();
			apple_1Created = true;
			add(apple_1);
		}
		if (apple_2Timer <= 0 && !apple_2Created) {
			apple_2 = new Apple();
			apple_2.setAppleX(310);
			apple_2.setAppleY(132);
			apple_2.incAppleGrowth();
			apple_2.growApple();
			apple_2.incAppleGrowth();
			apple_2.growApple();
			apple_2Created = true;
			add(apple_2);
		}
		if (apple_3Timer <= 0 && !apple_3Created) {
			apple_3 = new Apple();
			apple_3.setAppleX(450);
			apple_3.setAppleY(153);
			apple_3.incAppleGrowth();
			apple_3.growApple();
			apple_3.incAppleGrowth();
			apple_3.growApple();
			apple_3Created = true;
			add(apple_3);
		}
		if (apple_4Timer <= 0 && !apple_4Created) {
			apple_4 = new Apple();
			apple_4.setAppleX(500);
			apple_4.setAppleY(82);
			apple_4.incAppleGrowth();
			apple_4.growApple();
			apple_4.incAppleGrowth();
			apple_4.growApple();
			apple_4Created = true;
			add(apple_4);
		}
		if(apple_0 != null){
			if (apple_0.getAppleSprite().y > 480) {
				apple_0.destroy();
				apple_0 = null;
				apple_0Timer = 3;
				apple_0Created = false;
			}
		}
		if(apple_1 != null){
			if (apple_1.getAppleSprite().y > 480) {
				apple_1.destroy();
				apple_1 = null;
				apple_1Timer = 2;
				apple_1Created = false;
			}
		}
		if(apple_2 != null){
			if (apple_2.getAppleSprite().y > 480) {
				apple_2.destroy();
				apple_2 = null;
				apple_2Timer = 2.7;
				apple_2Created = false;
			}
		}
		if(apple_3 != null){
			if (apple_3.getAppleSprite().y > 480) {
				apple_3.destroy();
				apple_3 = null;
				apple_3Timer = 1.5;
				apple_3Created = false;
			}
		}
		if(apple_4 != null){
			if (apple_4.getAppleSprite().y > 480) {
				apple_4.destroy();
				apple_4 = null;
				apple_4Timer = 3.3;
				apple_4Created = false;
			}
		}
	}
	
	/**
	 * Function that is called when this state is destroyed - you might want to 
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		playBtn = FlxDestroyUtil.destroy(playBtn);
		tutButton = FlxDestroyUtil.destroy(tutButton);
		super.destroy();
	}

	/**
	 * Function that is called once every frame.
	 */
	override public function update():Void
	{	
		appleMachine();
		super.update();
	}	
	
	private function startGame():Void {
		mathless = false;
		tutorialOn = false;
		FlxG.switchState(new PlayState());
	}

	private function exitGame():Void {
		System.exit(0);
	}
	
	private function startTutorial():Void {
		mathless = false;
		tutorialOn = true;
		FlxG.switchState(new PlayState());
	}
	
	private function startMathless():Void {
		mathless = true;
		tutorialOn = false;
		FlxG.switchState(new PlayState());
	}
}