package ;

import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.system.FlxSound;
import openfl.geom.ColorTransform;
import flixel.util.FlxRandom;
import flixel.FlxG;
import flixel.plugin.MouseEventManager;
import flixel.util.FlxDestroyUtil;

/**
 * ...
 * @author BrioLavone
 * Apple will start to grow once a tree timer called treeHappyTimer reaches max
 * apple will appear in random half of tree foliage and start its own timer which
 * will ripen when max.	
 */
class Apple extends FlxGroup
{
	
	private var appleGroup:FlxGroup;
	private var apple:FlxSprite;
	private var basket:FlxSprite;
	private var pickedAppleLeaves:FlxSprite;
	private var pickedLeavesSound:FlxSound;
	private var pickedAppleGround:FlxSound;
	private var fallenAppleSound:FlxSound;
	private var appleGrowth:Int = -1;
	private var iAmRipe:Bool = false;
	private var iAmVisible:Bool = false;
	private var iAmDead:Bool = false;
	private var appleDropped:Bool = false;
	private var rotLock:Bool = false;
	private var iAmRotten:Bool = false;
	private var flyToBasket:Bool = false;
	private var inBasket:Bool = false;
	private var leavesFallOnce:Bool = false;
	private var stillOnTree:Bool = true;
	private var appleFreeFall:Bool = false;
	private var appleOnGround:Bool = false;
	private var timeToDrop:Float = 15;
	private var rotTimer:Float = 15;
	private var myInitialY:Float;
	private var randomXPos:Int;
	private var dropRate:Int = 300;
	private var rise:Float;
	private var run:Float;
	private var randomYPos:Int;
	private var myRipeCountDown:Float = 5;
	
	public function new() 
	{
		super();
		apple = new FlxSprite();
		appleGroup = new FlxGroup();
		pickedAppleLeaves = new FlxSprite();
		basket = null;
		apple.loadGraphic(AssetPaths.animatedAppleB__png, true, 15, 16);
		apple.animation.add("appleStart", [0], 1, false);
		apple.animation.add("appleRipen", [1, 2, 3, 4, 5, 6, 7], 3, false);
		apple.animation.add("flyingApple", [7], 1, false);
		apple.animation.add("appleRot", [8, 9 ,10 ,11], 3, false);
		apple.alpha = 0;
		pickedAppleLeaves.loadGraphic(AssetPaths.pickedAppleLeaves__png, true, 16, 16);
		pickedAppleLeaves.animation.add("leavesFall", [0, 1, 2, 3, 4], 5, false);
		pickedAppleLeaves.alpha = 0;
		appleGroup.add(apple);
		appleGroup.add(pickedAppleLeaves);
		add(appleGroup);
		
	}
	
	//returns a random X value position on tree
	public function getRandomX() {
		
		randomXPos = FlxRandom.intRanged(10, 64);
		return randomXPos;
		
	}
	//returns random Y value position on tree
	public function getRandomY() {
		
		randomYPos = FlxRandom.intRanged(25, 36);
		return randomYPos;
		
	}
	
	public function getAppleOnGround() {
		return appleOnGround;
	}
	
	public function getMyXPos() {
		
		return apple.x;
		
	}
	//return true if the apple has dropped on ground
	public function getAppleDropped() {
		return appleDropped;
	}
	
	public function getMyYPos() {
			
		return apple.y;
		
	}
	
	public function getAppleSprite() {
		return apple;
	}
	//returns time til apple rots
	public function getRotTimer() {
		return rotTimer;
	}
	//returns true if apple has appeared on screen
	public function getAppleVisible() {
		
		return iAmVisible;
		
	}
	//returns true if apple has gone rotten
	public function getIAmRotten() {
		return iAmRotten;
	}
	
	public function getAppleFreeFall() {
		return appleFreeFall;
	}
	
	//returns true if apple is ripe
	public function getIAmRipe() {
		
		return iAmRipe;
		
	}
	//returns value for current apple growth phase
	public function getAppleGrowth() {
		return appleGrowth;
	}
	
	public function getMyInitialY() {
		return myInitialY;
	}
	//returns true if apple is picked or goes rotten
	public function getIAmDead() {
		return iAmDead;
	}
	public function getInBasket() {
		return inBasket;
	}
	
	public function setMyInitialY(val:Float) {
		myInitialY = val;
	}
	
	public function getStillOnTree() { 
		return stillOnTree;
	}
	//sets value of apple fall speed
	public function setDropRate(val:Int) {
		dropRate = val;
	}
	public function setFlyToBasket(val:Bool) {
		flyToBasket = val;
	}
	//ripens apple after count down by incrementing growth and calling grow apple function
	//sets myRipeCountDown to 5 which leads to rotten status
	public function ripenApple() {
		
		myRipeCountDown -= FlxG.elapsed;
		
		if (myRipeCountDown < 0) {
			
			appleGrowth++;
			growApple();
			myRipeCountDown = 5;
			
		}
		
	}
	
	public function pauseRipen() {
		myRipeCountDown = 9999;
	}
	
	public function unpauseRipen() {
		myRipeCountDown = 5;
	}
	//depending on appleGrowth value, apple will wither be set to visible or start to ripen
	public function growApple() {
		
		switch(appleGrowth) {

			case 0:
				iAmVisible = true;
				apple.animation.play("appleStart");
				
			case 1:
				apple.animation.play("appleRipen");
				iAmRipe = true;
			
			case 2:
				apple.animation.play("flyingApple");
		}
		
	}
	//Transitions apple from low alpha to visible alpha
	private function revealApples() {
		
		if (iAmVisible && apple.alpha < 1) {
			apple.alpha += .01;
		}
		
	}
	
	public function setAppleGrowth(val:Int ) {
		appleGrowth = val;
	}
	
	public function setAppleX(xVal:Float) {
		
		apple.x = xVal;
		
	}
	public function setAppleY(yVal:Float) {
		
		apple.y = yVal;
		
	}
	
	public function setLeavesFallOnce(val:Bool) {
		leavesFallOnce = val;
	}
	
	public function incAppleGrowth() {
		
		appleGrowth++;
		
	}
	
	public function stopFreeFall() {
		apple.velocity.y = 0;
		apple.acceleration.y = 0;
		timeToDrop = 999;
		appleOnGround = true;
		appleFreeFall = false;
		run = -(basket.x + 30 - apple.x);
		rise = -(basket.y + 30 - apple.y);
		if (stillOnTree) {
			fallenAppleSound = FlxG.sound.load(AssetPaths.fallenAppleSound__wav);
			fallenAppleSound.play();
			stillOnTree = false;
		}
	}
	//If apple is ripened timeToDrop timer starts and drops apple to ground when timer is done.
	private function dropApple() {
		if (iAmRipe) {
			timeToDrop -= FlxG.elapsed;
			if (timeToDrop < 0) {
				appleFreeFall = true;
				appleDropped = true;
				apple.acceleration.y = dropRate;
			}
		}
	}
	
	private function rotApple() {
		if (appleDropped) {
			rotTimer -= FlxG.elapsed;
			if (rotTimer < 0 && !rotLock) {
				apple.animation.play("appleRot");
				iAmRotten = true;
				rotTimer = 5;
				rotLock = true;
			}
			if (rotTimer < 0 && rotLock) {
				iAmDead = true;
			}
		}
	}
	
	public function setRiseRun(basketSprite:FlxSprite) {
		basket = basketSprite;
		if(basket != null){
			run = -(basket.x + 30 - apple.x);
			rise = -(basket.y + 30 - apple.y);
		}
	}
	
	public function sendAppleToBasket() {
		if(flyToBasket && basket != null){
			if (apple != null) {
				if (apple.y >= basket.y + 30) {
					apple.y -= rise/25;
				}
				if (apple.x >= basket.x + 30) {
					apple.x -= run/25;
				}
				if (apple.x <= basket.x + 32 && apple.y <= basket.y + 32) {
					inBasket = true;
				}
			}
		}
	}
	
	private function leavesFallAnimation() {
		if (!appleDropped && leavesFallOnce) {
			pickedLeavesSound = FlxG.sound.load(AssetPaths.pickedLeavesSound__wav);
			pickedLeavesSound.play();
			pickedAppleLeaves.x = apple.x + 5;
			pickedAppleLeaves.y = apple.y + 5;
			pickedAppleLeaves.alpha = 1;
			pickedAppleLeaves.animation.play("leavesFall");
			leavesFallOnce = false;
		}
		if (appleDropped && leavesFallOnce) {
			pickedAppleGround = FlxG.sound.load(AssetPaths.pickedApplesGround__wav);
			pickedAppleGround.play();
			leavesFallOnce = false;
		}
	}
	
	private function resolveAnimation() {
		if (pickedAppleLeaves.animation.finished) {
			pickedAppleLeaves.alpha = 0;
		}
	}
	
	private function destroySounds() {
		if (pickedAppleGround != null && !pickedAppleGround.playing) {
			pickedAppleGround = FlxDestroyUtil.destroy(pickedAppleGround);
			//trace("destroying pickedAppleGroundSound");
		}
		if (fallenAppleSound != null && !fallenAppleSound.playing) {
			fallenAppleSound = FlxDestroyUtil.destroy(fallenAppleSound);
			//trace("destroying fallenAppleSound");
		}
		if (pickedLeavesSound != null && !pickedLeavesSound.playing ) {
			pickedLeavesSound = FlxDestroyUtil.destroy(pickedLeavesSound);
			//trace("destroying pickedLeavesSound");
		}
	}
	
	override public function update() {
		rotApple();
		dropApple();
		revealApples(); 
		sendAppleToBasket(); 
		leavesFallAnimation();
		resolveAnimation();
		destroySounds();
		super.update();
		
	}
	
	override public function destroy() {
		apple = null;
		appleGroup = null;
		basket = null;
		pickedAppleLeaves = null;
		pickedLeavesSound = null;
		pickedAppleGround = null;
		fallenAppleSound = null;
		super.destroy();
		
	}
	
}