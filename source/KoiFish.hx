package ;

import flixel.addons.display.shapes.FlxShapeBox;
import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.util.FlxRandom;
import flixel.FlxG;

/**
 * ...
 * @author ...
 */
class KoiFish extends FlxGroup
{
	
	private var koiFishGroup:FlxGroup;
	private var koiFish:FlxSprite;
	private var fishDirectionTime:Float = 0;
	private var currentFishDirection:Int;
	private var lastFishDirection:Int;
	private var koiFishSpeed:Float = .1;
	private var koiFishMaxSwimHeight:Float;
	private var koiFishMaxSwimWidth:Float;
	private var koiFishMaxSwimHeightNeg:Float;
	private var koiFishMaxSwimWidthNeg:Float;
	
	public function new(koiPondX:Float=0, koiPondY:Float=0) 
	{
		super();
		koiFishGroup = new FlxGroup();
		koiFish = new FlxSprite();
		koiFish.loadGraphic(AssetPaths.koiFishAnimation__png, true, 10, 10);
		koiFish.animation.add("koiFishSwimUp", [0, 1], 8, true);
		koiFish.animation.add("koiFishSwimRight", [2, 3], 8, true);
		koiFish.animation.add("koiFishSwimLeft", [4, 5], 8, true);
		koiFish.animation.add("koiFishSwimDown", [6, 7], 8, true);
		koiFish.animation.add("koiFishSwimUpLeft", [8, 9], 8, true);
		koiFish.animation.add("koiFishSwimUpRight", [10, 11], 8, true);
		koiFish.animation.add("koiFishSwimDownRight", [12, 13], 8, true);
		koiFish.animation.add("koiFishSwimDownLeft", [14, 15], 8, true);
		koiFish.x = koiPondX + 35;
		koiFish.y = koiPondY + 35;
		koiFishMaxSwimHeight = koiFish.y + 13;
		koiFishMaxSwimWidth = koiFish.x + 10;
		koiFishMaxSwimHeightNeg = koiFish.y - 7;
		koiFishMaxSwimWidthNeg = koiFish.x - 8;
		koiFishGroup.add(koiFish);
		add(koiFishGroup);
	}
	
	public function getFishSprite() {
		return koiFish;
	}
	
	public function fishScared() {
		koiFishSpeed = .4;
	}
	
	private function fishSwimAI() {
		
		fishDirectionTime -= FlxG.elapsed;
		if (fishDirectionTime <= 0) {
			fishDirectionTime = FlxRandom.floatRanged(1, 2);
			currentFishDirection = FlxRandom.intRanged(0, 7);
			lastFishDirection = currentFishDirection;
		}
		
		if (koiFish.x >= koiFishMaxSwimWidth) {
			currentFishDirection = FlxRandom.intRanged(4, 6);
			fishDirectionTime = FlxRandom.floatRanged(1, 2);
		}
		if (koiFish.x <= koiFishMaxSwimWidthNeg) {
			currentFishDirection = FlxRandom.intRanged(0, 2);
			fishDirectionTime = FlxRandom.floatRanged(1, 2);
		}
		if (koiFish.y >= koiFishMaxSwimHeight) {
			currentFishDirection = FlxRandom.intRanged(6, 7);
			fishDirectionTime = FlxRandom.floatRanged(1, 2);
		}
		if (koiFish.y <= koiFishMaxSwimHeightNeg) {
			currentFishDirection = FlxRandom.intRanged(3, 4);
			fishDirectionTime = FlxRandom.floatRanged(1, 2);
		}
		
		switch(currentFishDirection) {
			case 0:
				//upRight
				koiFish.x += koiFishSpeed;
				koiFish.y -= koiFishSpeed;
				koiFish.animation.play("koiFishSwimUpRight");
			case 1:
				//right
				koiFish.x += koiFishSpeed;
				koiFish.animation.play("koiFishSwimRight");
			case 2:
				//DownRight
				koiFish.x += koiFishSpeed;
				koiFish.y += koiFishSpeed;
				koiFish.animation.play("koiFishSwimDownRight");
				
			case 3:
				//down
				koiFish.y += koiFishSpeed;
				koiFish.animation.play("koiFishSwimDown");
			case 4:
				//DownLeft
				koiFish.x -= koiFishSpeed;
				koiFish.y += koiFishSpeed;
				koiFish.animation.play("koiFishSwimDownLeft");
			case 5:
				//left
				koiFish.x -= koiFishSpeed;
				koiFish.animation.play("koiFishSwimLeft");
			case 6:
				//upLeft
				koiFish.x -= koiFishSpeed;
				koiFish.y -= koiFishSpeed;
				koiFish.animation.play("koiFishSwimUpLeft");
			case 7:
				//UP
				koiFish.y -= koiFishSpeed;
				koiFish.animation.play("koiFishSwimUp");
		}
		
	}
	
	private function calmFishDown() {
		if (koiFishSpeed > .1) {
			koiFishSpeed -= .001;
		}
	}
	
	override public function update() {
		fishSwimAI();
		calmFishDown();
		super.update();
	}
	
	override public function destroy() {
		koiFish = null;
		koiFishGroup;
		super.destroy();
	}
	
}