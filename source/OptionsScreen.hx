package ;

import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.ui.FlxButton;
import flixel.plugin.MouseEventManager;
import openfl.events.MouseEvent;
import flash.system.System;
import flixel.input.android.FlxAndroidKeyList;
import flixel.input.android.FlxAndroidKeys;
import flixel.FlxG;

/**
 * ...
 * @author BrioLavone
 */
class OptionsScreen extends FlxGroup
{

	private var optionsScreenGroup:FlxGroup;
	private var optionsScreenSprite:FlxSprite;
	private var exitButton:FlxButton;
	private var resetButton:FlxButton;
	private var mainMenuButton:FlxButton;
	private var gameRestart:Bool = false;
	private var exitingGame:Bool = false;
	
	public function new() 
	{
		
		super();
		optionsScreenGroup = new FlxGroup();
		optionsScreenSprite = new FlxSprite();
		exitButton = new FlxButton(0, 0, "Exit Game");
		resetButton = new FlxButton(0, 0, "Reset");
		mainMenuButton = new FlxButton(0, 0, "Main Menu");
		optionsScreenSprite.loadGraphic(AssetPaths.optionsScreen__png, false);
		optionsScreenSprite.x = 440 / 2;
		optionsScreenSprite.y = 280 / 2;
		resetButton.x = optionsScreenSprite.x + 63;
		resetButton.y = optionsScreenSprite.y + 10;
		mainMenuButton.x = optionsScreenSprite.x + 63;
		mainMenuButton.y = optionsScreenSprite.y + 40;
		exitButton.x = optionsScreenSprite.x + 63;
		exitButton.y = optionsScreenSprite.y + 70;
		optionsScreenSprite.alpha = 0;
		//MouseEventManager.add(exitButton, exitGame, null, null, null);
		MouseEventManager.add(resetButton, resetGame, null, null, null);
		MouseEventManager.add(mainMenuButton, returnToMain, null, null, null);
		optionsScreenGroup.add(optionsScreenSprite);
		add(optionsScreenGroup);
		
	}
	
	public function getGameRestart() {
		return gameRestart;
	}
	
	public function getGameEnd() {
		return exitingGame;
	}
	
	public function setGameRestart(val:Bool) {
		gameRestart = val;	
	}
	
	public function menuVisible(val:Int) {
		if (val == 0) {
			optionsScreenSprite.alpha = 0;
			//optionsScreenGroup.remove(exitButton);
			optionsScreenGroup.remove(resetButton);
			optionsScreenGroup.remove(mainMenuButton);
		}
		else {
			optionsScreenSprite.alpha = 1;
			//optionsScreenGroup.add(exitButton);
			optionsScreenGroup.add(resetButton);
			optionsScreenGroup.add(mainMenuButton);
		}
	}
	
	public function exitGame(button:FlxButton) {
		exitingGame = true;
	}
	
	public function resetGame(button:FlxButton) {
		gameRestart = true;
	}
	
	public function returnToMain(button:FlxButton) {
		Greenhouse.greenHouseInPlay = false;
		FlxG.switchState(new MenuState());
	}
	
	override function destroy() {
		destroy;
		super.destroy();
	}
	
}