package ;

import flixel.FlxSprite;

/**
 * ...
 * @author BrioLavone
 */
class Tile extends FlxSprite {
	
	private var hasObject:Bool = false;
	private var tileOwned:Bool = false;
	private var tileClicked:Bool = false;
	private var buyMeSprite:FlxSprite;
	
	public function new(X:Float = 0, Y:Float = 0) {
		
		super(X, Y);
		loadGraphic(AssetPaths.tileAnimation__png, false, 192, 96);
		animation.add("owned", [0], 1, false);
		animation.add("notOwned", [1], 1, false);
		animation.play("notOwned");
		this.x = 223;
		this.y = 120;
		
	}
	
	public function getTileOwned() {
		return tileOwned;
	}
	
	public function getHasObject() {
		
		return hasObject;
		
	}
	
	public function getTileClicked() {
		return tileClicked;
	}
	
	public function setTileClicked(val:Bool) {
		tileClicked = val;
	}
	
	public function setTileOwned(val:Bool) {
		tileOwned = val;
	}
	
	public function setHasObject(val:Bool) {
		
		hasObject = val;
		
	}
	
	public function setAsOwned() {
		animation.play("owned");
		tileOwned = true;
	}
	
	public function setAsUnowned() {
		animation.play("notOwned");
		tileOwned = false;
	}
	
	override public function update() {
		
		super.update();
		
	}
	
}