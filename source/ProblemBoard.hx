package ;

import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.FlxG;
import flixel.util.FlxRandom;
import Math;

/**
 * ...
 * @author BrioLavone
 */
class ProblemBoard extends FlxGroup
{

	private var problemGroup:FlxGroup;
	private var blackBoard:FlxSprite;
	private var blackBoardCorrectBoarder:FlxSprite;
	private var blackBoardIncorrectBoarder:FlxSprite;
	private var singleDigitL:FlxSprite;
	private var singleDigitR:FlxSprite;
	private var operationVal:FlxSprite;
	private var problemAnswer:Int;
	private var problemAnswerString:String = "";
	private var equalsSign:FlxSprite;
	private var plusSign:FlxSprite;
	private var minusSign:FlxSprite;
	private var multiSign:FlxSprite;
	private var numberObjLeft:NumberChalk;
	private var numberObjRight:NumberChalk;
	private var leftSideNumber:FlxSprite;
	private var rightSideNumber:FlxSprite;
	private var numArray:Array<FlxSprite>;
	private var operationArray:Array<FlxSprite>;
	private var boardInUse:Bool = false;
	private var opShift:Int = 0;
	private var opShiftX:Int = 0;
	
	public function new() 
	{
		super();	
		init();
		
	}
	
	private function init() {
		
		numArray = new Array<FlxSprite>();
		operationArray = new Array<FlxSprite>();
		blackBoard = new FlxSprite();
		blackBoardCorrectBoarder = new FlxSprite();
		blackBoardIncorrectBoarder = new FlxSprite();
		equalsSign = new FlxSprite();
		plusSign = new FlxSprite();
		minusSign = new FlxSprite();
		multiSign = new FlxSprite();
		operationVal = new FlxSprite();
		numberObjLeft = new NumberChalk();
		numberObjRight = new NumberChalk();
		leftSideNumber = new FlxSprite();
		rightSideNumber = new FlxSprite();
		blackBoard.loadGraphic(AssetPaths.blackBoard__png, false);
		blackBoardCorrectBoarder.loadGraphic(AssetPaths.blackBoardCorrectBorder__png, false);
		blackBoardIncorrectBoarder.loadGraphic(AssetPaths.blackBoardWrongBorder__png, false);
		equalsSign.loadGraphic(AssetPaths.chalkEquals__png, false);
		plusSign.loadGraphic(AssetPaths.plusOperation__png, false);
		minusSign.loadGraphic(AssetPaths.minusOperation__png, false);
		multiSign.loadGraphic(AssetPaths.multiOperation__png, false );
		equalsSign.scale.x = .7;
		equalsSign.scale.y = .7;
		plusSign.scale.x = .7;
		plusSign.scale.y = .7;
		minusSign.scale.x = .7;
		minusSign.scale.y = .7;
		blackBoard.x = 245;
		blackBoard.y -= 100;
		blackBoardCorrectBoarder.x = 245;
		blackBoardCorrectBoarder.y -= 100;
		blackBoardIncorrectBoarder.x = 245;
		blackBoardIncorrectBoarder.y -= 100;
		blackBoardCorrectBoarder.alpha = 0;
		blackBoardIncorrectBoarder.alpha = 0;
		operationArray[0] = plusSign;
		operationArray[1] = minusSign;
		
		add(blackBoard);
		add(blackBoardCorrectBoarder);
		add(blackBoardIncorrectBoarder);
		add(equalsSign);
		
	}
	
	public function boardVisibile(val:Bool) {
		
		switch(val) {
			
			case true:
				blackBoard.alpha = 1;
			case false:
				blackBoard.alpha = 0;
			
		}
		
	}
	
	public function getProblemAnswer() {
		
		return problemAnswer;
		
	}
	
	public function getProblemAnswerString() {
		
		return problemAnswerString;
		
	}
	
	public function resetProblemString() {
		
		problemAnswerString = "";
		
	}
	
	public function getBoardY() {
		
		return blackBoard.y;
		
	}
	
	public function getBoardInUse() {
		
		return boardInUse;
		
	}
	
	public function setBoardInUse(val:Bool) {
		
		boardInUse = val;
		
	}
	
	public function setBoarderColor(val:String) {
		if (val == "correctAnswer") {
			blackBoardCorrectBoarder.alpha = 1;
		}
		else {
			blackBoardIncorrectBoarder.alpha = 1;
		}
	}
	
	private function boardScrollDown() {
		
		switch(boardInUse) {
		
			case true:
				if(blackBoard.y <= 30){
					blackBoard.y += 3;
					blackBoardCorrectBoarder.y += 3;
					blackBoardIncorrectBoarder.y += 3;
				}
			case false:
				if (blackBoard.y >= -80) {
					blackBoard.y -= 6;
					blackBoardCorrectBoarder.y -= 6;
					blackBoardIncorrectBoarder.y -= 6;
				}
			
		}
	}
	
	private function solveProblem(operation:Int, leftRand:Int, rightRand:Int) {
		
		if (operation == 1) {
			
			if (leftRand < rightRand) {
				
				problemAnswer = rightRand - leftRand;
				
			}
			else {
				
				problemAnswer = leftRand - rightRand;
				
			}
			
		}
		if(operation == 0) {
			
			problemAnswer = leftRand + rightRand;
			
		}
		if (operation == 2) {
			problemAnswer = leftRand * rightRand;
		}
		
		numberToString(problemAnswer);
		reverseAnswerString();

	}
	
	public function pickOperation(opType:String) {
		if (opType == "tree") {
			opShiftX = 0;
			var temp:FlxSprite;
			var randNum:Int = FlxRandom.intRanged(0, 1);		
			leftSideNumber = numberObjLeft.getRandomNumber(); 
			rightSideNumber = numberObjRight.getRandomNumber(); 
			operationVal = operationArray[randNum];
			var leftRandNum:Int = numberObjLeft.getMyIndex();
			var rightRandNum:Int = numberObjRight.getMyIndex();
			if (leftRandNum < rightRandNum) {
				temp = leftSideNumber;
				leftSideNumber = rightSideNumber;
				rightSideNumber = temp;
			}
			if (randNum == 0) {
				opShift = 25;	
			}
			else {
				opShift = 35;
			}
			solveProblem(randNum, leftRandNum, rightRandNum);
			add(operationVal);
			add(leftSideNumber);
			add(rightSideNumber);
		}
		if (opType == "machine") {
			var temp:FlxSprite;
			var randNum:Int = FlxRandom.intRanged(0, 1);		
			leftSideNumber = numberObjLeft.getRandomNumber(); 
			rightSideNumber = numberObjRight.getRandomNumber(); 
			operationVal = multiSign;
			var leftRandNum:Int = numberObjLeft.getMyIndex();
			var rightRandNum:Int = numberObjRight.getMyIndex();
			opShift = 32;
			opShiftX = 5;
			solveProblem(2, leftRandNum, rightRandNum);
			add(operationVal);
			add(leftSideNumber);
			add(rightSideNumber);
		}
	}
	
	private function numberToString(number:Float) {
		
		if (number/10 < 1) {
			
			switch(number%10) {
			
			case 0:
				problemAnswerString += "0";
			case 1:
				problemAnswerString += "1";
			case 2:
				problemAnswerString += "2";
			case 3:
				problemAnswerString += "3";	
			case 4:
				problemAnswerString += "4";
			case 5:
				problemAnswerString += "5";
			case 6:
				problemAnswerString += "6";
			case 7:
				problemAnswerString += "7";
			case 8:
				problemAnswerString += "8";
			case 9:
				problemAnswerString += "9";	
				
			}
			return;
			
		}
	
		switch(number%10) {
			
			case 0:
				problemAnswerString += "0";
			case 1:
				problemAnswerString += "1";
			case 2:
				problemAnswerString += "2";
			case 3:
				problemAnswerString += "3";	
			case 4:
				problemAnswerString += "4";
			case 5:
				problemAnswerString += "5";
			case 6:
				problemAnswerString += "6";
			case 7:
				problemAnswerString += "7";
			case 8:
				problemAnswerString += "8";
			case 9:
				problemAnswerString += "9";	
				
		}
		
		numberToString(Math.floor(number/10));	
		
		
	}
	
	private function reverseAnswerString() {
		
		var stringLength:Int = problemAnswerString.length; 
		var tempString:String = "";
		while (stringLength >= 0) {
			
			tempString += problemAnswerString.charAt(stringLength);
			stringLength--;
			
		}
		
		problemAnswerString = tempString;
		
	}
	
	private function updateBoardPieces() {
		
		leftSideNumber.y = blackBoard.y;
		leftSideNumber.x = blackBoard.x;
		rightSideNumber.y = blackBoard.y;
		rightSideNumber.x = blackBoard.x + 70;
		equalsSign.x = blackBoard.x + 120;
		equalsSign.y = blackBoard.y + 25;
		operationVal.x = blackBoard.x + 50 + opShiftX;
		operationVal.y = blackBoard.y + opShift;
		
	}
	
	override public function update() {
		
		boardScrollDown();
		updateBoardPieces();
		super.update();
		
	}
	
	override public function destroy() {
		
		numArray = null;
		operationArray = null;
		blackBoard = null;
		blackBoardCorrectBoarder = null;
		blackBoardIncorrectBoarder = null;
		equalsSign = null;
		plusSign = null;
		minusSign = null;
		multiSign = null;
		operationVal = null;
		numberObjLeft = null;
		numberObjRight = null;
		leftSideNumber = null;
		rightSideNumber = null;
		super.destroy();
		
	}
	
}